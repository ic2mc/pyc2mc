# Notebooks

This folder contains only links to jupyter notebooks available in
the documentation. This is a convenient way to display the notebooks
in the documentation while keeping them in the top `notebooks/` folder.