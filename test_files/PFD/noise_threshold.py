from file_processing import *
import pandas as pd
import matplotlib.pyplot as plt
from scipy.signal import find_peaks

DEBUG = True

def histogram(x: np.array, y: np.array, bin_width=1e-2, mz_range=(150,1500), plot=False)-> tuple:
    '''
    x: np.array
        The x-values to be binned
    y: np.array
        The y-values to be summed
    bin_width: float
        The width of the bins
    mz_range: tuple
        The range of mz values to consider when binning
    plot: bool
        Whether to plot the histogram
    return: tuple
        A tuple containing the bin edges and the binned sums
    '''

    # Determine the range of x-values for binning
    x_min = np.min(x)
    x_max = np.max(x)
    
    # Create the bin edges
    bin_edges = np.arange(x_min, x_max + bin_width, bin_width)
    
    # Using digitize to determine the index of the bin each x value belongs to
    bin_indices = np.digitize(x, bin_edges)
    
    # Summing up y-values for each bin
    binned_sums = np.bincount(bin_indices - 1, weights=y)
    
    # Get the binned sums and bin edges that are on a 400-550 x range
    x_lower = mz_range[0]
    x_upper = mz_range[1]

    # Identify valid bin start indices
    valid_bins_start = np.where((bin_edges[:-1] >= x_lower) & (bin_edges[:-1] < x_upper))[0]

    # Use the valid bin start indices to select binned_sums
    binned_sums = binned_sums[valid_bins_start]

    # Adjust indices to capture both start and end edges of each valid bin
    valid_bin_edges = np.concatenate([valid_bins_start, [valid_bins_start[-1]+1]])
    bin_edges = bin_edges[valid_bin_edges]
    
    if plot:
        # Plotting
        print('Plotting')
        plt.bar(bin_edges[:-1], binned_sums, width=bin_width, align='edge')
        plt.xlabel('X-values')
        plt.ylabel('Sum of Y-values')
        plt.title('Histogram with custom bin width')
        plt.show()

    return  bin_edges[:-1], binned_sums

def function_fit(y_data: np.array, x_range: tuple, x_data: np.array, title: str, ax=None, deg=10)-> np.array:
    '''
    y_data: np.array
        The y-values to be fitted
    x_range: tuple
        The range of x-values to consider when fitting
    x_data: np.array
        The x-values to be fitted
    title: str
        The title of the plot
    ax: plt.axis
        The axis to plot on
    deg: int
        The degree of the polynomial to fit
    return: np.array
        The y-values of the fit
        p: np.array
        The coefficients of the polynomial fit
    '''

    x_space = np.arange(x_range[0], x_range[1])

    p = np.polyfit(x_space, y_data, deg)
    y_fit = np.polyval(p, x_data)

    if ax is not None:
        ax.plot(x_space, y_data)
        ax.plot(x_data, y_fit, color='red')
        ax.set_title(title)

    return y_fit, p


def three_point_fit(mz_data: np.array, frequency_data: np.array, peak_indices: np.array)-> tuple:
    '''
    INTERPOLATION
    In the three-point fit method, each peak is fit using three data points: the point at the peak maximum and the two adjacent points.
    Mathematically, this can be represented by a quadratic equation:
        y=ax2+bx+c

    Where y is the intensity, and x is the mass-to-charge ratio (m/z). The coefficients a, b, and c are determined using the three 
    data points surrounding the peak.

    1. Identify Candidate Peaks: Initial identification of peaks can be done using a basic thresholding method, where points higher 
    than a certain intensity value are flagged.
    2. Take Three Points: For each candidate peak, take the intensity and m/z values for the peak maximum and the two adjacent points.
    3. Solve for Coefficients: Using the three points, solve for a, b, and c in the quadratic equation. This can be done using simple 
    algebra or matrix operations.
    4. Calculate the Peak Position: The position x_0 of the actual peak can be calculated as:
        x_0 = -b / (2 * a)
    This position often provides a more accurate measure of the peak's m/z ratio than simply using the data point with the highest 
    intensity.
    '''

    peak_positions = []
    peak_frequencies = []
    for i in peak_indices:
        if i == 0 or i == len(mz_data) - 1:
            continue # Skip the first and last points

        left = frequency_data[i-1]
        middle = frequency_data[i]
        right = frequency_data[i+1]

        # Get the mz values for the three points
        x1, x2, x3 = mz_data[i-1:i+2]
        y1, y2, y3 = left, middle, right

        # Perform three-point fit
        a = (y1 + y3 - 2 * y2) / ((x1 - x2) * (x1 - x3))
        b = (y3 - y1) / (x3 - x1) - a * (x1 + x3)
        c = y1 - a * x1**2 - b * x1

        # Calculate the peak position and frequency
        x0 = -b / (2 * a)
        y0 = a * x0**2 + b * x0 + c

        peak_positions.append(x0)
        peak_frequencies.append(y0)

    return peak_positions, peak_frequencies

# @time_it
def get_nominal_indexes(mz_of_signal: np.array, mz_range: tuple)-> list:
    '''
    mz_of_signal: np.array
        The mz values of the signal
    mz_range: tuple
        The range of mz values to consider when finding peaks
    return: list
        A list of indexes for each nominal mass in the mz range
    '''
    # List to store indexes for each nominal mass
    nominal_indexes = []
    print(mz_range)
    for i in range(mz_range[0], mz_range[1]):
        # Find the index of the first element in mz_of_signal that is greater than or equal to i
        lower_bound = np.searchsorted(mz_of_signal, i, side='left')
        # Find the index of the first element in mz_of_signal that is greater than or equal to i+1
        upper_bound = np.searchsorted(mz_of_signal, i+1, side='left')

        # Create an array of indexes from lower_bound to upper_bound
        nominal_indexes.append(np.arange(lower_bound, upper_bound))
    return nominal_indexes


def get_noise_values(mz_of_signal: np.array = None, signal: np.array = None, 
                    nominal_indexes: list = None, mz_range: tuple = (150, 1000), 
                    hist_bin_width: float = 1e-2, window_size: int = 5) -> tuple:
    '''
    mz_of_signal: np.array
        The mz values of the signal
    signal: np.array
        The intensity values of the signal
    nominal_indexes: list
        A list of indexes for each nominal mass in the mz range
    mz_range: tuple
        The range of mz values to consider when finding peaks
    hist_bin_width: float
        The width of the bins in the histogram (Da)
    window_size: int
        Number of histogram bins to consider when finding the noise threshold
    return: tuple
        A tuple containing the means and standard deviations of the noise for each nominal mass
    '''
    
    # Check if data is provided
    if mz_of_signal is None or signal is None:
        raise ValueError('Data must be provided')

    # Check if nominal indexes are provided
    if nominal_indexes is None:
        nominal_indexes = get_nominal_indexes(mz_of_signal, mz_range)

    # For each nominal mass in the mz range, find a proper average and standard deviation 
    # Arrays for the means and standard deviations of the noise. Initialized to zeros length of mz_range
    means = np.zeros(mz_range[1] - mz_range[0]) 
    st_dvs = np.zeros(mz_range[1] - mz_range[0])
    log.debug('Finding noise values')
    for i in range(mz_range[0], mz_range[1]):
        # Get the index of the current nominal mass
        idx = i - mz_range[0]

        # Get the mz and signal values for the current nominal mass
        mz_nominal = mz_of_signal[nominal_indexes[idx]]
        signal_nominal = signal[nominal_indexes[idx]]

        # Get the histogram of the signal
        bins, sum = histogram(x=mz_nominal, y=signal_nominal, bin_width=hist_bin_width, mz_range=(i, i+1))

        # Negative histogram values set to 0
        sum[sum < 0] = 0

        # Rolling window algorithm used to obtain the n smallest consecutive values. Thus, the region with smallest average
        # in the nominal mass is obtained.
        _, low_index, upper_index = n_smallest_consecutive(sum, window_size)

        # Get minimum and maximum mz values of the region
        min_mz, max_mz = bins[low_index], bins[upper_index]

        # Calculate mean and standard deviation of the signal in the region
        # (CHANGE: Return the sum from n_smallest to avoid recalculating the mean)
        valid_region = (mz_nominal >= min_mz) & (mz_nominal <= max_mz)
        means[idx]=np.average(signal_nominal[valid_region])
        st_dvs[idx]=np.std(signal_nominal[valid_region])

    return means, st_dvs


def save_as_pks(filename: str, peak_location: np.array, peak_heights: np.array, 
                sn: np.array, freq_array: np.array, avg_avg: float, avg_std: float)-> None:

    six_sigma = avg_avg + 6 * avg_std

    with open(filename, 'w') as f:
        f.write("Predator Analysis Peaklist V2.0 - There were {} Peaks found from m/z= {:8.2f}    to m/z= {:8.2f}.\n".format(len(peak_location), peak_location[0], peak_location[-1]))
        f.write('NEGATIVE ION MODE\n')
        f.write("Peak threshold: {:.6f}      Noise threshold: {:.6f}    Baseline for SN: {:.6f}   Sigma for SN: {:.6f}\n\n".format(
            six_sigma,
            six_sigma,
            avg_avg,
            avg_std)
        )
        f.write("Center of Mass for the Assigned Window is  {:9.4f}\n".format(np.mean(peak_location)))
        
        f.write("\n Peak Location      Peak Height            Abundance             Resolving Power        Frequency               S/N\n\n")
        
        max_height = max(peak_heights)
        for loc, abundance, frequency, sn in zip(peak_location, peak_heights, freq_array, sn):
            # Note: Replace the following placeholder values with actual calculations or data where necessary
            height = abundance / max_height * 100
            resolving_power = 1500000
            
            f.write("{:12.7f} {:17.3f} {:21.3f} {:23.0f} {:24.6f} {:15.3f}\n".format(loc, height, abundance, resolving_power, frequency, sn))


def n_smallest_consecutive(arr: np.array , n: int)-> tuple:
    '''
    arr: np.array
        The array to find the n smallest consecutive values
    n: int
        The number of consecutive values to find
    return: tuple
        A tuple containing the n consecutive values and their bounding indexes
    '''
    # Create a rolling window sum
    rolling_sum = np.convolve(arr, np.ones(n), 'valid')
    
    # Find the starting index of the minimum sum
    min_index = np.argmin(rolling_sum)
    
    # Return the n consecutive values and their bounding indexes
    return arr[min_index:min_index + n], min_index, min_index + n - 1

def read_first_file(path: str, type: str = 'pfd')-> object:
    pfd_file_paths = get_binary_files(path)
    data = read_pfd(pfd_file_paths[0])
        
    return data

def get_frequency_array(a : float, b : float, t : float, peak_locations: np.array)-> np.array:
    '''
    a: float
        The a term in the frequency calculation
    b: float
        The b term in the frequency calculation
    t: float
        The trap voltage in the frequency calculation
    peak_locations: np.array
        The locations of the peaks
    return: np.array
        The frequency array
    '''
    # Calculate the frequency of each peak
    # f = {sqrt(a^2 + 4*b*m*|t|)+a}/2m
    frequency_array = (np.sqrt(a**2 + 4*b*peak_locations*t) + a) / (2*peak_locations)

    return frequency_array


def  get_peaks(mean_function: np.array, st_dv_function: np.array, mz_of_signal: np.array, signal: np.array, num_sigmas=6)-> tuple:
    '''
    mean_function: np.array
        The mean function
    st_dv_function: np.array
        The standard deviation function
    mz_of_signal: np.array
        The mz values of the signal
    signal: np.array
        The intensity values of the signal
    nominal_indexes: list   
        A list of indexes for each nominal mass in the mz range
    mz_range: tuple
        The range of mz values to consider when finding peaks
    num_sigmas: int
        The number of standard deviations above the mean for the noise threshold
    return: tuple
        A tuple containing the peak locations, peak frequencies, signal-to-noise ratios, and noise thresholds
    '''

    # ADD RESOLVING POWER CALCULATION

    noise_thresholds = mean_function + num_sigmas * st_dv_function

    peak_indices, _ = find_peaks(signal, height=noise_thresholds)
    peak_locs, peak_signal = three_point_fit(mz_of_signal, signal, peak_indices)

    peak_locs = np.array(peak_locs)
    peak_signal = np.array(peak_signal)
    noise_thresholds = np.array(noise_thresholds)

    return peak_locs, peak_signal, noise_thresholds

def get_signal_to_noise(peak_signal: np.array, peak_locations: np.array, st_dv_function: np.array, mean_function: np.array, num_sigmas: int = 6)-> None:
    '''
    peak_signal: np.array
        The signal of the peaks
    peak_locations: np.array
        The locations of the peaks
    st_dv_function: np.array
        The standard deviation function
    mean_function: np.array
        The mean function
    num_sigmas: int
        The number of standard deviations above the mean for the noise threshold
    return: np.array
        The signal-to-noise ratios
    '''

    noise = np.polyval(st_dv_function, peak_locations) * 6 + np.polyval(mean_function, peak_locations)

    # Calculate the signal-to-noise ratio for each peak
    signal_to_noise = peak_signal / noise

    return signal_to_noise


def pfd_to_pks( mz_range:tuple=(200,1000), 
                num_sigmas:int = 6, 
                window_size:float = 5,     
                hist_bin_width:float = 1e-2, 
                fit_degree = 10,
                path:str = 'C:/Users/ajtello/Desktop/Martha/PFD/test/', 
                plot:bool = True,   
                save:bool = True)-> None:
    '''
    mz_range: tuple
        The range of mz values to consider when finding peaks. If upper bound greater than real max mz, 
        or lower bound less than real min mz, the function will use the real min and max mz values
    num_sigmas: int
        The number of standard deviations above the mean for the noise threshold
    window_size: float
        The number of the histograms to consider when finding the noise at each nominal mass
    hist_bin_width: float
        The width of the bins for the signal histogram at each nominal mass for noise calculation
    path: str
        The path to the pfd files. Takes a folder path since was expected to be used for batch processing.
        Currenly only processes the first file in the folder
    plot: bool
        Whether to plot and show the peaks and noise thresholds, among other figures
    save: bool
        Whether to save the peaks as a pks file
    '''

    data = read_first_file(path)

    # Get the signal and mz values from the data given the mz range
    mz_of_signal, signal, mz_range = data.mz_space(mz_range)

    if plot:
        # Plot the signal
        log.debug('Plotting signal')
        plt.plot(mz_of_signal, signal, linewidth=0.5, color='r')
        plt.show()

    # Array containing indexes of the mz values for each nominal mass
    nominal_indexes = get_nominal_indexes(mz_of_signal, mz_range)

    # Get the means and standard deviations of the noise for each nominal mass
    means, st_dvs = get_noise_values(mz_of_signal, signal, nominal_indexes, mz_range, hist_bin_width, window_size)

    if not plot:
        # Fit the means and standard deviations to a polynomial
        st_dv_function, s = function_fit(st_dvs, mz_range, mz_of_signal, "Standard Deviations as polyfit", deg=fit_degree)
        mean_function, m = function_fit(means, mz_range, mz_of_signal, "Average as polyfit", deg=fit_degree)
    else:
        # Plot the means and standard deviations, comparing the polynomial fit and the decay fit
        log.debug('Plotting means and standard deviations')
        fig, axs = plt.subplots(1,2)
        st_dv_function, s = function_fit(st_dvs, mz_range, mz_of_signal, "Standard Deviation", axs[0], fit_degree)
        mean_function, m = function_fit(means, mz_range, mz_of_signal,"Average", axs[1], fit_degree)
        plt.show()

    # Find signal peaks above the noise threshold (6 sigma by default)
    peak_locations, peak_signal, noise_thresholds = get_peaks(mean_function, st_dv_function, mz_of_signal, signal, num_sigmas)

    log.debug('Number of peaks found: {}'.format(len(peak_locations)))
    if plot:
        # Plot the signal, peaks, and noise thresholds
        log.debug('Plotting spectra')
        plt.plot(peak_locations, peak_signal, 'x', color='blue') # Peaks
        plt.plot(mz_of_signal, signal, linewidth=0.5, color='r') # Spectra
        plt.plot(mz_of_signal, noise_thresholds, color='black', linewidth=0.2) # Noise thresholds
        plt.show()

    # Calculate the signal-to-noise ratio for each peak
    signal_to_noise = get_signal_to_noise(peak_signal, peak_locations, s, m, num_sigmas)

    # Calculate the frequency of each peak
    frequency_arr = get_frequency_array(a=data.metadata.a_term, b=data.metadata.b_term, t=data.metadata.trap_voltage, peak_locations=peak_locations)

    if save:
        # Save the peaks as a pks file
        log.debug('Saving peaks')
        save_as_pks('test.pks', peak_locations, peak_signal, signal_to_noise, frequency_arr, np.average(mean_function), np.average(st_dv_function))


def main():
    print("Debugging enabled") if DEBUG else None

    pfd_to_pks(path = 'Z:/ICR/PyC2MC/Noise Fitting for AJ/3', num_sigmas=6, plot=True, save=False, mz_range=(150, 1000))
    # pfd_to_pks(path = 'C:/Users/ajtello/Desktop/Scripts/Testing_Data/Noise/', plot=True, save=False)



if __name__ == "__main__":
    main()
