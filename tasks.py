"""
Pyinvoke tasks.py file for automating production of tests,
benchmarks, docs and admin stuff.
"""

import datetime
import webbrowser
from pathlib import Path

from invoke import task

doc_source = Path(__file__).parent / "docs" / "source"
doc_build = Path(__file__).parent / "docs" / "build"
cov_path = Path(__file__).parent / "htmlcov"
bench_reference = "ref_bench.json"
benchmarks = ".benchmarks/**/*"


@task
def clean(ctx, docs=False, bytecode=False, benchmarks=False, extra=''):
    patterns = ['build']
    if docs:
        patterns.append('docs/build')
    if bytecode:
        patterns.append('**/*.pyc')
    if extra:
        patterns.append(extra)
    if benchmarks:
        patterns.append(".benchmarks")
    for pattern in patterns:
        ctx.run("rm -rf {}".format(pattern))


@task
def build(ctx, docs=False):
    """ build package """
    ctx.run("python setup.py build")
    if docs:
        build_doc(ctx)


@task
def run_tests(ctx, benchmarks=False, refbench=False):
    """ run pytest and (optional) benchmarks """
    if benchmarks:
        ctx.run("pytest --benchmark-autosave --benchmark-enable")
    elif refbench:
        date = f"{datetime.datetime.now():%Y.%-m.%-d}"
        ctx.run(f"pytest --benchmark-json=refbench_{date}.json  "
                "--benchmark-enable --benchmark-only --benchmark-min-rounds=10")
    else:
        # run pytest using default in pyproject.toml
        # by default benchmark are disable
        ctx.run("pytest")


@task
def compare_bench(ctx, num=benchmarks):
    """ Compare available benchmarks with ref_bench.json. """
    ctx.run(f"pytest-benchmark compare --sort=name "
            "--columns='min,max,mean,stddev,rounds' "
            f" {num} {bench_reference}")


@task
def build_doc(ctx, builder="html"):
    """ build documentation from sphinx """
    out_dir = doc_build / builder
    ctx.run(f"sphinx-build -b {builder} {doc_source} {out_dir}")


@task
def open_doc(ctx):
    """ Open local documentation in web browser. """
    p = doc_build / "html/index.html"
    if p.is_file():
        webbrowser.open(f"file://{p}")
    else:
        print("Error: docs is not available. Run invoke make-doc")


@task
def open_cov(ctx):
    """ Open coverage analyzes in web browser. """
    p = cov_path / "index.html"
    if p.is_file():
        webbrowser.open(f"file://{p}")
    else:
        print("Error: cov is not available. Run invoke run-tests")


# TODO: next tasks to implement
# @task
# def update_doc(ctx):
#     """
#     Update the web documentation.

#     :param ctx:
#     """
#     ctx.run("cp docs_rst/conf-normal.py docs_rst/conf.py")
#     make_doc(ctx)
#     ctx.run("git add .")
#     ctx.run('git commit -a -m "Update docs"')
#     ctx.run("git push")


# @task
# def publish(ctx):
#     """
#     Upload release to Pypi using twine.

#     :param ctx:
#     """
#     ctx.run("rm dist/*.*", warn=True)
#     ctx.run("python setup.py sdist bdist_wheel")
#     ctx.run("twine upload dist/*")


# @task
# def set_version(ctx, version):
#     with open("pymatgen/core/__init__.py") as f:
#         contents = f.read()
#         contents = re.sub(r"__version__ = .*\n",
#                           f"__version__ = {version!r}\n", contents)

#     with open("pymatgen/core/__init__.py", "w") as f:
#         f.write(contents)

#     with open("setup.py") as f:
#         contents = f.read()
#         contents = re.sub(r"version=([^,]+),",
#                           f"version={version!r},", contents)

#     with open("setup.py", "w") as f:
#         f.write(contents)


@task
def release(ctx, version=None, docs=True):
    """
    Run full sequence for releasing pyc2mc.

    :param ctx:
    :param nodoc: Whether to skip doc generation.
    """
    version = version or f"{datetime.datetime.now():%Y.%-m.%-d}"
    print(version)
    # ctx.run("rm -r dist build pymatgen.egg-info", warn=True)
    # set_ver(ctx, version)
    # if docs:
    #     make_doc(ctx)
    #     ctx.run("git add .")
    #     ctx.run('git commit --no-verify -a -m "Update docs"')
    #     ctx.run("git push")
    # release_github(ctx, version)

    # ctx.run("rm -f dist/*.*", warn=True)
    # ctx.run("python setup.py sdist bdist_wheel", warn=True)
    # ctx.run("twine upload --skip-existing dist/*.whl", warn=True)
    # ctx.run("twine upload --skip-existing dist/*.tar.gz", warn=True)
