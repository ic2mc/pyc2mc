#!/usr/bin/env python
# coding: utf-8

""" Merge calibration lists\n\n

Read all csv files in a given folder and merge all the calibration points in
a new csv files by droping all duplicates. The duplicates are identified based
on a tolerence criterium over the mass value.
"""


from pathlib import Path
import argparse

import pandas as pd
import numpy as np


def get_options():
    """ get options from command lines """

    parser = argparse.ArgumentParser(
        prog="merge_calib", 
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter,
    )

    # mandatory: folder name with csv files
    parser.add_argument("cl_path",
                        help="Folder containing the calibration lists.",
                        metavar="PATH",
                        type=str)

    # tolerance to include/exclude duplicates
    parser.add_argument("-t", "--tol",
                        help="tolerance, in Da, to exclude duplicates.",
                        metavar="tol",
                        default=1e-5,
                        type=float)

    return vars(parser.parse_args())


def main(cl_path: str = ".", tol: float = 1e-5):
    """ main function

    Args:
        cl_path (str): path containing the calibration lists in csv format
        tol (float): tolerance threshold to consider two points as equal.
        
    """

    cl_path = Path(cl_path)
    if not cl_path.is_dir():
        raise ValueError(f"The provided path is not a folder: {cl_path}")
    
    cl_lists = list()
    for name in cl_path.glob("*.csv"):
        df = pd.read_csv(name, skiprows=1, sep=",", usecols=(2,))
        df.columns = [col.strip() for col in df.columns]
        cl_lists.append(df["Theoretical"].values)
        print(f"{len(df):5d} points: {name}")
    print(f"total {sum([len(mz) for mz in cl_lists])}")
    
    df_lists = list()
    for name in cl_path.glob("*.csv"):
        df = pd.read_csv(name, skiprows=1, sep=",")
        colnames = [col.strip() for col in df.columns]
        df.columns = colnames
        df["name"] = name.stem
        df_lists.append(df)
    
    n_overlap = np.zeros((len(cl_lists), len(cl_lists)), dtype=np.int64)
    for i, cli in enumerate(cl_lists):
        for j, clj in enumerate(cl_lists):
            if j == i:
                continue
            mask = np.abs(cli[:,None] - clj) < tol
            idx, = np.nonzero(mask.any(1))
            jdx, = np.nonzero(mask.any(0))
            n_overlap[i, j] = len(idx)
            n_overlap[j, i] = len(jdx)
    
    print("\n#peak overlap between calibration lists")
    print(n_overlap)
    
    idx_list = list()
    all_mz = cl_lists[0].copy()
    for cl in cl_lists[1:]:
        # look if mz already exists
        mask = np.abs(all_mz[:, None] - cl) < tol
        idx, = np.nonzero(~mask.any(0))
        not_idx, = np.nonzero(mask.any(0))
    
        # concatenate with new mz values
        all_mz = np.concatenate([all_mz, cl[idx]])
#        print(len(not_idx), len(cl), len(all_mz))
        
        # save idx
        idx_list.append(idx)
        
    # concatenate all
    final_df = pd.concat([df_lists[0]] + [df.iloc[idx] for idx, df in zip(idx_list, df_lists[1:])])
    final_df.sort_values(by="Theoretical", inplace=True)
    final_df.reset_index(inplace=True, drop=True)
    final_df.head()
    
    cl_path_out = cl_path / "full_caliblist.csv"
    if not cl_path_out.exists():
        print("\nWrite file:", cl_path_out)
        print("final length:", len(final_df), "\n")
        with cl_path_out.open("w", encoding="utf-8") as out:
            out.write("Merged calibration lists\n")
            final_df.iloc[:, :-1].set_index("Number").to_csv(out)
        print("Done!")
    else:
        raise NameError(f"File {cl_path_out} exists.")

if __name__ == "__main__":
    options = get_options()
    main(**options)
