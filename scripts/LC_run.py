# coding: utf-8

""" LC run calibration and attribution\n\n

Calibrate and then attribute a set of scans.
"""

import argparse
from pathlib import Path

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from pyc2mc.core.formula import Formula
from pyc2mc.core.calibration import CalibList
from pyc2mc.io.caliblist import read_caliblist
from pyc2mc.core.formula_grid import FormulaGridList
from pyc2mc.processing.recalibration import WalkingCalibration, Recalibration
from pyc2mc.time_dependent.td_data import TimeDependentData
from pyc2mc.processing.kendrick_attribution import kendrick_attribution
from pyc2mc.utils import outliers_detection_MAD


def get_options():
    """ get options from command lines """

    def exist(f):
        """ 'Type' for argparse - checks that file exists but does not open """
        if not Path(f).is_file():
            raise argparse.ArgumentTypeError(f"{f} does not exist")
        return f

    def dir_exist(dir):
        """ 'Type' for argparse - checks that file exists but does not open """
        if not Path(dir).is_dir():
            raise argparse.ArgumentTypeError(f"{dir} does not exist")
        return dir

    parser = argparse.ArgumentParser(
        prog="LC_run",
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter,
    )

    # mandatory: folder name with csv files
    parser.add_argument("base_path",
                        help="Folder in which to proceed to the attribution.",
                        metavar="PATH",
                        type=str)

    parser.add_argument("-c", "--cl_file",
                        help="path to the calibration list file.",
                        metavar="calibrationList.csv",
                        type=exist)

    parser.add_argument("-g", "--fgrid",
                        help="path to the Formula Grid definition.",
                        metavar="fgrid.toml",
                        type=exist)

    parser.add_argument("-p", "--pks_dir",
                        help="path to the directory containing pks files.",
                        metavar="PKS/",
                        type=dir_exist)

    parser.add_argument("-m", "--method",
                        help=("Choose between 'caliblist' or 'kendrick' "
                              " calibration (default: caliblist)"),
                        dest="method", choices=["caliblist", "kendrick"],
                        default="caliblist",
                        type=str)

    parser.add_argument("-r", "--repeat_unit",
                        help=("Repeat unit to be use for kendrick calibration. "
                              "A valid formula is expected."),
                        dest="repeat_unit",
                        default="C H2",
                        type=str)

    # tolerance to include/exclude duplicates
#    parser.add_argument("-t", "--tol",
#                        help="tolerance, in Da, to exclude duplicates.",
#                        metavar="tol",
#                        default=1e-5,
#                        type=float)

    return vars(parser.parse_args())


def main(**kwargs):
    """ run LC according to method """

    method = kwargs.pop("method")
    if method == "caliblist":
        kwargs.pop("repeat_unit")
        run_caliblist(**kwargs)
    if method == "kendrick":
        run_kendrick(**kwargs)


def run_kendrick(base_path, repeat_unit, cl_file, pks_dir, fgrid):
    """ attribute LC based on kendrick calibration """

    print("\nRUN TD CALIBRATION WITH: kendrick calibration")
    print("---------------------------------------------")

    # input parameters
    # ----------------
    base_path = Path(base_path)
    cl_file = Path(cl_file)
    fg_file = Path(fgrid)

    # last scan to consider
    scan_max = 400

    # keywords to look for kendrick series
    ks_kws = dict(
        lambda_parameter=2,
        use_SN=True,
        use_isotopes=False, building_block=repeat_unit,
        use_lowest_error=False,
        min_length_threshold=4,
        ks_kwargs=dict(min_length=6, limit_number=400, n_max_hole=1,)
    )

    # second walking recalibration
    min_npts_wrecal = 100
    seg_kws = dict(min_npts_per_segment=10, min_segment_size=15)
    wrecal_kws = dict(model="poly", deg=2, fit_intercept=False,)

    # molecule file
    mol_file = "mol_list.txt"

    att_kws = dict(algo="lowest_error", lambda_parameter=0.5,
                   use_isotopes=True,)

    # output parameters
    # -----------------
    folder_cal_files = base_path / "calibrated"
    img_folder = base_path / "img"
    if not img_folder.exists():
        img_folder.mkdir(exist_ok=True, parents=True)

    print("\nLoad formula grid:", fg_file)
    fgrids = FormulaGridList(fg_file)

    # load data
    # ---------
    print("Read pks files in", pks_dir)
    raw = TimeDependentData.from_directory(pks_dir, verbose=False)
    raw = raw[:scan_max]
    raw = raw.delimit(mz_bounds=(200, 800))
    print("Read calibration list in", cl_file)
    cl = read_caliblist(cl_file, polarity=fgrids[0].polarity)

    # calibration
    # -----------
    print("\nStart calibration")
    print("------------------")
    params_data = list()
    calibrated_pl = list()
    wrecalibrations = list()

    for iscan, pl in enumerate(raw):
        # build calibration list from kendrick based series
        # build the calibration list for the run

        kendrick_calibration = False
        try:
            apl, aks = kendrick_attribution(pl, fgrids[0], **ks_kws)
            kendrick_calibration = True
        except RuntimeError:
            print(f"No Kendrick series assigned for scan: {iscan:4d}.")

        if kendrick_calibration:
            m_err = np.array([ks.attributed_peaklist.error_in_ppm.mean() for ks in aks])
            idx, = outliers_detection_MAD(m_err, threshold=2)

            m_err = np.array([ks.attributed_peaklist.error_in_ppm.mean() for ks in aks])
            std_err = np.array([ks.attributed_peaklist.error_in_ppm.std() for ks in aks])

            fig = plt.figure()
            plt.errorbar(
                x=range(len(m_err)),
                y=m_err,
                ls="", marker="o", color="C0",
                yerr=std_err,
                label="selected series")
            plt.errorbar(
                x=idx,
                y=m_err[idx],
                ls="", marker="o", color="C1",
                yerr=std_err[idx],
                label="outliers")
            plt.xlabel("C O Series index")
            plt.ylabel("Mean error (ppm)")
            plt.savefig(img_folder / f"scan_{iscan}_ks.png")
            plt.close(fig)

            for i in idx[::-1]:
                aks.pop(i)

            this_cl = CalibList.from_attributed_peaklist(aks[0].attributed_peaklist)
            for ks in aks[1:]:
                this_cl.extend(ks.attributed_peaklist)
            this_cl.add_isotopes(pl, lambda_parameter=1.5, inplace=True)
            this_cl.filter_outliers(inplace=True)
            print(f"Kendrick for scan: {iscan:4d} len: {len(this_cl):4d}")
        else:
            this_cl = cl.search_mz_exp(pl, lambda_parameter=2)
            std_recalibration = False
            if len(this_cl) > 2:
                recalibrate = True
                this_cl.add_isotopes(pl, lambda_parameter=2, inplace=True)
                this_cl.filter_outliers(inplace=True)

        # proceed standard recal
        if kendrick_calibration or std_recalibration:
            recal = Recalibration(
                this_cl, model="ledford", verbose=True, fit_intercept=False
            )
            params_data.append(recal.optimized_parameters)
            r1_pl = recal.recalibrate(pl)
            r_cl = recal.get_recalibrated_calibration_list()

            fig = plt.figure()
            r_cl.plot.error_trend(fig=fig)
            plt.savefig(img_folder / f"recal_scan_{iscan}.png")
            plt.close(fig)
        else:
            r1_pl = pl
            r_cl = this_cl

        # go to walking calibration
        if len(r_cl) > min_npts_wrecal:
            # second walking recal
            segments = this_cl.get_walking_segments(**seg_kws)
            wrecal = WalkingCalibration(r_cl, segment_size=segments,
                                        **wrecal_kws)

            wr_pl = wrecal.recalibrate(r1_pl)
            calibrated_pl.append(wr_pl)
            wrecalibrations.append(wrecal)

            w_cl = wrecal.get_recalibrated_calibration_list()
            fig = plt.figure()
            w_cl.plot.error_trend(fig=fig)
            plt.savefig(img_folder / f"wrecal_scan_{iscan}.png")
            plt.close(fig)
        else:
            print("Not enough calibration points to perform the walking cal"
                  f" for scan: {iscan:4d} len: {len(this_cl):4d}")
            calibrated_pl.append(r1_pl)
            wrecalibrations.append(None)

    params_std_calib = pd.DataFrame(params_data)
    params_std_calib.index.name = "scan"
    print("Average values of calibration parameters:")
    print("A (m/z)^2 + B (m/z) + C")
    print(params_std_calib.describe().transpose()[["mean", "std", "min", "max"]])
    params_std_calib.to_csv(base_path / "standard_calib_params.csv")

    # export walking cal parameters
    wscans = list()
    mz_segments = np.empty((0,))
    w_params = np.empty((0, 2))
    for iscan, wrecal in enumerate(wrecalibrations):
        if wrecal is None:
            continue
        segment_centers = np.diff(wrecal.segments) / 2 + wrecal.segments[:-1]
        wscans.extend(len(segment_centers) * [iscan])
        mz_segments = np.concatenate([mz_segments, segment_centers])
        w_params = np.concatenate([w_params, wrecal.opt_params])

    wdata = pd.DataFrame({
        "scan": wscans,
        "mz_segments": mz_segments,
        "A": w_params[:, 0],
        "B": w_params[:, 1]})
    wdata.to_csv(base_path / "walking_cal_parameters.csv")

    # try to save memory
    del raw

    # get back the time dependent object
    cal_data = TimeDependentData(peaklists=calibrated_pl)

    if Path(mol_file).exists():
        print("\nExport EIC")
        with open(mol_file, "r") as f:
            mol_list = f.read().split("\n")
        mol_list = [Formula.from_string(f) for f in mol_list if len(f) > 0]

        # remove one hydrogen because polarity is -1
        for i, f in enumerate(mol_list):
            d = dict(f)
            d["H"] -= 1
            mol_list[i] = Formula(d)

        dataTIC = pd.DataFrame({
            "scan": range(len(cal_data)),
            "TIC": cal_data.get_total_ion_chromatogram(normalize="")
        })
        dataTIC.set_index("scan", inplace=True)

        eic_data = list()
        for f in mol_list:
            print(f"{str(f):>15s} {f.get_ion_exact_mass(-1):12.6f}")
            scans, mz, eic = cal_data.get_extracted_ion_chromatogram(
                formula=f,
                polarity=-1,
                lambda_parameter=0.2,
                normalize="",
            )
            df = pd.DataFrame({"scan": scans, f"mz_{str(f)}": mz,
                            f"EIC_{str(f)}": eic})
            df.set_index("scan", drop=True, inplace=True)
            eic_data.append(df)

        df = pd.concat([dataTIC] + eic_data, axis=1, join="outer")
        df.to_csv(base_path / "EIC_formulas.csv")

    print("\nCalibration done, export calibrated files")
    if not folder_cal_files.exists():
        folder_cal_files.mkdir(exist_ok=True, parents=True)
    else:
        print("Warning: Data will be overwritten")

    for pl in calibrated_pl:
        pl.to_csv(folder_cal_files / (pl.name[:-4] + ".csv"))

    # perform attribution
    print("\nAttribute")
    att_data = cal_data.attribute(
        formula_grid=fgrids,
        verbose=True,
        save_to_file=True,
        folder=base_path / "attributed_scans",
        **att_kws
    )


def run_caliblist(base_path, cl_file, pks_dir, fgrid):
    """ attribute LC based on standard calibration list calibration """
    # input parameters
    # ----------------
    # base_path = Path("RTl1_Wlad_F4_Done/")
    base_path = Path(base_path)
    # pks_dir = base_path / "pks"
    cl_file = Path(cl_file)
    # cl_file = base_path / "CalibrationList" / "full_caliblist.csv"
    fg_file = Path(fgrid)

    polarity = -1

    # last scan to consider
    scan_max = 273  # all

    # first standard recalibration
    # build the calibration list for the run
    td_recal_kws = dict(
        method="standard",
        lambda_parameter=2, add_isotopes=True, filter_outliers=True,
        recal_kws=dict(model="ledford", fit_intercept=False)
    )

    # second walking recalibration
    min_npts_wrecal = 50
    seg_kws = dict(min_npts_per_segment=10, min_segment_size=15)
    wrecal_kws = dict(model="poly", deg=2, fit_intercept=False,)

    # molecule file
    mol_file = "mol_list.txt"

    att_kws = dict(algo="lowest_error", lambda_parameter=0.5,
                   use_isotopes=True,)

    # output parameters
    # -----------------
    folder_cal_files = base_path / "calibrated"

    # load data
    # ---------
    print("Read pks files in", pks_dir)
    raw = TimeDependentData.from_directory(pks_dir, verbose=False)
    raw = raw[:scan_max]
    raw = raw.delimit(mz_bounds=(200, 800))

    # calibration
    # -----------
    print("Read calibration list in", cl_file)
    cl = read_caliblist(cl_file, polarity=polarity)

    print("\nStart calibration")
    print("------------------")
    recal_data, td_recal = raw.recalibrate(
        calibration_lists=cl,
        save_caliblist=True,
        folder=base_path / "caliblist",
        verbose=True,
        apply_closest_calibration=True,
        **td_recal_kws,
    )
    params_std_calib = pd.DataFrame(
        td_recal.get_opt_params(),
        columns=["A", "B"],
    )
    params_std_calib.index.name = "scan"
    print("Average values of calibration parameters:")
    print("A (m/z)^2 + B (m/z)")
    print(params_std_calib.describe().transpose()[["mean", "std", "min", "max"]])
    params_std_calib.to_csv(base_path / "standard_calib_params.csv")

    print("\nStart Walking calibration")
    print("--------------------------")
    calibrated_pl = list()
    wrecalibrations = list()
    for idx, this_cl in enumerate(td_recal.calibrated_caliblists):
        if this_cl is None:
            # the scan was not calibrated => should not happen if
            # apply_closest_calibration is True
            continue

        if len(this_cl) > min_npts_wrecal:
            # second walking recal
            segments = this_cl.get_walking_segments(**seg_kws)
            wrecal = WalkingCalibration(this_cl, segment_size=segments,
                                        **wrecal_kws)

            wr_pl = wrecal.recalibrate(recal_data[idx])
            calibrated_pl.append(wr_pl)
            wrecalibrations.append(wrecal)
        else:
            print("Not enough calibration points to perform the walking cal"
                  f" for scan: {idx:4d} len: {len(this_cl):4d}")
            calibrated_pl.append(recal_data[idx])
            wrecalibrations.append(None)

    # export walking cal parameters
    wscans = list()
    mz_segments = np.empty((0,))
    w_params = np.empty((0, 2))
    for iscan, wrecal in enumerate(wrecalibrations):
        if wrecal is None:
            continue
        segment_centers = np.diff(wrecal.segments) / 2 + wrecal.segments[:-1]
        wscans.extend(len(segment_centers) * [iscan])
        mz_segments = np.concatenate([mz_segments, segment_centers])
        w_params = np.concatenate([w_params, wrecal.opt_params])

    wdata = pd.DataFrame({
        "scan": wscans,
        "mz_segments": mz_segments,
        "A": w_params[:, 0],
        "B": w_params[:, 1]})
    wdata.to_csv(base_path / "walking_cal_parameters.csv")

    # try to save memory
    del raw
    del td_recal
    del recal_data

    # get back the time dependent object
    cal_data = TimeDependentData(peaklists=calibrated_pl)

    print("\nCalibration done, export calibrated files")
    if not folder_cal_files.exists():
        folder_cal_files.mkdir(exist_ok=True, parents=True)
    else:
        print("Warning: Data will be overwritten")

    for pl in calibrated_pl:
        pl.to_csv(folder_cal_files / (pl.name[:-4] + ".csv"))

    if Path(mol_file).exists():
        print("\nExport EIC")
        with open(mol_file, "r") as f:
            mol_list = f.read().split("\n")
        mol_list = [Formula.from_string(f) for f in mol_list if len(f) > 0]

        # remove one hydrogen because polarity is -1
        for i, f in enumerate(mol_list):
            d = dict(f)
            d["H"] -= 1
            mol_list[i] = Formula(d)

        dataTIC = pd.DataFrame({
            "scan": range(len(cal_data)),
            "TIC": cal_data.get_total_ion_chromatogram(normalize="")
        })
        dataTIC.set_index("scan", inplace=True)

        eic_data = list()
        for f in mol_list:
            print(f"{str(f):>15s} {f.get_ion_exact_mass(-1):12.6f}")
            scans, mz, eic = cal_data.get_extracted_ion_chromatogram(
                formula=f,
                polarity=-1,
                lambda_parameter=0.2,
                normalize="",
            )
            df = pd.DataFrame({"scan": scans, f"mz_{str(f)}": mz,
                            f"EIC_{str(f)}": eic})
            df.set_index("scan", drop=True, inplace=True)
            eic_data.append(df)

        df = pd.concat([dataTIC] + eic_data, axis=1, join="outer")
        df.to_csv(base_path / "EIC_formulas.csv")

    print("\nLoad formula grid:", fg_file)
    fgrids = FormulaGridList(fg_file)

    # perform attribution
    print("\nAttribute")
    att_data = cal_data.attribute(
        formula_grid=fgrids,
        verbose=True,
        save_to_file=True,
        folder=base_path / "attributed_scans",
        **att_kws
    )


if __name__ == "__main__":
    options = get_options()
    main(**options)
