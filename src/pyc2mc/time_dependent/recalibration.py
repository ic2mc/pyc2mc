# coding: utf-8

"""
The ``StandardCalibration`` class is designed to perform a recalibration
of a whole mass spectrum or a series of mass spectra, one by one. In
addition to the arguments of the ``Recalibration`` class, the ``StandardCalibration``
needs the peak list or the set of peak list on which the recalibration is
applied.

The `WalkingCalibration` class implements a recalibration splitting the
mass spectra in a set of segments. The class allows to perform the recalibration
on one spectra or a set of spectra. The walking calibration is a methodology to improve
the error distribution by dividing on N segments of ``segment_size`` the peak list.
Each segment is calibrated using the calibrants that are present on the m/z range
of the segment.

It may occur that there is no or not enough points in a given interval. In
that case a global calibration is performed on the whole mass spectra and
applied on these segments.

The walking calibration needs the additional following arguments:

* the width of the segments
* parameters for the global calibration
* possibility to extend the calibration list

To increase the number of calibrants, the calibration list can be expanded
by adding the isotopomers of ``13C1`` and ``13C2`` of the calibration points.
This is enable by ``expand_list= True``. In that case, a searching mode
must be define which precise how the isotope candidates are selected along
with the `lambda_parameter` which define the error width in ppm.
If ``closest`` is selected then if more than one m/z value accomplish the 
error width constrain, it will take the closest m/z value to that of the 
calibration_list. If ``most_abundant`` is selected then if more than one m/z
value accomplish the error width constrain, it will take the m/z value with
the highest abundance. It is ``most_abundant`` by default, because the 
calibration list is usually built from the most abundant peaks.
"""

from typing import Union, Sequence
from pathlib import Path
import warnings

import numpy as np

from pyc2mc.processing.recalibration import Recalibration, WalkingCalibration

from pyc2mc.utils import find_nearest
from pyc2mc.core.calibration import CalibList
from pyc2mc.core.peak import PeakList
from pyc2mc.plotinator.calibration_plotter import TimeDependentCalibrationPlotter


def _find_closest_scan(tokens):
    """ 
    Find the closest recalibrated scan to a non-calibrated scan using a token
    of booleans.
    """
    tokens = np.array(tokens)
    pos_true, = np.where(tokens)
    closest_true_scan_idx = list()
    for scan, tk in enumerate(tokens):
        if not tk:
            closest_true_scan_idx.append(find_nearest(pos_true, scan)[0])
    return closest_true_scan_idx


class TimeDependentCalibration:
    """
    This class contains all the methods to perform a time dependent 
    recalibration of a set of peaklists.
    """
    
    def __init__(
        self,
        td_data,  # "TimeDependentData",
        calibration_lists: Union[CalibList, Sequence[CalibList]],
        method: str = "standard",
        lambda_parameter: float = 10.0,
        recal_kws: dict = None,
        searching_mode: str = 'most_abundant',
        use_SN: bool = True,
        filter_outliers: bool = True,
        add_isotopes: bool = True,
        apply_closest_calibration: bool = True,
        verbose: bool = False,
        segments: Union[Sequence[int], None] = None,
    ):
        """
        Instance method of the Standard Calibration class.
        
        Args:
            td_data (TimeDependentData): A TimeDependantData object
            calibration_lists (list or pyc2mc.io.CalibList): the calibration
                list to be used to apply the calibration or a list of
                calibration list. If a list is provided, you have either to
                provide a calibration list for each scan contained in the
                time dependant data, or, you have to define in ``segments``
                the scans boundaries for each calibration list.
            method (str): ``"standard"`` or ``"walking"`` to perform a standard
                recalibration or a walking calibration on each scan.
            lambda_parameter (float64): range in ppm for searching exp m/z values
                based on the calibration_list.
            recal_kws (dict): Arguments of the recalibration as a dict. This
                includes the model, fit_intercept, weights ...
            searching_mode (string): Defines the method's criteria to look for
                the experimental m/z values from the calibration list on each
                scan. Possible values are `"closest"`, ``"closest_corrected"``
                ``"most_abundanta"`` or ``"most_abundant_corrected"``. Look at
                the module ``pyc2mc.core.peak_search``.
            use_SN (bool): If True, default, use S/N else, use intensity when
                abundance of peak is considered for peak search.
            filter_outliers (bool): Apply a filtering of outlier to the
                calibration list using TSE.
            add_isotopes (bool): Extend the calibration list by looking for
                13C isotopes.
            apply_closest_calibration (bool): If true (default), if one scan
                is not calibrated, the calibration of the closest calibration
                scan is applied. If False, m/z values are kept uncalibrated.
            verbose (bool): In case of absence of regression displays a warning
                message.
            segments (list of int): Provide the monotonically increasing
                segment edges including the rightmost scan index. One 
                calibration list is used for each segment.
                Not used if only one calibration list is 
                provided or if the number of provided calibration lists 
                is the number of scans.
                If provided, the length of the list is 
                ``len(calibration_lists) + 1``. This could be used to define
                non uniform segments and to restrain the calibration to a
                subset of scans.

        """

        # TD data
        self._td_data = td_data

        # calibration lists
        self._calibration_lists = list()
        self._raw_calibration_lists = list()
        self._segments = segments
        self._calib_idx = list()
        self._setup_caliblists(calibration_lists)

        # check polarity for isotopes
        if add_isotopes:
            if not all([cl.polarity is not None 
                        for cl in self._calibration_lists]):
                raise ValueError(
                    "The polarity of the calibration list is not defined but "
                    "you ask for adding isotopes. You must define the "
                    "polarity in the calibration list."
                )
        
        # calibration method
        _method = method.strip().lower()
        if _method == "standard" or method == "s":
            self._recalibration_method = Recalibration
        elif _method == "walking" or method == "w":
            self._recalibration_method = WalkingCalibration
        else:
            raise ValueError(
                "method must be 'standard' or 'walking' but you set: "
                f"'{method}'")

        # parameters
        self._verbose = verbose
        self._lambda_parameter = lambda_parameter
        self._searching_mode = searching_mode
        self._use_SN = use_SN
        self._filter_outliers = filter_outliers
        self._add_isotopes = add_isotopes
        self._apply_closest_calibration = apply_closest_calibration

        default_recal_kws = dict(fit_intercept=True, use_weights=False,
                                 model="polynomial", deg=2, verbose=verbose)
        self._recal_kws = default_recal_kws.copy()
        self._recal_kws.update(recal_kws if recal_kws is not None else {})

        # storage lists
        self._is_recalibrated = list()
        self._calibrated_peaklists = list()
        self._recalibrations = list()

        # run calibration
        self._run()

        # plot
        self.plot = TimeDependentCalibrationPlotter(self)

    def _setup_caliblists(self, calibration_lists):
        """ Set up and check the calibration lists """

        # calibration list
        # When time-dependant data is going to be calibrated, the method
        # must search the experimental mz for each scann. Here the calibration
        # list is cleaned up keeping only based information.
        if isinstance(calibration_lists, list):
            for cl in calibration_lists:
                if isinstance(cl, CalibList):
                    self._calibration_lists.append(cl)
                    self._raw_calibration_lists.append(cl.get_raw_caliblist())
                else:
                    raise TypeError(
                        'the calibration_list argument must be a '
                        'pyc2mc.core.calibration.CalibList class. But'
                        f' it is of type {type(cl)}')

            # check lengths and segments:
            if len(calibration_lists) == len(self._td_data):
                # a calibration list is provided for each scan
                self._segments = list(range(len(self._td_data)))
            elif len(calibration_lists) == 1:
                self._segments = [0, len(self._td_data) - 1]
            # The provided segments' bounds will be used from that point
            elif self._segments is None:
                raise ValueError(
                    "Segments must be provided but segments "
                    "is 'None'")
            elif len(calibration_lists) == len(self._segments):
                # a calibration list is provided for each scan
                # segments must be consecutive number.
                if not np.all(np.diff(self._segments) == 1):
                    raise ValueError(
                        "The number of segments and calibration lists are "
                        "not consistent. As they are the same, it is "
                        "assumed that a calibration list is provided for "
                        "each scan and segments must be consecutive numbers."
                        f"\nsegments: '{self._segments}'."
                    )
                pass
            elif len(calibration_lists) + 1 == len(self._segments):
                # a calibration list is provided for each interval
                pass
            else:
                raise ValueError(
                    "The length of segments is not consistent with "
                    "the number of provided calibration lists. "
                    f"{len(self._segments) - 1} calibration lists "
                    "are excpected."
                    f"\nsegments: {self._segments}"
                    f"\n# calib list: {len(calibration_lists)}"
                )
            
            # check valid segments values
            if not np.all(np.diff(self._segments) > 0):
                raise ValueError(
                    "Scan boundaries are not in ascending order:\n"
                    f"segments: {self._segments}")
            if self._segments[-1] > len(self._td_data):
                raise ValueError(
                    "Segments definition out of scan bounds:"
                    f"\n# scan:{len(self._td_data)}"
                    f"\nlast segment:{self._segments[-1]}")
            if self._segments[0] < 0:
                raise ValueError(
                    "The index of the first scan cannot be negative."
                    f"\nfirst scan:{self._segments[0]}")

        elif isinstance(calibration_lists, CalibList):
            self._calibration_lists = [calibration_lists]
            self._raw_calibration_lists = [calibration_lists.get_raw_caliblist()]
            self._segments = [0, len(self._td_data) - 1]
        
        else:
            raise TypeError(
                'the calibration_list argument must be a '
                'pyc2mc.core.calibration.CalibList class or a list. But'
                f" it is of type '{type(calibration_lists)}'.")
        
        # make a list to get back the index of the needed caliblist
        if len(self._calibration_lists) == len(self._td_data):
            self._calib_idx = list(range(len(self._td_data)))
        elif len(self._calibration_lists) == len(self._segments):
            self._calib_idx = list(range(len(self._calibration_lists)))
        else:
            self._calib_idx = list()
            deltas = np.diff(self._segments)
            for i, delta in enumerate(deltas):
                self._calib_idx.extend([i] * delta)
            self._calib_idx.append(self._calib_idx[-1])

    @property
    def td_data(self):
        """ The time dependent data before calibration """
        return self._td_data

    @property
    def calibrated_peaklists(self):
        """ List of recalibrated peak list """
        return self._calibrated_peaklists
    
    @property
    def calibrated_caliblists(self):
        """ List of recalibrated calibration list """
        return [recal.get_recalibrated_calibration_list()
                if recal is not None else None
                for recal in self._recalibrations]

    @property
    def recalibrations(self):
        """ List of Recalibration object """
        return self._recalibrations

    def get_opt_params(self):
        """ Return an array of the recalibration parameter for each scan. """
        if self._recalibration_method.__name__ == "Recalibration":
            return np.array([recal.opt_param_array for recal in self.recalibrations])
        else:
            return [recal.opt_params for recal in self.recalibrations]

    def get_recalibrated_td_data(self):
        """ Return a new TimeDependentData object with the calibrated 
        peaklists. 
        """
        # -- Circular import issue -- Germain => I put the import of the 
        # class in the function to avoid circular import error as td_data
        # import TimeDependentCalibration and recalibration import 
        # TimeDependantData.
        # TODO: another option could be an "update_peaklists" method
        # in the TimeDependentData class
        from pyc2mc.time_dependent.td_data import TimeDependentData
        name = self._td_data.name
        name = name if "recalibrated" in name else "recalibrated_" + name 
        
        # apply segments restrictions
        if self._segments[0] != 0:
            t0 = self._td_data.t0 + self._segments[0] * self._td_data.dt
        else:
            t0 = self._td_data.t0
        scan_ids = self._td_data.scan_ids[self._segments[0]: self._segments[-1] + 1]

        return TimeDependentData(
            peaklists=self.calibrated_peaklists,
            name=name,
            scan_ids=scan_ids,
            dt=self._td_data.dt, t0=t0  
        )

    def _run(self):
        """ Run the recalibration of all the scans """

        self._is_recalibrated = list()
        self._calibrated_peaklists = list()
        self._recalibrations = list()

        # iterate to perform a calibration for each peak list
        for i_scan, peaklist in enumerate(self._td_data):

            # select calibration list according to segments definitions
            if i_scan < self._segments[0]:
                # skipped scan from segments definition
                continue
            if i_scan > self._segments[-1]:
                # skip the rest of the scan from segments definition
                break

            # set up the peak list for the current scan
            # search candidates in the peak list
            idx = self._calib_idx[i_scan - self._segments[0]]
            cl = self._raw_calibration_lists[idx].search_mz_exp(
                peaklist=peaklist,
                lambda_parameter=self._lambda_parameter,
                searching_mode=self._searching_mode,
                use_SN=self._use_SN,
            )

            if len(cl) == 0:
                # calibration list is empty => no recalibration
                self._calibrated_peaklists.append(peaklist)
                self._recalibrations.append(None)
                self._is_recalibrated.append(False)
                continue
            
            # add isotopes
            if self._add_isotopes:
                cl = cl.add_isotopes(peaklist, use_SN=self._use_SN)

            if self._filter_outliers:
                cl.filter_outliers()

            if self._verbose:
                title = (f"Scan: {i_scan} ({len(peaklist)} peaks)"
                         f"  {len(cl)} calib. points")
                print("\n" + "=" * 80)
                print(title.center(80))
                print("=" * 80 + "\n")

            try:
                recal = self._recalibration_method(
                    calibration_list=cl,
                    **self._recal_kws,
                )
            except (ValueError, RuntimeError) as error:
                if self._verbose:
                    warnings.warn("\n" + str(error), RuntimeWarning, stacklevel=2)
                self._calibrated_peaklists.append(peaklist)
                self._recalibrations.append(None)
                self._is_recalibrated.append(False)
                continue

            self._calibrated_peaklists.append(recal.recalibrate(peaklist))
            self._recalibrations.append(recal)
            self._is_recalibrated.append(True)

        # check how many scans were calibrated
        n_calibrated = np.count_nonzero(self._is_recalibrated)
        if n_calibrated == 0:
            raise RuntimeError(
                "No scans were calibrated. Check your calibration list.")
        if self._verbose:
            print("\nCalibration done.")
            print(f"Number of calibrated scans: {n_calibrated}")

        # try to calibrate non calibrated scans
        if self._apply_closest_calibration and not all(self._is_recalibrated):
            # all scans were not calibrated, the calibration parameters of
            # the closest scans will be applied.

            if self._verbose:
                print("\nTry to calibrate scans with the closest already calibrated.")

            # cti: closest token index
            cti = _find_closest_scan(self._is_recalibrated)
            count = 0
            for i_scan, calibrated in enumerate(self._is_recalibrated):
                if not calibrated:
                    self._recalibrations[i_scan] = self._recalibrations[cti[count]]
                    rpl = self._recalibrations[i_scan].recalibrate(
                            self._calibrated_peaklists[i_scan])
                    self._calibrated_peaklists[i_scan] = rpl
                    if self._verbose:
                        print(f"Recalibrate scan {i_scan} with scan {cti[count]}")
                    count += 1

        if self._verbose:
            self.summary()

    def summary(self):
        """ Prints a summary with regression information """
        print(self._txt_msg())

    def _txt_msg(self):
        """ Returns a text message with the summary """
        line = '\n' + 70 * "=" + "\n"
        line += 'TIME DEPENDENT CALIBRATION SUMMARY'.center(70) + "\n"
        line += f"Regression model    : {self._recal_kws['model']}\n"
        line += f"# scans             : {len(self._td_data)}\n"
        line += f"# recalibrated scans: {len(np.nonzero(self._is_recalibrated)[0])}"

        line += f"\n\nscan #pts {'RMSE ppm':>12s}   is recal\n"
        for s_idx, recal in enumerate(self._recalibrations):
            if recal is None:
                line += f"{s_idx:4d} {'nan':>4s} {'nan':>12s} {'nan':>10s}\n"
            else:
                line += (f"{s_idx:4d} {recal.n_data_points:4d}"
                         f" {recal.RMSE_ppm:12.6f}"
                         f" {str(self._is_recalibrated[s_idx]):>10s}\n")

        line += 70 * "=" + "\n"
        return line

    def get_diff_ppm(self):
        """
        Calculate the differences in ppm between the recalibrated PeakList 
        or the PeakListCollection object and the raw_data.

        Returns:
            A dictionary containing the mass errors for each PeakList.
        """
        diff = dict()
        if isinstance(self.calibrated_MS, PeakList):
            diff['PeakList'] =\
                ((self._raw_data[0].mz - self.calibrated_MS.mz)
                 / self._raw_data[0].mz * 1e6)
        else:
            for i, pl in enumerate(self._raw_data, 1):
                diff[i] = ((pl.mz - self.calibrated_MS[i - 1].mz)
                           / pl.mz * 1e6)
        return diff
    
    def to_csv(self, basename: str = "peaklist", folder='recalibrated'):
        """
        Write a recalibrated peak list file in csv format for each scan
        in the given folder.
        
        Args:
            basename (str): basename for the peaklist files, the index of the
                scan is added to the basename. Default ``'peaklist'``
            folder (string): folder in which peaklist are exported.
                Default ``'recalibrated'``
        """
        folder = Path(folder)
        if not folder.exists():
            folder.mkdir(exist_ok=True, parents=True)

        for idx, pl in enumerate(self.calibrated_peaklists):
            filename = folder / f"{basename}_{idx:04d}.csv"
            pl.to_csv(filename)

    def __repr__(self):
        return repr('2D Recalibration.')
