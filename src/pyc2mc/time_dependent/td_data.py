#!/usr/bin/env python
# coding: utf-8

"""
This module provides input/output tools for experimental data.
"""

import re
from time import time
from pathlib import Path
from typing import Sequence, Union

import numpy as np
from numpy.typing import ArrayLike
import pandas as pd

from pyc2mc.core.nddata import PeakListCollection
from pyc2mc.core.formula_grid import FormulaGrid
from pyc2mc.core.formula import Formula, get_formula
from pyc2mc.core.peak import PeakList, AttributedPeakList
from pyc2mc.plotinator.td_data_plotter import TDDataPlotter, AttributedTDDataPlotter
from pyc2mc.utils import smoothing_method, get_file_index
from pyc2mc.processing.attribution import AttributionAlgorithm
from pyc2mc.time_dependent.recalibration import TimeDependentCalibration

__all__ = ["TimeDependentData", "AttributedTimeDependentData"]


class TimeDependentData(PeakListCollection):
    """ This class represents the data obtained from an time dependent
    Mass Spectrometry technique such as LC/MS or GC/MS techniques. It
    basically consists in a list of ``pyc2mc.core.peak.PeakList`` objects
    and provides methods to manage or filter the results.

    Attributes:
        t0 (float): Retention time of the first scan
        dt (flaot): Time interval between two scans

    """

    def __init__(
        self,
        peaklists: Sequence[PeakList],
        name: str = None,
        dt: float = 1.,
        t0: float = 0.,
        scan_ids: Sequence[int] = None
    ):
        """ Instantiate a TimeDependentData object

        Args:
            peaklists (list of peaklist): List of PeakList objects.
            dt (float): time step between MS scan.
            t0 (float): time of the first scan.
            name (str): The file name or the sample name.
            scans_ids (list-like): list or array of indexes of scans if available,
                else, scans_ids is generated from 0 to N.
        """
        super().__init__(peaklists=peaklists, name=name, scan_ids=scan_ids)

        self.dt = dt
        self.t0 = t0

        # set up plotter
        self.plot = TDDataPlotter(self)

    def get_retention_times(self, dt: float = None, t0: float = None,
                            use_scan_ids: bool = False) -> ArrayLike:
        """ Return an array of the retention times of each scans. The unit
        depends on the unit of dt and t0. If dt and t0 are not provided,
        The one defined in the class instance are used.

        The retention time is computed from the scans index. It means that
        dt is the time interval between two scans. If you prefer to use
        scan_ids you have to set ``use_scan_ids=True``. In that case, dt is 
        the time interval between two scans ids and t0 is the time of the 
        first scan id.

        Args:
            dt (float): time step between MS scan.
            t0 (float): time of the first scan.
            use_scan_ids (bool): If True dt is the time interval between scan
                ids and t0 is the time of the first scan id.

        Returns:
            An array of retention times.
        
        """
        dt = dt if dt is not None else self.dt
        t0 = t0 if t0 is not None else self.t0

        if use_scan_ids:
            return t0 + dt * (self._scan_ids - self._scan_ids[0])
        else:
            return t0 + dt * np.arange(len(self._peaklists))
    
    def data_summary(self, dt: float = None, t0: float = None, 
                     normalize: str = "max", use_scan_ids: bool = False
                     ) -> pd.DataFrame:
        """ Produce a data frame with general information on each scan
        as a function of time. 
        
        Args:
            dt (float): time step between MS scan.
            t0 (float): time of the first scan.
            normalize (str): if ``'max'``, sets the maximum intensity of the TIC
                to 100. If ``'integral'``, divides the TIC by its integral.
            use_scan_ids (bool): If True dt is the time interval between scan
                ids and t0 is the time of the first scan id.

        Returns:
            A data frame with general information about scans.
        """
        bpc, mz_bpc = self.get_base_peak_chromatogram(
            normalize=normalize, return_mz=True)

        summary = {
            'scan': self._scan_ids,
            'retentionTime': self.get_retention_times(dt, t0, use_scan_ids),
            'mz_bounds': self.get_scans_mz_bounds(),
            'N_peaks': self.scan_lengths,
            'TIC': self.get_total_ion_chromatogram(normalize=normalize),
            "BPC":  bpc,
            "mz_max_I": mz_bpc,
        }
        return pd.DataFrame(summary).set_index("scan", drop=True)

    def get_total_ion_chromatogram(
        self,
        normalize: str = "max",
        smoothing: bool = False,
        smooth_kws: dict = None,
    ) -> ArrayLike:
        """ This function compute the Total Ion Chromatogram (TIC) by summing
        intensities on each scan. 

        Args:
            normalize (str): if ``'max'``, sets the maximum intensity of
                the TIC to 100. If ``'integral'``, divides the TIC by
                its integral.
            smoothing (bool): if True a smoothing TIC is returned
            smooth_kws (dict): smoothing options.

        Returns:
            A numpy array of the TIC.
        
        """
        tic = np.nansum(self._intensity, axis=1)
        
        if smoothing:
            s_params = dict(N=6, Wn=0.2)
            if smooth_kws is not None:
                s_params.update(**smooth_kws)
            tic = smoothing_method(signal=tic, **s_params)

        if normalize == "max":
            max_tic = np.nanmax(tic)
            tic = tic / max_tic * 100
        elif normalize == "integral":
            norm = np.trapz(tic)
            tic /= norm

        return tic
    
    def delimit(
        self,
        mz_bounds: tuple[float] = None,
        intensity_bounds: Union[tuple[float], float] = None,
        SN_bounds: Union[tuple[float], float] = None
    ):
        """
        Delimits all the scans according to the mz values, intensity and S/N
        and returns a new object. Filtering is done first on mz values,
        then on intensity and finally on S/N.

        Args:
            mz_bounds (tuple): a tuple of min and max mz values.
            intensity_bounds (tuple, float): a tuple of min and max intensity
                values. If only one value
                is given, intensity lower than this value are ignore.
            mz_bounds (tuple): a tuple of min and max SN values. If only 
                one value is given, SN lower than this value are ignore.

        Returns:
            The delimited data.
        """
        peaklists = [pl.delimit(mz_bounds, intensity_bounds, SN_bounds)
                     for pl in self._peaklists]

        name = self.name if "delimited" in self.name else "delimited_" + self.name
        return self.__class__(
            peaklists=peaklists, name=name, scan_ids=self.scan_ids,
            dt=self.dt, t0=self.t0,
        )

    def filter_intensity(self, threshold: float):
        """
        Filter data, by removing for each scan, the peaks with an intensity
        lower than the threshold.

        Args:
            threshold (float): remove peaks with an intensity lower than
            the threshold.

        Returns:
            A TimeDependentData object derived from the original one,
            limited to peak with intensity higer than threshold.
        """
        intens_max = self._intensity.max()
        peaklists = [peaklist.delimit(intensity_bounds=(threshold, intens_max + 1))
                     for peaklist in self._peaklists]

        name = self.name if "filter_Intens_" in self.name else "delimited_" + self.name
        return self.__class__(
            peaklists=peaklists, name=name, scan_ids=self.scan_ids,
            dt=self.dt, t0=self.t0,
        )

    def get_base_peak_chromatogram(
        self,
        normalize: str = "max",
        smoothing: bool = False,
        smooth_kws: dict = None, 
        return_mz: bool = False
    ) -> ArrayLike:
        """ This function compute the Base Peak Chromatogram (BPC) and return
        the maximum intensity of each scan.

        Args:
            normalize (str): if ``'max'``, sets the maximum intensity of the TIC
                to 100. If ``'integral'``, divides the TIC by its integral.
            smoothing (bool): if True a smoothing TIC is returned
            smooth_kws (dict): smoothing options.
            return_mz (bool): if True, returns both BPC and corresponding mz
                values.
        
        Retunrs:
            A numpy array of the BPC and mz values if return_mz is True.            
        """
        argmax = np.expand_dims(np.nanargmax(self._intensity, axis=1), axis=1)
        bpc = np.take_along_axis(self._intensity, argmax, axis=1).squeeze()

        if smoothing:
            s_params = dict(N=6, Wn=0.2)
            if smooth_kws is not None:
                s_params.update(**smooth_kws)
            bpc = smoothing_method(signal=bpc, **s_params)

        if normalize == "max":
            max_bpc = np.nanmax(bpc)
            bpc = bpc / max_bpc * 100
        elif normalize == "integral":
            norm = np.trapz(bpc)
            bpc /= norm

        if return_mz:
            mz_values = np.take_along_axis(self._mz, argmax, axis=1).squeeze()
            return bpc, mz_values
        else:
            return bpc
    
    def get_extracted_ion_chromatogram(
        self,
        mz_target: float = None,
        formula: Union[str, Formula] = None,
        lambda_parameter: float = 1,
        normalize: str = "max",
        polarity: int = None,
    ) -> ArrayLike:
        """ Get the extract ion chromatogram (EIC) of the given m/z value
        or molecular formula according to the relative error defined by 
        ``lambda_parameter``. If both m/z value and formula are provided,
        the m/z value is considered.
        
        Args:
            mz_target (float): The m/z value for which you want the EIC.
            formula (str or Formula): The molecular formula for which you want
                to get the EIC. In that case polarity must be provided to 
                compute the ion excat mass.
            lambda_parameter (float): relative error in ppm to get the EIC
            normalize (str): if ``'max'``, sets the maximum intensity of the TIC
                to 100. If ``'integral'``, divides the TIC by its integral.
            polarity (int): charge of the molecular formula.

        Returns
            The EIC as a numpy array.
        """

        if mz_target is not None:
            eic_mean = mz_target
        elif formula is not None:
            if polarity is None:
                raise ValueError(
                    "When providing the molecular formula you must define the"
                    " polarity in order to compute the ion exact mass.")
            eic_mean = get_formula(formula).get_ion_exact_mass(polarity)
        else:
            raise ValueError("Either formula or mz_target must be provided")

        mz_eic = list()
        intens_eic = list()
        scans = list()
        for idx_scan, mz_scan in enumerate(self._mz):
            # look for m/z and select closest one
            idx = np.searchsorted(mz_scan, eic_mean)
            if idx == len(mz_scan):
                # case eic_mean is out of the mz range
                idx -= 1
            selected_mz = mz_scan[[idx - 1, idx]]
            error = np.abs(selected_mz - eic_mean) / eic_mean * 1e6
            jdx = np.nanargmin(error)
            idx = idx - 1 if jdx == 0 else idx

            # check if it lies in the interval
            if error[jdx] < lambda_parameter:
                mz_eic.append(mz_scan[idx])
                intens_eic.append(self._intensity[idx_scan, idx])
                eic_mean = np.mean(mz_eic)
                scans.append(idx_scan)

        if len(mz_eic) > 0:
            # ion was found
            # convert to numpy arrays
            mz_eic = np.array(mz_eic)
            intens_eic = np.array(intens_eic)
            scans = np.array(scans)

            # normalize
            if normalize == "max":
                max_eic = np.nanmax(intens_eic)
                intens_eic = intens_eic / max_eic * 100
            elif normalize == "integral":
                norm = np.trapz(intens_eic, x=mz_eic)
                intens_eic /= norm

        return scans, mz_eic, intens_eic

    def attribute(
        self,
        formula_grid: Union[list, FormulaGrid],
        algo: Union[AttributionAlgorithm, str] = AttributionAlgorithm.LOWEST_ERROR,
        save_to_file: bool = False,
        basename: str = "attributed_scan",
        folder: str = "attributed_scans",
        verbose: bool = False,
        **kwargs,
    ):
        """ Attribution of all scans of the time dependant data. All
        scans are attributed independently according to the selected
        algorithm.
        
        TODO: Split the loop over scans into threads to be more efficient
        TODO: Combine with roi detection to speed up attribution

        Args:
            formula_grid (list or FormulaGrid): List of formula grid to be
                used for the attribution.
            algo (AttributionAlgorithm): Select the attribution algorithm
            save_to_file (bool): If True, recalibrated caliblist are saved
                in csv files.
            basename (str): base name for csv files.
            folder (str): folder in which caliblist are saved
            verbose (bool): If True, print message after each scan
                attribution, default False.
            kwargs keywords parameters of the selected algorithm.

        Returns:
            An AttributedTimeDependentData object containing all 
            attributed scans.

        """
        if isinstance(algo, str):
            algo = AttributionAlgorithm(algo)

        if save_to_file:
            folder = Path(folder)
            if not folder.exists():
                folder.mkdir(exist_ok=True, parents=True)

        if isinstance(formula_grid, FormulaGrid):
            formula_grid = [formula_grid]

        # perform the attribution for all scans
        attributed_peaklists = list()
        total_time = 0
        for idx, peaklist in enumerate(self._peaklists):
            elapsed_time = time()
            if verbose:
                print(f"Scan {idx} / {len(self._peaklists)}")

            # first attribution
            att_pl = algo(peaklist=peaklist, formula_grid=formula_grid[0], **kwargs)
            # iterative attribution
            for fg in formula_grid[1:]:
                att_pl = algo(peaklist=att_pl, formula_grid=fg, **kwargs)
            
            elapsed_time = time() - elapsed_time
            total_time += elapsed_time

            if verbose:
                print(f"Attribution done in {elapsed_time:.1f} s")
                att_pl.summary()

            if save_to_file:
                elapsed_time = time()
                # save attributed to csv
                filename = folder / f"{basename}_{idx:04d}.csv"
                att_pl.to_csv(filename, full_data=True)
                
                elapsed_time = time() - elapsed_time
                total_time += elapsed_time
                if verbose:
                    print(f"    save file {filename} in {elapsed_time:.1f} s")

            attributed_peaklists.append(att_pl)

        if verbose:
            print(f"\nAttribution done in {total_time:.1f} s")

        return AttributedTimeDependentData(
            peaklists=attributed_peaklists, dt=self.dt, t0=self.t0,
            scan_ids=self.scan_ids
        )

    def recalibrate(
        self,
        save_caliblist: bool = False,
        basename: str = "caliblist",
        folder: str = "caliblist",
        verbose: bool = False,
        **kwargs
    ):
        """ This method is a wrapper over the
        ``pyc2mc.time_dependent.TimeDependentCalibration`` class and will
        perform a recalibration of the whole time dependent data.

        Args:
            save_caliblist (bool): If True, recalibrated caliblist are saved
                in csv files.
            basename (str): base name for csv files.
            folder (str): folder in which caliblist are saved
            verbose (bool): If True, print summary after each scan
                recalibration, default False.
            kwargs as argument of ``pyc2mc.time_dependent.TimeDependentCalibration``.
        
        Returns:
            TimeDependentData: The calibrated time dependent data
            TimeDependentCalibration: The time dependent recalibration object.

        """
        elapsed_time = time()
        td_recal = TimeDependentCalibration(self, verbose=verbose, **kwargs)
        elapsed_time = time() - elapsed_time
        if verbose:
            print(f"Recalibration done in {elapsed_time:.1f} s.")

        if save_caliblist:
            elapsed_time = time()
            folder = Path(folder)
            if not folder.exists():
                folder.mkdir(exist_ok=True, parents=True)

            for idx, caliblist in enumerate(td_recal.calibrated_caliblists):
                if caliblist is None:
                    continue
                filename = folder / f"{basename}_{idx:04d}.csv"
                caliblist.to_csv(filename)
            if verbose:
                elapsed_time = time() - elapsed_time
                print(f"Save files in {elapsed_time:.1f} s.")

        name = "recalibrated_" + self.name.removeprefix("recalibrated_")
        # apply segments restrictions
        if td_recal._segments[0] != 0:
            t0 = self.t0 + td_recal._segments[0] * self.dt
        else:
            t0 = self.t0
        scan_ids = self.scan_ids[td_recal._segments[0]: td_recal._segments[-1] + 1]

        td_data = TimeDependentData(
            peaklists=td_recal.calibrated_peaklists,
            dt=self.dt, t0=t0,
            scan_ids=scan_ids,
            name=name
        )

        return td_data, td_recal

    def __getitem__(self, item: Union[int, slice, ArrayLike]):
        """ Return the required scan if item is an integer or a new
        class object if item is a slice object corresponding to the
        provided slice. """
        if isinstance(item, slice):
            start = 0 if item.start is None else item.start
            return TimeDependentData(
                peaklists=self._peaklists[item],
                scan_ids=self.scan_ids[item],
                dt=self.dt, t0=self.t0 + start * self.dt,
                name=self.name)
        elif isinstance(item, (int, np.integer)):
            return self._peaklists[item]
        else:
            scans = [self._peaklists[idx] for idx in item]
            return TimeDependentData(
                peaklists=scans, scan_ids=item,
                dt=self.dt, t0=self.t0 + item[0] * self.dt,
                name=self.name,
            )

    def __repr__(self):
        rep = super().__repr__()
        rep = rep.split("\n")
        rep[0] = f"TimeDependentData ({len(self._peaklists)} scans)"
        return "\n".join(rep)


class AttributedTimeDependentData(TimeDependentData):
    """ This class represents the attributed data gathered after the
    molecular attribution of scans obtained from a time dependent
    Mass Spectrometry technique such as LC/MS or GC/MS techniques. It
    basically consists in a list of ``pyc2mc.core.peak.AttributedPeakList``
    objects and provides methods to manage or filter the results.

    Attributes:
        t0 (float): Retention time of the first scan
        dt (flaot): Time interval between two scans

    """

    def __init__(
        self,
        peaklists: Sequence[AttributedPeakList] = None,
        name: str = None,
        dt: float = 1.,
        t0: float = 0.,
        scan_ids: Sequence[int] = None,
        gather_data: bool = True,
    ):
        """ Instantiate an AttributedTimeDependentData object

        Args:
            peaklists (list of peaklist): List of AttributedPeakList objects.
            dt (float): time step between MS scan.
            t0 (float): time of the first scan.
            name (str): The file name or the sample name.
            scans_ids (list-like): list or array of indexes of scans if available,
                else, scans_ids is generated from 0 to N.
            plot_data (bool): If True, data are gathered over all scans to
                be able to produces plots.
        """
        super().__init__(peaklists=peaklists,
                         name=name, scan_ids=scan_ids)

        self.dt = dt
        self.t0 = t0

        # initialize data    
        self._chem_classes = list()
        self._chem_groups = list()
        self._chem_groups_mono = list()
        self._chem_classes_mono = list()
        self._DBE_values = list()
        self._chem_group_classes_data = list()
        self._chem_group_classes_sum = None
        self._elemental_composition = None
        self._mean_error_ppm = list()
        self._std_error_ppm = list()

        self._gather_data = gather_data
        if gather_data:
            self.set_td_data()
    
        # set up plotter
        self.plot = AttributedTDDataPlotter(self)

    def set_td_data(self):
        """ Fill in arrays for plots or data management. """
        self._gather_data = True

        # set up arrays for filtering or gathering data
        for peaklist in self._peaklists:
            chem_classes = list()
            chem_groups = list()
            chem_groups_mono = list()
            chem_classes_mono = list()
            DBE_values = list()

            if self._elemental_composition is None:
                self._elemental_composition = set(peaklist.elemental_composition)
            else:
                self._elemental_composition.update(peaklist.elemental_composition)

            # sum up over chemical group / classes
            class_group_data = peaklist.get_classes(
                monoisotopic=True).set_index(["Chem. Group", "Chem. Class"])
            self._chem_group_classes_data.append(class_group_data.reset_index())
            if self._chem_group_classes_sum is None:
                self._chem_group_classes_sum = class_group_data.loc[:, "Intensity"].copy()
            else:
                self._chem_group_classes_sum = self._chem_group_classes_sum.add(
                    class_group_data.loc[:, "Intensity"], fill_value=0)

            for peak in peaklist:
                if peak.is_attributed:
                    chem_classes.append(peak.formula.chem_class)
                    chem_classes_mono.append(peak.formula.chem_class_mono)
                    chem_groups.append(peak.formula.chem_group)
                    chem_groups_mono.append(peak.formula.chem_group_mono)
                    DBE_values.append(peak.formula.dbe_value)
                else:
                    chem_classes.append("")
                    chem_classes_mono.append("")
                    chem_groups.append("")
                    chem_groups_mono.append("")
                    DBE_values.append(np.nan)

            self._chem_classes.append(np.array(chem_classes))
            self._chem_classes_mono.append(np.array(chem_classes_mono))
            self._chem_groups.append(np.array(chem_groups))
            self._chem_groups_mono.append(np.array(chem_groups_mono))
            self._DBE_values.append(np.round(DBE_values, 1))  # round for searching

        self._chem_group_classes_sum = self._chem_group_classes_sum.reset_index()

        # compute weighted average of the error in ppm
        stat_data = self._get_stat_average(
            [att_pl.error_in_ppm for att_pl in self._peaklists],
            q=[]
        )
        self._mean_error_ppm = stat_data[:, 0]
        self._std_error_ppm = stat_data[:, 1]

        # get unique and clean up DBE values
        values = np.unique(np.concatenate(self._DBE_values))
        self._unique_DBE_values = values[~np.isnan(values)]

    @property
    def are_data_gathered(self) -> bool:
        """ True if arrays over time were filled.
        Call ``self.set_td_data`` to set the arrays. """
        return self._gather_data

    @property
    def elemental_composition(self) -> list:
        """ list of element as string """
        return list(self._elemental_composition)

    @property
    def unique_chem_classes(self):
        """ Return a set of possible chemical classes """
        return np.unique(np.concatenate(self._chem_classes))

    @property
    def unique_chem_classes_mono(self):
        """ Return a set of possible monoisotopic chemical classes """
        return np.unique(np.concatenate(self._chem_classes_mono))

    @property
    def unique_chem_groups(self):
        """ Return a set of possible chemical groups """
        return np.unique(np.concatenate(self._chem_groups))

    @property
    def unique_chem_groups_mono(self):
        """ Return a set of possible monoisotopic chemical groups """
        return np.unique(np.concatenate(self._chem_groups_mono))

    @property
    def chem_group_class_data(self) -> list:
        """ Returns a list of data frame with abundance of chemical groups
        and chemical classes for each scan. """
        return self._chem_group_classes_data

    @property
    def chem_group_classes_sum(self) -> list:
        """ Returns a data frame with the abundance of chemical classes and
        chemical groupes summed over all scans """
        return self._chem_group_classes_sum

    @property
    def unique_DBE_values(self):
        """ Return a set of possible DBE values """
        return self._unique_DBE_values
    
    @property
    def mean_error_ppm(self):
        """ Return the average error in ppm of each scan weighted by intensity """
        return self._mean_error_ppm

    @property
    def std_error_ppm(self):
        """ Return the standard deviation of the error in ppm of each scan 
        weighted by intensity """
        return self._std_error_ppm

    @classmethod
    def from_MZxml(cls, *args, **kwargs):
        """ Not impemented for attributed peak list """
        raise NotImplementedError("Reading from MZxml not yet available.")
    
    @classmethod
    def from_mzml(cls, *args, **kwargs):
        """ Not impemented for attributed peak list """
        raise NotImplementedError("Reading from mzml not yet available.")
    
    @classmethod
    def from_filenames(
        cls,
        filenames: Sequence[str],
        polarity: int,
        verbose: bool = True,
        sorting: bool = True,
        sort_mz: bool = True,
        name: str = None,
        **kwargs
    ):
        """ Import the attributed peak list from a list of files. The code 
        is expected to read files in csv format as outputed by 
        AttributedPeakList.to_dataframe()

        Args:
            filenames (Sequence): Sequence of file names or paths
            polarity (int): ionization charge
            extension (str): file extension, default to 'pks'
            verbose (bool): If True, print some information
            sorting (bool): if true, sort files by index. Warning: files
                must have an index number like: filename-00.ext, where
                00 is the index.
            sort_mz (bool): if True, for each scan, m/z values are sorted
            name (str): name of the sample
            kwargs are arguments of the read_peaklist function.

        Returns:
            Returns the AttributedTimeDependentData object including all the
            data.
        """

        # check files
        for filename in filenames:
            filename = Path(filename)
            if not filename.is_file():
                raise FileNotFoundError(f"File {filename} does not exist.")
            
            if filename.suffix != ".csv":
                raise NotImplementedError(
                    f"Error reading file {filename}. "
                    "Only csv file are supported. Read file "
                    f"with extension '{filename.suffix}' is not yet available.")

        if sorting:
            filenames = sorted(filenames, key=get_file_index)

        list_of_scans = list()
        for filename in filenames:
            if verbose:
                print(f"Reading: {filename}")
            pl = AttributedPeakList.from_file(
                filepath=filename, polarity=polarity, sort_mz=sort_mz,
                name=filename.stem)
                
            list_of_scans.append(pl)

        if name is None:
            name = Path(filenames[0]).stem

        return cls(peaklists=list_of_scans, name=name)

    def get_classes(self, threshold: float = None,
                    cum_threshold: float = None,
                    sort_chem_class: bool = False, **kwargs) -> pd.DataFrame:
        """ Return a pandas dataFrame with chemical groups and classes """
        df = self._chem_group_classes_sum.copy()
        df["rel_abundance"] = df.Intensity.values / df.Intensity.values.sum()
        df.rel_abundance *= 100

        df.sort_values(by="Intensity", ascending=False, inplace=True)
        df["cumsum"] = df.rel_abundance.cumsum()
        # df = df.reset_index(drop=True)
        if threshold is not None:
            df = df[df.rel_abundance >= threshold]
        elif cum_threshold is not None:
            df = df[df["cumsum"] <= cum_threshold]

        df.columns = ["Chem. Group", "Chem. Class", "Intensity",
                      "Relative abundance (%)", "Cumulative abundance (%)"]
        
        if sort_chem_class:
            # sort the table by chem class using the amount of the element
            # in the chemical class.
            # If the chemical class contains more than one elements, the
            # sorting is done on the elements with highest variation
            int_patt = re.compile(r"(\d+)")
            df = df.sort_values("Chem. Group")
            groups = df["Chem. Group"].unique()

            sort_values = list()
            for group in groups:
                chem_classes = df.loc[df["Chem. Group"] == group, "Chem. Class"].values
                if (len(chem_classes) == 1) or (group == "HC"):
                    numbers = [1]
                elif len(group.split()) == 1:
                    numbers = [int(int_patt.findall(el)[-1]) for el in chem_classes]
                else:
                    elements = np.array([chem_class.split() for chem_class in chem_classes])
                    count = [len(set(elements[:, i])) for i in range(elements.shape[1])]
                    idx = count.index(max(count))
                    numbers = [int(int_patt.findall(el)[-1]) for el in elements[:, idx]]
                
                sort_values.extend(numbers)

            df["sort_values"] = sort_values      
            df = df.sort_values(["Chem. Group", "sort_values"])
            df.drop(columns="sort_values", inplace=True)

        return df

    def data_summary(self, dt: float = None, t0: float = None,
                     normalize: str = "max", use_scan_ids: bool = False
                     ) -> pd.DataFrame:
        """ Produce a data frame with general information on each attributed 
        scan as a function of time. 
        
        Args:
            dt (float): time step between MS scan.
            t0 (float): time of the first scan.
            normalize (str): if ``'max'``, sets the maximum intensity of the TIC
                to 100. If ``'integral'``, divides the TIC by its integral.
            use_scan_ids (bool): If True dt is the time interval between scan
                ids and t0 is the time of the first scan id.

        Returns:
            A data frame with general information about scans.
        """
        bpc, mz_bpc = self.get_base_peak_chromatogram(
            normalize=normalize, return_mz=True)

        summary = {
            'scan': self._scan_ids,
            'retentionTime': self.get_retention_times(dt, t0, use_scan_ids),
            'mz_bounds': self.get_scans_mz_bounds(),
            'N_peaks': self.scan_lengths,
            'TIC': self.get_total_ion_chromatogram(normalize=normalize),
            "BPC":  bpc,
            "mz_max_I": mz_bpc,
            "mean_error_ppm": self._mean_error_ppm,
            "std_error_ppm": self._std_error_ppm,
        }
        return pd.DataFrame(summary).set_index("scan", drop=True)

    def _get_stat_average(self, td_array: ArrayLike,
                          q: list = [.05, .25, .5, .75, .95]):
        """ Compute the weighted averages of data by relative abundance 
        as a function of time and returns average and standard deviation.
        
        Args:
            td_array (list of Array): List of arrays for each scan of the
                characteristic we want to average.
            q (list): List of floats corresponding to the percentiles to be
                returned

        Returns:
            A numpy array each line corresponding to a a scan and with the
            following columns: weighted average, standard deviation, minimum
            and maximum values and percentile corresponding to q.

        """
        data = list()
        for idx_scan, scan_array in enumerate(td_array):

            # manage nan values
            idx, = np.nonzero(~np.isnan(scan_array))
            array = scan_array[idx]
            intensity = self._intensity[idx_scan, idx]

            if len(array) == 0:
                data.append([np.nan] * (4 + len(q)))
                continue

            # compute percentiles if required
            if len(q) > 0:
                sort_idx = np.argsort(array)
                intensity = intensity[sort_idx]
                array = array[sort_idx]

                cum_sum = np.cumsum(intensity)
                cum_sum /= cum_sum[-1]

                idx_percentile = np.searchsorted(cum_sum, q)
                q_values = list(array[idx_percentile])
            else:
                q_values = list()

            ave = np.average(array, weights=intensity)
            std = np.sqrt(np.average((array - ave) ** 2, weights=intensity))
            mini = np.nanmin(array)
            maxi = np.nanmax(array)
            
            data.append([ave, std, mini, maxi] + q_values)

        return np.array(data)

    def _filter_intensity_array(
        self,
        target: str,
        td_array: ArrayLike,
        is_float: bool = False,
    ):
        """ Filter the intensity matrix according to a characteristic.

        Args:
            target (str): The target
            td_array (list of Array): List of arrays for each scan of the
                characteristic we want to filter. 
            is_float (bool): If True, use np.isclose to filter, default False
        """
        td_intensity = list()
        if is_float:
            for idx_scan, scan_array in enumerate(td_array):
                idx, = np.nonzero(np.isclose(scan_array, target))
                td_intensity.append(np.sum(self._intensity[idx_scan, idx]))
        else:
            for idx_scan, scan_array in enumerate(td_array):
                idx, = np.nonzero(target == scan_array)
                td_intensity.append(np.sum(self._intensity[idx_scan, idx]))

        return np.array(td_intensity, dtype=np.float64)
    
    def _get_filtered_chromatogram(
        self,
        target: Union[str, float],
        td_array: ArrayLike,
        unique_array: ArrayLike = None,
        is_float: bool = False,
        normalize: str = "max",
        smoothing: bool = False,
        smooth_kws: dict = None,
    ) -> ArrayLike:
        """
        This function compute the equivalent of the total ion chromatogram
        after filtering the data provided in ``td_array`` using the ``target``
        value. The ``unique_array`` is used to check first if the target
        exist in ``td_array``.

        Filtering is only based on string comparison. No float comparisons
        is considered.
        
        Args:
            target (str or float): The target, if it is a float, set is_float True.
            td_array (list of Array): List of arrays for each scan of the
                characteristic we want to filter.
            unique_array (ArrayLike): Array of unique values in td_array.
            is_float (bool): If True, use np.isclose to filter, default False
            normalize (str): if ``'max'``, sets the maximum intensity of
                the TIC to 100. If ``'integral'``, divides the TIC by
                its integral.
            smoothing (bool): if True a smoothing TIC is returned
            smooth_kws (dict): smoothing options.

        Returns:
            A numpy array of the filtered chromatogram
        
        """
        if isinstance(target, str):
            target = target.strip()

        if unique_array is None:
            unique_array = np.concatenante(td_array)

        if is_float:
            valid_target = (
                len(np.nonzero(np.isclose(unique_array, target))[0]) > 0)
        else:
            valid_target = (target in unique_array)
        
        if not valid_target:
            raise ValueError(f"{target} is not present in the data.")
        chromato = self._filter_intensity_array(target, td_array, is_float)

        if smoothing:
            s_params = dict(N=6, Wn=0.2)
            if smooth_kws is not None:
                s_params.update(**smooth_kws)
            chromato = smoothing_method(signal=chromato, **s_params)
                
        if normalize == "max":
            max_c = np.nanmax(chromato)
            chromato = chromato / max_c * 100
        elif normalize == "integral":
            norm = np.trapz(chromato)
            chromato /= norm

        return chromato

    def get_chem_class_chromatogram(
        self,
        chem_class: str,
        normalize: str = "max",
        monoisotopic: bool = True,
        smoothing: bool = False,
        smooth_kws: dict = None,
    ) -> ArrayLike:
        """ This function compute the equivalent of the total ion chromatogram
        after filtering the data to a given chemical class.
        
        Args:
            chem_class (str): The chemical class.
            normalize (str): if ``'max'``, sets the maximum intensity of
                the TIC to 100. If ``'integral'``, divides the TIC by
                its integral.
            monoisotopic (bool): If true, only monoisotopic chemical classes
                are considered.
            smoothing (bool): if True a smoothing TIC is returned
            smooth_kws (dict): smoothing options.

        Returns:
            A numpy array of the Chemical class chromatogram
        
        """
        if monoisotopic:
            return self._get_filtered_chromatogram(
                target=chem_class, td_array=self._chem_classes_mono,
                unique_array=self.unique_chem_classes_mono,
                normalize=normalize, smoothing=smoothing, smooth_kws=smooth_kws
            )
        else:
            return self._get_filtered_chromatogram(
                target=chem_class, td_array=self._chem_classes,
                unique_array=self.unique_chem_classes,
                normalize=normalize, smoothing=smoothing, smooth_kws=smooth_kws
            )

    def get_chem_group_chromatogram(
        self,
        chem_group: str,
        normalize: str = "max",
        monoisotopic: bool = True,
        smoothing: bool = False,
        smooth_kws: dict = None,
    ) -> ArrayLike:
        """ This function compute the equivalent of the total ion chromatogram
        after filtering the data to a given chemical group.
        
        Args:
            chem_class (str): The chemical class.
            normalize (str): if ``'max'``, sets the maximum intensity of
                the TIC to 100. If ``'integral'``, divides the TIC by
                its integral.
            monoisotopic (bool): If true, only monoisotopic chemical classes
                are considered.
            smoothing (bool): if True a smoothing TIC is returned
            smooth_kws (dict): smoothing options.

        Returns:
            A numpy array of the Chemical class chromatogram
        
        """
        if monoisotopic:
            return self._get_filtered_chromatogram(
                target=chem_group, td_array=self._chem_groups_mono,
                unique_array=self.unique_chem_groups_mono,
                normalize=normalize, smoothing=smoothing, smooth_kws=smooth_kws
            )
        else:
            return self._get_filtered_chromatogram(
                target=chem_group, td_array=self._chem_groups,
                unique_array=self.unique_chem_groups,
                normalize=normalize, smoothing=smoothing, smooth_kws=smooth_kws
            )

    def get_DBE_chromatogram(
        self,
        DBE: float,
        normalize: str = "max",
        smoothing: bool = False,
        smooth_kws: dict = None,
    ) -> ArrayLike:
        """ This function compute the equivalent of the total ion chromatogram
        after filtering the data to a given DBE value. The DBE is used as
        a string with one decimal for filtering.
        
        Args:
            DBE (str): The DBE value.
            normalize (str): if ``'max'``, sets the maximum intensity of
                the TIC to 100. If ``'integral'``, divides the TIC by
                its integral.
            smoothing (bool): if True a smoothing TIC is returned
            smooth_kws (dict): smoothing options.

        Returns:
            A numpy array of the Chemical class chromatogram
        
        """
        return self._get_filtered_chromatogram(
            target=DBE, td_array=self._DBE_values,
            unique_array=self.unique_DBE_values, is_float=True,
            normalize=normalize, smoothing=smoothing, smooth_kws=smooth_kws
        )

    def get_averaged_DBE(self, q: list = [.05, .25, .5, .75, .95]):
        """ Compute the weighted average by relative abundance of DBE
        for each scans and statistics
        
        Args:
            q (list): list of percentile between 0 and 1
        
        Returns:
            A numpy array with in columns: average, standard deviation,
            min and max values and percentiles
    
        """
        return self._get_stat_average(self._DBE_values, q=q)

    def get_elemental_ratio(self, el1: str = "C", el2: str = "H"):
        """ Compute the elemental ratio between el1 and el2.
        
        Args:
            el1 (str): first element (numerator)
            el2 (str): second element (denominator)
        
        Returns:
            A list of numpy array of the ratio for each scan.
    
        """
        ratios = list()
        for idx_scan, att_pl in enumerate(self._peaklists):
            el_composition = att_pl.elemental_composition_grid
            try:
                with np.errstate(divide='ignore', invalid='ignore'):
                    ratio = el_composition[el1].values / el_composition[el2].values
                # replace possible inf by nan
                idx, = np.nonzero(np.isinf(ratio))
                ratio[idx] = np.nan
            except KeyError:
                # one element does not exist
                ratio = np.full(len(att_pl), np.nan)

            ratios.append(ratio)
        
        return ratios

    def get_averaged_elemental_ratio(
        self,
        el1: str = "H",
        el2: str = "C",
        q: list = [.05, .25, .5, .75, .95]
    ):
        """ Compute the weighted average by relative abundance of an
        elemental ration for each scans and statistics.
        
        Args:
            el1 (str): first element (numerator)
            el2 (str): second element (denominator)
            q (list): list of percentile between 0 and 1
        
        Returns:
            A numpy array with in columns: average, standard deviation,
            min and max values and percentiles
    
        """
        ratios = self.get_elemental_ratio(el1, el2)
        return self._get_stat_average(ratios, q=q)
    
    def get_averaged_aromaticity(
        self,
        q: list = [.05, .25, .5, .75, .95]
    ):
        """ Compute the weighted average by relative abundance of aromaticity
        for each scans and statistics. The aromaticity is defined as the
        ratio of DBE over H/C.
        
        Args:
            q (list): list of percentile between 0 and 1
        
        Returns:
            A numpy array with in columns: average, standard deviation,
            min and max values and percentiles
    
        """

        ratios = self.get_elemental_ratio(el1="H", el2="C")

        aromaticity = list()
        for ratio, dbe in zip(ratios, self._DBE_values):
            aromaticity.append(dbe / ratio)
    
        return self._get_stat_average(aromaticity, q=q)

    def get_averaged_error_ppm(
        self,
        q: list = [.05, .25, .5, .75, .95]
    ):
        """ Compute the weighted average by relative abundance of the 
        attribution error in ppm for each scans and statistics.
        
        Args:
            q (list): list of percentile between 0 and 1
        
        Returns:
            A numpy array with in columns: average, standard deviation,
            min and max values and percentiles
    
        """    
        return self._get_stat_average(
            [att_pl.error_in_ppm for att_pl in self._peaklists], 
            q=q
        )

    def get_attribution_statistics(self):
        """ Return the attribution statistics of each scan as a function
        of time. """
        data = [att_pl.get_attribution_stats("dict")
                for att_pl in self._peaklists]
        df = pd.DataFrame(data)
        df.rename(columns={"n_peaks": "# peaks"}, inplace=True)
        df.index = self.scan_ids
        df.index.name = "Scan ids"

        return df
