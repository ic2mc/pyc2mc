#!/usr/bin/env python
# coding: utf-8

__author__ = "Carlos Celis Cornejo, Germain Salvato Vallverdu"
__copyright__ = "Copyright 2021, iC2MC"
__version__ = "2024.02.29"

import logging
# from . import io
# from . import formula_grid

__all__ = ["pyc2mc_title", "LOGGER_NAME", "configure_logger"]

pyc2mc_title = rf"""
 ___       ___ ___ __  __  ___ 
| _ \_  _ / __|_  )  \/  |/ __|
|  _/ || | (__ / /| |\/| | (__ 
|_|  \_, |\___/___|_|  |_|\___|
     |__/                      

Welcome!
Version: {__version__}
Copyright (C) 2023 iC2MC and contributors
Licensed under GPL License
"""

LOGGER_NAME = "C2MC"


class C2MCFormatter(logging.Formatter):
    """ A Formatter class to adapt format to log level """

    info_fmt = "%(message)s"
    general_fmt = "%(levelname)s: %(message)s"
    detailed_fmt = "%(levelname)s: %(module)s: %(funcName)s: %(message)s"

    def format(self, record):
        if record.levelno == logging.INFO:
            self._style._fmt = self.info_fmt
        elif record.levelno == logging.DEBUG:
            self._style._fmt = self.detailed_fmt
        else:
            self._style._fmt = self.general_fmt

        return super().format(record)


def configure_logger(level: str = "INFO",
                     filename: str = None) -> logging.Logger:
    """ Configure the logging options """

    # set logging level
    logging.basicConfig(
        level=level,
        encoding="utf-8",
        format="%(levelname)s: %(message)s",
        filename=filename,
    )

    # Create logger
    logger = logging.getLogger(LOGGER_NAME)
    formatter = C2MCFormatter()

    # out file
    if filename is None:
        # add console handler
        handler = logging.StreamHandler()
    else:
        handler = logging.FileHandler(filename)

    # configure handler
    handler.setLevel(level)

    logger.addHandler(handler)
    handler.setFormatter(formatter)
    logger.propagate = False  # needed ?

    return logger


# set up the logger for the library
logger = configure_logger()
