#!/usr/bin/env python
# coding: utf-8

"""
This module provides high level classes in order to perform a peak list
attribution using the implemented algorithms directly or by following an
iteration tables process.
"""

from enum import Enum, unique

from pyc2mc.processing.standard_attribution import (
    lowest_error_attribution,
    isotopic_pattern_attribution)
from pyc2mc.processing.kendrick_attribution import kendrick_attribution

from pyc2mc.controls.attribution import (
    LowestErrorControls,
    IsotopicPatternControls,
    KendrickAttributionControls)

__all__ = ["AttributionAlgorithm"]

# add the possbility to load the peaklist in the init
# add the a method to update formula grid


algo_controls_mapping = {
    "lowest_error": LowestErrorControls,
    "isotopic_pattern": IsotopicPatternControls,
    "kendrick_attribution": KendrickAttributionControls,
}

algo_function_mapping = {
    "lowest_error": lowest_error_attribution,
    "isotopic_pattern": isotopic_pattern_attribution,
    "kendrick_attribution": kendrick_attribution,
}


@unique
class AttributionAlgorithm(str, Enum):

    LOWEST_ERROR = "lowest_error"
    ISOTOPIC_PATTERN = "isotopic_pattern"
    KENDRICK_ATTRIBTUTION = "kendrick_attribution"

    def get_controls_class(self):
        """ Return the control class associated to the algorithm """
        return algo_controls_mapping[self.value]

    @property
    def keywords(self):
        """ keywords associated to the algorithm """
        kw = {"formula_grid", "lambda_parameter", "use_isotopes"}
        if self.value == "lowest_error":
            return kw.union({"use_SN"})
        elif self.value == "isotopic_pattern":
            return kw.union({
                "low_signals_attribution", "isotopic_pattern", "use_SN"})
        elif self.value == "kendrick_attribution":
            return kw.union({"low_signals_attribution", "building_block"})

    @classmethod
    def get_all_keywords(cls):
        kws = [kind.keywords for kind in cls]
        return set.union(*kws)

    def __call__(self, *args, **kwargs):
        """ Run the corresponding algorithm """

        attribution = algo_function_mapping[self.value]
        return attribution(*args, **kwargs)
