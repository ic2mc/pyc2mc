#!/usr/bin/env python
# coding: utf-8

""" This module provides a class to build a calibration list using
kendrick series of a list of repeat units. Each series is first attributed
following the kendrick series attribution algorithm in 
`pyc2mc.processing.kendrick_attribution` and the attributed series are
merged in a calibration list for later apply a recalibration. """

from typing import Tuple, Union, Sequence
import logging
from collections import defaultdict

import numpy as np
import matplotlib.pyplot as plt

from pyc2mc import LOGGER_NAME
from pyc2mc.core.formula import Formula, KendrickBuildingBlock
from pyc2mc.core.peak import PeakList
from pyc2mc.core.formula_grid import FormulaGrid
from pyc2mc.core.calibration import CalibList
from pyc2mc.processing.kendrick_attribution import kendrick_attribution
from pyc2mc.utils import outliers_detection_MAD

__all__ = ["CalibListBuilder"]

BB_TYPE = Union[KendrickBuildingBlock, str, Formula]
CH2 = Formula.from_string("C1 H2")
logger = logging.getLogger(LOGGER_NAME)


class CalibListBuilder:
    """
    This class contains methods to build a calibration list from
    a non calibrated peak list using a set of kendrick series. 
    The methodology is based on the detection
    of series of peaks separated each other by a given repeat unit. After
    attribution, these series are gathered into a new calibration list. 
    The default building block used as a repeat unit is CH2.

    Attributes:
        polarity: ionization charge
        lambda_parameter: error in ppm used to look for kendrick series
        building_block: The formula of  the building block or repeat unit.

    """

    def __init__(
        self,
        peaklist: PeakList,
        building_blocks: Union[Sequence, BB_TYPE] = [CH2],
    ):
        """
        Init method of the Calibration List Building Class.

        Args:
            peaklist (PeakList): The peak list to be calibrated.
            building_block: Kendrick building block

        """
        self._peaklist = peaklist
        if not isinstance(building_blocks, list):
            self._building_blocks = [building_blocks]
        else:
            self._building_blocks = building_blocks

        # default parameters of kendrick series identification
        self.ks_kwargs = dict(min_length=5, limit_number=100, n_max_hole=1)

        # default parameters of kendrick series attribution
        self.att_kwargs = dict(use_isotopes=False)

        # gathered data over runs
        self.formula_grid = None
        self._mean_err = None
        self._std_err = None
        self._removed_series = defaultdict(list)

        self._attributed_kendrick_series = None
        self._all_attributed_ks = None
        self._calibration_list = None

    @property
    def peaklist(self):
        """ The peaklist under consideration """
        return self._peaklist

    @property
    def building_blocks(self):
        """ List of building block to consider to build Kendrick Series """
        return self._building_blocks

    @property
    def attributed_kendrick_series(self) -> dict:
        """ Dict of attributed Kendrick series. The keys of the dict are
        the building blocks. """
        return self._attributed_kendrick_series

    @property
    def calibration_list(self) -> CalibList:
        """ Return the built calibration list """
        return self._calibration_list

    def run(
        self,
        building_blocks: Union[Sequence, BB_TYPE] = None,
        formula_grid: FormulaGrid = None,
        lambda_parameter: float = 1,
        extend: bool = True,
        filter_on: Union[str, None] = "both",
        filter_threshold: float = 3.5,
        error_range: Tuple[float, float] = None,
        att_kwargs: dict = None,
        ks_kwargs: dict = None,
    ):
        """ Run the whole process: look for Kendrick series, filter
        outliers and build the calibration list. This method is an
        automatic and direct processing that gathers several steps that
        can be used individually.

        Args:
            building_block: Kendrick building block
            formula_grid (FormulaGrid): The molecular formula grid to
                be used to attribute series.
            lambda_paramter (float): Largest error, in ppm, for series
                attribution, default is 1 ppm.
            extend (bool): If True, default, extend the current list of
                kendrick series. If False, previous data are deleted.
            filter_on (str or None): Select how to filter kendrick series.
                available options are: ``'mean'``, ``'std'``,
                ``'mean_std'`` or ``'range'``. See ``filter_outliers()``
                for more details.
            filter_threshold (float): Threshold for the MAD filter.
                Default value is 3.5.
            error_range (float, float): Only series with a mean error
                in the given range are selected.
            att_kwargs (dict): dict of parameters for the kendrick series
                attribution.
            ks_kwargs (dict): dict of parameters for kendrick series 
                calculations
        """
        # look for and determine kendrick series
        self.get_attributed_series(
            building_blocks=building_blocks,
            formula_grid=formula_grid,
            extend=extend,
            lambda_parameter=lambda_parameter,
            att_kwargs=att_kwargs,
            ks_kwargs=ks_kwargs,
        )

        # filter
        if filter_on is not None:
            try:
                self.filter_outliers(
                    on=filter_on, filter_threshold=filter_threshold,
                    error_range=error_range)
            except ValueError:
                logger.exception("Outliers filtering was canceled.")

        # build the calibration list
        self.build_calibration_list()

    def get_attributed_series(
        self,
        building_blocks: Union[Sequence, BB_TYPE] = None,
        formula_grid: FormulaGrid = None,
        lambda_parameter: float = 1,
        extend: bool = True,
        att_kwargs: dict = None,
        ks_kwargs: dict = None,
    ):
        """ This method look for Kendrick series for all provided 
        building blocks and proceed to the attribution of the series 
        according to the provided molecular formula grid and the 
        lambda parameter (error in ppm).

        If this method is called several times, you can manage using
        the ``extend`` arguments if you want to restart from a blank 
        list of series or extend the current attributed series. This 
        can be used to include more series with new building blocks. If
        one building block is used several times the series are 
        overwritten.

        Args:
            building_block: Kendrick building block. If None, the
                building blocks contained in the instance attribute are
                used.
            formula_grid (FormulaGrid): The molecular formula grid to
                be used to attribute series.
            lambda_paramter (float): Largest error, in ppm, for series
                attribution, default is 1 ppm.
            extend (bool): If True, default, extend the current list of
                kendrick series. If False, previous data are deleted.
            att_kwargs (dict): dict of parameters for the kendrick series
                attribution.
            ks_kwargs (dict): dict of parameters for kendrick series 
                calculations.
        """
        logger.info("Start building the calibration list.")

        # building block
        if building_blocks is not None:
            if not isinstance(building_blocks, list):
                building_blocks = [building_blocks]

            if extend:
                # include new building blocks
                nstart = len(self._building_blocks)
                self._building_blocks.extend(building_blocks)
            else:
                # restart from beginning
                nstart = 0
                self._building_blocks = building_blocks
        else:
            # if no building blocks is provided, I will consider previously
            # define building block.
            nstart = 0
            if extend and self._all_attributed_ks is not None:
                logger.warning(
                    "No building block was provided. Data will be overwritten.")

        # kendrick series parameters
        if ks_kwargs is not None:
            self.ks_kwargs.update(ks_kwargs)

        # kendrick series attribution parameters
        if att_kwargs is not None:
            self.att_kwargs.update(att_kwargs)
        if lambda_parameter is not None:
            self.att_kwargs.update(dict(lambda_parameter=lambda_parameter))

        # set up dictionnary of attributed kendrick series
        if self._attributed_kendrick_series is None:
            self._attributed_kendrick_series = defaultdict(list)
        if not extend:
            # erase previous data
            self._attributed_kendrick_series = dict()
            self._removed_series = defaultdict(list)
        
        # start attribution
        logger.info("Loop over building blocks.")
        logger.info("-" * 50)
        for building_block in self._building_blocks[nstart:]:
            building_block = KendrickBuildingBlock(building_block)
            str_bb = str(building_block.formula)
            logger.info("Building block: %s", str_bb)
            if str_bb in self._attributed_kendrick_series:
                logger.warning(
                    "For building block '%s', previous series will be overwritten.",
                    str_bb)

            apl, aks = kendrick_attribution(
                self._peaklist,
                formula_grid=formula_grid,
                building_block=building_block,
                ks_kwargs=self.ks_kwargs,
                **self.att_kwargs,
            )
            logger.info("# Attributed series: %d", len(aks))
            lengths = np.array([len(ks) for ks in aks])
            logger.info("Series length: %.1f (%.2f)",
                        lengths.mean(), lengths.std())
            min_mz, max_mz = apl.get_attributed().get_min_max_mz()
            logger.info("m/z range: %.1f -> %.1f", min_mz, max_mz)
            logger.info("-" * 50)

            self._attributed_kendrick_series[str_bb] = aks

        logger.info("Done\n")

        # list of all attributed series
        self._all_attributed_ks = [
            ks for str_building_block in self._attributed_kendrick_series
            for ks in self._attributed_kendrick_series[str_building_block]]

        # gather data and filtering
        self._mean_err = np.array([ks.attributed_peaklist.error_in_ppm.mean()
                                   for ks in self._all_attributed_ks])
        self._std_err = np.array([ks.attributed_peaklist.error_in_ppm.std()
                                  for ks in self._all_attributed_ks])

    def filter_outliers(
        self,
        on: str = "mean_std",
        filter_threshold: float = 3.5,
        error_range: Tuple[float, float] = None
    ):
        """ Filter outliers and remove the corresponding series from 
        the list of previously attributed series.

        Args:
            on (str): Define which kind of filtering is applied. Possible
                values are ``'mean'``, ``'std'``, ``'mean_std'`` or
                ``'range'``. See below for details. If ``'range'``, 
                ``'error_range'`` must be provided.
            filter_threshold (float): Threshold for the MAD filter.
                Default value is 3.5.
            error_range (float, float): Only series with a mean error
                in the given range are selected.
        
        If ``'mean'``, ``'std'`` or ``'mean_std'`` a MAD filter is 
        applied to the series considering the mean, the standard
        deviation or both of them (mean first and std in a second time)
        of the attribution error over the peaks of a series,
        respectively. In that case, the ``filter_threshold`` parameter
        can be used to tune the filtering.

        If the ``'range'`` option is chosen, you must also provid the
        ``error_range`` for the selection of the series.

        """
        if self._all_attributed_ks is None:
            raise ValueError("No attributed kendrick series available.")

        # look for outliers and filtering
        if on == "range":
            if error_range is None:
                raise ValueError(
                    "The filtering option is 'range'. "
                    "You must provide the error interval for series selection."
                )
        elif on not in ["mean_std", "mean", "std"]:
            raise ValueError(
                f"Filter outliers option '{on}' is not implemented. ",
                "Select one of 'mean', 'std', 'mean_std', 'range'."
            )

        # remove outliers
        logger.info("Filtering series.")

        # look for outliers on mean
        n_outliers = 0
        if on in ["mean", "mean_std"]:
            logger.info("Filter series on mean error.")
            outliers, = outliers_detection_MAD(
                self._mean_err, threshold=filter_threshold)
            
            n_outliers += len(outliers)

            # save back series
            for idx in outliers[::-1]:
                self._removed_series["mean"].append(
                    self._all_attributed_ks.pop(idx))

            # update arrrays
            self._mean_err = np.delete(self._mean_err, outliers)
            self._std_err = np.delete(self._std_err, outliers)

        # look for outliers on std
        if on in ["std", "mean_std"]:
            logger.info("Filter series on std error.")

            outliers, = outliers_detection_MAD(
                self._std_err, threshold=filter_threshold)

            n_outliers += len(outliers)

            # save back series
            for idx in outliers[::-1]:
                self._removed_series["std"].append(
                    self._all_attributed_ks.pop(idx))

            # update arrrays
            self._mean_err = np.delete(self._mean_err, outliers)
            self._std_err = np.delete(self._std_err, outliers)

        if on == "range":
            logger.info(
                "Filter series using range (%.3f, %.3f) ppm", *error_range)

            print(self._mean_err)
            outliers, = np.where(
                ((error_range[0] > self._mean_err) |
                 (self._mean_err > error_range[1]))
            )
            
            n_outliers += len(outliers)

            # save back series
            for idx in outliers[::-1]:
                self._removed_series["range"].append(
                    self._all_attributed_ks.pop(idx))

            # update arrrays
            self._mean_err = np.delete(self._mean_err, outliers)
            self._std_err = np.delete(self._std_err, outliers)

        logger.info("# Outliers: %d", n_outliers)

        # Build the dictionary of series without outliers
        self._attributed_kendrick_series = defaultdict(list)
        for ks in self._all_attributed_ks:
            bb_str = str(ks.building_block.formula)
            self._attributed_kendrick_series[bb_str].append(ks)

    def build_calibration_list(self) -> CalibList:
        """ Build the calibration list from all attributed Kendrick
        series """
        if self._all_attributed_ks is None:
            raise ValueError("No attributed kendrick series available.")

        self._calibration_list = CalibList.from_attributed_peaklist(
            self._all_attributed_ks[0].attributed_peaklist)
        for ks in self._all_attributed_ks[1:]:
            self._calibration_list.extend(ks.attributed_peaklist)
        logger.info("Calibration list done: %d unique calibration points",
                    len(self._calibration_list))

        return self._calibration_list

    def reset_series(self):
        """ Return series removed after filtering in the list of selected
        series """

        # Build the dictionary of series
        for _, series in self._removed_series.items():
            for ks in series:
                bb_str = str(ks.building_block.formula)
                self._attributed_kendrick_series[bb_str].append(ks)

        # build the list of all series
        self._all_attributed_ks = [
            ks for str_building_block in self._attributed_kendrick_series
            for ks in self._attributed_kendrick_series[str_building_block]]
    
        # update mean and std error
        self._mean_err = np.array([ks.attributed_peaklist.error_in_ppm.mean()
                                   for ks in self._all_attributed_ks])
        self._std_err = np.array([ks.attributed_peaklist.error_in_ppm.std()
                                  for ks in self._all_attributed_ks])

        # clear removed series
        self._removed_series = defaultdict(list)

    def plot_selected_series(
        self,
        show_outliers: bool = True,
        show_building_blocks: bool = True,
        ax: plt.Axes = None,
    ):
        """ Plot the mean error and the standard error deviation over 
        all considered Kendrick series. 

        Args:
            show_outliers (bool): If True, Kendrick series identified as
                outliers are plotted in a different color.
            ax (plt.Axes): axes on which the data are plotted.

        """
        if ax is None:
            fig = plt.gcf()
            ax = fig.add_subplot()

        # plot
        ax.errorbar(
            x=range(len(self._mean_err)),
            y=self._mean_err,
            ls="", marker="o", color="#333",
            yerr=self._std_err,
            label="selected series")

        previous = len(self._mean_err)
        if show_outliers:
            i_color = 1
            for key, series in self._removed_series.items():
                mean_err = np.array([ks.attributed_peaklist.error_in_ppm.mean()
                                     for ks in series])
                std_err = np.array([ks.attributed_peaklist.error_in_ppm.std()
                                    for ks in series])
                n_mean_outliers = len(mean_err)
                if n_mean_outliers == 0:
                    continue

                # plot series
                ax.errorbar(
                    x=previous + np.arange(len(mean_err)),
                    y=mean_err,
                    ls="", marker="o", color=f"C{i_color}",
                    yerr=std_err,
                    label=f"{key} outliers")

                i_color += 1
                previous += len(mean_err)

            ax.legend()

        if show_building_blocks:
            _, ytop = ax.get_ylim()
            prev = 0
            for building_block in self._attributed_kendrick_series:

                n_series = len(self._attributed_kendrick_series[building_block])
                ax.axvline(prev + n_series - 0.5, color="C3", linestyle="--")
                ax.text(x=prev + n_series / 2 - 0.5, y=ytop,
                        s=building_block, rotation=90,
                        va="bottom", ha="center", color="C3")
                prev += n_series
            # ax.set_ylim(top=ytop)

    def __repr__(self):
        text = f"{self.__class__.__name__}()"
        return text + repr(self._last_calibration_list)
