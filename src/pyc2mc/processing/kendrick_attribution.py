#!/usr/bin/env python
# coding: utf-8

"""
This module contains classes and functions to implement a molecular
attribution based on Kendrick series.
"""

from typing import Union
import copy
import logging
import numpy as np

from pyc2mc import LOGGER_NAME
from pyc2mc.core.isotopes import Isotope
from pyc2mc.core.formula_grid import FormulaGrid
from pyc2mc.core.formula import get_formula, Formula, KendrickBuildingBlock
from pyc2mc.core.peak import PeakList, AttributedPeak, AttributedPeakList
from pyc2mc.core.kendrick import KendrickSeries, AttributedKendrickSeries, KendrickSeriesList
from pyc2mc.processing.kendrick_series import get_kendrick_series
from pyc2mc.processing.assignment import assign_candidates

__all__ = ["kendrick_attribution", "lowest_error_series_attribution",
           "attribute_isotopic_series"]

CH2 = Formula.from_string("C1 H2")
logger = logging.getLogger(LOGGER_NAME)


def kendrick_attribution(
    peaklist: PeakList,
    formula_grid: FormulaGrid,
    kendrick_series: KendrickSeriesList = None,
    lambda_parameter: float = 1,
    use_SN: bool = True,
    use_isotopes: bool = True,
    isotopic_pattern: tuple = (6, 13),
    building_block: Union[KendrickBuildingBlock, str, float, Formula] = CH2,
    min_length_threshold: int = 4,
    use_lowest_error: bool = False,
    SN_threshold: float = 50,
    ks_kwargs: dict = None,
) -> AttributedPeakList:
    """
    Method to perform the molecular formula assignment following the
    Kendrick Series methodology. If only the peaklist is provided, the
    Kendrick based series with the given building block are considered.
    If the kendrick series are provided, the algorithm try to attribute
    only those series. The peaklist is still needed to be able to look
    for isotopic patterns.

    Args:
        peaklist (pyc2mc.core.peak.PeakList): a peak list object.
        formula_grid (pyc2mc.formula_grid.FormulaGrid): Formula Grid.
        lambda_parameter (float): is the maximum error distribution
            to be taken on the calculation for the peak assignment.
        use_SN (bool): if True, the cut off criteria will be based on the
            signal to noise ratio.
        use_isotopes (bool): if True, the algorithm will assign
            the isotopes of the monoisotopic attributed formula.
        isotopic_pattern (tuple): the (Z,A) values of the isotopic
            pattern.
        building_block (Any): specify the molecular composition of the
            Kendrick Series building block.
        min_length_threshold (int): If the number of attributed peak in a
            series is lower than this value, the series is not attributed.
        use_lowest_error (bool): If True and no isotopic peak is found
            a lowest error attribution is applied to the series.
        SN_threshold (float): threshold on the S/N ratio. Select the
            lowest m/z peak in the series with S/N over this threshold. 
        ks_kwargs (dict): dict of parameters for kendrick series 
            calculations
    """  
    # get exact masses from formula grid
    exact_masses = formula_grid.exact_masses

    # set up data
    pl_minimum_intensity = np.nanmin(peaklist.intensity)

    if kendrick_series is not None:
        # kendrick series are provided. Only take into account the provided
        # series. In that case, only the kendrick series are taken into
        # account, non grouped peak are not considered.
        building_block = kendrick_series.building_block

    else:
        # kendrick series were not provided, first look for series    
        # group the peak list by kendrick series
        building_block = KendrickBuildingBlock(building_block)
        if ks_kwargs is None:
            ks_kwargs = dict()
        if "use_SN" not in ks_kwargs:
            ks_kwargs["use_SN"] = use_SN
        if "lambda_parameter" not in ks_kwargs:
            ks_kwargs["lambda_parameter"] = lambda_parameter
            logger.info(
                "lamba_parameter for kendrick series identification: %f",
                lambda_parameter,
            )
        kendrick_series = get_kendrick_series(
            peaklist,
            building_block=building_block,
            **ks_kwargs
        )

        # save back all peak already attributed
        # no efficient if peaklist is totally not attributed
        attributed_peaks = [peak for peak in peaklist if peak.is_attributed]
        nohits = list()

        # recover the non grouped peaks of the kendrick series
        # add them to the nohits list if they are not attributed
        for peak in kendrick_series.non_grouped_peaklist:
            if not peak.is_attributed:
                nohits.append(peak)

    logger.info("# Kendrick series: %d", len(kendrick_series))

    # threshold on SN or on intensity
    use_SN_threshold = peaklist.SN_avail and use_SN

    # define the isotope of the Isotopic Pattern
    iso = Isotope(isotopic_pattern[0], isotopic_pattern[1])
    # estimate the mz delta of the isotopic pattern
    delta = iso.exact_mass - iso.exact_mass_most_abundant
    # define the rtol using the lambda_parameter in ppm
    rtol = lambda_parameter * 1e-6
    # get the mutable kendrick series list to perform the while loop
    ks_list = copy.deepcopy(kendrick_series._kendrick_series_list)

    attributed_kendrick_series = list()
    while len(ks_list) > 0:

        # Series are sorted by intensity so on each iteration
        # the highest intensity series is taken and evaluated
        k_series = ks_list.pop(0)
        attribution = False

        # Check if working with S/N or with intensity and choose the cut_off
        # if working with intensity
        if not use_SN_threshold:
            max_mz = k_series.mz[k_series.intensity.argmax()]
            attr_mz = assign_candidates(
                max_mz, formula_grid, lambda_parameter=lambda_parameter,
                returns_candidates_data=True
            )
            attr_mz = attr_mz.squeeze()

            if attr_mz.candidates_exist:
                # twice the signal of the minimum intensity
                cutt_off_factor = 2 * pl_minimum_intensity / iso.abundance
            else:
                # The highest intensity signal does not have candidates
                # it means that all the series must be rejected as no hits
                for peak in k_series:
                    if not peak.is_attributed:
                        nohits.append(peak)
                continue

            # check all the atom numbers of the element pattern present
            # in the candidates formulae
            elm_num = attr_mz['cand_list'][iso.element.symbol]

            # minimum cut_off in which could be possible to find the isotopic
            # pattern of the signal
            cut_off = np.min(cutt_off_factor / elm_num)

            # find in the series the closest value that accomplish this criteria
            idx = np.where(k_series.intensity > cut_off)[0]

            if len(idx) > 0:
                # we have a peak over the cut_off. As the peaks are sorted
                # according to m/z, 0 is the lowest m/z value above the
                # cutoff.
                selected_peaks = [k_series[i] for i in idx]
            elif use_lowest_error:
                # the signals in the series are too weak to consider the 
                # possibility of finding an isotopic pattern, but they could
                # be attributed with a possible monoisotopic candidate
                ap_le, nh_le = lowest_error_series_attribution(
                    series=k_series, 
                    formula_grid=formula_grid, 
                    kendrick_series_list=ks_list, 
                    peaklist=peaklist,
                    lambda_parameter=lambda_parameter,
                    use_isotopes=use_isotopes,
                    min_length_threshold=min_length_threshold,
                    building_block=building_block,
                )
                attributed_peaks.extend(ap_le)
                nohits.extend(nh_le)
                continue

            else:
                # reject the series
                for peak in k_series:
                    if not peak.is_attributed:
                        nohits.append(peak)
                continue

        # work with S/N
        else:
            # this is the cut off criteria when working with S/N
            # cut_off = 50
            idx = np.where(k_series.SN > SN_threshold)[0]
            if len(idx) > 0:
                # select the lowest index of peaks in the series with 
                # S/N over 50. As peak are sorted according to m/z, it is 
                # the lowest m/z value with S/N > 50.
                selected_peaks = [k_series[i] for i in idx]
            elif use_lowest_error:
                # the signals in the series are very weak to consider the 
                # possibility of finding an isotopic pattern, but they could
                # be attributed with a possible monoisotopic candidate
                ap_le, nh_le = lowest_error_series_attribution(
                    series=k_series, 
                    formula_grid=formula_grid, 
                    kendrick_series_list=ks_list,
                    peaklist=peaklist,
                    lambda_parameter=lambda_parameter,
                    use_isotopes=use_isotopes,
                    building_block=building_block,
                    min_length_threshold=min_length_threshold,
                )
                attributed_peaks.extend(ap_le)
                nohits.extend(nh_le)
                continue
            else:
                # reject the series
                for peak in k_series:
                    if not peak.is_attributed:
                        nohits.append(peak)
                continue

        # try to find an isotopic peak starting from the lowest m/z value
        for peak_position, selected_peak in enumerate(selected_peaks):

            # estimate the possible mz value of the isotopic pattern
            isopatt_mz = selected_peak.mz + delta

            # find this mz on the peak list
            jdx, = np.where(np.isclose(
                peaklist.mz, isopatt_mz, rtol=rtol, atol=0.))
            if len(jdx) > 0:
                # there is/are candidates for that peak
                break

        # if one peak is selected then get the mz value
        if len(jdx) == 1:
            iso_mz = peaklist.mz[jdx[0]]
        elif len(jdx) > 1:
            # if more than one peak is located then get the mz value of the
            # closest peak
            jdx = jdx[np.argmin(np.abs(isopatt_mz - peaklist.mz[jdx]))]
            iso_mz = peaklist.mz[jdx]
        else:
            # As the peak intensity or S/N is above the cutoff it is expected 
            # to find an isotope but we don't. This indicates that the series
            # should not be attributed and all the series is rejected
            if use_lowest_error:
                # try to attribute the series from lowest error
                ap_le, nh_le = lowest_error_series_attribution(
                    series=k_series, 
                    formula_grid=formula_grid, 
                    kendrick_series_list=ks_list,
                    peaklist=peaklist,
                    lambda_parameter=lambda_parameter,
                    use_isotopes=use_isotopes,
                    building_block=building_block,
                    min_length_threshold=min_length_threshold,
                )
                attributed_peaks.extend(ap_le)
                nohits.extend(nh_le)
                continue
            else:
                # reject the series
                for peak in k_series:
                    if not peak.is_attributed:
                        nohits.append(peak)
                continue

        # get the candidates for the isotope of the selected m/z
        attr_iso_mz = assign_candidates(
            iso_mz, formula_grid, lambda_parameter=lambda_parameter,
            returns_candidates_data=True
        )
        attr_iso_mz = attr_iso_mz.squeeze()
        cand_list = attr_iso_mz['cand_list']

        if attr_iso_mz.candidates_exist and cand_list.isotopic.any():
            # As only monoisotopic formulae are considered in this step, then
            # only formulae that contains one atom of the isotope are correct.
            # The other isotopes such as 13C2 will be assigned once the mono-
            # isotopic signal is found.
            cand_list = cand_list[cand_list[iso.symbol] == 1]
        else:
            # if no candidate is found for the isotopic pattern, reject
            # the series attribution or try lowest error
            if use_lowest_error:
                # try to attribute the series from lowest error
                ap_le, nh_le = lowest_error_series_attribution(
                    series=k_series, 
                    formula_grid=formula_grid, 
                    kendrick_series_list=ks_list,
                    peaklist=peaklist,
                    lambda_parameter=lambda_parameter,
                    use_isotopes=use_isotopes,
                    building_block=building_block,
                    min_length_threshold=min_length_threshold,
                )
                attributed_peaks.extend(ap_le)
                nohits.extend(nh_le)
                continue
            else:
                # reject the series
                for peak in k_series:
                    if not peak.is_attributed:
                        nohits.append(peak)
                continue

        # if more that one candidate with the correct isotope ratio are
        # found, take the lowest error option.
        if len(cand_list) > 1:
            iso_selected = cand_list.iloc[np.abs(cand_list.err_ppm.values).argmin()]
        else:
            if len(cand_list) == 1:
                iso_selected = cand_list.squeeze()
            else:
                # candidate list is empty
                # TODO: this is usually already handle from previous test 
                # on candidate exist.
                # reject the attribution of the monoisotopic series due to the lack of
                # formulas that accomplish the expected isotopic pattern
                for peak in k_series:
                    if not peak.is_attributed:
                        nohits.append(peak)
                continue

        # if the candidate list is not empty proceed to perform the attribution of
        # the mono isotopic series only. The isotopes are attributed later.
        iso_f = get_formula(iso_selected.formula, exact_masses=exact_masses)
        # substitute the isotope by the main isotope element
        attributed_formula = iso_f.substitute_elements(
            {iso.symbol: -1, iso.element.symbol: 1})

        # check if the formula is monoisotopic and reject the series if not
        if attributed_formula.is_isotopic:
            for peak in k_series:
                if not peak.is_attributed:
                    nohits.append(peak)
            continue

        # proceed to series attribution by kendrick isotopic pattern
        apeaks, nh = _attribute_series(
            serie=k_series,
            formula=attributed_formula,
            formula_grid=formula_grid,
            lambda_parameter=lambda_parameter, 
            building_block=building_block,
            min_length_threshold=min_length_threshold,
            method='kendrick_isotopic_pattern')

        # update
        attributed_peaks.extend(apeaks)
        nohits.extend(nh)
        if len(apeaks) > 0:
            attribution = True

            attributed_kendrick_series.append(
                AttributedKendrickSeries.from_list(
                    apeaks, polarity=formula_grid.polarity,
                    building_block=building_block,
                    sort_mz=False)
            )

            logger.debug(
                "selected peak: idx = %d mz= %.3f SN= %.2f",
                peak_position, selected_peak.mz, selected_peak.SN
            )

        # Attribute Isotopes
        if use_isotopes and attribution:
            # get the attributed peak with the highest intensity
            p_idx = np.argmax([p.intensity for p in apeaks])
            attributed_peak = apeaks[p_idx]
            apeaks_iso, nh_iso = attribute_isotopic_series(
                attributed_peak, formula_grid, ks_list,
                peaklist, lambda_parameter, building_block)
            attributed_peaks.extend(apeaks_iso)
            nohits.extend(nh_iso)
    
    if len(attributed_peaks) >= min_length_threshold:
        # complete the output by adding the no hits if they were not 
        # previously attributed
        attributed_pid = set([peak.pid for peak in attributed_peaks])
        attributed_peaks.extend(
            [peak for peak in nohits if peak.pid not in attributed_pid])
        return AttributedPeakList.from_list(attributed_peaks), attributed_kendrick_series
    else:
        raise RuntimeError(
            "Kendrick attribution fails. No series has been assigned.")
#     return attributed_peaks, nohits


def lowest_error_series_attribution(
    series: KendrickSeries,
    formula_grid: FormulaGrid,
    kendrick_series_list: list,
    peaklist: PeakList,
    lambda_parameter: float = 1,
    building_block: Union[KendrickBuildingBlock, str, float, Formula] = CH2,
    use_isotopes: bool = False,
    min_length_threshold: int = 4,
) -> tuple[list]:
    """
    Perform an attribution of series whose intensity or S/N is under the estimated
    cutoff to see possible isotopic patterns. In this case the main assumption
    is that the series will be attributed firstly by looking if it is possible
    to attribute them using only monoisotopic signals. To perform the
    attribution, the lowest mz value peak of the series is taken to search for
    candidates. If an attribution is performed, the algorithm will search for
    isotopes taking the highest intensity of the attributed series.
    
    Args:
        series (pyc2mc.core.kendrick.KendrickSeries): the current kendrick
            series.
        kendrick_series_list (list): a mutable kendrick series list object
            containing the groupe of kendrick series.
        formula_grid (pyc2mc.core.formula_grid.FormulaGrid): the defined
            formula grid.
        peaklist (pyc2mc.core.peak.PeakList): the non attributed peak list.
        lambda_parameter (float): is the maximum error distribution
            to be taken on the calculation for the peak assignment.
        building_block (str): the characteristic building block of the
            kendrick series. It is 'C1 H2', by default.
        use_isotopes (bool): if True, the algorithm will assign
            the isotopes of the monoisotopic attributed formula.
        min_length_threshold (int): If the number of attributed peak in a
            series is lower than this value, the series is not attributed.

    Returns:
        The list of attributed peak and the list of no hits

    """
    # initialize the output variables
    no_hits_le = []
    apeaks_le = []
    
    attribution_le = False
    building_block = KendrickBuildingBlock(building_block)
    
    # take the peak with the lowest mz value of the series
    selected = series[0]
    
    # search candidates for the peak
    attr_p = assign_candidates(selected.mz, formula_grid,
                               lambda_parameter=lambda_parameter,
                               returns_candidates_data=True)
    attr_p = attr_p.squeeze()
    cand_list = attr_p['cand_list']
    
    # check if any candidate was found
    if attr_p.candidates_exist:
        # take the monoisotopic candidates
        cand_list = cand_list[~cand_list['isotopic']]
    else:
        # if no candidate is found reject the assignment of the whole series
        for peak in series:
            if not peak.is_attributed:
                no_hits_le.append(peak)
        return apeaks_le, no_hits_le
    
    if cand_list.empty:
        # no monoisotopic candidate
        for peak in series:
            if not peak.is_attributed:
                no_hits_le.append(peak)
        return apeaks_le, no_hits_le
    
    # select the lowest error candidate
    cand_list = cand_list.iloc[np.abs(cand_list.err_ppm.values).argmin()]
    le_f = get_formula(cand_list.formula, formula_grid.exact_masses)
    ap_le, nh_le = _attribute_series(
        serie=series, 
        formula=le_f, 
        formula_grid=formula_grid,
        lambda_parameter=lambda_parameter,
        building_block=building_block,
        min_length_threshold=min_length_threshold,
        method='kendrick_lowest_error')
    no_hits_le.extend(nh_le)
    apeaks_le.extend(ap_le)
    # attribution_le = True
    if len(ap_le) > 0:
        attribution_le = True

    # search isotopes
    if use_isotopes and attribution_le:
        # take the peak with the highest intensity in the series
        # if the peak is attributed, try to find an isotopic series
        # Only monoisotpic peaks are considered
        p_idx = np.argmax(series.intensity)
        if series[p_idx].is_attributed and not series[p_idx].is_isotopic:
            ap_iso_le, nh_iso_le = attribute_isotopic_series(
                attributed_peak=series[p_idx],
                formula_grid=formula_grid,
                kendrick_series_list=kendrick_series_list,
                peaklist=peaklist,
                lambda_parameter=lambda_parameter, 
                building_block=building_block)
            apeaks_le.extend(ap_iso_le)
            no_hits_le.extend(nh_iso_le)
    
    return apeaks_le, no_hits_le


def attribute_isotopic_series(
    attributed_peak: AttributedPeak,
    formula_grid: FormulaGrid,
    kendrick_series_list: list,
    peaklist: PeakList,
    lambda_parameter: float = 1,
    building_block: Union[KendrickBuildingBlock, str, float, Formula] = CH2,
    min_length_threshold: int = 4,
) -> tuple[list]:
    """
    Look for isotopic kendrick series associated to the attributed peak
    given in argument and attribute the corresponding series. All the
    isotopic grids provided in the formula grid are probe.
    
    Args:
        attributed_peak (pyc2mc.core.peak.AttributedPeak): the attributed
            peak in the current kendrick series.
        formula_grid (pyc2mc.core.formula_grid.FormulaGrid): the current
            formula grid.
        kendrick_series_list (list): a mutable kendrick series list object
            containing the groupe of kendrick series.
        peaklist (pyc2mc.core.peak.PeakList): the non attributed peak list.
        lambda_parameter (float): is the maximum error distribution
            to be taken on the calculation for the peak assignment.
        building_block (str): the characteristic building block of the
            kendrick series. It is 'C1 H2', by default.
        min_length_threshold (int): If the number of attributed peak in a
            series is lower than this value, the series is not attributed.
            
    Returns:
        The list with the new attributed peak and the list of no hits.
    """
    # relative error for isclose method of numpy
    rtol = lambda_parameter * 1e-6
    building_block = KendrickBuildingBlock(building_block)
    attributed_peaks = []  # newly attributed peak
    no_hits_iso = []
    
    for pattern_grid in formula_grid.values():
        if not pattern_grid.is_isotopic:
            # monoisotopic grid
            continue

        # check if the monoisotopic formula of the input peak contains
        # the elements of the considered isotopic substitution
        elts_in_formula = all([iso.element.symbol in attributed_peak.formula 
                               for iso in pattern_grid.isotopes])
        if not elts_in_formula:
            continue

        elts_not_null = all([attributed_peak.formula[iso.element.symbol] != 0
                             for iso in pattern_grid.isotopes])
        if not elts_not_null:
            continue
        
        # target mz
        mz_iso_theo = attributed_peak.mz + pattern_grid.mz_shift

        # index of closest peaks
        mask = np.isclose(peaklist.mz, mz_iso_theo, rtol=rtol)

        if not mask.any():
            # no any peak exist for this isotopic pattern
            continue
        
        # select closest mzs
        select_mz = np.array(peaklist.mz[mask])
        pid_iso = np.array(peaklist.pid[mask])
        error = np.abs(select_mz - mz_iso_theo)
        select_mz = select_mz[error.argmin()]
        pid_iso = pid_iso[error.argmin()]

        attr_iso = assign_candidates(select_mz, formula_grid,
                                     lambda_parameter=lambda_parameter,
                                     returns_candidates_data=True)

        # check if there is a candidate
        attr_iso = attr_iso.squeeze()
        if attr_iso.candidates_exist:
            candidate_list = attr_iso['cand_list']
        else:
            continue
        
        is_valid_isotopic_formula = [
            all([iso.symbol in f for iso in pattern_grid.isotopes])
            for f in candidate_list.formula.values
        ]
        candidate_list = candidate_list.loc[is_valid_isotopic_formula]

        # select the best candidates based on the lowest mz
        # deviation
        if not candidate_list.empty:
            if len(candidate_list) > 1:
                ix = np.abs(candidate_list.err_ppm.values).argmin()
            else:
                ix = 0

            candidate = candidate_list.iloc[ix]
            iso_formula = get_formula(
                candidate.formula,
                exact_masses=formula_grid.exact_masses)
            
            # one last thing is to validate if the candidate matches the parent
            if not (attributed_peak.formula.elemental_composition ==
                    iso_formula.elemental_composition):
                continue
            
            # find the series with the isotopic peak
            idx_s = [i for i, series in enumerate(kendrick_series_list)
                     if pid_iso in series.pid]
            
            # check if the series was not already attributed
            if len(idx_s) == 0:
                # Series was already attributed
                continue
                
            # get the series from the list
            iso_s = kendrick_series_list.pop(idx_s[0])
            # attribute the series
            apeaks_iso, nhi = _attribute_series(
                serie=iso_s,
                formula=iso_formula,
                formula_grid=formula_grid,
                lambda_parameter=lambda_parameter,
                building_block=building_block,
                min_length_threshold=min_length_threshold,
                method="kendrick_isotopes")
            no_hits_iso.extend(nhi)
            attributed_peaks.extend(apeaks_iso)
    
    return attributed_peaks, no_hits_iso


def _attribute_series(
    serie: KendrickSeries,
    formula: Formula,
    formula_grid: FormulaGrid,
    lambda_parameter: float = 1,
    building_block: Union[KendrickBuildingBlock, str, float, Formula] = CH2,
    min_length_threshold: int = 4,
    method: str = ''
) -> tuple[list]:
    """
    Take a series and add the attributed peaks to the attributed_peak_list,
    by adding the corresponding formula of the kendrick series.
    
    Args:
        serie (pyc2mc.core.kendrick.KendrickSeries): the KendrickSeries that was
            targeted with an attributed peak on it.
        formula (Formula): the attributed formula of the peak.
        formula_grid (pyc2mc.core.formula_grid.FormulaGrid): the current
            formula grid.
        lambda_parameter (float): 
        building_block (str): the characteristic building block of the
            kendrick series. It is 'C1 H2', by default.
        min_length_threshold (int): If the number of attributed peak in a
            series is lower than this value, the series is not attributed.
        method (str): the attribution method name
    
    Returns:
        A list with the attributed peaks and the no hits
    """
    building_block = KendrickBuildingBlock(building_block)

    # returned list
    attributed_peaks = []  # newly attributed peak
    no_hits = []
    
    # assign candidates to all peaks, this guarantee that the attributed
    # formula to each peak respects the assignment rules defined in the
    # formula grid.
    attr_s = assign_candidates(serie, formula_grid,
                               lambda_parameter=lambda_parameter,
                               returns_candidates_data=True)
    
    # check how many peaks have candidates
    candidates_exist, = np.nonzero(attr_s.candidates_exist.values)
    if candidates_exist.size < min_length_threshold:
        # not enough candidates the series cannot be attributed
        no_hits = [peak for peak in serie if not peak.is_attributed]
        return attributed_peaks, no_hits
    
    n_attributed = 0
    for i, peak in enumerate(serie):
        
        if not attr_s.candidates_exist.values[i]:
            if not peak.is_attributed:
                no_hits.append(peak)
            continue
        
        # factor of the building block
        factor = np.round(
            (peak.mz - formula.exact_mass) / building_block.exact_mass
        ).astype(np.int8)
        # get the peak formula
        substitution_dict = {el: factor * n 
                             for el, n in building_block.formula.items()}
        try:
            new_formula = formula.substitute_elements(substitution_dict)
        except ValueError:
            # impossible to build the formula
            if not peak.is_attributed:
                no_hits.append(peak)
            continue

        if peak.is_attributed and new_formula == peak.formula:
            # the previous attribution agrees with the current series and the
            # associated building block
            n_attributed += 1

        elif str(new_formula) in attr_s.cand_list.values[i].formula.values:
            # ensure that the formulas respect the assignment rules
            attributed_peaks.append(
                AttributedPeak(
                    mz=peak.mz,
                    intensity=peak.intensity,
                    formula=new_formula,
                    polarity=formula_grid.polarity,
                    SN=peak.SN,
                    pid=peak.pid,
                    properties=peak.properties,
                    attribution_method=method,
                    exact_masses=formula_grid.exact_masses
                )
            )
            n_attributed += 1
        else:
            # f = get_formula(new_formula, exact_masses=formula_grid.exact_masses)
            # print("    wrong formula", str(new_formula), attr_s.cand_list.values[i].formula.values)
            # mz = f.get_ion_exact_mass(polarity=formula_grid.polarity)
            # print(f"    {serie.name} {peak.mz: .4f} {mz: .4f} {(peak.mz - mz) / mz * 1e6:.4f}")
            if not peak.is_attributed:
                no_hits.append(peak)

    # check the length of attributed peaks
    if n_attributed < min_length_threshold:
        # print("After attribution, not enough attribution", len(attributed_peaks))
        # the series attribution is rejected because not enough peaks in
        # the series could be attributed
        no_hits = [peak for peak in serie if not peak.is_attributed]
        attributed_peaks = list()

    return attributed_peaks, no_hits
