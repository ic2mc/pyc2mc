#!/usr/bin/env python
# coding: utf-8

"""
This module contains classes comprising the 2D data treatment to perform
the Regions of Interest (ROI) detection.
"""

import numpy as np
import copy
import warnings
from sklearn.cluster import k_means
try:
    from ckwrap import ckmeans
    CKWRAP_AVAILABLE = True
except ModuleNotFoundError:
    CKWRAP_AVAILABLE = False    
import pandas as pd

from pyc2mc.utils import find_nearest
from pyc2mc.core.nddata import PeakListCollection
import pyc2mc.core.roi


__author__ = "Carlos Celis Cornejo, Germain Salvato Vallverdu"
__copyright__ = "Copyright 2021, iC2MC"
__version__ = "2022.11.08"
__all__ = []


# This method is used by both sub classes FeatureDetection and KmeansDetection
# to unify the recoverd ROI in the boundaries of integer_part mz and the 
# general ROI.

def unifying_rois(ROI, ROI_boundaries, lambda_parameter: float = 3.0):
    """
    This method look into the general ROI detected and recovers all
    the features that were caught in the ROI_boundaries. The method
    overwrite the features of ROI at a very close mzmeans value if
    they exists.

    Args:
        ROI (dict): A dictionary containing four keys named, mzmeans, mz,
            intensity, and scans. Where mzmeans contains a list with the
            average of each mz feature detected. The other keys of the
            dictionary are a list of lists containing the information of
            the detected features.
        ROI_boundaries (dict): As ROI, is a similar dictionary, but this
            one is filled with the features detected in the boundaries of
            integer_part mz.
        lambda_parameter (float): m/z treshold for grouping information.

    Returns:
        A new ROI dictionary with the updated values.
    """
    
    # new_ROI = copy.copy(ROI) ?

    rtol = lambda_parameter * 1e-6
    
    for i, val in enumerate(ROI_boundaries['mzmeans']):
        
        idx = np.where(np.isclose(np.array(ROI['mzmeans']), val, rtol=rtol))[0]
        # if a value is found it must be replaced, else, a new one must be set
        if len(idx) > 0:
            if len(idx) > 1:
                abs_diff = np.abs(np.array(
                    [ROI['mzmeans'][i] for i in idx]) - val)
                argmin = np.argmin(abs_diff)
                idx = [idx[argmin]]
    
            # replace the old values
            for key in ROI.keys():
                ROI[key][idx[0]] = ROI_boundaries[key][i]
        else:
            # add new ROIs
            position = np.searchsorted(ROI['mzmeans'], val)
            for key in ROI.keys():
                ROI[key].insert(position, ROI_boundaries[key][i])
    
    return ROI


class FeatureDetection:
    """ 
    Feature Detection algorithm based on the concept of Tautenhahn et al., 
    BMC Bioinformatics 2008. Two different options are implemented herein.
    The first, and the default option, is a modified iterative method based
    on the same concept published by Tautenhahn et al. In this option the
    data is firstly splitted into groups or clusters of m/z integer part. The
    advantage of this method is that the memory allocation is considerably
    reduced. Consequently, the computational time is drastically reduced
    compared with the classical feature detection method. The second option
    is the classical feature detection method as published by Tautenhahn et
    al. The option to set either one or the other is the groupby_integer_part
    argument, which is True by default, thus indicating that the method
    employed is the one which splits the data on clusters of m/z integer part.

    .. attribute: groupby_integer_part

        It contains a boolean to decide if use the method that splits the data
        into m/z integer part or the classic iterative one that treats the 
        entire data set.

    .. attribute: clusters

        Is a list which contains the values of existing m/z integer part.
        The term cluster is used here to define the regions of m/z integer part
        where it is possible to find peaks along the time axis.
    """

    def __init__(self, raw_data: PeakListCollection,
                 min_centroids: int = 2,
                 lambda_parameter: float = 3.0,
                 continuity_min: int = 1,
                 intensity_cutoff: float = None,
                 groupby_integer_part: bool = True,
                 check_boundaries: bool = False):
        """
        Initialize the feature detection class.

        Args:
            raw_data (pyc2mc.io.nddata.HyphenantedMassSpecData): object 
                containing the lists of masses and intensities from the 
                time-resolved data (scans)..
            min_centroids (int): minimum number of consecutive points to 
                consider a feature as a ROI. For defining this value it
                could be helpful to know the minimum chromatographic 
                peak width.
            lambda_parameter (float): this value is the mass accuracy of the mass
                spectra, it is given in ppm. It has a default value of
                10 ppm.
            continuity_min (int): minimum number of scans to check if a ROI is
                taken as one or if it is splitted due to this minimum 
                separation. It has a default value of 1, if it is not set. A 
                value of 1 means that if a ROI is not continue in the scan 
                order it will be splitted. A value of two implies that if a 
                disonctinuity of one scan exists, it is negligable, so the ROI
                is not splitted, even with this void.
            intensity_cutoff (float): filter ROIs whose intensity is lower 
                than this value.
            groupby_integer_part (bool): condition to apply the clustering approach
                by m/z integer part, which stands that there is only an island
                of data on each m/z integer part.
            check_boundaries (bool): if True, the algorithm will search on 
                each exact integer_part mz, to look if there are some features
                lost due to the groupby_integer_part approach. 
        """

        # 1. Instantiate the input information
        self._data = raw_data
        self._pmin = min_centroids
        self._lambda_parameter = lambda_parameter
        self._continuity_min = continuity_min
        self._intensity_cutoff = intensity_cutoff
        self.groupby_integer_part = groupby_integer_part
        self._check_boundaries = check_boundaries

        # 2. set up the groups by integer part in a data frame
        list_intens = list()
        list_mz = list()
        list_scan = list()
        for s, pl in enumerate(self._data):
            if len(pl) == 0:
                # avoid error if pl is empty for one scan
                continue
            scan = [s] * len(pl)
            list_scan.extend(scan)
            list_intens.extend(pl.values[:, 1])
            list_mz.extend(pl.values[:, 0])

        df = pd.DataFrame(
            {'mz': list_mz, 'intensity': list_intens, 'scan': list_scan})
        df['integer_part'] = np.floor(df.mz).astype(np.uint16)
        self._data_grouped_nm = df.groupby(by="integer_part")
        self.clusters = list(set((df.integer_part.values)))

        # save temporary df with the concatenated data this will be further
        # deleted.
        self._df = df
        del df

    def run(self, min_centroids: int = None,
            lambda_parameter: float = None,
            continuity_min: int = None,
            intensity_cutoff: float = None,
            groupby_integer_part: bool = None,
            check_boundaries: bool = None):
        """
        Run the detection algorithm based on the concept of Tautenhahn et al., 
        BMC Bioinformatics 2008. Two different options are implemented herein.
        The first, and the default option, is a modified iterative method based
        on the same concept published by Tautenhahn et al. In this option the
        data is firstly splitted into groups or clusters of m/z integer part. The
        advantage of this method is that the memory allocation is considerably
        reduced. Consequently, the computational time is drastically reduced
        compared with the classical feature detection method (ption 2). Then,
        the second option is the classical unmodified, feature detecion method
        as published by Tautenhahn et al.
        The option to set either one or the other is the groupby_integer_part
        argument, which is True by default, thus indicating that the method
        employed is the one which splits the data on clusters of m/z integer
        part.

        Args:
            min_centroids (int): minimum number of consecutive points to 
                consider a feature as a ROI. For defining this value it
                could be helpful to know the minimum chromatographic 
                peak width.
            lambda_parameter (float): this value is the mass accuracy of the mass
                spectra, it is given in ppm. It has a default value of
                10 ppm.
            continuity_min (int): minimum number of scans to check if a ROI is
                taken as one or if it is splitted due to this minimum 
                separation. It has a default value of 1, if it is not set. A 
                value of 1 means that if a ROI is not continue in the scan 
                order it will be splitted. A value of two implies that if a 
                discontinuity of one scan exists, it is negligible, so the ROI
                is not splitted, even with this void.
            intensity_cutoff (float): filter ROIs whose intensity is lower 
                than this value.
            groupby_integer_part (bool): condition to apply the clustering approach
                by m/z integer part, which stands that there is only an island
                of data on each m/z integer part. This is True by default.
            check_boundaries (bool): if True, the algorithm will search on 
                each exact integer_part mz, to look if there are some features
                lost due to the groupby_integer_part approach.

        Returns:
            A ROIList object containing the regions of interest detected.
        """
        # Re-instantiate the input information if it is redefined in run
        if min_centroids is not None:
            self._pmin = min_centroids
        if lambda_parameter is not None:
            self._lambda_parameter = lambda_parameter
        if continuity_min is not None:
            self._continuity_min = continuity_min
        if intensity_cutoff is not None:
            self._intensity_cutoff = intensity_cutoff
        if groupby_integer_part is not None:
            self.groupby_integer_part = groupby_integer_part
        if check_boundaries is not None:
            self._check_boundaries = check_boundaries

        # 3. Normally this is True by default, this option defines if use
        # the feature detection method clustered by m/z integer part or
        # if use the classic iterative method with the whole information.
        if self.groupby_integer_part:
            ROI = FeatureDetection._feature_detection_integer_part(
                lambda_parameter=self._lambda_parameter,
                df=self._data_grouped_nm,
                clusters=self.clusters,
                data=self._data)

            # 3.1. If the groupby_integer_part mass option is True, the data is
            # normally splitted using the integer part of m/z. In this case,
            # some Features located in the boundaries of m/z integer part could
            # be partitioned by error. If so, The check_boundaries condition is
            # intended to recover this splitted ROIs and unifying them into new
            # ones.
            if self._check_boundaries:
                ROI_boundaries = FeatureDetection._feature_detection_integer_part(
                    lambda_parameter=self._lambda_parameter,
                    df=self._df,
                    clusters=self.clusters,
                    data=self._data,
                    mode='boundaries')
                del self._df
                ROI_boundaries = FeatureDetection._continuity_check(
                    ROI=ROI_boundaries,
                    min_centroids=self._pmin,
                    continuity_min=self._continuity_min,
                    intensity_cutoff=self._intensity_cutoff
                )
                ROI_boundaries = FeatureDetection._filter_centroids(
                    ROI_boundaries, self._pmin)

        else:
            ROI = self._feature_detection(self._lambda_parameter)

        # 4. Once the data is arranged into the _ROI attribute, after the
        # feature detection is applied, a the _filter_centroids function
        # eliminates the ROIs which do not respect the _pmin constrain.
        ROI = FeatureDetection._filter_centroids(ROI, self._pmin)

        # 5. Next step is to clear ROIs which do not respect a minimun space
        # of continuity_min points.
        ROI = FeatureDetection._continuity_check(
            ROI, self._pmin, self._continuity_min, self._intensity_cutoff)

        if self._check_boundaries:
            # 5.1. match the general ROI and ROI_boundaries
            ROI = unifying_rois(
                ROI, ROI_boundaries, self._lambda_parameter)
        
        # TODO: verify if this next filtering step contributes or not ???
        ROI = FeatureDetection._filter_centroids(ROI, self._pmin)

        # from this point all these methods are intended to arrange the
        # information into attributed to enhance the plotting speed and
        # the data representation... However, all these methods are
        # available in the ROI_list class of roi.py.

        # save the final ROI dictionary as a private attribute
        self._ROI = ROI

        return pyc2mc.core.roi.ROIList(
            scans=ROI["scans"],
            mz=ROI["mz"],
            intensity=ROI["intensity"],
            name="ROIs from integer part grouping algorithm"
        )

    @ staticmethod
    def _continuity_check(ROI: dict, min_centroids: int = 3,
                          continuity_min: int = 1,
                          intensity_cutoff: float = None):
        """
        Filter data in ROI which has less than min_centroids continuous values.
        In addition this method also verifies if the maximum intensity of a
        valid ROI is higher than the intensity_cutoff. This function modifies
        the self._ROI attribute. 

        Args:
            ROI (dict): A dictionary containing four keys named, mzmeans, mz,
                intensity, and scans. Where mzmeans contains a list with the
                average of each mz feature detected. The other keys of the
                dictionary are a list of lists containing the information of
                the detected features.
            min_centroids (int): minimum number of consecutive points to 
                consider a feature as a ROI. For defining this value it
                could be helpful to know the minimum chromatographic 
                peak width.
            continuity_min (int): minimum number of scans to check if a ROI is
                taken as one or if it is splitted due to this minimum 
                separation. It has a default value of 1, if it is not set. A 
                value of 1 means that if a ROI is not continue in the scan 
                order it will be splitted. A value of two implies that if a 
                discontinuity of one scan exists, it is negligeable, so the ROI
                is not splitted, even with this void.
            intensity_cutoff (float): is a filter constrain, if defined, the 
                method  will search in all features if there is an intensity
                higher than this cutoff, if yes then it keeps the feature, if 
                not, the feature is deleted.
        """

        if continuity_min <= 0:
            raise ValueError(
                f'The argument continuity_min should be greater than 0 '
                f'not: {continuity_min}')

        mzroi = []
        intens_roi = []
        scan_roi = []
        means_roi = []
        for j, roi_mz in enumerate(ROI['mz']):
            mean = ROI['mzmeans'][j]
            roi_scans = ROI['scans'][j]
            roi_intensity = ROI['intensity'][j]
            group_scans = [[roi_scans[0]]]
            group_values = [[roi_mz[0]]]
            group_intensity = [[roi_intensity[0]]]
            count = 0
            for i in range(len(roi_scans) - 1):
                condition1 = (roi_scans[i + 1] -
                              roi_scans[i] <= continuity_min)
                if condition1:
                    condition2 = (roi_scans[i + 1] - roi_scans[i] == 0)
                    if condition2:
                        err_l = [abs(roi_mz[i] - mean),
                                 abs(roi_mz[i + 1] - mean)]
                        pos = err_l.index(min(err_l))
                        group_values[count][-1] = roi_mz[i + pos]
                        group_intensity[count][-1] = roi_intensity[i + pos]
                    else:
                        group_scans[count].append(roi_scans[i + 1])
                        group_values[count].append(roi_mz[i + 1])
                        group_intensity[count].append(roi_intensity[i + 1])
                else:
                    condition3 = len(group_scans[count]) >= min_centroids
                    if intensity_cutoff is not None:
                        condition4 = len(
                            [*filter(lambda x: x >= intensity_cutoff, group_intensity[count])]) > 0
                    else:
                        condition4 = True
                    if condition3:
                        if condition4:
                            means_roi.append(np.mean(group_values[count]))
                            count += 1
                            group_scans.append([roi_scans[i + 1]])
                            group_values.append([roi_mz[i + 1]])
                            group_intensity.append([roi_intensity[i + 1]])
                    else:
                        group_scans[count] = [roi_scans[i + 1]]
                        group_values[count] = [roi_mz[i + 1]]
                        group_intensity[count] = [roi_intensity[i + 1]]
            means_roi.append(np.mean(group_values[count]))
            condition5 = len(group_scans[count]) <= min_centroids
            if intensity_cutoff is not None:
                condition6 = len(
                    [*filter(lambda x: x >= intensity_cutoff, group_intensity[count])]) <= 1
            else:
                condition6 = False
            if condition5:
                means_roi.pop(-1)
                group_scans.pop(count)
                group_values.pop(count)
                group_intensity.pop(count)
            elif condition6:
                means_roi.pop(-1)
                group_scans.pop(count)
                group_values.pop(count)
                group_intensity.pop(count)
            scan_roi.extend(group_scans)
            mzroi.extend(group_values)
            intens_roi.extend(group_intensity)

        return {
            'mzmeans': means_roi,
            'mz': mzroi,
            'intensity': intens_roi,
            'scans': scan_roi
            }

    @staticmethod
    def _get_n_values_in_ROI(ROI):
        """ Returns the number of elements per ROI detected.
        """
        n_values = list()
        for i in range(len(ROI['mz'])):
            n_values.append(len(ROI['mz'][i]))
        return n_values

    @staticmethod
    def _filter_centroids(ROI, min_centroids: int = 3):
        """ Filters the ROI with the min_centroids parameter
        """
        n_values = FeatureDetection._get_n_values_in_ROI(ROI)
        ROI_values, ROI_scan, ROI_intensity, ROI_mzmeans = [], [], [], []
        for i, n in enumerate(n_values):
            if n >= min_centroids:
                ROI_values.append(ROI['mz'][i])
                ROI_scan.append(ROI['scans'][i])
                ROI_intensity.append(ROI['intensity'][i])
                ROI_mzmeans.append(ROI['mzmeans'][i])
        return {
            'mzmeans': ROI_mzmeans,
            'mz': ROI_values,
            'intensity': ROI_intensity,
            'scans': ROI_scan}

    @staticmethod
    def _feature_detection_integer_part(
            lambda_parameter: float, df: pd.DataFrame, clusters: list,
            data: PeakListCollection, mode: str = None):
        """
        Feature Detection algorithm based on the concept of Tautenhahn et al., 
        BMC Bioinformatics 2008, including the clustering approach by m/z
        integer part to speed up the code.

        Args:
            lambda_parameter (int): value in ppm for the mass accuracy of
                the mass spectra. It has a default value of 10 ppm.
            df (pandas.DataFrame): data frame with the concatenated data
                of all scans.
            data (PeakListCollection): the raw data.
            clusters (list): list of masses where peaks are found in the
                range of mz.
            mode (str): if mode is `'boundaries` the algorithm search
                features only in the integer_part values.

        Returns:
            A dictionary with the Regions of Interest information.
        """
        list_of_integer_mz = clusters
        if mode == 'boundaries':
            if df is None:
                raise ValueError('A data frame containing all the concatenated '
                                 'data must be passed throught the arguments.s')
            # capture the points for all boundaries
            list_of_points = list()
            list_of_integer_mz = list()
            for val in clusters:
                idx = np.where(np.isclose(df.mz.values, val, rtol=5e-6))[0]
                if len(idx) > 0:
                    list_of_integer_mz.append(val)
                    list_of_points.append(df.iloc[idx, :])
        
        # a majorant value
        mz_roof = np.ceil(data.get_min_max_mz()[1])

        roi_values = []  # list of mz values by ROI
        roi_means = []  # means of mz in a ROI
        roi_intens = []  # intensity of peaks in a ROI
        roi_scans = []  # scans index in the ROI

        for j, mz_integer_part in enumerate(list_of_integer_mz):
            if mode == 'boundaries':
                df_nm = list_of_points[j]
            else:
                df_nm = df.get_group(mz_integer_part)
            mz_per_scan = []
            intensity = []
            scans = []
            for __, temp in df_nm.groupby(by='scan'):
                mz_per_scan.append(list(temp.mz.values))
                intensity.append(list(temp.intensity.values))
                scans.append(list(temp.scan.values))

            # ROI initialization with scan 0, type = list
            ROI_mzmeans = copy.deepcopy(mz_per_scan[0])
            # this roof is add to avoid a StopIteration Error
            ROI_mzmeans.append(mz_roof)
            ROI_values = [[mz] for mz in mz_per_scan[0]]
            ROI_intensity = [[inten] for inten in intensity[0]]
            ROI_scan = [[scan] for scan in scans[0]]

            # iterating over all scans from 1
            for scan_idx, mz_scan in enumerate(mz_per_scan[1:], 1):
                istart = 0
                for idx_mz, mz in enumerate(mz_scan):
                    # The algorithm is based on the fact that mz values
                    # are initially sorted in the peaklists. We take only
                    # to the first above mz_mean and consider the previous
                    # value (ant) and the above value (pnt)
                    roi_mz_mean = next(mz_mean for mz_mean in ROI_mzmeans[istart:]
                                       if mz_mean > mz)
                    roi_idx_above = ROI_mzmeans.index(roi_mz_mean)
                    if roi_idx_above > 0:
                        ant = abs(ROI_mzmeans[roi_idx_above - 1] - mz)
                        pnt = abs(roi_mz_mean - mz)
                        pos = [pnt, ant].index(min([pnt, ant]))
                        roi_idx_closest = roi_idx_above - pos
                        roi_mz_mean = ROI_mzmeans[roi_idx_closest]
                    else:
                        roi_idx_closest = roi_idx_above
                    if roi_mz_mean == mz_roof:  # TODO: correct the max limit
                        roi_mz_mean = ROI_mzmeans[roi_idx_closest - 1]
                        roi_idx_closest -= 1
                    accept_mz = (abs(mz - roi_mz_mean) / roi_mz_mean * 1e6 <= lambda_parameter)
                    # condition2 = abs(scans[s][i] - ROI_scan[idx_closest][-1]) > 0
                    scan_exist = scans[scan_idx][idx_mz] in ROI_scan[roi_idx_closest]
                    if accept_mz:
                        if scan_exist:
                            # scan index already exist in the selected ROI
                            # as we go scan by scan it means that it is the last
                            # values in the ROI.
                            # keep only the closest to the mz_mean
                            last_roi_mz = ROI_values[roi_idx_closest][-1]
                            ant = abs(roi_mz_mean - last_roi_mz)
                            pnt = abs(roi_mz_mean - mz)  # current mz
                            jdx = [pnt, ant].index(min([pnt, ant]))
                            mz_keep = [mz, last_roi_mz][jdx]
                            intens = [intensity[scan_idx][idx_mz],
                                      ROI_intensity[roi_idx_closest][-1]][jdx]
                            ROI_values[roi_idx_closest][-1] = mz_keep
                            # re-compute mz mean for this roi
                            ROI_mzmeans[roi_idx_closest] = np.mean(ROI_values[roi_idx_closest])
                            ROI_intensity[roi_idx_closest][-1] = intens
                        else:
                            # append to the selected ROI the data
                            ROI_values[roi_idx_closest].append(mz)
                            ROI_mzmeans[roi_idx_closest] = np.mean(ROI_values[roi_idx_closest])
                            ROI_intensity[roi_idx_closest].append(intensity[scan_idx][idx_mz])
                            ROI_scan[roi_idx_closest].append(scans[scan_idx][idx_mz])
                    else:
                        # insert a new ROI at roi_idx_above to keep ROI
                        # sorted by mz.
                        ROI_values.insert(roi_idx_above, [mz])
                        ROI_mzmeans.insert(roi_idx_above, mz)
                        ROI_intensity.insert(roi_idx_above, [intensity[scan_idx][idx_mz]])
                        ROI_scan.insert(roi_idx_above, [scans[scan_idx][idx_mz]])

                    # for next iteration start from idx_above
                    istart = roi_idx_above

            ROI_mzmeans.remove(mz_roof)
            roi_values.extend(ROI_values)
            roi_means.extend(ROI_mzmeans)
            roi_intens.extend(ROI_intensity)
            roi_scans.extend(ROI_scan)
        
        return {
            'mzmeans': roi_means,
            'mz': roi_values,
            'intensity': roi_intens,
            'scans': roi_scans
            }

    def _feature_detection(self, lambda_parameter):
        """
        Feature Detection algorithm based on the concept of Tautenhahn et al., 
        BMC Bioinformatics 2008.

        Args:
            lambda_parameter (int): value in ppm for the mass accuracy of the mass 
                spectra. It has a default value of 10 ppm.

        Returns
            A dictionary with the Regions of Interest information.
        """

        # ROI initialization with the first scan, type = list
        mz_lists = [pl.mz.tolist() for pl in self._data]
        intens_lists = [pl.intensity.tolist() for pl in self._data]
        ROI_mzmeans = mz_lists[0].copy()

        min_mz, max_mz = self._data.get_min_max_mz()
        mz_roof = np.ceil(max_mz)

        # this is required to avoid errors due to the largest value on
        # the different scan
        ROI_mzmeans.append(mz_roof)

        # initialization of ROI from values of scan 0
        ROI_values = [[m] for m in mz_lists[0]]
        ROI_values.append([mz_roof])
        ROI_intensity = [[intens] for intens in intens_lists[0]]
        ROI_intensity.append([0.0])
        ROI_scan = [[0] for _ in range(len(mz_lists[0]))]
        ROI_scan.append([0])

        # iterating in all scans
        for s, mz_list in enumerate(mz_lists[1:], 1):
            for i, xs in enumerate(mz_list):
                roi_val = next(x for x in ROI_mzmeans if (x > xs))
                idx = ROI_mzmeans.index(roi_val)
                if idx > 0:
                    ant = abs(ROI_mzmeans[idx - 1] - xs)
                    pnt = abs(roi_val - xs)
                    pos = [pnt, ant].index(min([pnt, ant]))
                    idx = idx - pos
                    roi_val = ROI_mzmeans[idx]
                else:
                    pos = 0
                condition = (abs(xs - roi_val) / roi_val * 1e6 <= lambda_parameter)
                if condition:
                    if not abs(s - ROI_scan[idx][-1]) > 0:
                        ant = abs(roi_val - ROI_values[idx][-1])
                        pnt = abs(roi_val - xs)
                        jdx = [pnt, ant].index(min([pnt, ant]))
                        xs = [xs, ROI_values[idx][-1]][jdx]
                        inten = [intens_lists[s][i],
                                 ROI_intensity[idx][-1]][jdx]
                        ROI_values[idx][-1] = xs
                        ROI_mzmeans[idx] = np.mean(ROI_values[idx])
                        ROI_intensity[idx][-1] = inten
                    else:
                        ROI_values[idx].append(xs)
                        ROI_mzmeans[idx] = (roi_val + xs) / 2
                        ROI_mzmeans[idx] = np.mean(ROI_values[idx])
                        ROI_intensity[idx].append(intens_lists[s][i])
                        ROI_scan[idx].append(s)

                else:
                    ROI_values.insert(idx + pos, [xs])
                    ROI_mzmeans.insert(idx + pos, xs)
                    ROI_intensity.insert(idx + pos, [intens_lists[s][i]])
                    ROI_scan.insert(idx + pos, [s])

        return {
            'mzmeans': ROI_mzmeans,
            'mz': ROI_values,
            'intensity': ROI_intensity,
            'scans': ROI_scan
            }


# This class executes the pattern recognition using the _kmeans_detection
# method. One particularity is that the k_means and ckmeans methods return
# labels to sort data on buckets. Consequently, the post-processing methods
# implemented in this class to validate the continuity_min and the min_centroids
# constrains they are characteristic to manage the way in which data is
# represented here.


class KmeansDetection:
    """
    Kmeans Algorithm which uses the clustering approach by m/z integer part.
    This class contains all the methods to execute a Kmeans algorithm using
    sklearn.cluster.k_means or ckwrap.ckmeans.

    .. attribute: algorithm

        Is a keyword argument of sklearn.cluster.k_means use to specify
        the type of algorithm that the method uses.

    .. attribute: model

        This parameter defines which kmeans clustering method will be
        employed to perform the feature detection. It could be 'ckmeans'
        or 'sklearn'.

    .. attribute: clusters

        Is a list which contains the values of existing m/z integer part.
        The term cluster is used here to define the regions of m/z integer part
        where is possible to find peaks among the time axis.
    """

    def __init__(self, raw_data: PeakListCollection,
                 min_centroids: int = 2,
                 lambda_parameter: float = 3.0,
                 continuity_min: int = 1,
                 clustering_model: str = 'ckmeans',
                 sklearn_algorithm: str = None,
                 check_boundaries: bool = False):
        """
        Initialize the KmeansDetection class.

        Args:
            raw_data (pyc2mc.core.nddata.HyphenantedMassSpecData): object 
                containing the lists of masses and intensities from the 
                time-resolved data (scans).
            min_centroids (int): minimum number of consecutive points to 
                consider a feature as a ROI. For defining this value it could 
                be helpful to know the minimum chromatographic peak width.
            lambda_parameter (float): this value is the mass accuracy of the mass 
                spectra, it is given in ppm. It has a default value of 10 ppm.
            continuity_min (int): minimum number of scans to check if a ROI is
                taken as one or if it is splitted due to this minimum 
                separation. It has a default value of 1, if it is not set. A 
                value of 1 means that if a ROI is not continue in the scan 
                order it will be splitted. A value of two implies that if a 
                disonctinuity of one scan exists, it is negligable, so the ROI
                is not splitted, even with this void.
            clustering_model (str): options 'ckmeans' for using ckwarp.ckmeans
                methodology as implemented by Wang & Song (2011), or 'sklearn'
                to use the sklearn.cluster.k_means approach.
            sklearn_algorithm (str): {"auto", "full", "elkan"}, default="auto"
                K-means algorithm to use. See more information in sklearn.cluster
                for the k_means function.
            check_boundaries (bool): if True, the algorithm will search on 
                each exact integer_part mz, to look if there are some features
                lost due to the groupby_integer_part approach. 
        """
        # 1. Test the inputs
        if sklearn_algorithm is None:
            self.algorithm = 'auto'
        else:
            self.algorithm = sklearn_algorithm

        if self.algorithm not in ["auto", "full", "elkan"]:
            raise TypeError(
                'Algorithm are not in the available options of sklearn.')

        if clustering_model not in ['ckmeans', 'sklearn']:
            raise TypeError('Models are not in the available options.')
        if not CKWRAP_AVAILABLE:
            clustering_model = "sklearn"
            message = "ckwrap is not availabe. Scikit-learn will be used"
            warnings.warn(message, UserWarning, stacklevel=2)

        # 2. Instantiate the input information
        self._data = raw_data
        self.model = clustering_model  # model name
        self._pmin = min_centroids
        self._lambda_parameter = lambda_parameter
        self._continuity_min = continuity_min
        self._check_boundaries = check_boundaries

        # 3. set up the groups by m/z integer part in a data frame
        list_intens = list()
        list_mz = list()
        list_scan = list()
        for s, pl in enumerate(self._data):
            if len(pl) == 0:
                # avoid error if pl is empty for one scan
                continue
            scan = [s] * len(pl)
            list_scan.extend(scan)
            list_intens.extend(pl.values[:, 1])
            list_mz.extend(pl.values[:, 0])

        df = pd.DataFrame(
            {'mz': list_mz, 'intensity': list_intens, 'scan': list_scan})
        df['integer_part'] = np.floor(df.mz).astype(np.int32)
        self._data_grouped_nm = df.groupby(by="integer_part")
        self.clusters = list(set((df.integer_part.values)))

        # save temporary df with the concatenated data this will be further
        # deleted.
        self._df = df
        del df

    def run(self, min_centroids: int = None,
            lambda_parameter: float = None,
            continuity_min: int = None,
            clustering_model: str = None,
            sklearn_algorithm: str = None,
            check_boundaries: bool = None):
        """
        Kmeans Algorithm which uses the clustering approach by m/z integer part.
        This class contains all the methods to execute a Kmeans algorithm using
        sklearn.cluster.k_means or ckwrap.ckmeans.

        Args:
            min_centroids (int): minimum number of consecutive points to 
                consider a feature as a ROI. For defining this value it could 
                be helpful to know the minimum chromatographic peak width.
            lambda_parameter (float): this value is the mass accuracy of the mass 
                spectra, it is given in ppm. It has a default value of 10 ppm.
            continuity_min (int): minimum number of scans to check if a ROI is
                taken as one or if it is splitted due to this minimum 
                separation. It has a default value of 1, if it is not set. A 
                value of 1 means that if a ROI is not continue in the scan 
                order it will be splitted. A value of two implies that if a 
                disonctinuity of one scan exists, it is negligable, so the ROI
                is not splitted, even with this void.
            clustering_model (str): options 'ckmeans' for using ckwarp.ckmeans
                methodology as implemented by Wang & Song (2011), or 'sklearn'
                to use the sklearn.cluster.k_means approach.
            sklearn_algorithm (str): {"auto", "full", "elkan"}, default="auto"
                K-means algorithm to use. See more information in sklearn.cluster
                for the k_means function.
            check_boundaries (bool): if True, the algorithm will search on 
                each exact integer_part mz, to look if there are some features
                lost due to the groupby_integer_part approach.
        
        Returns:
            A ROIList object containing the regions of interest detected.
        """
        if sklearn_algorithm is None:
            sklearn_algorithm = self.algorithm
        if clustering_model is None:
            clustering_model = self.model

        # test the redefined inputs to verify the consistency.
        if sklearn_algorithm not in ["auto", "full", "elkan"]:
            raise TypeError(
                'Algorithm are not in the available options of sklearn.')

        if clustering_model not in ['ckmeans', 'sklearn']:
            raise TypeError('Models are not in the available options.')
        if not CKWRAP_AVAILABLE:
            clustering_model = "sklearn"
            message = "ckwrap is not availabe. Scikit-learn will be used"
            warnings.warn(message, UserWarning, stacklevel=2)

        # Re-instantiate the input data if it is redefined.
        if min_centroids is not None:
            self._pmin = min_centroids
        if lambda_parameter is not None:
            self._lambda_parameter = lambda_parameter
        if continuity_min is not None:
            self._continuity_min = continuity_min
        if clustering_model is not None:
            self.model = clustering_model
        if sklearn_algorithm is not None:
            self.algorithm = sklearn_algorithm
        if check_boundaries is not None:
            self._check_boundaries = check_boundaries

        # 1. Run the kmeans detection method. Herein, the continuity check and
        # the filter centroids methods are implemented.
        ROI = self._kmeans_detection()

        # 2. As the method groups the data by integer_part of mz, the data is
        # normally splitted in blocks of nominal masses. In this case, some
        # Features located in the boundaries of m/z integer part could be 
        # partitioned by error. If so, The check_boundaries condition is
        # intended to recover this splitted ROIs and unifying them into new
        # ones.
        if self._check_boundaries:
            ROI_boundaries = FeatureDetection._feature_detection_integer_part(
                lambda_parameter=self._lambda_parameter,
                df=self._df,
                clusters=self.clusters,
                data=self._data,
                mode='boundaries')
            del self._df
            ROI_boundaries = FeatureDetection._continuity_check(
                ROI=ROI_boundaries,
                min_centroids=self._pmin,
                continuity_min=self._continuity_min
            )
            ROI_boundaries = FeatureDetection._filter_centroids(
                ROI_boundaries, self._pmin)
            
            # 2.1. match the general ROI and ROI_boundaries
            ROI = unifying_rois(
                ROI, ROI_boundaries, self._lambda_parameter)

        # save the final ROI dictionary as a private attribute
        self._ROI = ROI

        return pyc2mc.core.roi.ROIList(
            scans=ROI["scans"],
            mz=ROI["mz"],
            intensity=ROI["intensity"],
            name="ROIs from integer part grouping algorithm"
        )

    def _filtering_buckets(self, buckets_mz, buckets_intensity, buckets_scans):
        """
        Sort features with a minimum length of self._pmin (min_centroids).

        Args:
            buckets_mz (list-like): list that contains mz data.
            buckets_intensity (list-like): list that contains intensities data.
            buckets_scans (list-like): list that contains scans data.
        """
        bukfinalmz = list()
        bukfinalinten = list()
        bukfinalscans = list()
        for i, buk in enumerate(buckets_mz):
            if len(buk) >= self._pmin:
                bukfinalmz.append(buk)
                bukfinalinten.append(buckets_intensity[i])
                bukfinalscans.append(buckets_scans[i])
        return bukfinalmz, bukfinalinten, bukfinalscans

    def _window_check(self, mzroi, intens_roi, scan_roi):
        """
        Sort clusters from their mz. This method separates features that 
        could be saved as a single cluster and separate them using the means of 
        each feature.
        Ratio between the mz and the means of the feature should be less than 
        lambda_parameter.

        Args:
            mzroi (list-like): list that contains mz data.
            intens_roi (list-like): list that contains intensities data.
            scan_roi (list-like): list that contains scans data.
        """
        list_temp_mz_final = []
        list_temp_scans_final = []
        list_temp_intens_final = []
        for j, mz_j in enumerate(mzroi):
            count = 0
            list_temp_mz = [[mz_j[0]]]
            list_temp_scans = [[scan_roi[j][0]]]
            list_temp_intens = [[intens_roi[j][0]]]
            means_mz = [mz_j[0]]

            for i in range(len(mzroi[j]) - 1):
                val, idx = find_nearest(means_mz, mz_j[i + 1])
                condition = (
                    (abs(mz_j[i + 1] - val) / mz_j[i + 1]) * 1e6 < self._lambda_parameter)
                if condition:
                    list_temp_mz[idx].append(mz_j[i + 1])
                    list_temp_scans[idx].append(scan_roi[j][i + 1])
                    list_temp_intens[idx].append(intens_roi[j][i + 1])
                    means_mz[idx] = (means_mz[idx] + mz_j[i + 1]) / 2
                else:
                    count += 1
                    list_temp_mz.append([mz_j[i + 1]])
                    list_temp_scans.append([scan_roi[j][i + 1]])
                    list_temp_intens.append([intens_roi[j][i + 1]])
                    means_mz.append(mz_j[i + 1])

            for k in range(len(list_temp_mz)):
                if len(list_temp_mz[k]) > self._pmin:
                    list_temp_mz_final.append(list_temp_mz[k])
                    list_temp_scans_final.append(list_temp_scans[k])
                    list_temp_intens_final.append(list_temp_intens[k])
        return list_temp_mz_final, list_temp_intens_final, list_temp_scans_final

    def _continuity_kmeans(self, buckets_mz, buckets_intensity, buckets_scans):
        """
        Filter data in ROI which has less than min_centroids continuous values.
        In addition this method erase duplicates and calculate means of each feature.

        Args:
            buckets_mz (list-like): list that contains mz data.
            buckets_intensity(list-like): list that contains intensities data.
            buckets_scans(list-like) : list that contains scans data.
        """
        roi_scans = []
        roi_mz = []
        roi_intensity = []
        mzroi = []
        intens_roi = []
        scan_roi = []
        means_roi = []

        for j, roi_mz in enumerate(buckets_mz):
            argsort = np.argsort(buckets_scans[j])
            roi_scans = [buckets_scans[j][idx] for idx in argsort]
            roi_mz = [buckets_mz[j][idx] for idx in argsort]
            mean = np.mean(roi_mz)
            roi_intensity = [buckets_intensity[j][idx] for idx in argsort]
            group_scans = [[roi_scans[0]]]
            group_values = [[roi_mz[0]]]
            group_intensity = [[roi_intensity[0]]]
            count = 0

            for i in range(len(roi_mz) - 1):
                condition1 = (
                    abs(roi_scans[i + 1] - roi_scans[i]) <= self._continuity_min)
                if condition1:

                    condition2 = (roi_scans[i + 1] - roi_scans[i] == 0)
                    if condition2:
                        err_l = [abs(roi_mz[i] - mean), abs(roi_mz[i + 1] - mean)]
                        pos = err_l.index(min(err_l))
                        group_values[count][-1] = roi_mz[i + pos]
                        group_intensity[count][-1] = roi_intensity[i + pos]
                    else:
                        group_scans[count].append(roi_scans[i + 1])
                        group_values[count].append(roi_mz[i + 1])
                        group_intensity[count].append(roi_intensity[i + 1])
                else:
                    condition3 = len(group_scans[count]) >= self._pmin
                    if condition3:
                        means_roi.append(np.mean(group_values[count]))
                        count += 1
                        group_scans.append([roi_scans[i + 1]])
                        group_values.append([roi_mz[i + 1]])
                        group_intensity.append([roi_intensity[i + 1]])
                    else:
                        group_scans[count] = [roi_scans[i + 1]]
                        group_values[count] = [roi_mz[i + 1]]
                        group_intensity[count] = [roi_intensity[i + 1]]

            means_roi.append(np.mean(group_values[count]))
            condition5 = len(group_scans[count]) <= self._pmin
            if condition5:
                means_roi.pop(-1)
                group_scans.pop(count)
                group_values.pop(count)
                group_intensity.pop(count)

            scan_roi.extend(group_scans)
            mzroi.extend(group_values)
            intens_roi.extend(group_intensity)

        return mzroi, intens_roi, scan_roi

    def _kmeans_detection(self):
        """
        KMeans Detection method.
        This method start to separate clusters using the integer part of m/z. 
        Thus, a cluster is an existing value of integer part mass where is 
        possible to find islands of peaks among the time axis.
        For each cluster, the method k_means or ckmeans are launched and
        return labels to sort data on buckets.
        It is necessary to post-processing the obtained buckets because some
        features could not be well separated. For that reason, the _window_check
        method is executed after the clustering, to validate that the grouped
        buckets respect the window or lambda_parameter constrain. If not, the method
        splits the bucket into the correct features. The resulting buckets
        are cleaned up when _continuity_check and _filtering_buckets are
        excetued to validate the continuity_min and min_centroids constrains.
        """

        roi_values = []
        roi_intens = []
        roi_scans = []
        roi_means = []

        for i, integer_part in enumerate(self.clusters):
            df_nm = self._data_grouped_nm.get_group(integer_part)
            mz_i = df_nm.mz.values
            intensity_i = df_nm.intensity.values
            scans_i = df_nm.scan.values
            num = int(df_nm.groupby(by='scan').count().max().mz)
            if self.model == 'sklearn':
                mz_to_kmeans = mz_i.reshape(1, -1).T
                km_centroid, km_label, __ = k_means(
                    X=mz_to_kmeans,
                    n_clusters=num,
                    random_state=1,
                    algorithm=self.algorithm)
            elif self.model == 'ckmeans':
                km = ckmeans(mz_i, num)
                km_label = km.labels

            buckets_intensity = []
            buckets_mz = []
            buckets_scans = []
            for i in range(num):
                buckets_intensity.append([])
                buckets_mz.append([])
                buckets_scans.append([])
            for i in range(len(mz_i)):
                buckets_intensity[km_label[i]].append(intensity_i[i])
                buckets_mz[km_label[i]].append(mz_i[i])
                buckets_scans[km_label[i]].append(scans_i[i])
            buckets_means = []
            for i in range(len(buckets_mz)):
                buckets_means.append(np.mean(buckets_mz[i]))
            argsort = np.argsort(buckets_means)
            buckets_means = [buckets_means[idx] for idx in argsort]
            buckets_intensity = [buckets_intensity[idx] for idx in argsort]
            buckets_mz = [buckets_mz[idx] for idx in argsort]
            buckets_scans = [buckets_scans[idx] for idx in argsort]

            buckets_mz, buckets_intensity, buckets_scans = self._filtering_buckets(
                buckets_mz,
                buckets_intensity,
                buckets_scans
            )
            buckets_mz, buckets_intensity, buckets_scans = self._window_check(
                buckets_mz,
                buckets_intensity,
                buckets_scans
            )
            buckets_mz, buckets_intensity, buckets_scans = self._continuity_kmeans(
                buckets_mz,
                buckets_intensity,
                buckets_scans
            )
            buckets_mz, buckets_intensity, buckets_scans = self._filtering_buckets(
                buckets_mz,
                buckets_intensity,
                buckets_scans
            )

            buckets_means = []
            for i in range(len(buckets_mz)):
                buckets_means.append(np.mean(buckets_mz[i]))

            roi_values.extend(buckets_mz)
            roi_means.extend(buckets_means)
            roi_intens.extend(buckets_intensity)
            roi_scans.extend(buckets_scans)

        ROI = {'mzmeans': roi_means,
               'mz': roi_values,
               'intensity': roi_intens,
               'scans': roi_scans}

        return ROI
