# coding: utf-8

""" This module implements a class in order to explore mz differences
in a peak list. """

from time import time
from typing import Sequence, Tuple
from numpy.typing import ArrayLike

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from pyc2mc.core.isotopes import Isotope
from pyc2mc.core import formula
from pyc2mc.core.peak import PeakList
from pyc2mc.processing.sparse_histogram import sparse_histogram

__all__ = ["MassDifferences"]

# main building blocks for identification from mz shift
MAIN_BUILDING_BLOCKS = [
    "C H2", "C H", "N H", "N H2", "O H", "O H2", "C2 H4", "C H O", "C O",
    "F H", "O", "C", "N", "B", "S", "S H", "H",
]

# main elements for identification from isotopic mz shift
ELEMENTS = ["C", "N", "O", "S", "B", "Cl"]


class MassDifferences:
    """ Class to compute and represent a m/z difference matrix. The m/z
    differences are compute by subtracting the most abundant peak to the
    less abundant peak.

    Attributes:
        peaklist (PeakList): The peaklist from which mz differences are
            computed.
        mz (array): The m/z values from the input peaklist
        mz_diff (array): the mass differences array

    """
    
    def __init__(
        self,
        peaklist: PeakList,
        delta_mz_bounds: Tuple[float, float],
        sort: bool = False,
    ):
        """ Initialisation of the class 
        
        Args:
            peaklist (PeakList): A peak list including intensities
            delta_mz_bounds (float, float): mass difference interval to be
                considered for populating the mass differences array.

        """
        
        self.peaklist = peaklist
        self.delta_mz_bounds = delta_mz_bounds

        self.mz = peaklist.mz
        self.mz_diff = np.array([])
        self._indices = np.array([])
        self.is_sorted = sort
        self._compute_differences()

    def _compute_differences(self):
        """ Compute mz differences """

        mass_differences = list()
        mass_differences_idx = [[], []]
        md_range = np.array(self.delta_mz_bounds)

        start0 = time()
        start = time()
        for idx in range(len(self.peaklist)):
            if idx % int(len(self.peaklist) / 10) == 0:
                end = time()
                print(f"{idx:5d} {idx / len(self.peaklist) * 100:5.1f}% -- {end - start:5.1f} s")
                start = end
            md_i = self.peaklist.mz[idx + 1:] - self.peaklist.mz[idx]
        
            i_start, i_end = np.searchsorted(md_i, md_range)
            mass_differences.append(md_i[i_start: i_end])
            
            jdx = np.arange(i_start, i_end, dtype=np.uint16)

            mass_differences_idx[0].append(np.full(jdx.shape, idx))
            mass_differences_idx[1].append(jdx + idx + 1)

        # transform to numpy arrays and concatenate all  
        mass_differences_idx[0] = np.concatenate(mass_differences_idx[0])
        mass_differences_idx[1] = np.concatenate(mass_differences_idx[1])
        mass_differences = np.concatenate(mass_differences, dtype=np.float64)
        mass_differences_idx = np.vstack(mass_differences_idx)

    
        if self.is_sorted:
            # Sort mz_differences and adjust mz_differences_idx accordingly
            idx_start = mass_differences_idx[0]
            idx_end = mass_differences_idx[1]
            differences = mass_differences
            
            # Sort differences and get the indices of the sorted order
            sorted_indices = np.argsort(differences)
            
            # Adjust idx_start and idx_end according to the sorted order
            idx_start_sorted = idx_start[sorted_indices]
            idx_end_sorted = idx_end[sorted_indices]
            differences_sorted = differences[sorted_indices]
            
            # Update mz_differences_idx and mz_differences
            mass_differences_idx[0] = idx_start_sorted
            mass_differences_idx[1] = idx_end_sorted
            mass_differences = differences_sorted

        print(f"Done {time() - start0:.1f}s")
        self.mz_diff = mass_differences
        self._indices = mass_differences_idx

    def get_mz_diff_in_range(
        self,
        delta_mz_bounds: tuple = (13., 30.),
        return_indices: bool = False,
    ) -> ArrayLike:
        """ Extract from all calculated mz differences the array of
        mz differences in the input range.
        
        Args:
            delta_mz_bounds (tuple): min and max values of mz differences
            return_indices (bool): if True, return also the indices of mz
                values in the original peaklist in the delta mz range.

        Returns:
            mz_diff (ArrayLike): mz differences in the give range
            indices (ArrayLike):  (i, j) pairs of indices in the original
                peaklist which leads to the mz differences.

        """
        vmin, vmax = delta_mz_bounds
        mask, = ((self.mz_diff > vmin) & (self.mz_diff < vmax)).nonzero()
        if return_indices:
            return self.mz_diff[mask], (self._indices[0][mask],
                                        self._indices[1][mask])
        else:
            return self.mz_diff[mask]
        
    def get_mz_diff_error_in_range(
        self,
        delta_mz_bounds: tuple = (13., 30.),
        lambda_parameter: float = 2,
        return_indices: bool = False,
    ) -> ArrayLike:
        """ Compute the error on the mz differences assuming a relative
        error lambda on the mz values. The calculation is done on the
        given mz range.

        If the relative error in ppm on mz is lambda_parameter,
        the absolute error on mz is lambda_parameter * 1e-6 * mz. 
        The error on mz_j - mz_i is thus the sum of each individual error.
        
        Args:
            delta_mz_bounds (tuple): min and max values of mz differences
            return_indices (bool): if True, return also the indices of mz
                values in the original peaklist in the delta mz range.

        Returns:
            mz_diff (ArrayLike): mz differences in the given range
            indices (ArrayLike):  (i, j) pairs of indices in the original
                peaklistw which leads to the mz differences.

        """
        _, (idx, jdx) = self.get_mz_diff_in_range(delta_mz_bounds, True)
        error = lambda_parameter * 1e-6 * (self.mz[idx] + self.mz[jdx])
        if return_indices:
            return error, (idx, jdx)
        else:
            return error

    def _get_target_list(
        self,
        delta_mz_bounds: tuple = (0, 30),
        repeat_units: Sequence[tuple] = None,
        building_blocks: Sequence = MAIN_BUILDING_BLOCKS,
        elements: Sequence = ELEMENTS,
        threshold: float = 1e-3,
    ) -> pd.DataFrame:
        """ Set up a list of tuple (mz, target_mz_diff)
        
        Args:
            delta_mz_bounds (tuple): min and max values of mz differences.
            repeat_units (list of tuple): list of tuple of a name of the
                repeat unit and the target mz difference.
            building_blocks (list): A list of formula to consider as a
                repeat units.
            elements (list): A list of elements. Mass shifts between all
                isotope pairs of this element are searched in the mz
                difference values.
            threshold (float): probability threshold to filter possible pairs
                of isotopes.
        
        Returns:
            target_list (list of tuple): List of (mz, target_mz_diff, color)

        """
        target_units = list()

        repeat_units = list() if repeat_units is None else repeat_units
        # look at mz value in the custom list
        for name, mz_diff, _ in repeat_units:
            if (mz_diff > delta_mz_bounds[0]) and (mz_diff < delta_mz_bounds[1]):
                target_units.append((name, mz_diff, "C1"))

        # look at isotopic mz shift to add on the plot
        elements = list() if elements is None else elements
        for element in elements:
            df = Isotope.get_mass_shifts(element, threshold=threshold)
            if len(df) == 0:
                continue
            for _, row in df.iterrows():
                mass_shift = np.abs(row.mass_shift)
                # TODO: warning here, mass_shift is a mass. This should be
                # devided by z in case we are looking at multicharged species

                if ((mass_shift > delta_mz_bounds[0]) and (
                        mass_shift < delta_mz_bounds[1])):
                    target_units.append((row.isotopes, mass_shift, "C2"))

        # filter building_blocks not in delta_mz_bounds
        building_blocks = list() if building_blocks is None else building_blocks
        for bb in building_blocks:
            bb = formula.get_formula(bb)
            if (bb.exact_mass > delta_mz_bounds[0] and bb.exact_mass < delta_mz_bounds[1]):
                target_units.append((str(bb), bb.exact_mass, "C3"))

        return target_units

    def get_target_stats(
        self,
        delta_mz_target: float,
        delta_mz_bounds: tuple = (13., 30.),
        method: str = "atol",
        atol: float = 1e-4,
        lambda_parameter: float = 2.0,
        return_indices: bool = False,
    ) -> dict:
        r""" Scan the m/z differences to gather statistics over the
        target m/z difference. The target m/z difference is supposed to be
        exact as it comes from a computed exact mass of the elements and
        isotopic shits.

        Two options are possible to decide if an experimental m/z difference
        correspond to the target m/z difference.

        * Assuming a relative error lambda on the m/z values, compute the error
          on the m/z difference and check if the m/z target is in this error
          range with a coverage factor k. This method is selected by
          ``method="rtol"``.
        * Assume a constant absolute error atol for all the m/z difference
          check if the m/z target is such as :math:`|target - m/z| < atol`. 
          This method is selected by ``method="atol"``.
        
        In order to count if the target mz difference
        is in the mz differences, the ``lambda_parameter`` is used to define
        a width around the mz differences.
        
        Args:
            delta_mz_target (float): target mz difference.
            delta_mz_bounds (tuple): min and max values of mz differences.
            method (str): choice of method to count m/z difference. Must be
                ``"atol"`` or ``"rtol"``.
            atol (float): absolute error width in Da, default 1e-4.
            lambda_parameter (float): coverage factor, default 2
            return_indices (bool): If True, return the indices in the 
                ``mz_diff`` vector that satisfy the criteria, default False.

        Returns:
            data (dict): a dictionary with the gathered data, such as:

                {
                    "count": X,
                    "percentage": X,  # over the number of mz in the interval
                    "m/z mean": ,  # average mz difference values
                    "m/z std": ,  # standard dev of mz difference values
                    "exact shift":  # target mz difference,
                    "error Da": ,  # between target and mean
                    "error ppm": ,  # between target and mean
                }
        

        """
        # select method
        if method.lower() not in ["atol", "rtol"]:
            raise ValueError(
                "Method must be 'atol' or 'rtol' but you set value: '{method}'.")
        method = method.lower()
    
        mz_diff_domain, (mz_idx, mz_jdx) = self.get_mz_diff_in_range(
            delta_mz_bounds, return_indices=True)

        if method == "atol":
            idx, = (np.abs(mz_diff_domain - delta_mz_target) < atol).nonzero()

        elif method == "rtol":
            error = lambda_parameter * 1e-6 * (self.mz[mz_idx] + self.mz[mz_jdx])
            idx, = (np.abs(mz_diff_domain - delta_mz_target) < error).nonzero()

        if len(idx) == 0:
            mz_diff_selected = np.array([])
            mz_mean = np.nan
            mz_std = np.nan
        else:
            mz_diff_selected = mz_diff_domain[idx]
            mz_mean = mz_diff_selected.mean()
            mz_std = mz_diff_selected.std()

        data = {
            "count": len(idx),
            "percentage": len(idx) / len(mz_diff_domain) * 100.,
            "m/z mean": mz_mean,
            "m/z std": mz_std,
            "exact shift": delta_mz_target,
            "error Da": (delta_mz_target - mz_mean),
            "error ppm": (delta_mz_target - mz_mean) / delta_mz_target * 1e6,
        }

        if return_indices:
            # the idx indices are in the filtered mz_diff_domain vector
            # mz_idx and mz_jdx are in the sorted peak list as done in setup
            return data, (mz_idx[idx], mz_jdx[idx])
        else:
            return data

    def get_shifts_abundance(
        self,
        delta_mz_bounds: tuple = (0, 30),
        method: str = "atol",
        atol: float = 1e-4,
        lambda_parameter: float = 2.0,
        repeat_units: Sequence[tuple] = None,
        building_blocks: Sequence = MAIN_BUILDING_BLOCKS,
        elements: Sequence = ELEMENTS,
        threshold: float = 1e-3,
    ) -> pd.DataFrame:
        """ For each building block, considered as a repeat unit, or isotopic
        shifts between two isotopes of an element, gather statistics over
        the list of mz differences. All data are gathered and returned as
        a DataFrame. See the method `get_target_stats` for more details.
        
        Args:
            delta_mz_bounds (tuple): min and max values of mz differences.
            method (str): choice of method to count m/z difference. Must be
                ``"atol"`` or ``"rtol"``.
            atol (float): absolute error width in Da, default 1e-4.
            lambda_parameter (float): coverage factor, default 2
            repeat_units (list of tuple): list of tuple of a name of the
                repeat unit and the target mz difference.
            building_blocks (list): A list of formula to consider as a
                repeat units.
            elements (list): A list of elements. Mass shifts between all
                isotope pairs of this element are searched in the mz
                difference values.
            threshold (float): probability threshold to filter possible pairs
                of isotopes.
        
        Returns:
            data (pd.DataFrame): Data gathered in a DataFrame. The table
                includes the name of the repeat unit, the number of times the
                target was found in the mz differences table, the percentage,
                the average and standard deviation of the mz differences, the
                exact mz difference, the error in Da and then in ppm.

        """
        # select method
        if method.lower() not in ["atol", "rtol"]:
            raise ValueError(
                "Method must be 'atol' or 'rtol' but you set value: '{method}'.")
        method = method.lower()

        target_units = self._get_target_list(
            delta_mz_bounds, repeat_units, building_blocks,
            elements, threshold)

        data = list()
        for name, target_mz_diff, _ in target_units:
            d = {"unit": name}
            res = self.get_target_stats(
                target_mz_diff, delta_mz_bounds, method, atol, lambda_parameter)
            if res["count"] > 0:
                d.update(res)
                data.append(d)
        
        df = pd.DataFrame(data)
        df = df.sort_values(by="count", ascending=False).reset_index(drop=True)
        return df

    def hist(
        self,
        delta_mz_bounds: tuple = (13., 30.),
        bin_size: float = 1e-2,
        density: bool = True,
        ax: plt.Axes = None,
    ):
        """ Plot as an histogramm the mass differences in a given range
        of delta m/z values.
        
        Args:
            delta_mz_bounds (tuple): min and max values of mz differences.
            bin_size (float): Histogram bin size, default 0.01.
            density (bool): If True, histogram is normalized.
            ax (Axes): a matplotlib axes to plot the data.
            max_points (int): Maximum number of points in the grid, default 1000.

        """
        if self.is_sorted:
            self.sparse_hist(delta_mz_bounds, bin_size, ax)
            return
        
        if not ax:
            ax = plt.subplot(1, 1, 1)

        vmin, vmax = delta_mz_bounds
        delta_mz = self.get_mz_diff_in_range(delta_mz_bounds)
        npts = int((vmax - vmin + 2 * bin_size) / bin_size) + 1
        # npts = min(max_points, npts + 1)
        bins = np.linspace(vmin - bin_size, vmax + bin_size, npts)
        ax.hist(delta_mz, rwidth=.9, bins=bins[:-1], density=density, zorder=2)

        ax.set_ylim(bottom=0.)
        ax.set_ylabel("Density")
        ax.set_xlabel(r"$\Delta(m/z)$ (Da)")
        ax.xaxis.set_tick_params(rotation=45)
    
    def sparse_hist(
        self,
        delta_mz_bounds: tuple = (13., 30.),
        bin_size: float = 1e-2,
        ax: plt.Axes = None,
    ):
        """ Plot as an histogramm the mass differences in a given range
        of delta m/z values.
        
        Args:
            delta_mz_bounds (tuple): min and max values of mz differences.
            bin_size (float): Histogram bin size, default 0.01.
            density (bool): If True, histogram is normalized.
            ax (Axes): a matplotlib axes to plot the data.
            max_points (int): Maximum number of points in the grid, default 1000.

        """
        if not self.is_sorted:
            raise RuntimeError("The mz differences are not sorted. ")
        
        if not ax:
            ax = plt.subplot(1, 1, 1)

        vmin, vmax = delta_mz_bounds
        delta_mz = self.get_mz_diff_in_range(delta_mz_bounds)
        print(delta_mz)
        x, y = sparse_histogram(delta_mz, bin_size=bin_size, zero_correction=False)
        ax.bar(x,y, width=0.9 * bin_size, zorder=2)
        ax.set_ylim(bottom=0.)
        ax.set_ylabel("# Ocurrences")
        ax.set_xlabel(r"$\Delta(m/z)$ (Da)")
        ax.xaxis.set_tick_params(rotation=45)


    def plot(
        self,
        delta_mz_bounds: tuple = (13., 30.),
        bin_size: float = 1e-2,
        density: bool = True,
        ax: plt.Axes = None,
        repeat_units: Sequence[tuple] = None,
        building_blocks: Sequence = MAIN_BUILDING_BLOCKS,
        elements: Sequence = ELEMENTS,
        threshold: float = 1e-3,
    ):
        """ Return a plot in a given range of m/z values to identify
        formula repeat units.
        
        Args:
            delta_mz_bounds (tuple): min and max values of mz differences.
            bin_size (float): width of gaussian functions, default 0.01
            density (bool): If True, KDE is normalized.
            ax (Axes): a matplotlib axes to plot the data.
            repeat_units (list of tuple): list of tuple of a name of the
                repeat unit and the target mz difference.
            building_blocks (list): A list of formula to consider as a
                repeat units.
            elements (list): A list of elements. Mass shifts between all
                isotope pairs of this element are searched in the mz
                difference values.
            threshold (float): probability threshold to filter possible pairs
                of isotopes.

        """
        if not ax:
            ax = plt.subplot(1, 1, 1)

        # target units (formula, isotopic shifts, others, to add on the plots)
        target_units = self._get_target_list(
            delta_mz_bounds, repeat_units, building_blocks,
            elements, threshold)

        self.hist(delta_mz_bounds, bin_size, density, ax)

        ax.set_ylim(bottom=0.)
        ax.set_ylabel("Density")
        ax.set_xlabel(r"$\Delta(m/z)$ (Da)")
        ax.xaxis.set_tick_params(rotation=45)
        
        for _, target_mz_diff, color in target_units:
            ax.axvline(target_mz_diff, color=color, linestyle=":",
                       alpha=1, zorder=3, linewidth=2)

        # add labels on top
        ax2 = ax.twiny()
        xticks = [mz for _, mz, _ in target_units]
        ax2.set_xticks(xticks)
        xticklabels = [name for name, _, _ in target_units] 
        ax2.set_xticklabels(xticklabels, rotation=75)
        ax2.set_xlim(ax.get_xlim())

    def plot_around_mz(
        self,
        delta_mz_target: float,
        width: float = 2,
        bin_size: float = 1e-2,
        density: bool = True,
        ax: plt.Axes = None,
        center: bool = False,
        **kwargs,
    ):
        """ Return a plot in a given range of m/z values to identify
        formula repeat units.
        
        Args:
            delta_mz_target (float): target mz difference.
            width (float): Plot width in dalton.
            delta_mz_bounds (tuple): min and max values of mz differences.
            bin_size (float): width of gaussian functions, default 0.01
            density (bool): If True, KDE is normalized.
            ax (Axes): a matplotlib axes to plot the data.

        """
        if not ax:
            ax = plt.subplot(1, 1, 1)

        delta_mz_bounds = (delta_mz_target - width, delta_mz_target + width)

        vmin, vmax = delta_mz_bounds
        delta_mz = self.get_mz_diff_in_range(delta_mz_bounds)
        npts = int((vmax - vmin) / bin_size) + 1
        if npts > 100_000:
            print("WARNINGS: number of bin is very high")
        if center:
            bins = np.linspace(-width, width, npts)
            ax.hist(delta_mz_target - delta_mz, rwidth=.9, bins=bins[:-1], 
                    density=density, zorder=1, **kwargs)
            ax.axvline(0, color="C3", linestyle="-", lw=3, zorder=2)
        else:
            bins = np.linspace(vmin - bin_size, vmax + bin_size, npts)
            ax.hist(delta_mz, rwidth=.9, bins=bins[:-1], density=density, zorder=1, **kwargs)
            ax.axvline(delta_mz_target, color="C3", linestyle="-", lw=3, zorder=2)

        ax.set_ylim(bottom=0.)
        ax.set_ylabel("Density")
        ax.set_xlabel(r"$\Delta(m/z)$ (Da)")
        ax.xaxis.set_tick_params(rotation=45)

        # add vertical line on mz target        
