#!/usr/bin/env python
# coding: utf-8

"""
This module contains classes and functions in order to 
proceed the recalibration on 1D data. Two different strategies are 
implemented. The first part provides classes in order to proceed to a
calibration using a predifined calibration list. You can look
at the :py:class:`pyc2mc.core.calibration.CalibList` class to know how to manage
calibration list and at the :py:mod:`pyc2mc.processing.caliblist_builder` module
to build a calibration list from Kendrick series. The second part provides
classes in order to proceed to a calibration from mass differences analysis.
This method does not rely on calibration lists.

Look at the 
:py:mod:`pyc2mc.processing.recalibration_models` for
more details about which recalibration model is available.

"""

from enum import Enum
from typing import Union, Tuple
import warnings
from collections.abc import Sequence

import numpy as np
from numpy.typing import ArrayLike

from pyc2mc.core.formula import get_formula
from pyc2mc.processing.mass_differences import MassDifferences
from pyc2mc.processing.recalibration_models import RecalibrationModels
from pyc2mc.core.calibration import CalibList
from pyc2mc.core.peak import Peak, AttributedPeak, PeakList
from pyc2mc.utils import gaussian_weights, step_weights


__all__ = ["Recalibration", "WalkingCalibration", "MassDifferencesCalibration"]


class RecalibrationMethods(str, Enum):
    """ define the different calibration methods """
    
    standard = "standard"
    walking = "walking"
    mass_differences = "mass_differences"

    def __call__(self, *args, **kwargs):
        """ Return the associated calibration object """

        if self.value == RecalibrationMethods.standard:
            return Recalibration(*args, **kwargs)
        
        elif self.value == RecalibrationMethods.walking:
            return WalkingCalibration(*args, **kwargs)
        
        elif self.value == RecalibrationMethods.mass_differences:
            return MassDifferencesCalibration(*args, **kwargs)


class BaseRecalibration:
    """
    This class is a base class for recalibration work. It contains the 
    generic methods to check and store the data and compute properties when
    calibration is done. This class is not supposed to be instantiated.
    """
    
    def __init__(
        self,
        calibration_list: CalibList, 
        model: Union[RecalibrationModels, str] = 'auto',
        fit_intercept: bool = True,
        deg: int = 2,
        verbose: bool = False,
        use_weights: bool = False,
    ):
        """
        Instance method for the recalibration.
        
        Args:
            calibration_list (CalibList): A CalibList object containing
                theoretical and experimental m/z values.
            model (int, str): It defines the type of regression to perform:
                for polynomials set 'polynomial' and the degree (see 'deg').
                Accepted values are 'ledford' for the Ledford equation and 
                'masselon', for the Masselon equation. If `auto`, the
                model fits to a linear model.
            fit_intercept (bool): if False set intercept to zero, otherwise,
                the intercept will be determined by the best fit curve.
            deg (int): If a polynomial model is selected, define the
                degree of the polynomial. Default is 2.
            verbose (bool): In case of absence of regression displays a warning
                message.
            use_weights (bool): If True use the weights defined in the 
                calibration list to perform the recalibration. Default False.

        """
            
        if isinstance(calibration_list, CalibList):
            self._calibration_list = calibration_list
        else:
            raise TypeError('the calibration_list argument must be a '
                            'pyc2mc.core.calibration.CalibList class')

        if isinstance(model, str):
            model = model.lower()
            if model == 'auto':
                model = "polynomial"
                deg = 2
            if model in "polynomial":
                # accept shortcuts for polynomial, such as "p" or "poly"
                model = "polynomial"
        
        # default values for private attributes
        self._verbose = verbose
        self._use_weights = use_weights
        self._fit_intercept = fit_intercept
        self._model = RecalibrationModels(model)
        self._deg = deg
        self._popt = np.empty(
            self._model.get_n_params(
                fit_intercept=self._fit_intercept, deg=self._deg),
            dtype=np.float64)
        self._y_cal = np.empty(self.n_data_points, dtype=np.float64)

        # Check and Extract data from the calibration list
        self._X, self._y = self._check()

    @property
    def model(self):
        """
        returns the regression model used for the recalibration.
        """
        return self._model
    
    @property
    def fit_intercept(self):
        """ True if the intercept of the model is determinated, else False. """
        return self._fit_intercept

    @property
    def deg(self):
        """If the model used for calibration is a polynomial function, deg
        is the degree of the polynomial function."""
        return self._deg

    @property
    def calibrated_mz(self):
        """ Return the calibrated values of experimental m/z. """
        return self._y_cal
        
    @property
    def calibration_mz_range(self):
        """
        Returns in a tuple with the (min, max) interval took for obtaining the
        recalibration function.
        """
        return self._calibration_list.mz_theo.min(), self._calibration_list.mz_theo.max()

    @property
    def n_data_points(self):
        """
        Returns the number of data points found in the calibration list.
        """
        return len(self._calibration_list)
    
    @property
    def calibration_list(self) -> CalibList:
        """
        Returns the CalibList object employed on the regression.
        """
        return self._calibration_list

    @property
    def residuals(self) -> ArrayLike:
        """ Calculates and returns the residuals of the regression. """
        return self._y_cal - self._y

    @property
    def residuals_ppm(self) -> ArrayLike:
        """ Returns the residuals in ppm. """
        return self.residuals / self._y * 1e6

    @property
    def R2(self) -> float:
        """ The r-squared value of the regression. """
        scr = np.sum(self.residuals**2)
        ss_tot = np.sum((self._y - np.mean(self._y))**2)
        return 1 - (scr / ss_tot)

    @property
    def RMSE(self) -> float:
        """ Return the Root-Mean-Square Error (in Da)."""
        return np.sqrt(np.sum(self.residuals**2) / (self.residuals.size - 2))

    @property
    def RMSE_ppm(self) -> float:
        """ Return the Root-Mean-Square Error in ppm."""
        return np.sqrt(np.sum(self.residuals_ppm**2) / (self.residuals.size - 2))

    @property
    def MAE(self) -> float:
        """ Return the Mean Absolute error (in Da) """
        return np.mean(np.abs(self.residuals))
    
    @property
    def MAE_ppm(self) -> float:
        """ Return the Mean Absolute error in ppm """
        return np.mean(np.abs(self.residuals_ppm))

    @property
    def mean_error_ppm(self) -> float:
        """ Calculates and returns the mean error in ppm. """
        return np.mean(self.residuals_ppm)
    
    def _check(self):
        """
        Search the data for regression. That could be available in
        the calibration list, or if only the calibrant masses are
        given, then search on the peak list the information to 
        perform the regression.

        Returns:
            X and y data to perform the regression
        """
        y = self._calibration_list.mz_theo
        if self._model == RecalibrationModels.LEDFORD:
            if not self._calibration_list._is_freq_avail:
                raise ValueError(
                    'Frequency is not available in the Calibration List '
                    'and it is required to use the two walking equation.'
                    )
            X = self._calibration_list.frequency
        elif self._model == RecalibrationModels.MASSELON:
            if not self._calibration_list._is_freq_avail:
                raise ValueError(
                    'Frequency is not available in the Calibration List '
                    'and it is required to use the three walking equation.'
                    )
            if not self._calibration_list._is_intens_avail:
                raise ValueError(
                    'Intensity is not available in the Calibration List '
                    'and it is required to use the three walking equation.'
                    )
            X = (self._calibration_list.frequency,
                 self._calibration_list.intensity)
        else:
            if not self._calibration_list._is_mz_exp_avail:
                raise ValueError(
                    'The experimental mz is not available in the Calibration '
                    f'List and it is required to use the {self._model.value}'
                    '-order regression.'
                    )
            X = self._calibration_list.mz_exp

        # check if there is enough information to perform the Least Squares
        n_points_min = self._model.get_n_params(self._fit_intercept, self._deg) + 1
        if len(y) < n_points_min:
            raise RuntimeError(
                f'A regression with {self._model.value} was required '
                f' but only {len(y)} points are available. Nothing was done.')

        elif len(y) == n_points_min:
            if self._verbose:
                warnings.warn(
                    'The regression will be performed using a minimum number '
                    'of calibration points (observations). This could lead '
                    'to a low confidence because the model do not deviate '
                    'from the curve.\n', UserWarning, stacklevel=2)
            
        return X, y

    def get_recalibrated_calibration_list(self):
        """ Using the calibration parameters, compute the new m/z experimental
        values and return a new calibration list. """
        return self._calibration_list.update_mz_exp(self._y_cal)

    def summary(self):
        """ Prints a summary with regression information """
        print(self._txt_msg())

    def __repr__(self):
        return repr(f'Recalibration in the range: {self.calibration_mz_range}')


class Recalibration(BaseRecalibration):
    """
    This class contains all the methods to perform the recalibration of a 
    single mass spectrum using a calibration list and a given model.
    """
    
    def __init__(
        self,
        calibration_list: CalibList, 
        model: Union[int, str] = 'auto',
        fit_intercept: bool = True,
        deg: int = 2,
        verbose: bool = False,
        use_weights: bool = False,
    ):
        """
        Instance method for the recalibration.
        
        Args:
            calibration_list (CalibList): A CalibList object containing
                theoretical and experimental m/z values.
            model (str): It defines the type of regression to perform.
                The available models are in RecalibrationModels Enum:
                'polynomial', 'ledford', 'masselon'. If 'polynomial', the
                degree must be set from the 'deg' argument.  If `auto`, the
                model fits to a linear model.
            fit_intercept (bool): if False set intercept to zero, otherwise,
                the intercept will be determined by the best fit curve.
            deg (int): If a polynomial model is selected, define the
                degree of the polynomial. Default is 2.
            verbose (bool): In case of absence of regression displays a warning
                message.
            use_weights (bool): If True use the weights defined in the 
                calibration list to perform the recalibration. Default False.
            method (str): ...

        """

        # initialize base class
        super().__init__(calibration_list, model, fit_intercept, deg, 
                         verbose, use_weights)
        
        # Perform the regression using non linear least squares
        self._popt = self._regression()
        self._y_cal = self._model(self._X, *list(self._popt),
                                  fit_intercept=self._fit_intercept,
                                  deg=self._deg)

        # display summary
        if self._verbose:
            self.summary()

    @property
    def opt_param_array(self) -> ArrayLike:
        """
        Returns an array with the optimized parameters.
        """
        return self._popt

    @property 
    def optimized_parameters(self) -> dict:
        """
        Returns a dictionary with the optimized parameters.
        """
        label = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"  # max deg = 26 ???
        return {label[i]: param
                for i, param in enumerate(self._popt)}

    def _regression(self):
        """
        performs the regression depending on the chosen model.

        Returns:
            An array with the optimized parameters.
        """
        # EXPERIMENTAL PEAK VALUES => self._X
        # THEORETICAL MZ VALUES => self._y

        coeff_matrix = self._model.get_matrix(
            self._X, self._fit_intercept, self._deg)
        
        if self._use_weights:
            popt = np.linalg.lstsq(
                np.dot(np.diag(self.calibration_list.weights), coeff_matrix),
                np.dot(np.diag(self.calibration_list.weights), self._y),
                rcond=None)[0]
        else:
            popt = np.linalg.lstsq(coeff_matrix, self._y, rcond=None)[0]

        return popt

    def recalibrate(
        self, 
        peaklist: PeakList, 
        out_of_range_tol: float = 300,
    ) -> PeakList:
        """
        Apply the recalibration on the provided peak list. The 
        ``out_of_range_tol`` parameter set the maximum tolerated distance
        for a given peak to the calibration range. If a peak is out of this
        tolerance, a warning is raise.

        Args:
            peaklist (pyc2mc.core.peak.PeakList): The peaklist object of
                the module peak, to be recalibrated. 
            out_of_range_tol (float): Tolerance m/z interval for peaks out of
                calibration range, default is 300 Da.
        
        Returns:
            The calibrated peaklist.
        """
        if not isinstance(peaklist, PeakList):
            raise TypeError(f'peaklist is {type(peaklist)} and must be of'
                            'type pyc2mc.core.peak.PeakList.')
        
        mz_min, mz_max = peaklist.get_min_max_mz()
        mz_theo_min, mz_theo_max = self.calibration_mz_range

        if (((mz_theo_min - mz_min) > out_of_range_tol) or 
                ((mz_max - mz_theo_max) > out_of_range_tol)):
            line = f"peaklist   : ({mz_min:.2f} -> {mz_max:.2f}) Da\n"
            line += f"calib. list: ({mz_theo_min:.2f} -> {mz_theo_max:.2f}) Da\n"
            warnings.warn(
                'The range of the calibration list is shorter than the range '
                'of the peaklist by more than 50 Da. '
                'Peaks out of the calibration range will be calibrated.\n'
                + line,
                UserWarning, stacklevel=2)
            print("WARNING     : peaklist out of calibration list range.")
            print(line)

        if self._verbose:
            print(f"Calibrating peaklist: {peaklist.name}")
            print(f"{len(peaklist):5d} peaks ({mz_min:.2f} -> {mz_max:.2f}) Da")

        # check frequency available
        freq_avail = "frequency" in peaklist.peak_properties_keys
        if not freq_avail and self._model.need_frequency:
            raise ValueError('Frequency is not available in the peak list, '
                             'in consequence, it is not possible to recalibrate '
                             f'the peak list using {self._model}.')
    
        # get the correct xdata depending on the model
        if self._model == RecalibrationModels.LEDFORD:
            xdata = np.array(peaklist.peak_properties['frequency'])
        elif self._model == RecalibrationModels.MASSELON:
            xdata = (np.array(peaklist.peak_properties['frequency']),
                     peaklist.intensity)
        else:
            xdata = peaklist.mz

        # estimate the new calibrated mz
        new_mz = self._model(xdata, *list(self._popt),
                             fit_intercept=self._fit_intercept,
                             deg=self._deg)

        # update peaks
        # this will work if peaklist is partially attributed
        peaks = list()
        for i, peak in enumerate(peaklist):
            d = peak.as_dict()
            d.pop("mz")
            if peak.is_attributed:
                peak = AttributedPeak(mz=new_mz[i], **d)
            else:
                peak = Peak(mz=new_mz[i], **d)
            peaks.append(peak)

        # new peaklist
        name = peaklist.name if "recalibrated" in peaklist.name else "recalibrated_" + peaklist.name 
        return PeakList.from_list(peaks, name=name)

    def as_dict(self):
        """
        Returns a dictionary with the input parameters of the non linear
        least squares fitting.
        """
        return "regression input => as dict"
    
    def _txt_msg(self):
        """ Returns a text message with the summary """
        mzrange = self.calibration_mz_range
        params = " ".join([f"{k}: {v:.5e}" 
                           for k, v in self.optimized_parameters.items()])
        line = 'CALIBRATION SUMMARY\n'
        line += '-------------------'
        line += f'\nCalibration list             : {self._calibration_list.name}'
        line += f'\nNumber of data points        : {len(self._calibration_list)}'
        line += f'\nRegression model             : {self._model}'
        if self._model == RecalibrationModels.POLYNOMIAL:
            line += f'\nPolynomial degree            : {self._deg}'
        line += f'\nParameters of the model      : {params}'
        line += f'\nRoot mean squared error (ppm): {self.RMSE_ppm:.6f}'
        line += f'\nCalibration mz range         : {mzrange[0]:.2f}, {mzrange[1]:.2f}'
        line += '\n'
        return line

    def __repr__(self):
        return repr(f'Calibration in the range: {self.calibration_mz_range}')
    

class WalkingCalibration(BaseRecalibration, Sequence):
    """
    This class contains all the methods to perform a walking recalibration by
    splitting the m/z domain in segments and then recalibrate each segment 
    independently. If not enough calibration points are available in a
    segment a global recalibration is applied.
    """
    
    def __init__(
        self,
        calibration_list: CalibList, 
        model: Union[int, str] = 'auto',
        fit_intercept: bool = True,
        deg: int = 2,
        segment_size: Union[float, list, str] = 25.0,
        verbose: bool = False,
        use_weights: bool = False,
        global_recal_kws: dict = None,
        segments_kws: dict = dict(min_npts_per_segment=10, 
                                  min_segment_size=25),
    ):
        """
        Instance method for the recalibration.
        
        Args:
            calibration_list (CalibList): A CalibList object that store the
                calibration list.
            model (int, str): It defines the type of regression to perform:
                for polynomials set (1) linear, (2) quadratic, (3) cubic. For
                walking set `'two_walking'` or `'three_walking'`. If `auto`, the
                model fits to the linear model.
            fit_intercept (bool): if False set intercept to zero, otherwise,
                the intercept will be determined during the least squarre.
            segment_size (float, list, str): The width in Da of the segments or a
                sequence or ``"auto"``. If ``segment_size`` is a sequence, 
                it defines the bin edges including the left edge of the first 
                segment and the right edge of the last segment. This allows
                to perform the walking calibration in unequally spaced
                segments. If ``segment_size`` is ``"auto"`` the edges are
                determined using the method ``get_walking_segments()`` from
                ``pyc2mc.core.calibration.CalibList``.
            verbose (bool): In case of absence of regression displays a warning
                message.
            use_weights (bool): If True use the weights defined in the 
                calibration list to perform the recalibration. Default False.
            global_recal_kws (dict): Parameters for the global recalibration
                in the segments which do not contain enough point. The dict
                contains arguments of the ``Recalibration`` class.
            segments_kws (dict): Parameters for automatic definition of
                segments based on a minimum number of points per segment
                and a minimum width.

        """

        # initialize base class
        super().__init__(calibration_list, model, fit_intercept, deg,
                         verbose, use_weights)
        
        if segment_size == "auto":
            segment_size = self.calibration_list.get_walking_segments(
                **segments_kws)

        try:
            self._n_segments = len(segment_size) - 1
            if not np.all(np.diff(segment_size) >= 0):
                raise ValueError(
                    f"Segment's edges must be sorted: {segment_size}")
            # bin edges were provided
            self._segments = segment_size
        except TypeError:
            # segment_size is not a Sequence, determine bin edges
            mzmin = self._calibration_list.mz_theo.min()
            mzmin -= 1e-6 * mzmin  # - 1 ppm to include first point
            mzmax = self._calibration_list.mz_theo.max()
            mzmax += 1e-6 * mzmax  # + 1ppm to include last point
            self._n_segments = int((mzmax - mzmin) / segment_size)
            self._segments = np.linspace(mzmin, mzmax, self._n_segments + 1)

        # arguments for the global recalibration if not enough points are
        # available in a given bin.
        default_global_recal = dict(
            model=self._model, fit_intercept=self._fit_intercept,
            deg=self._deg, verbose=False, use_weights=self._use_weights
        )
        if global_recal_kws is None:
            self._global_recal_kws = default_global_recal
        else:
            self._global_recal_kws = default_global_recal.copy()
            self._global_recal_kws.update(global_recal_kws)

        # split calibration lists
        self._calibration_lists, self._calibration_points_idx = self.segmentation()

        # Perform the regression using non linear least squares
        self._recalibrations = [None] * self._n_segments
        self._is_recalibrated = [False] * self._n_segments
        self._global_recal = None
        self._y_cal = None
        self._popt = None
        self._regression()

        # display summary
        if self._verbose:
            self.summary()

    @property
    def opt_params(self) -> list:
        """
        Returns a list with the optimized parameters.
        """
        return self._popt
    
    @property
    def optimized_parameters(self) -> dict:
        """
        Returns a dictionary with the optimized parameters.
        """
        label = ['A', 'B', 'C', 'D']
        return [{label[i]: param
                 for i, param in enumerate(popt)}
                for popt in self._popt]

    @property
    def segments(self):
        """ Returns bin edges including the left edge of the first segment
        and the right edge of the last segment. """
        return self._segments
    
    @property
    def segment_size(self):
        """ Returns the average segment size. If the segments were 
        determined to span evenly the calibration list this is the segment
        size. If the size of the segments is not constant it is an average. """
        return np.mean(np.diff(self._segments)) 

    def _regression(self):
        """
        performs the regression depending on the chosen model for each
        segment.

        Returns:
            An array with the optimized parameters.
        """

        for seg_idx in range(self._n_segments):
            # check the segment contains enough points
            if len(self._calibration_lists[seg_idx]) == 0:
                continue

            try:
                recal = Recalibration(
                    self._calibration_lists[seg_idx],
                    model=self._model,
                    fit_intercept=self._fit_intercept,
                    deg=self._deg,
                    verbose=False,
                    use_weights=self._use_weights
                )
            except RuntimeError:
                # run time error during recalibration
                # probably not enough points in the segments
                if self._verbose:
                    print(
                        f"Segment: [{self._segments[seg_idx]:10.4f}; "
                        f"{self._segments[seg_idx + 1]:10.4f}] was not calibrated.")
                    print(f"N points: {len(self._calibration_lists[seg_idx])}")
                    print("Global calibration will be applied.")
                continue

            self._recalibrations[seg_idx] = recal
            self._is_recalibrated[seg_idx] = True

        if not all(self._is_recalibrated):
            # at least one segment was not calibrated, proceed to a global
            # recalibration of the spectra using the global_recal keywords
            self._global_recal = Recalibration(
                calibration_list=self._calibration_list,
                **self._global_recal_kws
            )
            global_calibrated_mz = self._global_recal.calibrated_mz
            
        # gather all parameters and compute estimates
        self._popt = list()
        self._y_cal = list()
        for seg_idx in range(self._n_segments):
            if self._is_recalibrated[seg_idx]:
                recal = self._recalibrations[seg_idx]
                self._popt.append(recal.opt_param_array)
                self._y_cal.extend(recal.calibrated_mz)
            else:
                cp_idx = self._calibration_points_idx[seg_idx]
                self._popt.append(self._global_recal.opt_param_array)
                self._y_cal.extend(global_calibrated_mz[cp_idx])

        if len(set([popt.shape for popt in self._popt])) == 1:
            # in that case the list is rectangular as the same model is used
            # for all segments
            self._popt = np.array(self._popt, dtype=np.float64)

    def segmentation(self) -> Tuple[list, list]:
        """
        Split the calibration list in a list of calibration list for each
        segment.

        Returns:
            A list of splitted calibration lists.
        """

        # get segment index of calibration points
        idx = np.searchsorted(self._segments, self._calibration_list.mz_theo)
        idx -= 1

        # split calibration lists
        calibration_lists = list()
        calibration_points_idx = list()
        for seg_idx in range(self._n_segments):
            cp_idx, = np.nonzero(idx == seg_idx)
            cl = CalibList.from_list([self._calibration_list[i] for i in cp_idx])
            calibration_lists.append(cl)
            calibration_points_idx.append(cp_idx)

        return calibration_lists, calibration_points_idx

    def recalibrate(
        self, 
        peaklist: PeakList, 
        out_of_range_tol: float = None,
    ) -> PeakList:
        """
        Apply the recalibration on the provided peak list. The 
        ``out_of_range_tol`` parameter set the maximum tolerated distance
        for a given peak to the calibration range. If a peak is out of this
        tolerance, a warning is raise. By default, the tolerance is set
        as two times the average segment size.

        Args:
            peaklist (pyc2mc.core.peak.PeakList): The peaklist object of
                the module peak, to be recalibrated. 
            out_of_range_tol (float): Tolerance m/z interval for peaks out of
                calibration range, default is 300 Da.
        
        Returns:
            The calibrated peaklist.
        """ 
        if not isinstance(peaklist, PeakList):
            raise TypeError(f'peaklist is {type(peaklist)} and must be of'
                            'type pyc2mc.core.peak.PeakList.')
        
        # set default value or tolerance
        if out_of_range_tol is None:
            out_of_range_tol = 2 * self.segment_size

        mz_min, mz_max = peaklist.get_min_max_mz()
        mz_theo_min, mz_theo_max = self.calibration_mz_range
        if self._verbose:
            print(f"Calibrating peaklist: {peaklist.name}")
            print(f"    {len(peaklist):5d} peaks ({mz_min:.2f} -> {mz_max:.2f}) Da")
        
        # check frequency available
        freq_avail = "frequency" in peaklist.peak_properties_keys
        if not freq_avail and self._model.need_frequency:
            raise ValueError('Frequency is not available in the peak list, '
                             'in consequence, it is not possible to recalibrate '
                             f'the peak list using {self._model}.')
        
        # check out of range tolerance
        if (((mz_theo_min - mz_min) > out_of_range_tol) or
                ((mz_max - mz_theo_max) > out_of_range_tol)):
            line = f"peaklist         : ({mz_min:.2f} -> {mz_max:.2f}) Da\n"
            line += f"calib. list      : ({mz_theo_min:.2f} -> {mz_theo_max:.2f}) Da\n"
            line += f"Mean segment size: {self.segment_size:.2f} Da\n"
            warnings.warn(
                'The range of the calibration list is shorter than the range '
                'of the peaklist by more than the tolerance '
                'Peaks out of the calibration range will be calibrated.\n'
                + line,
                UserWarning, stacklevel=2)
            print("\nWARNING     : peaklist out of calibration list range.")
            print(line)

        # get segment index of mz values
        mz_pl = peaklist.mz
        idx = np.searchsorted(self._segments, mz_pl)
        idx -= 1
        # points out of calibration list range
        npts_out_of_range = (np.count_nonzero(idx < 0) + 
                             np.count_nonzero(idx >= self._n_segments))

        if npts_out_of_range > 0 and self._verbose:
            print(f"{npts_out_of_range} peaks are out of the calibration list range.")
            print(f"mz range before calib. list: {mz_theo_min - mz_min:.2f}")
            print(f"mz range above calib. list : {mz_max - mz_theo_max:.2f}")

        if (mz_theo_min - mz_min) < out_of_range_tol:
            # fix indices for mz values below the first segment edge.
            # if they are not further than out of range tolerance
            # use the calibration parameters of the first segment
            idx = np.where(idx < 0, 0, idx)

        if (mz_max - mz_theo_max) < out_of_range_tol:
            # fix indices for mz values above the last segment edge.
            # if they are not further than out of range tolerance
            # use the calibration parameters of the last segment
            idx = np.where(idx >= self._n_segments, self._n_segments - 1, idx)

        # split by segment the mz indexes
        mz_idx_segment = list()
        for seg_idx in range(self._n_segments):
            mz_idx, = np.nonzero(idx == seg_idx)
            mz_idx_segment.append(mz_idx)

        # get the correct xdata depending on the model
        if self._model == RecalibrationModels.LEDFORD:
            xdata = np.array(peaklist.peak_properties['frequency'])
        elif self._model == RecalibrationModels.MASSELON:
            xdata = (np.array(peaklist.peak_properties['frequency']),
                     peaklist.intensity)
        else:
            xdata = mz_pl

        # loop over segments and compute new mz values
        new_mz = list()
        for is_recal, recal, mz_idx in zip(self._is_recalibrated,
                                           self._recalibrations,
                                           mz_idx_segment):
            # select xdata in the segment
            if self._model == RecalibrationModels.MASSELON:
                seg_xdata = (xdata[0][mz_idx], xdata[1][mz_idx])
            else:
                seg_xdata = xdata[mz_idx]

            # apply recalibration
            if is_recal:
                new_mz_seg = recal.model(seg_xdata, *list(recal.opt_param_array),
                                         fit_intercept=recal.fit_intercept,
                                         deg=recal.deg)
            else:
                new_mz_seg = self._global_recal.model(
                    seg_xdata, *list(self._global_recal.opt_param_array),
                    fit_intercept=self._global_recal.fit_intercept,
                    deg=self._global_recal.deg)
            
            new_mz.append(new_mz_seg)

        new_mz = np.concatenate(new_mz)
        new_mz_idx = np.concatenate(mz_idx_segment)
        
        # Points not in segments and out of range, will be recalibrated
        # using the global recalibration
        out_of_range_idx = np.concatenate([
            np.nonzero(idx < 0)[0],
            np.nonzero(idx >= self._n_segments)[0]
        ])

        if len(out_of_range_idx) > 0:
            if self._global_recal is None:
                # proceed to a global recalibration of the spectra using the
                # global_recal keywords
                self._global_recal = Recalibration(
                    calibration_list=self._calibration_list,
                    **self._global_recal_kws
                )

            # select xdata of out of range peaks
            if self._model == RecalibrationModels.MASSELON:
                out_xdata = (xdata[0][out_of_range_idx], 
                             xdata[1][out_of_range_idx])
            else:
                out_xdata = xdata[out_of_range_idx]

            new_mz_out = self._global_recal.model(
                out_xdata, *list(self._global_recal.opt_param_array),
                fit_intercept=self._global_recal.fit_intercept,
                deg=self._global_recal.deg)
        else:
            new_mz_out = np.array([])

        # update peaks
        # this will work if peaklist is partially attributed
        new_mz = np.concatenate([new_mz, new_mz_out])
        new_mz_idx = np.concatenate([new_mz_idx, out_of_range_idx])

        peaks = list()
        for idx, mz in zip(new_mz_idx, new_mz):
            d = peaklist[idx].as_dict()
            d.pop("mz")

            if peaklist[idx].is_attributed:
                peak = AttributedPeak(mz=mz, **d)
            else:
                peak = Peak(mz=mz, **d)

            peaks.append(peak)

        # new peaklist
        name = peaklist.name if "recalibrated" in peaklist.name else "recalibrated_" + peaklist.name
        return PeakList.from_list(peaks, name=name)

    def _txt_msg(self):
        """ Returns a text message with the summary """
        line = 'WALKING CALIBRATION SUMMARY\n'
        line += 27 * "-" + "\n"
        line += f"Regression model: {self._model}\n"
        line += f"# segments      : {self._n_segments}\n"

        for seg_idx, recal in enumerate(self._recalibrations):
            mzrange = self._segments[seg_idx: seg_idx + 2]
            line += (f"\nSegment {seg_idx:3d}: {mzrange[0]:.2f} "
                     f"-> {mzrange[1]:.2f} ({mzrange[1] - mzrange[0]:.2f})\n")
            if self._is_recalibrated[seg_idx]:
                mzrange = recal.calibration_mz_range
                params = " ".join([f"{k}: {v:.5e}" 
                                   for k, v in recal.optimized_parameters.items()])
                npts = len(self._calibration_lists[seg_idx])

                line += f"    Calibrated: {True}\n"
                line += f"    # points  : {npts}\n"
                line += f"    Parameters: {params}\n"
                line += f"    RMSE (ppm): {recal.RMSE_ppm:.6f}\n"
            else:
                params = " ".join([
                    f"{k}: {v:.5e}" 
                    for k, v in self._global_recal.optimized_parameters.items()
                ])
                line += "    Calibrated: global\n"
                line += f"    Parameters: {params}\n"
                line += f"    RMSE (ppm): {self._global_recal.RMSE_ppm:.6f}\n"

        return line
    
    def __len__(self):
        return len(self._recalibrations)

    def __getitem__(self, item: Union[int, slice]):
        """ Return the required recalibration segments """
        return self._recalibrations[item]

    def __iter__(self):
        return iter(self._recalibrations)


class MassDifferencesCalibration:
    """
    This class contains all the methods to perform a calibration based on
    mass differences.
    """
    
    def __init__(
        self,
        mass_differences: MassDifferences,
        targets: Sequence = ["C H2"],
        sigma: float = 5e-4,
        weights: str = "step",
        weights_kws: dict = None,
        verbose: bool = False,
    ):
        """
        Instance method for the recalibration.
        
        Args:
            mass_differences (MassDifferences): The MassDifferences object
            targets (list): List of theoretical mass differences to be 
                used to proceed to the calibration
            sigma (float): mass differences are selected if they are in
                an interval arround the target value +/- sigma, default
                5e-4
            weights (str): 'step' (default) or 'gaussian'. Defines the
                shape of the weighting function. If None, no weights are
                applied.
            weights_ks (dict): Parameters in order to define the weighting
                function
            verbose (bool): control verbosity

        """        
        self._mass_differences = mass_differences
        self._weights = weights
        self._weights_kws = weights_kws
        self._sigma = sigma
        self._verbose = verbose
        self._model = RecalibrationModels("ledford")
        self._popt = np.empty(2, dtype=np.float64)

        # manage targets
        self._targets = dict()
        for target in targets:
            f = get_formula(target)
            if f is None:
                try:
                    target = float(target)
                    self._targets[f"{str(target):.7f}"] = target
                except ValueError:
                    raise ValueError(
                        f"{target} is not a valid target. You must provide"
                        " either a formula or a float value.")
            else:
                self._targets[str(f)] = f.exact_mass

        self._dmz_theo, self._dmz, self._indices = self._regression()

    @property
    def weights(self):
        """ Weights function to be considered: 'step' or 'gaussian' """
        return self._weights

    @property
    def model(self):
        """
        returns the regression model used for the recalibration.
        """
        return self._model

    @property
    def opt_param_array(self) -> ArrayLike:
        """
        Returns an array with the optimized parameters.
        """
        return self._popt

    @property 
    def optimized_parameters(self) -> dict:
        """
        Returns a dictionary with the optimized parameters.
        """
        label = ['A', 'B']
        return {label[i]: param
                for i, param in enumerate(self._popt)}
    
    @property
    def residuals(self) -> ArrayLike:
        """ Calculates and returns the residuals of the regression. """
        return self._dmz_theo - self._dmz

    @property
    def residuals_ppm(self) -> ArrayLike:
        """ Returns the residuals in ppm. """
        return self.residuals / self._dmz_theo * 1e6

    @property
    def R2(self) -> float:
        """ The r-squared value of the regression. """
        scr = np.sum(self.residuals**2)
        ss_tot = np.sum((self._dmz_theo - np.mean(self._dmz_theo))**2)
        return 1 - (scr / ss_tot)

    @property
    def RMSE(self) -> float:
        """ Return the Root-Mean-Square Error (in Da)."""
        return np.sqrt(np.sum(self.residuals**2) / (self.residuals.size - 2))

    @property
    def RMSE_ppm(self) -> float:
        """ Return the Root-Mean-Square Error in ppm."""
        return np.sqrt(np.sum(self.residuals_ppm**2) / (self.residuals.size - 2))

    @property
    def MAE(self) -> float:
        """ Return the Mean Absolute error (in Da) """
        return np.mean(np.abs(self.residuals))
    
    @property
    def MAE_ppm(self) -> float:
        """ Return the Mean Absolute error in ppm """
        return np.mean(np.abs(self.residuals_ppm))

    @property
    def mean_error_ppm(self) -> float:
        """ Calculates and returns the mean error in ppm. """
        return np.mean(self.residuals_ppm)

    def _lst_matrix_ledford(self, target: float, f_i: ArrayLike, 
                            f_j: ArrayLike, weights_ij: ArrayLike):
        """ set up the least square system with the ledford equation """

        Xij = 1 / f_j - 1 / f_i
        Yij = 1 / f_j ** 2 - 1 / f_i ** 2

        coupling = np.sum(Xij * Yij * weights_ij)
        
        M = np.array([
            [np.sum(weights_ij * Xij ** 2), coupling],
            [coupling, np.sum(weights_ij * Yij ** 2)],
        ], dtype=np.float64)

        SM = target * np.array([np.sum(weights_ij * Xij), np.sum(weights_ij * Yij)])

        return M, SM

    def _lst_matrix_Masselon(self, target: float, f_i: ArrayLike, 
                             f_j: ArrayLike, weights_ij: ArrayLike, 
                             I_i: ArrayLike, I_j: ArrayLike):
        """ set up the least square system 
        Using A / f + B / f^2 + C I / f^2
        """

        Xij = 1 / f_j - 1 / f_i
        Yij = 1 / f_j ** 2 - 1 / f_i ** 2
        Zij = I_j / f_j ** 2 - I_i / f_i ** 2

        XY = np.sum(Xij * Yij * weights_ij)
        XZ = np.sum(Xij * Zij * weights_ij)
        YZ = np.sum(Yij * Zij * weights_ij)

        X2 = np.sum(weights_ij * Xij ** 2)
        Y2 = np.sum(weights_ij * Yij ** 2)
        Z2 = np.sum(weights_ij * Zij ** 2)
        
        M = np.array([
            [X2, XY, XZ],
            [XY, Y2, YZ],
            [XZ, YZ, Z2],
        ], dtype=np.float64)

        SM = target * np.array([
            np.sum(weights_ij * Xij), 
            np.sum(weights_ij * Yij),
            np.sum(weights_ij * Zij),
        ])

        return M, SM

    def _regression(self):
        """ calibrate using the freq and several mass differences distribution """

        # TODO if the Masselon equation is interesting, need a test here
        # and set up 3 x 3 arrays
        coeff_M = np.zeros((2, 2))
        ordinate = np.zeros((2,))

        pl = self._mass_differences.peaklist
        frequencies = np.array(pl.peak_properties["frequency"])
        
        all_dmz = list()
        all_dmz_theo = list()
        all_indices = [[], []]
        for label, target in self._targets.items():
            if self._verbose:
                print(f"{label:>10s}: {target:12.6f}")

            # look for mz values
            dmz, indices = self._mass_differences.get_mz_diff_in_range(
                delta_mz_bounds=(target - self._sigma, target + self._sigma),
                return_indices=True)
            
            all_dmz.append(dmz)
            all_dmz_theo.append(np.full(dmz.shape, target))
            all_indices[0].append(indices[0])
            all_indices[1].append(indices[1])

            # get the f_i and f_j
            idx, jdx = indices
            f_i = frequencies[idx]
            f_j = frequencies[jdx]

            SN_i = pl.SN[idx]
            SN_j = pl.SN[jdx]

            if self._weights == "step":
                weights_i = step_weights(SN_i, **self._weights_kws)
                weights_j = step_weights(SN_j, **self._weights_kws)
                # set weights to 1 if i AND j are not zero
                weights = np.where(weights_i + weights_j > 1, 1, 0)
            elif self._weights == "gaussian":
                # for now use SN of i but maybe i and j should be combined ?
                weights_i = gaussian_weights(SN_i, **self._weights_kws)
                weights_j = gaussian_weights(SN_i, **self._weights_kws)
                weights = weights_i * weights_j
            else:
                weights = np.ones(idx.shape)

            coeff_M_t, ordinate_t = self._lst_matrix_ledford(target, f_i, f_j, weights)

            coeff_M += coeff_M_t
            ordinate += ordinate_t

        X, residuals, rank, s = np.linalg.lstsq(coeff_M, ordinate, rcond=None)
        if self._verbose:
            print("M            :\n", coeff_M)
            print("SM           :\n", ordinate)
            print("Params       :", X)
            print("rank         :", rank)
            print("singular val.:", s)
        
        self._popt = X
        all_indices = [np.concatenate(all_indices[0]),
                       np.concatenate(all_indices[1])]
        return np.concatenate(all_dmz_theo), np.concatenate(all_dmz), all_indices

    def recal(pl, params):
        A, B = params
        freq = np.array(pl.peak_properties["frequency"])
        mz = A / freq + B / freq ** 2
        return PeakList(mz=mz, intensity=pl.intensity, SN=pl.SN)

    def recalibrate(
        self, 
        peaklist: PeakList = None,
    ) -> PeakList:
        """
        Apply the recalibration.

        Args:
            peaklist (pyc2mc.core.peak.PeakList): The peaklist to be 
                recalibrated. If no peaklist is provided, it takes the 
                peaklist used to compute mass differences.                
        
        Returns:
            The calibrated peaklist.
        """
        if peaklist is None:
            peaklist = self._mass_differences.peaklist

        mz_min, mz_max = peaklist.get_min_max_mz()

        if self._verbose:
            print(f"Calibrating peaklist: {peaklist.name}")
            print(f"{len(peaklist):5d} peaks ({mz_min:.2f} -> {mz_max:.2f}) Da")

        # check frequency available
        freq_avail = "frequency" in peaklist.peak_properties_keys
        if not freq_avail:
            raise ValueError('Frequency is not available in the peak list, '
                             'in consequence, it is not possible to recalibrate '
                             f'the peak list using {self._model}.')
    
        frequencies = np.array(peaklist.peak_properties['frequency'])
        # estimate the new calibrated mz
        new_mz = self._model(frequencies, *list(self.opt_param_array),
                             fit_intercept=False)

        # update peaks
        # this will work if peaklist is partially attributed
        peaks = list()
        for i, peak in enumerate(peaklist):
            d = peak.as_dict()
            d.pop("mz")
            if peak.is_attributed:
                peak = AttributedPeak(mz=new_mz[i], **d)
            else:
                peak = Peak(mz=new_mz[i], **d)
            peaks.append(peak)

        # new peaklist
        name = peaklist.name if "md_recal" in peaklist.name else "md_recal" + peaklist.name 
        return PeakList.from_list(peaks, name=name)

    def as_dict(self):
        """
        Returns a dictionary with the input parameters of the non linear
        least squares fitting.
        """
        return "regression input => as dict"
    
    def _txt_msg(self):
        """ Returns a text message with the summary """
        params = " ".join([f"{k}: {v:.5e}" 
                           for k, v in self.optimized_parameters.items()])
        line = 'MASS DIFFERENCES CALIBRATION SUMMARY\n'
        line += '------------------------------------'
        line += f'\nRegression model             : {self._model}'
        line += f'\nParameters of the model      : {params}'
        line += f'\nRoot mean squared error (ppm): {self.RMSE_ppm:.6f}'
        line += '\n'
        return line

    def __repr__(self):
        return repr(f'Calibration in the range: {self.calibration_mz_range}')

    def summary(self):
        """ Prints a summary with regression information """
        print(self._txt_msg())
