#!/usr/bin/env python
# coding: utf-8

"""
This module provides a function that perform an assignment of all the
possible candidates to a mass corresponding to a peak. The results are
returned as a dataframe containing all the information needed to further
perform an attribution.

The assignment is done on the basis of a provided formula grid which
defines the list of elements and isotopes to be considered. Each mass
or peak is assigned independently.
"""

from typing import Tuple, Union
from collections import defaultdict

import pandas as pd
import numpy as np

from pyc2mc.core.peak import PeakList, Peak
from pyc2mc.core.formula_grid import FormulaGrid


def assign_candidates(
    peak_list: Union[PeakList, Peak, np.typing.ArrayLike, float],
    formula_grid: FormulaGrid,
    mz_bounds: Tuple[float] = None,
    lambda_parameter: float = 1.0,
    returns_candidates_data: bool = False
) -> pd.DataFrame:
    """ Create the lis of candidates for each mass of the provided peak
    list or a single peak by using the Formula Grid.

    Warning: The peak list must be sorted according to mass.

    Args:
        peak_list (pyc2mc.core.peak.PeakList): Is the peak list object.
        formula_grid (pyc2mc.core.formula_grid.FormulaGrid): The formula grid
            used for the list of candidates.
        mz_bounds (float, float): A tuple to limit the peak list to
            a mz range.
        lambda_parameter (float): largest error in ppm to select the
            candidates.
        returns_candidates_data (bool): Default is False. If True,
            retunrns a column containing data frames with detailed
            data about the candidates.

    Returns
        A dataframe containing a column ``"candidates"`` with a string
        of comma separated list of candidates. If ``returns_candidates_data``
        is ``True``, returns also a column ``"cand_list"`` with
        detailed data about the candidates.
    """

    # extract a dataframe from the peak or peak list in a mass range
    try:
        pl = peak_list.to_dataframe(mz_bounds=mz_bounds)
    except AttributeError:
        try:
            pl = pd.DataFrame({
                "mz": np.array(peak_list, dtype=np.float64),
                "intensity": len(peak_list) * [np.nan],
            })
        except TypeError:
            pl = pd.DataFrame({
                "mz": np.array([peak_list], dtype=np.float64),
                "intensity": [np.nan],
            })

    # TODO: keep or sum ?
    # remove duplicated mass if this exist
    pl = pl[~pl.mz.duplicated(keep='first')]

    # get an array of mz values
    mz_values = pl.mz.values

    # compute error limits in ppm
    upper_limit = mz_values + lambda_parameter * 1e-6 * mz_values
    lower_limit = mz_values - lambda_parameter * 1e-6 * mz_values
    pl["lower_limit"] = lower_limit
    pl["upper_limit"] = upper_limit
    
    # compute integer part
    floor_mz_values = np.floor(mz_values).astype(np.uint16)
    pl['integer_part'] = floor_mz_values
    pl['pid'] = pl.index
    # save back pid, reset index to match True index of the dataframe
    pl.reset_index(drop=True, inplace=True)

    # group by integer part to speed up
    unique_floor_mz_values = np.unique(floor_mz_values)

    formulas_str = [np.nan] * len(mz_values)
    candidates_data = [np.nan] * len(mz_values)
    candidates_exist = np.full(len(mz_values), False)

    # loop over groups
    for integer_part in unique_floor_mz_values:

        target_mz_idx, = (floor_mz_values == integer_part).nonzero()

        integer_parts = set([integer_part - 1, integer_part, integer_part + 1])
        if not any([g in formula_grid.integer_parts for g in integer_parts]):
            # the formula grid does not provide candidates
            continue

        # loop over Formula Grids by isotopes for that group
        mz_data_fg = defaultdict(list)
        for key, grid in formula_grid.items():

            candidates_mz = np.array([])
            fg_idx = np.array([], dtype=np.uint32)
            for group in integer_parts:
                idx, = (grid.integer_part == group).nonzero()
                fg_idx = np.concatenate([fg_idx, idx])
                candidates_mz = np.concatenate(
                    [candidates_mz, grid.calculated_mz[idx]])

            lower_mask = np.greater.outer(candidates_mz, lower_limit[target_mz_idx])
            upper_mask = np.less.outer(candidates_mz, upper_limit[target_mz_idx])
            mask = (lower_mask & upper_mask).nonzero()

            # mask[0]: contains the index in candidates_mz of the possible candidates
            #          it corresponds to formula grid values
            # mask[1]: contains the index in target_mz_idx for which there is a match
            #          it corresponds to peak list values

            if len(mask[0]) == 0:
                # no match
                continue

            for idx_in_filtered in np.unique(mask[1]):
                # get index of mz in the initial peak list
                mz_idx_in_pl = target_mz_idx[idx_in_filtered]

                # get candidates index in the formula grid
                candidates_idx = mask[0][idx_in_filtered == mask[1]]
                candidates_idx_in_fg = fg_idx[candidates_idx]

                mz_data_fg[mz_idx_in_pl].append((key, candidates_idx_in_fg))

        for mz_idx_in_pl, fg_data in mz_data_fg.items():

            mz_target = mz_values[mz_idx_in_pl]

            # build the list of candidate formulas
            formulas = list()
            calculated_mz = list()
            candidates_nominal_mass = list()
            elemental_compositions = list()
            isotopic = list()
            for key, candidates_idx_in_fg in fg_data:
                if key == (0, 0, 0):
                    isotopic += [False] * len(candidates_idx_in_fg)
                else:
                    isotopic += [True] * len(candidates_idx_in_fg)

                # get calculated mz from formula grid
                calculated_mz.append(
                    formula_grid[key].calculated_mz[candidates_idx_in_fg])
                candidates_nominal_mass.append(
                    formula_grid[key].nominal_mass[candidates_idx_in_fg])
                
                # set up the list of formula strings
                grid = formula_grid[key].values[candidates_idx_in_fg]
                for composition in grid:
                    formula = " ".join(
                        [f"{el}{n}" 
                         for el, n in zip(formula_grid[key].species, composition)
                         if n > 0])
                    formulas.append(formula)

                elemental_compositions.append(grid)

            calculated_mz = np.concatenate(calculated_mz, axis=0)
            candidates_nominal_mass = np.concatenate(candidates_nominal_mass,
                                                     axis=0)

            if returns_candidates_data:
                error_Da = mz_target - calculated_mz
                elemental_compositions = np.concatenate(
                    elemental_compositions,
                    axis=0
                )
                df = pd.DataFrame(elemental_compositions, 
                                  columns=formula_grid.species)
                df["ion_exact_mass"] = calculated_mz  # change column name here
                df["nominal_mass"] = candidates_nominal_mass
                df["err_mDa"] = error_Da * 1e3
                df["err_ppm"] = error_Da / calculated_mz * 1e6
                df["isotopic"] = isotopic
                df["formula"] = formulas

                candidates_data[mz_idx_in_pl] = df

            candidates_exist[mz_idx_in_pl] = True
            formulas_str[mz_idx_in_pl] = ", ".join(formulas)

    # set back pid as index
    pl.index = pl.pid

    # store new data
    pl['candidates'] = formulas_str
    pl["candidates_exist"] = candidates_exist
    if returns_candidates_data:
        pl['cand_list'] = candidates_data

    return pl


# TODO: the function is supposed to become the default `assign_candidates`
# in contrast to the old one (above) it returns only indexes in the FormulaGrid
# of the candidates for a given mass. 
# This is a low level function, other functions/methods calling that one
# are supposed to manage the formula grid data to get what they need.
def assign_candidates_new(
    peak_list: Union[PeakList, Peak, np.typing.ArrayLike, float],
    formula_grid: FormulaGrid,
    mz_bounds: Tuple[float] = None,
    lambda_parameter: float = 1.0,
) -> pd.DataFrame:
    """ Create the lis of candidates for each mass of the provided peak
    list or a single peak by using the Formula Grid.

    Warning: The peak list must be sorted according to mass.

    Args:
        peak_list (pyc2mc.core.peak.PeakList): Is the peak list object.
        formula_grid (pyc2mc.core.formula_grid.FormulaGrid): The formula grid
            used for the list of candidates.
        mz_bounds (float, float): A tuple to limit the peak list to
            a mz range.
        lambda_parameter (float): largest error in ppm to select the
            candidates.
        returns_candidates_data (bool): Default is False. If True,
            retunrns a column containing data frames with detailed
            data about the candidates.

    Returns
        A dataframe containing a column ``"candidates"`` with a string
        of comma separated list of candidates. If ``returns_candidates_data``
        is ``True``, returns also a column ``"cand_list"`` with
        detailed data about the candidates.
    """

    # extract a dataframe from the peak or peak list in a mass range
    try:
        pl = peak_list.to_dataframe(mz_bounds)
    except AttributeError:
        try:
            pl = pd.DataFrame({
                "mz": np.array(peak_list, dtype=np.float64),
                "intensity": len(peak_list) * [np.nan],
            })
        except TypeError:
            pl = pd.DataFrame({
                "mz": np.array([peak_list], dtype=np.float64),
                "intensity": [np.nan],
            })

    # TODO: keep or sum ?
    # remove duplicated mass if this exist
    pl = pl[~pl.mz.duplicated(keep='first')]

    # get an array of mz values
    mz_values = pl.mz.values

    # compute error limits in ppm
    upper_limit = mz_values + lambda_parameter * 1e-6 * mz_values
    lower_limit = mz_values - lambda_parameter * 1e-6 * mz_values
    
    # compute integer part
    floor_mz_values = np.floor(mz_values).astype(np.uint16)

    # group by integer part to speed up
    unique_floor_mz_values = np.unique(floor_mz_values)

    candidates_data = [None] * len(mz_values)  # this could be an object ?

    # loop over groups
    for integer_part in unique_floor_mz_values:

        target_mz_idx, = (floor_mz_values == integer_part).nonzero()

        integer_parts = set([integer_part - 1, integer_part, integer_part + 1])
        if not any([g in formula_grid.integer_parts for g in integer_parts]):
            # the formula grid does not provide candidates
            continue

        # loop over Formula Grids by isotopes for that group
        mz_data_fg = defaultdict(list)
        for key, grid in formula_grid.items():

            candidates_mz = np.array([])
            fg_idx = np.array([], dtype=np.uint32)
            for group in integer_parts:
                idx, = (grid.integer_part == group).nonzero()
                fg_idx = np.concatenate([fg_idx, idx])
                candidates_mz = np.concatenate(
                    [candidates_mz, grid.calculated_mz[idx]])

            lower_mask = np.greater.outer(candidates_mz, lower_limit[target_mz_idx])
            upper_mask = np.less.outer(candidates_mz, upper_limit[target_mz_idx])
            mask = (lower_mask & upper_mask).nonzero()

            # mask[0]: contains the index in candidates_mz of the possible candidates
            #          it corresponds to formula grid values
            # mask[1]: contains the index in target_mz_idx for which there is a match
            #          it corresponds to peak list values

            if len(mask[0]) == 0:
                # no match
                continue

            for idx_in_filtered in np.unique(mask[1]):
                # get index of mz in the initial peak list
                mz_idx_in_pl = target_mz_idx[idx_in_filtered]

                # get candidates index in the formula grid
                candidates_idx = mask[0][idx_in_filtered == mask[1]]
                candidates_idx_in_fg = fg_idx[candidates_idx]

                mz_data_fg[mz_idx_in_pl].append((key, candidates_idx_in_fg))

        for mz_idx_in_pl, fg_data in mz_data_fg.items():
            candidates_data[mz_idx_in_pl] = fg_data

    return candidates_data

# =========================
# May be you can imagine a Object Oriented version


from collections.abc import Sequence


class CandidatesList(Sequence):
    """ This class provides a list of Candidates object associated to a
    list of peaks.

    TODO: The aim of that class would be to ease the collection of data
    joining the candidate list and the formula grid for later perform the
    attribution. But, the point, I guess, is to limit the calculations to
    what is really needed.
    To speed up the process don't forget to use the
    `exact_masses` of formula grid when you make a Formula instance.
    """
    
    def __init__(
        self,
        peak_list: PeakList,
        formula_grid: FormulaGrid,
        mz_bounds: Tuple[float] = None,
        lambda_parameter: float = 1.0,
    ):
        """ Create the lis of candidates for each mass of the provided peak
        list or a single peak by using the Formula Grid.

        Warning: The peak list must be sorted according to mass.

        Args:
            peak_list (pyc2mc.core.peak.PeakList): Is the peak list object.
            formula_grid (pyc2mc.core.formula_grid.FormulaGrid): The formula grid
                used for the list of candidates.
            mz_bounds (float, float): A tuple to limit the peak list to
                a mz range.
            lambda_parameter (float): largest error in ppm to select the
                candidates.
            returns_candidates_data (bool): Default is False. If True,
                retunrns a column containing data frames with detailed
                data about the candidates.

        Returns
            A dataframe containing a column ``"candidates"`` with a string
            of comma separated list of candidates. If ``returns_candidates_data``
            is ``True``, returns also a column ``"cand_list"`` with
            detailed data about the candidates.
        """
        self._peak_list = peak_list
        self._fromula_grid = formula_grid
        self._mz_bounds = mz_bounds
        self._lambda_parameter = lambda_parameter
        self._candidates_list = self._setup()
        pass

    def _setup(self):
        """ proceed assignment """
        pass

    def get_peak(self, pid: int) -> Peak:
        """ get the corresponding peak """
        pass

    def get_candidates_data(self, pid: int):
        """ Use indices to get information from the formula grid. """
        pass

    @classmethod
    def from_list(self, candidates_list: Sequence):
        """ setup the candidate list from a list of something """
        self._candidates_list = candidates_list
        pass

    def __getitem__(self, item: Union[int, slice]):
        """ Return the required Peak if item is an integer or a new PeakList
        if item is a slice object """
        if isinstance(item, slice):
            start = item.start if item.start is not None else 0
            step = item.step if item.step is not None else 1
            if item.stop is None:
                stop = len(self._candidates_list) if step > 0 else -1
            else:
                stop = item.stop
            if start == stop:
                return CandidatesList([])
            else:
                return CandidatesList.from_list(
                    [self._candidates_list[i] for i in range(start, stop, step)])
        else:
            return self._peaks[item]

    def __len__(self):
        return len(self._candidates_list)
