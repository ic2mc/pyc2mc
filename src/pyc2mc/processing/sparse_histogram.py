import numpy as np 

from pyc2mc.data_structures.linked_list import DoublyLinkedList
from pyc2mc.utils import generate_bin_centers_array

def sparse_histogram(sorted_mz_diffs, bin_size=1e-7, zero_correction = False):
    """
    Sparse Histogram implementation using linked list with sparse zero count bins.

    Args:
        sorted_delta_array: sorted delta array
        bin_size: bin size
        zero_correction: whether to add 2 zero bins in between two non-zero bins.
                                without this we will encounter artifacts in base of the histogram
                                and its minimum value would be 1 instead of 0.
    """
    start_bin = sorted_mz_diffs[0]
    bin_edges = DoublyLinkedList() if zero_correction else list()
    bin_edges.append(start_bin)
    count = DoublyLinkedList() if zero_correction else list()
    count.append(1)
    
    for i in range(1,len(sorted_mz_diffs)):
            if sorted_mz_diffs[i] - start_bin > bin_size:
                bin_edges.append(start_bin + bin_size)
                start_bin = sorted_mz_diffs[i]
                bin_edges.append(start_bin)
                count.append(1)
            else:
                if zero_correction:
                    count.tail.data += 1
                else:
                    count[-1] += 1
    bin_edges.append(start_bin + bin_size)

    if zero_correction:

        curr_bin = bin_edges.head
        curr_bin = curr_bin.next
        
        curr_count = count.head
        threshold = bin_size / 4
        while curr_bin and curr_count:
            temp_bin = None

            if curr_bin.next:
                temp_bin = curr_bin.next.next
            
            temp_count = curr_count.next
            if curr_bin.next and curr_bin.next.data - curr_bin.data > 4 * bin_size:
                left_edge_value = curr_bin.data + threshold
                bin_edges.insert_after(curr_bin, left_edge_value)
                curr_bin = curr_bin.next
                bin_edges.insert_after(curr_bin, left_edge_value + bin_size)
                count.insert_after(curr_count, 0)
                
                curr_bin = curr_bin.next

                if temp_bin:
                    right_edge_value = temp_bin.data - threshold
                    bin_edges.insert_before(temp_bin, right_edge_value)
                    bin_edges.insert_after(curr_bin, right_edge_value - bin_size) 
                    count.insert_before(temp_count, 0)

            curr_bin = temp_bin
            curr_count = temp_count
    
    if zero_correction:
        bin_centers = bin_edges.generate_bin_centers_array()
    else:
        bin_centers = generate_bin_centers_array(bin_edges)

    counts = count.to_numpy_array() if zero_correction else np.array(count)
    
    return bin_centers, counts