# coding: utf-8

""" This module and its submodules provides processing tools of 
mass spectrometry data. The data used as input of these processing tools
can be imported from the tools provided in the io module and 
its submodules. Each processing tool provides as output, advanced objects
that represent the processed data and aim to give an easy access to
properties in order to explore the results."""