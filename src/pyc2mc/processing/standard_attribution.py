#!/usr/bin/env python
# coding: utf-8

"""
This module provides functions to perform the ion molecular formula attribution
of a peak list by isotopic pattern or lowest error algorithms.
"""

import logging
import warnings
from typing import Tuple, Union

import pandas as pd
import numpy as np

from pyc2mc.core.peak import Peak, AttributedPeak
from pyc2mc.core.peak import PeakList, AttributedPeakList
from pyc2mc.core.formula_grid import FormulaGrid
from pyc2mc.core.formula import Composition
from pyc2mc.processing.assignment import assign_candidates

__all__ = ["lowest_error_attribution", "attribute_isotopes",
           "attribute_pattern", "isotopic_pattern_attribution"]

# this cut off value is arbitrary and based on the consideration of
# 100 carbon atoms and a probability to get one 13C isotope
# CUT_OFF_FACTOR = 2.0 * 100 / 1.1


def lowest_error_attribution(
    peaklist: Union[Peak, PeakList, AttributedPeakList],
    formula_grid: FormulaGrid,
    lambda_parameter: float = 1,
    use_isotopes: bool = False,
    use_SN: bool = True,
) -> AttributedPeakList:
    """
    Method to perform the formula attribution considering a lowest error
    attribution algorithm. The formula leading to the smallest mz
    deviation is selected as the best candidate.

    If the attribution of isotopes is required, for each monoisotopic
    formula previously determined the algorithm looks for the presence
    of a peak at a m/z shift corresponding to an isotopic formula in the
    required mz deviation range.

    Args:
        peaklist (pyc2mc.core.peak.PeakList): a pyc2mc Peaklist object
            which contains the peaklist to attribute.
        formula_grid (pyc2mc.formula_grid.FormulaGrid): Formula Grid.
        lambda_parameter (float): is the maximum error in ppm for the
            peak assignment.
        use_isotopes (bool): if True, the algorithm will assign the
            isotopes of the monoisotopic attributed formula.
        use_SN (bool): if True, the attribution is done by decreasing
            values of SN.

    Returns
    -------
    A tuple containing (attributed_peaklist, no_hits).

    attributed_peaklist : pyc2mc.core.peak.AttributedPeakList 
        An attributed peaklist object.
    no_hits : pyc2mc.core.peak.PeakList
        the peaklist of non-attributed signals.
    
    """

    # check consistency between formula grid and isotopic attribution
    if use_isotopes and not formula_grid.is_isotopic:
        raise ValueError("Isotopic formulas cannot be attributed because "
                         "the formula grid does not contains any isotopes."
                         f" Formula grid keys: {formula_grid.keys()}")

    attributed_peaks = list()
    if isinstance(peaklist, AttributedPeakList):
        attributed_peaks = [peak for peak in peaklist if peak.is_attributed]
        peaklist = peaklist.get_nohits()
    elif isinstance(peaklist, Peak):
        # support of single peak assignment
        peaklist = PeakList.from_list([peaklist], name="peaklist")

    # assign a list of candidates to the peak in the list
    candidates = assign_candidates(
        peaklist, formula_grid, returns_candidates_data=True,
        lambda_parameter=lambda_parameter)

    # split hit and not hits
    mask = candidates.candidates.isna()
    no_hits_peaks = list()
    list_of_peaks = list()
    for peak, flag in zip(peaklist, mask):
        if flag:
            no_hits_peaks.append(peak)
        else:
            list_of_peaks.append(peak)
    
    candidates.dropna(axis="rows", subset="candidates", inplace=True)
    # save back peak idx
    candidates["peak_idx"] = range(len(list_of_peaks))

    # sort dataframe either by SN or by intensity
    if peaklist.SN_avail and use_SN:
        candidates.sort_values(by='SN', ascending=False, inplace=True)
    else:
        candidates.sort_values(by='intensity', ascending=False, inplace=True)

    # iterate the peaklist
    while len(candidates) > 0:

        attribution = False
        df_index, row = next(candidates.iterrows())

        # get non isotopic candidates
        cl_mono = row.cand_list[~row.cand_list['isotopic']]

        if not cl_mono.empty:
            # first look at the existance of non isotopic formula
            ix = cl_mono.err_ppm.abs().values.argmin()
            candidate = cl_mono.iloc[ix]
            composition = Composition.from_string(candidate.formula)

            # for convenience, rename all column to intensity or intensities
            attributed_peak = AttributedPeak(
                formula=composition,
                polarity=formula_grid.polarity,
                attribution_method="lowest_error",
                exact_masses=formula_grid.exact_masses,
                **list_of_peaks[row.peak_idx].as_dict(),
            )
            attributed_peaks.append(attributed_peak)
            attribution = True

        else:
            # only isotopic candidates exist, add the row to no_hits data frame
            no_hits_peaks.append(list_of_peaks[row.peak_idx])

        # remove the mz from the list
        candidates.drop(index=df_index, inplace=True)

        # attribute all isotopes of current formula
        if use_isotopes and attribution:
            attributed_isotope_peaks = attribute_isotopes(
                attributed_peak=attributed_peak,
                formula_grid=formula_grid,
                lambda_parameter=lambda_parameter,
                candidates=candidates,
                list_of_peaks=list_of_peaks,
            )

            # update the list of attributed peaks
            attributed_peaks.extend(attributed_isotope_peaks)

            # remove attributed isotopes peaks
            candidates.drop(
                index=[attr_peak.pid 
                       for attr_peak in attributed_isotope_peaks],
                inplace=True
            )

    # complete the list of attributed peak with non attributed from nohit
    attributed_peaks.extend(no_hits_peaks)

    # return an attributed peak list object
    name = peaklist.name if "attributed" in peaklist.name else "attributed_" + peaklist.name
    return AttributedPeakList.from_list(
        attributed_peaks, polarity=formula_grid.polarity,
        name=name)


def attribute_pattern(
    peak: Peak,
    formula_grid: FormulaGrid,
    pattern_key: tuple = (6, 13, 1),
    lambda_parameter: float = 1.,
    candidates: pd.DataFrame = None,
    list_of_peaks: list = None,
    peaklist: Union[PeakList, AttributedPeakList] = None,
) -> AttributedPeak:
    """
    Performs the isotopic attribution method. This method attributes
    each peak by looking for the isotopic pattern of the peak. First
    the algorithm looks if a mz exists shifted from the mz of the
    given peak by the ``mass_shift / z`` of the pattern, in the given error
    range defined from the ``lambda_parameter``. The attribution
    following the ``"pattern attribtion"`` method is done if the
    isotopic formula is consistent with the formula of the initial peak.

    If several peaks exist at the mass shifted by the isotopic pattern,
    the peak with the highest S/N (or intensity) is considered. This
    situation is unlikely except if you consider a quite large error
    or a bad calibration.

    Args:
        peak (Peak): Main peak for which pattern attribution is done
        formula_grid (FormulaGrid): Formula grid.
        pattern_key (tuple): Isotopic pattern to consider as a tuple which
            define the key of the pattern_grid in the formula Grid.
        candidates (DataFrame): A dataframe containing the mz to
            attribute and the corresponding candidates. This dataframe
            is an output of the assign_candidate function.
        list_of_peaks (list): List of peak object associated to the
            candidates data frame.
        peaklist (PeakList): if candidates are not provided, there are
            assigned from this peak list.

    Returns:
        An attributed peak as an ``AttributedPeak`` object, along with
        a tuple of ``(attribution, attribution method, pid)``.
        ``attribution`` is true or false depending on the success of the
        attribution. ``attribution_method`` is a string with the
        name of the algorithm effectively used during the attribution.
        ``pid`` is the peak id of the isotopic peak used for the
        attribution.

    """
    # relative error for isclose method of numpy
    rtol = lambda_parameter * 1e-6

    # assign a list of candidates of all the peaks in the peaklist
    if candidates is None:
        if peaklist is None:
            raise ValueError("A peak list must be provided if candidates are "
                             "not provided.")

        if isinstance(peaklist, AttributedPeakList):
            peaklist = peaklist.get_nohits()

        candidates = assign_candidates(
            peaklist, formula_grid, returns_candidates_data=True,
            lambda_parameter=lambda_parameter)
        candidates["peak_idx"] = range(len(peaklist))

    # the main peak under attribution is supposed to be monoisotopic and
    # we will try to find an isotpic peak with a composition that matches
    # the existing candidates of the main peak
    peak_candidates_list = candidates.loc[peak.pid].cand_list
    peak_candidates_list = peak_candidates_list[~peak_candidates_list.isotopic]
    peak_candidates_composition = \
        peak_candidates_list.loc[:, formula_grid.species].values

    # init output
    attributed_peak = None
    isotope_pid = None

    # isotopic pattern informations
    # at the moment, only pattern with one isotope are supported
    pattern_grid = formula_grid[pattern_key]
    pattern_isotope = pattern_grid.isotopes[0]    
    # match = re.match(r"([0-9]+)([a-z]+)", symbol_pattern, re.I)
    elm_pattern = pattern_isotope.element.symbol

    # target mz from the main peak
    target_isotopic_mz = peak.mz + pattern_grid.mz_shift

    # find secondary peaks corresponding to the requested isotopic mz shift
    # look for the the secondary peak which is the closest to the mz shift
    delta_mz = np.abs(candidates.mz.values - target_isotopic_mz)
    argmin = np.argmin(delta_mz)
    delta_mz = delta_mz[argmin]

    # check if the closest secondary peak is in the error accepted range
    # TODO:
    # implementation may include to check if the relative intensity agrees
    # with the isotopic pattern

    if delta_mz <= rtol * target_isotopic_mz:
        pattern_data = candidates.iloc[argmin]
        isotope_pid = pattern_data.pid

        # check in the candidate list of the selected isotope mz, if
        # the formulas include the requested isotopic pattern
        is_pattern = [pattern_isotope.symbol in f
                      for f in pattern_data.cand_list.formula.values]
        pattern_candidate_list = pattern_data.cand_list.loc[is_pattern]

        if not pattern_candidate_list.empty:
            # isotopic pattern exist at the target mz shift

            # look for composition matching between isotopic peak (the
            # secondary peak) and the main peak (monoisotopic peak)
            compatible_formulas_pattern = list()
            compatible_formulas_peak = list()
            for _, row in pattern_candidate_list.iterrows():
                composition = row[formula_grid.species]
                composition[elm_pattern] += 1
                composition[pattern_isotope.symbol] -= 1

                compo_mask = (peak_candidates_composition ==
                              composition.values).all(axis=1)
                if any(compo_mask):
                    compatible_formulas_pattern.append(row)

                    peak_candidate = peak_candidates_list.loc[compo_mask]
                    compatible_formulas_peak.append(peak_candidate.squeeze())

            # if there is a matching between the main peak and isotopic
            # formulas => select the formula with the lowest error
            attribution = False
            if len(compatible_formulas_peak) == 1:
                peak_candidate = compatible_formulas_peak[0]
                attribution = True
            elif len(compatible_formulas_peak) > 1:
                attribution = True
                compatible_formulas = pd.DataFrame(compatible_formulas_pattern)
                argmin = compatible_formulas.err_ppm.abs().values.argmin()
                peak_candidate = compatible_formulas_peak[argmin]

            if attribution:
                composition = Composition.from_string(peak_candidate.formula)

                # for convenience, rename all column to intensity or
                # intensity
                attributed_peak = AttributedPeak(
                    formula=composition,
                    polarity=formula_grid.polarity,
                    attribution_method="isotopic_pattern",
                    exact_masses=formula_grid.exact_masses,
                    **peak.as_dict()
                )

    return attributed_peak, isotope_pid


def attribute_isotopes(
    attributed_peak: AttributedPeak,
    formula_grid: FormulaGrid,
    lambda_parameter: float = 1,
    candidates: pd.DataFrame = None,
    list_of_peaks: list = None,
    peaklist: Union[PeakList, AttributedPeakList] = None
) -> list[AttributedPeak]:
    """
    This method attributes the possible isotopes associated to a given
    mono-isotopic formula considering all the possible isotopes with a
    lowest error hypothesis.

    Args:
        attributed_peak (AttributedPeak): mono-isotopic formula for which
            we assign the existence of isotopes.
        formula_grid (FormulaGrid): Formula Grid.
        lambda_parameter (float): is the maximum error in ppm for the
            peak assignment.
        candidates (DataFrame): A dataframe containing the mz to
            attribute and the corresponding candidates. This dataframe
            is an output of the assign_candidate function.
        list_of_peaks (list): List of peak object associated to the
            candidates data frame.
        peaklist: (PeakList): if candidates are not provided, there are
            assigned from this peak list.

    Returns:
        A list of AttributedPeak objects with the new attributed
        peaks. If an AttributedPeakList was provided as argument, the
        new attributed peaks are append to the input attributed peak
        list.
    """
    # relative error for isclose method of numpy
    rtol = lambda_parameter * 1e-6

    if isinstance(peaklist, AttributedPeakList):
        attributed_peaks = [peak for peak in peaklist if peak.is_attributed]
        peaklist = peaklist.get_nohits()
    else:
        attributed_peaks = list()

    # assign a list of candidates of all the peaks in the peaklist
    if candidates is None:
        if peaklist is None:
            raise ValueError("A peak list must be provided.")

        candidates = assign_candidates(
            peaklist, formula_grid, returns_candidates_data=True,
            lambda_parameter=lambda_parameter)
        candidates["peak_idx"] = range(len(peaklist))

    for pattern_grid in formula_grid.values():
        if not pattern_grid.is_isotopic:
            # monoisotopic grid
            continue

        # check if the monoisotopic formula of the input peak contains
        # the elements of the considered isotopic substitution
        elts_in_formula = all([iso.element.symbol in attributed_peak.formula 
                               for iso in pattern_grid.isotopes])
        if not elts_in_formula:
            continue

        elts_not_null = all([attributed_peak.formula[iso.element.symbol] != 0
                             for iso in pattern_grid.isotopes])
        if not elts_not_null:
            continue

        # target mz
        mz_iso_theo = attributed_peak.mz + pattern_grid.mz_shift

        # index of closest peaks
        mask = np.isclose(candidates.mz.values, mz_iso_theo, atol=0., rtol=rtol)

        if not mask.any():
            # no any peak exist
            continue

        # select the closest m/z value
        select_mz = candidates.loc[mask]
        error = np.abs(select_mz.mz.values - mz_iso_theo)
        best_row = select_mz.iloc[error.argmin()]

        # filter the isotopes in the candidates' formulas of
        # the target peak that corresponds to the mz of the current
        # isotopes
        candidate_list = best_row["cand_list"]
        is_valid_isotopic_formula = [
            all([iso.symbol in f for iso in pattern_grid.isotopes])
            for f in candidate_list.formula.values
        ]
        candidate_list = candidate_list.loc[is_valid_isotopic_formula]

        # select the best candidates based on the lowest mz
        # deviation
        if not candidate_list.empty:
            if len(candidate_list) > 1:
                ix = candidate_list.err_ppm.abs().values.argmin()
            else:
                ix = 0

            candidate = candidate_list.iloc[ix]
            composition = Composition.from_string(candidate.formula)

            # validate if the elemental composition of the candidate found
            # is the same that the attributed_peak
            if not (composition.elemental_composition ==
                    attributed_peak.formula.elemental_composition):
                continue

            attributed_peaks.append(AttributedPeak(
                formula=composition,
                polarity=formula_grid.polarity,
                attribution_method="isotopes",
                exact_masses=formula_grid.exact_masses,
                **list_of_peaks[best_row.peak_idx].as_dict()
            ))

    return attributed_peaks


def isotopic_pattern_attribution(
    peaklist: Union[PeakList, AttributedPeakList],
    formula_grid: FormulaGrid,
    lambda_parameter: float = 1,
    use_SN: bool = True,
    use_isotopes: bool = True,
    isotopic_pattern: Tuple[int, int] = (6, 13),
    SN_threshold: float = 50,
) -> AttributedPeakList:
    """
    Method to perform the formula attribution considering the isotopic
    pattern attribution algorithm. The selection of the best candidate
    is determined by the identification of the isotopic pattern of the
    formula.

    If some isotopes and/or isotope combinations exists in the Formula
    Grid, the algorithm will search the corresponding patterns of the
    available isotopes (and isotope combinations) and will attribute
    the corresponding signals. However, if no isotope (neither the 13C)
    is defined in the Formula Grid, it will be not possible to execute
    the isotopic pattern algorithm, so the lowest error attribution is
    executed instead.

    Args:
        peaklist (pyc2mc.core.peak.PeakList): a pyc2mc Peaklist object
            which contains the peaklist to attribute.
        formula_grid (pyc2mc.formula_grid.FormulaGrid): Formula Grid. If
            no isotope is define in the FormulaGrid, the method attributes
            the candidates with the lowest error.
        lambda_parameter (float): is the maximum error distribution
            to be taken on the calculation for the peak assignment.
        use_SN (bool): if True, the cut_off criteria will be based on the
            signal to noise ratio and the attribution will be done
            decreasing SN.
        use_isotopes (bool): if True, the algorithm will assign
            the isotopes of the monoisotopic attributed formula.
        isotopic_pattern (tuple): a tuple which describes the atomic number
            and mass number (Z, A) of the isotope, it is (6, 13) for 13C
            by default.
        SN_threshold (float): threshold on the S/N ratio. If the S/N of
            a monoisotopic formula is lower than this threshold we
            consider that it is not possible to find an isotopic peak.

    Returns
        Returns an attributed peaklist pyc2mc.core.peak.AttributedPeakList
        object.
    """

    # TODO: because of this, use_isotopes is useless ???
    if not formula_grid.is_isotopic:
        # fall down to lowest error attribution
        message = ("No isotopic formula grid, fall down to lowest error "
                   "attribution.")
        warnings.warn(message, RuntimeWarning, stacklevel=2)
        return lowest_error_attribution(
            peaklist, formula_grid, lambda_parameter, use_isotopes=False)

    # set up peak list and save back already attributed peak
    if isinstance(peaklist, AttributedPeakList):
        attributed_peaks = [peak for peak in peaklist if peak.is_attributed]
        peaklist = peaklist.get_nohits()
    else:
        attributed_peaks = list()

    # TODO: for now, skip combinations and more than 2 isotopes
    # only single isotopic formula are considered
    Z, A = isotopic_pattern
    try:
        pattern_grid = formula_grid.get_pattern_grid(pattern={(Z, A): 1})
    except KeyError:
        raise ValueError(f"Requested isotopic pattern is {isotopic_pattern} "
                         "but this isotopie is not included in the "
                         "fromula grid.")

    # Cut-off factor estimation twice the minimum intensity
    iso_abundance = pattern_grid.isotopes[0].abundance
    elm_pattern = pattern_grid.isotopes[0].element.symbol
    # cut_off_factor = CUT_OFF_FACTOR
    cut_off_factor = 2 * np.min(peaklist.intensity) / iso_abundance

    # assign a list of candidates
    candidates = assign_candidates(
        peaklist, formula_grid, returns_candidates_data=True,
        lambda_parameter=lambda_parameter)

    # save back peak with no candidates
    mask = candidates.candidates.isna()
    no_hits_peaks = list()
    list_of_peaks = list()
    for peak, flag in zip(peaklist, mask):
        if flag:
            no_hits_peaks.append(peak)
        else:
            list_of_peaks.append(peak)
    
    candidates.dropna(axis="rows", subset="candidates", inplace=True)
    # save back peak idx
    candidates["peak_idx"] = range(len(list_of_peaks))
    # check if a monoisotopic candidate exists
    mono_exist = np.array([~cand_list.isotopic.all()
                           for cand_list in candidates.cand_list])

    # sort candidates data either by SN or by intensity
    if peaklist.SN_avail and use_SN:
        candidates.sort_values(by='SN', ascending=False, inplace=True)
    else:
        if use_SN:
            logging.warning("use_SN is True but S/N is not available")
        candidates.sort_values(by='intensity', ascending=False, inplace=True)

    # iterate the input peaklist
    while len(candidates) > 0:

        attribution = False
        df_index, row = next(candidates.iterrows())
        peak = list_of_peaks[row.peak_idx]

        # check if at list one monoisotopic candidate exists
        # cl_mono = row.cand_list[~row.cand_list['isotopic']]
        if not mono_exist[row.peak_idx]:
            # only isotopic candidates exists.
            no_hits_peaks.append(list_of_peaks[row.peak_idx])
            candidates.drop(index=df_index, inplace=True)
            continue

        # define the cut_off for considering intensity / SN as high
        if not peaklist.SN_avail or not use_SN:
            n_atom = np.nanmax(row.cand_list[elm_pattern].values)
            if n_atom == 0:
                # the element associated to the isotopic pattern does not
                # exist in the available candidates
                cut_off_condition = False
            else:
                cut_off_condition = row.intensity > (cut_off_factor / n_atom)
        else:
            cut_off_condition = row.SN > SN_threshold

        if cut_off_condition:
            # peak of high intensity or high S/N
            #   => implement isotopic pattern

            attributed_peak, isotope_pid = attribute_pattern(
                peak=peak,
                formula_grid=formula_grid,
                pattern_key=pattern_grid.key,
                lambda_parameter=lambda_parameter,
                candidates=candidates,
                peaklist=peaklist,
                list_of_peaks=list_of_peaks,
                # full_return=True,
            )
            attribution = False if attributed_peak is None else True
            if attribution:
                attributed_peaks.append(attributed_peak)

        if not attribution:
            # add the row to no_hits peaks
            no_hits_peaks.append(list_of_peaks[row.peak_idx])

        # remove the mz from the list
        candidates.drop(index=df_index, inplace=True)

        # attribute all isotopes of current formula
        if use_isotopes and attribution:
            attributed_isotope_peaks = attribute_isotopes(
                attributed_peak=attributed_peak,
                formula_grid=formula_grid,
                lambda_parameter=lambda_parameter,
                candidates=candidates,
                list_of_peaks=list_of_peaks,
            )

            # update the list of attributed peaks
            attributed_peaks.extend(attributed_isotope_peaks)

            # remove attributed isotopes peaks
            candidates.drop(
                index=[
                    attr_peak.pid for attr_peak in attributed_isotope_peaks],
                inplace=True
            )

    # complete the list of attributed peak with non attributed from nohit
    attributed_peaks.extend(no_hits_peaks)

    # return an attributed peak list object
    name = peaklist.name if "attributed" in peaklist.name else "attributed_" + peaklist.name
    return AttributedPeakList.from_list(
        attributed_peaks, polarity=formula_grid.polarity,
        name=name)
