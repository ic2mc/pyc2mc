#!/usr/bin/env python
# coding: utf-8

r"""The methods and classes of this module are use to describe the
regression models for the recalibration. The main class is called
RecalibrationModels, and provide the list of the available models.

Implemented models are the specific Ledford and Masselon equations, based
on the frequencies. The Ledford equation reads:

.. math::

    (m/z) = \frac{A}{f} + \frac{B}{f^2}

where A and B are the parameters and f is the frequency.

The Masselon equation reads:

.. math::

    (m/z) = \frac{A}{f} + \frac{B}{f^2} + \frac{C I}{f^2}

This model is an extension of the Ledford equation with an additional
parameter, C. The third term depends on the intensity, I.

Other available models are simple polynomial functions of any degree such
as:

.. math::

    (m/z) = A (m/z)^2 + B (m/z) + C
        
For each model, it is possible to consider, or not, a constant term using
the ``fit_intercept`` flag. This flag is a bool and can turn on or off 
a constant term in the model."""

from enum import Enum, unique
import numpy as np
from numpy.typing import ArrayLike

__all__ = ["polynomial_function", "vandermonde_matrix", "ledford", "masselon",
           "ledford_matrix", "masselon_matrix", "RecalibrationModels"]


def polynomial_function(x: ArrayLike, *params, fit_intercept: bool = True,
                        deg: int = 2) -> ArrayLike:
    """ polynomial model. The params are in decreasing order of the power of
    x. A second order polynomial reads: 
    ``params[0] x ** 2 + params[1] x + params[2]
    
    Args:
        x (array): m/z values
        params (floats): the parameters of the polynom
        fit_intercept (bool): if True (default), a constant term is included
        deg (int): polynomial degree
    
    Retunrs:
        array of the polynomial values.
    """
    if fit_intercept:
        poly = [params[i] * x ** (deg - i) for i in range(deg + 1)]
    else:
        poly = [params[i] * x ** (deg - i) for i in range(deg)]
    
    return np.sum(poly, axis=0)


def vandermonde_matrix(x: ArrayLike, fit_intercept: bool = True,
                       deg: int = 2) -> ArrayLike:
    """ polynomial model. The params are in decreasing order of the power of
    x. A second order polynomial reads: 
    ``params[0] x ** 2 + params[1] x + params[2]``

    Args:
        x (array): m/z values
        fit_intercept (bool): if True (default), a constant term is included
        deg (int): polynomial degree
    
    Returns:
        The Vandermonde matrix for least square regression
    """
    if fit_intercept:
        return np.vstack([x ** (deg - i) for i in range(deg + 1)]).T
    else:
        return np.vstack([x ** (deg - i) for i in range(deg)]).T


def ledford(f, A, B, C=0) -> ArrayLike:
    """ Two terms Ledford equation with an optional C constant
    Mass eq. from ICR freq. Ledford et al. (1984) """
    return A / f + B / (f**2) + C


def ledford_matrix(f, fit_intercept: bool = False) -> ArrayLike:
    """ X-walking Matrix from ICR freq. Ledford et al. (1984)"""
    if fit_intercept:
        return np.vstack((1 / f, 1 / f**2, np.ones_like(f))).T
    else:
        return np.vstack((1 / f, 1 / f**2)).T


def masselon(fi, A, B, C, D=0) -> ArrayLike:
    """ Three terms Masselon equation, extension of Ledford equation 
    including intensities.
    Mass eq. from ICR freq. Masselon et al. (2002)"""
    f, intens = fi  # tuple of (frequency, intensity)
    return A / f + B / f**2 + C * intens / f**2 + D


def masselon_matrix(fi, fit_intercept: bool = False) -> ArrayLike:
    """ X-walking Matrix from ICR freq. Masselon et. al. (2002)
    
    """
    f, intens = fi  # tuple of (frequency, intensity)
    if fit_intercept:
        return np.vstack((1 / f, 1 / f**2, intens / (f**2), np.ones_like(f))).T
    else:
        return np.vstack((1 / f, 1 / f**2, intens / (f**2))).T


@unique
class RecalibrationModels(Enum):
    """
    This class defines all the possible values for the polyfunction
    argument. In addition the class could return the evaluated model
    and the matrix of parameters for each polyfunction value.

    The class could be instantiated by giving the order of the 
    allowed polynomial functions (1, 2 or 3) or the name of the 
    walking equations: 'two_walking' or 'three_walking'.

    # TODO: should be better to have a consistent type over the Enum and 
    # and make the Enum inherit from string => better for toml export
    """
    LEDFORD = 'ledford'  # Ledford equation (A / f + B / f^2)
    MASSELON = 'masselon'  # Masselon equation (A / f + B / f^2 + C I / f^2)
    POLYNOMIAL = "polynomial"
    # linear = 1
    # quadratic = 2
    # cubic = 3

    def __call__(self, X: ArrayLike, *params, deg: int = 2, 
                 fit_intercept: bool = True) -> ArrayLike:
        model = self.get_model(fit_intercept=fit_intercept, deg=deg)
        if self == RecalibrationModels.POLYNOMIAL:
            return model(X, *params, fit_intercept=fit_intercept, deg=deg)
        else:
            return model(X, *params)
        
    @classmethod
    def from_alias(cls, alias: str) -> "RecalibrationModels":
        """ Include a little bit of flexibility for the names and make it
        non case sensitive. """
        alias = alias.upper()
        for member in cls.__members__:
            if alias in member:
                return cls.__members__[member]
        
        raise ValueError(f"'{alias}' is not a valid model ({cls.__members__}).")
    
    @property
    def need_frequency(self) -> bool:
        """ Return True if the model needs frequency """
        if (self == self.LEDFORD) or (self == self.MASSELON):
            return True
        else:
            return False
    
    @property
    def need_intensity(self) -> bool:
        """ Return True if the model needs intensity """
        if self == self.MASSELON:
            return True
        else:
            return False

    def get_n_params(self, fit_intercept: bool = False, deg: int = None) -> int:
        """ Give the number of parameters 
        
        Args:
            fit_intercept (bool): if True, the method will fit the 
                intercept, if False, the intercept will be taken as
                zero.
            deg (int): Degree of the polynomial

        Returns:
            The number of parameters
        
        """
        if self.value == self.LEDFORD.value:
            return 3 if fit_intercept else 2
        elif self.value == self.MASSELON.value:
            return 4 if fit_intercept else 3
        elif self.value == self.POLYNOMIAL.value:
            return deg if fit_intercept else deg - 1

    def get_matrix(self, X: ArrayLike, fit_intercept: bool = True, 
                   deg: int = None) -> ArrayLike:
        """
        This method creates the matrix of coefficients of X for the
        selected model used to perform the least squares minimization.

        Args:
            X (ArrayLike): The array of X.
            fit_intercept (bool): if True, the method will fit the 
                intercept, if False, the intercept will be taken as
                zero.
        
        Returns:
            The matrix of coefficients of X.
        """
        if self.value == self.LEDFORD.value:
            return ledford_matrix(X, fit_intercept=fit_intercept)
        elif self.value == self.MASSELON.value:
            return masselon_matrix(X, fit_intercept=fit_intercept)
        elif self.value == self.POLYNOMIAL.value:
            return vandermonde_matrix(X, deg=deg, fit_intercept=fit_intercept)

    def get_model(self, fit_intercept: bool = True, deg: int = 2):
        """
        This function finds the correct model according to the
        method that was defined when the class was intantiated.

        Args:
            fit_intercept (bool): if True, the method will fit the 
                intercept, if False, the intercept will be taken as
                zero.

        Returns:
            A function of the Recalibration Model according to 
            the defined information when instantiated the class and
            the arguments given through this function. 
        """
        if self.value == self.LEDFORD.value:
            return ledford
        elif self.value == self.MASSELON.value:
            return masselon
        elif self.value == self.POLYNOMIAL.value:
            return polynomial_function
