# coding: utf-8


"""
This module contains the methods to find the kendrick series of a 
peaklist.
"""

from typing import Union
import numpy as np
import pandas as pd

from pyc2mc.core.peak import PeakList
from pyc2mc.core.kendrick import (KendrickSeries, KendrickSeriesList,
                                  AttributedKendrickSeries)
from pyc2mc.core.formula import Formula, KendrickBuildingBlock

CH2 = Formula.from_string("C1 H2")


def get_kendrick_series(
    peaklist,
    lambda_parameter: float = 1,
    min_length: int = 2,
    building_block: Union[KendrickBuildingBlock, str, float, Formula] = CH2,
    use_SN: bool = True,
    threshold: float = None,
    limit_number: int = None,
    n_max_hole: int = None,
) -> KendrickSeriesList:
    """ 
    Method to find the kendrick series on a MS peaklist.

    Args:
        peaklist (pyc2mc.core.PeakList): PeakList object.
        lambda_parameter (float): is the maximum error distribution
            to be taken on the calculation for the peak assignment.
        min_length (int): is the minimum length to accept
            a Kendrick Series or reject it.
        building_block (float, str, Formula): Kendrick Building Block
            formula, or mass. If not defined, C1 H2 mass is taken by 
            default.
        use_SN (bool): if True, the S/N ratio is used to sort the peaklist.
            If False, the peaklist is sorted according to intensity.
        threshold (float): A threshold to group Kendrick Series that 
            contains peaks with intensities over a threshold percentage
            with respect to the highest intensity. It is None by default.
        limit_number (int): limit the number of kendrick series to a 
            maximum number.
        n_max_hole (int): maximum number of missing peak in a series. If
            the number of missing peaks is equal to n_max_hole
            the building of the series is stopped.

    Returns a KendrickSeriesList object with the found Kendrick Series
    of the corresponding peaklist.
    """
    building_block = KendrickBuildingBlock(building_block)
    intens_values = peaklist.intensity

    # number of required series
    if limit_number is None:
        limit_number = len(peaklist)

    # number of accepted missing peaks
    if n_max_hole is None:
        # all hole lengths are permitted
        n_max_hole = len(peaklist)
    else:
        n_max_hole = int(n_max_hole)

    # cut off value on peak intensity
    cut_off = 0
    if threshold:
        cut_off = np.nanmax(intens_values) * threshold

    # compute kendrick mass
    km_values = peaklist.get_kendrick_masses(building_block)
    low_km = km_values.min()
    low_km -= low_km * 1e-7  # -0.1 ppm to include lowest value
    high_km = km_values.max()
    high_km += high_km * 1e-7 # + 0.1 ppm to include highest value

    # sort according to SN if avail and asked or intensity
    if use_SN and peaklist.SN_avail:
        argsort = np.argsort(peaklist.SN)
    else:
        argsort = np.argsort(intens_values)
    # sort descending
    argsort = argsort[::-1]

    peaklist = [peaklist[i] for i in argsort]
    intens_values = intens_values[argsort]
    km_values = km_values[argsort]

    # integer part of kendrick mass
    knm_values = np.floor(km_values).astype(np.uint16)
    knm_group_keys = set(np.unique(knm_values))
    peak_idx = list(range(len(peaklist)))

    # grouped by kendrick integer mass
    df_grouped = pd.DataFrame({"knm": knm_values, "km": km_values,
                               "peak_idx": peak_idx})
    df_grouped = df_grouped.groupby("knm")
    knm_groups = dict()
    for knm in knm_group_keys:
        grouped_data = df_grouped.get_group(knm)
        knm_groups[knm] = {
            "km": grouped_data["km"].values,
            "peak_idx": grouped_data["peak_idx"].values
        }
    del df_grouped

    removed_idx = set()
    kendrick_series_list = list()
    non_grouped_peaks = list()

    # loop over peaks from most abundant
    n_peaks = len(peaklist)
    jdx = 0
    while len(removed_idx) < n_peaks:
        # find next first peak of the series
        while jdx in removed_idx:
            jdx += 1
        removed_idx.update({jdx})
        ks_peaks = [peaklist[jdx]]

        if intens_values[jdx] < cut_off:
            # all remaining peak are considered as non grouped because
            # peaks are sorted according to intensity and intensity of
            # following ones will be smaller
            non_grouped_peaks.extend([
                peaklist[i] for i in peak_idx
                if i not in removed_idx
            ])
            break

        # initialize new series
        initial_km = km_values[jdx]
        current_km = initial_km  # initialize the current_km variable for this series
        condition = low_km  # reverse mode, sign = -1
        n_building_block = 1  # initialize the counter over building block

        # look for a series in backward and then forward way from the initial
        # km value
        for sign in [-1, 1]:
            n_missing_peak = 0
            while sign * (current_km + sign * building_block.factor) < sign * condition:
                current_km = initial_km + sign * n_building_block * building_block.factor
                current_keys = [np.floor(current_km).astype(np.uint16)]

                # check frontier
                round_distance = abs(current_km - np.round(current_km)) / current_km
                round_distance *= 1e6  # ppm
                if round_distance <= lambda_parameter:
                    # need to look at previous or next integer mass
                    if current_km - np.round(current_km) > 0:
                        current_keys.append(current_keys[0] - 1)
                    else:
                        current_keys.append(current_keys[0] + 1)

                new_peak = False
                if any([knm in knm_group_keys for knm in current_keys]):

                    idx = find_kpeak(current_keys, knm_groups, current_km, 
                                     lambda_parameter)
                    if idx is not None and idx not in removed_idx:
                        # include peaks in the series
                        removed_idx.update({idx})
                        ks_peaks.append(peaklist[idx])
                        new_peak = True

                n_building_block += 1

                # series continuity check
                if new_peak:
                    # series is continuing
                    n_missing_peak = 0
                else:
                    # a peak is missing
                    n_missing_peak += 1
                    if n_missing_peak > n_max_hole:
                        break

            # now, search peak in forward way
            current_km = initial_km
            condition = high_km  # switch to sign = +1
            n_building_block = 1

        if len(ks_peaks) >= min_length:
            # kendrick series is long enough and valid
            name = (f"Series {len(kendrick_series_list)}" 
                    f"({str(building_block.formula)})")
            
            # check if one or more peaks were already attribued
            attributed = any([peak.is_attributed for peak in ks_peaks])
            if attributed:
                polarity = set([peak.polarity for peak in ks_peaks
                                if peak.is_attributed])
                if len(polarity) > 1:
                    raise ValueError(
                        f"All peaks don't have the same polarity {polarity}")
                polarity = polarity.pop()
                ks = AttributedKendrickSeries.from_list(
                    ks_peaks,
                    polarity=polarity,
                    building_block=building_block,
                    sort_mz=True,
                    name=name
                )
            else:
                ks = KendrickSeries.from_list(
                    ks_peaks,
                    building_block=building_block,
                    sort_mz=True,
                    name=name
                )

            kendrick_series_list.append(ks)

            if len(kendrick_series_list) >= limit_number:
                # the required number of series was found, skip other peaks
                non_grouped_peaks.extend([peaklist[i] for i in peak_idx
                                          if i not in removed_idx])
                break  # stop searching series because of the limit_number constrain
        else:
            non_grouped_peaks.extend(ks_peaks)

    if len(non_grouped_peaks) > 0:
        non_grouped_peaklist = PeakList.from_list(non_grouped_peaks)
    else:
        non_grouped_peaklist = PeakList([], [], name="non_grouped_peaklist")

    return KendrickSeriesList(
        kendrick_series_list=kendrick_series_list,
        non_grouped_peaklist=non_grouped_peaklist,
        building_block=building_block)


def find_kpeak(knm_key, knm_groups, current_km, lambda_parameter) -> int:
    """ Finds a peak of the kendrick series in a given kendrick mass group.

    Args:
        knm_key (int): integer part of the kendrick mass 
        km_groups (dict): Dict of grouped peaks by integer kendrick mass
        current_km (float): The estimated value to search a peak of the kendrick
            series in the peaklist.
        lambda_parameter (float): is the searching window in ppm.
    """
    idx = list()
    km_candidates = list()
    for knm_key_i in knm_key:
        if knm_key_i not in knm_groups:
            continue
        km = knm_groups[knm_key_i]["km"]
        km_min = current_km - current_km * lambda_parameter / 1e6
        km_max = current_km + current_km * lambda_parameter / 1e6

        # these index are indexes in the knm group
        idx_i, = np.where((km > km_min) & (km < km_max))

        km_candidates.append(km[idx_i])
        # get back true peaklist indexes
        idx.append(knm_groups[knm_key_i]["peak_idx"][idx_i])

    idx = np.concatenate(idx)
    km = np.concatenate(km_candidates)

    if len(idx) > 1:
        error = np.abs(km - current_km) / current_km
        idx_min = np.argmin(error)
        idx = idx[idx_min]
    elif len(idx) == 0:
        idx = None
    else:
        idx = idx[0]

    return idx
