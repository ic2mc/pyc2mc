# coding: utf-8

"""
This module contains the utility functions or general mathematical tools.
"""

import re
from typing import Any, Union
import numpy as np
import collections
from pathlib import Path
from numpy.typing import ArrayLike
from scipy.signal import ellip, filtfilt


def find_nearest(array, value):
    """
    Find the closest number in an array python.
    """
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return array[idx], idx


def smoothing_method(N, Wn, signal):
    """ smoothing function using eliptical filter """
    b, a = ellip(N, 0.01, 120, Wn)
    return filtfilt(b, a, signal, padlen=50)


def outliers_detection_MAD(array: ArrayLike, threshold=3.5):
    """ Detection of outliers based on the median absolute deviation.
    Boris Iglewicz and David Hoaglin (1993), "Volume 16: How to Detect and
    Handle Outliers", The ASQC Basic References in Quality Control:
    Statistical Techniques, Edward F. Mykytka, Ph.D., Editor. 
    
    Args:
        y (ArrayLike): y values
        threshold (float): The modified z-score to use as a threshold. 
            Observations with a modified z-score (based on the median 
            absolute deviation) greater than this value will be 
            classified as outliers.

    Returns:
        The indexes of the outliers.           
            
    """
    array = np.asarray(array)
    if len(array.shape) == 1:
        array = array[:, None]
    median = np.median(array, axis=0)
    diff = np.sum((array - median)**2, axis=-1)
    diff = np.sqrt(diff)
    med_abs_deviation = np.median(diff)

    modified_z_score = 0.6745 * diff / med_abs_deviation

    return np.nonzero(modified_z_score > threshold)


def outliers_detection_TSE(y: ArrayLike, x: ArrayLike, k: float = 3.0):
    """
    Estimates the outliers using the Thiel-Sen Estimator which is
    a robust determination of the dispersion when the residuals
    present heteroskedasticity. The method is based on the
    calculation of the slopes for each point and a determination
    of a median slope that is the estimator of the increasing
    residual's dispersion. Using the estimator, the method returns
    the indexes of the outliers.

    Args:
        y (ArrayLike): y data,
        x (ArrayLike): x data
        k (float): Tuning parameter that determines the maximum
            deviation to detect the outliers.

    Returns:
        The indexes of the outliers.
    """

    y = np.asarray(y)
    x = np.asarray(x)

    # Data is adjusted to a second order polynomial function 
    popt = np.polyfit(x, y, 2)

    # residuals of the extended calibration list and statistic desciption
    # of the residuals are obtained
    residuals = (y - popt[0] * x**2 - popt[1] * x - popt[0] * np.ones_like(x))
    mean = np.mean(residuals)
    MAD = np.median(np.abs(residuals - np.median(residuals)))
    
    # Calculate the slopes by the Thiel-Sen Estimator
    deltas_x = np.subtract.outer(x, x)
    deltas_fx = np.subtract.outer(residuals, residuals)
    slopes = deltas_fx[deltas_x > 0] / deltas_x[deltas_x > 0]
    
    # take only negative slopes
    slopes_neg = slopes[slopes < 0]
    slope_neg = np.median(slopes_neg)

    # take only positive slopes
    slopes_pos = slopes[slopes > 0]
    slope_pos = np.median(slopes_pos)

    # calculate the upper and lower limits using the Median Absolute
    # Deviation
    upper_limit = mean - slope_neg * x + k * MAD
    lower_limit = mean - slope_pos * x - k * MAD
    # find the indexes of the outliers
    idx = np.where(residuals < lower_limit)
    idx = np.append(idx, np.where(residuals > upper_limit))

    return idx


def fill_with_na(arr: ArrayLike, idx: ArrayLike, start: int, end: int,
                 na_rep: Any = np.nan, dtype: type = np.float64) -> ArrayLike:
    """
    Return a new array with the value of ``arr`` at the indices ``idx``
    with a length corresponding
    to ``end - start + 1``. Missing values are filed with the ``na_rep``.
    
    Args:
        arr (ArrayLike): an array
        start (int): index of first value in output array
        end (int): index of last value in output array
        na_rep (object): missing data representation.
        dtype (type): numpy type of the output array
    """

    filled_array = np.full((end - start + 1), na_rep, dtype=dtype)
    filled_array[idx - start] = arr
    return filled_array


def get_file_index(filename: str) -> int:
    """
    Return the index of the given filename in order to sort the files.
    The csv/xlsx files are supposed to be named such as `basenameXX.ext`
    where XX is an integer. If there are several integers in the name only
    the LAST one is returned.
    For example the index of the file 'OV5-49.csv' will be 49.

    """
    return int(re.findall(r"(\d+)", str(filename))[-1])


def get_file_list(directory, extension: str = None, sorting: bool = True):        
    """ Look in the given directory and return all files with a given
    extension. The code assume the following: all files have the same basename 
    and extension with an index
    such as `basename_pathXext` so all scans should be a continuum.

    Args:
        directory_path (str): directory path of the files
        extension (str): file extension, default to the most abundant
        verbose (bool): If True, print some information
        sorting (bool): if true, sort files by index. Warning: files
            must have an index number like: filename-00.asc, where
            00 is the index.

    Returns:
        Returns the list of file names.
    """
    directory_path = Path(directory)
    if not directory_path.exists() or not directory_path.is_dir():
        raise ValueError(
            f"Directory '{directory_path}' does not exist or is not a directory.")
    
    if extension is None:
        # select the extension which is the most represented
        extensions = [file.suffix for file in directory_path.iterdir()]
        extension, _ = collections.Counter(extensions).most_common(1)[0]

    # TODO
    # file_fmt = get_file_format(extension)
    # if file_fmt is None:
    #     raise NotImplementedError(
    #         f"Error reading files from {directory_path}. Read file "
    #         f"with extension '{extension}' not yet available.")

    filenames = [
            filename 
            for filename in directory_path.glob(f"*{extension}")
            if filename.is_file() and (filename.stem[0] not in [".", "_"])
        ]

    if sorting:
        filenames = sorted(filenames, key=get_file_index)

    if len(filenames) == 0:
        raise FileNotFoundError(
            f"No files found with extension '{extension}' in the "
            f"directory '{directory_path}'."
        )

    return filenames


def gaussian_weights(x: Union[float, ArrayLike], g0: float = 1e2, 
                     width: float = 2e-3,
                     loc: float = 10) -> Union[float, ArrayLike]:
    """ a gaussian like function with three parameters that controls the
    widths, the position and the starting value.
    
    Args:
        x (float, ArrayLike): the x value
        g0 (float): mainly affect the value at x = 0
        width (float): mainly affect the width
        loc (float): mainly affect the position

    Returns:
        the y value of the function
    """
    w = (x ** 2 + g0) * np.exp(-width * (x - loc) ** 2)
    return w / w.max()


def step_weights(x: Union[float, ArrayLike], x_min: float = 0, 
                 x_max: float = 50) -> Union[float, ArrayLike]:
    """ This is a step function equal to 1 between x_min and x_max and
    zero elsewhere 
    
    Args:
        x (float, ArrayLike): the x value
        x_min, x_max (float): the interval in between the weight is 1

    Returns:
        The weights

    """
    return np.where((x_min <= x) & (x <= x_max), 1, 0)

def generate_bin_centers_array(data) -> np.array:
    """
    Generate an array of bin centers from an array of bin edges.

    Args:
        data (ArrayLike): An array of bin edges.
        
    Returns:
        An array of bin centers.
    """
    arr = []

    # Traverse the list in steps of 2 and calculate bin centers
    for idx in range(0, len(data) - 1, 2):
        bin_center = (data[idx] + data[idx + 1]) / 2.0
        arr.append(bin_center)

    return np.array(arr)