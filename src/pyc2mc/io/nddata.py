#!/usr/bin/env python
# coding: utf-8

"""
This module provides input/output tools for experimental data.
"""

from typing import Sequence
from pathlib import Path

import numpy as np

from pyteomics import mzxml, mzml

from pyc2mc.core.peak import PeakList

__all__ = ["read_MZxml", "read_mzml"]


def read_MZxml(input_path: str, **kwargs) -> Sequence[PeakList]:
    """
    Reading function for a set of scan in time-resolved, using the
    MZxml file. The mzxml.read() method from pyteomics is used to
    iteratively read the file.

    Args:
        input_path (str): directory and name of the file.
        **kwargs as defined in mzxml.read()

    Returns:
        A list of couple [mz, intensities] for each scan as float
        numpy arrays.
    """
    input_path = Path(input_path)
    if not input_path.exists():
        raise FileNotFoundError(f"MZxml file not found {input_path}.")

    # input_path must be a string
    reader = mzxml.read(str(input_path), **kwargs)

    scans = list()
    for _, scan in enumerate(reader):
        mz = np.array(scan['m/z array'], dtype=np.float64)
        intensity = np.array(scan['intensity array'], dtype=np.float64)
        scans.append(PeakList(mz, intensity))

    return scans


def read_mzml(input_path: str, **kwargs) -> Sequence[PeakList]:
    """
    Reading function for a set of scan in time-resolved, using the
    mzml file. The mzml.read() method from pyteomics is used to
    iteratively read the file.

    Args:
        input_path (str): directory and name of the file.
        **kwargs as defined in mzml.read()

    Returns:
        A list of PeakList objects.
    """
    input_path = Path(input_path)
    if not input_path.exists():
        raise FileNotFoundError(f"MZxml file not found {input_path}.")

    # input_path must be a string
    reader = mzml.read(str(input_path), **kwargs)

    scans = list()
    for _, scan in enumerate(reader):
        mz = np.array(scan['m/z array'], dtype=np.float64)
        intensity = np.array(scan['intensity array'], dtype=np.float64)
        idx = np.where(intensity > 0)
        scans.append(PeakList(mz[idx], intensity[idx]))

    return scans
