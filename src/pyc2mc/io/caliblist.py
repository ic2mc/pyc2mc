#!/usr/bin/env python
# coding: utf-8

"""
This module provides the methods to read calibration list when they
were exported as files. The suported extenstions are:

csv
    A PyC2MC or predator calibration list.

txt
    A text file calibration list which contains just formulas and
    internal calibrant masses.

ref
    DataAnalysis calibration list.

"""

import re
import warnings
import numpy as np
import pandas as pd
from pathlib import Path
from typing import Union

from pyc2mc.core.formula import Formula
from pyc2mc.core.calibration import CalibList

__all__ = ["read_caliblist", "read_ref", "read_txt", "read_csv"]

# supported file formats
FILE_FORMATS = {
    "csv": [".csv"],
    "ref": [".ref"],
    "txt": [".txt"],
}

# regex to read molecular formulas in ref files
mol_patt = re.compile(r"([A-Z][a-z]?)(\d*)")


def get_file_format(ext: str, fmt: str = None) -> str:
    """
    Try to determine the file format according to the extension.

    Args:
        ext (str): extension of the file.
        fmt (str): the format if defined.
    
    Returns:
        The file format according to the defined FILE_FORMATS.
    """
    
    file_fmt = None

    if not fmt:
        # format is not provided, try to detect file format
        for key, exts in FILE_FORMATS.items():
            if ext in exts:
                file_fmt = key
                break

    elif str(fmt).lower().strip() in FILE_FORMATS:
        # format is provided and is a valid format
        file_fmt = str(fmt).lower().strip()
    
    return file_fmt


def read_caliblist(input_path: Union[str, Path],
                   fmt: str = "", name: str = None, **kwargs):
    """
    General reading function to open calibration lists, on the
    supported formats, which are: ref, txt and csv.

    Args:
        input_path (str, Path): directory and name of the file.
        fmt (str): the extension of the file, or file format, 
            e.g. "txt", "ref", "csv".
        name (str): A name for the calibration list
        kwargs: as defined in read_csv method from pandas.
    
    Returns:
        A CalibList object.
    """
    
    # check file exists
    path = Path(input_path)
    if not path.is_file():
        raise FileNotFoundError(f"File {path} is not a regular file.")

    # determine file format
    file_fmt = get_file_format(path.suffix.lower(), fmt)    
    if file_fmt is None:
        raise ValueError(f"Format of file {path} was not identified or is "
                         "not supported. Supported file formats are:\n"
                         f"{FILE_FORMATS}")
    
    # read file
    try:
        if file_fmt == "ref":
            cl = read_ref(path, name=name, **kwargs)
        elif file_fmt == "csv":
            cl = read_csv(path, name=name, **kwargs)
        elif file_fmt == "txt":
            cl = read_txt(path, name=name, **kwargs)
    except pd.errors.ParserError:
        raise ValueError(f"Error when reading file {path} with format "
                         f"{file_fmt}. File contents and format may be "
                         "inconsistent.")
    
    return cl


def read_txt(input_path, name: str = None, **kwargs):
    """
    Reading function for the calibration list in .txt format. This is a  
    simple  wrapper above the pandas read_csv function assuming the file 
    contains the m/z values on a single column. 
    All additional arguments are passed to the read_csv function of pandas.

    Args:
        input_path (str): directory and name of the file.
        name (str): A name for the calibration list
        kwargs: as defined in read_csv method from pandas.
    
    Returns:
        A CalibList object.
    """
    
    input_path = Path(input_path)
    df = pd.read_csv(input_path,
                     names=["mz"], dtype=np.float64, usecols=(0),
                     **kwargs)

    if name is None:
        name = input_path.name
    return CalibList(mz_theo=df.mz.values, name=name)


def read_ref(input_path, name: str = None, **kwargs):
    """
    Reading function for the calibration list in .ref format. This is a  
    simple  wrapper above the pandas read_csv function assuming the file 
    contains the Ion Formula, m/z values and the charge on three columns,
    separated by white spaces. 
    All additional arguments are passed to the read_csv function of pandas.

    Args:
        input_path (str): directory and name of the file.
        name (str): A name for the calibration list
        kwargs: as defined in read_csv method from pandas.
    
    Returns:
        A CalibList object.
    """

    # polarity is not supposed to be in kwargs for ref files
    if "polarity" in kwargs:
        kwargs.pop("polarity")
    
    if 'skiprows' not in kwargs:
        kwargs['skiprows'] = 3

    input_path = Path(input_path)
    df = pd.read_csv(input_path, sep=r"\s+",
                     names=["formula", "mz_theo", "charge"],
                     usecols=(0, 1, 2),
                     **kwargs)
    
    # read polarity
    try:
        df["charge"] = df.charge.map({"1+": 1, "1-": -1})
    except AssertionError:
        warnings.warn("Unable to read charge state of the calibration list",
                      UserWarning, stacklevel=2)
        df["charge"] = np.nan

    polarity = set(df["charge"].values)
    if len(polarity) == 1:
        polarity = polarity.pop()
    else:
        raise ValueError(f"Several ionisation mode coexists: {polarity}")

    # read formulas
    formulas = list()
    for formula in df["formula"].values:
        composition = dict(mol_patt.findall(formula))
        if len(composition) == 0:
            warnings.warn(f"Unable to read molecule formula '{formula}'.",
                          UserWarning, stacklevel=2)
            formulas.append("")
        else:
            composition = {k: int(v) if len(v) > 0 else 1
                           for k, v in composition.items()}
            formulas.append(Formula(composition))

    if name is None:
        name = input_path.name

    return CalibList(mz_theo=df.mz_theo.values, formula=formulas,
                     polarity=polarity, name=name)


def read_csv(input_path, polarity: int = None, name: str = None, **kwargs):
    """
    Reading function for the calibration list in .csv format. This is a  
    simple  wrapper above the pandas read_csv function assuming the file 
    contains the Ion Formula, m/z values and the charge on three columns,
    separated by white spaces. 
    All additional arguments are passed to the read_csv function of pandas.

    Args:
        input_path (str): directory and name of the file.
        polarity (int): ionization charge
        name (str): A name for the calibration list
        kwargs: as defined in read_csv method from pandas.

    Returns:
        A CalibList object.
    """

    try:
        df = pd.read_csv(input_path)
    except TypeError:
        raise TypeError('Unrecognized CSV format.')

    if name is None:
        name = input_path.name

    if df.columns.size == 1: # assume Predator CalibList

        if polarity is None:
            # polarity is not available in predator csv format
            warnings.warn("Polarity/charge is not known.", UserWarning,
                          stacklevel=2)
            polarity = np.nan

        kwargs['skiprows'] = 1
        df = pd.read_csv(input_path, **kwargs)
        n_cols = len(df.columns)
        kwargs['skiprows'] = 2
        kwargs['usecols'] = range(n_cols)
        kwargs['names'] = ['pid', 'frequency', 'mz', 'meassured',
                           'err_ppm', 'scaled_intensity', 'intensity', 'SN']
        df = pd.read_csv(input_path, **kwargs)
        return CalibList(mz_theo=df.mz.values, frequency=df.frequency.values,
                         intensity=df.intensity.values, name=name,
                         polarity=polarity)
    else:
        # assume calibration list from pyc2mc
        csv_polarity = set(df["polarity"].values)
        if len(csv_polarity) == 1:
            csv_polarity = csv_polarity.pop()
            if polarity is not None and csv_polarity != polarity:
                raise ValueError(
                    "Inconsistent values of polarity/charge. "
                    f"From file, the charge is '{csv_polarity}' but you set "
                    f"as argument of the function polarity '{polarity}'.")
            else:
                polarity = csv_polarity
        else:
            raise ValueError(f"Several ionisation mode coexists: {polarity}")

        return CalibList(mz_theo=df.mz_theo.values, formula=df.formula.values,
                         mz_exp=df.mz_exp.values,
                         frequency=df.frequency.values,
                         intensity=df.intensity.values,
                         polarity=polarity,
                         name=name)
