# coding: utf-8

""" This module provide the read_data function that aggregates all reading
function of PyCM2C and return a pyc2mc data object. """

import logging
from pathlib import Path

from pyc2mc import LOGGER_NAME
from pyc2mc.core.typing import PyC2MCPeakObject
from pyc2mc.core import SampleType
from pyc2mc.core.peak import AttributedPeakList
from pyc2mc.io.peaklist import read_peaklist
from pyc2mc.time_dependent.td_data import AttributedTimeDependentData, TimeDependentData

logger = logging.getLogger(LOGGER_NAME)


def read_data(**kwargs) -> PyC2MCPeakObject:
    """ This function is a general wrapper that select the suitable 
    reading function in order to import data in pyc2mc.

    Args:
        path: Path to be read
        sample_type: The type of sample from SampleType
        filenames: List of files

    TODO: will be refactor after simplification between PeakList/Attributed
    """

    try:
        sample_type = kwargs.pop("sample_type")
    except KeyError:
        raise KeyError("Sample type is missing.")
    sample_type = SampleType(sample_type)

    # single file or folder
    if path := kwargs.get("path"):
        kwargs.pop("path")
        path = Path(path)
        logger.info("Read in path: %s", str(path))

        if sample_type == SampleType.raw_1D:
            logger.info("read peaklist in: %s", str(path))
            return read_peaklist(path, **kwargs)

        if sample_type == SampleType.att_1D:
            return AttributedPeakList.from_file(path, **kwargs)

        if sample_type == SampleType.raw_TD:
            if path.is_dir():
                return TimeDependentData.from_directory(path, **kwargs)
            elif path.suffix.lower() == ".mzml":
                return TimeDependentData.from_mzml(path, **kwargs)
            elif path.suffix.lower() == ".mzxml":
                return TimeDependentData.from_MZxml(path, **kwargs)

        if sample_type == SampleType.att_time_dependend:
            if path.is_dir():
                return AttributedTimeDependentData.from_directory(
                    path, **kwargs
                )

            raise ValueError(
                f"I cannot read input file or path:\n{path}")

    elif filenames := kwargs.get("filenames"):
        # a list of file is provided
        kwargs.pop("filenames")

        if sample_type == SampleType.raw_TD:
            return TimeDependentData.from_filenames(
                filenames, **kwargs)

        if sample_type == SampleType.att_time_dependend:
            return AttributedTimeDependentData.from_filenames(
                filenames, *kwargs
            )

        raise ValueError(
            f"I cannot read the list of files:\ntype: {sample_type}\n"
            f"paths: {filenames}")

    else:
        raise ValueError("path or filemanes must be provided.")
