#!/usr/bin/env python
# coding: utf-8

"""
This module provides input/output tools for experimental data.
"""

from typing import Union
from pathlib import Path
import re

import numpy as np
import pandas as pd
import yaml

import pyc2mc.core.peak
from pyc2mc.core.isotopes import Isotope, Element
from pyc2mc.core.formula import Composition

__all__ = ["read_peaklist", "read_ascii", "read_pks", "read_predator",
           "read_txt", "read_csv"]

# supported file formats
FILE_FORMATS = {
    "ascii": [".asc", ".ascii"],
    "predator": [".csv"],
    "pks": [".pks"],
    "csv": [".csv"],
    "txt": [".txt"],
}


def get_file_format(ext: str, fmt: str = None) -> str:
    """ Try to determine the file format according to the extension. """
    
    file_fmt = None

    if not fmt:
        # format is not provided, try to detect file format
        for key, exts in FILE_FORMATS.items():
            if ext in exts:
                file_fmt = key
                break

    elif str(fmt).lower().strip() in FILE_FORMATS:
        # format is provided and is a valid format
        file_fmt = str(fmt).lower().strip()
    
    return file_fmt


def read_peaklist(path: str, /, fmt: str = "", **kwargs):
    """ Generic function to determine file format read the file and
    return a PeakList object. Format is always lowercase and the file
    extension is converted in lowercase before file format detection.
    Supported format includes:

    * ascii format (asc, ascii)
    * pks format (pks)
    * predator format (predator)
    * csv format (csv)
    * txt format (txt)

    Args:
        path (str): Path to the file to read
        fmt (str): file format, if not provided, the function tries to
            determine the format based on the extension. Possible values are
            `ascii`, `predator`, `pks`, `csv` or `txt`.
        kwargs: all other keywords arguments are passed to the reading
            function. Look at the documentation of the following functions
            for more detail on the parameters.

    Returns:
        A peak list object.
    """

    # check file exists
    path = Path(path)
    if not path.is_file():
        raise FileNotFoundError(f"File {path} is not a regular file.")

    # determine file format
    file_fmt = get_file_format(path.suffix.lower(), fmt)
    if file_fmt is None:
        raise ValueError(f"Format of file {path} was not identified or is "
                         "not supported. Supported file formats are:\n"
                         f"{FILE_FORMATS}")

    # read file
    try:
        if file_fmt == "ascii":
            pl = read_ascii(path, **kwargs)
        elif file_fmt == "predator":
            pl = read_predator(path, **kwargs)
        elif file_fmt == "pks":
            pl = read_pks(path, **kwargs)
        elif file_fmt == "csv":
            pl = read_csv(path, **kwargs)
        elif file_fmt == "txt":
            pl = read_txt(path, **kwargs)
    except pd.errors.ParserError:
        raise ValueError(f"Error when reading file {path} with format "
                         f"{file_fmt}. File contents and format may be "
                         "inconsistent.")
    
    return pl


def read_ascii(input_path: str, SN_avail: bool = True, **kwargs):
    """
    Reading function for the peak list in .ascii format. This is a
    simple wrapper above the pandas read_csv function assuming the
    file contains the mass on the first column and the intensity on
    the second column. If available, S/N ratio data should be the third
    column. All additional arguments are passed to the read_csv function
    of pandas.

    Args:
        input_path (str): directory and name of the file.
        SN_avail (bool): if False the reading function will avoid the
            S/N ratio column, and will only read mass and intensity.
        **kwargs are additional parameters for the peaklist
    """
    if "sort_mz" in kwargs:
        sort_mz = kwargs.pop("sort_mz")
    else:
        sort_mz = True

    input_path = Path(input_path)
    name = input_path.name
    ext = input_path.suffix
    if ext == '.asc':
        if SN_avail:
            df = pd.read_csv(
                input_path, sep=r"\s+",
                names=["mass", "intensity", "SN"], 
                usecols=(0, 1, 2))
        else:
            df = pd.read_csv(
                input_path,
                sep=r"\s+",
                names=["mass", "intensity"],
                usecols=(0, 1))
    elif ext == '.txt':
        df = pd.read_csv(
            input_path,
            names=["mass", "intensity"],
            sep=r"\s+",
        )
    else:
        if SN_avail:
            df = pd.read_csv(
                input_path,
                sep=r"\s+",
            )
            df.rename(columns={'m/z': 'mass', 'I': 'intensity', 'S/N': 'SN'},
                      inplace=True)
        else:
            df = pd.read_csv(input_path, sep=r"\s+")
            df.rename(columns={'m/z': 'mass', 'I': 'intensity'}, inplace=True)

    if "SN" in df.columns:
        return pyc2mc.core.peak.PeakList(
            df.mass.values,
            df.intensity.values,
            df.SN.values,
            pid=list(df.index.values),
            name=name)
    else:
        return pyc2mc.core.peak.PeakList(
            df.mass.values,
            df.intensity.values,
            pid=list(df.index.values),
            sort_mz=sort_mz,
            name=name,
            **kwargs)


def read_pks(input_path, **kwargs):
    """
    Reading function for the peak list in .pks format. This is a
    simple wrapper above the pandas read_csv function assuming the
    file contains the mass on the first column and the intensity on
    the second column. If available, S/N ratio data should be the thrid
    column. All additional arguments are passed to the read_csv function
    of pandas. If the faile contains a header, use the command skiprows = N,
    where N represents the number of lines in the head just until data,
    counting empty lines. 

    Args:
        input_path (str): directory and name of the file.
        **kwargs additional parameters for the peaklist
    """
    if 'skiprows' not in kwargs:
        skiprows = 8

    if "sort_mz" in kwargs:
        sort_mz = kwargs.pop("sort_mz")
    else:
        sort_mz = True

    input_path = Path(input_path)
    name = input_path.name

    with input_path.open("r") as finput:
        # get metadata
        line = finput.readline()
        k = line.index("There were")
        line = line[k:].split()
        metadata = dict(npeaks=int(line[2]), min_mz=float(line[7]),
                        max_mz=float(line[10].strip(".")))
        line = finput.readline()
        if "POSITIVE" in line:
            polarity = 1
        elif "NEGATIVE" in line:
            polarity = -1
        else:
            polarity = None
        metadata["polarity"] = polarity

        line = finput.readline()
        patterns = {
            "peak_threshold": r"Peak threshold:\s*(\d+\.\d+)\s*",
            "noise_threshold": r"Noise threshold:\s+(\d+\.\d+)\s*",
            "baseline_SN": r"Baseline for SN:\s*(\d+\.\d+)\s+",
            "sigma_SN": r"Sigma for SN:\s*(\d+\.\d+)",
        }
        for key, pattern in patterns.items():
            if m := re.search(pattern, line):
                metadata[key] = float(m.group(1))

        # case of LC run pks file => timing data
        line = finput.readline()
        if "Data Detected at" in line:
            metadata["time"] = float(line.split()[-1])

        # end of the header
        finput.readline()
        finput.readline()
        
        # column names
        names = [n.strip() for n in finput.readline().split()]

    SN_avail = False
    if 'S/N' in names:
        names = ["mass", "peak height", "intensity", "resolving power",
                 "frequency", "SN"]
        usecols = (0, 1, 2, 3, 4, 5)
        SN_avail = True
    else:
        usecols = (0, 1, 2, 3, 4)
        names = ["mass", "peak height", "intensity", "resolving power",
                 "frequency"]
    metadata["is_SN_avail"] = SN_avail

    df = pd.read_csv(
        input_path,
        sep=r"\s+",
        names=names,
        usecols=usecols,
        dtype=np.float64,
        skiprows=skiprows,
    )
    
    if SN_avail:
        SN = df.SN.values
    else:
        SN = None
    
    peak_properties = {}
    for key in ["frequency", "resolving power"]:
        if key in df.columns:
            peak_properties[key] = df[key].values
    
    metadata = pyc2mc.core.peak.PeakListMetadata(**metadata)

    return pyc2mc.core.peak.PeakList(
        mz=df.mass.values,
        intensity=df.intensity.values,
        SN=SN,
        peak_properties=peak_properties,
        sort_mz=sort_mz,
        name=name,
        metadata=metadata,
        **kwargs
    )


def read_predator(input_path, **kwargs):
    """
    Reading function for the peak list in .csv predator format. This 
    is a simple wrapper above the pandas read_csv function assuming 
    the file contains the mass on the first column and the intensity 
    on the second column. If available, S/N ratio data should be the thrid
    column. All additional arguments are passed to the read_csv function
    of pandas. If the faile contains a header, use the command skiprows = N,
    where N represents the number of lines in the head just until data,
    counting empty lines.

    Args:
        input_path (str): directory and name of the file.
        **kwargs additional parameters for the peaklist
    """

    if "sort_mz" in kwargs:
        sort_mz = kwargs.pop("sort_mz")
    else:
        sort_mz = True

    input_path = Path(input_path)
    name = input_path.name

    if 'skiprows' not in kwargs:
        skiprows = 10

    # read header and column names
    with input_path.open("r") as finput:
        [finput.readline() for _ in range(9)]
        names = [n.strip() for n in finput.readline().split(',')]

    if names[-1] == "Molecular Formula":
        names.pop()
        n_cols = len(names)
    else:
        n_cols = len(names)
    
    # read whole file
    df = pd.read_csv(
        input_path,
        on_bad_lines='skip',
        usecols=range(n_cols),
        names=names,
        low_memory=False,
        skiprows=skiprows,
    )
    
    if 'Rel. Abundance' in names:
        df.rename(columns={'Rel. Abundance': 'intensity'}, inplace=True)
    elif 'Abs. Abundance' in names:
        df.rename(columns={'Abs. Abundance': 'intensity'}, inplace=True)
    else:
        raise NameError(
            'No Rel. Abundance or Abs. Abundance found in the file. '
            f"{input_path}"
        )
    
    columns = ["Mass", "intensity"]

    if 'SN' in names:
        columns.append('SN')
    if 'Frequency' in names:
        columns.append('Frequency')

    df = df[columns].copy()
    df.dropna(inplace=True)

    if 'SN' in columns:
        SN = df["SN"].values
    else:
        SN = None

    if 'Frequency' in names:
        peak_properties = {
            'frequency': np.array(df['Frequency'].values, dtype=float)}
    else:
        peak_properties = None

    return pyc2mc.core.peak.PeakList(
        mz=df.Mass.values,
        intensity=df.intensity.values, 
        SN=SN,
        peak_properties=peak_properties,
        sort_mz=sort_mz,
        name=name,
        **kwargs
    )


def read_csv(input_path, **kwargs):
    """
    Reading function for the peak list in .csv format. This is a
    simple wrapper above the pandas read_csv function assuming the
    file contains the mass on the first column and the intensity on
    the second column. All additional arguments are passed to the
    read_csv function of pandas. If data has a header, then use the
    command skiprows=N to avoid reading the N number of lines. For
    orbitrap data use skiprows = 5.

    Args:
        input_path (str): directory and name of the file.
        **kwargs additional parameters for the peaklist
    """

    if "sort_mz" in kwargs:
        sort_mz = kwargs.pop("sort_mz")
    else:
        sort_mz = True

    input_path = Path(input_path)
    name = input_path.name
    df = pd.read_csv(input_path, dtype=np.float64)

    if len(df.columns) <= 2:
        raise ValueError(
            "At least two columns with mass and intensity are expected.")

    sn_col = None
    for sn in ["SN", "S/N"]:
        if sn in df.columns:
            sn_col = sn
            break
    if sn_col is not None:
        SN = df.loc[:, sn_col].values
    else:
        SN = None

    # we assume col 1 is mass and col 2 is intensity
    return pyc2mc.core.peak.PeakList(
        mz=df.iloc[:, 0].values,
        intensity=df.iloc[:, 1].values,
        SN=SN,
        pid=list(df.index.values),
        sort_mz=sort_mz,
        name=name,
        **kwargs
    )


def read_txt(input_path, names=None, **kwargs):
    """
    Reading function for the peak list in .txt format. This is a
    simple wrapper above the pandas read_csv function assuming the
    file contains the mass on the first column and the intensity on
    the second column. All additional arguments are passed to the
    read_csv function of pandas. If data has a header, then use the
    command skiprows=N to avoid reading the N number of lines. For
    orbitrap data use skiprows = 5.

    Args:
        input_path (str): directory and name of the file.
        **kwargs additional parameters for the peaklist
    """

    if "sort_mz" in kwargs:
        sort_mz = kwargs.pop("sort_mz")
    else:
        sort_mz = True

    input_path = Path(input_path)
    name = input_path.name
    if names is None:
        names = ["mass", "intensity", "Res.", 'S/N ratio']
    else:
        names = names
    df = pd.read_csv(input_path, dtype=np.float64)
    df.columns = names

    return pyc2mc.core.peak.PeakList(
        df.mass.values,
        df.intensity.values,
        SN=df['S/N ratio'].values,
        pid=list(df.index.values),
        sort_mz=sort_mz,
        name=name,
        **kwargs
    )


def read_attributed_petroorg(
    input_path: Union[str, Path],
    polarity: int,
) -> pyc2mc.core.peak.AttributedPeakList:
    """  Reading function for an attributed file from petroOrg in csv format.

    Args:
        input_path (str): path to the csv file
        polarity (int): the ionization charge

    Returns:
        The attributed peak list
    
    """
    path = Path(input_path)

    with path.open("r") as fcsv:
        _ = fcsv.readline()  # head line

        # manage columns
        cols = fcsv.readline().split(",")
        cols = [c.strip() for c in cols]
        cols[0] = "pid"
        
        # read csv lines
        data = list()
        compositions = list()
        species = set()
        for line in fcsv:
            vals = line.split(",")
            data.append([float(v) for v in vals[:10]])
            compo_d = {vals[i]: int(vals[i + 1]) for i in range(10, len(vals) - 1, 2)}
            compo = Composition(compo_d)
            compositions.append(compo)
            species.update(compo.keys())

    # print(species)

    # set up a dataframe with the data and get formulas
    df = pd.DataFrame(data, columns=cols[:-1])
    isotopes = list()
    for specie in species:
        try:
            iso = Isotope.from_string(specie)
            if iso.is_most_abundant:
                symbol = iso.element.symbol
            else:
                symbol = iso.symbol
                isotopes.append(iso.symbol)
        except ValueError:
            try:
                elmt = Element.from_string(specie)
                symbol = elmt.symbol
            except ValueError:
                raise ValueError(
                    "This species was not recognized neither as an Isotope "
                    f"nor as an element. species is '{specie}'.")
   
        df[symbol] = [compo[specie] if specie in compo else 0 for compo in compositions]

    df["formula"] = [compo.to_formula() for compo in compositions]

    is_isotopic = np.zeros(len(df), dtype=np.int32)
    for isotope in isotopes:
        is_isotopic += df[isotope]
    df["isotopic"] = is_isotopic > 0
    df.sort_values(by="Exp. m/z", inplace=True, ascending=True)
    df.reset_index(inplace=True)

    return pyc2mc.core.peak.AttributedPeakList(
        mz=df["Exp. m/z"],
        intensity=df["Rel. Abundance"],
        formulas=df["formula"],
        SN=df["Signal2Noise"],
        peak_properties={"mz_theo": df["Theor. Mass"]},
        polarity=polarity,
    )


def write_pks(peaklist: pyc2mc.core.peak.PeakList, 
              path: str = "data.pks", decimals: int = 7):
    """ Write a Peaklist in pks format 
    
    Args:
        peaklist (PeakList): The peaklist to export
        path (str): the path to write the pks file

    """
    line = f"PyC2MC pks - There were {len(peaklist)} Peaks found from "
    line += f"m/z= {peaklist.metadata.min_mz:8.2f}    to "
    line += f"m/z= {peaklist.metadata.max_mz:8.2f}.\n"
    line += "POSITIVE ION MODE" if peaklist.metadata.polarity > 0 else "NEGATIVE ION MODE"
    line += "\n"
    keys = {
        "Noise threshold:": peaklist.metadata.noise_threshold,
        "Peak threshold:": peaklist.metadata.peak_threshold,
        "Baseline for SN:": peaklist.metadata.baseline_SN,
        "Sigma for SN:": peaklist.metadata.sigma_SN,
    }
    for key, value in keys.items():
        if value is not None:
            line += f"{key} {value:8.6}      "
    line = line.strip()
    line += "\n\n"
    line += f"Center of Mass for the Assigned Window is {peaklist.mz.mean():10.4f}\n\n"

    if peaklist.SN_avail:
        line += (" Peak Location      Peak Height            Abundance"
                 "             Resolving Power        Frequency"
                 "               S/N")
    else:
        line += (" Peak Location      Peak Height            Abundance"
                 "             Resolving Power        Frequency")
    line += "\n\n"

    freq_avail = "frequency" in peaklist.peak_properties_keys
    rp_avail = "resolving power" in peaklist.peak_properties_keys
        
    for peak in peaklist:
        line += f"{peak.mz:12.{decimals}f}{0.0:18.3f}"
        line += f"{peak.intensity:22.3f}"
        if rp_avail:
            line += f"{peak.properties['resolving power']:24.6f}"
        else:
            line += f"{0:24d}"
        if freq_avail:
            line += f"{peak.properties['frequency']:25.6f}"
        if peaklist.SN_avail:
            line += f"{peak.SN:16.3f}"
        line += "\n"

    with open(path, "w") as fpks:
        fpks.write(line)


def write_ypks(peaklist: pyc2mc.core.peak.PeakList, 
               path: str = "data.pks", decimals: int = 7):
    """ Write a Peaklist in ypks format. ypks means yaml pks. This is 
    essentially a pks file with a yaml frontmatter to describe the
    metadata associated to the peak list.

    # TODO: check the adequate floating format for each column
    # m/z and frequency needs a lot of digit but may other columns not
    # resolving power is a huge number.
    
    Args:
        peaklist (PeakList): The peaklist to export
        path (str): the path to write the pks file

    """
    line = "# PyC2MC yaml pks\n---\n"
    metadata = peaklist.metadata.to_dict()
    line += yaml.dump(metadata)
    line += "---\n"
    
    cols = ["Peak Location", "Peak Height", "Abundance", "Resolving Power",
            "Frequency", "S/N"]
    
    if not peaklist.SN_avail:
        cols.pop(-1)
    line += "".join([f"{col:>16s}" for col in cols])
    line += "\n"

    freq_avail = "frequency" in peaklist.peak_properties_keys
    rp_avail = "resolving power" in peaklist.peak_properties_keys
        
    for peak in peaklist:
        line += f"{peak.mz:16.{decimals}f}{0.0:16.3f}"
        line += f"{peak.intensity:16.3f}"
        if rp_avail:
            line += f"{peak.properties['resolving power']:16.6f}"
        else:
            line += f"{0:26.6f}"
        if freq_avail:
            line += f"{peak.properties['frequency']:16.6f}"
        if peaklist.SN_avail:
            line += f"{peak.SN:16.3f}"
        line += "\n"

    with open(path, "w") as fpks:
        fpks.write(line)


def read_ypks(input_path: str, **kwargs):
    """ Read a Peaklist in ypks format. ypks means yaml pks. This is 
    essentially a pks file with a yaml frontmatter to describe the
    metadata associated to the peak list.
    
    Args:
        path (str): the path to write the pks file

    """
    if "sort_mz" in kwargs:
        sort_mz = kwargs.pop("sort_mz")
    else:
        sort_mz = True

    input_path = Path(input_path)
    name = input_path.name

    # read the file
    with input_path.open("r", encoding="utf-8") as fpks:

        # read frontmatter
        # ----------------
        line = fpks.readline()
        while "---" not in line:
            # the yaml frontmatter start with '---'
            line = fpks.readline()
        if line[0] != "-":
            index = line.index("-")
            comment = line[:index]

        header = list()
        line = fpks.readline()
        while "---" not in line:
            line = line.lstrip(comment)
            header.append(line)
            line = fpks.readline()
            
        metadata = yaml.safe_load("".join(header))
        print(metadata)

        # read data
        # ---------
        # column names
        names = [n.strip() for n in fpks.readline().split()]

        SN_avail = False
        if 'S/N' in names:
            names = ["mass", "peak height", "intensity", "resolving power",
                     "frequency", "SN"]
            usecols = (0, 1, 2, 3, 4, 5)
            SN_avail = True
        else:
            usecols = (0, 1, 2, 3, 4)
            names = ["mass", "peak height", "intensity", "resolving power",
                     "frequency"]
        metadata["is_SN_avail"] = SN_avail

        # read csv part
        df = pd.read_csv(
            fpks,
            sep=r'\s+',
            names=names,
            usecols=usecols,
            dtype=np.float64,
        )
    
    if SN_avail:
        SN = df.SN.values
    else:
        SN = None
    
    peak_properties = {}
    for key in ["frequency", "resolving power"]:
        if key in df.columns:
            peak_properties[key] = df[key].values
    
    metadata = pyc2mc.core.peak.PeakListMetadata(**metadata)

    return pyc2mc.core.peak.PeakList(
        mz=df.mass.values,
        intensity=df.intensity.values,
        SN=SN,
        peak_properties=peak_properties,
        sort_mz=sort_mz,
        name=name,
        metadata=metadata,
        **kwargs,
    )
