# coding: utf-8

import struct
import re
import time
from typing import Union
from pathlib import Path
import mmap

import numpy as np
import logging

from pyc2mc.core.ft_icr_signal import SignalFileType
from pyc2mc.core.ft_icr_signal import FT_ICR_Signal

DEBUG = False

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG) if DEBUG else log.setLevel(logging.INFO)
logging.getLogger("matplotlib").setLevel(logging.ERROR)
logging.getLogger("PIL.PngImagePlugin").setLevel(logging.INFO)


# Constants
FLOAT = 'float'
INT = 'int'
INT32_SIZE = 4
FLOAT32_SIZE = 4
REAL64_SIZE = 8
DAT = '.dat'
PFD = '.pfd'


# Use regex for extracting information
PATTERNS = {
    "lowfreq": (re.compile(br"lowfreq:(\d+)"), float),
    "highfreq": (re.compile(br"highfreq:(\d+)"), float),
    "number_points": (re.compile(br"Data Points: (\d+)"), int),
    "voltage_scale": (re.compile(br"Voltage Scale: (\d+\.\d+)"), float),
    "zoomcount": (re.compile(br"Zoomcount: (\d+)"), int),
    "start_index": (re.compile(br"Start Index: (\d+)"), int),
    "bandwidth": (re.compile(br"Bandwidth: (\d+\.\d+)"), float),
    "b_term": (re.compile(br"Bterm: (-?\d+\.\d+)"), float),
    "a_term": (re.compile(br"Aterm: (-?\d+\.\d+)"), float),
    "trap_voltage": (re.compile(br"Trapvoltage: (-?\d+\.\d+)"), float),
    "zeroorder": (re.compile(br"Zeroorder: (-?\d+\.\d+e[+-]\d+)"), float),
    "phased": (re.compile(br"Phased: (\d+)"), int),
    "rephase_sections": (re.compile(br"Rephase Sections: (\d+)"), int),
}


def read_write_until_match(string, r_file, w_file=None):
    """
    Reads lines from the read-file until a match is found, copies to write-file if provided.
    :param string: The string to match.
    :param r_file: The file to read from.
    :param w_file: The file to write to.
    :return: The line that was matched.
    """
    length = len(string)
    line = r_file.readline().decode('utf-8')
    while line[:length] != string:
        if w_file is not None:
            w_file.write(line.encode('utf-8'))
        try:
            line = r_file.readline().decode('utf-8')
        except UnicodeDecodeError:
            line = ''            
    return line


def write_signal(signal: FT_ICR_Signal, path: str, signal_file: str):
    """
    Calls the appropriate function to save the new coadded signal to the 
    given file type.
    """
    if signal.metadata.file_type == SignalFileType.DAT:
        write_dat(signal, path, signal_file)
    elif signal.metadata.file_type == SignalFileType.PFD:
        write_pfd(signal, path, signal_file)
    else:
        raise NotImplementedError(
            f"File type '{signal.metadata.file_type}' is not supported")


def write_dat(signal: FT_ICR_Signal, path: str, dat_file: str):
    """
    Saves the coadded signal to a new .dat file.

    The header of the dat file is read from `dat_file`.

    TODO: is it possible to be abble to write a dat file without the 
    need of an existing one?

    Args:
        signal (FT_ICR_Signal): The signal to save
        path (str or path): The path to new file
        dat_file (str or path): A dat file from which the header is read
        
    """
    step_size = signal.metadata.step_size
    with open(path, 'wb') as file:
        with open(dat_file, 'rb') as first:
            
            look_for = 'expcount:'
            read_write_until_match(look_for, first, file)
            new_expcount = f"{look_for} {step_size}\n"
            file.write(new_expcount.encode('utf-8'))
            
            look_for = 'Data Detected at (seconds): '
            read_write_until_match(look_for, first, file)
            
            look_for = 'Storage Type: '
            read_write_until_match(look_for, first, file)
            new_type = look_for + FLOAT + '\n'
            file.write(new_type.encode('utf-8'))
            
            look_for = 'Data:\n'
            read_write_until_match(look_for, first, file)
            file.write(look_for.encode('utf-8'))

            time_start = time.time() if DEBUG else None
            for data in signal.signal:
                file.write(struct.pack('f', data))
            time_end = time.time() if DEBUG else None
            log.debug(f"Writing signal: {time_end - time_start}") if DEBUG else None


def write_pfd(signal: FT_ICR_Signal, path: str, pfd_file: str):
    """
    Saves the new coadded signal to a new .pfd file.
    :param signal: The signal to save.
    :param path: The path to save the coadd to.
    :param pfd_file: The first file in the coadd.
    :return: None
    """

    zeroorder = signal.metadata.zeroorder if signal.metadata.zeroorder is not None else 0
    step_size = signal.metadata.step_size

    with open(path, 'wb') as file:
        with open(pfd_file, 'rb') as first:
            look_for = 'expcount:'
            read_write_until_match(look_for, first, file)
            new_expcount = f"{look_for} {step_size}\n"
            file.write(new_expcount.encode('utf-8'))
            
            look_for = 'Data Detected at (seconds): '
            read_write_until_match(look_for, first, file)
            
            look_for = 'Storage Type: '
            read_write_until_match(look_for, first, file)
            new_type = look_for + FLOAT + '\n'
            file.write(new_type.encode('utf-8'))

            look_for = 'Zeroorder: '
            read_write_until_match(look_for, first, file)
            new_zeroorder = look_for + format(zeroorder, ".10e") + '\n' 
            file.write(new_zeroorder.encode('utf-8'))

            line = read_write_until_match('Rephase Sections: ', first, file)
            num_sections = int(line[17:-1])
            file.write(line.encode('utf-8'))

            look_for = 'Rephase Section Initial Indexes as Int32:'
            read_write_until_match(look_for, first, file)
            new_line = look_for + '\n'
            file.write(new_line.encode('utf-8'))
            for _ in range(num_sections):
                read_data = first.read(INT32_SIZE)
                if len(read_data) != INT32_SIZE:
                    break
                file.write(read_data)
            
            look_for = 'Rephase Section Final Indexes as Int32:'
            line = read_write_until_match(look_for, first, file)
            new_line = look_for + '\n'
            file.write(new_line.encode('utf-8'))
            for _ in range(num_sections):
                read_data = first.read(INT32_SIZE)
                if len(read_data) != INT32_SIZE:
                    break
                file.write(read_data)

            look_for = 'Rephase D Terms as Real64:'
            line = read_write_until_match(look_for, first, file)
            new_line = look_for + '\n'
            file.write(new_line.encode('utf-8'))
            for _ in range(num_sections):
                read_data = first.read(REAL64_SIZE)
                if len(read_data) != REAL64_SIZE:
                    break
                file.write(read_data)
                    
            look_for = 'Data:\n'
            line = read_write_until_match(look_for, first, file)
            file.write(look_for.encode('utf-8'))

            for data in signal.signal:
                file.write(struct.pack('f', data))


def read_signal(file_path, extension: str = None):
    '''
    Calls the appropriate read function based on the file type.
    '''

    if extension is None:
        path = Path(file_path)
        extension = path.suffix

    if extension == SignalFileType.DAT:
        data = read_dat(file_path)
    elif extension == SignalFileType.PFD:
        data = read_pfd(file_path)
    else:
        raise NotImplementedError(
            f"Cannot read files with extension: '{extension}'."
        )  
        
    return data


def read_pfd(file_path):
    '''
    Reads the data from a .pfd file.
    :param file_path: The path to the .pfd file.
    :return: The signal, number of points, bandwidth, and zero order.
    '''
    log.debug(f"Reading {file_path}")

    with open(file_path, 'rb') as file:
        mmapped_file = mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ)

        results = {}
        for key, (pattern, conv) in PATTERNS.items():
            if match := pattern.search(mmapped_file):
                results[key] = conv(match.group(1))
            else:
                results[key] = None
        
        results["phased"] = True if results["phased"] == 1 else False
        data_count = results['number_points']
        zoomcount = results['zoomcount']
        rephase_sections = results['rephase_sections']
        
        # Extract rephase indexes and terms
        if results["phased"]:
            time_start = time.time() if DEBUG else None
            start = mmapped_file.find(b'Rephase Section Initial Indexes as Int32:\n') + len(b'Rephase Section Initial Indexes as Int32:\n')
            end = start + rephase_sections * INT32_SIZE
            initial_rephase_indexes = list(struct.unpack(f'{rephase_sections}i', mmapped_file[start:end]))
            time_end = time.time() if DEBUG else None
            log.debug(f"Initial rephase indexes: {time_end - time_start}") if DEBUG else None

            time_start = time.time() if DEBUG else None
            start = mmapped_file.find(b'Rephase Section Final Indexes as Int32:') + len(b'Rephase Section Final Indexes as Int32:\n')
            end = start + rephase_sections * INT32_SIZE
            final_rephase_indexes = list(struct.unpack(f'{rephase_sections}i', mmapped_file[start:end]))
            time_end = time.time() if DEBUG else None
            log.debug(f'Final rephase indexes: {time_end - time_start}') if DEBUG else None

            time_start = time.time() if DEBUG else None
            start = mmapped_file.find(b'Rephase D Terms as Real64:\n') + len(b'Rephase D Terms as Real64:\n')
            end = start + rephase_sections * REAL64_SIZE
            rephase_d_terms = list(struct.unpack(f'{rephase_sections}d', mmapped_file[start:end]))
            time_end = time.time() if DEBUG else None
            log.debug(f'Rephase d terms: {time_end - time_start}') if DEBUG else None

        # Extract signal data
        time_start = time.time() if DEBUG else None
        if zoomcount is not None:
            start = mmapped_file.find(b'Data:\n') + len('Data:\n')
            end = start + zoomcount * FLOAT32_SIZE

            signal = np.array(struct.unpack(f'{zoomcount}f', mmapped_file[start:end]))
        else:
            start = mmapped_file.find(b'Data:\n') + len('Data:\n')
            data_left = int(len(mmapped_file[start:]) / FLOAT32_SIZE)
            end = start + data_count * FLOAT32_SIZE

            signal = np.array(struct.unpack(f'{data_count}f', mmapped_file[start:end]))

        time_end = time.time() if DEBUG else None
        log.debug(f'Signal: {time_end - time_start}') if DEBUG else None   

    results["phased"] = True if results["phased"] else False
    results["file_type"] = SignalFileType.PFD
    return FT_ICR_Signal(signal, file_path=file_path, **results)


def read_dat(file_path: Union[str, Path]):
    '''
    Reads the data from a .dat file.
    :param file_path: The path to the .dat file.
    :return: The signal, number of points, bandwidth, and zero order.
    '''
    with open(file_path, 'rb') as file:
        mmapped_file = mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ)

        data_type = re.search(br"Storage Type: (\w+)", mmapped_file).group(1).decode('utf-8')

        results = {}
        for key, (pattern, conv) in PATTERNS.items():
            if match := pattern.search(mmapped_file):
                results[key] = conv(match.group(1))
            else:
                results[key] = None
        
        number_points = results["number_points"]

        # Set the unpacking format based on storage type
        if FLOAT in data_type:
            data_size_bytes = 4
            data_format = 'f' * number_points
        elif INT in data_type:
            data_size_bytes = 2
            data_format = 'h' * number_points
        
        # Find data start and read in bulk
        start = mmapped_file.find(b'Data:\n') + 6
        end = start + number_points * data_size_bytes
        data = mmapped_file[start:end]
        signal = np.array(struct.unpack(data_format, data))

        if INT in data_type:
            signal = signal.astype(np.float32)
            signal *= results["voltage_scale"]

    results["file_type"] = SignalFileType.DAT
    return FT_ICR_Signal(signal, file_path=file_path, **results)
