# coding: utf-8

"""
A convenient script to run a workflow through PyC2MC instructions from the command line
"""

import sys
from pathlib import Path
import argparse
from argparse import RawDescriptionHelpFormatter
import logging
import importlib
from datetime import datetime


try:
    import pyc2mc
    from pyc2mc.workflows.workflows import DataProcessing
    from pyc2mc.core.formula_grid import FormulaGridList
except ModuleNotFoundError:
    raise ModuleNotFoundError("PyC2MC is not available.")


def run_cli(args):
    """ Run main command line """

    logger = logging.getLogger(pyc2mc.LOGGER_NAME)

    # run workflow
    logger.info("Read input file: %s", args.input)
    job = DataProcessing.load(args.input)

    formula_grids = None
    if args.fgrid is not None:
        logger.info("Read formula grids file: %s", args.fgrid)

        formula_grids = FormulaGridList()
        formula_grids.load(args.fgrid)
        logger.info("Available formula grids are:")
        logger.info("\n%s", formula_grids)

    logger.info("Startup done.")

    job.run(formula_grids=formula_grids)


def exist(filename):
    """ 'Type' for argparse - checks that file exists but does not open """
    if not Path(filename).is_file():
        raise argparse.ArgumentTypeError(
            f"Input file {filename} does not exist")
    return filename


def get_options():
    """ Define the argparse parser and returns options """
    # parse command line options
    parser = argparse.ArgumentParser(
        description=f"{pyc2mc.pyc2mc_title}",
        formatter_class=RawDescriptionHelpFormatter,
        epilog="Authors: iC2MC development team.")

    parser.add_argument('--version', action='version',
                        version=f"{pyc2mc.__version__}")
    parser.add_argument("-i", '--input',
                        help="Toml input file",
                        metavar="TOML/YAML",
                        default="input.toml",
                        type=exist)
    parser.add_argument("-g", "--fgrid",
                        help="path to the Formula Grid definition file.",
                        metavar="FORMULA_GRIDS",
                        type=exist)
    parser.add_argument(
        "-l", "--log",
        help="Set the logging level (DEBUG, INFO, WARNING, ERROR, CRITICAL).",
        action="store",
        choices=["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"],
        default="INFO")
    parser.add_argument(
        "-o", "--out",
        help="Name of the output file with log information.",
        metavar="LOG_FILE",
        # default="c2mc.log",
    )
    parser.set_defaults(func=run_cli)

    # define subparsers
    # -----------------
    # subparsers = parser.add_subparsers()

    # configuration
    # parser_config = subparsers.add_parser(
    #     "config",
    #     help="Tools for configuring pyc2mc, e.g., formula grid folder ..."
    # )

    args = parser.parse_args()
    try:
        getattr(args, "func")
    except AttributeError:
        parser.print_help()
        sys.exit(-1)

    return args


def main():
    """ run the script """

    if sys.version_info[0] < 3:
        raise RuntimeError('PyC2MC only supports Python 3')

    try:
        importlib.import_module("pyc2mc")
    except ImportError:
        raise ImportError("PyC2MC is not available.")

    # get options
    args = get_options()

    # warmup
    logger = pyc2mc.configure_logger(level=args.level, filename=args.out)
    now = datetime.now()
    logger.info("Date: %s", now.strftime("%Y-%m-%d %H:%M"))
    logger.info("%s", pyc2mc.pyc2mc_title)

    # go
    args.func(args)


if __name__ == '__main__':
    main()
