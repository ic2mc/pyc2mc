# coding: utf-8

""" This module provides functions to implement the coaddition of a set
of FT-ICR-MS signals along an LC run. """

from pathlib import Path
from typing import Sequence, Union, Iterator, Tuple
from collections import abc
import logging

from math import ceil

from pyc2mc.utils import get_file_list
from pyc2mc.io.ft_icr_signal import read_signal, write_signal
from pyc2mc.core.ft_icr_signal import FT_ICR_Signal

DEBUG = False

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG) if DEBUG else log.setLevel(logging.INFO)
logging.getLogger("matplotlib").setLevel(logging.ERROR)
logging.getLogger("PIL.PngImagePlugin").setLevel(logging.INFO)


class FT_ICR_SignalCollection(abc.Sequence):
    """ Essentially a sequence of FT_ICR_Signal object.
    The class provides methods to implement a co-addition of signals and
    improve S/N ratio.
    """

    def __init__(self, signals: Sequence[FT_ICR_Signal]):
        """ Create a collection of signals
         
        Args:
            signals (list): List of FT_ICR_Signal object
        
        """
        self._signals = signals

    def coadd(
        self,
        scan_range: Tuple[int, int] = None,
        step_size: int = 10
    ) -> "FT_ICR_SignalCollection":
        """ Co-add the signals according to the step_size and scan_range
        
        Args:
            scan_range (int, int): The range of scans to read
            step_size (int): The number of scan to coadd at a time.

        Returns:

        """
        if scan_range is None:
            signals = self._signals
        else:
            scan_i, scan_f = scan_range
            signals = self._signals[scan_i: scan_f + 1]
        total = len(signals)
        num_loops = ceil(total / step_size)

        coadded_signals = []
        for step in range(num_loops):
            selected_signals = []
            for signal in signals[step * step_size: (step + 1) * step_size]:
                selected_signals.append(signal.signal)

            coadded_signal = sum(selected_signals) / len(selected_signals)
            metadata = signal.metadata.to_dict()
            metadata["step_size"] = step_size
            coadded_signals.append(
                FT_ICR_Signal(coadded_signal, 
                              file_path=signals[0].file_path,
                              **metadata)
            )

        return FT_ICR_SignalCollection(coadded_signals)

    def save_signals(
        self,
        folder_name: Union[str, Path],
        basename: str = "ca_",
    ):
        '''
        Saves the coadded signals into file in a folder.

        Args:
            folder_name: The name of the folder to save the signals to.
            zeroorder: The zero order.
            file_type: The file type.
        
        Returns:
            None
        '''
        folder = Path(folder_name)
        if not folder.exists():
            folder.mkdir(exist_ok=True, parents=True)

        for idx, cs in enumerate(self._signals):
            new_dat = folder / f'{basename}{idx + 1}.dat'

            write_signal(signal=cs,
                         path=new_dat,
                         signal_file=cs.file_path)  

    @classmethod
    def from_files(
        cls,
        directory_path: str,
        extension: str = ".dat",
        scan_range: Tuple[int, int] = None,
        sorting: bool = True,
        verbose: bool = True,
        **kwargs
    ) -> "FT_ICR_SignalCollection":
        """ Import the data from signals files.

        Args:
            directory_path (str): directory path of the files
            extension (str): file extension.
            scan_range (int, int): The range of scans to read
            verbose (bool): If True, print some information
            sorting (bool): If true, sort files by index. Warning: files
                must have an index number like: filename-00.ext, where
                00 is the index.
            kwargs are transfer to the reading function.

        Returns:
            Returns the FT_ICR_SignalCollection object including all the
            data.
        """
        file_list = get_file_list(directory_path, extension, sorting=sorting)
        if scan_range is not None:
            scan_i, scan_f = scan_range
            file_list = file_list[scan_i: scan_f + 1]
        
        signals = list()
        for filename in file_list:
            if verbose:
                print(filename)
            data = read_signal(filename)
            signals.append(data)

        return cls(signals=signals)

    def __iter__(self) -> Iterator[FT_ICR_Signal]:
        return iter(self._signals)

    def __getitem__(self, ind: Union[int, slice]) -> Sequence[FT_ICR_Signal]:
        return self._signals[ind]

    def __len__(self) -> int:
        return len(self._signals)


# ---------------------------------------------------------
# Below that lines are old functions: to be deleted
# ---------------------------------------------------------
def coadd_signals(
    file_list: Sequence[FT_ICR_Signal],
    step_size: int
) -> Sequence[FT_ICR_Signal]:
    '''
    Reads the data from a list of .dat or .pfd files and coadds them in
    groups of step_size.

    :param file_list: The list of .dat or .pfd files.
    :param step_size: The number of files to coadd at a time.
    :param bar_length: The length of the progress bar.
    :return: The coadded signals.
    '''
    coadded_signals = []
    step = 0
    total = len(file_list)
    num_loops = ceil(total / step_size)

    for i in range(num_loops):
        signals = []
        for filename in file_list[step:step+step_size]:
            print(i, filename)
            data = read_signal(filename)
            signals.append(data.signal)

        coadded_signal = sum(signals) / len(signals)
        metadata = data.metadata.to_dict()
        metadata["step_size"] = step_size
        coadded_signals.append(
            FT_ICR_Signal(coadded_signal, **metadata)
        )
        step += step_size

    return coadded_signals


def save_coadded_signals(
    coadded_signals: Sequence,
    file_list: Sequence[Union[str, Path]],
    step_size: int,
    folder_name: Union[str, Path]
):
    '''
    Saves the coadded signals into file in a folder.

    :param coadded_signals: The coadded signals.
    :param file_list: The list of .dat or .pfd files.
    :param step_size: The number of files to coadd at a time.
    :param folder_name: The name of the folder to save the signals to.
    :param zeroorder: The zero order.
    :param file_type: The file type.
    :return: None
    '''
    folder = Path(folder_name)
    if not folder.exists():
        folder.mkdir(exist_ok=True, parents=True)

    for idx, cs in enumerate(coadded_signals):
        new_dat = folder / f'ca_{idx + 1}.dat'

        write_signal(signal=cs,
                     path=new_dat,
                     signal_file=file_list[idx * step_size])    


def coadd(
    scan_range: tuple,
    step_size: int, 
    directory: str, 
    extension: str = ".dat",
    save_folder: str = "Coadded"
):
    '''
    Coadds the signals of the range of scans in a directory, 
    grouped by step_size.

    :param scan_range: The range of scans to coadd.
    :param step_size: The number of files to coadd at a time.
    :param directory: The directory to search.
    :param extension: The extension of data file
    :param save_dir: The directory to save the coadded signals to.
    :return: None
    '''

    file_list = get_file_list(directory, extension=extension)
    file_list = file_list[scan_range[0]: scan_range[1] + 1]
    
    # Coadd signals
    log.info('Coadding signals')
    coadded_signals = coadd_signals(file_list, step_size)

    # Save coadded signals
    log.info('Saving coadded signals')
    folder_name = f'{save_folder}/{scan_range[0]+1}-{scan_range[1]}/'
    save_coadded_signals(coadded_signals, file_list, step_size, folder_name)
