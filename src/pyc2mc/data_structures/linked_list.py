import numpy as np

class Node:
    """
    Classic Node class for a doubly linked list.
    """
    def __init__(self, data):
        self.data = data
        self.prev = None
        self.next = None

class DoublyLinkedList:
    """
    Doubly linked list implementation.
    """
    def __init__(self, container = None):
        self.head = None
        self.tail = None
        self.length = 0

        if container is not None:
            for item in container:
                self.append(item)

    def insert_at_beginning(self, data):
        new_node = Node(data)
        if self.head is None:
            self.head = new_node
            self.tail = new_node
        else:
            new_node.next = self.head
            self.head.prev = new_node
            self.head = new_node
        self.length += 1

    def insert_after(self, node, data):
        if node is None:
            return

        new_node = Node(data)

        new_node.next = node.next
        if node.next is not None:
            node.next.prev = new_node

        node.next = new_node
        new_node.prev = node
        self.length += 1

        if node == self.tail:
            self.tail = new_node
    
    def insert_before(self, node, data):
        if node is None:
            return

        new_node = Node(data)

        if node.prev is None:
            # If the given node is the head, update head and new_node as the new head
            new_node.next = self.head
            self.head.prev = new_node
            self.head = new_node
        else:
            # Update new_node's next and prev pointers
            new_node.next = node
            new_node.prev = node.prev

            # Update the previous node's next pointer
            node.prev.next = new_node

            # Update the given node's prev pointer
            node.prev = new_node

        self.length += 1

    def append(self, data):
        new_node = Node(data)
        if self.tail is None:
            self.head = new_node
            self.tail = new_node
        else:
            self.tail.next = new_node
            new_node.prev = self.tail
            self.tail = new_node
        self.length += 1

    def remove(self, node):
        if node is None:
            return

        if node.prev is not None:
            node.prev.next = node.next
        else:
            self.head = node.next

        if node.next is not None:
            node.next.prev = node.prev

        if node == self.tail:
            self.tail = node.prev

        self.length -= 1

    def get_length(self):
        return self.length

    def to_numpy_array(self):
        # Create an empty array with the length of the linked list
        arr = np.empty(self.length)

        # Iterate over the linked list and populate the array
        current = self.head
        idx = 0
        while current is not None:
            arr[idx] = current.data
            current = current.next
            idx += 1

        return arr
        
    def generate_bin_centers_array(self):
        # Calculate the length of the array
        arr_length = self.length // 2

        # Create an empty NumPy array to store the bin centers
        arr = np.empty(arr_length)

        # Traverse the linked list and calculate bin centers
        current = self.head
        idx = 0
        while current and current.next:
            bin_center = (current.data + current.next.data) / 2.0
            arr[idx] = bin_center
            current = current.next.next
            idx += 1

        return arr
    
    def extend(self, container):
        """
        Extend the linked list with the given container.
        """
        for item in container:
            self.append(item)

    def non_zero_intervals(self, min_size=0):
        start_node = None
        intervals = []
        node = self.head
        i = 0
        while node:
            if node.data != 0:
                if start_node is None:
                    start_node = i
            else:
                if start_node is not None:
                    if i - start_node >= min_size:  # Check if the interval is long enough
                        intervals.append((start_node, i - 1))
                    start_node = None
            node = node.next
            i += 1

        # Check if the list ended with a non-zero sequence
        if start_node is not None and self.length - start_node >= min_size:
            intervals.append((start_node, self.length - 1))

        return intervals
    
    def to_list(self):
        """
        Converts the DoublyLinkedList into a Python list.
        """
        list_data = []
        current_node = self.head
        while current_node is not None:
            list_data.append(current_node.data)
            current_node = current_node.next
        return list_data