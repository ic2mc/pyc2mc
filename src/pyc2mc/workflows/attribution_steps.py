# coding: utf-8

import logging
from pathlib import Path
from typing import Union
from time import time
from datetime import datetime

from pyc2mc import LOGGER_NAME
from pyc2mc.processing.attribution import AttributionAlgorithm
from pyc2mc.workflows import ProcessingStep
from pyc2mc.workflows.steps_type import ProcessingStepKind
from pyc2mc.controls.attribution import (
    LowestErrorControls,
    IsotopicPatternControls,
    KendrickAttributionControls)

__all__ = ["AttributionStep"]

AttributionControls = Union[dict, LowestErrorControls,
                            IsotopicPatternControls,
                            KendrickAttributionControls]

logger = logging.getLogger(LOGGER_NAME)


class AttributionStep(ProcessingStep):
    """ This class implement the calibration of 1D data using a 
    calibration list.  """

    def __init__(self, *,
                 controls: AttributionControls = None,
                 **kwargs):
        """ This init function is used to define the main parameters.

        Args:
            controls (dict): dictionary with parameters
            kwargs are used if controls is None.
        """

        # identify algorithm
        algo_key = None
        for key in kwargs:
            if key.lower() in ["algorithm", "algo"]:
                algo_key = key

        self.algorithm = AttributionAlgorithm(kwargs.pop(algo_key))
        control_class = self.algorithm.get_controls_class()

        self.name = f"{ProcessingStepKind.ATTRIBUTE.value} - {self.algorithm}"

        # get default params
        if controls is None:
            self.controls = control_class(**kwargs)
        else:
            try:
                # controls is a dict
                self.controls = control_class(**controls)
            except TypeError:
                # assume controls is a ProcessingControls object
                self.controls = controls

    def run(self, /, **kwargs) -> dict:
        """ Apply attribution to the input data (peaklist). The attribution
        implement iteration tables by iterating over all available formula
        grids or the ones selected in the parameters.

        Args:
            sample (data): The peaklist to be attributed
            formula_grids: Formula grid to use for the attribution

        Returns:
            dict: {"data": calibrated data, "object": recalibration object}

        """
        # get formula grids list
        try:
            all_formula_grids = kwargs.pop("formula_grids")
        except KeyError:
            raise ValueError(
                "Formula Grids are missing."
            )
        if all_formula_grids is None:
            raise ValueError("Formula Grids are missing.")

        # get peaklist data to be attributed
        peaklist = ProcessingStepKind.find_member(
            ProcessingStepKind.SAMPLE, kwargs)

        # pick up selected formula grids
        formula_grids = all_formula_grids.get_subset(
            self.controls.formula_grids)

        # implement attribution
        logger.info("Start attribution")
        now = datetime.now()
        logger.info("Date: %s", now.strftime("%Y-%m-%d %H:%M"))
        start = time()
        start0 = start

        # start first iteration
        logger.info("")
        logger.info("Start iteration 1:")
        logger.info("-------------------")
        logger.info("\n%s", str(formula_grids[0]))
        att_pl = self.algorithm(
            peaklist=peaklist,
            formula_grid=formula_grids[0],
            **self.controls.get_params()
        )
        logger.info("\n%s", att_pl.str_summary())
        end = time()
        logger.info("Iteration 1 done in %.1f s", (end - start))
        start = end

        # iterate over formula grids
        for it, fg in enumerate(formula_grids[1:], 2):
            logger.info("")
            logger.info("Start iteration %d:", it)
            logger.info("-------------------")
            logger.info("%s", str(fg))

            att_pl = self.algorithm(
                peaklist=att_pl,
                formula_grid=fg,
                **self.controls.get_params()
            )
            logger.info("%s", att_pl.str_summary())
            end = time()
            logger.info("Iteration %d done in %.1f s", it, (end - start))
            start = end
        logger.info("Attribution Done in: %.1f s", (end - start0))

        if self.controls.save.save_data:
            p = Path(self.controls.save.folder)
            if not p.exists():
                p.mkdir(exist_ok=False, parents=True)
            if p.is_file():
                raise ValueError(f"'{p}' exists and is a regular file.")
            self.controls.save.save_attributed_peaklist(
                peaklist=att_pl
            )

        kwargs.update({ProcessingStepKind.SAMPLE: att_pl})
        return kwargs
