# coding: utf-8

""" This file provides a list of defined steps corresponding to a 
data processing. """

import re
from enum import Enum

# from pyc2mc.core import PyC2MCPeakObject


__all__ = ["ProcessingStepKind"]


PATTERN_MAPPING = {
    "sample": re.compile(r"data[s]?|experiment[s]?|sample[s]?"),
    "calibration": re.compile(r".*calibration.*"),
    "delimit": re.compile(r"delimit"),
    "attribute": re.compile(r"attribute|assign|attribution|annotate"),
}


class ProcessingStepKind(str, Enum):
    """ this class provide the label associated to each available step
    along the workflow. A flexibility in the step names is included 
    using regex patterns.
    """
    SAMPLE = "sample"
    CALIBRATION = "calibration"
    DELIMIT = "delimit"
    ATTRIBUTE = "attribute"

    @classmethod
    def from_alias(cls, alias: str) -> "ProcessingStepKind":
        """ Try to include a little bit of flexibility and allow few
        variation in the label names. """
        alias = alias.lower()
        for enum_member, pattern in PATTERN_MAPPING.items():
            if pattern.match(alias):
                return cls(enum_member)

        raise ValueError(f"'{alias}' is not a valid step.")

    @classmethod
    def find_member(cls, member: str, params: dict):
        """ Look for a given member in a dict of parameters and return the
        corresponding values.

        Args:
            member (ProcessingStep): the member
            params (dict): the dictionnary

        Returns
            the value associated to the key corresponding to member in the dict
        """
        if isinstance(member, str):
            member = cls(member)

        for key, value in params.items():
            if PATTERN_MAPPING[member.value].match(key):
                return value

        raise ValueError(f"Member '{member}' not found.")
