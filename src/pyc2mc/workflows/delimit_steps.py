# coding: utf-8

from typing import Union

from pyc2mc.core.peak import PeakList
from pyc2mc.controls.delimit import DelimitControls
from pyc2mc.time_dependent.td_data import TimeDependentData
from pyc2mc.workflows import ProcessingStep
from pyc2mc.workflows.steps_type import ProcessingStepKind

__all__ = ["DelimitDataStep"]


class DelimitDataStep(ProcessingStep):
    """ This class implement the step concerning the read oof the data. """

    def __init__(self,
                 controls: Union[dict, DelimitControls] = None,
                 **kwargs):
        """ This init function is used to define the main parameters.

        Args:
            controls (dict): dictionary with parameters
            kwargs are used as additional keywords
    
        """
        super().__init__(controls_class=DelimitControls,
                         controls=controls, **kwargs)
        self.name = ProcessingStepKind.DELIMIT.value

    def run(self, /, **kwargs) -> dict:
        """ Read the data.

        Args:
            kwargs are passed to the reading function

        Returns:
            dict: {"data": pyc2mc object}

        """

        data = ProcessingStepKind.find_member(
            ProcessingStepKind.SAMPLE, kwargs)

        # if data.is_1D():
        if isinstance(data, PeakList):
            # 1D sample case
            params = self.controls.get_params()
            if params:
                new_data = data.delimit(**params)

        # elif data.is_TD():
        elif isinstance(data, TimeDependentData):
            # time dependent case
            if self.controls.scan_slice is not None:
                new_data = data[self.controls.scan_slice.get_slice()]

            params = self.controls.get_params()
            if params:
                new_data = new_data.delimit(**params)

        if new_data is None:
            return {ProcessingStepKind.SAMPLE: data}
        else:
            return {ProcessingStepKind.SAMPLE: new_data}
