# coding: utf-8

from typing import Union

from pyc2mc.controls.sample import SampleImportControls
from pyc2mc.workflows import ProcessingStep
from pyc2mc.workflows.steps_type import ProcessingStepKind
from pyc2mc.io.reader import read_data

__all__ = ["ReadDataStep"]


class ReadDataStep(ProcessingStep):
    """ This class implement the step concerning the read oof the data. """

    def __init__(self,
                 controls: Union[dict, SampleImportControls] = None,
                 **kwargs):
        """ This init function is used to define the main parameters.

        Args:
            controls (dict): dictionary with parameters

        """
        super().__init__(controls_class=SampleImportControls,
                         controls=controls, **kwargs)
        
        self.name = ProcessingStepKind.SAMPLE.value

    def run(self, /, **kwargs) -> dict:
        """ Read the data.

        Args:
            kwargs are passed to the reading function

        Returns:
            dict: {"sample": pyc2mc object}

        """

        params = self.controls.get_params()
        return {ProcessingStepKind.SAMPLE: read_data(**params)}
