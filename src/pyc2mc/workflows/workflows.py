# coding: utf-8

""" This file provides a workflow class that process operations from 
a TOML file """

import re
from typing import Union, IO, Any
from pathlib import Path
from collections.abc import MutableSequence, Sequence
import logging

import tomlkit
import yaml

try:
    # python 3.11
    import tomllib
except ModuleNotFoundError:
    # python < 3.11
    import tomli as tomllib

# from pyc2mc.core import PyC2MCPeakObject
from pyc2mc import LOGGER_NAME

from pyc2mc.workflows import ProcessingStep
from pyc2mc.workflows.steps_type import ProcessingStepKind
from pyc2mc.workflows.step_selection import get_processing_step


__all__ = ["DataProcessing"]


logger = logging.getLogger(LOGGER_NAME)

PROCESSING_PATTERN = re.compile(r"processing.*|treatment.*|step.*|run")
FORMULA_GRID_PATTERN = re.compile(r"formula_grid.*|fgrid.*|tables.*|run")


def get_value_from_pattern(d: dict, pattern: re.Pattern, pop: bool = True) -> Any:
    """ Look for a key in dictionary d that match the provided pattern
    and returns the corresponding value. None is returned if there 
    is no matching key. The key is removed from the dictionary if pop is
    True. You have to pass a copy of d or set pop to False if you don't
    want to alter the dictionary.

    Args:
        d (dict): The dictionary
        pattern (Pattern): a regex pattern
        pop (bool): Default is True, remove the key from the dictionary

    Returns:
        The value associated to the given pattern
    """
    for key in d:
        if pattern.match(key):
            if pop:
                return d.pop(key)
            else:
                return d[key]

    return None


class DataProcessing(MutableSequence):
    """ This class represents a workflow divided in steps that implements
    a chain of data processing. Basically this class is a mutable sequence
    of ProcessingSteps. """

    def __init__(self, *args):
        """ init workflow from the data and a list of steps. The first step
        is supposed to be a ReadDataStep.

        Possible syntaxes include a list of steps or individual steps:

        .. code::

            Workflow(data_step, step1, step2, step3)
            Workflow([data_step, step1, step2, step3])

        Args:
            *args: ProcessingStep to append to the list.
        """

        self._steps = list()
        if len(args) > 0:
            if len(args) == 1:
                if isinstance(args[0], DataProcessing):
                    # make an instance from same type
                    self._steps[:] = args[0]._steps
                elif isinstance(args[0], Sequence):
                    # from a sequence
                    self._steps[:] = list(args[0])
                else:
                    # a single step
                    self._steps = [args[0]]
            else:
                self._steps = list(args)

        # check validity
        for step in self._steps:
            if issubclass(type(step), ProcessingStep):
                continue
            else:
                raise TypeError(
                    "Argument must be a valid ProcessingStep.\n"
                    f"Invalid step: '{step}'\ntype: ({type(step)})"
                )

    def __getitem__(self, item):
        if isinstance(item, slice):
            return self.__class__(self._steps[item])
        else:
            return self._steps[item]

    def __setitem__(self, item: int, obj: ProcessingStep, /):
        """ change a step """
        if isinstance(obj, ProcessingStep):
            self._steps[item] = obj
        else:
            raise TypeError(
                "obj must be of type ProcessingStep but is of type: "
                f"'{type(obj)}'")

    def __delitem__(self, item: int, /):
        """ delete a steps """
        del self._steps[item]

    def __len__(self):
        """ return the number of steps """
        return len(self._steps)

    def insert(self, index: int, step: ProcessingStep, /):
        """ insert a step at the position given by index """
        self._step.insert(index, step)

    @classmethod
    def load(cls, input_file: Union[str, Path, IO] = "input.toml",
             kind: str = "toml") -> "DataProcessing":
        """ load parameters from a toml or yaml file. The toml/yaml file
        is supposed to contain at least a field "sample".

        Args:
            input_file: path to the file or IO object
            kind (str): 'toml' if the input file is a TOML file or 
                'yaml' or 'yml' if the input file is in YAML syntax.

        """
        if kind.lower() == "toml":
            try:
                with open(input_file, "rb") as fi:
                    d = tomllib.load(fi)
            except TypeError:
                d = tomllib.load(input_file)
        elif kind.lower() in ["yaml", "yml"]:
            try:
                with open(input_file, "r") as fi:
                    d = yaml.load(fi, Loader=yaml.SafeLoader)
            except TypeError:
                d = yaml.load(input_file, Loader=yaml.SafeLoader)

        return cls.from_dict(d)

    @classmethod
    def from_dict(cls, d: dict) -> "DataProcessing":
        """ Load parameters from a dictionnary and returns a
        class instance. 

        Args:
            d (dict): A valid dictionary that describes the steps.

        """
        # sample step is supposed to come first
        try:
            sample_data = ProcessingStepKind.find_member(
                ProcessingStepKind.SAMPLE, d)
        except ValueError:
            # sample is not loaded
            logger.warning("no sample/data information.")
            sample_data = None

        if sample_data is None:
            steps = list()
        else:
            data_step = get_processing_step(ProcessingStepKind.SAMPLE,
                                            **sample_data)
            steps = [data_step]

        # read processing steps:
        processing_steps = get_value_from_pattern(d, PROCESSING_PATTERN)
        if processing_steps is not None:
            for step in processing_steps:
                (step_name, params), = step.items()
                step_type = ProcessingStepKind.from_alias(step_name)
                steps.append(get_processing_step(step_type, **params))

        return cls(steps)

    def as_dict(self) -> dict:
        """ Export workflow parameters as dict """
        steps = [step.controls.model_dump(exclude_none=True)
                 for step in self[1:]]
        return {
            ProcessingStepKind.SAMPLE: self[0].controls.model_dump(exclude_none=True),
            "processing": steps}

    def export(self,
               export_file: Union[str, Path, IO] = None,
               file_fmt: str = "toml") -> Union[str, None]:
        """ Export/dump the workflow in toml format """
        document = self.as_dict()

        if export_file is None:
            if file_fmt == "toml":
                return tomlkit.dumps(document)
            else:
                return yaml.d
        else:
            try:
                with open(export_file, "w", encoding="utf-8") as fo:
                    tomlkit.dump(document, fo)
            except TypeError:
                tomlkit.dump(document, export_file)

    def run(self, **kwargs):
        """ Run successively each step of the workflow. The steps are 
        applied on the provided data.

        Args:
            data (Any): The data on which the workflow will be applied
            formula_grids (FormulaGrid, FormulaGridList): the formula grid or 
                a list of formula grid.

        """
        logger.info("-" * 80)
        logger.info("PYC2MC STARTS PROCESSING YOUR DATA".center(80))
        logger.info("-" * 80 + "\n")

        # get formula grid if present
        formula_grids = get_value_from_pattern(kwargs, FORMULA_GRID_PATTERN)

        # loop over steps and apply processing successively to data
        results = kwargs.copy()
        for step in self._steps:
            logger.info("\n")
            logger.info("Step: %s", step.name)
            logger.info("-" * 80)
            results = step.run(formula_grids=formula_grids, **results)

        return results
