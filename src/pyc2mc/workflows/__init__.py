# coding: utf-8

"""
This module provides steps and sequence of steps defined as a workflow.
"""

from typing import Union
from abc import ABC, abstractmethod

from pyc2mc.controls import ProcessingControls


class ProcessingStep(ABC):
    """ This is a base class to implement steps which will be executed 
    along a workflow. 
    """

    @abstractmethod
    def __init__(self, *,
                 controls_class: ProcessingControls,
                 controls: Union[dict, ProcessingControls] = None,
                 **kwargs):
        """ This init function is used to define the main parameters.

        Args:
            controls_class (ProcessingControls): Validation class for
                parameters of the processing
            controls (dict): dictionary with parameters
            kwargs are used if params is None or a dict as additionl keywords

        """
        if controls is None:
            self.controls = controls_class(**kwargs)
        elif isinstance(controls, dict):
            controls.update(kwargs)
            self.controls = controls_class(**controls)
        else:
            self.controls = controls

    @abstractmethod
    def run(self, /, **kwargs) -> dict:
        """ This method implement the data processing and return a dict
        with the data and relevant objects. The returned dictionary
        must at least contain a key 'data'/'sample' whose value corresponds
        to the results of the step.

        The input parameters and returned dict have the same key/value 
        structure. Key being ProcessingStepKind and value the
        corresponding object.

        Args:
            kwargs: key / value pairs corresponding to processing step
                types.

        Returns:
            A dict, keys being processing step type and value the
            corresponding object
        """
        pass

    def __repr__(self):
        return self.controls.__repr__()
