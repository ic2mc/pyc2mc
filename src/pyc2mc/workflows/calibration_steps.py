# coding: utf-8

import logging
from pathlib import Path
from typing import Union

from pyc2mc import LOGGER_NAME
from pyc2mc.io.caliblist import read_caliblist
from pyc2mc.processing.recalibration import RecalibrationMethods
from pyc2mc.workflows import ProcessingStep
from pyc2mc.controls.calibration import CalibrationListControls
from pyc2mc.workflows.steps_type import ProcessingStepKind

__all__ = ["CalibrationListStep"]

logger = logging.getLogger(LOGGER_NAME)


class CalibrationListStep(ProcessingStep):
    """ This class implement the calibration of 1D data using a 
    calibration list.  """

    def __init__(self, *,
                 controls: Union[dict, CalibrationListControls] = None,
                 **kwargs):
        """ This init function is used to define the main parameters.

        Args:
            controls (dict): dictionary with parameters
            kwargs are used if controls is None.
        """
        super().__init__(controls_class=CalibrationListControls,
                         controls=controls, **kwargs)
        self.name = (f"{ProcessingStepKind.CALIBRATION.value}"
                     f" - {self.controls.method}")

    def run(self, /, **kwargs) -> dict:
        """ Apply calibration to the input peaklist. This involves two steps:

        1. load/read the calibration list
        2. build the calibration list
        3. Apply the calibration

        Args:
            sample data: The peaklist to be calibrated

        Returns:
            dict: {"data": calibrated data, "object": recalibration object}

        """

        # get the data
        peaklist = ProcessingStepKind.find_member(
            ProcessingStepKind.SAMPLE, kwargs)

        # get the calibration list
        if self.controls.caliblist is None:
            logger.info("Use calibration list from a previous step.")
            logger.info("Calibration setup parameters were not used.")
            old_recal = kwargs.get(ProcessingStepKind.CALIBRATION)
            if old_recal is None:
                raise ValueError(
                    "Calibration list is not provided and not available "
                    "from a previous step.")
            else:
                cl = old_recal.get_recalibrated_calibration_list()

        else:
            logger.info(
                "Read calibration list %s", str(self.controls.caliblist))
            cl = read_caliblist(self.controls.caliblist)
            logger.info(
                "Setup calibration list and look for corresponding"
                " experimental m/z values.")
            cl = self.controls.setup.setup_calibration_list(
                peaklist=peaklist, cl=cl)

        recal_params = self.controls.model.get_params()
        if self.controls.method == RecalibrationMethods.walking:
            recal_params.update(self.controls.walking_ctrl.get_params())

        recal = self.controls.method(calibration_list=cl, **recal_params)
        logger.info("\n%s", recal._txt_msg())

        r_peaklist = recal.recalibrate(peaklist)

        if self.controls.save is not None:
            logger.info("Save calibration list in folder: %s",
                        self.controls.save.folder)
            p = Path(self.controls.save.folder)
            if not p.exists():
                p.mkdir(exist_ok=False, parents=True)
            if p.is_file():
                raise ValueError(f"'{p}' exists and is a regular file.")

            recal_cl = recal.get_recalibrated_calibration_list()
            logger.info("    Calibration list file: %s.%s",
                        self.controls.save.basename, self.controls.save.file_fmt)
            self.controls.save.save_caliblist(cl=recal_cl)

        return {
            ProcessingStepKind.SAMPLE: r_peaklist,
            ProcessingStepKind.CALIBRATION: recal
        }


class MassDifferencesCalibrationStep(ProcessingStep):
    pass
