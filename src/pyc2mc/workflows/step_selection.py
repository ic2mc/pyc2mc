# coding: utf-8

""" This module provides a function that implements a mapping between
the ProcessingStepKind and the ProcessingSteps classes """

from pyc2mc.workflows.steps_type import ProcessingStepKind
from pyc2mc.workflows import (
    ProcessingStep,
    calibration_steps,
    data_steps,
    delimit_steps,
    attribution_steps,
)

__all__ = ["get_processing_step"]

STEPS_CLASS_MAPPING = {
    ProcessingStepKind.SAMPLE: data_steps.ReadDataStep,
    ProcessingStepKind.DELIMIT: delimit_steps.DelimitDataStep,
    ProcessingStepKind.CALIBRATION: calibration_steps.CalibrationListStep,
    ProcessingStepKind.ATTRIBUTE: attribution_steps.AttributionStep,
}


def get_processing_step(kind: ProcessingStepKind, **kwargs) -> ProcessingStep:
    """ Return the step class associated to the value

    Args:
        kind: The processing step kind
        kwargs are passed to the corresponding ProcessingStep class.

    Returns:
        The instance of the ProcessingStep 
    """

    return STEPS_CLASS_MAPPING[kind.value](**kwargs)
