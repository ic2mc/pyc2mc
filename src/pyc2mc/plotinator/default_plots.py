#!/usr/bin/env python
# coding: utf-8

"""
This module define default plot functions, color scales and plot configuration.

Attributes:
    c2mc_map (matplotlib.ColorMaps): A color map with a grayscale for small
        values stacked with Viridis color map
    c2mc_map_lighter (matplotlib.ColorMaps): Same is ``c2mc_map`` but with
        lighter colors.
    c2mc_colorscale (list): A color scale to be used with plotly.
    c2mc_colorscale_ligher (list): A ligher color scale to be used with plotly.
"""

from typing import Union, Sequence

import pandas as pd
import numpy as np
from numpy.typing import ArrayLike
import matplotlib.pyplot as plt
from matplotlib.colors import Colormap

from pyc2mc.plotinator import c2mc_map

__all__ = ["scatter_plot", "stem_plot", "bar_plot"]


def scatter_plot(
    x: ArrayLike, y: ArrayLike,
    s: ArrayLike = None,
    c: ArrayLike = None,
    cmap: Colormap = c2mc_map,
    y_bounds: tuple = None,
    scale_size: tuple = True,
    markersize: float = 50,
    normalize: str = "max",
    colorscale_label: str = "Intensity",
    scale_alpha: bool = True,
    fig: plt.Figure = None,
    **kwargs
):
    """
    Generic matplotlib scatter plot initializing method. If s is None
    and ``scale_size`` is True, the size of the points is defined as the
    product of ``markersize`` and the values of c.

    Args:
        x (ArrayLike): contains the x-axis data.
        y (ArrayLike): contains the y-axis data.
        s (ArrayLike): data to use to scale size of points
        c (ArrayLike): data to use as color scale
        cmap (Colormap): matplotlib color maps
        y_bounds (tuple): min and max bounds on y data
        scale_size (bool): If True, defaults, scale point size with s data.
        markersize (float): Size of points. if scale_size is True, s is 
            multiplied by markersize.
        normalize (str): If "max", maximum value of c is set to 100.
        colorscale_label (str): Label of the color scale axes.
        scale_alpha (bool): If True, opacity is modified between .7 and .9
            according to c values.
        fig (plt.Figure): Figure object.
        kwargs as keywords arguments of the plt.scatter function

    Returns:
        a matplotlib.pyplot.subplot ax from the scatter kind.
    """
    if fig is None:
        fig, ax = plt.subplots()
    else:
        fig.clear()
        ax = fig.add_subplot(1, 1, 1)

    normalized_c = c / np.nanmax(c)
    if normalize == "max":
        c = normalized_c * 100

    if scale_size and c is not None:
        s = markersize * normalized_c
    elif s is None:
        s = markersize

    # slight transparency for low intensity points
    if scale_alpha:
        alpha = 0.2 * normalized_c + .7
    else:
        alpha = 1

    # apply y_bounds:
    if y_bounds is not None:
        idx = np.nonzero((y >= y_bounds[0]) & (y <= y_bounds[1]))
        y = y[idx]
        x = x[idx]
        c = c[idx]
        if scale_size:
            s = s[idx]
        if scale_alpha:
            alpha = alpha[idx]

    scatter = ax.scatter(x, y, s, c, cmap=cmap, alpha=alpha, **kwargs)

    fig.colorbar(scatter, ax=ax, label=colorscale_label)
    
    return ax, scatter


def stem_plot(x: ArrayLike, y: ArrayLike, ax: plt.Axes = None, **kwargs):
    """
    Generic matplotlib stem plot initializing method.

    Args:
        x (ArrayLike): contains the x-axis data.
        y (ArrayLike): contains the y-axis data.
        ax (matplotlib.pyplot.subplot): subplot axis.
        **kwargs of the plt.stem function

    Returns:
        a matplotlib.pyplot.subplot ax from the stem kind.
    """
    if ax is None:
        ax = plt.subplot(1, 1, 1)

    ax.stem(x, y, markerfmt=" ", basefmt=" ", **kwargs)

    return ax


def bar_plot(x: ArrayLike, y: ArrayLike, ax: plt.Axes = None, **kwargs):
    """
    Generic matplotlib bar plot initializing method.

    Args:
        x (ArrayLike): contains the x-axis data.
        y (ArrayLike): contains the y-axis data.
        ax (matplotlib.pyplot.subplot): subplot axis.
        kwargs of bar plot

    Returns:
        a matplotlib.pyplot.subplot ax from the bar kind.

    """
    if ax is None:
        ax = plt.subplot(1, 1, 1)

    ax.bar(x, y, align="center", **kwargs)

    return ax


def plot_classes(
    data,  # AttributedPeakList, AttributedTimeDependantData
    monoisotopic: bool = True,
    threshold: float = None,
    cum_threshold: float = None,
    exclude: Union[str, Sequence] = None,
    orientation: str = "vertical",
    sort_chem_class: bool = False,
    ax: plt.Axes = None,
    **kwargs
):
    """ Return a bar plot of the relative abundance of the classes, in
    percent, of the attributed peak list.

    Args:
        data (AttributedPeakList, AttributedTimeDependantData): PyC2MC
            attributed object
        monoisotopic (bool): If True, only monoisotopic classes are included
        threshold (float): minimum value of the relative abundance
            in percent of the classes included.
        cum_treshold (float): cumulative value of the relative abundance
            in percent of the classes included.
        exclude (str, list-like): A sequence of chemical species to be excluded
            from the plot. For example if it is needed to remove '13C' species,
            use: exclude = '13C'.
        orientation (str): 'horizontal' (or 'h') or 'vertical' (or 'v')
            to use barh or bar, respectively.
        sort_chem_class (bool): If True, the table is sorted by
            chemical group and chemical classes. Default is False 
            the table is sorted by abundance.
        ax (plt.Axes): A matplotlib axes
        kwargs: key value arguments of the plot function

    """

    if ax is None:
        ax = plt.subplot(1, 1, 1)

    # get class data
    df = data.get_classes(monoisotopic=monoisotopic,
                          threshold=threshold,
                          cum_threshold=cum_threshold,
                          exclude=exclude,
                          sort_chem_class=sort_chem_class)

    if len(df) < 1:
        raise ValueError("Data are empty, change the thresholds.")

    # produce plots
    if orientation.lower()[0] == "h":
        # horizontal direction
        df.sort_values(by="Relative abundance (%)",
                       ascending=True).plot.barh(
            x="Chem. Class",
            y="Relative abundance (%)",
            legend=False,
            ax=ax, zorder=1, **kwargs)
        ax.set_xlabel('Relative Abundance (%)')
    else:
        # classical vertical direction
        df.plot.bar(
            x="Chem. Class",
            y="Relative abundance (%)",
            legend=False,
            ax=ax, zorder=1, **kwargs)
        ax.tick_params(axis="x", rotation=90)
        ax.set_ylabel('Relative Abundance (%)')

    ax.spines[['right', 'top']].set_visible(False)
    ax.minorticks_off()
    ax.tick_params(top=False, right=False)
    ax.grid(False)


def plot_classes_circular(
    data,  # AttributedPeakList, AttributedTimeDependantData
    monoisotopic: bool = True,
    threshold: float = None,
    cum_threshold: float = None,
    fontsize: float = 10,
    padding: int = 1,
    group_order: list = None,
    group_colors: list = None,
    fig: plt.Figure = None,
    **kwargs
):
    """ Produce a circular bar plot that displays chemical classes 
    grouped by chemical group.
    
    This plot is inspired from this page:
    https://python-graph-gallery.com/circular-barplot-with-groups/
    
    Args:
        data (AttributedPeakList, AttributedTimeDependantData): PyC2MC
            attributed object
        monoisotopic (bool): If True, only monoisotopic classes are
            included, default is True.
        threshold (float): minimum value of the relative abundance of the
            classes included.
        cum_treshold (float): cumulative value of the relative abundance
            in percent of the classes included.
        fontsize (float): font size for labels
        padding (int): space between groups
        group_order (list): order of chemical group for the plot.
        group_colors (list): Colors for each chemical group
        fig (plt.Figure): matplotlib figure
        kwargs: kwargs are passed to the matplotlib.pyplot.scatter function
            or used to setup the plot (such as xlim/ylim).

    """

    def get_label_rotation(angle, offset):
        # Rotation must be specified in degrees :(
        rotation = np.rad2deg(angle + offset)
        if angle <= np.pi:
            alignment = "right"
            rotation = rotation + 180
        else: 
            alignment = "left"
        return rotation, alignment

    if fig is None:
        fig, ax = plt.subplots(subplot_kw={"projection": "polar"})
    else:
        fig.clear()
        ax = fig.subplots(subplot_kw={"projection": "polar"})

    # set a default threshold if not provided
    if threshold is None and cum_threshold is None:
        threshold = 1

    # get class data
    df = data.get_classes(monoisotopic=monoisotopic,
                          threshold=threshold,
                          cum_threshold=cum_threshold,
                          sort_chem_class=True)
    
    # list of chemical groups and order
    chem_groups = sorted(list(df.loc[:, "Chem. Group"].unique()))
    if group_order is None:
        group_order = chem_groups
    else:
        if len(group_order) != len(chem_groups):
            raise ValueError(
                "You must provide all possible chemical groups.\n"
                f"Group order: {group_order}\n"
                f"Chemical Groups: {chem_groups}"
            )
        for chem_group in group_order:
            if chem_group not in chem_groups:
                raise KeyError(
                    f"Chem group {chem_group} not in {chem_groups}.")

    # colors of chemical groups
    if group_colors is None:
        group_colors = [f"C{i}" for i in range(len(chem_groups))]
    else:
        if len(group_colors) != len(chem_groups):
            print(f"WARNING: {len(chem_groups)} group colors needed.")
            group_colors = [f"C{i}" for i in range(len(chem_groups))]

    # sort by group and size of groups
    group_counts = df["Chem. Group"].value_counts()
    group_counts = group_counts.loc[group_order]
    groups = df.groupby("Chem. Group")
    df = pd.concat([groups.get_group(g) for g in group_order])

    # Bar height are relative abundace
    values = df["Relative abundance (%)"].values
    
    # renormalize values
    values /= values.max() / 100
    labels = df["Chem. Class"].values

    theta_offset = np.pi / 2  # offset of 0 angle as define in matplotlib polar
    n_angles = len(df) + padding * len(group_counts)
    angles = np.linspace(0, 2 * np.pi, num=n_angles, endpoint=False)

    # skip angles corresponding to empty space between groups
    offset = 0
    idx = []
    for size in group_counts:
        idx += list(range(offset + padding, offset + size + padding))
        offset += size + padding

    ax.set_theta_offset(theta_offset)
    ax.set_ylim(-80, 100)
    ax.set_frame_on(False)
    ax.xaxis.grid(False)
    ax.yaxis.grid(False)
    ax.set_xticks([])
    ax.set_yticks([])

    COLORS = [group_colors[i] 
              for i, size in enumerate(group_counts) 
              for _ in range(size)]

    ax.bar(
        angles[idx], values, width=2 * np.pi / n_angles, color=COLORS, 
        edgecolor="white", linewidth=2
    )

    # add labels
    # ----------
    # This is the space between the end of the bar and the label
    label_padding = 4
    
    # Iterate over angles, values, and labels, to add all of them.
    for angle, value, label, in zip(angles[idx], values, labels):                
        # Obtain text rotation and alignment
        rotation, alignment = get_label_rotation(angle, theta_offset)

        # And finally add the text
        ax.text(
            x=angle, 
            y=value + label_padding, 
            s=label, 
            ha=alignment, 
            va="center", 
            rotation=rotation, 
            rotation_mode="anchor",
            fontsize=fontsize,
        )

    # This iterates over the sizes of the groups adding reference
    # lines and annotations.
    offset = 0
    i = 0
    for group, size in group_counts.items():
        # Add line below bars
        x1 = np.linspace(angles[offset + padding], angles[offset + size + padding - 1], num=50)
        # ax.plot(x1, [-5] * 50, linewidth=3, color="#333333")
        ax.plot(x1, [-5] * 50, linewidth=3, color=group_colors[i])
        
        # Add text to indicate group
        rotation, alignment = get_label_rotation(np.mean(x1), theta_offset)
        ax.text(
            np.mean(x1), -20, group, fontsize=fontsize * 1.3,
            color=group_colors[i],
            fontweight="bold", ha="center", va="center",
        )

        i += 1
        offset += size + padding

    ax.text(0, -80, "Chem.\nClasses", ha="center", va="center", 
            fontweight="bold", color="#333333", fontsize=fontsize * 1.5)
