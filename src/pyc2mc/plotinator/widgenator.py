# coding: utf-8

""" This module provides functions which display widget applications
embedded in jupyter notebook. """

import ipywidgets as ipw
from IPython.display import display
import matplotlib.pyplot as plt

from pyc2mc.plotinator.peak_plotter import AttributedPeakListPlotter


def display_chem_classes(
    attpl,
    threshold: float = 0.3,
    plot_type: str = "points",
    markersize: int = 100,
    **kwargs,
):
    """ Display a notebook widget application to explore data of an
    attributed peaklist through 2D histograms of DBE against the number
    of carbon atoms for various chemical classes.

    The menus allow the selection of the chemical class, the type of plot
    and the size of the markers.

    Args:
        attpl (AttributedPeakList): An attributed peak list object
        threshold (float): A threshold values to filter the number of chemical
            class on the total intensity of the class (in percent).
        plot_type (str): plot_type is "points", "KDE" or "classes". "KDE" and
            "points" display a 2D histogram, "classes" displays an histogram
            of the chemical classes abundance.
        markersize (int): a scaling factor of marker size.
        kwargs (dict): kwargs are passed to the plot_dbe function.

    """

    output = ipw.Output()

    pl_plt = AttributedPeakListPlotter(attpl)
    df_chem_classes = attpl.get_classes(threshold=threshold)

    dropdown_chem_class = ipw.Dropdown(
        options=df_chem_classes["Chem. Class"],
        value=df_chem_classes.iat[0, 1],
        description='Chem. class')

    dropdown_plot_type = ipw.Dropdown(
        options=["points", "KDE", "classes"],
        value=plot_type,
        description="Plot type")

    int_text_markersize = ipw.IntText(
        value=markersize, description="markersize")

    def plot_2D_hist(chem_class, plot_type, markersize):
        """ plot the 2D histogram of the DBE as a function of #C for a
        given chemical class. """
        fig, ax = plt.subplots()
        pl_plt.plot_dbe(
            chem_class,
            plot_type=plot_type,
            markersize=markersize,
            fig=fig,
            **kwargs,
        )
        ax.set_title(f"Chem class {chem_class}")
        plt.close(fig=fig)
        with output:
            output.clear_output(wait=True)
            display(fig)

    def plot_chem_class_hist(threshold):
        """ plot the chemical classes histogram filtered from threshold. """
        fig, ax = plt.subplots(figsize=(12, 8))
        pl_plt.plot_classes(threshold=threshold, ax=ax)
        ax2 = ax.twinx()
        df_chem_classes.plot(y="Cumulative abundance (%)", ax=ax2, color="C3",
                             legend=False)
        ax2.grid(False)
        plt.close(fig=fig)
        with output:
            output.clear_output(wait=True)
            display(fig)

    def dropdown_chem_class_eventhandler(change):
        """ select chem class """
        chem_class_choice = change.new
        if dropdown_plot_type.value == "classes":
            dropdown_plot_type.value = "points"
        plot_2D_hist(chem_class_choice, dropdown_plot_type.value,
                     int_text_markersize.value)

    def dropdown_plot_type_eventhandler(change):
        """ select plot type: KDE or points """
        plot_type = change.new
        if plot_type == "classes":
            plot_chem_class_hist(threshold)
        else:
            plot_2D_hist(dropdown_chem_class.value, plot_type,
                         int_text_markersize.value)

    def int_text_markersize_eventhandler(change):
        """ change point size """
        if dropdown_plot_type.value == "points":
            if dropdown_plot_type.value == "classes":
                dropdown_plot_type.value = "points"
            markersize = change.new
            plot_2D_hist(dropdown_chem_class.value, dropdown_plot_type.value,
                         markersize)

    # link dropdowns to event handler
    dropdown_chem_class.observe(
        dropdown_chem_class_eventhandler, names='value')
    dropdown_plot_type.observe(dropdown_plot_type_eventhandler, names="value")
    int_text_markersize.observe(
        int_text_markersize_eventhandler, names="value")

    input_widgets = ipw.HBox(
        [dropdown_chem_class, dropdown_plot_type, int_text_markersize])
    widgets = ipw.VBox([input_widgets, output])
    display(widgets)

    if plot_type == "points":
        plot_chem_class_hist(threshold)
    else:
        plot_2D_hist(chem_class=dropdown_chem_class.value,
                     plot_type=plot_type, markersize=markersize)


def display_error(
    attpl,
    threshold: float = 0.3,
    distribution: bool = True,
    monoisotopic: bool = True,
    **kwargs,
):
    """ Display a notebook widget application to look at error plots from
    an attributed peaklist. It provides plots of error as a function of
    m/z values with the possibility to superimpose chemical classes.

    The menu allows the selection of the chemical class.

    Args:
        attpl (AttributedPeakList): An attributed peak list object
        threshold (float): A threshold values to filter the number of chemical
            class.
        distribution (bool): If True (default), the error distribution is
            plotted along with error.
        monoisotopic (bool): If True, only monoisotopic classes are
                included, default True.
        kwargs (dict): kwargs are passed to the plot_dbe function.

    """

    output = ipw.Output()

    pl_plt = AttributedPeakListPlotter(attpl)
    df_chem_classes = attpl.get_classes(threshold=threshold,
                                        monoisotopic=monoisotopic)
    chem_classes = ["all"] + list(df_chem_classes["Chem. Class"].values)

    select_chem_class = ipw.SelectMultiple(
        options=chem_classes,
        value=("all",),
        rows=20,
        description='Chem class',
    )

    show_distribution = ipw.ToggleButton(
        value=distribution,
        description='Show distribution',
        disabled=False,
        button_style='info',  # 'success', 'info', 'warning', 'danger' or ''
        tooltip='Show/Hide distribution',
        icon='chart-area',  # (FontAwesome names without the `fa-` prefix)
        layout=ipw.Layout(margin="20px auto 0 auto")
    )

    def plot_error(chem_classes, distribution):
        """ plot the error plots """
        if distribution:
            fig = plt.figure()
            pl_plt.plot_error(chem_classes, distribution=distribution,
                              fig=fig, **kwargs)
        else:
            fig, ax = plt.subplots()
            pl_plt.plot_error(chem_classes, distribution=distribution,
                              fig=fig, **kwargs)

        plt.close(fig=fig)
        with output:
            output.clear_output(wait=True)
            display(fig)

    def selection_eventhandler(change):
        """ select chem class """
        chem_classes = change.new
        plot_error(chem_classes, show_distribution.value)

    def show_distribution_eventhandler(change):
        """ select chem class """
        plot_error(select_chem_class.value, change.new)

    # link dropdowns to event handler
    select_chem_class.observe(selection_eventhandler, names='value')
    show_distribution.observe(show_distribution_eventhandler, names="value")

    input_widgets = ipw.VBox([select_chem_class, show_distribution],
                             layout=ipw.Layout(width='25%'))

    widgets = ipw.HBox([input_widgets, output])
    display(widgets)

    plot_error(select_chem_class.value, show_distribution.value)


def display_kendrick_series(ks):
    """ Display a notebook widget application to look at kendrick series.
    The menu allows to select a Kendrick series and display a plot of the
    corresponding peak with on the right side a table with the data 
    associated to the series.
    
    Args:
        ks (KendrickSeriesList): A KendrickSeriesList object.

    """

    output_graph = ipw.Output()
    output_right = ipw.Output(layout=ipw.Layout(margin="20px"))
    out_df = ipw.Output()

    # output_df = ipw.Output()

    select_ks = ipw.IntText(
        value=0,
        description='Kendric Series:',
        style={'description_width': 'initial'},
        layout=ipw.Layout(margin='20px auto 40px auto', width='50%')
    )

    def plot_ks(idx):
        """ plot the kendrick series """
        fig, ax = plt.subplots()
        ks[idx].plot(fig=fig, title=f"Kendrick Series {idx}")
        plt.close(fig=fig)
        with output_graph:
            output_graph.clear_output(wait=True)
            display(fig)

    def display_data(idx):
        """ display data frame and title """
        with out_df:
            out_df.clear_output(wait=True)
            display(ks[idx].to_dataframe())

        with output_right:
            output_right.clear_output(wait=True)
            title = ipw.HTML(value=(
                f"<h3>Kendrick Series: {idx}</h3>"
                f"<ul><li>Series length: {len(ks[idx])}</li>"
                f"<li>m/z range: [{ks[idx].mz.min():.3f}; {ks[idx].mz.max():.3f}]</li>"
                "</ul>"
            ))
            display(ipw.VBox([title, out_df]))

    def selection_eventhandler(change):
        """ select chem class """
        idx = change.new
        plot_ks(idx)
        display_data(idx)

    # link ks selection to event
    select_ks.observe(selection_eventhandler, names='value')

    grid = ipw.HBox([ipw.VBox([select_ks, output_graph]), output_right])

    display(grid)
    plot_ks(idx=0)
    display_data(idx=0)
