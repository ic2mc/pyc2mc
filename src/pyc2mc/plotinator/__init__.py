# coding: utf-8

""" This module provides classes or function in order to display static
or interactive plots from the main classes of PyC2MC. The ``ipympl`` 
package is needed to display the widgets. """

from pathlib import Path
from typing import Union
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap

# define the pyc2mc color scales
# from white to black folowed by viridis
dark_scale = plt.cm.binary(np.linspace(0.3, 1, 25))
viridis_scale = plt.cm.viridis(np.linspace(0, 1, 231))
dark2_scale = plt.cm.binary(np.linspace(0.01, 1, 25))
colors_1 = np.vstack((dark_scale, viridis_scale))
colors_lighter = np.vstack((dark2_scale, viridis_scale))

c2mc_map = LinearSegmentedColormap.from_list('C2MC colormap', colors_1)
c2mc_map_lighter = LinearSegmentedColormap.from_list(
    'C2MC colormap lighter', colors_lighter)

# plotly colorscales
c2mc_colorscale = [
    [i / 255, f"rgb({r * 255:.0f}, {g * 255:.0f}, {b * 255:.0f})"]
    for i, (r, g, b, a) in enumerate(colors_1)
]
c2mc_colorscale_lighter = [
    [i / 255, f"rgb({r * 255:.0f}, {g * 255:.0f}, {b * 255:.0f})"]
    for i, (r, g, b, a) in enumerate(colors_lighter)
]

# default plotly template
PLOTLY_TEMPLATE = "simple_white"

# default pyc2mc style sheet
pyc2mc_style_sheet = Path(__file__).parent / "pyc2mc.mplstyle"


def set_pyc2mc_plot_style(
        style: Union[str, list, dict, Path] = pyc2mc_style_sheet):
    """ Set the pyc2mc plot style for matplotlib 
    
    Args:
        style: all accepted params for the matplotlib.style.use function.

    """
    plt.style.use(pyc2mc_style_sheet)
