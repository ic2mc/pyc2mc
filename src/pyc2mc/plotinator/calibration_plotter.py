#!/usr/bin/env python
# coding: utf-8

"""
This module contains all the methods and classes to perform the different
visualization task for calibration list.
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import gaussian_kde

from pyc2mc.processing.recalibration_models import RecalibrationModels as Models

__all__ = ["CalibListPlotter"]


class CalibListPlotter:
    """ This class contains plotting functions for the calibration list and
    the results of the recalibration. """

    def __init__(self, calib_list):
        """
        Instance method of the CalibrationPlotter.

        Args:
            calib_list (pyc2mc.core.calibration.CalibList): a CalibList object

        """
        self.calib_list = calib_list

    def hist(self, ax: plt.Axes = None, **kwargs):
        """ Plot an histogram of the theroetical mz values in the
        calibration list.
        
        Args:
            ax (plt.Axes): a matplotlib axes
            kwargs are passed to the hist function of matplotlib.
            
        """        
        if ax is None:
            ax = plt.subplot(111)

        ax.hist(self.calib_list.mz_theo, rwidth=.9, **kwargs)
        ax.set_xlabel("theoretical m/z (Da)")
        ax.set_ylabel("# calibration points")

    def error_trend(
        self,
        recalibration=None,
        npts_model: int = 100,
        fig: plt.Figure = None,
        **kwargs
    ):
        """ Plot the error in ppm as a function the theoretical m/z values
        of the calibration points. This plot needs a calibration list which
        already contains the experimental m/z values.

        Args:
            recalibration (pyc2mc.processing.recalibration.Recalibration):
                A Recalibration object, containing the model and recalibration
                parameters.
            npts_model (int): number of point to plot the model
            fig (plt.Figure): a matplotlib figure
            kwargs are passed to the plot function of matplotlib.

        """
        if not self.calib_list.is_mz_exp_avail:
            raise ValueError("Cannot plot error trend without experimental m/z.")

        if fig is None:
            fig, ax = plt.subplots()
        else:
            fig.clear()
            ax = fig.subplots()

        ax.plot(self.calib_list.mz_theo, self.calib_list.err_ppm, "o",
                label="Calib. points", **kwargs)

        # add model error if provided
        if recalibration is not None:
            mz_min = self.calib_list.mz_theo.min()
            mz_max = self.calib_list.mz_theo.max()
            mz_theo = np.linspace(mz_min, mz_max, npts_model)
            if recalibration.model == Models.LEDFORD:
                f_min = self.calib_list.frequency.min()
                f_max = self.calib_list.frequency.max()
                X = np.linspace(f_min, f_max, npts_model)
            elif recalibration.model == Models.MASSELON:
                f_min = self.calib_list.frequency.min()
                f_max = self.calib_list.frequency.max()

                i_min = self.calib_list.intensity.min()
                i_max = self.calib_list.intensity.max()

                X = (np.linspace(f_min, f_max, npts_model),
                     np.linspace(i_min, i_max, npts_model))
            else:
                X = mz_theo
            
            mz_model = recalibration.model(
                X, fit_intercept=recalibration.fit_intercept,
                **recalibration.optimized_parameters)
            err_model = (mz_theo - mz_model) / mz_model * 1e6
            ax.plot(mz_theo, err_model, "C3--", label="model")
            ax.legend()

        ax.set_xlabel("theoretical m/z (Da)")
        ax.set_ylabel("Error (ppm)")
        ax.yaxis.set_major_formatter('{x:,.3f}')

    def residuals(
        self,
        fig: plt.Figure = None,
        recalibration=None,
        distribution: bool = True,
        **kwargs
    ):
        """
        Plot the error between the theoretical mz and the recalculated mz
        in ppm.

        Args:
            ax (plt.Axes): a matplotlib axes
            recalibration (pyc2mc.processing.recalibration.Recalibration):
                A Recalibration object, containing the model and recalibration
                parameters.
            distribution (bool): displays the KDE distribution of the
                calibrated reference mz. If distribution is True,
                the method do not accept axes.
        """
        if fig is None:
            fig = plt.figure()

        if distribution:
            ax, ax2 = fig.subplots(
                ncols=2, sharey=True,
                gridspec_kw=dict(width_ratios=(3, 1), wspace=0)
            )
        else:
            ax = fig.add_subplot(1, 1, 1)

        mz_theo = self.calib_list.mz_theo
        mz_calc = recalibration.calibrated_mz
        y_err = (mz_calc - mz_theo) / mz_theo * 1e6
        
        ax.plot(mz_calc, y_err, "C0o", **kwargs)
        ax.axhline(0, color="C3", linestyle="--", linewidth=1)
        ax.set_xlabel('m/z (Da)')
        ax.set_ylabel('Residuals (ppm)')
        ax.yaxis.set_major_formatter('{x:,.3f}')

        if np.abs(y_err).max() < 0.05:
            ax.set_ylim(-0.05, 0.05)

        if distribution:
            x_error = np.linspace(y_err.min(), y_err.max(), 200)
            error_kde = gaussian_kde(y_err)
            ax2.axhline(0, color="C3", linestyle="--", linewidth=1)
            ax2.plot(error_kde(x_error), x_error, "C0-") 


class TimeDependentCalibrationPlotter:
    """ This class contains plotting functions for the calibration list and
    the results of the recalibration. """

    def __init__(self, td_recal):
        """
        Instance method of the TimeDependentCalibrationPlotter.

        Args:
            rec (pyc2mc.time_dependent.recalibration.TimeDependentCalibration): a
                TimeDependentCalibration.

        """
        self.td_recal = td_recal

    def plot_n_data_points(
        self,
        ax: plt.Axes = None,
        dt: float = None,
        t0: float = None,
        xlabel: str = "Scans",
    ):
        """ Plot the number of calibration points in the calibration 
        list along each scans.

        Args:
            ax (plt.Axes): A matplotlib ax from subplot.
            dt (float): time step between each scan.
            t0 (float): first value on the time axis.
            xlabel (str): Label of the x axes.

        """

        if ax is None:
            ax = plt.subplot(111)

        tps = self.td_recal.td_data.get_retention_times(dt, t0)
        data_points = [r.n_data_points for r in self.td_recal.recalibrations]
        ax.plot(tps, data_points)
        ax.set_xlabel(xlabel)

    def error_trend(
        self,
        ax: plt.Axes = None,
        dt: float = None,
        t0: float = None,
        xlabel: str = "Scans",
        recalibrated: bool = False,
        percentiles: list = [5, 25, 50, 75, 95],
    ):
        """ Plot the median error of the calibration points and highlight
        the inter-quartile intervals for calibration lists before or
        after recalibration of the scans.

        Args:
            ax (plt.Axes): A matplotlib ax from subplot.
            dt (float): time step between each scan.
            t0 (float): first value on the time axis.
            xlabel (str): Label of the x axes.
            recalibrated (bool): If True, use recalibrated calibration list,
                default False.
            percentiles (list): List of 5 percentiles to plot. The
                third one is supposed to be the median. The area
                are plotted between 1 and 3 and then between 0 and 4.
        """

        if ax is None:
            ax = plt.subplot(111)

        # tps
        tps = self.td_recal.td_data.get_retention_times(dt, t0)

        # compute percentiles
        if recalibrated:
            percentiles = np.array([
                np.percentile(caliblist.err_ppm, percentiles)
                for caliblist in self.td_recal.calibrated_caliblists
            ])
        else:
            percentiles = np.array([
                np.percentile(recal.calibration_list.err_ppm, percentiles)
                for recal in self.td_recal.recalibrations
            ])

        # plot median
        ax.plot(
            tps,
            percentiles[:, 2],
            "C3o",
            zorder=3,
        )
        # first interval between Q1 and Q3
        ax.fill_between(
            tps,
            percentiles[:, 1],
            percentiles[:, 3],
            color="C0",
            alpha=.7,
            zorder=2,
        )
        # 95% interval
        ax.fill_between(
            tps,
            percentiles[:, 0],
            percentiles[:, 4],
            color="C7",
            alpha=.4,
            zorder=1,
        )
        ax.set_xlabel(xlabel)
        ax.set_ylabel("Error (ppm)")

    def plot_error_trend(
        self,
        iscan: int,
        fig: plt.Figure = None,
        **kwargs
    ):
        """ Plot the residuals for one give scan 
        
        Args:
            iscan (int): Index of the required scan
            fig (plt.Figure): a figure object for the plot
            kwargs of the ``CalibListPlotter.error_trend`` function.
        """
        recal = self.td_recal.recalibrations[iscan]
        cl = self.td_recal.recalibrations[25].calibration_list
        cl.plot.error_trend(recalibration=recal, fig=fig, **kwargs)

    def plot_residuals(
        self,
        iscan: int,
        fig: plt.Figure = None,
        **kwargs
    ):
        """ Plot the residuals for one give scan 
        
        Args:
            iscan (int): Index of the required scan
            fig (plt.Figure): a figure object for the plot
            kwargs of the ``CalibListPlotter.residuals`` function.
        """
        recal = self.td_recal.recalibrations[iscan]
        cl = self.td_recal.recalibrations[25].calibration_list
        cl.plot.residuals(recalibration=recal, fig=fig, **kwargs)

    def plot_parameters_trends(
        self,
        dt: float = None,
        t0: float = None,
        xlabel: str = "Scans",
        cmap: str = "PuBuGn",
        fig: plt.Figure = None,
    ):
        """ plot time evolution of the parameters 
        
        Args:
            dt (float): time step between each scan.
            t0 (float): first value on the time axis.
            xlabel (str): Label of the x axes.
            cmap (str): matplotlib color map
            fig (plt.Figure): A matplotlib figure

        """

        if fig is None:
            fig = plt.figure()
        else:
            fig.clear()

        tps = self.td_recal.td_data.get_retention_times(dt, t0)

        param_names = list("ABCDEF")

        if self.td_recal._recalibration_method.__name__ == "WalkingCalibration":
            # in case of walking recalibration of LC MS data, make a scatter 
            # plot with parameters values as a function of time and center of
            # the segments used for calibration

            # this will work only if opt_params is a rectangular array of
            # parameters. It means, the same model was used for all segments
            # and all scans
            opt_params = self.td_recal.get_opt_params()

            if len(set([popt.shape[1] for popt in opt_params])) != 1:
                raise ValueError(
                    "The model used for all segments or all scans is not the "
                    "same. You probably used a different model for the global "
                    "recalibration vs the the recalibration on each segment.\n"
                    "In that case I don't know how to do the plot."
                )
            n_params = opt_params[0].shape[1]

            segment_centers = list()
            all_tps = list()
            for iscan, recal in enumerate(self.td_recal.recalibrations):
                mz_segments = np.diff(recal.segments) / 2 + recal.segments[:-1]
                segment_centers.append(mz_segments)
                all_tps.extend([tps[iscan]] * len(mz_segments))

            segment_centers = np.concatenate(segment_centers)

            # plot each parameter on one subplot
            axes = fig.subplots(nrows=n_params, sharex=True, sharey=True) 
            for ip in range(n_params):
                params = np.concatenate([popt[:, ip] for popt in opt_params])
                vmin = params.mean() - 3 * params.std()
                vmax = params.mean() + 3 * params.std()
                scatter = axes[ip].scatter(
                    x=all_tps, y=segment_centers, 
                    c=params, vmax=vmax, vmin=vmin, 
                    cmap=cmap
                )

                axes[ip].set_title(f"<{param_names[ip]}> = {params.mean():.5e})")
                fig.colorbar(scatter, ax=axes[ip], label=f"{param_names[ip]}")

            axes[-1].set_xlabel(xlabel)
            fig.text(0.0, .5, "m/z (Center of calibration segments)",
                     rotation="vertical", va="center", ha="left")

        elif self.td_recal._recalibration_method.__name__ == "Recalibration":

            opt_params = self.td_recal.get_opt_params()

            if opt_params.shape[1] == 2:
                fig, ax = plt.subplots()
                ax.plot(tps, opt_params[:, 0], "C0o--", label="A")
                ax.set_ylabel(f"A (<A> = {opt_params[:, 0].mean():.6e})", color="C0")
                ax.tick_params(axis='y', colors='C0')
                ax.axhline(opt_params[:, 0].mean(), ls=":", color="C0")
                
                ax2 = ax.twinx()
                ax2.plot(tps, opt_params[:, 1], "C3o--", label="B")
                ax2.set_ylabel(f"B (<B> = {opt_params[:, 1].mean():.6e})", color="C3")
                ax2.tick_params(axis='y', colors='C3')
                ax2.axhline(opt_params[:, 1].mean(), ls=":", color="C3")
                ax2.grid(False)

                ax.set_xlabel(xlabel)

            else:
                fig, axes = plt.subplots(nrows=opt_params.shape[1], sharex=True)
                for ip in range(opt_params.shape[1]):
                    axes[ip].plot(tps, opt_params[:, ip], linestyle="--", marker="o")
                    axes[ip].set_ylabel(f"{param_names[ip]}")
                    axes[ip].text(
                        1, 1, ha="right", va="bottom",
                        s=f"<{param_names[ip]}> = {opt_params[:, ip].mean():.4e}",
                        transform=axes[ip].transAxes
                    )

                axes[-1].set_xlabel(xlabel)
