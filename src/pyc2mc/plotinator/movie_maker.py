# coding: utf-8

""" This module provide a class """

import re
from pathlib import Path
from typing import Callable, Sequence

import numpy as np
import matplotlib.pyplot as plt
import cv2


class MovieMaker:
    """ This class provides a way to output a mp4 movie from a set of image. """
    
    def __init__(self, duration: float = 100, frame_per_second: int = 24,
                 step: int = 1, dpi: int = 150, img_fmt: str = "png"):
        """ Initialize the class with video parameter.
        
        Args:
            duration (float): duration in ms of each picture
            frame_per_second (int): number of frame per second in the final video
            step (int): step between two elements included in the video
            dpi (int): pictures resolution. Be careful, this will strongly
                impact the size of the pictures. 150 is good, 300 is a high
                quality.
            img_fmt (str): format of images.
        """
        self.duration = duration
        self.frame_per_second = frame_per_second
        self.step = step
        self.dpi = int(dpi)
        self.img_fmt = img_fmt
    
    @property
    def n_repeat(self) -> int:
        """ Number of times each frame has to be repeated to reach the required
        duration and frame per second parameters. """
        return int(self.duration * self.frame_per_second / 1000)

    @staticmethod
    def get_pictures_from_folder(
        path: str = "./",
        basename: str = "",
        ext: str = "png"
    ) -> list:
        """ Read in the given path and return a list of sorted picture 
        according to the basename and extension. In order to be able to sort
        the pictures all file names are supposed to be such as 
        ``'basename'_XX.'ext'``, where ``basename`` is common in all file
        names and ``ext`` is the extension (for example ``.png``).
        
        Args:
            path (str): Folder containing the pictures
            basename (str): basename of the pictures.
            ext (str): extension of the picture, without dot
            
        Returns:
            A list of sorted pictures.
        """
        
        path = Path(path)
        indexes = list()
        pictures = list()
        for pict in path.glob(f"{basename}*.{ext}"):
            name = pict.name.strip(basename).strip(ext)
            num = re.findall(r"\d+", name)
            if len(num) == 0:
                raise ValueError(
                    f"Wrong file name, index cannot be determined: '{pict}'.")
            else:
                indexes.append(int(num[-1]))
            pictures.append(pict)
                
        pictures = [pictures[i] for i in np.argsort(indexes)]

        return pictures
    
    def make_pictures(
        self,
        sequence: Sequence,
        plotter: Callable,
        folder: str = "pictures/",
        basename: str = "frame",
        **kwargs
    ):
        """ This function produce all the figures to be included in the
        video. 
        
        Args:
            sequence: An iterable object one picture is produced for each
                element of the iterable.
            plotter: The function which produces the plot. The first argument
                of the function must be the element of the iterable and
                the function has to return a matplotlib.Figure object.
            folder (str): Path to the folder where to store the files
            basename (str): Basename of the figures file.
            kwargs are transferred to the plotter function.

        Returns:
            The list of path of the generated figures.
            
        """

        # set up output folder
        path = Path(folder)
        path.mkdir(exist_ok=True, parents=True)

        # make figures
        pictures = list()
        for i in range(0, len(sequence), self.step):
            filename = path / f"{basename}_{i}.{self.img_fmt}"
            pictures.append(filename)

            # make figures
            fig = plotter(sequence[i], **kwargs)

            # avoid transparent background
            fig.patch.set_facecolor('white')
            
            # save the picture
            fig.savefig(filename, dpi=self.dpi)
            plt.close(fig)
        
        return pictures
    
    def run(self, pictures: list, output: str = "output.mp4", **kwargs):
        """ Make the movie form the list of pictures provided as argument.

        Args:
            pictures (list): List of path to the figures
            output (str): name of the output video.
            kwargs contains parameters of the movie generation (duration,
                frame_per_second, step) ...
        """
        # update parameters
        if "step" in kwargs:
            self.step = kwargs.pop("step")
        if "duration" in kwargs:
            self.duration = kwargs.pop("duration")
        if "frame_per_second" in kwargs:
            self.frame_per_second = kwargs.pop("frame_per_second")

        # initialization
        fourcc = cv2.VideoWriter_fourcc(*"mp4v")
        frame = cv2.imread(str(pictures[0]))
        
        h, w, _ = frame.shape
        writer = cv2.VideoWriter(output, fourcc, self.frame_per_second, (w, h))
        
        for i in range(0, len(pictures), self.step):
            frame = cv2.imread(str(pictures[i]))
            
            for _ in range(self.n_repeat):
                writer.write(frame)
        
        writer.release()
