#!/usr/bin/env python
# coding: utf-8

"""
This module provides classes to display plots of N dimensional data.
"""

from typing import Union

import matplotlib.pyplot as plt
from matplotlib.colors import Colormap

import numpy as np
import pandas as pd
from numpy.typing import ArrayLike
from pyc2mc.core.formula import Formula
from pyc2mc.plotinator import c2mc_map
from .default_plots import scatter_plot, plot_classes, plot_classes_circular

__all__ = ["TDDataPlotter", "AttributedTDDataPlotter"]


class TDDataPlotter:
    """
    This class contains the methods to obtain the plots for the raw time-resolved
    data from Chromatography + HR Mass Spectrometry. The Acronym HMSData stands for
    Hyphenated Mass Spectrometry Data.
    """

    def __init__(self, td_data):
        """
        Instance method of the HMSDataPlotter class.

        Args:
            raw_data (pyc2mc.io.nddata.HyphenantedMassSpecData): object 
                containing the lists of mz and intensities from the 
                time-resolved data (scans).
        """
        self._data = td_data

    def plot_TIC(
        self,
        normalize: str = "max",
        dt: float = None,
        t0: float = None,
        xlabel: str = "Scans",
        show_peaks_nbr: bool = False, 
        smoothing: bool = False,
        smooth_kws: dict = None,
        fig: plt.Figure = None,
        **kwargs
    ):
        """
        Plot the total ion chromatogram of the raw data.

        Args:
            normalize (str): 'max', 'integral' or '', for normalization
            dt (float): time step between each scan.
            t0 (float): first value on the time axis.
            xlabel (str): Label of the x axes.
            show_peak_nbr (bool): If True, show the number of peaks for each
                scan along with the TIC.
            smoothing (bool): if True a smoothing TIC is plotted
            smooth_kws (dict): smoothing options.
            fig (plt.Figure): A matplotlib figure
            kwargs as available in ax.plot().

        """
        if fig is None:
            fig, ax = plt.subplots()
        else:
            fig.clear()
            ax = fig.add_subplot(1, 1, 1)

        # plot tic
        tic = self._data.get_total_ion_chromatogram(normalize)
        tps = self._data.get_retention_times(dt, t0)
        if smoothing:
            smooth_kws = {} if smooth_kws is None else smooth_kws
            smoothed_tic = self._data.get_total_ion_chromatogram(
                normalize=normalize, smoothing=True, smooth_kws=smooth_kws)
            ax.plot(tps, smoothed_tic, "C3-", label='smoothed TIC', **kwargs)
        else:
            ax.plot(tps, tic, "C3-", label='TIC', **kwargs)
        
        if show_peaks_nbr:
            ax2 = ax.twinx()
            dt = 1 if dt is None else dt
            ax2.bar(tps, self._data.scan_lengths, width=0.8 * dt, alpha=.5, color="C7")
            ax2.grid(False)
            ax2.set_ylabel("#peaks", color="C7")
            ax2.tick_params(axis='y', labelcolor="C7")

            ax.set_zorder(ax2.get_zorder() + 1) # put ax in front of ax2
            ax.patch.set_visible(False)

        ax.set_xlabel(xlabel)
        if normalize not in ["max", "integral"]:
            ax.set_ylabel("Total intensity")
        else:
            ax.set_ylabel("Normalized intensity")
        ax.legend(loc='best')

    def plot_BPC(
        self,
        normalize: str = "max",
        dt: float = None,
        t0: float = None,
        xlabel: str = "Scans",
        show_mz: bool = False, 
        smoothing: bool = False,
        smooth_kws: dict = None,
        fig: plt.Figure = None,
        **kwargs
    ):
        """
        Plot the total ion chromatogram of the raw data.

        Args:
            normalize (str): 'max', 'integral' or '', for normalization
            dt (float): time step between each scan.
            t0 (float): first value on the time axis.
            xlabel (str): Label of the x axes.
            show_mz (bool): If True, show the mz corresponding to the max
                intensity along the BPC.
            smoothing (str): if True show a smoothed BPC.
            smooth_kws (dict): smoothing options.
            fig (plt.Figure): A matplotlib figure.
            kwargs as available in ax.plot().

        """
        if fig is None:
            fig, ax = plt.subplots()
        else:
            fig.clear()
            ax = fig.add_subplot(1, 1, 1)

        # get bpc
        smooth_kws = {} if smooth_kws is None else smooth_kws
        label = "smoothed BPC" if smoothing else "BPC"
        
        bpc, mz = self._data.get_base_peak_chromatogram(
            normalize=normalize, smoothing=smoothing, 
            smooth_kws=smooth_kws, return_mz=True)
        tps = self._data.get_retention_times(dt, t0)
        ax.plot(tps, bpc, "C3-", label=label, **kwargs)
        
        if show_mz:
            ax2 = ax.twinx()
            c = plt.rcParams["axes.prop_cycle"].by_key()["color"][0]

            ax2.scatter(tps, mz, facecolor=c + "44", edgecolors="C0", marker="o")
            ax2.grid(False)
            ax2.set_ylabel("m/z (Da)", color="C0")
            ax2.tick_params(axis='y', labelcolor="C0")

            ax.set_zorder(ax2.get_zorder() + 1) # put ax in front of ax2
            ax.patch.set_visible(False)

        ax.set_xlabel(xlabel)
        ax.set_ylabel('Relative Abundance')
        ax.legend(loc='best')

    def plot_EIC(
        self,
        mz_target: float = None,
        formula: Union[str, Formula] = None,
        lambda_parameter: float = 1,
        normalize: str = "max",
        dt: float = None, 
        t0: float = None, 
        xlabel: str = "Scans",
        show_mz: bool = True,
        show_tic: bool = False,
        polarity: int = None,
        fig: plt.Figure = None,
        as_dataframe: bool = False,
        **kwargs
    ):
        """ Plot the Extractive Ion Chromatogram of a given m/z value or a
        molecular formula. If you provide a molecular formula, you must also
        define the polarity to compute the ion exact mass. The EIC is
        determined according to the ``lambda_parameter`` relative error.
        If both m/z value and formula are provided, the m/z value is
        considered.

        Args:
            mz_target (float): The m/z value for which you want the EIC.
            formula (str or Formula): The molecular formula for which you want
                to get the EIC. In that case polarity must be provided to 
                compute the ion exact mass.
            lambda_parameter (float): relative error in ppm to get the EIC
            normalize (str): if ``'max'``, sets the maximum intensity of the TIC
                to 100. If ``'integral'``, divides the TIC by its integral.
            dt (float): time step between each scan.
            t0 (float): first value on the time axis.
            xlabel (str): Label of the x axes.
            show_mz (bool): If True (default), show the mz corresponding to the EIC.
            show_tic (bool): If True, superimpose EIC and TIC, default False.
            polarity (int): charge of the molecular formula.
            fig (plt.Figure): A matplotlib figure
            as_dataframe (bool): if True, return a pandas data frame, default False.
            kwargs: additional options for plots or figure

        Returns:
            The data associated to the EIC as numpy array by default or as a
            pandas dataframe if ``as_dataframe`` is True.
        """
        if fig is None:
            fig = plt.figure()
        else:
            fig.clear()

        if show_mz:
            ax1, ax2 = fig.subplots(
                nrows=2, sharex=True,
                gridspec_kw=dict(hspace=0, height_ratios=(2, 1)))
        else:
            ax1 = fig.subplots()

        dt = dt if dt is not None else self._data.dt
        t0 = t0 if t0 is not None else self._data.t0

        scans, mz, eic = self._data.get_extracted_ion_chromatogram(
            mz_target, formula, lambda_parameter, normalize, polarity)
        
        if len(scans) > 1:
            # EIC exist
            mz_bounds = (mz.min() - 3 * mz.std(), mz.max() + 3 * mz.std())

            if 'c' in kwargs:
                color = kwargs.pop['c']
            else:
                color = 'C3'
            tps = t0 + dt * scans
            ax1.plot(tps, eic, label="EIC", color=color, zorder=10)
            ax1.set_xlabel(xlabel)
            ax1.set_ylabel('Intensity')

            if show_tic:
                tic = self._data.get_total_ion_chromatogram(normalize)
                t2 = t0 + dt * np.arange(0, len(tic))
                ax1.plot(t2, tic, label="TIC", color="C7", alpha=.75)
                ax1.legend()

            if show_mz:
                if "cmap" in kwargs:
                    cmap = kwargs.pop("cmap")
                else:
                    cmap = "viridis"
                im = ax2.scatter(tps, mz, c=eic, cmap=cmap)
                ax2.set_xlabel(xlabel)
                ax2.set_ylabel('m/z (Da)')

                fig.colorbar(im, ax=(ax1, ax2), label='Intensity', shrink=.33, 
                             anchor=(0., 0.))

                ax2.set_ylim(mz_bounds)
                ax2.yaxis.set_major_formatter('{x:,.4f}')
            
            # return as dataframe or numpy arrays
            if as_dataframe:
                return pd.DataFrame({
                    "scan": scans,
                    "retentionTime": tps,
                    "mz": mz,
                    "EIC": eic
                }).set_index("scan", drop=True)
            else:
                return scans, tps, mz, eic

        else:
            return None, None, None, None

    def plot_2D_data(
        self,
        normalize: str = "max",
        markersize: float = 50,
        dt: float = None,
        t0: float = None, 
        xlabel: str = "Scans",
        mz_bounds: tuple = None,
        scale_intensity: bool = True,
        cmap: Colormap = c2mc_map,
        fig: plt.Figure = None,
        **kwargs
    ):
        """
         
        Args:
            normalize (bool): if True, default, sets the maximum intensity to 100.
            markersize (float): Default is 50, the size of the points is
                computed as the intensity times markersize.
            dt (float): time step between each scan.
            t0 (float): first value on the time axis.
            xlabel (str): Label of the x axes.
            mz_bounds (tuple): Min and max mz values to delimit the plot
            scale_intensity (bool): If True, markersize is scale by intensity.
            cmap (Colormap): matplotlib color maps
            fig (plt.Figure): A matplotlib figure.
            kwargs as keyword arguments of the scatter function.

        """

        # x axis
        t = self._data.get_retention_times(dt, t0)
        n_data = self._data.mz.shape[1]
        tps = np.array([np.full(n_data, ti) for ti in t])

        # change intensity to 0 if nan (alpha calculations)
        c = np.nan_to_num(self._data.intensity)

        ax, _ = scatter_plot(
            x=tps,
            y=self._data.mz,
            c=c,
            cmap=cmap,
            y_bounds=mz_bounds,
            scale_size=scale_intensity,
            markersize=markersize,
            normalize=normalize,
            colorscale_label="Intensity",
            fig=fig,
            **kwargs,
        )
        
        ax.grid(False)
        ax.set_ylabel("m/z (Da)")
        ax.set_xlabel(xlabel)

    def __call__(self, *args, **kwargs):
        return self.plot_TIC(*args, **kwargs)


class AttributedTDDataPlotter(TDDataPlotter):
    """
    This class contains the methods to obtain the plots for attributed
    time-resolved MS data.
    """

    def __init__(self, td_data):
        """
        Instance method of the AttributedTDDataPlotter class.

        Args:
            td_data (pyc2mc.time_dependent.td_data.AttributedTimeDependentData):
                object containing a list of attributed Peak list.
        """
        super().__init__(td_data)

    def plot_chem_group_chromatogram(
        self,
        chem_group: Union[str, list] = None,
        monoisotopic: bool = True,
        normalize: str = "max",
        dt: float = None,
        t0: float = None,
        xlabel: str = "Scans",
        show_tic: bool = True,
        smoothing: bool = False,
        smooth_kws: dict = None,
        scale_tic: float = 1.,
        ax: plt.Axes = None,
        **kwargs
    ):
        """
        Plot the chemical group chromatogram corresponding to the total
        intensity over molecular formula belonging to a chemical group
        as a function of time.

        Args:
            chem_group (str or list): List of chemical group to be considered
            monoisotopic (bool): If True, the monoisotopic groups are considered
            normalize (str): 'max', 'integral' or '', for normalization
            dt (float): time step between each scan.
            t0 (float): first value on the time axis.
            xlabel (str): Label of the x axes.
            show_TIC (bool): If True, show TIC in second axis
            smoothing (bool): if True a smoothing TIC is plotted
            smooth_kws (dict): smoothing options.
            scale_tic (float): scaling factor for TIC.
            ax (plt.Axes): A matplotlib axes
            kwargs as available in ax.plot().

        """
        if ax is None:
            ax = plt.subplot(1, 1, 1)

        if chem_group is None:
            raise ValueError("You must provide a chemical group.")
        elif isinstance(chem_group, str):
            chem_group = [chem_group]

        tps = self._data.get_retention_times(dt=dt, t0=t0)

        if show_tic:
            tic = scale_tic * self._data.get_total_ion_chromatogram(
                normalize=normalize, smoothing=smoothing, smooth_kws=smooth_kws)
            ax.fill_between(tps, tic, label="TIC", color="C7", alpha=.5)
        
        for cg in chem_group:
            cgc = self._data.get_chem_group_chromatogram(
                cg, normalize=normalize, monoisotopic=monoisotopic,
                smoothing=smoothing, smooth_kws=smooth_kws)
            ax.plot(tps, cgc, label=cg)

        ax.set_xlabel(xlabel)
        ax.legend()
        if normalize not in ["max", "integral"]:
            ax.set_ylabel("Total intensity")
        else:
            ax.set_ylabel("Normalized intensity")

    def plot_chem_class_chromatogram(
        self,
        chem_class: Union[str, list] = None,
        monoisotopic: bool = True,
        normalize: str = "max",
        dt: float = None,
        t0: float = None,
        xlabel: str = "Scans",
        show_tic: bool = True,
        smoothing: bool = False,
        smooth_kws: dict = None,
        scale_tic: float = 1.,
        ax: plt.Axes = None,
        **kwargs
    ):
        """
        Plot the chemical class chromatogram corresponding to the total
        intensity over molecular formula belonging to a chemical class
        as a function of time.

        Args:
            chem_class (str or list): List of chemical class to be considered
            monoisotopic (bool): If True, the monoisotopic classes are considered
            normalize (str): 'max', 'integral' or '', for normalization
            dt (float): time step between each scan.
            t0 (float): first value on the time axis.
            xlabel (str): Label of the x axes.
            show_TIC (bool): If True, show TIC in second axis
            smoothing (bool): if True a smoothing TIC is plotted
            smooth_kws (dict): smoothing options.
            scale_tic (float): scaling factor for TIC.
            ax (plt.Axes): A matplotlib axes
            kwargs as available in ax.plot().

        """
        if ax is None:
            ax = plt.subplot(1, 1, 1)

        if chem_class is None:
            raise ValueError("You must provide a chemical class.")
        elif isinstance(chem_class, str):
            chem_class = [chem_class]

        tps = self._data.get_retention_times(dt=dt, t0=t0)

        if show_tic:
            tic = scale_tic * self._data.get_total_ion_chromatogram(
                normalize=normalize, smoothing=smoothing, smooth_kws=smooth_kws)
            ax.fill_between(tps, tic, label="TIC", color="C7", alpha=.5)
        
        for cc in chem_class:
            ccc = self._data.get_chem_class_chromatogram(
                cc, normalize=normalize, monoisotopic=monoisotopic,
                smoothing=smoothing, smooth_kws=smooth_kws)
            ax.plot(tps, ccc, label=cc)

        ax.set_xlabel(xlabel)
        ax.legend()
        if normalize not in ["max", "integral"]:
            ax.set_ylabel("Total intensity")
        else:
            ax.set_ylabel("Normalized intensity")

    def plot_chem_data_chromatogram(
        self,
        chem_class: Union[str, list] = None,
        chem_group: Union[str, list] = None,
        monoisotopic: bool = True,
        normalize: str = "max",
        dt: float = None,
        t0: float = None,
        xlabel: str = "Scans",
        show_tic: bool = True,
        smoothing: bool = False,
        smooth_kws: dict = None,
        scale_tic: float = 1.,
        fig: plt.Figure = None,
        **kwargs
    ):
        """
        Plot the chemical classes or group chromatogram corresponding to the total
        intensity over molecular formula belonging to a chemical group
        as a function of time.

        Args:
            chem_class (str or list): List of chemical classes to be considered
            chem_group (str or list): List of chemical group to be considered
            monoisotopic (bool): If True, the monoisotopic groups are considered
            normalize (str): 'max', 'integral' or '', for normalization
            dt (float): time step between each scan.
            t0 (float): first value on the time axis.
            xlabel (str): Label of the x axes.
            show_TIC (bool): If True, show TIC in second axis
            smoothing (bool): if True a smoothing TIC is plotted
            smooth_kws (dict): smoothing options.
            scale_tic (float): scaling factor for TIC.
            fig (plt.Figure): A matplotlib figure
            kwargs as available in ax.plot().

        """
        if fig is None:
            fig, ax = plt.subplots()
        else:
            fig.clear()
            ax = fig.subplots()

        # make chem group/class a list
        if chem_group is None and chem_class is None:
            show_tic = True
            # raise ValueError("You must provide a chemical group or class.")
        if isinstance(chem_group, str):
            chem_group = [chem_group]
        elif chem_group is None:
            chem_group = list()
        if isinstance(chem_class, str):
            chem_class = [chem_class]
        elif chem_class is None:
            chem_class = list()

        tps = self._data.get_retention_times(dt=dt, t0=t0)
        tic = self._data.get_total_ion_chromatogram(
            normalize=normalize, smoothing=smoothing, smooth_kws=smooth_kws)
        df = pd.DataFrame({"retentionTime": tps, "TIC": tic})

        if show_tic:
            ax.fill_between(tps, scale_tic * tic, label="TIC", color="C7",
                            alpha=.5)
        
        # plot chemical groups
        for cg in chem_group:
            cgc = self._data.get_chem_group_chromatogram(
                cg, normalize=normalize, monoisotopic=monoisotopic,
                smoothing=smoothing, smooth_kws=smooth_kws)
            ax.plot(tps, cgc, label=cg)
            df[cg] = cgc

        # plot chemical classes
        for cc in chem_class:
            ccc = self._data.get_chem_class_chromatogram(
                cc, normalize=normalize, monoisotopic=monoisotopic,
                smoothing=smoothing, smooth_kws=smooth_kws)
            ax.plot(tps, ccc, label=cc)
            df[cc] = ccc

        ax.set_xlabel(xlabel)
        ax.legend()
        if normalize not in ["max", "integral"]:
            ax.set_ylabel("Total intensity")
        else:
            ax.set_ylabel("Normalized intensity")

        return df

    def plot_DBE_chromatogram(
        self,
        DBE_values: Union[str, list] = None,
        normalize: str = "max",
        dt: float = None,
        t0: float = None,
        xlabel: str = "Scans",
        show_tic: bool = True,
        smoothing: bool = False,
        smooth_kws: dict = None,
        scale_tic: float = 1.,
        fig: plt.Figure = None,
        **kwargs
    ):
        """
        For each DBE value provided as a list, plot, as a function of time,
        a chromatogram which represents the sum of intensity of attributed
        peaks with a formula with the corresponding DBE value.

        Args:
            DBE_values (str or list): List of DBE values
            normalize (str): 'max', 'integral' or '', for normalization
            dt (float): time step between each scan.
            t0 (float): first value on the time axis.
            xlabel (str): Label of the x axes.
            show_TIC (bool): If True, show TIC in second axis
            smoothing (bool): if True a smoothing TIC is plotted
            smooth_kws (dict): smoothing options.
            scale_tic (float): scaling factor for TIC.
            ax (plt.Axes): A matplotlib axes
            kwargs as available in ax.plot().

        """
        if fig is None:
            fig, ax = plt.subplots()
        else:
            fig.clear()
            ax = fig.subplots()

        if DBE_values is None:
            raise ValueError("You must provide a DBE value.")
        elif isinstance(DBE_values, str):
            DBE_values = [DBE_values]

        tps = self._data.get_retention_times(dt=dt, t0=t0)
        tic = self._data.get_total_ion_chromatogram(
            normalize=normalize, smoothing=smoothing, smooth_kws=smooth_kws)
        df = pd.DataFrame({"retentionTime": tps, "TIC": tic})

        if show_tic:
            ax.fill_between(tps, scale_tic * tic, label="TIC", color="C7",
                            alpha=.5)
        
        for DBE_value in DBE_values:
            dbe_chromato = self._data.get_DBE_chromatogram(
                DBE_value, normalize=normalize,
                smoothing=smoothing, smooth_kws=smooth_kws)
            ax.plot(tps, dbe_chromato, label=DBE_value)
            df[f"{DBE_value:.1f}"] = dbe_chromato

        ax.set_xlabel(xlabel)
        ax.legend()
        if normalize not in ["max", "integral"]:
            ax.set_ylabel("Total intensity")
        else:
            ax.set_ylabel("Normalized intensity")

        return df

    def _plot_average(
        self,
        stats: ArrayLike,
        ylabel: str,
        dt: float = None,
        t0: float = None,
        show_extrema: bool = False,
        show_median: bool = False,
        show_IQR: bool = True,
        show_TIC: bool = False,
        xlabel: str = "Scans",
        fig: plt.Figure = None,
    ):
        """ Plot the weighted average of the provided data and the standard
        deviation as an envelope using sigma factors as multiplication factors.

        Args:
            stats (array): Array of statistical data as returned from
                AttributedTimeDependentData._get_stat_average methods.
            ylabel (str): Label of the y axis.
            dt (float): time step between each scan.
            t0 (float): first value on the time axis.
            show_extrema (bool): If True, plot min and max values of DBE,
                default False
            show_median (bool): If True, show median instead of mean.
            show_IQR (bool): If True highlight IQR region in blue.
            show_TIC (bool): If True, show TIC in second axis
            xlabel (str): Label of the x axes.
            fig (plt.Figure): A matplotlib figure.

        """

        if fig is None:
            fig, ax = plt.subplots()
        else:
            fig.clear()
            ax = fig.subplots()

        # tps
        tps = self._data.get_retention_times(dt, t0)

        # plot median
        if show_median:
            ax.plot(tps, stats[:, 6], "C3o", zorder=10, label=ylabel)
        else:
            ax.plot(tps, stats[:, 0], "C3o", zorder=10, label=ylabel)

        if show_IQR:
            ax.fill_between(tps, stats[:, 5], stats[:, 7],
                            color="C0", alpha=.7, zorder=2)
            ax.fill_between(tps, stats[:, 4], stats[:, 8],
                            color="C7", alpha=.2, zorder=1)

        if show_extrema:
            ax.plot(tps, stats[:, 2], "C6:", linewidth=1)
            ax.plot(tps, stats[:, 3], "C6:", linewidth=1)

        if show_TIC:
            ax2 = ax.twinx()
            ax2.plot(tps, self._data.get_total_ion_chromatogram(normalize="max"), "C1-")
            ax2.grid(False)
            ax2.set_ylabel("TIC", color="C1")
            ax2.tick_params(axis='y', labelcolor="C1")

            ax.set_zorder(ax2.get_zorder() + 1) # put ax in front of ax2
            ax.patch.set_visible(False)

        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)

        return pd.DataFrame({
            "RetentionTime": tps,
            f"{ylabel} average": stats[:, 0],
            f"{ylabel} median": stats[:, 6],
            f"{ylabel} std": stats[:, 1],
            f"{ylabel} Q1": stats[:, 5],
            f"{ylabel} Q3": stats[:, 7],
            f"{ylabel} IQR": stats[:, 7] - stats[:, 5],
        })

    def plot_average_DBE(
        self,
        **kwargs,
    ):
        """ Plot the weighted average of DBE and the standard deviation as
        an envelope using sigma factors as multiplicating factors.

        Args:
            dt (float): time step between each scan.
            t0 (float): first value on the time axis.
            show_extrema (bool): If True, plot min and max values of DBE,
                default False
            show_median (bool): If True, show median instead of mean.
            show_IQR (bool): If True highlight IQR region in blue.
            show_TIC (bool): If True, show TIC in second axis
            xlabel (str): Label of the x axes.
            fig (plt.Figure): A matplotlib figure.

        """
        q = [0.05, 0.25, 0.5, 0.75, 0.95]
        stats = self._data.get_averaged_DBE(q=q)

        df = self._plot_average(stats=stats, ylabel="DBE", **kwargs)
        return df

    def plot_average_elemental_ratio(
        self,
        el1: str = "H",
        el2: str = "C",
        **kwargs
    ):
        """ Plot the weighted average of two elements ratio and the standard
        deviation as an envelope using sigma factors as multiplication factors.

        Args:
            el1 (str): first element (numerator)
            el2 (str): second element (denominator)
            dt (float): time step between each scan.
            t0 (float): first value on the time axis.
            show_extrema (bool): If True, plot min and max values of DBE,
                default False
            show_median (bool): If True, show median instead of mean.
            show_IQR (bool): If True highlight IQR region in blue.
            show_TIC (bool): If True, show TIC in second axis
            xlabel (str): Label of the x axes.
            fig (plt.Figure): A matplotlib figure.

        """

        q = [0.05, 0.25, 0.5, 0.75, 0.95]
        stats = self._data.get_averaged_elemental_ratio(el1=el1, el2=el2, q=q)

        ylabel = f"{el1} / {el2}"
        df = self._plot_average(stats=stats, ylabel=ylabel, **kwargs)
        return df

    def plot_average_aromaticity(
        self,
        **kwargs
    ):
        """ Plot the weighted average of aromaticit and the standard deviation as
        an envelope using sigma factors as multiplication factors. The 
        aromaticity is defined as the ratio of DBE over H/C.

        Args:
            dt (float): time step between each scan.
            t0 (float): first value on the time axis.
            show_extrema (bool): If True, plot min and max values of DBE,
                default False
            show_median (bool): If True, show median instead of mean.
            show_IQR (bool): If True highlight IQR region in blue.
            show_TIC (bool): If True, show TIC in second axis
            xlabel (str): Label of the x axes.
            fig (plt.Figure): A matplotlib figure.

        """

        q = [0.05, 0.25, 0.5, 0.75, 0.95]
        stats = self._data.get_averaged_aromaticity(q=q)

        df = self._plot_average(stats=stats, ylabel="aromaticity", **kwargs)
        return df

    def plot_average_error_ppm(
        self,
        **kwargs
    ):
        """ Plot the weighted average of the error in ppm and the standard 
        deviation as an envelope using sigma factors as multiplication
        factors.

        Args:
            dt (float): time step between each scan.
            t0 (float): first value on the time axis.
            show_extrema (bool): If True, plot min and max values of DBE,
                default False
            show_median (bool): If True, show median instead of mean.
            show_IQR (bool): If True highlight IQR region in blue.
            show_TIC (bool): If True, show TIC in second axis
            xlabel (str): Label of the x axes.
            fig (plt.Figure): A matplotlib figure.

        """

        q = [0.05, 0.25, 0.5, 0.75, 0.95]
        stats = self._data.get_averaged_error_ppm(q=q)

        df = self._plot_average(stats=stats, ylabel="Error (ppm)", **kwargs)
        return df

    def plot_attribution_statistics(
        self,
        dt: float = None,
        t0: float = None,
        show_TIC: bool = False,
        xlabel: str = "Scans",
        fig: plt.Figure = None,
        **kwargs
    ):
        """ Plot a stacked bar plot with attribution statistics as a function
        of time. 

        Args:
            dt (float): time step between each scan.
            t0 (float): first value on the time axis.
            show_extrema (bool): If True, plot min and max values of DBE,
                default False
            show_median (bool): If True, show median instead of mean.
            show_IQR (bool): If True highlight IQR region in blue.
            show_TIC (bool): If True, show TIC in second axis
            xlabel (str): Label of the x axes.
            fig (plt.Figure): A matplotlib figure.
        """

        if fig is None:
            fig, ax = plt.subplots()
        else:
            fig.clear()
            ax = fig.subplots()
        
        tps = self._data.get_retention_times(dt=dt, t0=t0)

        # get data
        data = self._data.get_attribution_statistics()
        hits = data["hits_intensity_percentage"].values
        fill_kws = dict(step="mid", alpha=.65)
        ax.fill_between(x=tps, y1=hits, color="C0", label="hits", **fill_kws)
        ax.fill_between(tps, y1=hits, y2=np.full(hits.shape, 100), 
                        label="no hits", color="C3", linewidth=0, **fill_kws)

        # df.plot.bar(y=["hits", "no hits"], stacked=True, ax=ax)
    
        if show_TIC:
            ax2 = ax.twinx()
            ax2.plot(tps, self._data.get_total_ion_chromatogram(normalize="max"),
                     color="#333")
            ax2.grid(False)
            ax2.set_ylabel("TIC", color="C7")
            ax2.tick_params(axis='y', labelcolor="C7")
    
        # ax.set_xticks([])
        ax.legend()
        
        ax.set_ylabel("hits percentage")
        ax.set_xlabel(xlabel)
        ax.grid(False)
        ax.set_ylim(0, 100)

        return pd.DataFrame({
            "RetentionTime": tps,
            "hits (%)": hits,
        })
    
    def plot_classes(
        self,
        monoisotopic: bool = True,
        threshold: float = None,
        cum_threshold: float = None,
        orientation: str = "vertical",
        ax: plt.Axes = None,
        **kwargs
    ):
        """ Return a bar plot of the relative abundance of the chemical
        classes, over all the scans.

        Args:
            monoisotopic (bool): If True, only monoisotopic classes are included
            threshold (float): minimum value of the relative abundance
                in percent of the classes included.
            cum_treshold (float): cumulative value of the relative abundance
                in percent of the classes included.
            exclude (str, list-like): A sequence of chemical species to be excluded
                from the plot. For example if it is needed to remove '13C' species,
                use: exclude = '13C'.
            orientation (str): 'horizontal' (or 'h') or 'vertical' (or 'v')
                to use barh or bar, respectively.
            ax (plt.Axes): A matplotlib axes
            kwargs: key value arguments of the plot function

        """
        plot_classes(
            data=self._data,
            monoisotopic=monoisotopic,
            threshold=threshold,
            cum_threshold=cum_threshold,
            orientation=orientation,
            ax=ax,
            **kwargs
        )

    def plot_classes_circular(
        self,
        monoisotopic: bool = True,
        threshold: float = None,
        cum_threshold: float = None,
        fontsize: float = 10,
        padding: int = 1,
        group_order: list = None,
        group_colors: list = None,
        fig: plt.Figure = None,
        **kwargs
    ):
        """ Produce a circular bar plot that displays chemical classes 
        grouped by chemical group.
        
        This plot is inspired from this page:
        https://python-graph-gallery.com/circular-barplot-with-groups/
        
        Args:
            monoisotopic (bool): If True, only monoisotopic classes are
                included, default is True.
            threshold (float): minimum value of the relative abundance of the
                classes included.
            cum_treshold (float): cumulative value of the relative abundance
                in percent of the classes included.
            fontsize (float): font size for labels
            padding (int): space between groups
            group_order (list): order of chemical group for the plot.
            group_colors (list): Colors for each chemical group
            fig (plt.Figure): matplotlib figure
            kwargs: kwargs are passed to the matplotlib.pyplot.scatter function
                or used to setup the plot (such as xlim/ylim).

        """
        plot_classes_circular(
            data=self._data,
            monoisotopic=monoisotopic,
            threshold=threshold,
            cum_threshold=cum_threshold,
            fontsize=fontsize,
            padding=padding,
            group_order=group_order,
            group_colors=group_colors,
            fig=fig,
            **kwargs
        )
