#!/usr/bin/env python
# coding: utf-8

"""
This module contains a class to display plots of isotopic patterns.
"""

import matplotlib.pyplot as plt
from .default_plots import stem_plot

__all__ = ["IsoPatternPlotter"]


class IsoPatternPlotter:
    """
    This class is used in the private IsotopicPattern class to represent
    the isotopic pattern.
    """

    def __init__(self, iso_patt):
        """
        Instantiate the IsoPatternPlotter.

        Args:
            iso_patt (pyc2mc.core.formula.IsotopicPattern): an object of
                the IsotopicPattern class.

        """
        self.isopatt = iso_patt

    def plot(self, values: bool = False, ax: plt.Axes = None, **kwargs):
        """
        Return an ax of the plot with the isotopic pattern.

        Args:
            values (bool): display the values of each pattern at one
                side of the peak.
            ax (plt.Axes): matplotlib Axes object

        Returns:
            A matplotlib ax.
        """
        if ax is None:
            ax = plt.subplot(1, 1, 1)

        self.isopatt._get_masses()
        df = self.isopatt.to_dataframe()
        x = df['pattern_exact_mass'].values
        y = df['calc. abund.'].values * 100
        ax = stem_plot(x, y, ax=ax, **kwargs)
        ax.set_xlabel('m/z')
        ax.set_ylabel('Cal. Abundances %')
        if values:
            for i, m in enumerate(x):
                txt = str(round(m, 6))
                ax.text(m, 1.01 * y[i], txt, size=9)
        ax.set_ylim(bottom=0.)

    def __call__(self, **kwargs):
        return self.plot(**kwargs)
