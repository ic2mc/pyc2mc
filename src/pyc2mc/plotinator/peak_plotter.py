#!/usr/bin/env python
# coding: utf-8

"""
This module contains all the methods and classes to perform the different
visualization task for PeakList objects.
"""

import re
from typing import Union, Sequence

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.colors import Colormap
from scipy.stats import gaussian_kde

from pyc2mc.core.formula import KendrickBuildingBlock
from .default_plots import plot_classes_circular, scatter_plot, plot_classes
from pyc2mc.plotinator import c2mc_map, c2mc_map_lighter

__all__ = ["PeakListPlotter", "AttributedPeakListPlotter"]


# regex pattern for matplotlib line style
# C0- or r-- or just b or C1 ...
line_fmt_pattern = re.compile(r"^(C\d|[bgrcmykw])(-|--|-\.|:)?$")


class PeakListPlotter:
    """ This class provides the plotting methods for a PeakList object 
    which is not yet attributed. """

    def __init__(self, pl):
        """ Initialize the class from any of the peak list object 

        Args:
            pl (PeakList): A peak list object.

        """
        self.pl = pl

    def plot_spectrum(
        self,
        xlabel: str = "m/z",
        ylabel: str = "Intensity",
        title: str = None,
        rug: bool = False,
        peak_density: bool = False, 
        linefmt: str = 'C3-',
        density_kws: dict = None,
        fig: plt.Figure = None,
        **kwargs
    ):
        """
        Generic matplotlib stem plot to represent intensities as a function of
        m/z.

        Args:
            xlabel (str): label of x axes, default 'm/z'
            ylabel (str): label of y axes, default 'intensity'
            title (str): plot title
            rug (bool): If True, draw a rug plot on top of the spectrum
            peak_density (bool): If True, draw density of peaks on secondary
                axes.
            linefmt (str): line format, default is red and solid line ('C3-')
            density_kws (dict): dict of parameters for the density of peaks.
                Default values are ``{"npts": 1000, "sigma": 0.04}``.
            fig (plt.Figure): A matplotlib figure.
            kwargs: kwargs are passed to the matplotlib.pyplot.stem function

        """

        # figure setup
        if fig is None:
            fig, ax = plt.subplots()
        else:
            fig.clear()
            ax = fig.subplots()

        ax.stem(
            self.pl.mz,
            self.pl.intensity,
            linefmt=linefmt,
            markerfmt=" ", basefmt=" ", **kwargs)

        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)
        ax.set_ylim(bottom=0)
        if title is not None:
            ax.set_title(title)
        else:
            ax.set_title(self.pl.name)

        if rug:
            # plot peak positions as a rug plot
            ymin, ymax = ax.get_ylim()
            d = ymax - ymin

            if m := line_fmt_pattern.match(linefmt.strip()):
                color = m.groups()[0]
            else:
                color = "C3"

            ax.plot(self.pl.mz, np.full(self.pl.mz.shape, ymax),
                    f"{color}|", alpha=.5, markersize=10)
            ax.set_ylim(ymin, ymax + d * 0.05)

        if peak_density:
            # plot the density of peaks as a function of mz using gaussian
            # smoothing
            sigma = 4e-2
            npts = 1000
            if density_kws is None:
                density_kws = dict()
            if "sigma" in density_kws:
                sigma = density_kws["sigma"]
            if "npts" in density_kws:
                npts = density_kws["npts"]

            if m := line_fmt_pattern.match(linefmt.strip()):
                color = m.groups()[0]
            else:
                color = "C3"

            ax2 = ax.twinx()
            kde = gaussian_kde(self.pl.mz, bw_method=sigma)
            mz_val = np.linspace(self.pl.mz.min(), self.pl.mz.max(), npts)
            ax2.plot(mz_val, kde(mz_val), color="white", zorder=2, alpha=.5, lw=4)
            ax2.plot(mz_val, kde(mz_val), f"{color}-", zorder=3)
            ax2.grid(False)
            ax2.set_ylabel("Density of Peaks")

    def plot_kendrick_mass_defect(
        self,
        building_block: Union[KendrickBuildingBlock, str, float],
        normalize: str = "max",
        markersize: float = 5,
        scale_intensity: bool = False,
        max_intensity: float = None,
        cmap: Colormap = c2mc_map,
        fig: plt.Figure = None,
        **kwargs
    ):
        """ Produce a kendrick mass defect plot using the given building
        blocK. The building block can be passed as a float or a molecular
        formula.
        
        Args:
            building_block (KendrickBuildingBlock): Kendrick Building Block
                as formula, a float or a KendrickBuildingBlock, default CH2.
            normalize (str): if "max", default, sets the maximum intensity to 100.
            markersize (float): Default is 50, the size of the points is
                computed as the intensity times markersize.
            scale_intensity (bool): If True, markersize is scale by intensity.
            max_intensity (float): Value of intensity max to normalize the
                plot.
            cmap (Colormap): matplotlib color maps
            fig (plt.Figure): matplotlib figure object
            kwargs: kwargs are passed to the matplotlib.pyplot.scatter function
                or used to setup the plot (such as xlim/ylim).

        """
        # compute kendrick data
        km = self.pl.get_kendrick_masses(building_block)
        int_km = np.round(km)
        kmd = int_km - km

        # normalize intensity
        intensity = self.pl.intensity
        if max_intensity is not None:
            normalize = ""
            intensity /= max_intensity

        if "ylim" in kwargs:
            y_bounds = kwargs.pop("ylim")
        else:
            y_bounds = None

        ax, _ = scatter_plot(
            x=int_km,
            y=kmd,
            c=intensity,
            cmap=cmap,
            y_bounds=y_bounds,
            scale_size=scale_intensity,
            markersize=markersize,
            normalize=normalize,
            colorscale_label="Intensity",
            fig=fig,
            **kwargs,
        )

        if "xlim" in kwargs:
            ax.set_xlim(kwargs.pop("xlim"))

        ax.set_xlabel("Kendrick integer mass")
        ax.set_ylabel("Kendrick mass defect")

    def plot_SN_statistics(
        self,
        logscale: bool = True,
        nbins: int = 30,
        SN_max: float = None,
        weight_by_intensity: bool = True,
        density: bool = True,
        percentiles: Sequence = None,
        fig: plt.Figure = None,
        **kwargs
    ):
        """ Plot an histogram of S/N values in order to give an overview
        of the repartition of the S/N values over the spectra.

        Note: Keep in minds that if the number of bins is small, the
        positions of quantiles are not exactly the same as the cumulative
        sum values.

        Note 2: If density is True, the height if each bin is divided by
        the number of counts and the bin width (look at matplotlib.pyplot.hist
        documentation). If ``logscale`` is True, for high S/N values, the
        density will thus be very small and vanishes.
        
        Args:
            logscale (bool): If True (default), a log scale is used for 
                x axes.
            nbins (int): Number of bins to be used
            SN_max (float): Largest value of S/N to consider
            weight_by_intensity (bool): If True (default), histogram of
                S/N values is weighted by intensity.
            density (bool): If True (default), the histogram is normalized.
            percentiles (list): List of percentile values for which a
                vertical line is plotted. The expected values are in the
                interval [0, 100] and computed using np.percentile.
                
            building_block (KendrickBuildingBlock): Kendrick Building Block
                as formula, a float or a KendrickBuildingBlock, default CH2.
            normalize (str): if "max", default, sets the maximum intensity to 100.
            markersize (float): Default is 50, the size of the points is
                computed as the intensity times markersize.
            scale_intensity (bool): If True, markersize is scale by intensity.
            max_intensity (float): Value of intensity max to normalize the
                plot.

            fig (plt.Figure): matplotlib figure object
            kwargs: kwargs are passed to the matplotlib.pyplot.scatter function
                or used to setup the plot (such as xlim/ylim).

        """
        if not self.pl.SN_avail:
            raise ValueError("S/N is not available in the data.")
        
        # figure setup
        if fig is None:
            fig, ax = plt.subplots()
        else:
            fig.clear()
            ax = fig.subplots()

        # largest SN value
        SN_max = np.nanmax(self.pl.SN) if SN_max is None else SN_max

        # bins
        if logscale:
            bins = np.logspace(0, 4, nbins)
        else:
            bins = np.linspace(0, SN_max, nbins)

        # weight:
        weights = self.pl.intensity if weight_by_intensity else np.ones_like(self.pl.SN)

        # get histogram
        histo, bins, _ = ax.hist(
            self.pl.SN, rwidth=.9, bins=bins, density=density, alpha=.5,
            weights=weights
        )

        if logscale:
            ax.set_xscale("log")

        if weight_by_intensity:
            ax.set_ylabel("# peaks weighted by intensity")
        else:
            ax.set_ylabel("# peaks")
        ax.set_xlabel("S/N")

        ax.axvline(50, color="C3")
        ax.text(50, 0, "S/N = 50 ", ha="center", va="top",
                rotation=90, color="C3")
        ax.axvline(5, color="C3")
        ax.text(5, 0, "S/N = 5 ", ha="center", va="top",
                rotation=90, color="C3")

        ax.set_ylim(bottom=0)
        _, ymax = ax.get_ylim()
        if percentiles is not None:
            q_values = np.percentile(self.pl.SN, percentiles)
            ax.vlines(q_values, ymax=ymax, ymin=0, colors="C2", ls="--")
            for i in range(len(percentiles)):
                ax.text(q_values[i], ymax, f" {percentiles[i]:.1f} %",
                        ha="center", va="bottom",
                        rotation=90, color="C2", fontsize="small")

        # secondary axes
        ax2 = ax.twinx()
        
        steps = np.diff(bins) / 2
        cum_sum = np.cumsum(histo) / histo.sum() * 100
        ax2.plot(bins[:-1] + steps, cum_sum, color="C1")
        ax2.grid(False)
        ax2.set_ylim(0, 102)
        ax2.set_ylabel("Cumulative percentage of peaks", color="C1")

    def __call__(self, **kwargs):
        self.plot_spectrum(**kwargs)


class AttributedPeakListPlotter(PeakListPlotter):
    """ This class represent the plotting methods for a PeakList object.
    """

    def __init__(self, pl):
        """ Initialize the class from any of the peak list object 

        Args:
            pl (PeakList): A peak list or Attributed peak list object

        """
        super().__init__(pl)

        # this save time and avoid to reload dataframe for each plot
        self._monoisotopic = True
        self._df = None
        self._df_chem_classes = None
   
    def plot_spectrum(
        self, 
        xlabel: str = "m/z", 
        ylabel: str = "Intensity",
        title: str = None, 
        rug: bool = False,
        peak_density: bool = False,
        hits_fmt: str = 'C0-',
        nohits_fmt: str = 'C3-',
        hide_nohit: bool = False,
        highlight_nohits: bool = True,
        density_kws: dict = None,
        fig: plt.Figure = None,
        **kwargs
    ):
        """
        Generic matplotlib stem plot to represent intensities as a function of
        m/z. Options allows to distinguish hits and no hits.

        Args:
            xlabel (str): label of x axes, default 'm/z'
            ylabel (str): label of y axes, default 'intensity'
            title (str): plot title
            rug (bool): If True, draw a rug plot on top of the spectrum
            peak_density (bool): If True, draw density of peaks on secondary
                axes.
            hits_fmt (str): line format, of hits, default is ('C0-')
            nohits_fmt (str): line format, of no hits, default is ('C3-')
            hide_nohit (bool): If True, no hits are not displayed (default False)
            highlight_nohits (bool): If True, hits and no hits have different
                colors, default True.
            density_kws (dict): dict of parameters for the density of peaks.
                Default values are ``{"npts": 1000, "sigma": 0.04}``.
            fig (plt.Figure): matplotlib figure object
            kwargs: kwargs are passed to the matplotlib.pyplot.stem function

        """

        if fig is None:
            fig, ax = plt.subplots()
        else:
            fig.clear()
            ax = fig.subplots()

        if not highlight_nohits:
            # set same line format to hit and no hit
            hits_fmt = nohits_fmt

        # plot hits
        hits = self.pl.get_attributed()
        ax.stem(
            hits.mz,
            hits.intensity,
            linefmt=hits_fmt,
            markerfmt=" ", basefmt=" ",
            label="hits",
            **kwargs)
        
        # show no hits ?
        nohits = list()
        if not hide_nohit:
            nohits = self.pl.get_nohits()
            if len(nohits) > 0:
                ax.stem(
                    nohits.mz,
                    nohits.intensity,
                    linefmt=nohits_fmt,
                    markerfmt=" ", basefmt=" ",
                    label="no hits", **kwargs)  

        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)
        ax.set_ylim(bottom=0)
        ax.legend()
        if title is not None:
            ax.set_title(title)
        else:
            ax.set_title(self.pl.name)

        if rug:
            # plot peak positions as rug plot
            ymin, ymax = ax.get_ylim()
            d = ymax - ymin
            if m := line_fmt_pattern.match(hits_fmt.strip()):
                color = m.groups()[0]
            else:
                color = "C0"

            ax.plot(hits.mz, np.full(hits.mz.shape, ymax - d * 0.02), 
                    f"{color}|", alpha=.5, markersize=10)
    
            if not hide_nohit and len(nohits) > 1:
                if m := line_fmt_pattern.match(nohits_fmt.strip()):
                    color = m.groups()[0]
                else:
                    color = "C3"
                ax.plot(nohits.mz, np.full(nohits.mz.shape, ymax + d * 0.02),
                        f"{color}|", alpha=.5, markersize=10)
            ax.set_ylim(ymin, ymax + d * 0.05)

        if peak_density:
            # plot the density of peaks as a function of mz using gaussian
            # smoothing
            sigma = 4e-2
            npts = 1000
            if density_kws is None:
                density_kws = dict()
            if "sigma" in density_kws:
                sigma = density_kws["sigma"]
            if "npts" in density_kws:
                npts = density_kws["npts"]

            ax2 = ax.twinx()
            mz_val = np.linspace(self.pl.mz.min(), self.pl.mz.max(), npts)

            # hits
            if m := line_fmt_pattern.match(hits_fmt.strip()):
                color = m.groups()[0]
            else:
                color = "C0"
            kde = gaussian_kde(hits.mz, bw_method=sigma)
            ax2.plot(mz_val, kde(mz_val), color="white", zorder=2, alpha=.5, lw=4)
            ax2.plot(mz_val, kde(mz_val), f"{color}-", zorder=3)

            # no hits
            if not hide_nohit and len(nohits) > 1:
                if m := line_fmt_pattern.match(nohits_fmt.strip()):
                    color = m.groups()[0]
                else:
                    color = "C0"
                kde_nohits = gaussian_kde(nohits.mz, bw_method=sigma)
                ax2.plot(mz_val, kde_nohits(mz_val), color="white", zorder=2, 
                         alpha=.5, lw=4)
                ax2.plot(mz_val, kde_nohits(mz_val), f"{color}-", zorder=3)

            ax2.grid(False)
            ax2.set_ylabel("Density of Peaks")

    def plot_classes(
        self,
        monoisotopic: bool = True,
        threshold: float = None,
        cum_threshold: float = None,
        exclude: Union[str, Sequence] = None,
        orientation: str = "vertical",
        ax: plt.Axes = None,
        **kwargs
    ):
        """ Return a bar plot of the relative abundance of the classes, in
        percent, of the attributed peak list.

        Args:
            monoisotopic (bool): If True, only monoisotopic classes are included
            threshold (float): minimum value of the relative abundance
                in percent of the classes included.
            cum_treshold (float): cumulative value of the relative abundance
                in percent of the classes included.
            exclude (str, list-like): A sequence of chemical species to be excluded
                from the plot. For example if it is needed to remove '13C' species,
                use: exclude = '13C'.
            orientation (str): 'horizontal' (or 'h') or 'vertical' (or 'v')
                to use barh or bar, respectively.
            ax (plt.Axes): A matplotlib axes
            kwargs: key value arguments of the plot function

        """
        plot_classes(
            data=self.pl,
            monoisotopic=monoisotopic,
            threshold=threshold,
            cum_threshold=cum_threshold,
            exclude=exclude,
            orientation=orientation,
            sort_chem_class=False,
            ax=ax,
            **kwargs
        )

    def plot_error(
        self,
        chem_class: Union[str, list] = None,
        chem_group: Union[str, list] = None,
        monoisotopic: bool = True,
        distribution: bool = True,
        sigma: float = None,
        title: str = None,
        fig: plt.Figure = None,
        **kwargs
    ):
        """ Return a scatter plot of relative error in ppm as a function
        of the m/z values of the peak list.

        Args:
            chem_class (str or list): If not None, only the error of a given class
                is plotted. Else, the whole data are plotted. In order to
                superimpose all data to data of a given class, add 'all'
                to the list.
            chem_group (str or list): If not None, only the error of a given 
                chemical group is plotted. Else, the whole data are plotted. 
                In order to superimpose all data to data of a given class, 
                add 'all' to the list.
            monoisotopic (bool): If True, only monoisotopic classes are
                included, default is True.
            distribution (bool): If True, the error distribution is plotted
                on the right along with the scatter plot.
            sigma (float): width of the gaussian function used to compute
                the kernel density estimate of the error. By default, sigma 
                is computed following the Scott rule.
            title (str): plot title.
            fig (plt.Figure): A matplotlib figure
            kwargs: key value arguments of the plot function

        Returns:
            A matplotlib axes object or a figure object if distribution is
            plotted.
        """
        if fig is None:
            fig = plt.figure()
        else:
            fig.clear()

        if distribution:
            ax, ax2 = fig.subplots(
                ncols=2, sharey=True,
                gridspec_kw=dict(width_ratios=(3, 1), wspace=0)
            )
        else:
            ax = fig.add_subplot(1, 1, 1)

        # scatter plot of error against m/z
        if self._df is None:
            self._df = self.pl.to_dataframe(attributed_only=True, full_data=True)

        i = 1
        if chem_class:
            if isinstance(chem_class, str):
                chem_class = [chem_class]

            col = "chem_class_mono" if monoisotopic else "chem_class"
            chem_classes = self._df[col].unique()

            for aclass in chem_class:
                if aclass in chem_classes:
                    class_df = self._df[self._df[col] == aclass]

                    ax.scatter(class_df["mz"], class_df["error_ppm"],
                               c=f"C{i:d}", marker='o', zorder=i + 10,
                               alpha=0.75, label=aclass, **kwargs)
                    i += 1
                elif aclass == "all":
                    ax.scatter(self.pl.mz, self.pl.error_in_ppm, c="C0",
                               zorder=1, marker='o', alpha=0.5, label="all",
                               **kwargs)
                else:
                    raise ValueError(f"The chemical class '{aclass}'"
                                     f" is not in {chem_classes}")

            ax.legend()
            if title:
                ax.set_title(title)

        if chem_group:
            if isinstance(chem_group, str):
                chem_group = [chem_group]

            col = "chem_group_mono" if monoisotopic else "chem_group"
            chem_groups = self._df[col].unique()
            
            for agroup in chem_group:
                if agroup in chem_groups:
                    group_df = self._df[self._df[col] == agroup]

                    ax.scatter(group_df["mz"], group_df["error_ppm"],
                               c=f"C{i:d}", marker='o', zorder=i + 1,
                               alpha=0.75, label=agroup, **kwargs)
                    i += 1
                elif agroup == "all":
                    ax.scatter(self.pl.mz, self.pl.error_in_ppm, c="C0",
                               zorder=1, marker='o', alpha=0.5, label="all",
                               **kwargs)
                else:
                    raise ValueError(f"The chemical group '{agroup}'"
                                     f" is not in {chem_groups}")

            ax.legend()
            if title:
                ax.set_title(title)

        if chem_class is None and chem_group is None:
            ax.scatter(self.pl.mz, self.pl.error_in_ppm,
                       c="C0", marker='o', alpha=0.75, **kwargs)

        # horizontal line
        ax.axhline(color="#333333")

        # plot error distribution on ax2
        if distribution:
            ax2.axhline(color="#333333")

            # width of gaussian functions used in the KDE
            if sigma:
                bw_method = sigma
            else:
                bw_method = "scott"

            # x value on which KDE is computed
            x_error = np.linspace(
                self._df['error_ppm'].values.min(),
                self._df['error_ppm'].values.max(), 200)

            i = 1
            if chem_class:
                col = "chem_class_mono" if monoisotopic else "chem_class"

                for aclass in chem_class:
                    if aclass == "all":
                        error_kde_all = gaussian_kde(self._df['error_ppm'].values)
                        ax2.plot(error_kde_all(x_error), x_error,
                                 color="C0", label="all")
                    else:
                        class_df = self._df[self._df[col] == aclass]
                        if len(class_df) > 2:
                            # if len < 2 impossible to compute KDE
                            error_kde = gaussian_kde(
                                class_df["error_ppm"], bw_method)

                            ax2.plot(error_kde(x_error), x_error,
                                     color=f"C{i}", label=aclass)
                        i += 1

            if chem_group:
                col = "chem_group_mono" if monoisotopic else "chem_group"

                for agroup in chem_group:
                    if agroup == "all":
                        error_kde_all = gaussian_kde(self._df['error_ppm'].values)
                        ax2.plot(error_kde_all(x_error), x_error,
                                 color="C0", label="all")
                    else:
                        class_df = self._df[self._df[col] == agroup]
                        if len(class_df) > 2:
                            # if len < 2 impossible to compute KDE
                            error_kde = gaussian_kde(
                                class_df["error_ppm"], bw_method)

                            ax2.plot(error_kde(x_error), x_error,
                                     color=f"C{i}", label=agroup)
                        i += 1

            if chem_class is None and chem_group is None:
                error_kde_all = gaussian_kde(self._df['error_ppm'].values, bw_method)
                ax2.plot(error_kde_all(x_error), x_error, color="C0")

            ax2.set_xlim(left=0)
            ax2.set_xticks([])
            ax2.set_xlabel("distribution")

            if title:
                fig.suptitle(title)

        ax.set_xlabel('m/z (Da)')
        ax.set_ylabel('error (ppm)')

    def plot_dbe(
        self,
        chem_class: str = None,
        chem_group: str = None,
        monoisotopic: bool = True,
        scale_intensity: bool = False,
        non_integer_dbe: bool = False,
        integer_dbe: bool = False,
        plot_type: str = "points",
        vmin: float = 0,
        vmax: float = None,
        marker: str = "s", 
        markersize: float = 20,
        sigma: Union[float, str] = "scott", 
        resolution: float = 0.5,
        fig: plt.Figure = None, 
        **kwargs
    ):
        """
        Returns a matplotlib axes representing the abundance of the chemical
        class on a 2D histogram in function of DBE and carbon number. Two plot
        types are available. A plot type `'points'` is scatter plot of all
        the molecules superimposed. A plot type `'KDE'` provide an gaussian
        smoothing of the plot.

        Args:
            chem_class (str): chemical class to plot.
            chem_group (str): chemical group to plot.
            monoisotopic (bool): If True, only monoisotopic classes are
                included, default is True.
            scale_intensity (bool): If True, markersize is scale by intensity.
            non_integer_dbe (bool): eliminate the non-integer DBE values.
            integer_dbe (bool): eliminate the integer DBE values.
            plot_type (str): 'points', or 'KDE'.
            vmin (floor): minimum abundance value in the color scale.
            vmax (floor): maximum abundance value in the color scale.
            marker (str): matplotlib marker type, is bullet by default.
            markersize (float): marker size.
            sigma (float or str): width of the gaussian function used to compute
                the kernel density estimate in case of a KDE plot type. 
                By default, sigma is computed following the Scott rule.
            resolution (float): define the resolution of the layer plot,
                the higher the value, the better the resolution but it could
                significantly increase the calculation time.
            fig (plt.Figure): A matplotlib figure
            kwargs: key value arguments of the plot function

        """

        if plot_type not in ['points', 'KDE']:
            raise ValueError(
                f"not type '{plot_type}' in plot_dbe, choose: 'points' or 'KDE'.")

        if fig is None:
            fig, ax = plt.subplots()
        else:
            fig.clear()
            ax = fig.add_subplot(1, 1, 1)

        if self._df is None:
            self._df = self.pl.to_dataframe(attributed_only=True, full_data=True)
        if "DBE_even" not in self._df:
            double_DBE = (self._df["DBE"].values * 2).astype(np.int16)
            self._df["double_DBE"] = double_DBE  # this avoid floating error
            self._df["integer_DBE"] = (double_DBE % 2) == 0

        # filter chem classes
        if chem_class:
            if monoisotopic:
                col = "chem_class_mono"
            else:
                col = "chem_class"
            chem_classes = self._df[col].unique()
            if chem_class not in chem_classes:
                raise ValueError(
                    f"The chemical class '{chem_class}' is not"
                    f" in {chem_classes}.")
            df = self._df[self._df[col].values == chem_class].copy()
        elif chem_group:
            if monoisotopic:
                col = "chem_group_mono"
            else:
                col = "chem_group"
            chem_groups = self._df[col].unique()
            if chem_group not in chem_groups:
                raise ValueError(
                    f"The chemical group '{chem_group}' is not"
                    f" in {chem_groups}."
                )
            df = self._df[self._df[col].values == chem_group].copy()
        else:
            df = self._df.copy()

        # filter integer or non integer DBE
        if integer_dbe:
            df_filt = df[df.integer_DBE]
        elif non_integer_dbe:
            df_filt = df[~df.integer_DBE]
        else:
            df_filt = df

        # group and sum up intensity over DBE and #C values
        data = df_filt.groupby(["C", "double_DBE"]).agg({"intensity": "sum"})
        data.reset_index(inplace=True)
        
        dbe = data['double_DBE'].values / 2
        CN = data['C'].values
        intensity = data['intensity'].values

        if plot_type == 'points':
            if vmax is None:
                vmax = intensity.max()

            scatter_plot(x=CN, y=dbe, c=intensity,
                         scale_size=scale_intensity, scale_alpha=True,
                         colorscale_label="Abundance", marker=marker,
                         markersize=markersize, normalize="",
                         vmin=vmin, vmax=vmax, edgecolors="none",
                         fig=fig, **kwargs)

        elif plot_type == 'KDE':
            x = np.arange(CN.min() * 0.85, CN.max() * 1.1, resolution)
            y = np.arange(dbe.min() - 1, dbe.max() + 1, resolution)
            X, Y = np.meshgrid(x, y)
            extent = np.min(x), np.max(x), np.min(y), np.max(y)
            positions = np.vstack([X.ravel(), Y.ravel()])
            values = np.vstack([CN, dbe])

            kernel = gaussian_kde(values, bw_method=sigma, weights=intensity)
            Z = np.reshape(kernel(positions).T, X.shape) * intensity.sum()

            if vmax is None:
                vmax = Z.max()

            dbe_plot = ax.imshow(
                Z, cmap=c2mc_map_lighter, extent=extent, alpha=0.9, 
                vmin=vmin, vmax=vmax,
                origin='lower', aspect='auto', **kwargs
            )

            # add a color bar
            fig.colorbar(dbe_plot, label='Abundance', ax=ax)

        fig.axes[0].grid(False)
        fig.axes[0].set_xlabel('C#')
        fig.axes[0].set_ylabel('DBE')

    def plot_attribution_stats(
        self,
        xaxis: str = "intensity",
        fig: plt.Figure = None, 
        **kwargs
    ):
        """
        Returns a matplotlib axes with an horizontal bar plot representing
        the statistic of the attribution and the alogithm implemented.

        Args:
            xaxis (str): expected values are 'intensity' or 'peaks'. Default
                is 'intensity' and the percentage of attributed intensity
                is used as x axis. If 'peaks' the number of peaks is used
                as x axis.
            fig (plt.Figure): A matplotlib figure
            kwargs: key value arguments of the plot function
    
        """
        if fig is None:
            fig, ax = plt.subplots()
        else:
            fig.clear()
            ax = fig.add_subplot(1, 1, 1)

        # get the data frame with stats
        dfm = self.pl.get_attribution_stats(kind="dataframe")
        dfm.drop(index=["total"], inplace=True)
        dfm.index = [val.replace("_", " ") for val in dfm.index.values]

        # plot production
        if xaxis == "peaks":
            ax = dfm.plot.barh(y="# peaks", grid=False, legend=False,
                               ax=ax, **kwargs)
            ax.set_xlabel("# peaks")

        elif xaxis == "intensity":
            ax = dfm.plot.barh(y="percentage", grid=False, legend=False,
                               ax=ax, **kwargs)
            ax.set_xlabel(r"% intensity")
        else:
            raise ValueError(
                f"xaxis is 'intensity' or 'peaks' but you set '{xaxis}'."
            )
        ax.set_ylabel("")
        ax.set_frame_on(False)
        ax.tick_params("both", length=0)
        ax.minorticks_off()
        ax.vlines(0, -1, len(dfm), color="C7", linewidth=4)
        # ax.text(0, len(dfm) - .5, "Algorithm", ha="center", va="bottom")

        for i, bar in enumerate(ax.patches):
            if i == 0:
                bar.set_color("C2")
            if i == 1:
                bar.set_color("C3")

        if xaxis == "peaks":
            n_max = dfm["# peaks"].max()
            ax.text(n_max * 1.1, len(dfm) - .5, r"% intenstiy", va="center",
                    ha="right", color="black", fontsize=14)
        else:
            n_max = dfm["percentage"].max()
            ax.text(n_max * 1.1, len(dfm) - .5, "# peaks", va="center",
                    ha="right", color="black", fontsize=14)
            
        for idx, row in dfm.reset_index().iterrows():
            if idx == 0:
                color = "C2"
            elif idx == 1:
                color = "C3"
            else:
                color = "black"
            if xaxis == "peaks":
                value = f"{row.percentage:.1f} %"
            else:
                value = f"{row['# peaks']:5d}"
            ax.text(n_max * 1.1, idx, value, va="center", ha="right",
                    color=color, fontsize=14)

    def plot_van_krevelen(
        self,
        x_ratio: str = "O / C",
        y_ratio: str = "H / C",
        markersize: float = 50,
        max_intensity: float = None,
        vmin: float = None,
        vmax: float = None,
        fig: plt.Figure = None,
        **kwargs
    ):
        """ Produce a Van Krevelen plot, corresponding to a scatter plot with
        two element ratios on x and y axes.
        
        Args:
            x_ratio (str): Element ration on x axis, such as ``"el1 / el2"``.
                Default is ``"O / C"``.
            y_ratio (str): Element ration on y axis, such as ``"el1 / el2"``.
                Default is ``"H / C"``.
            markersize (float): Default is 50, the size of the points is
                computed as the intensity times markersize.
            max_intensity (float): Value of intensity max to normalize the
                plot.
            vmin (float): min intensity for the color scale.
            vmax (float): max intensity for the color scale.
            fig (plt.Figure): matplotlib figure
            kwargs: kwargs are passed to the matplotlib.pyplot.scatter function
                or used to setup the plot (such as xlim/ylim).

        """

        if fig is None:
            fig, ax = plt.subplots()
        else:
            fig.clear()
            ax = fig.add_subplot(1, 1, 1)

        x_species = [sp.strip() for sp in x_ratio.split("/")]
        y_species = [sp.strip() for sp in y_ratio.split("/")]

        elemental_composition = self.pl.elemental_composition
        for sp in x_species + y_species:
            if sp not in elemental_composition:
                raise ValueError(
                    f"'{sp}' is not in the elemental composition: {elemental_composition}.")

        if self._df is None:
            self._df = self.pl.to_dataframe(attributed_only=True, full_data=True)

        # select rows that contain the species
        species = set(x_species + y_species)
        mask = [self._df[sp].values >= 1 for sp in species]
        mask = np.all(mask, axis=0)
        df = self._df[mask]
        x_values = df.loc[:, x_species[0]].values / df.loc[:, x_species[1]].values
        y_values = df.loc[:, y_species[0]].values / df.loc[:, y_species[1]].values
        
        # normalize intensity
        intensity = df.intensity
        if max_intensity:
            intensity /= max_intensity
        else:
            intensity /= np.nanmax(intensity)
        vmin = np.nanmin(intensity) if vmin is None else vmin
        vmax = np.nanmax(intensity) if vmax is None else vmax

        if "ylim" in kwargs:
            ax.set_ylim(kwargs.pop("ylim"))

        if "xlim" in kwargs:
            ax.set_xlim(kwargs.pop("xlim"))

        scatter = ax.scatter(
            x=x_values, y=y_values, s=markersize * intensity,
            vmin=vmin, vmax=vmax,
            c=intensity, cmap=c2mc_map, **kwargs)
        
        fig.colorbar(scatter, label='Normalized Intensity', ax=ax)

        ax.set_xlabel(" / ".join(x_species))
        ax.set_ylabel(" / ".join(y_species))

    def plot_sunburst_classes(self, threshold: float = 0.1, export: bool = False):
        """ Plot the chemical classes using a sunburst plot from plotly.
        You need to install plotly to use it.
        TODO: If more plot are made from plotly add plotly in requirements

        Args:
            threshold (float): minimum value of the relative abundance of the
                classes included.
            export (bool): if True, export HTML

        """
        try:
            import plotly.express as px
        except ModuleNotFoundError:
            print("Please, install plotly express to plot sunburst.")
            print("    pip install plotly")
            print("    or")
            print("    conda install plotly")
            return ""
        
        if threshold < 0.04:
            print("WARNING: you've asked for very small wedge and it may not work.")

        if self._df is None:
            self._df = self.pl.to_dataframe(attributed_only=True, full_data=True)

        # set up the data frame
        class_data = self.pl.get_classes(monoisotopic=True, 
                                         threshold=threshold)
        intensity = "Relative abundance (%)"

        # get the chem groups to the central circle
        groups = class_data.groupby("Chem. Group").agg({intensity: "sum"}).reset_index()

        # header for center
        head = pd.DataFrame({
            "Chem. Group": [""] + len(groups) * ["Sample"],
            "Chem. Class": ["Sample"] + list(groups["Chem. Group"].values),
            intensity: [class_data[intensity].sum()] + list(groups[intensity].values)
        })

        # final data frame
        class_data = pd.concat([head, class_data])

        # make the figure
        fig = px.sunburst(
            class_data,
            names='Chem. Class',
            parents='Chem. Group',
            values=intensity,
            branchvalues="total",
        )
        fig.update_traces(
            hovertemplate="<br>".join([
                "Chem. Class: %{label}",
                "Chem. Group: %{parent}",
                "Intensity: %{value:.2f}%",
            ])
        )
        fig.update_layout(
            height=800,
            width=800,
            title=dict(text="Chemical class"),
            plot_bgcolor='white',
        )

        if export:
            fig.write_html(file=self.pl.name + "_sunburst.html")
        else:
            fig.show()

    def plot_classes_circular(
        self,
        monoisotopic: bool = True,
        threshold: float = None,
        cum_threshold: float = None,
        fontsize: float = 10,
        padding: int = 1,
        group_order: list = None,
        group_colors: list = None,
        fig: plt.Figure = None,
        **kwargs
    ):
        """ Produce a circular bar plot that displays chemical classes 
        grouped by chemical group.
        
        This plot is inspired from this page:
        https://python-graph-gallery.com/circular-barplot-with-groups/
        
        Args:
            monoisotopic (bool): If True, only monoisotopic classes are
                included, default is True.
            threshold (float): minimum value of the relative abundance of the
                classes included.
            cum_treshold (float): cumulative value of the relative abundance
                in percent of the classes included.
            fontsize (float): font size for labels
            padding (int): space between groups
            group_order (list): order of chemical group for the plot.
            group_colors (list): Colors for each chemical group
            fig (plt.Figure): matplotlib figure
            kwargs: kwargs are passed to the matplotlib.pyplot.scatter function
                or used to setup the plot (such as xlim/ylim).

        """
        plot_classes_circular(
            data=self.pl,
            monoisotopic=monoisotopic,
            threshold=threshold,
            cum_threshold=cum_threshold,
            fontsize=fontsize,
            padding=padding,
            group_order=group_order,
            group_colors=group_colors,
            fig=fig,
            **kwargs
        )
