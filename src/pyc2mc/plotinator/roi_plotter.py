#!/usr/bin/env python
# coding: utf-8

"""
This module contains classes to display plots of Region Of Interest (ROI)
associated to the processing of 2D data.
"""

import matplotlib.pyplot as plt
from matplotlib.colors import Colormap

from .default_plots import scatter_plot, c2mc_map

__all__ = ["ROIPlotter", "ROIListPlotter"]


class ROIPlotter:
    """
    This class implements the methods to obtain plots from a ROI.
    """

    def __init__(
        self,
        ROI,
        dt: float = None,
        t0: float = None

    ):
        """ Initialize the class from a ROI.

        Args:
            ROI (pyc2mc.core.peak.ROI): a ROI object.
            dt (float): time step between each scan
            t0 (float): retention time of the first scan

        """
        self.ROI = ROI
        self.dt = dt if dt is not None else 1.0
        self.t0 = t0 if t0 is not None else 0.0

    def plot_ROI(
        self,
        dt: float = None,
        t0: float = None,
        xlabel: str = "Scans",
        fig: plt.Figure = None,
        **kwargs
    ):
        """ Plot the Extractive Ion Chromatogram and the Feature.

        Args:
            dt (float): time step between each scan.
            t0 (float): time of the first scan.
            xlabel (str): Label of the x axes.
            fig (plt.Figure): A matplotlib figure.

        """
        if fig is None:
            fig = plt.figure()
        else:
            fig.clear()
    
        axes = fig.subplots(
            nrows=2, ncols=2,
            gridspec_kw=dict(
                hspace=0, width_ratios=[1, 0.03], height_ratios=[1, 1],
                wspace=0.1)
        )

        self.plot_feature(ax=axes[1, 0], dt=dt, t0=t0, xlabel=xlabel,
                          cax=axes[1, 1])
        self.plot_EIC(ax=axes[0, 0], dt=dt, t0=t0)

        axes[0, 1].axis("off")

    def plot_EIC(
        self,
        dt: float = None,
        t0: float = None,
        xlabel: str = "Scans",
        ax: plt.Axes = None,
        **kwargs
    ):
        """
        Plot the Extractive Ion Chromatogram only.

        Args:
            dt (float): time step between each scan.
            t0 (float): time of the first scan.
            ax (matplotlib.pyplot.subplot): an axis from subplots.

        """
        if ax is None:
            ax = plt.subplot(1, 1, 1)

        dt = dt if dt is not None else self.dt
        t0 = t0 if t0 is not None else self.t0
        x = t0 + dt * self.ROI.scans

        ax.plot(x, self.ROI.intensity, "C3.:")
        ax.set_xlabel(xlabel)
        ax.set_ylabel('Intensity')

        return ax

    def plot_feature(
        self,
        dt: float = None,
        t0: float = None,
        xlabel: str = "Scans",
        ax: plt.Axes = None,
        cax: plt.Axes = None,
        **kwargs
    ):
        """
        Plot the Feature: (m/z) as a function of time as a scatter
        plot with intensity as color scale.

        Args:
            dt (float): time step between each scan.
            t0 (float): time of the first scan.
            xlabel (str): Label for the x axies
            ax (plt.Axes): Axes for the main plot
            cax (plt.Axes): Axes for the  color bar

        """
        if ax is None:
            ax = plt.subplot(1, 1, 1)

        dt = dt if dt is not None else self.dt
        t0 = t0 if t0 is not None else self.t0
        x = t0 + dt * self.ROI.scans

        mz = self.ROI.mz
        c = self.ROI.intensity
        mz_min = min(mz) - 3 * (max(mz) - min(mz))
        mz_max = max(mz) + 3 * (max(mz) - min(mz))

        im = ax.scatter(x, mz, c=c, cmap='viridis', **kwargs)
        ax.set_xlabel(xlabel)
        ax.set_ylabel('m/z (Da)')

        if cax is None:
            plt.colorbar(im, ax=ax, label='Intensity')
        else:
            plt.colorbar(im, ax=ax, cax=cax, label='Intensity')

        ax.yaxis.set_major_formatter('{x:,.4f}')
        ax.set_ylim(mz_min, mz_max)

        return ax

    def __call__(self, *args, **kwargs):
        return self.plot_ROI(*args, **kwargs)


class ROIListPlotter:
    """
    This class implements the methods to perform plots of a list of ROI.
    """

    def __init__(self, roi_list):
        """
        Initialize the ROIs plotter class.

        Args:
            ROIs (pyc2mc.core.peak.ROIs): a ROIs object.

        """
        self._roi_list = roi_list

    def plot_ROIs(
        self,
        normalize: str = "",
        markersize: float = 50,
        dt: float = 1,
        t0: float = 0.,
        xlabel: str = "Scans",
        mz_bounds: tuple = None,
        scale_intensity: bool = False,
        cmap: Colormap = c2mc_map,
        fig: plt.Figure = None,
        **kwargs
    ):
        """
        Return an axis of a scatter plot.

        Args:
            normalize (bool): if True, default, sets the maximum intensity to 100.
            markersize (float): Default is 50, the size of the points is
                computed as the intensity times markersize.
            dt (float): time step between each scan.
            t0 (float): first value on the time axis.
            xlabel (str): Label of the x axes.
            mz_bounds (tuple): Min and max mz values to delimit the plot
            scale_intensity (bool): If True, markersize is scale by intensity.
            cmap (Colormap): Matplotlib colormap
            fig (plt.Figure): A matplotlib figure
            kwargs as keyword arguments of the scatter function.

        """
        ax, _ = scatter_plot(
            x=t0 + dt * self._roi_list._ROI_xdata,
            y=self._roi_list._ROI_ydata,
            c=self._roi_list._ROI_cdata,
            cmap=cmap,
            y_bounds=mz_bounds,
            scale_size=scale_intensity,
            markersize=markersize,
            normalize=normalize,
            colorscale_label="Intensity",
            fig=fig,
            **kwargs,
        )

        ax.grid(False)
        ax.set_ylabel("m/z (Da)")
        ax.set_xlabel(xlabel)

    def plot_TIC(
        self,
        normalize: str = "max",
        dt: float = None,
        t0: float = None,
        xlabel: str = "Scans",
        ax: plt.Axes = None,
        smoothing: bool = False,
        smooth_kws: dict = None,
        **kwargs
    ):
        """
        Performs the total ion chromatogram of the ROI list.

        Args:
            normalize (str): 'max', 'integral' or '', for normalization
            dt (float): time step between each scan.
            t0 (float): first value on the time axis.
            xlabel (str): Label of the x axes.
            ax (matplotlib.pyplot.subplot.ax): A matplotlib ax from subplot.
            smoothing (bool): if True, add a smoothing.
            smooth_kws (dict): smoothing options.
            kwargs as available in ax.plot().

        Returns:
            A figure, and an ax of matplotlib.pyplot.subplot containing the
            Total Ion Current Chromatogram of the ROI list.
        """
        if ax is None:
            ax = plt.subplot(1, 1, 1)

        smooth_kws = {} if smooth_kws is None else smooth_kws
        label = "smoothed TIC" if smoothing else "TIC"

        scans, tic = self._roi_list.get_total_ion_chromatogram(
            normalize=normalize, smoothing=smoothing, smooth_kws=smooth_kws)
        t0 = t0 if t0 is not None else self._roi_list.t0
        dt = dt if dt is not None else self._roi_list.dt
        tps = t0 + scans * dt

        ax.plot(tps, tic, "C3-", label=label, **kwargs)

        ax.set_xlabel(xlabel)
        ylabel = "Total Intensity"
        if normalize == "max":
            ylabel = "Normalized Total Intensity (max)"
        elif normalize == "integral":
            ylabel = "Normalized Total Intensity (sum)"
        ax.set_ylabel(ylabel)

        ax.legend(loc="best")

    def __call__(self, **kwargs):
        return self.plot_ROIs(**kwargs)
