#!/usr/bin/env python

"""
This module provides a model to validate parameters and delimit data.
"""

from typing import Tuple, Union

from pydantic import (BaseModel, NonNegativeInt, field_validator, PositiveFloat,
                      PositiveInt)

from pyc2mc.controls import ProcessingControls

__all__ = ["DelimitControls", "ScanSlice"]


class ScanSlice(BaseModel):
    """ This model is to validates slice parameters and return a slice 
    object. """
    start: NonNegativeInt = None
    stop: PositiveInt = None
    step: PositiveInt = None

    def get_slice(self):
        """ return a slice object """
        params = self.model_dump(exclude_none=True)
        if len(params) == 0:
            raise ValueError(f"Impossible to make a slice with {params}")

        start = params.get("start")
        stop = params.get("stop")
        step = params.get("step")
        if step is None and start is None and stop is not None:
            return slice(stop)
        elif step is None and start is not None and stop is not None:
            return slice(start, stop)
        elif all([i is not None for i in [start, stop, step]]):
            return slice(start, stop, step)
        else:
            raise ValueError(f"Impossible to make a slice with {params}")


class DelimitControls(ProcessingControls):
    """ parameters in order to delimit data. 

    Attributes:
        mz_bounds, intensity_bounds, SN_bounds: tuples to delimite
            mz, intensity and S/N, respectively.
        scan_slice: For Time dependent data, this is used to delimit
            the scans to be considered.
    """

    mz_bounds: Tuple[PositiveFloat, PositiveFloat] = None
    intensity_bounds: Tuple[PositiveFloat, PositiveFloat] = None
    SN_bounds: Tuple[PositiveFloat, PositiveFloat] = None
    scan_slice: Union[ScanSlice, dict] = None

    @field_validator("mz_bounds", "intensity_bounds", "SN_bounds")
    @classmethod
    def bounds_match(cls, field_value):
        """ Check if the lower bound is smaller than the upper bound. """
        if field_value[0] > field_value[1]:
            raise ValueError(
                f"{field_value}: lower bound is greater than upper bound.")
        return field_value

    @field_validator("scan_slice")
    @classmethod
    def check_slice(cls, field_value):
        """ make dict a slice """
        if isinstance(field_value, dict):
            return ScanSlice(**field_value)
        else:
            return field_value

    def get_params(self, scan_slice: bool = False) -> dict:
        """ Get the parameter dict. By default scan_slice are not returned.
        They can be included for a full output of the parameters.

        Args:
            scan_slice (bool): If True include scan slice parameters if
                they exist.
        """
        if scan_slice:
            return self.model_dump(exclude_none=True, exclude={"scan_slice"})
        else:
            return self.model_dump(exclude_none=True)
