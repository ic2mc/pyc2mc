# coding: utf-8

"""
This module provides models that aims to store and validate data read from
configuration files in toml syntax.
"""


from pydantic import BaseModel
from abc import ABCMeta, abstractmethod


class ProcessingControls(BaseModel, metaclass=ABCMeta):
    """ This class is a base class to implement controls validation
    for PyC2MC workflow """

    @abstractmethod
    def get_params(self) -> dict:
        """ Get the parameters as a dict """
        pass
