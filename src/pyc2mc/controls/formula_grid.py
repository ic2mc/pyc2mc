# coding: utf-8

""" This module provides a model class which allows to validate
parameters of a formula grid. """

from collections.abc import Sequence
from typing import Tuple, Dict, Annotated, Union, IO

import tomlkit
try:
    # python 3.11
    import tomllib
except ModuleNotFoundError:
    # python < 3.11
    import tomli as tomllib

from pyc2mc.core.isotopes import Isotope
# from pyc2mc.core.formula_grid import FormulaGrid
from pydantic import (BaseModel, PositiveFloat, NonNegativeFloat,
                      field_validator, PositiveInt, NonNegativeInt,
                      StringConstraints)


# string constraint for element validation
CONELM = Annotated[str, StringConstraints(strip_whitespace=True, pattern=r"^[A-Z][a-z]?$")]
CONISO = Annotated[str, StringConstraints(strip_whitespace=True, pattern=r"^\d{1,3}[A-Z][a-z]?$")]


class FormulaGridControls(BaseModel):
    """ This class contains the input parameters in order to set up a
    formula grid.

    Args:
        polarity (str): must be ``"positive"`` or ``"negative"``
        elements (list): a list of the InputElements object
        limits (list): a list of tuples with min, max boundaries of each
            element.
        H_C_bounds (list): [min, max] lowest and highest values of the
            hydrogen to carbon ratio.
        dbe_bounds (list): [min, max] lowest and highest values of the
            double bon equivalent limits.
        mass_bounds (list): [min, max] lowest and highest values of the m/z
            range.
        element_ratios (dict): A dict of element pairs as keys such as 
            ``O_C`` for the O/C ration and tuples of min an max values 
            as values.
        isotopes (dict): dict of isotopes to be included in the grid. The
            keys of the dict are the isotope symbol, such as ``13C`` or ``18O``
            and the values are the maximum number of isotopes to be included
            in the grid.
        combine_isotopes (list of dict): isotopes combination to be included in
            the grid. If defined, the formula grid will create an extra grid
            that contains combination of two isotopes. For each dict in the
            list, the keys are the isotope symbols and the values, the number
            of isotopes to be included in the grid. For example, for at most
            two 10B and one 13C: ``{10B: 1, 13C: 1}``
        lewis_filter (bool): If True apply the Lewis filtering, default
                False.
        name (str): Name of the formula grid


    """

    polarity: int
    elements: Sequence[CONELM]
    limits: Sequence[Tuple[NonNegativeInt, PositiveInt]]
    H_C_bounds: Tuple[NonNegativeFloat, PositiveFloat] = (0.2, 3)
    dbe_bounds: Tuple[NonNegativeInt, PositiveInt] = (0, 50)
    mass_bounds: Tuple[NonNegativeFloat, PositiveFloat] = (0., 2000.)
    element_ratios: Dict[str, Tuple[NonNegativeFloat, PositiveFloat]] = None
    isotopes: Dict[CONISO, NonNegativeInt] = None
    combine_isotopes: Sequence[Dict[CONISO, NonNegativeInt]] = None
    lewis_filter: bool = False
    name: str = "FormulaGrid_0"

    @field_validator("mass_bounds", "H_C_bounds", "dbe_bounds")
    @classmethod
    def bounds_match(cls, field_value):
        """ Check if the lower bound is smaller than the upper bound. """
        if field_value[0] > field_value[1]:
            raise ValueError(f"{field_value}: lower bound is greater than upper bound.")
        return field_value
    
    @field_validator("limits")
    @classmethod
    def limits_validation(cls, field_value, values):
        """ Check if the lower bound is smaller than the upper bound in limits
        and if the length of limits is consistent with the length of elements.
        """
        n_limit = len(field_value)
        n_element = len(values.data["elements"])
        if n_limit != n_element:
            raise ValueError(
                "You must provide the limits for each elements.\n"
                f"elements: {values.data['elements']}\n"
                f"limits: {field_value}\n"
            )

        if any([limit[0] > limit[1] for limit in field_value]):
            raise ValueError(f"{field_value}: lower limit is greater than upper limit.")
        return field_value
    
    @field_validator("polarity")
    @classmethod
    def polarity_check(cls, field_value):
        """ Check polarity is not zero """
        try:
            field_value = int(field_value)
            if field_value == 0:
                raise ValueError(f"Polarity must not be zero, polarity: '{field_value}'")
        except ValueError as error:
            print(error)
            raise ValueError(f"Polarity must be an integer, polarity: '{field_value}'")

        return field_value

    @field_validator("element_ratios")
    @classmethod
    def element_ratios_validation(cls, field_value, values):
        """ Check if the keys of element ratios have the good shape.
        """
        elements = values.data["elements"]
        for key in field_value:
            elm_ratio = key.split("_")
            if len(elm_ratio) != 2:
                raise ValueError(
                    "A constraint on the ratio X over Y must be 'X_Y' but"
                    f" the provided key is {key}.")
            if not all([el in elements for el in elm_ratio]):
                raise ValueError(
                    f"A constraint on elements {key} cannot be applied as "
                    "at least one of the elements is not in the formula grid"
                    f"\nelements: {elements}.\n"
                )

        return field_value

    def to_dict(self):
        """ Return a dictionary object compatible with Formula Grid constructor.
        If you use the ``model_dump`` method, you must create FormulaGrid
        from the ``from_dict`` method.
        """

        d = self.model_dump()

        # check if isotopes are symbols or tuples
        if self.isotopes is not None:
            isotopes = {
                Isotope.from_string(iso): n for iso, n in self.isotopes.items()
            }
            isotopes = {(iso.Z, iso.A): n for iso, n in isotopes.items()}
            d["isotopes"] = isotopes

        # check if combinations are symbols or tuples
        if self.combine_isotopes is not None:
            combinations = [
                {Isotope.from_string(iso): n for iso, n in combination.items()}
                for combination in self.combine_isotopes
            ]
            combinations = [
                {(iso.Z, iso.A): n for iso, n in combination.items()}
                for combination in combinations
            ]
            d["combine_isotopes"] = combinations

        # change element ratio constraints:
        if self.element_ratios is not None:
            d["element_ratios"] = {
                tuple(k.split("_")): v for k, v in self.element_ratios.items()
            }

        return d

    @classmethod
    def from_toml(cls, filename: Union[str, IO] = None):
        """ Set up a formula grid control, reading a toml file or a string
        containing the formula grid parameters. If a table of formula
        grid exists in the toml file, only the first one is read.

        Args:
            filename (str or ioStream): file name or an open file object.
        
        """
        try:
            with open(filename, "rb") as fi:
                d = tomllib.load(fi)
        except TypeError:
            d = tomllib.load(filename)
        
        if "formula_grid" not in d:
            raise KeyError(f"'formula_grid' not in toml:\n{list(d.keys())}")

        if isinstance(d["formula_grid"], list):
            if len(d["formula_grid"]) > 1:
                print("Read first formula grid in file")
            d = d["formula_grid"][0]
        else:
            d = d["formula_grid"]

        return cls(**d)

    def to_toml(self, filename: Union[str, IO] = None):
        """ Export the formula grid to a toml configuration file.

        Args:
            filename (str or ioStream): file name or an open file object.
        
        """
        d = self.model_dump()
        document = dict(formula_grid=[d])
        if filename is None:
            return tomlkit.dumps(document)
        else:
            try:
                with open(filename, "w", encoding="utf-8") as fo:
                    tomlkit.dump(document, fo)
            except TypeError:
                tomlkit.dump(document, filename)

    # GSV: TODO or not TODO...
    # this may cause circular import error. Maybe, using the dict method
    # of BaseModel, in combination of from_dict of formulagrid is enough
    # and this function is not necessary
    # We can keep this only for controls and validations
    # def get_formula_grid(self):
    #     """ Return a pyc2mc.core.formula_grid.FormulaGrid object from 
    #     the parameter. """
    #     # transform isotopes
    #     isotopes = {Isotope.from_string(iso): n for iso, n in self.isotopes.items()}
    #     isotopes = {(iso.Z, iso.A): n for iso, n in isotopes.items()}

    #     # transform combinations
    #     combinations = [
    #         {Isotope.from_string(iso): n for iso, n in combination.items()}
    #         for combination in self.combine_isotopes
    #     ]
    #     combinations = [
    #         {(iso.Z, iso.A): n for iso, n in combination.items()}
    #         for combination in combinations
    #     ]
    #     d = self.dict()
    #     d["isotopes"] = isotopes
    #     d["combine_isotopes"] = combinations
    #     return FormulaGrid(**d)
