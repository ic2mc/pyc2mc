#!/usr/bin/env python

"""
This module provides enum class of attribution algorithms.
"""


from pydantic import PositiveFloat, PositiveInt, field_validator

from pyc2mc.controls import ProcessingControls
from pyc2mc.core.formula import KendrickBuildingBlock

__all__ = ["KendrickSeriesControls"]


class KendrickSeriesControls(ProcessingControls):
    """ This class provides the parameters in order to look for Kendrick
    series based on a custom repeat unit.

    Attributes:
        lambda_parameter (float): error un ppm associated to the searching
            mode of experimental m/z.
        min_length (int): is the minimum length to accept
            a Kendrick Series or reject it.
        building_block (float, str, Formula): Kendrick Building Block
            formula, or mass. If not defined, C1 H2 mass is taken by 
            default.
        use_SN (bool): if True, the S/N ratio is used to sort the peaklist.
            If False, the peaklist is sorted according to intensity.
        threshold (float): A threshold to group Kendrick Series that 
            contains peaks with intensities over a threshold percentage
            with respect to the highest intensity. It is None by default.
        limit_number (int): limit the number of kendrick series to a 
            maximum number.
        n_max_hole (int): maximum number of missing peak in a series. If
            the number of missing peaks is equal to n_max_hole
            the building of the series is stopped.

    """
    lambda_parameter: PositiveFloat = 1.0
    min_length: PositiveInt = 2
    building_block: str = "C H2"
    use_SN: bool = True
    threshold: PositiveFloat = None
    limit_number: PositiveInt = None
    n_max_hole: PositiveInt = None

    @field_validator("building_block")
    @classmethod
    def building_block_check(cls, field_value):
        """ check if isotopic pattern is a valid isotope """
        try:
            bb = KendrickBuildingBlock(field_value)
        except ValueError as error:
            raise error
        else:
            return bb

    def get_params(self) -> dict:
        """ Get the parameter dict """
        return self.model_dump(exclude_none=True)
