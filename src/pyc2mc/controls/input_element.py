# coding: utf-8

from collections.abc import Sequence

from typing import Tuple, Union, Dict
from pydantic import (BaseModel, constr, PositiveFloat, NonNegativeFloat,
                      validator, PositiveInt, NonNegativeInt)

from pyc2mc.core.formula_grid import Polarity
from pyc2mc.core.isotopes import Element

# string constraint for element validation
CONELM = constr(strip_whitespace=True, regex=r"^[A-Z][a-z]?$")
CONISO = constr(strip_whitespace=True, regex=r"^\d{1-3}[A-Z][a-z]?$")


def default_isotopes(elements):
    """
    Returns a default dict of isotopes in the form:

        {(6, 13, 1): 1, (6, 13, 2): 2, ...}

    Args:
        elements(FormulaGrid.elements): is a list of elements of the
            pyc2mc.core.isotopes.Element class.
    """
    isotopes = {}
    for el in elements:
        if el.symbol == "C":
            isotopes[6, 13] = 1
            isotopes[6, 13] = 2
        elif el.symbol == "N":
            isotopes[7, 15] = 1
        elif el.symbol == "O":
            isotopes[8, 18] = 1
        elif el.symbol == "S":
            isotopes[16, 34] = 1

    return isotopes


class InputElement(BaseModel):
    """ This class contains the information of the required element. The
    element is defined from its atomic number and by default the most
    abundant isotope is used. Additional isotopes can be included by
    defining the mass number and the amount of isotopes by formula.

    Args:
        Z (int): Positive integer, atomic number to define the element
        bounds (int, int): Tuple of the lower and upper bounds of the
            amount of the element.
        check_abundant (bool): If isotopes of the elements have to be
            included, if True, check if the abundance of the isotopes is
            known.
        isotopes (list of tuple): List of tuple defining the additional
            isotopes which will be included. Each element of the list is
            a tuple of integer, the first integer is the mass number and
            the second is the maximum number of this isotope in a
            chemical formula. For example, for at most two 10B and one
            13C: ``[(10, 2), (13, 1)]``
    """

    element: Union[CONELM, PositiveInt]  # no default
    bounds: Tuple[NonNegativeInt, PositiveInt] = (0, 10)
    check_abundant: bool = False
    isotopes: Sequence[Tuple[PositiveInt, PositiveInt]] = None

    @validator("element")
    def element_check(cls, v):
        """ Check if element exist """
        try:
            element = Element(v)
        except ValueError as error1:
            try:
                element = Element.from_str(v)
            except ValueError as error2:
                print(error1)
                print(error2)
                raise ValueError(f"Impossible to get element from {v}.")

        return element.symbol

    @validator("bounds")
    def bounds_match(cls, v):
        """ Check if the lower bound is smaller than the upper bound. """
        if v[0] > v[1]:
            raise ValueError(f"{v}: lower bound is greater than upper bound.")
        return v

    @validator("isotopes")
    def isotopes_check(cls, v, values):
        if "element" not in values:
            raise ValueError(
                "Element was wrong, could not validate isotopes.")
        else:
            element = Element.from_str(values["element"])

        if values["check_abundant"]:
            iso_data = element.get_isotopes(dropna=True)
        else:
            iso_data = element.get_isotopes()

        for A, _ in v:
            if A not in iso_data["mass number"].values:
                raise ValueError(
                    f"Isotope {A} of element {element} does not exist.")

        return v

    @property
    def lower_bound(self):
        """ Minimum number of this element to include in formula """
        return self.bounds[0]

    @property
    def upper_bound(self):
        """ Maximum number of this element to include in formula """
        return self.bounds[1]

    @property
    def use_isotopes(self):
        """ Returns True if isotopes have to be considered """
        return True if self.isotopes is not None else False

    def get_element(self):
        return Element.from_str(self.element)
