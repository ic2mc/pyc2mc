# coding: utf-8

""" This module provides a model class which allows to validate
parameters for calibration """

from pathlib import Path
from typing import Union

from pydantic import (PositiveFloat, PositiveInt,
                      field_validator)

from pyc2mc.controls import ProcessingControls
from pyc2mc.core.calibration import CalibList
from pyc2mc.core.peak import PeakList
from pyc2mc.processing.recalibration import RecalibrationMethods
from pyc2mc.processing.recalibration_models import RecalibrationModels
from pyc2mc.core.peak_search import SearchingMode


class CalibrationModelControls(ProcessingControls):
    """ This class provides the parameters to define the equation used
    for calibration.

    Attributes:
        model (str): one of the RecalibrationModels
        fit_intercept (bool): If True a constant parameter is included
        deg (int): polynomial degree if model is 'polynomial'
    """
    # model selection
    model: RecalibrationModels = RecalibrationModels.POLYNOMIAL
    deg: PositiveInt = 2
    fit_intercept: bool = False

    @field_validator("model", mode="before")
    @classmethod
    def model_validation(cls, field_value):
        """ Check if model is in members of Enum """
        return RecalibrationModels.from_alias(field_value)

    def get_params(self) -> dict:
        """ Get the parameter dict """
        return self.model_dump(exclude_none=True)


class CalibrationListSetUpControls(ProcessingControls):
    """ This class provides the parameters in order to import or build
    the calibration list and look for experimental values.

    Attributes:
        searching_mode: How to search for experimental m/z values from the
            theoretical m/z values from the calibration list.
        lambda_parameter (float): error un ppm associated to the searching
            mode of experimental m/z.
        use_SN (bool): If True use S/N to determine most abundant peak in
            searching mode 'most_abundant'
        add_isotopes (bool): If True, 13C1 and 13C2 molecular formula are
            added to calibration list.
        filter_outliers (bool): If True, a TSE filter of calibration points
            is applied.
    """
    searching_mode: SearchingMode = SearchingMode.most_abundant
    lambda_parameter: PositiveFloat = 2.0
    use_SN: bool = True
    add_isotopes: bool = True
    filter_outliers: bool = True

    def get_params(self) -> dict:
        """ Get the parameter dict """
        return self.model_dump(exclude_none=True)

    def setup_calibration_list(
        self,
        peaklist: PeakList,
        cl: CalibList,
    ) -> CalibList:
        """ Set up the caliblist using input parameters """

        cl = cl.search_mz_exp(
            peaklist=peaklist, inplace=False,
            searching_mode=self.searching_mode,
            use_SN=self.use_SN, lambda_parameter=self.lambda_parameter
        )

        if len(cl) == 0:
            raise ValueError(
                "Calibration list is empty. Any matching found between "
                "experimental and theoretical m/z values."
            )

        if self.add_isotopes:
            cl = cl.add_isotopes(
                peaklist=peaklist,
                inplace=False,
                lambda_parameter=self.lambda_parameter,
                use_SN=self.use_SN,
                searching_mode=self.searching_mode)

        if self.filter_outliers:
            cl = cl.filter_outliers(inplace=False)

        return cl


class WalkingCalibrationControls(ProcessingControls):
    """ This class provides the parameters of a walking calibration 

    Attributes:
        segment_size (float or str): segment width or 'auto' for automatic
            determination of segments, see 'min_npts_per_segment' and
            'min_segment_size'.
        min_npts_per_segment (int): minimum number of points per segment
            in automatic mode for walking calibration
        min_segment_size (float): minimum width of segments in automatic
            mode for walking calibration
    """
    segment_size: Union[str, PositiveFloat] = "auto"
    min_npts_per_segment: PositiveInt = 10
    min_segment_size: PositiveFloat = 25.0
    global_recal_kws: CalibrationModelControls = {}

    @field_validator("segment_size")
    @classmethod
    def segments_validation(cls, field_value):
        """ Check if segment_size is either a float or "auto" """
        if isinstance(field_value, str):
            if field_value != "auto":
                raise ValueError(
                    "'segment_size' must be a float or 'auto'."
                    f"'segment_size' is '{field_value}'.")
        return field_value

    def get_params(self) -> dict:
        """ Get the parameter dict """
        if self.segment_size == "auto":
            d = dict(segments_kws=dict(
                min_npts_per_segment=self.min_npts_per_segment,
                min_segment_size=self.min_segment_size,
            ))
        else:
            d = dict(segment_size=self.segment_size)
        if len(self.global_recal_kws) > 0:
            d.update(dict(global_recal_kws=self.global_recal_kws))
        return d
        # return self.model_dump(exclude_none=True)


class SaveCalibrationListControls(ProcessingControls):
    """ This class provides the parameters for save calibration list to a
    file.

    Attributes:
        folder (str or Path): A folder path where the calibration list 
            will be recorded.
        basename (str): The base name of the file of the exported calibration
            list
        file_fmt (str): 'ref' or 'csv', defines the format.

    """
    folder: Union[str, Path] = "caliblist"
    basename: str = "caliblist"
    file_fmt: str = "csv"

    @field_validator("file_fmt")
    @classmethod
    def file_fmt_validation(cls, field_value):
        """ Check if file format is known """
        if field_value not in ["ref", "csv"]:
            raise ValueError(
                f"'file_fmt' is '{field_value}' but it must be a 'csv' or 'ref'."
            )
        return field_value

    @field_validator("folder")
    @classmethod
    def folder_exists(cls, field_value):
        """ Check if the given folder exist and make it if no """
        p = Path(field_value)
        if p.is_dir():
            return field_value
        elif p.is_file():
            raise ValueError(f"'{p}' exists and is a regular file.")
        else:
            p.mkdir(exist_ok=False, parents=True)

    def get_params(self) -> dict:
        """ Get the parameter dict """
        return self.model_dump(exclude_none=True)

    def save_caliblist(self, cl: CalibList, name: str = ""):
        """ Save the caliblist with a given name and in a give folder """

        if name == "":
            name = f"{self.basename}.{self.file_fmt}"

        path = Path(self.folder) / f"{name}"

        if self.file_fmt == "csv":
            cl.to_csv(path)
        elif self.file_fmt == "ref":
            cl.to_ref_file(path)


class CalibrationListControls(ProcessingControls):
    """ This class contains the input parameters in order to set up a
    calibration list and proceed to the calibration. Here the calibration
    is done based on a lits of calibration points corresponding to 
    theoretical exact mz. The link between theoretical and experimental
    mz is done depending on the setup parameters.

    Attributes:
        caliblist (str or Path): path to the calibration list file
        setup (CalibrationListSetUpControls): Parameters to link exp and
            theoretical mz and feed the calibration list.
        method: standard or walking (or mass difference)
        model: define the recalibration function
        walking_ctrl: additional parameters for walking equation
        save: parameters for exporting the calibration list.        

    """
    # calibration list
    caliblist: Union[str, Path] = None

    # setup calibration list
    setup: CalibrationListSetUpControls = CalibrationListSetUpControls()

    # calibration method
    method: RecalibrationMethods = RecalibrationMethods.standard

    # model
    model: CalibrationModelControls = CalibrationModelControls()

    # walking
    walking_ctrl: WalkingCalibrationControls = WalkingCalibrationControls()

    # export
    save: SaveCalibrationListControls = SaveCalibrationListControls()

    @field_validator("caliblist")
    @classmethod
    def path_exists(cls, field_value):
        """ Check if the given path exist or not """
        p = Path(field_value)
        if p.is_file():
            return field_value
        else:
            raise FileNotFoundError(
                f"'{p}' does not exist or is not a regular file.")

    def get_params(self) -> dict:
        """ Get the parameter dict """
        return self.model_dump(exclude_none=True)
