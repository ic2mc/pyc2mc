#!/usr/bin/env python

"""
This module provides enum class of attribution algorithms.
"""

from typing import Tuple, Union, Sequence
from pathlib import Path
from pydantic import (
    PositiveFloat,
    PositiveInt,
    NonNegativeInt,
    ValidationError,
    field_validator,
    Field,
)

from pyc2mc.core.formula import KendrickBuildingBlock
from pyc2mc.core.isotopes import Isotope
from pyc2mc.controls import ProcessingControls
from pyc2mc.controls.kendrick_series import KendrickSeriesControls
from pyc2mc.core.peak import AttributedPeakList


__all__ = ["LowestErrorControls", "IsotopicPatternControls",
           "KendrickAttributionControls"]


FormulaGridsSelector = Union[str, NonNegativeInt,
                             Sequence[NonNegativeInt], Sequence[str]]


class SaveAttributionControls(ProcessingControls):
    """ This class provides the parameters to save the files after attribution

    Attributes:
        save_data (bool): Whether to save data or not.
        folder (str or Path): A folder path to save the data
        basename (str): The base name of the file of the exported data
        attributed_only (bool): If True, only attributed peak are saved,
            default is False.
        full_data (bool): If True (default), additional data are exported

    """
    save_data: bool = True
    folder: Union[str, Path] = "Attribution"
    basename: str = "pyc2mc_attribution"
    attributed_only: bool = False
    full_data: bool = True
    # file_fmt: str = "csv"  # this is the only one supported

    # @field_validator("file_fmt")
    # @classmethod
    # def file_fmt_validation(cls, field_value):
    #     """ Check if file format is known """
    #     if field_value not in ["csv"]:
    #         raise ValueError(
    #             f"'file_fmt' is '{field_value}' but it must be a 'csv'"
    #         )
    #     return field_value

    @field_validator("folder")
    @classmethod
    def folder_exists(cls, field_value):
        """ Check if the given folder exist and make it if no """
        p = Path(field_value)
        if p.is_dir():
            return field_value
        elif p.is_file():
            raise ValueError(f"'{p}' exists and is a regular file.")
        else:
            p.mkdir(exist_ok=False, parents=True)

    def get_params(self) -> dict:
        """ Get the parameter dict """
        return self.model_dump(exclude_none=True)

    def save_attributed_peaklist(self, peaklist: AttributedPeakList,
                                 name: str = ""):
        """ Save the attributed peaklist with a given name and in a 
        given folder """

        if name == "":
            # name = f"{self.basename}.{self.file_fmt}"
            name = f"{self.basename}.csv"

        path = Path(self.folder) / f"{name}"

        peaklist.to_csv(path, full_data=self.full_data,
                        attributed_only=self.attributed_only)


class BaseAttributionControls(ProcessingControls):
    """ This class provides the main parameters which are shared by
    several attribution algorithms.

    Attributes:
        formula_grids: The list of formula grid to be used can be 
            defined in several ways. If it is a scalar,
            the corresponding formula grid is selected according to
            its name (if a str) or its index (if a int). If it is
            a sequence, a list of formula grid is considered in order
            to implement iteration tables. Formula grids are selected
            by their names (if sequence of str) or their index (if 
            sequence of int).
        lambda_parameter (float): error un ppm used for attribution.
        use_isotopes (bool): if True, the algorithm will assign the
            isotopes of the monoisotopic attributed formula.
        use_SN (bool): If True use S/N to determine most abundant peak in
            searching mode 'most_abundant'

    """
    formula_grids: FormulaGridsSelector = Field("all", alias="formula_grid")
    lambda_parameter: PositiveFloat = 2.0
    use_isotopes: bool = True
    use_SN: bool = True
    save: SaveAttributionControls = SaveAttributionControls()

    @field_validator("formula_grids", mode="after")
    @classmethod
    def formula_grid_check(cls, field_value):
        """ Convert formula grids to a sequence """
        if isinstance(field_value, list):
            return field_value
        else:
            return [field_value]

    def get_params(self) -> dict:
        """ Get the parameter dict.

        The formula_grids is excluded because here it only stores the names
        of the index of the wanted formula grid. Not the real instance of
        a FormulaGrid object.

        Save is also excluded are it is not part of the attribution functions.
        """
        return self.model_dump(exclude_none=True,
                               exclude=["formula_grids", "save"])


class LowestErrorControls(BaseAttributionControls):
    """ This class provides the parameters in order to proceed to a 
    lowest error attribution.

    Attributes:
        formula_grids: The list of formula grid to be used can be 
            defined in several ways. If it is a scalar,
            the corresponding formula grid is selected according to
            its name (if a str) or its index (if a int). If it is
            a sequence, a list of formula grid is considered in order
            to implement iteration tables. Formula grids are selected
            by their names (if sequence of str) or their index (if 
            sequence of int).
        lambda_parameter (float): error un ppm used for attribution.
        use_isotopes (bool): if True, the algorithm will assign the
            isotopes of the monoisotopic attributed formula.
        use_SN (bool): If True use S/N to determine most abundant peak in
            searching mode 'most_abundant'

    """
    pass


class IsotopicPatternControls(LowestErrorControls):
    """ This class provides the parameters in order to proceed to an
    isotopic pattern attribution.

    Attributes:
        formula_grids: The list of formula grid to be used can be 
            defined in several ways. If it is a scalar,
            the corresponding formula grid is selected according to
            its name (if a str) or its index (if a int). If it is
            a sequence, a list of formula grid is considered in order
            to implement iteration tables. Formula grids are selected
            by their names (if sequence of str) or their index (if 
            sequence of int).
        lambda_parameter (float): error un ppm used for attribution.
        use_isotopes (bool): if True, the algorithm will assign the
            isotopes of the monoisotopic attributed formula.
        use_SN (bool): If True use S/N to determine most abundant peak in
            searching mode 'most_abundant'
        isotopic_pattern (tuple): isotopic pattern to consider for the 
            attribution as a tuple (Z, A). Default is 13C (6, 13)
        SN_threshold (float): threshold on the S/N ratio. If the S/N of
            a monoisotopic formula is lower than this threshold we
            consider that it is not possible to find an isotopic peak.

    """
    isotopic_pattern: Tuple[PositiveInt, PositiveInt] = (6, 13)
    SN_threshold: float = 50

    @field_validator("isotopic_pattern")
    @classmethod
    def isotope_check(cls, field_value):
        """ check if isotopic pattern is a valid isotope """
        try:
            Isotope(Z=field_value[0], A=field_value[1])
        except ValueError as error:
            raise error
        else:
            return field_value


class KendrickAttributionControls(LowestErrorControls):
    """ This class provides the parameters in order to proceed to an
    attribution based on kendrick series.

    Attributes:
        formula_grids: The list of formula grid to be used can be 
            defined in several ways. If it is a scalar,
            the corresponding formula grid is selected according to
            its name (if a str) or its index (if a int). If it is
            a sequence, a list of formula grid is considered in order
            to implement iteration tables. Formula grids are selected
            by their names (if sequence of str) or their index (if 
            sequence of int).
        lambda_parameter (float): error un ppm used for attribution.
        use_isotopes (bool): if True, the algorithm will assign the
            isotopes of the monoisotopic attributed formula.
        use_SN (bool): If True use S/N to determine most abundant peak in
            searching mode 'most_abundant'
        isotopic_pattern (tuple): isotopic pattern to consider for the 
            attribution as a tuple (Z, A). Default is 13C (6, 13)
        building_block (str or Formula): specify the molecular composition 
            of the Kendrick Series building block.
        min_length_threshold (int): If the number of attributed peak in a
            series is lower than this value, the series is not attributed.
        use_lowest_error (bool): If True and no isotopic peak is found
            a lowest error attribution is applied to the series.
        ks_kwargs (dict): dict of parameters for kendrick series 
            calculations

    """
    isotopic_pattern: Tuple[PositiveInt, PositiveInt] = (6, 13)
    building_block: str = "C H2"
    min_length_threshold: PositiveInt = 4
    use_lowest_error: bool = False
    ks_kwargs: dict = None

    @field_validator("isotopic_pattern")
    @classmethod
    def isotope_check(cls, field_value):
        """ check if isotopic pattern is a valid isotope """
        try:
            Isotope(Z=field_value[0], A=field_value[1])
        except ValueError as error:
            raise error
        else:
            return field_value

    @field_validator("building_block")
    @classmethod
    def building_block_check(cls, field_value):
        """ check if isotopic pattern is a valid isotope """
        try:
            bb = KendrickBuildingBlock(field_value)
        except ValueError as error:
            raise error
        else:
            return bb

    @field_validator("ks_kwargs")
    @classmethod
    def ks_kwargs_check(cls, field_value):
        """ check parameters for kendrick series """
        if field_value is None:
            return None
        else:
            try:
                KendrickSeriesControls(**field_value)
            except ValidationError as exc:
                m = "Wrong parameters for kendrick series"
                raise ValidationError(m) from exc
            return field_value
