#!/usr/bin/env python

"""
This module provides a model to validates all parameters to read/import
data.
"""

from typing import Union, Sequence
from pathlib import Path

from pydantic import field_validator

from pyc2mc.controls import ProcessingControls
from pyc2mc.core import SampleType


__all__ = ["SampleType", "SampleImportControls"]


class SampleImportControls(ProcessingControls):
    """ parameter for reading the sample data.

    Attribute:
        sample_type: define the sample type
        polarity (int): the charge (TODO: change this to charge)
        ionization_source: define the ionization source (TODO: combine this
            with charge instead of polarity)
        path (str or path): folder or filename where the data have to be read
        filenames (list): list of path or filename to read.

    """

    sample_type: SampleType = SampleType.raw_1D
    polarity: int  # should be charge
    ionization_source: str = "TODO"  # implement maxime Enum
    path: Union[str, Path] = None
    filenames: Sequence[Union[str, Path]] = None

    @field_validator("path")
    @classmethod
    def path_exists(cls, field_value):
        """ Check if the given path exist or not """
        p = Path(field_value)
        if p.exists():
            return field_value
        else:
            raise FileNotFoundError(f"Path {p} does not exist.")

    @field_validator("filenames")
    @classmethod
    def paths_exist(cls, field_value):
        """ Check if all filenames exist """
        for filename in field_value:
            p = Path(filename)
            if not p.exists():
                raise FileNotFoundError(f"Path {p} does not exist.")
        return field_value

    def get_params(self) -> dict:
        """ Get the parameter dict """
        return self.model_dump(exclude_none=True)
