# coding: utf-8


"""
This module defines classes representing peak and attributed peak
"""

from abc import ABCMeta
import warnings
from typing import Union
from collections.abc import Sequence
from collections import defaultdict
from pathlib import Path
from dataclasses import dataclass, asdict
import re

import numpy as np
from numpy.typing import ArrayLike
import pandas as pd

from pyc2mc.core.isotopes import all_exact_masses
from pyc2mc.core.formula import (Formula, Composition, compute_dbe,
                                 get_formula, KendrickBuildingBlock,
                                 get_species, sort_elements)
import pyc2mc.plotinator.peak_plotter as pplot

MAX_PEAKS = 50
CH2 = Formula.from_string("C1 H2")
iso_patt = re.compile(r"^\d{0,3}([A-Za-z]{1,2})$")


@dataclass
class NoiseFittingMetadata:
    """ This class provides the parameters used for noise threshold calculation
    and filtering.

    Attributes:
        polynomial_order (int): The order of the polynomial function used
            for noise fitting.
        polynomial_params (list of floats): Parameters of the polynomial
            function used for noise fitting in decreasing power order.
        n_sigma (int): the threshold is computed as the S/N mean plus
            n times sigma, sigma being the S/N standard deviation. Usually
            a 6 sigma threshold is used.

    """

    polynomial_order: int
    polynomial_params: Sequence[float]
    n_sigma: int = 6

    def to_dict(self):
        d = {k: v for k, v in asdict(self).items() if v is not None}
        return d


@dataclass
class PeakListMetadata:
    """ This class provides metadata associated to a peak list object. 
    In particular, the metadata describe the parameters use to implement
    the peak picking step.
    
    Attributes:
        min_mz (float): minimal m/z value
        max_mz (float): maximal m/z value
        npeaks (int): total number of peaks
        polarity (int): charge (TODO: Charge is not exactly polarity)
        apodization (str): The apodization functions (TODO ??? maybe needs additional parameters)
        noise_threshold (float or str): If noise_threshold is a scalar
            number, it means that a constant noise threshold value was
            used for the peak picking procedure. In other case, noise_threshold
            provides the fitting parameters.
        peak_threshold (float):
        baseline_SN (float):
        sigma_SN (float):
        noise_fitting (NoiseFittingMetadata): Describe the parameters of
            noise fitting before peak peaking, if peak_peacking method is
            'fitting'.
        time (float): the time at which the scan was recorded in LC run
        is_SN_avail (bool): True if S/N is available

    """

    min_mz: float
    max_mz: float
    npeaks: int
    polarity: int
    apodization: str = None
    noise_threshold: Union[float, NoiseFittingMetadata] = None
    peak_threshold: float = None
    baseline_SN: float = None
    sigma_SN: float = None
    time: float = None
    is_SN_avail: bool = False

    def to_dict(self):
        d = {k: v for k, v in asdict(self).items() if v is not None}
        return d


def _check_SN_avail(array) -> bool:
    """ Check if the array is None or an array of np.nan or a valid list
    and in consequence, return a array """
    if array is None:
        avail = False
    elif isinstance(array, Sequence) and len(array) == 0:
        # empty array
        avail = False
    elif np.all(np.isnan(array)):
        avail = False
    else:
        avail = True

    return avail


class Peak:
    """
    This class represents a general peak in a mass spectroscopy experiement.
    It is mainly a container providing information about the peak.
    """

    def __init__(
        self, 
        mz: float, 
        intensity: float,
        SN: float = None,
        pid: Union[int, None] = None,
        properties: Union[dict, None] = None,
        **kwargs,
    ):
        """
        Create a peak

        Args:
            mz (float): the m/z value
            intensity (float): intensity or abundance of the peak
            SN (float): the Signal / Noise value
            pid (int): peak identifier, basically the index of the peak
                in the spectra.
            properties (dict): a dictionary containing additional
                properties of a peak.
        """

        self._mz = mz
        self._intensity = intensity
        self._pid = pid if pid else 0
        self._properties = properties if properties else {}
        self._is_attributed = False
        if "attributed" in kwargs and kwargs["attributed"]:
            message = "If peak is attributed you shall use Attributed Peak."
            warnings.warn(message, UserWarning, stacklevel=2)

        self._SN_avail = False if SN is None else True
        self._SN = SN

    @property
    def mz(self) -> float:
        """ Measured mass / z value of the peak. """
        return self._mz

    @property
    def intensity(self) -> float:
        """ Intensity of the peak. """
        return self._intensity

    @property
    def pid(self) -> int:
        """ Peak identifier a uniq integer to identify the peak. """
        return self._pid

    @property
    def SN(self) -> float:
        """ Signal to noise value. """
        return self._SN

    @property
    def properties(self) -> dict:
        """ Return a dictionary with the additional properties of the peak """
        return self._properties

    @property
    def integer_part(self):
        """ Returns the integer part of the mz of a peak. """
        return np.floor(self.mz).astype(np.uint16)

    @property
    def is_attributed(self) -> bool:
        """ For compatibility with attributed peak, returns a bool indicating
        that the peak is non attributed """
        return self._is_attributed

    @property
    def SN_avail(self):
        """ True if S/N ratio is available and False otherwise. """
        return self._SN_avail

    def get_kendrick_mass(
        self,
        building_block: Union[KendrickBuildingBlock, str, float] = CH2
    ) -> float:
        """
        Returns the kendrick mass for the corresponding building
        block.

        Args:
            building_block (KendrickBuildingBlock): Kendrick Building Block
                as formula, a float or a KendrickBuildingBlock, default CH2.
        """
        building_block = KendrickBuildingBlock(building_block)
        factor = np.round(building_block.exact_mass)
        return self._mz * factor / building_block.exact_mass

    def get_kendrick_mass_defect(
        self,
        building_block: Union[KendrickBuildingBlock, str, float] = CH2
    ) -> float:
        """
        Returns the kendrick mass for the corresponding building
        block.

        Args:
            building_block (KendrickBuildingBlock): Kendrick Building Block
                as formula, a float or a KendrickBuildingBlock, defaut CH2.
        """
        km = self.get_kendrick_mass(building_block)
        return np.round(km) - km

    def __repr__(self):
        if self._SN_avail:
            return (f"Peak(mz={self.mz:12.6f}, intensity={self.intensity:8e},"
                    f" SN={self.SN:8.3f}, pid={self.pid})")
        else:
            return (f"Peak(mz={self.mz:12.6f}, intensity={self.intensity:8e},"
                    f" pid={self.pid})")

    def __str__(self):
        if self._SN_avail:
            return (f"{self.pid:6d} {self.mz:12.6f} "
                    f"{self.intensity:8e} {self.SN:8.3f}")
        else:
            return f"{self.pid:6d} {self.mz:12.6f} {self.intensity:8e}"

    def __eq__(self, other):
        """ equality is based on peak identifier """
        return self.pid == other.pid

    def __ne__(self, other):
        return not self.__eq__(other)

    def __gt__(self, other):
        """ Sort peak according to mass over z """
        return self.mz > other.mz

    def as_dict(self) -> dict:
        """
        JSON-serializable dict representation for Peak
        """
        d = {
            "mz": self._mz,
            "intensity": self._intensity,
            "SN": self._SN,
            "pid": self._pid,
            "attributed": self._is_attributed,
            "properties": self._properties
        }
        return d

    def to_dataframe(self, include_properties: bool = False, 
                     *args, **kwargs) -> pd.DataFrame:
        """
        Returns a single row dataframe containing the peak data.
        """
        d = self.as_dict()
        pid = d.pop("pid")
        props = d.pop("properties")
        df = pd.DataFrame(d, index=[pid])
        if include_properties:
            for prop in props:
                df[prop] = props[prop]
        return df

    @classmethod
    def from_dict(cls, d: dict):
        """
        Create a Peak from a dict
        """
        for k in d:
            if k not in ["mz", "intensity", "SN", "pid", "properties"]:
                d.pop(k)
        return cls(**d)


class AttributedPeak(Peak):
    """ This class represent an attributed peak containing a list of
    formula candidates """

    def __init__(
        self,
        mz: float, 
        intensity: float,
        formula: Union[dict, str, Formula, Composition],
        polarity: int,
        SN: Union[float, None] = None,
        pid: Union[int, None] = None,
        attribution_method: str = "unknown",
        exact_masses: dict = None,
        properties: Union[dict, None] = None,
        **kwargs
    ):
        """
        Create an attributed peak.

        Args:
            mz (float): the m/z value.
            intensity (float): intensity or abundance of the peak.
            formula (dict, Formula, Composition): A dictionnary with the
                molecular composition, or a Composition or a Formula object.
            polarity (int): the ionization charge
            SN (float): the Signal / Noise value
            pid (int): peak identifier, basically the index of the peak
                in the spectra.
            attribution_method (str): A string indicating how the
                formula was attributed to the peak, default is 'unknown'
            exact_masses (dict): dictionnary containing the values for
                the exact masses, this is optional, if not, the masses
                will be obtained from the Elements and Isotope classes.
            properties (dict): a dictionary containing additional
                properties of a peak.
        """
        super().__init__(mz, intensity, SN, pid, properties=properties)
        if not isinstance(formula, Formula):
            formula = get_formula(formula, exact_masses=exact_masses)

        if formula:
            self._formula = formula
            self._is_attributed = True
        else:
            self._formula = np.nan
            self._is_attributed = False

        self._polarity = polarity
        self._attribution_method = attribution_method

    @property
    def formula(self) -> Formula:
        """ Molecular formula of the attributed peak """
        return self._formula

    @property
    def is_isotopic(self):
        """
        Returns a boolean that indicates if it is an isotopic formula or
        not.
        """
        return self.formula.is_isotopic

    @property
    def chem_class(self):
        """
        Returns a string with the information of the chemical class
        corresponding to the attributed molecular formula.
        """
        return self.formula.chem_class

    @property
    def chem_group(self):
        """
        Returns a string with the information of the chemical group
        corresponding to the attributed molecular formula.
        """
        return self.formula.chem_group

    @property
    def elemental_composition(self):
        """
        Returns a dicionnary with the elemental composition of the peak,
        whether it is an isotope or not.
        """
        return self.formula.elemental_composition

    @property
    def attribution_method(self):
        """ A string indicating the attribution method used to attribute
        the formula to the peak. """
        return self._attribution_method

    @property
    def polarity(self):
        """ The charge corresponding to the ionization mode """
        return self._polarity

    @property
    def nominal_mass(self):
        """ Nominal mass of the formula attributed to the peak  """
        return self.formula.nominal_mass

    @property
    def calculated_mz(self):
        """ Returns the calculated Ion Molecular Mass of the formula
         attributed to the peak """
        return self.formula.get_ion_exact_mass(self.polarity)

    @property
    def str_formula(self):
        """ Returns the molecular formula as a string. """
        return str(self.formula)

    @property
    def error_in_ppm(self):
        """ Calculates and returns the error in ppm. """
        return self.formula.error_ppm(self.polarity, self.mz)

    @property
    def error_in_mDa(self):
        """ Calculates and returns the error in mDa. """
        return self.formula.error_mDa(self.polarity, self.mz)

    def as_dict(self) -> dict:
        """
        JSON-serializable dict representation for Peak
        """
        d = {
            "mz": self._mz,
            "intensity": self._intensity,
            "formula": str(self._formula),
            "polarity": self._polarity,
            "SN": self._SN,
            "pid": self._pid,
            "error_ppm": self.error_in_ppm,
            "attributed": self._is_attributed,
            "attribution_method": self._attribution_method,
            "properties": self._properties,
        }
        return d
    
    def to_dataframe(self, include_properties: bool = False, 
                     *args, **kwargs) -> pd.DataFrame:
        """
        Returns a single row dataframe containing the peak data.
        """
        d = self.as_dict()
        pid = d.pop("pid")
        props = d.pop("properties")
        df = pd.DataFrame(d, index=[pid])
        if include_properties:
            for prop in props:
                df[prop] = props[prop]
        return df

    def __repr__(self):
        if self._SN_avail:
            return (f"AttributedPeak(mz={self.mz:12.6f}, "
                    f"intensity={self.intensity:8e}, formula={self.formula},"
                    f" SN={self.SN:8.3f}, pid={self.pid})")
        else:
            return (f"AttributedPeak(mz={self.mz:12.6f}, "
                    f"intensity={self.intensity:8e}, formula={self.formula},"
                    f" pid={self.pid})")

    def __str__(self):
        if self._SN_avail:
            return (f"{self.pid:6d} {self.mz:12.6f} {self.intensity:8e} "
                    f" {str(self.formula):>20s} {self.SN:8.3f}")
        else:
            return (f"{self.pid:6d} {self.mz:12.6f} {self.intensity:8e} "
                    f" {str(self.formula):>20s}")


class PeakCollection(Sequence, metaclass=ABCMeta):
    """ This class represent a list of peaks """
    pass


class PeakList(Sequence):
    """ This class represent an immutable list of Peak objects. """

    def __init__(
        self,
        mz: Sequence[float],
        intensity: Sequence[float],
        SN: Sequence[float] = None,
        pid: Sequence[int] = None,
        peak_properties: dict = None,
        name: str = "",
        sort_mz: bool = True,
        metadata: PeakListMetadata = None,
        **kwargs
    ):
        """
        Args:
            mz (list-like): List or array of mass over z values.
            intensity (list-like): list or array of intensities.
            SN (list-like): list or array of the signal/noise values.
            pid (list-like): list or peak identifiers.
            peak_properties (dict): a dictionary containing additional
                properties of a peak.
            name (string): name of the peak list.
            sort_mz (bool): if True, values are sorted by mz.
            metadata (PeakListMetadata): Peak list information

        """
        self.name = name
        self.columns = ['mz', 'intensity', 'SN']
        self._SN_avail = False if SN is None else True
        self.metadata = metadata
        
        # convert to numpy array
        # conversion is not really neaded by it permits to check type and shape
        mz = np.array(mz, dtype=np.float64)
        intensity = np.array(intensity, dtype=np.float64)
        if self._SN_avail:
            SN = np.array(SN, dtype=np.float64)
        else:
            SN = np.full(len(mz), None)

        # this works only if pid is a list-like variable
        if pid is None or len(pid) == 0:
            pid = np.arange(len(mz), dtype=np.uint64)
        else:
            pid = np.array(pid, dtype=np.uint64)

        # check shape of data
        if not all([a.shape == mz.shape for a in [intensity, SN, pid]]):
            raise ValueError(
                "mz, intensity, SN and pid must all have "
                f"the same shape. mz is {mz.shape} "
                f"intensity is {intensity.shape} "
                f"SN is {SN.shape} "
                f"pid is {pid.shape}")

        # sort mz
        if sort_mz:
            argsort = np.argsort(mz)
        else:
            argsort = range(len(mz))

        # set up the list of peak
        peak_properties = {} if peak_properties is None else peak_properties
        self._peaks = list()
        self._peak_properties_keys = set()
        for i in argsort:
            props = None
            if peak_properties:
                props = {k: v[i] for k, v in peak_properties.items()}
                if i == argsort[0]:
                    self._peak_properties_keys = set(props.keys())
                else:
                    if self._peak_properties_keys != set(props.keys()):
                        raise ValueError(
                            "The list of properties of each peak is not "
                            "consistent over the whole peaklist.\n"
                            f"properties: {self._peak_properties_keys}\n"
                            f"current peak: {Peak(mz[i], intensity[i], pid[i])}\n"
                            f"current peak props: {props}.\n"
                        )
            self._peaks.append(
                Peak(mz[i], intensity[i], SN[i], pid[i], properties=props))
        
        # keep peak list immutable by default
        self._peaks = tuple(self._peaks)

        # set the plotter
        self.plot = pplot.PeakListPlotter(self)

    @property
    def mz(self) -> ArrayLike:
        """ mz values of peaks """
        return np.array([p.mz for p in self._peaks], dtype=np.float64)

    @property
    def intensity(self) -> ArrayLike:
        """ Intensity of peaks """
        return np.array([p.intensity for p in self._peaks], dtype=np.float64)

    @property
    def SN(self) -> ArrayLike:
        """ Signal to noise of peaks """
        return np.array([p.SN for p in self._peaks], dtype=np.float64)
    
    @property
    def pid(self) -> ArrayLike:
        """ peak id """
        return np.array([p.pid for p in self._peaks], dtype=np.uint32)

    @property
    def peak_properties(self):
        """ Return a dictionary with the additional properties of the peak """
        props = defaultdict(list)
        for peak in self._peaks:
            for k, v in peak.properties.items():
                props[k].append(v)
        return props
    
    @property
    def peak_properties_keys(self):
        """ Return a set with the available peak properties """
        return self._peak_properties_keys

    @property
    def values(self):
        """ Contains the PeakList in an array of three columns being:
        mz, intensity and signal to noise.
        """
        return np.array([
            [p.mz, p.intensity, p.SN] for p in self._peaks
        ], dtype=np.float64)
    
    @property
    def SN_avail(self) -> bool:
        """ True if signal to noise ratio is available """
        return self._SN_avail

    @property
    def size(self):
        """ length of data """
        return len(self._peaks)

    def to_dataframe(
        self,
        mz_bounds: tuple[float] = None,
        full_data: bool = False,
    ) -> pd.DataFrame:
        """
        Return the peak list in a pandas Data Frame format. If mz
        boundaries are provided, limit the masses to the corresponding range.

        Args:
            mz_bounds (list, tuple): min and max mz values
            full_data (bool): Default False, include peak properties 
                as columns

        Returns:
            A data frame with three columns: mz, intensity and S/N ratio.
        """

        if mz_bounds is None:
            df = pd.DataFrame(
                self.values,
                columns=self.columns,
                index=self.pid)
            if full_data:
                for prop in self.peak_properties_keys:
                    df.loc[:, prop] = self.peak_properties[prop]
        else:
            values = self.values
            imin, imax = np.searchsorted(values[:, 0], mz_bounds)
            df = pd.DataFrame(
                values[imin: imax],
                columns=self.columns,
                index=self.pid[imin: imax])
            if full_data:
                for prop in self.peak_properties_keys:
                    df.loc[:, prop] = self.peak_properties[prop][imin: imax]
        
        return df

    def delimit(
        self,
        mz_bounds: tuple[float] = None,
        intensity_bounds: Union[tuple[float], float] = None,
        SN_bounds: Union[tuple[float], float] = None
    ):
        """
        Delimits the peaklist according to the mz values, intensity and S/N
        and returns a new peak list. Filtering is done first on mz values,
        then on intensity and finally on S/N.

        Args:
            mz_bounds (tuple): a tuple of min and max mz values between which
                delimit the peaklist.
            intensity_bounds (tuple, float): a tuple of min and max intensity
                values between which delimit the peaklist. If only one value
                is given, intensity lower than this value are ignore.
            mz_bounds (tuple): a tuple of min and max SN values between which
                delimit the peaklist. If only one value is given, SN lower
                lower than this value are ignore.

        Returns:
            The delimited peaklist.
        """
        peaks = None

        if mz_bounds:
            try:
                if len(mz_bounds) != 2:
                    raise ValueError(
                        'mz_bounds must be a tuple of 2 floats. '
                        f"mz_bounds is: '{mz_bounds}'."
                    )
            except TypeError:
                raise ValueError(
                    'mz_bounds must be a tuple of 2 floats. '
                    f"mz_bounds is: '{mz_bounds}'."
                )

            imin, imax = np.searchsorted(self.values[:, 0], mz_bounds)
            peaks = [self._peaks[i] for i in range(imin, imax)]
        
        # filter on intensity
        if intensity_bounds:
            try:
                int_min, int_max = intensity_bounds
            except TypeError:
                int_min = intensity_bounds
                int_max = -1

            if peaks is None:
                peaks = self._peaks

            if int_max > 0:
                peaks = [p for p in peaks if (int_min < p.intensity < int_max)]
            else:
                peaks = [p for p in peaks if (p.intensity > int_min)]

        # filter on S/N
        if SN_bounds and self._SN_avail:
            try:
                SN_min, SN_max = SN_bounds
            except TypeError:
                SN_min = SN_bounds
                SN_max = -1

            if peaks is None:
                peaks = self._peaks

            if SN_max > 0:
                peaks = [p for p in peaks if (SN_min < p.SN < SN_max)]
            else:
                peaks = [p for p in peaks if (p.SN > SN_min)]

        if peaks is None:
            # all filtering was wrong
            peaks = []

        name = self.name if "delimited" in self.name else "delimited_" + self.name
        return self.from_list(peaks, name=name)

    def filter_threshold(self, mz_bounds: list[float], sigma: float = 1,
                         by_SN: bool = False):
        """
        Prefiltering data by intensity or SN in order to clear data. It will
        delete all data above a certain threshold, which is a cut off determined
        between a mz_bound containing mainly noise and not important signals.
        This cut off is calculated as the average plus sigma*stddev.
        If no constrain is specified then it will assume a sigma of 1 and it
        will filter by SN.

        Args:
            mz_bounds (list-like): m/z range to calculate the average and
                standard deviation. This region should not contain important
                signals.
            sigma (float): is the factor that removes data lower than n-times
                the standard deviation of the data between the defined bounds.
            by_SN (bool): this function filters by intensity by default,
                however, if this option is True, it will filter by S/N ratio.

        .. code-block:: python
            :caption: Filtering example with a pyc2mc.core.PeakList
            :linenos:

            # example
            new_pl = pl.filter_threshold(sigma=1, mz_bounds=[201.6, 201.9], by_intensity=False)
        """

        if by_SN:
            j = 2  # the third column of the values array is SN
        else:
            j = 1  # the second column of the values array is intensity

        imin, imax = np.searchsorted(self.values[:, 0], mz_bounds)
        data = self.values[imin: imax].copy()
        average = np.mean(data[:, j])
        stddev = np.std(data[:, j])
        cut_off = average + sigma * stddev
        if j == 1:
            new_pl = [peak for peak in self.peaks if peak.intensity > cut_off]
        else:
            new_pl = [peak for peak in self.peaks if peak.SN > cut_off]
        values = np.array([np.array(
            [peak.mz, peak.intensity, peak.SN, peak.pid]
        ) for peak in new_pl])
        return PeakList(
            values[:, 0],
            values[:, 1],
            values[:, 2],
            list(values[:, 3]),
            self.name)

    def get_min_max_intensity(self) -> tuple[float]:
        """
        Calculate the minimum and maximum intensities

        Returns
            min_I (float): minimum intensity
            max_I (float): maximum intensity

        """
        if len(self._peaks) > 0:
            # to avoid errors when delimiting out of boundaries
            intensity = self.intensity
            min_I = np.nanmin(intensity)
            max_I = np.nanmax(intensity)
        else:
            min_I = np.nan
            max_I = np.nan
        return min_I, max_I

    def get_min_max_mz(self) -> tuple[float]:
        """
        Calculate the minimum and maximum mz values

        Returns
            min_mz (float): minimum mz
            max_mz (float): maximum mz

        """
        if len(self._peaks) > 0:
            # to avoid errors when delimiting out of boundaries
            mz = self.mz
            min_mz = np.nanmin(mz)
            max_mz = np.nanmax(mz)
        else:
            min_mz = np.nan
            max_mz = np.nan
        return min_mz, max_mz

    @classmethod
    def from_dataframe(cls, df: pd.DataFrame, **kwargs):
        """
        Initialize the class PeakList from a pandas.DataFrame. The index
        of the data frame will be used as peak id.

        Args:
            df (pandas.DataFrame): a dataframe containing the mz,
                intensity and SN information.
            kwargs (dict): key value arguments of a PeakList object
        """
        if "SN" in df:
            return cls(
                df.mz.values,
                df.intensity.values,
                df.SN.values,
                list(df.index.values),
                **kwargs
            )
        else:
            return cls(
                df.mz.values,
                df.intensity.values,
                pid=list(df.index.values),
                **kwargs
            )

    @classmethod
    def from_list(cls, peaks: list, **kwargs):
        """ Initialize the class PeakList from a list of Peak object.

        Args:
            peaks (list): list of Peak object
            kwargs (dict): keywords argument passed to PeakList init.
        """

        if len(peaks) == 0:
            # empty peaklist
            return PeakList([], [])

        props = defaultdict(list)
        pids = list()
        values = list()
        SN_avail = list()
        for peak in peaks:
            pids.append(peak.pid)
            SN_avail.append(peak.SN_avail)
            values.append([peak.mz, peak.intensity, peak.SN])
            for k, v in peak.properties.items():
                props[k].append(v)

        values = np.array(values, dtype=np.float64)
        if all(SN_avail):
            SN = values[:, 2]
        else:
            SN = None

        return cls(
            mz=values[:, 0],
            intensity=values[:, 1],
            SN=SN,
            pid=pids,
            peak_properties=props,
            **kwargs
        )

    def get_kendrick_masses(
        self,
        building_block: Union[KendrickBuildingBlock, str, float] = CH2
    ) -> Sequence[float]:
        """
        Returns the corresponding kendrick masses.

        Args:
            building_block (KendrickBuildingBlock): Kendrick Building Block
                as formula, a float or a KendrickBuildingBlock, defaut CH2.
        """
        building_block = KendrickBuildingBlock(building_block)
        factor = np.round(building_block.exact_mass)
        return self.mz * factor / building_block.exact_mass

    def get_kendrick_mass_defects(
        self,
        building_block: Union[KendrickBuildingBlock, str, float] = CH2
    ) -> Sequence[float]:
        """
        Returns an array with the corresponding kendrick mass defect for
        the  original values array.

        Args:
            building_block (KendrickBuildingBlock): Kendrick Building Block
                as formula, a float or a KendrickBuildingBlock, defaut CH2.

        Returns:
            An array with the kendrick mass defects
        """
        km = self.get_kendrick_masses(building_block)
        return np.round(km) - km

    def to_csv(self, filename: str = "peaklist.csv", **kwargs):
        """ create a csv file. 
        
        Args:
            filename (str): name of the output csv file
            kwargs are assed to the ``to_dataframe`` method.

        """
        self.to_dataframe(**kwargs).to_csv(filename, index=False)

    def get_pid(self, pid: int) -> Peak:
        """ Return the peak corresponding to the given pid.

        Args:
            pid (int): pid of the requested peak.

        Returns:
            A Peak object.
        """
        message = "Please use the get_peak_index() method instead."
        warnings.warn(message, DeprecationWarning, stacklevel=2)
        idx, = np.nonzero(self.pid == pid)
        try:
            return self._peaks[idx[0]]
        except IndexError:
            raise IndexError(f"pid '{pid}' does not exist.")

    def get_peak_index(self, pid: int = None, mz_target: float = None) -> int:
        """ Return the index of a peak in the peak list according to a 
        pid or a m/z value. If m/z is provided, the returned peak is the
        one with the closest m/z value from ``mz_target``. If both pid and
        m/z are provided, pid is not taken into account.

        Args:
            pid (int): pid of the peak
            mz_target (float): mz value of the peak

        """
        if mz_target is not None:
            idx = np.searchsorted(self.mz, mz_target) - 1
            jdx = np.abs(self.mz[idx: idx + 1] - mz_target).argmin()
            idx += jdx
        elif pid is not None:
            idx, = np.nonzero(self.pid == pid)
            if len(idx) == 0:
                raise IndexError(f"pid '{pid}' does not exist.")
            else:
                idx = idx[0]
        else:
            raise ValueError(
                "You must provide mz_target or pid but: "
                f"mz_target is '{mz_target}' and pid is '{pid}'")

        return idx

    def __getitem__(self, item: Union[int, slice]):
        """ Return the required Peak if item is an integer or a new PeakList
        if item is a slice object """
        if isinstance(item, slice):
            start = item.start if item.start is not None else 0
            step = item.step if item.step is not None else 1
            if item.stop is None:
                stop = len(self._peaks) if step > 0 else -1
            else:
                stop = item.stop
            return self.from_list(
                [self._peaks[i] for i in range(start, stop, step)],
                name=self.name,
                sort_mz=False)
        else:
            return self._peaks[item]

    def __iter__(self):
        # this avoid using getitem and speed up (x3) loops over peaklist 
        return iter(self._peaks)

    def __len__(self):
        return len(self._peaks)

    def __str__(self):
        out = [f"PeakList, {len(self._peaks)} peaks"]
        for peak in self._peaks:
            out.append(str(peak))
        return "\n".join(out)

    def __repr__(self):
        out = [f"Peaklist ({len(self._peaks)} peaks)"]
        if len(self._peaks) > MAX_PEAKS:
            for peak in self._peaks[:6]:
                out.append(repr(peak))
            out.append("...")
            for peak in self._peaks[-5:]:
                out.append(repr(peak))
        else:
            for peak in self._peaks:
                out.append(repr(peak))
        return "\n".join(out)


class AttributedPeakList(PeakList):
    """ This class represent an attributed peak list. The class provides
    formula candidates for each peak.

    This class will contains a list of AttribtutedPeak objects
    """

    def __init__(
        self,
        mz: Sequence[float],
        intensity: Sequence[float],
        formulas: Sequence[Union[str, Formula, Composition, float]],
        polarity: int,
        SN: Sequence[float] = None,
        pid: Sequence[int] = None,
        peak_properties: dict = None,
        attribution_method: Sequence[str] = None,
        exact_masses: dict = {},
        name: str = "",
        sort_mz: bool = True,
        **kwargs
    ):
        """
        Initialize the AttributedPeakList class.

        Args:
            mz (list-like): List or array of mass over z values.
            intensity (list-like): list or array of intensities.
            formulas (list-like): list or array containing the molecular
                formula of each peak, could be strings, Compositions,
                Formulas or np.nan.
            polarity (int): the ionization charge
            SN (list-like): list or array of the signal/noise values.
            pid (list-like): list or peak identifiers.
            peak_properties (dict): a dictionary containing additional
                properties of a peak.
            attribution_method (list): name of the algorithm used to
                attribute each peak
            exact_masses (dict): dictionary containing the values for
                the exact masses, this is optional, if not, the masses
                will be obtained from the Elements and Isotope classes.
            name (string): name of the peak list.
            sort_mz (bool): if true, sort data by m/z values.
        """
        super().__init__(mz=mz, intensity=intensity, SN=SN, pid=pid, 
                         peak_properties=peak_properties,
                         name=name, sort_mz=False)
        
        n_data = len(self._peaks)
    
        self.name = name
        self._polarity = polarity

        if sort_mz:
            argsort = np.argsort(mz)
        else:
            argsort = np.arange(n_data)

        # check shapes of data
        if len(formulas) != n_data:
            raise ValueError(
                "Arguments must all have the same length and formulas is"
                f" not consistent. mz length is {n_data} "
                f"formula is {len(formulas)}")

        if attribution_method is None:
            attribution_method = np.full(n_data, "unknown")
        else:
            if len(attribution_method) != n_data:
                raise ValueError(
                    "Arguments must all have the same length and "
                    "attribution_method is"
                    f" not consistent. mz length is {n_data} "
                    f"attribution_method is {len(attribution_method)}")
            else:
                attribution_method = np.array(attribution_method)

        # look for the list of species in all the formula and get the
        # exact masses. This speed up the calculation of exact masses of
        # the formula. If species and exact_masses are not consistent
        # exact masses are recomputed.
        self._species = get_species(formulas)
        if set(self._species).issubset(exact_masses):
            self._exact_masses = exact_masses
        else:
            self._exact_masses = all_exact_masses(self._species)

        self._elemental_composition = set([iso_patt.match(specie).group(1)
                                           for specie in self._species])
        self._isotopes = set(self.species) - self._elemental_composition

        peaks = list()
        for i in argsort:
            if isinstance(formulas[i], Formula):
                formula = formulas[i]
            else:
                formula = get_formula(formulas[i], exact_masses=self._exact_masses)
            if formula is None:
                peaks.append(self._peaks[i])
            else:
                d = self._peaks[i].as_dict()
                d.pop("attributed")
                peaks.append(AttributedPeak(
                    **d,
                    formula=formula,
                    polarity=polarity,
                    attribution_method=attribution_method[i],
                    exact_masses=self._exact_masses))
            
        # keep peak list immutable by default
        self._peaks = tuple(peaks)

        self.all_classes = {peak.formula.chem_class for peak in self._peaks
                            if peak.is_attributed}

        self.monoisotopic_classes = {
            peak.formula.chem_class for peak in self._peaks
            if peak.is_attributed and not peak.formula.is_isotopic
        }

        # set the plotter
        self.plot = pplot.AttributedPeakListPlotter(self)
    
    @property
    def polarity(self):
        """ Polarity: ionization charge, used to proceed to the attribution. """
        return self._polarity

    @property
    def is_isotopic(self):
        """
        Returns an array with booleans that indicates if it is or not an
        isotopic formula.
        """
        return np.array([p.formula.is_isotopic if p.is_attributed else False
                         for p in self._peaks])

    @property
    def is_attributed(self):
        """
        Returns an array with booleans that indicates if it is or not an
        attributed peak.
        """
        return np.array([p.is_attributed for p in self._peaks])

    @property
    def chem_classes(self):
        """
        Returns an array of strings with the chemical classes for the
        corresponding calculated mz of the original values array.
        """
        return [p.chem_class if p.is_attributed else np.nan
                for p in self._peaks]

    @property
    def chem_groups(self):
        """
        Returns an array of strings with the chemical group for
        the corresponding calculated mz of the original values array.
        """
        return [p.chem_group if p.is_attributed else np.nan
                for p in self._peaks]
    
    @property
    def species(self):
        """ list of species as strings. This includes all isotopes """
        return self._species

    @property
    def elemental_composition(self) -> list:
        """ list of all elements present in the formulas. """
        return list(self._elemental_composition)

    @property
    def isotopes(self) -> list:
        """ list of isotopes present in the formulas. """
        return list(self._isotopes)
    
    @property
    def exact_masses(self) -> dict:
        """ dictionary of species and their exact masses. """
        return self._exact_masses

    @property
    def calculated_mz(self):
        """
        Returns an array with the corresponding calculated mz for the
        original values array.
        """
        return np.array([p.calculated_mz if p.is_attributed else np.nan
                         for p in self._peaks])

    @property
    def integer_part(self):
        """
        Returns an array of the m/z integer part.
        """
        return np.floor(self.mz).astype(np.uint16)

    @property
    def formulas(self):
        """
        Returns a numpy array with the corresponding molecular formulas
        """
        a = np.empty(len(self._peaks), object)
        for i in range(len(self._peaks)):
            if self._peaks[i].is_attributed:
                a[i] = self._peaks[i].formula
            else:
                a[i] = None
        return a

    @property
    def str_formulas(self):
        """ Return a numpy array of the formulas as strings """
        return np.array([str(p.formula) if p.is_attributed else "" 
                         for p in self._peaks])

    @property
    def error_in_ppm(self):
        """
        Returns an array with the corresponding error in ppm for the
        original values array.
        """
        return np.array([p.error_in_ppm if p.is_attributed else np.nan
                         for p in self._peaks])

    @property
    def error_in_mDa(self):
        """
        Returns an array with the corresponding error in ppm for the
        original values array.
        """
        return np.array([p.error_in_mDa if p.is_attributed else np.nan
                         for p in self._peaks])

    @classmethod
    def from_list(cls, attributed_peaks: list[Peak, AttributedPeak],
                  polarity: int = None, **kwargs):
        """ Initialize the class AttributedPeakList from a list of
        AttributedPeak objects. If the list contains AttributedPeak, the
        polarity is guessed from them. If the list contains only non
        attributed peak (Peak object), the polarity must be provided.
        The polarity arguments is considered only in this later case.

        Args:
            attributed_peaks (list): List of Peak or AttributedPeak
            polarity (int): The ionization charge

        Returns:
            An attributed peak list object.
        """
        if len(attributed_peaks) == 0:
            # empty peak list
            return cls(mz=[], intensity=[], formulas=[], polarity=None)
    
        props = defaultdict(list)
        pids = list()
        polarity_check = polarity
        formulas = list()
        attribution_method = list()
        SN_avail = list()
        values = list()
        for peak in attributed_peaks:
            pids.append(peak.pid)
            values.append([peak.mz, peak.intensity, peak.SN])
            SN_avail.append(peak.SN_avail)
            formulas.append(peak.formula if peak.is_attributed else None)
            attribution_method.append(
                peak.attribution_method if peak.is_attributed else "unknown"
            )
            
            if peak.is_attributed:
                if polarity_check is not None and peak.polarity != polarity_check:
                    raise ValueError(
                        "Polarity is not unique in the list of peak.\n"
                        f"{polarity_check}, {peak.polarity}")
                else:
                    polarity_check = peak.polarity

            for k, v in peak.properties.items():
                props[k].append(v)

        # check peak properties
        if not all([len(props[k]) == len(attributed_peaks) for k in props]):
            raise ValueError("Peak properties must be available for all peaks.")

        values = np.array(values, dtype=np.float64)
        if all(SN_avail):
            SN = values[:, 2]
        else:
            SN = None

        if polarity is None:
            # polarity was determined from peaks
            polarity = polarity_check

        if not polarity:
            # polarity is not available, the list is likely only a list
            # of not attributed peak
            message = ("Try to set up an AttributedPeakList but the list "
                       "contains only non attributed peak.\n"
                       "Polarity is not available.")
            raise ValueError(message)

        return cls(
            mz=values[:, 0], intensity=values[:, 1], formulas=formulas,
            polarity=polarity, SN=SN, pid=pids,
            peak_properties=props, attribution_method=attribution_method,
            **kwargs
        )
    
    def get_peaklist(self):
        """
        Return a peak list object with only mz, intensity, SN , pid and
        the peak properties. 
        """
        pid = self.pid
        values = self.values
        if self.SN_avail:
            return PeakList(values[:, 0], values[:, 1], values[:, 2], pid=pid,
                            peak_properties=self.peak_properties, name=self.name)
        else:
            return PeakList(values[:, 0], values[:, 1], SN=None, pid=pid,
                            peak_properties=self.peak_properties, name=self.name)

    def to_dataframe(
        self,
        mz_bounds: tuple[float] = None,
        attributed_only: bool = False,
        full_data: bool = False,
        building_block: Union[KendrickBuildingBlock, str, float] = CH2
    ) -> pd.DataFrame:
        """
        Returns a DataFrame with the attributed main results. If mz
        boundaries are provided, limit the masses to the corresponding range.
        The DataFrame can be used by from_dataframe to set up the object.

        Args:
            mz_bounds (list, tuple): min and max mz values.
            attribured_only (bool): if True, only attributed peak are
                exported, default is False.
            full_data (bool): if True, get additional data.
            building_block (KendrickBuildingBlock): Kendrick Building Block
                as formula, a float or a KendrickBuildingBlock, defaut CH2.

        Returns:
            A data frame with columns: mz, intensity, formula,
            attributed, isotopic, error_ppm and SN.

            If full_data is True, additional data are provided: nominal
            mass, Kendrick mass, chem_class, chem_group, DBE.
        """
        # avoid to set up the kendrick block for each peak
        building_block = KendrickBuildingBlock(building_block)
        data = list()
        pids = list()
        for peak in self._peaks:
            if attributed_only and not peak.is_attributed:
                continue
            d = peak.as_dict()
            pids.append(d.pop("pid"))
            props = d.pop("properties")

            if not self.SN_avail:
                d.pop("SN")

            if not peak.is_attributed:
                d["formula"] = ""

            if full_data:
                d['kendrick_mass'] = peak.get_kendrick_mass(building_block)
                if peak.is_attributed:
                    d['chem_class'] = peak.formula.chem_class
                    d['chem_class_mono'] = peak.formula.chem_class_mono
                    d['chem_group'] = peak.formula.chem_group
                    d['chem_group_mono'] = peak.formula.chem_group_mono
                    d['nominal_mass'] = peak.formula.nominal_mass

                    composition_grid = peak.formula.elemental_composition
                    d.update(composition_grid)

                    d["DBE"] = compute_dbe(composition_grid)
                    d["isotopic"] = peak.formula.is_isotopic
                else:
                    d["isotopic"] = False
                    d["DBE"] = np.nan

                for k, v in props.items():
                    d[k] = v

            data.append(d)
        
        data = pd.DataFrame(data, index=pids)
        if full_data:
            data[self.elemental_composition] =\
                data[self.elemental_composition].fillna(0).astype(np.uint16)

            # reorder columns and move elemental composition to the end
            cols = ["mz", "intensity", "formula", "polarity",
                    "error_ppm", "attributed", "attribution_method",
                    "nominal_mass", "kendrick_mass", "DBE",
                    "isotopic", "chem_group", "chem_group_mono", 
                    "chem_class", "chem_class_mono"]
            if self.SN_avail:
                cols.insert(4, "SN")
            cols = cols + sort_elements(self.elemental_composition)
            data = data[cols]

        # select mz boundaries
        if mz_bounds is None:
            return data
        else:
            imin, imax = np.searchsorted(data.mz, mz_bounds)
            return data.iloc[imin: imax]

    @classmethod
    def from_dataframe(cls, df: pd.DataFrame, polarity: int):
        """
        Initialize the class AttributedPeakList from a DataFrame. The
        index of the dataframe will be used as peak id.

        Args:
            df (pd.DataFrame): the pandas dataframe of an attributed
                peaklist. The corresponding dataframe must contain the
                following columns named: mz, intensity, formulas, and
                SN (optional).
            polarity (int): the ionization charge of the AttributedPeakList.
        """
        # check S/N is available
        if "SN" not in df:
            # set SN to None if not in the dataframe
            SN = None
        else:
            SN = df.SN.values

        # check attribution method
        if "attribution_method" not in df:
            df["attribution_method"] = ["unknown"] * len(df)

        # check pid is available
        if "pid" not in df:
            pid = list(df.index.values)
        else:
            pid = list(df.pid)

        return cls(
            mz=df.mz.values,
            intensity=df.intensity.values,
            formulas=df.formula.values,
            polarity=polarity,
            SN=SN,
            attribution_method=df.attribution_method.values,
            pid=pid)

    @classmethod
    def from_file(cls, filepath: Union[str, Path], polarity: int,
                  name: str = None, sort_mz: bool = False, **kwargs):
        """
        Read an attributed peak list file. This file must contain, at
        least the columns of mz, intensity and formulas. The S/N ratio,
        and the Peak_ID they are optional.

        Args:
            filepath (str or Path): the directory of the file.
            polarity (int): the ionization charge.
            name (str, optional): filename.
            sort_mz (bool): If True, m/z values are sorted.
            kwargs of pandas.read_csv()

        Returns:
            An object of the AttributedPeakList class.
        """
        if name is None:
            name = str(filepath).split('/')[-1]
        df = pd.read_csv(filepath, **kwargs)

        # TODO: define the correct variable names.

        # define expected column names
        mass_col_names = ['m/z', 'mz', 'mass formula', 'observed m/z', "mass"]
        intens_col_names = ['intensities', 'observed intens', 'i',
                            'intensity', 'rel. abundance']
        formula_col_names = ['formula', 'molecular formula', 'sum formula']
        sn_col_names = ['sn', 's/n'] # SN ?
        pid_col_names = ['pid', 'peak number']

        # find the mass column index m_idx
        m_idx, = np.nonzero(df.columns.str.lower().isin(mass_col_names))
        if m_idx.size == 0:
            raise ValueError(
                'It was not possible to identify a mz column.')
        else:
            m_idx = m_idx[0]

        # find the intensity column index i_idx
        i_idx, = np.nonzero(df.columns.str.lower().isin(intens_col_names))
        if i_idx.size == 0:
            raise ValueError(
                'It was not possible to identify an intensity column.')
        else:
            i_idx = i_idx[0]

        # find the formula column index m_idx
        f_idx, = np.nonzero(df.columns.str.lower().isin(formula_col_names))
        if f_idx.size == 0:
            raise ValueError(
                'It was not possible to identify a formula column.')
        else:
            f_idx = f_idx[0]

        # fill in data
        mz = df.iloc[:, m_idx].values
        intensity = df.iloc[:, i_idx].values
        formulas = df.iloc[:, f_idx].values

        # find S/N ratio if available
        sn_idx, = np.nonzero(df.columns.str.lower().isin(sn_col_names))
        if sn_idx.size == 0:
            SN = None
        else:
            SN = df.iloc[:, sn_idx[0]].values

        # find pid
        pid_idx, = np.nonzero(df.columns.str.lower().isin(pid_col_names))
        if pid_idx.size == 0:
            pid = np.arange(len(df))
        else:
            pid = df.iloc[:, pid_idx[0]].values

        # attribution method
        if "attribution_method" in df:
            attribution_method = df.loc[:, "attribution_method"]
        else:
            attribution_method = None
        # TODO: recover peak_properties ?
        return cls(
            mz=mz,
            intensity=intensity,
            formulas=formulas,
            polarity=polarity,
            SN=SN,
            pid=pid,
            attribution_method=attribution_method,
            name=name,
            sort_mz=sort_mz,
        )

    def to_viewer(self, filename: str = 'results.csv', attributed_only=True,
                  mz_bounds: tuple[float] = None):
        """
        Exports the results in csv format suitable for PyC2MC viewer. If
        mz boundaries are provided, limit the masses to the corresponding
        range.

        Args:
            filename (str): filename name or path+name.
            mz_bounds (list, tuple): min and max mz values.
            attribured_only (bool): if True, only attributed peak are
                exported, default is False.
        """
        message = "Please use the to_csv() method instead."
        warnings.warn(message, DeprecationWarning, stacklevel=2)
        self.to_csv(filename, attributed_only=attributed_only,
                    mz_bounds=mz_bounds)

    def summary(self):
        """ Prints a summary of the attribution performance"""
        print(self.str_summary())

    def str_summary(self):
        """ Returns a summary text"""
        attr_intensities = sum([p.intensity for p in self if p.is_attributed])
        hits = attr_intensities / self.intensity.sum()
        hits = round(hits * 100, 2)
        attributed = len([p for p in self._peaks if p.is_attributed])
        total = self.size
        attr = round(attributed / total * 100, 2)
        line = '========================' + len(str(total)) * '=' + '==='
        line += '\n Attribution results:'
        line += '\n Total number of peaks: ' + str(total)
        line += '\n Attributed peaks     : ' + str(attributed)
        line += f'\n Mean error (ppm)     : {np.nanmean(self.error_in_ppm):.5f}'
        line += f'\n Std error (ppm)      : {np.nanstd(self.error_in_ppm):.5f}'
        line += '\n Attributed percentage: ' + str(attr) + ' %'
        line += '\n hits percentage      : ' + str(hits) + ' %\n'
        line += '========================' + len(str(total)) * '=' + '==='

        return line

    def get_nohits(self, as_dataframe: bool = False):
        """
        Return a peaklist with the no hits info.
        """
        if as_dataframe:
            df = self.to_dataframe()
            df = df[~self.is_attributed].copy()
            return df
        else:
            peaks = [p for p in self._peaks if not p.is_attributed]
            name = self.name if "no_hits" in self.name else "no_hits_" + self.name
            return PeakList.from_list(peaks, name=name)

    def get_attributed(self):
        """
        Returns an AttributedPeakList object including only the attributed
        peaks.
        """
        peaks = [p for p in self._peaks if p.is_attributed]
        return AttributedPeakList.from_list(peaks, self._polarity)

    def to_csv(self, filename: str = 'results.csv', **kwargs):
        """
        Exports results in csv format.

        Args:
            filename (str): filename name or path+name.
            kwargs (dict): arguments of to_dataframe method.
        """
        df = self.to_dataframe(**kwargs)
        df.to_csv(filename, index=False)

    @property
    def elemental_composition_grid(self) -> pd.DataFrame:
        """ list of all elements present in the formulas. """
        return pd.DataFrame([
            peak.elemental_composition if peak._is_attributed else {}
            for peak in self._peaks]
        ).fillna(0).astype(np.uint16)

    def get_composition_grid(self):
        """ Returns the composition grid as a DataFrame. """
        warnings.warn("Use elemental_composition_grid properties", 
                      DeprecationWarning, stacklevel=2)
        return pd.DataFrame(
            [f.elemental_composition if isinstance(f, Formula) else {}
             for f in self.formulas],
            index=self.pid
        ).fillna(0)

    def get_dbe(self) -> np.array:
        """ Returns an array with the DBE values."""
        return np.array(
            self.elemental_composition_grid.apply(compute_dbe, axis=1)
        )

    def get_classes(self, monoisotopic: bool = False, threshold: float = None,
                    cum_threshold: float = None,
                    sort_chem_class: bool = False,
                    exclude: Union[str, Sequence] = None) -> pd.DataFrame:
        """ Get a data frame with all the classes and their relative abundance
        in percentage of the sum of all intensities. By default the table
        is sorted by chemical abundance.

        Args:
            monoisotopic (bool): If True, only monoisotopic classes are
                included
            threshold (float): minimum value of the relative abundance
                in percent of the classes included.
            cum_treshold (float): cumulative value of the relative abundance
                in percent of the classes included.
            sort_chem_class (bool): If True, the table is sorted by
                chemical group and chemical classes. Default is False 
                the table is sorted by abundance.
            exculde (str, list-like): A sequence of chemical species to be
                excluded from the list of classes. For example if it is needed
                to remove '13C' species, use: exclude = '13C'.

        Returns:
            A pandas DataFrame with the relative abundance in percent
        """

        # get dataframe and filter attributed only peaks
        df = self.to_dataframe(full_data=True, attributed_only=True)

        # filter formula with only most abundant isotopes
        if monoisotopic:
            # df = df[~df.isotopic].copy()
            class_column = "chem_class_mono"
            group_column = "chem_group_mono"
        else:
            class_column = "chem_class"
            group_column = "chem_group"
        
        if exclude is not None:
            if isinstance(exclude, str):
                exclude = [exclude]
            for specie in exclude:
                if not isinstance(specie, str):
                    raise ValueError('The exclude argument must contain strings only.')
                if specie not in self.species:
                    raise ValueError(f'The specie {specie} does not exist in the list '
                                     f'of possible species: {self.species}.')
                
                df = df[~df['chem_group'].str.contains(specie)]

        df = df.groupby([group_column, class_column]).agg({"intensity": "sum"})
        df["rel_abundance"] = df.intensity.values / df.intensity.values.sum()
        df.rel_abundance *= 100
        # df /= df.intensity.sum()
        # df *= 100  # percentage
        df.sort_values(by="intensity", ascending=False, inplace=True)
        df["cumsum"] = df.rel_abundance.cumsum()
        df = df.reset_index()
        if threshold is not None:
            df = df[df.rel_abundance >= threshold]
        elif cum_threshold is not None:
            df = df[df["cumsum"] <= cum_threshold].copy()

        df.columns = ["Chem. Group", "Chem. Class", "Intensity",
                      "Relative abundance (%)", "Cumulative abundance (%)"]
        
        if sort_chem_class:
            # sort the table by chem class using the amount of the element
            # in the chemical class.
            # If the chemical class contains more than one elements, the
            # sorting is done on the elements with highest variation
            int_patt = re.compile(r"(\d+)")
            df = df.sort_values("Chem. Group")
            groups = df["Chem. Group"].unique()

            sort_values = list()
            for group in groups:
                chem_classes = df.loc[df["Chem. Group"] == group, "Chem. Class"].values
                if (len(chem_classes) == 1) or (group == "HC"):
                    numbers = [1]
                elif len(group.split()) == 1:
                    numbers = [int(int_patt.findall(el)[-1]) for el in chem_classes]
                else:
                    elements = np.array([chem_class.split() for chem_class in chem_classes])
                    count = [len(set(elements[:, i])) for i in range(elements.shape[1])]
                    idx = count.index(max(count))
                    numbers = [int(int_patt.findall(el)[-1]) for el in elements[:, idx]]
                
                sort_values.extend(numbers)

            df["sort_values"] = sort_values      
            df = df.sort_values(["Chem. Group", "sort_values"])
            df.drop(columns="sort_values", inplace=True)

        return df

    def get_attribution_stats(
        self, kind: str = "dataframe",
    ) -> Union[pd.DataFrame, dict]:
        """ Group attributed peak by attribution methods and return 
        attribution data about the number of attributed peaks and the amount
        of attributed intensity.

        Args:
            kind (str{'dataframe', 'dict'}): Format of the output table. 
                * 'dataframe' (default): a pandas dataframe table
                * 'dict': a python dict.

        Returns:
            A data frame with two columns, ``attribution`` and ``percentage``.
            The lines are the name of the attribution methods.
            
        * ``n_peaks``: total number of peaks,
        * ``n_attributed_peaks``: number of attributed peaks,
        * ``attributed_percentage``: percentage of attributed peaks,
        * ``hits_intensity``: amount of attributed intensity,
        * ``hits_intensity_percentage``: percentage of attributed intensity,
        * ``mean_error_ppm``: mean error in ppm,
        * ``std_error_ppm``: standard deviation of the error in ppm,
        * ``isotopes``: number of peaks attributed by isotopes identification,
        * ``lowest_error``: number of peaks attributed by lowest error,
        * ``isotopes_percentage``: percentage of peaks attributed by isotopes identification,
        * ``lowest_error_percentage``: percentage of peaks attributed by lowest error,

        """
        # general stats
        hit_intensity = sum([p.intensity for p in self if p.is_attributed])
        n_attributed = len([p for p in self._peaks if p.is_attributed])
        total_intensity = self.intensity.sum()

        data = {
            "n_peaks": self.size,
            "n_attributed_peaks": n_attributed,
            "attributed_percentage": n_attributed / self.size * 100,
            "hits_intensity": hit_intensity,
            "hits_intensity_percentage": hit_intensity / total_intensity * 100,
            "mean_error_ppm": np.nanmean(self.error_in_ppm),
            "std_error_ppm": np.nanstd(self.error_in_ppm),
        }

        # get attribution methods and compute stats
        attribution_method = [p.attribution_method if p.is_attributed else "" 
                              for p in self._peaks]
        dfm = pd.DataFrame({"method": attribution_method, 
                            "intensity": self.intensity})
        dfm = dfm.groupby("method").agg({"intensity": ["count", "sum"]})
        dfm.columns = dfm.columns.droplevel()
        if "" in dfm.index:
            # remove no hit or non attributed methods label
            dfm.drop(index="", inplace=True)
        attribution_method = list(dfm.index)
        data.update(dfm.to_dict()["count"])
    
        dfm.loc["hits"] = [n_attributed, data["hits_intensity"]]
        dfm.loc["no_hits"] = [self.size - n_attributed,
                              total_intensity - data["hits_intensity"]]

        # sort index, to put no hits at the bottom of the plot
        attribution_method = ["hits", "no_hits"] + attribution_method
        dfm = dfm.reindex(attribution_method)
        # compute percentatge
        dfm["percentage"] = dfm["sum"] / total_intensity * 100
        dfm.columns = ["# peaks", "intensity", "percentage"]
        dfm.insert(1, "% peaks", dfm["# peaks"] / self.size * 100)
        dfm.loc["total"] = dfm.loc[["hits", "no_hits"]].sum()

        # return dict or data frame
        if kind == "dict":
            d = dfm.to_dict()
            data.update({f"{k}_percentage": v 
                         for k, v in d["percentage"].items()
                         if k not in ["hits", "no_hits", "total"]})
            return data
        elif kind == "dataframe":
            dfm["# peaks"] = dfm["# peaks"].astype(dtype=pd.Int64Dtype())

            return dfm
        else:
            raise ValueError(
                f"kind '{kind}' not understood. Must be 'dict' or 'dataframe'.")

    def update_peaks(self, peaklist: list, **kwargs):
        """ Update the list of peaks on the basis on the pid using the
        peaks provided in the peaklist arguments.
        
        Args:
            peaklist (list): list of Peak or AttributedPeak objects.
            
        Returns:
            A new attributed peaklist
        """
        pids = [peak.pid for peak in peaklist]

        new_peaks = list()
        for peak in self._peaks:
            if peak.pid in pids:
                idx = pids.index(peak.pid)
                new_peaks.append(peaklist[idx])
            else:
                new_peaks.append(peak)

        return AttributedPeakList.from_list(new_peaks,
                                            polarity=self.polarity,
                                            **kwargs)

    def __repr__(self):
        out = [f"AttributedPeakList ({len(self._peaks)} peaks)"]
        if len(self._peaks) > MAX_PEAKS:
            for peak in self._peaks[:6]:
                out.append(repr(peak))
            out.append("...")
            for peak in self._peaks[-5:]:
                out.append(repr(peak))
        else:
            for peak in self._peaks:
                out.append(repr(peak))
        return "\n".join(out)


def merge_peaklists(list_of_peaklists: Sequence, **kwargs) -> PeakList:
    """
    Merge a list of peak lists.

    Args:
        list_of_peaklists (list-like): a list of peak lists.
        kwargs of the PeakList.

    Returns:
        A merged PeakList.
    """
    
    list_of_peaks = []
    for pl in list_of_peaklists:
        list_of_peaks.extend(pl)
    return PeakList.from_list(list_of_peaks, **kwargs)
