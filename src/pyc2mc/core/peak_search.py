# coding: utf-8

""" This module implements functions in order to select peaks in a given
peak list according to several conditions. The first condition is always
on the relative error respectively to a m/z target value. In consequence,
the first step is always to look for m/z values which lie in a given
error range around the target value. The error range is given in ppm
through the ``lambda_parameter``. 

Then two main algorithms are available: ``'closest'`` or 
``'most_abundant'``. They select the best candidate based on the minimum
error or the highest abundant peak, respectively.

Then, when looking for a series of peaks, the selection can be improved
by considering the mean error of the series of peaks. The idea is to obtain
peaks with the lowest standard deviation of the error.
The algorithm computes the mean error and the standard error deviation.
Peaks are dropped if their error does not lie in an interval 
centered around the median of the error and a width of 4 times 
the standard deviation of the error. This leads to a minimization of the
standard deviation error but this increases the calculation time.

closest error algorithm:
    If several m/z values lie in the interval defined from the 
    ``lambda_parameter`` this algorithm selects the m/z value with the
    lowest error.

closest error algorithm corrected:
    The closest algorithm is applied first and then peak selection is
    improved by comparing the error to the mean error and the standard
    deviation of the error.

most abundant algorithm:
    If several m/z values lie in the interval defined from the 
    ``lambda_parameter`` this algorithm selects the m/z value with the
    highest S/N value or intensity if S/N is not available.
    
most abundant algorithm corrected:
    Once m/z values have been selected using the most abundant algorithm
    the peak selection is improved by comparing the error to the mean error
    and the standard deviation of the error.

"""

from enum import unique, Enum

import numpy as np
from numpy.typing import ArrayLike

from pyc2mc.core.peak import PeakList

__all__ = ["SearchingMode", "apply_mean_error_correction", "select_closest_mz",
           "select_closest_mz_corrected", "select_most_abundant",
           "select_most_abundant_corrected"]


@unique
class SearchingMode(str, Enum):
    """ This class defines the possible values for Searching Mode
    algorithms. There are two main algorithms: ``'closest'`` or 
    ``'most_abundant'``. Both can be completed by a mean error correction
    which aims to minimize the standard deviation of the error. This
    correction is not relevant for single peak search.

    Attributes:
        closest: Select the m/z value the closest from the target
        most_abundant: If several m/z values lie in an error interval
            the peak with the highest abundance (S/N or intensity) is
            selected.
        most_abundant_corrected: Same as most_abundant. After selection
            of a list of m/z values, compute the median error and drop
            points which lie far from the median.

    """
    closest = 'closest'
    closest_corrected = 'closest_corrected'
    most_abundant = 'most_abundant'
    most_abundant_corrected = "most_abundant_corrected"

    def __call__(self, *args, **kwargs) -> tuple:
        if self.value == "closest":
            return select_closest_mz(*args, **kwargs)
        elif self.value == "closest_corrected":
            return select_closest_mz_corrected(*args, **kwargs)
        elif self.value == "most_abundant":
            return select_most_abundant(*args, **kwargs)
        elif self.value == "most_abundant_corrected":
            return select_most_abundant_corrected(*args, **kwargs)


def select_closest_mz(
    mz_target: ArrayLike,
    peaklist: PeakList,
    lambda_parameter: float = 10,
    **kwargs,
) -> tuple:
    """ Select the peak with a mz value in a range around the mz_target
    value defined from the lambda_parameter. Then among possible peak,
    the one with the lowest error is returned.

    Args:
        mz_target (float): the target mz value.
        peaklist (PeakList): list of peak into which peaks is selected
        lambda_parameter (float): error range in ppm.

    Returns:
        A list of tuple such as (i, peak), i being the index in the input
        mz_target of the m/z value associated to the peak.
    """
    mz = peaklist.mz
    # compute error
    err_ppm = np.abs(np.subtract.outer(mz, mz_target)) / mz_target * 1e6
    if err_ppm.ndim == 1:
        err_ppm = err_ppm[:, np.newaxis]

    # select min error for each mz_target
    idx_min = err_ppm.argmin(axis=0)
    err_ppm = err_ppm[idx_min, np.arange(len(idx_min))]

    # select peak if error is lower than lambda
    peaks = [peaklist[idx] for i, idx in enumerate(idx_min)
             if err_ppm[i] < lambda_parameter]
    idx = [i for i in range(len(idx_min)) if err_ppm[i] < lambda_parameter]

    return idx, peaks


def select_most_abundant(
    mz_target: ArrayLike,
    peaklist: PeakList,
    lambda_parameter: float = 10,
    use_SN: bool = True,
    **kwargs,
) -> tuple:
    """ Select the peak with a mz value in a range around the mz_target
    value defined from the lambda_parameter. Then among possible peak,
    the one with the highest intensity is returned.

    Args:
        mz_target (float): the target mz value.
        peaklist (PeakList): list of peak into which peaks is selected
        lambda_parameter (float): error range in ppm.
        use_SN (bool): If True, default, use S/N else, use intensity.

    Returns:
        A list of tuple such as (i, peak), i being the index in the input
        mz_target of the m/z value associated to the peak.

    """
    mz = peaklist.mz
    if use_SN and peaklist.SN_avail:
        abund = peaklist.SN
    else:
        abund = peaklist.intensity

    # compute error
    err_ppm = np.abs(np.subtract.outer(mz, mz_target)) / mz_target * 1e6
    if err_ppm.ndim == 1:
        err_ppm = err_ppm[:, np.newaxis]

    peaks = list()
    idx_target = list()
    for i_col in range(err_ppm.shape[1]):
        idx_min, = np.where(err_ppm[:, i_col] < lambda_parameter)
        if idx_min.size == 1:
            peaks.append(peaklist[idx_min[0]])
            idx_target.append(i_col)
        elif idx_min.size > 1:
            # take the highest abundance (SN or intensity)
            idx = np.nanargmax(abund[idx_min])
            peaks.append(peaklist[idx_min[idx]])
            idx_target.append(i_col)

    return idx_target, peaks


def apply_mean_error_correction(
    idx_target: ArrayLike,
    selected_peaks: list,
    mz_target: ArrayLike,
    peaklist: PeakList,
    searching_mode: SearchingMode,
    lambda_parameter: float = 10,
    use_SN: bool = True,
) -> tuple:
    """ From the selected peak, compute the mean error and drop peaks
    with errors which do not lie in an interval of two times the
    standard deviation of the error. This help to remove outliers and
    to obtain peaks with the most possible consistent errors.

    Args:
        idx_target (ArrayLike): Array of the indexes in mz_target that are
            linked to the selected peaks
        selected_peaks (list): List of selected peak that match mz_target
            values.
        mz_target (ArrayLike): m/z target values for which we look for peaks
            in the peaklist.
        peaklist (PeakList): Peak list in which we look for peak that match
            the target.
        searching_mode (SearchingMode): Searching mode used to identified
            peaks.
        lambda_parameter (float): error range in ppm.
        use_SN (bool): If True (default), S/N is used, else intensity is used.
        
    Returns:
        A list of tuple such as (i, peak), i being the index in the input
        mz_target of the m/z value associated to the peak.
     
    """
    # compute error averages
    err = [(peak.mz - mz_target[i]) / mz_target[i] * 1e6
           for i, peak in zip(idx_target, selected_peaks)]
    median_err = np.median(err)
    stddev_err = np.std(err)

    # look for outlier
    outliers_idx, = np.where(
        (err > median_err + 2 * stddev_err) | (err < median_err - 2 * stddev_err)
    )

    # detect outlier and try to make a correction
    new_peaks = list()
    new_indexes = list()
    for idx in outliers_idx:
        mzvalue = mz_target[idx_target[idx]]
        corrected_mz = mzvalue + mzvalue * median_err / 1e6
        
        new_idx, new_peak = searching_mode(
            corrected_mz, peaklist, lambda_parameter=2 * stddev_err,
            use_SN=use_SN)

        if len(new_idx) == 1:
            new_peaks.append(new_peak[0])
            new_indexes.append(idx_target[idx])

    # remove outliers from the end of the list
    # outliers_idx = outliers_idx
    for idx in outliers_idx[::-1]:
        idx_target.pop(idx)
        selected_peaks.pop(idx)

    # update the list with new selected peak
    selected_peaks.extend(new_peaks)
    idx_target.extend(new_indexes)

    return idx_target, selected_peaks


def select_most_abundant_corrected(
    mz_target: ArrayLike,
    peaklist: PeakList,
    lambda_parameter: float = 10,
    use_SN: bool = True,
    **kwargs,
) -> tuple:
    """ Select the peak with a mz value in a range around the mz_target
    value defined from the lambda_parameter. Then among possible peak,
    the one with the highest intensity is returned. Finally, a 
    correction is applied to drop peak with a larger error and trying to
    select peak which minimize the standard deviation of the error.

    Args:
        mz_target (float): the target mz value.
        peaklist (PeakList): list of peak into which peaks is selected
        lambda_parameter (float): error range in ppm.
        use_SN (bool): If True, default, use S/N else, use intensity.

    Returns:
        A list of tuple such as (i, peak), i being the index in the input
        mz_target of the m/z value associated to the peak.

    """
    idx_target, selected_peaks = select_most_abundant(mz_target, peaklist,
                                                      lambda_parameter, use_SN)

    idx_target, selected_peaks = apply_mean_error_correction(
        idx_target, selected_peaks, mz_target, peaklist,
        SearchingMode.most_abundant, lambda_parameter, use_SN
    )

    return idx_target, selected_peaks


def select_closest_mz_corrected(
    mz_target: ArrayLike,
    peaklist: PeakList,
    lambda_parameter: float = 10,
    **kwargs,
) -> tuple:
    """ Select the peak with a mz value in a range around the mz_target
    value defined from the lambda_parameter. Then among possible peak,
    the one with the highest intensity is returned. Finally, a 
    correction is applied to drop peak with a larger error and trying to
    select peak which minimize the standard deviation of the error.

    Args:
        mz_target (float): the target mz value.
        peaklist (PeakList): list of peak into which peaks is selected
        lambda_parameter (float): error range in ppm.
        use_SN (bool): If True, default, use S/N else, use intensity.

    Returns:
        A list of tuple such as (i, peak), i being the index in the input
        mz_target of the m/z value associated to the peak.

    """
    idx_target, selected_peaks = select_closest_mz(mz_target, peaklist,
                                                   lambda_parameter)

    idx_target, selected_peaks = apply_mean_error_correction(
        idx_target, selected_peaks, mz_target, peaklist,
        SearchingMode.closest, lambda_parameter
    )

    return idx_target, selected_peaks
