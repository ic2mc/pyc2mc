# coding: utf-8

from typing import Union

from pyc2mc.core.peak import PeakCollection
from pyc2mc.core.ft_icr_signal import FT_ICR_Signal
from pyc2mc.core.nddata import PeakListCollection

# Define a union type for name (string or integer)
PyC2MCPeakObject = Union[PeakCollection, PeakListCollection]
PyC2MCProfileObject = Union[FT_ICR_Signal]
