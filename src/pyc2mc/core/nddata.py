#!/usr/bin/env python
# coding: utf-8

"""
This module provides input/output tools for experimental data.
"""


from collections import abc
from pathlib import Path
from typing import Sequence, Union

import numpy as np
from numpy.typing import ArrayLike

from pyc2mc.io.nddata import read_mzml, read_MZxml
from pyc2mc.io.peaklist import read_peaklist, get_file_format
from pyc2mc.utils import get_file_index, get_file_list
from pyc2mc.core.peak import PeakList


__all__ = ["PeakListCollection"]

MAX_LINES = 50


class PeakListCollection(abc.Sequence):
    """ This class represents a collections of PeakList object. This
    corresponds for example to the data obtained from an Hyphenated Mass
    Spectrometry technique such as LC/MS or GC/MS techniques. This class
    contains the raw data, obtained from the experiments. It aims to
    provides based methods for filtering and curating data before any
    processing.
    """

    def __init__(self, peaklists: Sequence[PeakList] = None,
                 name: str = None, scan_ids: Sequence[int] = None,
                 sort_mz: bool = False):
        """ Instance method for the PeakListCollection class.

        Args:
            peaklists (list of peaklist): List of PeakList objects, if this
                infotmation is available, it is not recommended to put also
                the scans_values.
            name (str): The file name or the sample name.
            scans_ids (list-like): list or array of indexes of scans if available,
                else, scans_ids is generated from 0 to N.
            sort_mz (bool): if True, mz values are sorted.

        """
        self._peaklists = peaklists

        self._scan_lengths = [len(pl) for pl in self._peaklists]
        self._max_length = np.max(self._scan_lengths)
        self._min_length = np.min(self._scan_lengths)
        self._SN_avail = all([pl.SN_avail for pl in self._peaklists])
        self.name = name

        if scan_ids is None:
            self._scan_ids = np.arange(len(self._peaklists))
        else:
            if len(scan_ids) == len(self._peaklists):
                self._scan_ids = np.array(scan_ids, dtype=np.int32)
            else:
                raise ValueError(
                    "Length of scan ids is not consistent with scan "
                    "values. "
                    f"\n# scan_ids = {len(scan_ids)} "
                    f"\n# peaklist = {len(self._peaklists)}.")

        # set up a rectangular mz and intensity matrices filling
        # missing values with np.nan at the end of the vectors
        # TODO: ?? check if this is really useful because it might not be 
        # consistent with the data in the list of peaklist
        # GSV 23/07/26 => use this for chem class chromatograms
        shape = (len(self._peaklists), self._max_length)
        self._mz = np.full(shape, np.nan)
        self._intensity = np.full(shape, np.nan)
        self._SN = np.full(shape, np.nan)
        for ip, pl in enumerate(self._peaklists):
            if len(pl) == 0:
                # could be the case if you perform mz filtering and the
                # returned peaklist is empty
                continue
            values = pl.values
            self._mz[ip, :len(values)] = values[:, 0]
            self._intensity[ip, :len(values)] = values[:, 1]
            if pl.SN_avail:
                self._SN[ip, :len(values)] = values[:, 2]

        self._values = np.concatenate(
            (self._mz[np.newaxis], 
             self._intensity[np.newaxis], 
             self._SN[np.newaxis]
             ),
            axis=0
        )

    @property
    def peaklists(self):
        """ List of PeakList object for each scan """
        return self._peaklists

    @property
    def mz(self):
        """ 2D array of m/z values for each scan. If scans do not have the
        same length they are completed with nan values. """
        return self._mz

    @property
    def intensity(self):
        """ 2D array of intensity values for each scan. If scans do not have the
        same length they are completed with nan values. """
        return self._intensity

    @property
    def SN(self):
        """ 2D array of S/N values for each scan. If scans do not have the
        same length they are completed with nan values. """
        return self._SN
    
    @property
    def values(self):
        """ Array of m/z, intensity and S/N values for each scan and each
        peaks. If scans do not have the same length they are completed 
        with nan values. The shape of the array is (3, #scan, #peaks). """
        return self._values

    @property
    def SN_avail(self) -> bool:
        """ True if signal to noise ratio is available for all scans """
        return self._SN_avail

    @property
    def scan_ids(self):
        """ identifiers of scans to get back information after
        filtering or slicing the data. """
        return self._scan_ids

    @property
    def scan_lengths(self):
        """ lengths (number of peaks) of each scan """
        return self._scan_lengths

    @property
    def max_length(self):
        """ Maximum length of the scans on the mz dimension """
        return self._max_length

    @property
    def min_length(self):
        """ Minimum length of the scans on the mz dimension """
        return self._min_length

    @classmethod
    def from_MZxml(cls, path, **kwargs):
        """ Read scans data from a MZxml file. """
        path = Path(path)
        peaklists = read_MZxml(path, **kwargs)
        return cls(peaklists=peaklists, name=path.name)

    @classmethod
    def from_mzml(cls, path, **kwargs):
        """ Read scans data from a MZxml file. """
        peakLists = read_mzml(path, **kwargs)
        return cls(peaklists=peakLists, name=path)

    @classmethod
    def from_filenames(
        cls,
        filenames: Sequence[str],
        extension: str = None,
        verbose: bool = True,
        sorting: bool = True,
        sort_mz: bool = True,
        name: str = None,
        **kwargs
    ):
        """ Import the data from a list of files and make a list of arrays of
        mz and intensities. The code assume the following:

        * one file corresponds to one m/z scan
        * if sorting is True, all files have an index such as
          `basename_pathXext`.

        At the end, mz and intensities are sorted according to the
        mz if sort_mz is True.

        Args:
            filenames (Sequence): Sequence of file names or paths
            extension (str): file extension, default to 'pks'
            verbose (bool): If True, print some information
            sorting (bool): if true, sort files by index. Warning: files
                must have an index number like: filename-00.ext, where
                00 is the index.
            sort_mz (bool): if True, for each scan, m/z values are sorted
            name (str): name of the sample
            kwargs are arguments of the read_peaklist function.

        Returns:
            Returns the PeakListCollection object including all the
            data.
        """

        # check files
        for file in filenames:
            file = Path(file)
            if not file.is_file():
                raise FileNotFoundError(f"File {file} does not exist.")
            
            if extension is None:
                extension = get_file_format(file.suffix)
                if extension is None:
                    raise NotImplementedError(
                        f"Error reading file {file}. Read file "
                        f"with extension '{extension}' is not yet available.")

        if sorting:
            filenames = sorted(filenames, key=get_file_index)

        list_of_scans = list()
        for filename in filenames:
            if verbose:
                print(f"Reading: {filename}")
            pl = read_peaklist(filename, fmt=extension, sort_mz=sort_mz, **kwargs)
                
            list_of_scans.append(pl)

        if name is None:
            name = Path(filenames[0]).stem

        return cls(peaklists=list_of_scans, name=name)

    @classmethod
    def from_directory(
        cls,
        directory_path: str,
        extension: str = None,
        verbose: bool = True,
        sorting: bool = True,
        sort_mz: bool = True,
        name: str = None,
        **kwargs
    ):
        """ Import the data from scan files and make a list of arrays of
        mz and intensities. The code assume the following:

        * one file corresponds to one m/z scan
        * all files have the same basename and extension with an index
          such as `basename_pathXext` so all scans should be a continuum.

        At the end, mz and intensities are sorted according to the
        mz if sort_mz is True (default).

        Args:
            directory_path (str): directory path of the files
            extension (str): file extension.
            verbose (bool): If True, print some information
            sorting (bool): If true, sort files by index. Warning: files
                must have an index number like: filename-00.asc, where
                00 is the index.
            sort_mz (bool): If True (default), each scan are sorted along mz.
            name (str): sample name
            kwargs are transfer to the read_peaklist function.

        Returns:
            Returns the PeakListCollection object including all the
            data.
        """
        directory_path = Path(directory_path)
        
        # get file names
        filenames = get_file_list(directory_path, extension, sorting)

        if name is None:
            name = directory_path.name

        return cls.from_filenames(filenames, extension=extension,
                                  verbose=verbose, sorting=sorting,
                                  sort_mz=sort_mz, name=name, **kwargs)

    def delimit(self, mz_bounds: Union[list, tuple]):
        """
        Delimit all scans between a [mz_min, mz_max] boundary constrain.

        Args:
            bounds (list, tuple): a 2 element list or tuple with the
                minimum m/z value and the maximum m/z value, min_mz and
                max_mz respectively.

        Returns:
            A PeakListCollection object derived from the original one,
            limited on the set bounds.
        """
        peaklists = [pl.delimit(mz_bounds) for pl in self._peaklists]
        return self.__class__(
            peaklists=peaklists, name=self.name, scan_ids=self.scan_ids)

    def filter_intensity(self, threshold: float):
        """
        Filter data, by removing for each scan, the peaks with an intensity
        lower than the threshold.

        Args:
            threshold (float): remove peaks with an intensity lower than
            the threshold.

        Returns:
            A PeakListCollection object derived from the original one,
            limited to peak with intensity higer than threshold.
        """
        intens_max = self._intensity.max()
        peaklists = [peaklist.delimit(intensity_bounds=(threshold, intens_max + 1))
                     for peaklist in self._peaklists]

        return self.__class__(
            peaklists=peaklists, name=self.name, scan_ids=self.scan_ids)

    def get_min_max_intensity(self) -> tuple:
        """ Returns the minimum and maximum values of intensity over all scans
        as a tuple (min, max)."""
        min_I = np.nanmin(self.intensity)
        max_I = np.nanmax(self.intensity)
        return min_I, max_I

    def get_min_max_mz(self) -> tuple:
        """ Returns the minimum and maximum values of m/z values over all scans
        as a tuple (min, max)."""
        min_m = np.nanmin(self.mz)
        max_m = np.nanmax(self.mz)
        return min_m, max_m
    
    def get_scans_mz_bounds(self) -> list[tuple]:
        """ Return a list of mz bounds for each scan """
        mz_bounds = list()
        for pl in self._peaklists:
            mz_bounds.append(pl.get_min_max_mz())
        return mz_bounds

    def get_scans_intensity_bounds(self) -> list[tuple]:
        """ Return a list of mz bounds for each scan """
        intens_bounds = list()
        for pl in self._peaklists:
            intens_bounds.append(pl.get_min_max_intensity())
        return intens_bounds

    def to_csv(self, path: str = '.'):
        """
        Save the CSV files for each PeakList in the PeakListCollection.

        Args:
            folder_name (string): this is an optional argument, if it is define,
                it will create a new folder as named.
        """

        path = Path(path)
        
        if not path.exists():
            path.mkdir(parents=True, exist_ok=True)

        for i, pl in enumerate(self._peaklists):
            if pl.name.strip() == "":
                name = f"scan_{i:04d}"
            else:
                name = Path(pl.name).stem

            filename = f"{name}.csv"
            out_path = path / filename
            pl.to_csv(out_path)

    def __iter__(self):
        # implementing iter avoid tests in getitem and make it faster
        return iter(self._peaklists)

    def __getitem__(self, item: Union[int, slice, ArrayLike]):
        """ Return the required scan if item is an integer or a new
        class object if item is a slice object corresponding to the
        provided slice. """
        if isinstance(item, slice):
            return PeakListCollection(
                peaklists=self._peaklists[item],
                scan_ids=self.scan_ids[item],
                name=self.name)
        elif isinstance(item, (int, np.integer)):
            return self._peaklists[item]
        else:
            scans = [self._peaklists[idx] for idx in item]
            return PeakListCollection(
                peaklists=scans, scan_ids=item,
                name=self.name,
            )

    def __len__(self):
        return len(self._peaklists)

    def __repr__(self):
        out = [f"PeakListCollection ({len(self._peaklists)} scans)"]
        if len(self._peaklists) > MAX_LINES:
            for i in range(6):
                min_mz, max_mz = self._peaklists[i].get_min_max_mz()
                out.append(
                    f"Scan {self._scan_ids[i]}: {self._scan_lengths[i]} peaks"
                    f" [{min_mz:.2f} ; {max_mz:.2f}]"
                )
            out.append("...")
            for i in range(5):
                min_mz, max_mz = self._peaklists[i].get_min_max_mz()
                idx = len(self._peaklists) - 5 + i
                out.append(
                    f"Scan {self._scan_ids[idx]}: {self._scan_lengths[idx]} peaks"
                    f" [{min_mz:.2f} ; {max_mz:.2f}]"
                )
        else:
            for i in range(len(self._peaklists)):
                min_mz, max_mz = self._peaklists[i].get_min_max_mz()
                out.append(
                    f"Scan {self._scan_ids[i]}: {self._scan_lengths[i]}"
                    f" [{min_mz:.2f} ; {max_mz:.2f}]"
                )
        return "\n".join(out)
