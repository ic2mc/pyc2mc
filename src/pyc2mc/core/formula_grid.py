#!/usb/bin/env python
# -*- coding: utf-8 -*-

""" This module defines a class that aims to build a formula grid
depending on a list of elements, range and isotopic data. """

from enum import Enum, unique
from typing import Sequence, Union, IO
from collections import abc
from dataclasses import dataclass, field
import tomlkit
try:
    # python 3.11
    import tomllib
except ModuleNotFoundError:
    # python < 3.11
    import tomli as tomllib

import pandas as pd
import numpy as np
from numpy.typing import ArrayLike, DTypeLike

from pyc2mc.controls.formula_grid import FormulaGridControls
from pyc2mc.core.isotopes import (Element, Isotope, ELECTRON_MASS,
                                  all_exact_masses, all_nominal_masses)
from pyc2mc.core.formula import (Formula, Composition, get_formula,
                                 get_str_formula, sort_elements)

__all__ = ["FormulaGrid"]


def default_isotopes(elements):
    """
    Returns a default dict of isotopes in the form:

        {(6, 13, 1): 1, (6, 13, 2): 2, ...}

    Args:
        elements(FormulaGrid.elements): is a list of elements of the
            pyc2mc.core.isotopes.Element class.
    """
    isotopes = {}
    for el in elements:
        if el.symbol == "C":
            isotopes[6, 13] = 1
            isotopes[6, 13] = 2
        elif el.symbol == "N":
            isotopes[7, 15] = 1
        elif el.symbol == "O":
            isotopes[8, 18] = 1
        elif el.symbol == "S":
            isotopes[16, 34] = 1

    return isotopes


@unique
class Polarity(Enum):
    """ This class defines the two possible values for the polarity """
    positive = "positive"
    negative = "negative"

    @property
    def charge(self):
        """ Return the z value depending on the ionization polarity """
        if self.value == "positive":
            return 1
        elif self.value == "negative":
            return -1


@dataclass
class PatternGrid:
    """ This class represents a formula grid which is limited to only
    one set of isotopes or let say one isotopic pattern. For example
    formula with on 13C or one 13C and one 15N.

    The formula grid is stored in the ``values`` attributed in which each
    line is associated to one formula. The ``values`` attribute is an 
    array of unsigned integers.
    
    Attributes:
        values (Array): Array of integers which define the composition. The
            order of the columns is linked to the ``species`` attributed.
        species (list of str): Chemical species (mono-isotope and isotopes) in
            the same order as the columns of ``values``.
        polarity (int): charge of the molecules
        mz_shift (float): m/z shift to be added from a monoisotopic formula
            to get the m/z value of the formula including the isotopes.
        calculated_mz (Array): Calculated m/z values of the formulas
        nominal_mass (Array): Calculated nominal mass of the formulas using
            the mass number of each species.
        is_isotopic (bool): If True, the grid contains formulas with isotopes.
        exact_masses (dict): dict of species and their exact masses to speed
            up calculations of m/z values of formulas.
        nominal_masses (dict): dict of species and their nominal masses to speed
            up calculations of the nominal masses of formulas.
        integer_part (Array): integer part (floor) of the calculated m/z values.
        isotopes (list): List of ``Isotope`` object associated to the species

    """

    values: ArrayLike
    species: list[str]
    polarity: int
    key: tuple
    mz_shift: float = 0.0
    calculated_mz: ArrayLike = None
    nominal_mass: ArrayLike = None
    isotopes: list = None

    exact_masses: dict = field(init=False, default_factory=dict)
    nominal_masses: dict = field(init=False, default_factory=dict)
    integer_part: ArrayLike = field(init=False)

    def __post_init__(self):

        self.exact_masses = all_exact_masses(self.species)
        self.nominal_masses = all_nominal_masses(self.species)

        if self.calculated_mz is None:
            exact_masses = np.array(
                [self.exact_masses[k] for k in self.species])
            z = np.abs(self.polarity)
            m_e = self.polarity * ELECTRON_MASS
            self.calculated_mz = self.values @ exact_masses - m_e
            if z != 0:
                self.calculated_mz = self.calculated_mz / z

        if self.nominal_mass is None:
            nominal_masses = np.array(
                [self.nominal_masses[k] for k in self.species],
                dtype=np.uint16,
            )
            self.nominal_mass = self.values @ nominal_masses

        self.integer_part = np.floor(self.calculated_mz).astype(np.uint16)

    @property
    def is_isotopic(self) -> bool:
        """ True if the grid contains isotopes. """
        return self.isotopes is not None
    
    @property
    def isotope_numbers(self) -> list:
        """ Return the number of isotopes """
        n_times = list()
        i = 0
        while i < len(self.key):
            n = self.key[i + 2]
            i += 3
            n_times.append(n)
        return n_times

    def to_dataframe(self, with_formula: bool = True) -> pd.DataFrame:
        """ Return a data frame containing the data of the formula grid.
        In particular, the data frame contains the species composition
        the calculated m/z value, the calculated nominal mass and the
        integer part of the calculated m/z value. The ``isotopic`` column
        is True if the formula contains an isotope.

        Args:
            with_formula (bool): If True, a column with a string representation
                of the formula is included in the data frame. This may slow
                down the function.

        Returns:
            A pandas DataFrame with the data.

        """

        df = pd.DataFrame(self.values, columns=self.species)
        if with_formula:
            df["formula"] = df.apply(get_str_formula, axis=1)
        df["calculated_mz"] = self.calculated_mz
        df["nominal_mass"] = self.nominal_mass
        df["integer_part"] = np.floor(self.calculated_mz).astype(np.uint16)
        df["isotopic"] = self.is_isotopic

        return df


class FormulaGrid(abc.Mapping):
    """
    FormulaGrid class for creating all possible compositions from a set of
    chemical elements and isotopes. This class is a key, value mapping with values
    being PatternGrid objects and keys an identifier of the isotopic pattern.

    .. attribute: limits

        A list of tuples with the min-max limits of each elements to define
        the compositions. Must be in the same order as the element list.

    .. attribute: H_C_bounds

        Boundary values of the H/C ratio.

    .. attribute: dbe_bounds

        Boundary values of the Double Bond Equivalent.

    .. attribute: mass_bounds

        Boundary values of the nominal mass.

    .. attribute: element_ratios

        Dictionary of constraints on elements pairs.
        This is an optional constrain to limit the formula grid. As an
        example, here is the dictionnary to constraint the O/C ratio between
        0 and 1.2 ``{("O", "C"): [0, 1.2]}``

    .. attribute: isotopes

        contains the list of isotope objects presents in the formula grid.

    .. attribute: combine_isotopes

        This attribute contains the information of a combined isotopic formula
        grid in a form of a dictionary.

    .. attribute: exact_masses

        A dict whose keys are the element symbol and values are the exact
        mass. The dict contains both element and isotopes. For element,
        the most abundant isotope is considered.

    """

    def __init__(
        self,
        polarity: int,
        elements: list = ['C', 'H', "O", "N", "S"],
        limits: Sequence[Sequence[int]] = [
            [0, 100], [0, 200], [0, 6], [0, 6], [0, 1]],
        H_C_bounds: Union[tuple, list] = (0.2, 3),
        dbe_bounds: Union[tuple, list] = None,  # (0, 50), TODO ???
        mass_bounds: Union[tuple, list] = (0, 1500),
        element_ratios: dict = None,
        isotopes: dict = None,
        combine_isotopes: Sequence[dict] = None,
        lewis_filter: bool = False,
        name: str = "FormulaGrid_0",
    ):
        """
        Define the elements and the boundaries to create the sum formula
        matrix.

        Args:
            polarity (int): Ionization charge of the formula grid.
            elements (list): a list of the elements such as ``["C", "H", "N"]``
            limits (list): a list of tuples with min, max boundaries of each
                element.
            H_C_bounds (list): [min, max] lowest and highest values of the
                hydrogen to carbon ratio.
            dbe_bounds (list): [min, max] lowest and highest values of the
                double bon equivalent limits.
            mass_bounds (list): [min, max] lowest and highest values of the m/z
                range.
            element_ratios (dict): ratio between elements as a dictionary
                given the pair of element and the ratio bounds. For example
                ``{("O", "C"): (0, 0.8)}`` constraints the O/C ration
                in the ``[0, 0.8]`` interval.
            isotopes (dict): dict of isotopes to be included in the grid. The
                keys of the dict are the couple (Z, A) and the values are the
                maximum number of isotopes to be included in the grid. For
                example, for at most two 13C and one 15N:
                ``{(6, 13): 2, (7, 15): 1, ...}``
            combine_isotopes (list-like): combined isotopes to be included in
                the grid. If defined, the sum formula grid will create an extra
                grid that contains an exceptional combination of two isotopes.
                For example a grid that contains 10B1 and 13C1 species.
                The keys of the dict are the couple (Z, A) and the values are
                the maximum number of isotopes to be included in the grid. For
                example, for at most one 10B and two 13C:
                ``{(5, 10): 1, (6, 13): 2}``
            name (str): Name of the formula grid
            lewis_filter (bool): If True apply the Lewis filtering, default
                False.

        """
        # sort elements according to ELEMENTS_ORDER
        elements, idx = sort_elements(elements, return_index=True)

        self._elements = [Element.from_string(el) for el in elements]
        self._polarity = int(polarity)
        self.limits = [limits[i] for i in idx]
        self.H_C_bounds = H_C_bounds
        self.dbe_bounds = dbe_bounds
        self.mass_bounds = mass_bounds
        self.element_ratios = element_ratios\
            if element_ratios is not None else dict()
        self.name = name
        self.lewis_filter = lewis_filter

        # a dict containing the exact masses for further compute the exact
        # mass of formula more efficiently
        self.exact_masses = {el.symbol: el.exact_mass_most_abundant
                             for el in self.elements}

        # a dict to get the mapping between element symbols and column index
        self.species_mapping = {
            el: i for i, el in enumerate(self.element_symbols)}

        # dtype of the arrays according to the max number of elements
        if max([lmax for _, lmax in self.limits]) > 255:
            # this case includes up to 65_535 atoms
            self.dtype = np.uint16
        else:
            # up to 255 atoms
            self.dtype = np.uint8

        # isotopic attributes
        self._is_isotopic = False
        self._isotopes = list()
        self._input_isotopes = {} if isotopes is None else isotopes.copy()
        self.combine_isotopes = list()
        if isotopes is not None:
            self._setup_isotopes(isotopes, combine_isotopes)

            # complete mapping with isotopes
            self.species_mapping.update({
                symbol: i 
                for i, symbol in enumerate(self.isotope_symbols, len(self.elements))
            })

        # setup no isotopic grid including only most abundant isotopes
        grid0 = self._setup_no_isotopic_grid()
        self._grids = {(0, 0, 0): PatternGrid(
            grid0, self.species, self._polarity, (0, 0, 0))}

        # manage isotopes
        if self.is_isotopic:
            self._grids = self._setup_isotopic_grids(self._grids, isotopes)

        # make grouped grids
        self._grouped_grids = pd.concat(
            [grid.to_dataframe(with_formula=False)
             for grid in self._grids.values()],
            ignore_index=True,
        )
        self._grouped_grids = self._grouped_grids.groupby('integer_part')
        self._integer_parts = set(self._grouped_grids.groups.keys())

    @property
    def elements(self):
        """ Contains the list of elements as Element objects. """
        return self._elements

    @property
    def element_symbols(self):
        """ The list of element symbols as strings. """
        return [el.symbol for el in self.elements]

    @property
    def isotopes(self):
        """ List of isotopes as isotopes objects. """
        return self._isotopes

    @property
    def isotope_symbols(self):
        """ The list of isotope symbols as strings. """
        return [iso.symbol for iso in self.isotopes]

    @property
    def species(self):
        """ The list of all species as strings including both elements
        and isotopes """
        return self.element_symbols + self.isotope_symbols

    @property
    def polarity(self):
        """ Polarity is the charge of the molecule in the formula grid. """
        return self._polarity

    @property
    def is_isotopic(self):
        """ True if the formula grid includes several isotopes of an
        element. """
        return self._is_isotopic

    @property
    def no_isotope_grid(self):
        """ formula grid with only most abundant isotope """
        return self._grids[(0, 0, 0)]

    @property
    def integer_parts(self):
        """ return the set of the integer part of the exact masses of the
        formula in the formula grid. The integer part of the exact mass
        can differ from the real nominal mass of the formula when you
        reach high masses. """
        return self._integer_parts

    def _setup_isotopes(self, isotopes: dict, combine_isotopes: list):
        """ Performs check on arguments and fill in isotopic information """

        if isotopes is not None and len(isotopes) != 0:
            self._is_isotopic = True
            self._isotopes = list()
            self.combine_isotopes = combine_isotopes\
                if combine_isotopes is not None else list()

        # check isotopic information and sort isotopes according to A
        # from now, isotopes are always in ascending A values
        isotopes_list = [(Z, A) for Z, A in isotopes]
        isotopes_list = sorted(isotopes_list, key=lambda x: x[1])
        for Z, A in isotopes_list:
            isotope = Isotope(Z, A)
            self._isotopes.append(isotope)

            # check if the element is included in the formula grid and
            # how much times
            try:
                i_el = self.elements.index(isotope.element)
            except ValueError as error:
                print(error)
                raise ValueError(
                    f"Isotope {isotope}{(Z, A)} is requested but the "
                    "corresponding element was not included in the "
                    f"formula grid\n{self.elements}")

            _, max_bound = self.limits[i_el]
            if isotopes[(Z, A)] > max_bound:
                raise ValueError(
                    f"Isotope {isotope}{(Z, A)} is requested with at most "
                    f"{isotopes[(Z, A)]} times this isotope in the "
                    "formulas but the most aboundant isotope is present "
                    f"only {max_bound} times in the formulas.\n"
                    f"Element {isotope.element}, limits: {self.limits[i_el]}\n"
                    f"Isotope {isotope}, n_times {isotopes[(Z, A)]}"
                )

            self.exact_masses[isotope.symbol] = isotope.exact_mass

        # check combinations of isotopes
        for combination in self.combine_isotopes:
            # check if all isotopes included in a combination
            # exist in the formula grid as individual isotope
            exist = all([iso in isotopes for iso in combination])
            if not exist:
                raise ValueError(
                    'The following combination of isotopes is not '
                    'permitted as each isotope was not included '
                    'individually\n'
                    f'Combination is {combination}\n'
                    f'Isotopes is {isotopes}')

    def _setup_no_isotopic_grid(self):
        """ Create the matrix of all the possible formulas considering only
        the most abundant isotopes for each element.
        """

        # make the whole grid
        grid = np.stack(
            np.meshgrid(
                *(np.arange(n_min, n_max + 1, dtype=self.dtype)
                  for n_min, n_max in self.limits)
            )
        )
        grid = grid.reshape((len(self.elements), -1)).transpose()

        # Organic sample with at least C and H
        if all([elm in self.element_symbols for elm in ["C", "H"]]):
            # Hmax filtering
            Hmax = 2 * grid[:, self.species_mapping["C"]] + 2
            if "N" in self.species_mapping:
                Hmax += grid[:, self.species_mapping["N"]]
            idx, = np.where(grid[:, self.species_mapping["H"]] <= Hmax)
            grid = grid[idx, :]

            # H/C ratio filtering
            if self.H_C_bounds is not None:
                idx = self.filter_elements_ratio(
                    grid, ("H", "C"), self.H_C_bounds,
                    self.species_mapping, self.dtype)
                grid = grid[idx, :]

        # filtering according to elements ratio constraints
        for el_ratio in self.element_ratios:
            if not all([elm in self.element_symbols for elm in el_ratio]):
                raise ValueError(
                    "The constraint on element ratio is wrong at least one "
                    "of the elements is not in the formula grid.\n"
                    f"Elements: {self.element_symbols}\n"
                    f"Ratio constraints: {el_ratio}")

            # apply filter
            idx = self.filter_elements_ratio(
                grid, el_ratio, self.element_ratios[el_ratio],
                self.species_mapping, self.dtype)
            grid = grid[idx, :]

        # compute nominal masses and filter according to nominal mass
        nominal_masses = [el.mass_number_most_abundant for el in self.elements]
        nominal_masses = grid @ nominal_masses
        idx, = np.where((self.mass_bounds[0] <= nominal_masses) &
                        (nominal_masses <= self.mass_bounds[1]))
        grid = grid[idx, :]

        # compute and filter according to DBE
        if self.dbe_bounds is not None:
            dbe = self.compute_dbe_grid(grid, self.species_mapping)
            idx, = np.where((self.dbe_bounds[0] <= dbe) &
                            (dbe <= self.dbe_bounds[1]))
            grid = grid[idx, :]

        # compute the number of electrons of the neutral precursor
        # and apply lewis filter
        if self.lewis_filter:
            Z_values = np.array([el.Z for el in self.elements], dtype=np.uint16)
            # Warning: grid is uint8 and is limited to 255. If a python list
            # is used numpy broadcasts to int64 and the calculation is correct.
            # To keep it in mind, I enforce the type here. Pandas does not
            # adapt the type and may lead to wrong results.
            n_electrons = grid @ Z_values
            try:
                iH = self.species_mapping["H"]
            except KeyError as error:
                print(error)
                raise NotImplementedError(
                    "Elemental composition does not contain hydrogen but a "
                    "Lewis filtering is required.")

            # TODO: improve this with ionization mode
            if self.polarity > 0:
                # suppose this is ESI+ => need to check with Ionization Source
                if "Na" in self.element_symbols:
                    iNa = self.species_mapping["Na"]

                    # what if Na > 1 ??? compare to charge
                    if self.limits[iNa][1] > self.polarity:
                        print("WARNING: Na number")
                    # remove Na electrons if Na (what if Na > 1 ???)
                    n_electrons -= 11 * grid[:, iNa]
                    # if no Na, the ions have additional H (H+) 
                    # there are 1 additional H per charge (polarity)
                    # remove 1 electron per H if no Na to get the true number
                    # of electrons of the neutral molecule
                    n_electrons -= (self.polarity - grid[:, iNa])

                else:
                    # if no Na, the ions have an additional H (H+) 
                    # there are 1 additional H per charge (polarity)
                    # remove 1 electron per H (so per charge)
                    n_electrons = np.where(
                        grid[:, iH] >= self.polarity,
                        n_electrons - self.polarity,
                        n_electrons
                    )

            elif self.polarity < 0:
                # suppose this is ESI-
                # case M-H --> M.- + H+: neutral molecule has lost one H
                # the neutral molecule has thus one more e-
                # if z < -1, we assume a loss of z Hydrogen ???
                n_electrons += abs(self.polarity)

            # Lewis filter => assume a kind of octet rule
            # Organic molecule are singulet in the ground state
            #   => valid molecule have an even number of e-
            idx, = np.where(n_electrons % 2 == 0)
            grid = grid[idx, :]

        # if isotopic, complete with zero for each isotopes
        grid = np.concatenate(
            [grid, np.zeros((grid.shape[0], len(self._isotopes)),
                            dtype=self.dtype)],
            axis=1,
        )

        return grid

    def _setup_isotopic_grids(self, grids: dict, isotopes: dict):
        """ setup the grid according to input information
        
        Args:
            grids (dict): Dict of already built grids
            isotopes (dict): Input dictionary of isotopes such with (Z, A)
                as keys and amount as values.

        Returns:
            An update grids dictionnary
        """

        # start from non isotope grid
        grid0 = grids[(0, 0, 0)]

        # build grids including isotopes and fill in isotopic informations
        # 1) grids with only one single isotope
        for isotope in self.isotopes:
            for n_times in range(isotopes[(isotope.Z, isotope.A)]):
                # add isotopes
                grid = self._add_isotope(isotope, grid0, n_times=n_times + 1)
                if grid is not None:
                    grids[(isotope.Z, isotope.A, n_times + 1)] = grid

        # 2) include grids with isotopes combinations
        for combination in self.combine_isotopes:
            grids = self._add_combination(combination, grids)

        return grids

    def _add_isotope(self, isotope: Isotope, grid0: PatternGrid,
                     n_times: int = 1) -> PatternGrid:
        """ Using the monoisotopic grid, substitute ntimes times the
        corresponding element by the given isotope and return the new grid.

        Args:
            Z (int): atomic number of the isotope
            A (int): mass number of the isotope
            n_times (int): number of substitution per formula

        Returns:
            A new grid including the given isotopes ntimes times.
        """
        element = isotope.element
        delta_mz = isotope.exact_mass - element.exact_mass_most_abundant
        if self._polarity != 0:
            delta_mz /= abs(self._polarity)

        # extract relevant part of the grid
        i_el = self.species_mapping[element.symbol]
        i_isotope = self.species_mapping[isotope.symbol]
        mask = grid0.values[:, i_el] >= n_times

        if mask.any():
            values = grid0.values[mask, :]
            values[:, i_el] -= n_times

            # update grid
            values[:, i_isotope] = np.full(values.shape[0], n_times, dtype=self.dtype)
            
            # compute masses
            mz_shift = n_times * delta_mz
            calculated_mz = grid0.calculated_mz[mask] + mz_shift
            # nominal mass only relies on most abundant isotopes
            nominal_mass = grid0.nominal_mass[mask]

            # set up MonoIsotopeGrid
            if not grid0.is_isotopic:
                # case adding one single isotope to non isotopic grid
                grid_key = (isotope.Z, isotope.A, n_times)
                isotopes = [isotope]
            else:
                # case completing an already isotopic grid
                grid_key = grid0.key + (isotope.Z, isotope.A, n_times)
                isotopes = grid0.isotopes + [isotope]

            return PatternGrid(
                values,
                species=self.species,
                polarity=self._polarity,
                key=grid_key,
                mz_shift=mz_shift,
                calculated_mz=calculated_mz,
                nominal_mass=nominal_mass,
                isotopes=isotopes,
            )
        else:
            return None

    def _add_combination(self, combination: dict, grids: dict) -> dict:
        """
        Add a combination of isotopes in the formula grid
        """
        # sort combination along A
        sorted_AZ = sorted([key for key in combination], key=lambda x: x[1])
        
        # first, get the grid of formulas that include
        # at list n_times the first isotopes of the combination
        Z, A = sorted_AZ[0]
        key = (Z, A, combination[(Z, A)])
        grid0 = grids[key]
        mz_shift = grid0.mz_shift

        for Z, A in sorted_AZ[1:]:
            n_times = combination[(Z, A)]
            key += (Z, A, n_times)

            isotope = Isotope(Z, A)
            grid = self._add_isotope(isotope, grid0, n_times)
            mz_shift += grid.mz_shift

            # update a new grid0
            grid0 = PatternGrid(
                grid.values,
                species=self.species,
                polarity=self._polarity,
                key=key,
                mz_shift=mz_shift,
                calculated_mz=grid.calculated_mz,
                nominal_mass=grid.nominal_mass,
                isotopes=grid.isotopes,
            )

        grids[key] = grid0

        return grids

    def get_pattern_grid(self, pattern={(6, 13): 1}) -> PatternGrid:
        """ Return the pattern grid associated to the pattern. """
        # sort by increasing A
        sorted_isotopes = sorted([key for key in pattern], key=lambda x: x[1])
                
        key = ()
        for Z, A in sorted_isotopes:
            key += (Z, A, pattern[(Z, A)])

        if key in self._grids:
            return self._grids[key]
        else:
            raise KeyError(
                f"Pattern {pattern} corresponds to key {key} and is not"
                f" a valid key.\nValid keys: {self._grids.keys()}")

    @staticmethod
    def filter_elements_ratio(grid: ArrayLike,
                              elements: tuple = ("H", "C"),
                              bounds: tuple = [0.3, 3],
                              species_mapping: dict = {"C": 0, "H": 1},
                              dtype: DTypeLike = np.uint8) -> ArrayLike:
        """ Function for grid filtering according to an element ratio.
        The aim is to apply a constraint on the grid depending on the
        relative amount of two elements.

        Args:
            grid (ArrayLike): A grid of elemental composition.
            elements (tuple): Tuple of strings of the elements on which
                the constraint will be applied.
            bounds (tuple): Tuple of floats that define the ratio constraints.
            species_mapping (dict): Mapping between species and column index.
            dtype (DTypeLike): Numpy dtype for the grid.

        Returns:
            The indexes that satisfy the constraint.
        """
        # index of the elements on which applys the constraint ratio
        # ratio: numerator is 0 denominator is 1
        i_num = species_mapping[elements[0]]
        i_den = species_mapping[elements[1]]

        n_min = np.ceil(bounds[0] * grid[:, i_den]).astype(dtype)
        n_max = np.floor(bounds[1] * grid[:, i_den]).astype(dtype)

        # <= is needed to allow 0 values
        idx, = np.where(
            (grid[:, i_den] == 0) |
            ((n_min <= grid[:, i_num]) & (grid[:, i_num] <= n_max))
        )

        return idx

    @staticmethod
    def compute_dbe_grid(grid: ArrayLike, species_mapping: dict) -> ArrayLike:
        """ Compute the DBE on a grid of integer using the elements
        mapping dict to identify chemical mappings.

        Compute DBE support only the following elements: C, H, N, P, Na,
        Si, Cl, F, Br, I.

        Args:
            grid (ndarray): A grid of integer defining the formula composition
            species_mapping (dict): A dict where keys are species symbol
                and value the column index.

        Returns:
            Array of DBE values
        """

        # find C
        try:
            i_C = species_mapping["C"]
            n_C = grid[:, i_C]
        except KeyError:
            n_C = np.uint8(0)

        # Si ?
        if "Si" in species_mapping:
            i_Si = species_mapping["Si"]
            n_C += grid[:, i_Si]

        # other elements
        idx_P = list()
        idx_X = list()
        for el in species_mapping:
            if el in ["H", "Na", "F", "Cl", "Br", "I"]:
                idx_X.append(species_mapping[el])
            elif el in ["N", "P"]:
                idx_P.append(species_mapping[el])

        dbe = n_C - grid[:, idx_X].sum(axis=1) / 2 \
            + grid[:, idx_P].sum(axis=1) / 2 + 1

        return dbe

    def get_group(self, integer_part: int):
        """ Returns the group of formula grid with formulas whose integer
        part of their exact masses correspond to the given inter_part. """
        return self._grouped_grids.get_group(integer_part)

    @classmethod
    def from_formula(cls, formula: Union[Formula, Composition, dict, str],
                     polarity: int, **kwargs):
        """ Set up a formula grid, from a molecular formula, considering
        the amount of each element as the highest bounds of the grid for
        this element. If one element of the formula is carbon, the minimum
        amount of carbon is supposed to be 1.

        Args:
            formula (str, Formula, Composition): A molecular formula
            polarity (int): The ionization charge of the formula grid
            kwargs are passed to FormulaGrid constructor
        """
        formula = get_formula(formula)
        elements = list(formula.elemental_composition)
        limits = [[0, formula.elemental_composition[el]] for el in elements]

        # if C in elements, set the minimum value to 1.
        if "C" in elements:
            idx_C = elements.index("C")
            limits_C = limits[idx_C]
            limits[idx_C] = [1, limits_C[1]]

        isotope_dict = dict()
        for isotope_str in formula.isotopes:
            isotope = Isotope.from_string(isotope_str)
            isotope_dict[(isotope.Z, isotope.A)] = formula[isotope_str]

        return cls(
            polarity=polarity,
            elements=elements,
            limits=limits,
            isotopes=isotope_dict,
            **kwargs)

    def as_dict(self, symbol: bool = False) -> dict:
        """ Returns a dict including the input params that can be
        exported for example to a toml/yaml/json file to be saved and
        used again later. The output dictionary can be used directly
        in the class construcor or using `from_dict`.
        
        Args:
            symbol (bool): If True, print isotopes symbol instead of (Z, A)
                tuples.

        Returns:
            dict of formula grid parameters
        """

        d = {
            "elements": self.element_symbols,
            "limits": self.limits,
            "polarity": self.polarity,
            "H_C_bounds": self.H_C_bounds,
            "mass_bounds": self.mass_bounds,
            "name": self.name,
        }
        if self.dbe_bounds is not None:
            d["dbe_bounds"] = self.dbe_bounds
        if len(self.element_ratios) > 0:
            d["element_ratios"] = self.element_ratios
        if len(self.isotopes) > 0:
            d["isotopes"] = self._input_isotopes
        if len(self.combine_isotopes) > 0:
            d["combine_isotopes"] = self.combine_isotopes

        if symbol:
            if len(self.isotopes) > 0:
                isotopes = {Isotope(*iso).symbol: n 
                            for iso, n in self._input_isotopes.items()}
                d["isotopes"] = isotopes
            if len(self.combine_isotopes) > 0:
                combinations = [
                    {Isotope(*iso).symbol: n for iso, n in combination.items()}
                    for combination in self.combine_isotopes
                ]
                d["combine_isotopes"] = combinations
            if len(self.element_ratios) > 0:
                d["element_ratios"] = {
                    "_".join(k): v for k, v in self.element_ratios.items()
                }

        return d

    @classmethod
    def from_dict(cls, input_dict: dict):
        """ setup the class from a dictionary containing all the relevant
        parameters to create a new formula grid. The function manage
        cases where elements or isotopes are defined from their symbol.

        Args:
            input_dict (dict): input parameter as dict.

        The corresponding dictionary can be obtained from the ``as_dict``
        method. The ``'polarity'`` part is mandatory.

        .. code-block:: python

            {'elements': ['C', 'H', 'O', 'N', 'B'],
            'limits': [(0, 100), (0, 200), (0, 20), (0, 5), (0, 1)],
            'polarity': -1,
            'H_C_bounds': (0.2, 3.0),
            'dbe_bounds': (0.0, 40.0),
            'mass_bounds': (100.0, 1200.0),
            'element_ratios': {'O_C': (0.0, 0.5)},
            'isotopes': {'13C': 2, '18O': 1, '15N': 1, '10B': 1},
            'combine_isotopes': [{'10B': 1, '13C': 1}, {'13C': 1, '18O': 1}],
            'name': 'formula_grid'}
        
        .. code-block:: python

            {'elements': ['C', 'H', 'O', 'N', 'B'],
            'limits': [(0, 100), (0, 200), (0, 20), (0, 5), (0, 1)],
            'polarity': -1,
            'H_C_bounds': (0.2, 3.0),
            'dbe_bounds': (0.0, 40.0),
            'mass_bounds': (100.0, 1200.0),
            'element_ratios': {'O_C': (0.0, 0.5)},
            'isotopes': {(6, 13): 2, (8, 18): 1, (7, 15): 1, (5, 10): 1},
            'combine_isotopes': [{(5, 10): 1, (6, 13): 1}, {(6, 13): 1, (8, 18): 1}],
            'name': 'formula_grid'}

        """
        if "polarity" not in input_dict:
            raise KeyError("Missing polarity.")

        # check if isotopes are symbols or tuples
        if "isotopes" in input_dict and input_dict["isotopes"] is not None:
            is_str = all([
                isinstance(iso, str) for iso in input_dict["isotopes"]
            ])
            if is_str:
                isotopes = {
                    Isotope.from_string(iso): n
                    for iso, n in input_dict["isotopes"].items()
                }
                isotopes = {(iso.Z, iso.A): n for iso, n in isotopes.items()}
                input_dict["isotopes"] = isotopes

        # check if combinations are symbols or tuples
        if "combine_isotopes" in input_dict and input_dict["combine_isotopes"] is not None:
            is_str = all([isinstance(iso, str)
                          for combination in input_dict["combine_isotopes"]
                          for iso in combination])
            if is_str:
                combinations = [
                    {Isotope.from_string(iso): n for iso, n in combination.items()}
                    for combination in input_dict["combine_isotopes"]
                ]
                combinations = [
                    {(iso.Z, iso.A): n for iso, n in combination.items()}
                    for combination in combinations
                ]
                input_dict["combine_isotopes"] = combinations
    
        if "element_ratios" in input_dict and input_dict["element_ratios"] is not None:
            is_tuple = all([isinstance(k, tuple) for k in input_dict["element_ratios"]])
            if not is_tuple:
                input_dict["element_ratios"] = {
                    tuple(k.split("_")): bounds
                    for k, bounds in input_dict["element_ratios"].items()
                }

        return cls(**input_dict)
    
    def to_toml(self, filename: Union[str, IO] = None):
        """ Export the formula grid to a toml configuration file.

        Args:
            filename (str or ioStream): file name or an open file object.
        
        """
        d = self.as_dict(symbol=True)
        document = dict(formula_grid=[d])
        if filename is None:
            return tomlkit.dumps(document)
        else:
            try:
                with open(filename, "w", encoding="utf-8") as fo:
                    tomlkit.dump(document, fo)
            except TypeError:
                tomlkit.dump(document, filename)

    @classmethod
    def from_toml(cls, filename: Union[str, IO] = None):
        """ Set up a formula grid, reading a toml file or string containing
        the formula grid parameters.

        Args:
            filename (str or ioStream): file name or an open file object.
        """
        try:
            with open(filename, "rb") as fi:
                d = tomllib.load(fi)
        except TypeError:
            d = tomllib.load(filename)
        
        if "formula_grid" not in d:
            raise KeyError(f"'formula_grid' not in toml:\n{list(d.keys())}")

        if isinstance(d["formula_grid"], list):
            if len(d["formula_grid"]) > 1:
                print("Read first formula grid in file")
            d = d["formula_grid"][0]
        else:
            d = d["formula_grid"]

        ctrl = FormulaGridControls(**d)
        return cls.from_dict(ctrl.model_dump())

    def get_formula_overview(self) -> str:
        """ Return a string representing as a molecular formula an overview
        of the formula grid. """

        max_lim = [valmax for _, valmax in self.limits]
        iso_dict = {Isotope(*iso).symbol: n for iso, n in self._input_isotopes.items()}

        formula = list()
        for symb, n in zip(self.element_symbols, max_lim):
            formula.append(f"{symb}{n}")
        for symb, n in iso_dict.items():
            formula.append(f"{symb}{n}")

        return " ".join(formula)

    def __getitem__(self, key):
        return self._grids[key]

    def __iter__(self):
        return iter(self._grids)

    def __len__(self):
        return len(self._grids)

    def __repr__(self):
        # print(self.input_info())
        return (
            f"FormulaGrid: {self.name}\n"
            f"    polarity={self._polarity},\n"
            f"    elements={self.element_symbols},\n"
            f"    limits={self.limits},\n"
            f"    H_C_bounds={self.H_C_bounds},\n"
            f"    dbe_bounds={self.dbe_bounds},\n"
            f"    mass_bounds={self.mass_bounds},\n"
            f"    element_ratios={self.element_ratios},\n"
            f"    isotopes={self._input_isotopes},\n"
            f"    lewis_filter={self.lewis_filter},\n"
            f"    combine_isotopes={self.combine_isotopes}\n")
    
    def __str__(self):
        """ User's define input information """
        return (f'FormulaGrid: {self.name}\
            \n   Elements: {self.element_symbols}\
            \n   Elemental Limits: {self.limits}\
            \n   Polarity: {self._polarity}\
            \n   Mass bounds: {self.mass_bounds}\
            \n   H/C ratio bounds: {self.H_C_bounds}\
            \n   DBE bounds: {self.dbe_bounds}\
            \n   ratio constrains: {self.element_ratios}\
            \n   Isotopes: {self._input_isotopes}\
            \n   lewis filter: {self.lewis_filter}\
            \n   Combine isotopes: {self.combine_isotopes}')


class FormulaGridList(abc.Sequence):
    """ This class define a list of FormulaGrid object, to be used to
    implement iterative attribution """

    def __init__(self, filename: Union[str, IO] = None):
        """
        Initialize the formula grids list from a toml configuration file
        or with an empty list.

        Args:
            filemane (str): path to the filename or a file object.
            
        """
        self._formula_grids = list()

        if filename is not None:
            try:
                with open(filename, "rb") as fi:
                    d = tomllib.load(fi)
            except TypeError:
                d = tomllib.load(filename)

            if "formula_grid" not in d:
                raise KeyError(
                    f"'formula_grid' not in toml:\n{list(d.keys())}")

            if isinstance(d["formula_grid"], dict):
                d = [d["formula_grid"]]
            else:
                d = d["formula_grid"]

            for fg_params in d:
                ctrl = FormulaGridControls(**fg_params)
                self._formula_grids.append(FormulaGrid.from_dict(ctrl.model_dump()))

    @classmethod
    def from_list(cls, formula_grids: list):
        """ Create the object from a list of formula grid """
        fg_list = cls()
        for fg in formula_grids:
            fg_list.append(fg)
        return fg_list

    def as_dict(self, symbol: bool = False) -> dict:
        """ Returns a dict including a list of formula grid dictionary.
        Set ``symbol`` to True if you want to print isotope symbol instead
        of (Z, A) tuples. This is a good option to output formula grids to
        configuration files.
        
        Args:
            symbol (bool): If True, print isotopes symbol instead of (Z, A)
                tuples.

        Returns:
            dict of formula grids

        """
        d = {"formula_grid": list()}
        for fg in self._formula_grids:
            d["formula_grid"].append(fg.as_dict(symbol=symbol))

        return d
    
    def append(self, fg: FormulaGrid):
        """ Append a Formula grid to the list """
        if not isinstance(fg, FormulaGrid):
            raise TypeError(f"fg must be a FormulaGrid, but obj is {type(fg)}")
        self._formula_grids.append(fg)

    def load(self, filename: Union[str, IO]):
        """ Load from a toml configuration file one or several formula grid
        and append them to the list.

        Args:
            filemane (str): path to the filename or a file object.
    
        """

        try:
            with open(filename, "rb") as fi:
                d = tomllib.load(fi)
        except TypeError:
            d = tomllib.load(filename)

        if "formula_grid" not in d:
            raise KeyError(
                f"'formula_grid' not in toml:\n{list(d.keys())}")

        if isinstance(d["formula_grid"], dict):
            d = [d["formula_grid"]]
        else:
            d = d["formula_grid"]

        for fg_params in d:
            ctrl = FormulaGridControls(**fg_params)
            self._formula_grids.append(FormulaGrid.from_dict(ctrl.model_dump()))

    def dump(self, filename: Union[str, IO] = None):
        """ Export the list of formula grid to a toml configuration file.
        If filename is None, return a string representation.

        Args:
            filename (str or ioStream): File name or an open file object
        
        """
        d = self.as_dict(symbol=True)
        if filename is None:
            return tomlkit.dumps(d)
        else:
            try:
                with open(filename, "w", encoding="utf-8") as fo:
                    tomlkit.dump(d, fo)
            except TypeError:
                tomlkit.dump(d, filename)        

    def get(self, item: Union[str, int]) -> FormulaGrid:
        """ Return the formula grid using either the name or the index 
        
        Args:
            item (str or int): If a string is provided return the formula
                grid with the corresponding name. If an int is provided
                return the formula grid at the given index.

        Returns:
            A formula grid.

        """
        if isinstance(item, int):
            return self._formula_grids[item]
        elif isinstance(item, str):
            for fg in self._formula_grids:
                if fg.name == item:
                    return fg
            else:
                raise KeyError(
                    f"Formula grid with name '{item}' does not exist.\n"
                    "Available grids are: "
                    f"{', '.join([fg.name for fg in self._formula_grids])}")
        else:
            raise ValueError(
                f"{type(item)} is not a valid index for formula grids."
            )
    
    def get_subset(
        self,
        items: Union[str, list], 
    ) -> list[FormulaGrid]:
        """ get the list of formula grid from the provided list using
        either index (if int are provided) or names (if str are provided)
        """

        if len(items) == 1:
            item = items[0]
            if isinstance(items[0], str) and item.lower() == "all":
                return [fg for fg in self._formula_grids]
        
        return [self.get(item) for item in items]
        
    def __len__(self):
        return len(self._formula_grids)

    def __getitem__(self, item: int):
        return self._formula_grids[item]

    # def __setitem__(self, item: int, obj: FormulaGrid):
    #     if not isinstance(obj, FormulaGrid):
    #         raise TypeError(f"obj must be a FormulaGrid, but obj is {type(obj)}")
    #     self._formula_grid[item] = obj

    def __repr__(self):
        lines = ["List of Formula Grids"]
        for fg in self._formula_grids:
            lines.append(f"{fg.name} -> {fg.get_formula_overview()}"
                         f" (polarity={fg.polarity})")
        return "\n".join(lines)
