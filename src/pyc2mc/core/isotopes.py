#!/usr/bin/env python
# coding: utf-8

"""
This module contains classes presenting Isotope and IsotopicData. The
classes provide methods in order to get the mass data of a given isotope
or a list of isotopes.

Attributes:
    DIFF_13C12C (float): calculated mass shift between 13C and 12C isotopes of carbon
    ELECTRON_MASS (float): mass of the electron in Da
    VERSION (str): Version of the NIST data file used for atomic data
    DATA_FILE (str): Path to the NIST data file
"""

import re
from typing import Union
from pathlib import Path
from itertools import combinations
import numpy as np
import pandas as pd

__all__ = ["NISTData", "Element", "Isotope", "VERSION", "DATA_FILE",
           "NIST_DATA", "DIFF_13C12C"]

iso_patt = re.compile(r"^(\d{0,3})([A-Za-z]{1,2})$")


class NISTData:
    """ This class is a container of NIST Data 
    
    https://www.nist.gov/pml/atomic-weights-and-isotopic-compositions-relative-atomic-masses
    """

    def __init__(self, data: pd.DataFrame, version: str = None):
        """ The class should be initialize from the read_isotopes_data
        class method. It takes as argument a DataFrame wit the NIST Data.
        
        Args:
            data (pd.DataFrame): The NIST data.
        """
        self._data = data
        self._version = version

        # specific data to speed up filtering
        self._atomic_numbers = self._data["atomic number"].values.astype(np.uint8)
        self._mass_numbers = self._data["mass number"].values.astype(np.uint16)
        self._element_symbol = self._data["element"].values

        # abundance exists
        self._abundance_exist = ~np.isnan(self._data["isotopic abundance"].values)
        self._data_w_abundance = self._data.iloc[self._abundance_exist, :]

        # dict to speed up element selection
        # idxmax returns the index in the whole table (self._data)
        idx = self._data_w_abundance.groupby(["atomic number"]).agg(
            {"isotopic abundance": "idxmax"}).to_dict()
        self._Z_most_abundant = {
            k: self._data.iloc[idx].to_dict() 
            for k, idx in idx["isotopic abundance"].items()}
        
        # dict to speed up isotopes selection
        self._isotopic_data = {
            (row["atomic number"], row["mass number"]): row.to_dict() 
            for _, row in self._data.iterrows()}
        
        self._Z_symbol_mapping = {k: v["element"] 
                                  for k, v in self._Z_most_abundant.items()}
        self._symbol_ZA_mapping = {v["element"]: (k, v["mass number"]) 
                                   for k, v in self._Z_most_abundant.items()}

        # isotopic mass shift matrix
        self._isotopic_mass_shift_matrix = np.subtract.outer(
            self.data_w_abundance["exact mass"].values, 
            self.data_w_abundance["exact mass"].values)

    @property
    def data(self):
        """ The NIST Data as a pandas DataFrame """
        return self._data

    @property
    def data_w_abundance(self):
        """ Return the NIST DATA frame with not NaN values for abundance. """
        return self._data_w_abundance
    
    @property
    def isotopic_mass_shift_matrix(self):
        """ Matrix of all isotopic mass shift between isotopes for which
        the relative abundance is known. """
        return self._isotopic_mass_shift_matrix

    @property
    def version(self):
        """ Return the current version if provided """
        return self._version
        
    def get_element_data(self, Z: int = 1):
        """ Filter the NIST data and return the most abundant isotope
        from the given Z value. """

        try:
            data = self._Z_most_abundant[Z]
        except KeyError:
            raise ValueError(f"Element Z = {Z} does not exist.")

        return data

    def get_Z_from_string(self, symbol: str):
        """ Filter the NIST data and return the most abundant isotope
        corresponding to the element symbol. """
        try:
            Z = self._symbol_ZA_mapping[symbol.strip()][0]
        except KeyError:
            raise ValueError(f"Element '{symbol}' does not exist.")
        
        return Z

    def get_isotope_data(self, Z: int = 1, A: int = 1):
        """ Filter the NIST data and return the isotope for the given Z
        and A """

        try:
            data = self._isotopic_data[(Z, A)]
        except KeyError:
            raise ValueError(f"Isotope ({Z}, {A}) does not exist.")

        return data

    @classmethod
    def from_NIST_file(cls, path: Union[str, Path], **kwargs):
        """ Read isotopes data in NIST data base and returns a data frame 

        Args:
            path (str, Path): path to the NIST data file

        Returns:
            A data frame with isotopic data
        """

        path = Path(path)
        if not path.exists():
            raise FileNotFoundError(f"NIST data file not found at: {path}.")

        # read in NIST file
        with path.open("r") as f:
            data = dict()
            el_data = list()
            for line in f:
                if line.strip() == "":
                    el_data.append(data)
                    data = dict()
                elif line[0] == "#":
                    continue
                else:
                    key, value = line.strip().split("=")
                    if key.strip() == "Atomic Symbol":
                        if value.strip() == 'D':
                            data["element"] = 'H'
                        elif value.strip() == 'T':
                            data["element"] = 'H'
                        else:
                            data["element"] = value.strip()
                    elif key.strip() == "Atomic Number":
                        data["atomic number"] = int(value)
                    elif key.strip() == "Relative Atomic Mass":
                        value = "".join(value.split("(")).split(")")[0].strip("#")
                        data["exact mass"] = np.float64(value)
                    elif key.strip() == "Mass Number":
                        data["mass number"] = int(value)
                    elif key.strip() == "Standard Atomic Weight":
                        if value.strip() == "":
                            value = np.nan
                        else:
                            if "[" in value:
                                value = value.strip().strip("[]")
                                if "," in value:
                                    vmin, vmax = value.split(",")
                                else:
                                    vmin, vmax = value, value
                                value = (np.float64(vmin) + np.float64(vmax)) / 2
                            elif "(" in value:
                                value = "".join(value.split("(")).split(")")[0]
                        data["standard atomic weight"] = np.float64(value)
                    elif key.strip() == "Isotopic Composition":
                        if value == " 1":
                            value = np.float64(value)
                        elif value.strip() == "":
                            value = np.nan
                        else:
                            value = "".join(value.split("(")).split(")")[0]
                        data["isotopic abundance"] = np.float64(value)

        data = pd.DataFrame(el_data)
        data["super symbol"] = data.apply(
            lambda row: f'{superscripts(str(row["mass number"]))}{row["element"]}',
            axis="columns"
        )
        data["symbol"] = data.apply(
            lambda row: f'{row["mass number"]}{row["element"]}',
            axis="columns"
        )
        return cls(data=data, **kwargs)


def superscripts(word):
    """ Method to create the superscripts for the isotopes.

    Args:
        word (str): a string containing the isotope name
    """
    superscript_map = {"0": "⁰", "1": "¹", "2": "²", "3": "³", "4": "⁴",
                       "5": "⁵", "6": "⁶", "7": "⁷", "8": "⁸", "9": "⁹"}
    trans = str.maketrans(superscript_map)
    word = word.translate(trans)

    return word


def subscripts(word):
    """ Method to create the subscripts for the isotopes.

    Args:
        word (str): a string containing the isotope name
    """
    superscript_map = {"0": "₀", "1": "₁", "2": "₂", "3": "₃", "4": "₄",
                       "5": "₅", "6": "₆", "7": "₇", "8": "₈", "9": "₉"}
    trans = str.maketrans(superscript_map)
    word = word.translate(trans)

    return word


# electron mass
# source (doi): 10.1103/PhysRevLett.75.3598
# atomic mass units (Da)
ELECTRON_MASS = 0.0005485799

# Reading NIST DATA
TXT = Path("./AtomicWeightsAndIsotopicDataNIST2021.txt")
VERSION = 20210913
DATA_FILE = Path(__file__).parent / TXT
# DATA_FILE = Path(__file__).absolute().parents[1] / TXT

NIST_DATA = NISTData.from_NIST_file(DATA_FILE, version=VERSION)


def all_nominal_masses(species: list) -> dict:
    """ Returns a dict with all possible nominal mass according to the
    list of species. The nominal mass of a species is the nearest integer
    of the the mass of the most abundant isotope of the current element.

    Args:
        species (list): List of species (element or isotope) as string.

    Returns
        A dict with species as keys and nominal mass as values.
    """
    nominal_masses = dict()
    for specie in species:
        m = iso_patt.match(specie)
        element = Element.from_string(m.group(2))
        nominal_masses[specie] = np.round(element.exact_mass_most_abundant).astype(np.uint16)

    return nominal_masses


def all_exact_masses(species: list) -> dict:
    """ Returns a dict with all possible exact mass according to the
    list of species. If the species is an element, the exact mass of the
    most abundant isotope is returned. If the species is an isotope, both the
    exact masses of the isotope and its most abundant isotopes are returned.
    
    Args:
        species (list): List of species (element or isotope) as string.

    Returns
        A dict with species as keys and exact mass as values.
    """
    exact_masses = dict()
    for k in species:
        m = iso_patt.match(k)
        A = m.group(1)
        element = Element.from_string(m.group(2))
        if A:
            Z = element.atomic_number
            iso = Isotope(Z, A)
            exact_masses[k] = Isotope(Z, A).exact_mass
            exact_masses[iso.element.symbol] = iso.exact_mass_most_abundant
        else:
            exact_masses[k] = element.exact_mass_most_abundant
    return exact_masses


class Element:
    """ This class represent a chemical element """

    def __init__(self, Z: int = 1):
        """ Return an element according to its atomic number """

        self._Z = int(Z)
        self.data = NIST_DATA.get_element_data(self._Z)

    @property
    def Z(self):
        """ Atomic number """
        return self._Z

    @property
    def atomic_number(self):
        """ Atomic number """
        return self._Z

    @property
    def symbol(self):
        """ Element symbol """
        return self.data["element"]

    @property
    def atomic_weight(self):
        """ Standard atomic weight"""
        return self.data["standard atomic weight"]

    @property
    def mass_number_most_abundant(self):
        """ The mass number of the most abundant isotope of the element """
        return self.data["mass number"]

    @property
    def most_abundant_isotope(self):
        """ Return the most abundant isotope """
        return Isotope(self.Z, self.mass_number_most_abundant)

    @property
    def exact_mass_most_abundant(self):
        """ The exact mass of the most abundant isotope of the element """
        return self.data["exact mass"]
    
    @property
    def abundance(self):
        """ Returns the abundance of the most abundant isotope. """
        return self.data['isotopic abundance']

    @classmethod
    def from_string(cls, symbol):
        """ Return an element from its symbol """
        return cls(Z=NIST_DATA.get_Z_from_string(symbol))

    def get_isotopes(self, dropna=False, return_type="data"):
        """ Returns the list of isotopes of the element.

        Args:
            dropna (bool): Either to drop isotopes with Nan values or not
            return_type (str): Either "data" or "object". If "data", returns
                a dataframe with the isotopes. If "object", returns a set of
                Isotope objects.

        Returns
            A DataFrame with all the isotopes of the element.
        """
        if return_type == "data":
            return Isotope.get_isotopes_data(atomic_numbers=[self.Z], dropna=dropna)
        elif return_type == "object":
            return Isotope.get_isotopes(atomic_numbers=[self.Z], dropna=dropna)
        else:
            raise ValueError("'return_type' must be 'data' or 'object' but the"
                             f" actual value is '{return_type}'")

    def to_dict(self):
        """ return a dict version of the isotop data """
        return {"atomic number": self.Z,
                "symbol": self.symbol,
                "standard atomic weight": self.atomic_weight,
                "mass number most abundant": self.mass_number_most_abundant,
                }

    def __hash__(self):
        return self.Z

    def __str__(self):
        return f"{self.symbol}"

    def __repr__(self):
        return f"{self.symbol}; Element({self.Z})"

    def __eq__(self, other):
        if isinstance(other, Element):
            return self.Z == other.Z
        elif isinstance(other, int):
            return self.Z == other
        elif isinstance(other, str):
            return self.symbol == other
        else:
            raise TypeError("Cannot compare Element with {type(other)}.")


class Isotope:
    """ This class represent an isotop and provides related properties 
    such as its exact mass and the isotopic abundance. 

    version:
        NIST file version

    data_file:
        Path of the NIST data file

    """

    # data base info
    version = VERSION
    data_file = DATA_FILE

    def __init__(self, Z: int = 1, A: int = 1):
        """ Return an isotope defined from its atomic number and its
        mass number.

        Args:
            Z (int): atomic number
            A (int): mass number
        """
        self.Z = int(Z)
        self.A = int(A)
        self._element = Element(self.Z)

        self._data = NIST_DATA.get_isotope_data(self.Z, self.A)

    @property
    def atomic_number(self):
        """ Atomic number """
        return self.Z

    @property
    def symbol(self):
        """ Isotope symbol written as AX such as 13C for carbon with A = 13 """
        return self._data["symbol"]

    @property
    def super_symbol(self):
        """ Isotope symbol written with a super script font """
        return self._data["super symbol"]

    @property
    def mass_number(self):
        """ Mass number or nominal mass """
        return self.A

    @property
    def exact_mass(self):
        """ Exact mass in g.mol-1 """
        return self._data["exact mass"]

    @property
    def abundance(self):
        """ Isotopic aboundance """
        return self._data["isotopic abundance"]

    @property
    def element(self):
        """ The element of this isotope """
        return self._element

    @property
    def is_most_abundant(self):
        """ return True if it is the most abundant isotope of the current element """
        return self.A == self._element.mass_number_most_abundant

    @property
    def exact_mass_most_abundant(self):
        """ Exact mass of the most abundant isotopes of the same element """
        return self._element.exact_mass_most_abundant
    
    @property
    def mass_number_most_abundant(self):
        """ mass number of the most abundant isotopes of the same element """
        return self._element.mass_number_most_abundant

    @classmethod
    def from_string(cls, symbol):
        """ Return an isotope from its symbol assuming a syntax such as 13C """

        if m := iso_patt.search(symbol):
            A, symbol = m.groups()
            try:
                A = int(A)
            except ValueError:
                raise ValueError(
                    f"Invalid mass number for an isotope: '{symbol}', A = {A}"
                )
        else:
            raise ValueError(f"String '{symbol}' is not a valid isotope.")

        el = Element.from_string(symbol)
        return cls(el.Z, int(A))

    @classmethod
    def find_mass_split(cls, requested_split=1.003355, tolerance=0.0001,
                        superscript=False, sort=True):
        """ Find a mass gap between two isotopes. The split is supposed
        to be between masses and not between ionic masses.

        Args:
            requested_split (float): the gap between isotopes' masses
            tolerance (float): the tolerance to find the candidates
            superscript (bool): if True, name use superscript symbol
            sort (bool): if True, sort the candidates using the lowest absolute error

        Returns:
            A list of candidates with tuple (symbol shift, value, error)
        """
        candidates = list()

        idx = np.where(
            np.abs(NIST_DATA.isotopic_mass_shift_matrix - requested_split) < tolerance)
        icandidates = np.stack(idx).transpose()

        if len(icandidates) > 0:
            # look for min value
            minval = 1e10
            for i, j in icandidates:
                NIST_value = NIST_DATA.isotopic_mass_shift_matrix[i, j]
                error = NIST_value - requested_split
                if abs(error) < abs(minval):
                    minval = error

                if superscript:
                    name = (
                        f'{NIST_DATA.data_w_abundance.iloc[i]["super symbol"]}'
                        f' - {NIST_DATA.data_w_abundance.iloc[j]["super symbol"]}')
                else:
                    name = (
                        f'{NIST_DATA.data_w_abundance.iloc[i]["symbol"]}'
                        f' - {NIST_DATA.data_w_abundance.iloc[j]["symbol"]}')

                candidates.append((name, NIST_value, error))

        # sort according to the lowest absolute error
        if sort:
            candidates = sorted(candidates, key=lambda x: abs(x[1]))

        return candidates

    @classmethod
    def get_most_abundant(cls, elements=None, atomic_numbers=None):
        """ From the list of elements return a data frame with exact masses
        of the most abundant isotopes. A list of element symbols, as string, 
        or atomic numbers, as integer must be provided.

        Args:
            elements (list): List of element symbols, ``["C", "H", "N"]``
            atomic_numbers (list): List of atomic numbers, ``[6, 1, 7]``

        Returns
            A DataFrame with the most abundant isotope for each element.
        """

        sel = cls.get_isotopes_data(elements, atomic_numbers)
        sel = sel.loc[sel.groupby(
            "element")["isotopic abundance"].idxmax()].copy()

        return sel.reset_index(drop=True)

    @classmethod
    def get_isotopes(cls, elements=None, atomic_numbers=None, dropna=False):
        """ From the list of elements return a list of Isotope objects which
        represent the existing isotopes for each element. A list of element 
        symbols, as string, or atomic numbers, as integer must be provided.
        If dropna is True, only isotopes with existing abundance values are
        returned.

        Args:
            elements (list): List of element symbols, ``["C", "H", "N"]``
            atomic_numbers (list): List of atomic numbers, ``[6, 1, 7]``
            dropna (bool): Either to drop isotopes with Nan values or not

        Returns
            A set of Isotope objects.
        """
        data = cls.get_isotopes_data(elements, atomic_numbers, dropna)

        isotopes = set()
        for _, (Z, A) in data[["atomic number", "mass number"]].iterrows():
            isotopes.add(cls(Z, A))

        return isotopes

    @staticmethod
    def get_isotopes_data(elements=None, atomic_numbers=None, dropna=False):
        """ From the list of elements return a data frame with the corresponding
        isotopes. A list of element symbols, as string, or atomic numbers, 
        as integer must be provided.

        Args:
            elements (list): List of element symbols, ``["C", "H", "N"]``
            atomic_numbers (list): List of atomic numbers, ``[6, 1, 7]``
            dropna (bool): Either to drop isotopes with Nan values or not

        Returns
            A DataFrame with the all the isotopes for each element.
        """
        if elements:
            if not isinstance(elements, list):
                elements = [elements]
            try:
                sel = NIST_DATA.data[NIST_DATA.data.element.isin(elements)].copy()
            except TypeError:
                raise TypeError("elements must be a list object.")
        elif atomic_numbers:
            if not isinstance(atomic_numbers, list):
                atomic_numbers = [atomic_numbers]
            try:
                sel = NIST_DATA.data[NIST_DATA.data["atomic number"].isin(atomic_numbers)].copy()
            except TypeError:
                raise TypeError("atomic_numbers must be a list object.")
        else:
            raise ValueError("Invalid arguments. Need a list of elements"
                             " or a list of atomic numbers.")

        if len(sel) == 0:
            raise ValueError("elements or atomic numbers not found.\n"
                             f"elements: {elements}\n"
                             f"atomic_numbers: {atomic_numbers}")

        # add a column with isotop names with superscript format
        sel["isotope"] = sel.apply(
            lambda r: f'{superscripts(str(r["mass number"]))}{r["element"]}',
            axis="columns"
        )

        if dropna:
            return sel.dropna().reset_index(drop=True)
        else:
            return sel.reset_index(drop=True)
        
    @classmethod
    def get_mass_shifts(cls, element: Union[Element, str, int], 
                        threshold: float = 1e-3, as_dataframe: bool = True
                        ) -> Union[pd.DataFrame, list]:
        """ For the given element, compute all the possible mass shifts
        between the isotopes of this element. The list is filtered
        comparing the threshold with the product of the abundances of the
        two isotopes. The mass shift is computed as the less abundant 
        isotope mass less the most abundant isotope mass.
        
        Args:
            element (Element, str, int): Element to consider as an Element object
                a string or the atomic number.
            threshold (float): Pairs with a probability lower than threshold
                are ignored.
            as_dataframe (bool): If True, default, return the data in a pandas
                DataFrame. If False, return a list of dict.

        Returns:
            A pandas DataFrame or a dict depending on ``as_dataframe``.
        """
        try:
            # element is an Element
            symbol = element.symbol
        except AttributeError:
            try:
                # element is an integer
                symbol = Element(Z=element).symbol
            except ValueError:
                try:
                    # element is a string
                    symbol = Element.from_string(element).symbol
                except (ValueError, AttributeError):
                    raise ValueError(
                        f"The element '{element}' was not identified.")

        isotopes = cls.get_isotopes([symbol], dropna=True)

        data = list()
        for pair in combinations(isotopes, 2):
            iso1, iso2 = pair
            if iso1.abundance < iso2.abundance:
                iso2, iso1 = iso1, iso2
            probability = iso1.abundance * iso2.abundance
            if probability < threshold:
                continue

            mass_shift = iso2.exact_mass - iso1.exact_mass
            data.append({"isotopes": f"{iso2}-{iso1}",
                         "mass_shift": mass_shift,
                         "probability": probability})

        if len(data) == 0:
            return pd.DataFrame([]) if as_dataframe else []
        else:
            if as_dataframe:
                df = pd.DataFrame(data).sort_values(by="probability",
                                                    ascending=False)
                df.reset_index(inplace=True, drop=True)
                return df
            else:
                return data
                        
    def to_dict(self):
        """ return a dict version of the isotop data """
        return {"atomic number": self.Z, "mass number": self.A,
                "symbol": self.symbol,
                "element": self._data["element"],
                "exact mass": self.exact_mass,
                "isotopic abundance": self.abundance}

    def __hash__(self):
        return hash((self.Z, self.A))

    def __str__(self):
        # TODO using superscript ?
        # return f"{superscripts(self.A)}{self.symbol}""
        return self.symbol

    def __repr__(self):
        return f"^{self.A}{self.element}; Isotope({self.Z}, {self.A})"

    def __eq__(self, other):
        return isinstance(other, Isotope) and self.Z == other.Z and self.A == other.A

    def __add__(self, other):
        return self.exact_mass + other.exact_mass

    def __sub__(self, other):
        return self.exact_mass - other.exact_mass


# compute difference between 12C and 13C mass
DIFF_13C12C = Isotope(6, 13).exact_mass - Isotope(6, 12).exact_mass
