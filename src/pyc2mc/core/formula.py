# coding: utf-8

""" This module provides core classes to work with chemical formula. In
particular the module defines the classes Composition and Formula. The
first one is a simple dictionary which provides the species and their
amount. The methods included aims to manage this composition. The Fromula
object is more complete and provides methods to compute the exact mass of
the formula.
"""

from __future__ import annotations  # if python < 3.10
from typing import Union, Any

import re
import warnings
import itertools
from collections.abc import Mapping

import numpy as np
import pandas as pd

from pyc2mc.core.isotopes import (Element, Isotope, ELECTRON_MASS,
                                  all_exact_masses)
from pyc2mc.plotinator.isotopic_plotter import IsoPatternPlotter

__all__ = ["sort_elements", "compute_dbe", "get_str_formula", 
           "get_dict_formula", "IsotopicPattern", "Formula", "Composition",
           "get_formula", "get_species", "KendrickBuildingBlock"]

el_patt = re.compile(r"^(\d{0,3})([a-zA-Z]{1,2})(\d*)$")

ELEMENTS_ORDER = ["O", "N", "B", "P", "S", "F", "Cl", "Br", "I"]


def sort_elements(elements: list, return_index: bool = False) -> list:
    """ Sort the elements in the input list according to the
    ELEMENTS_ORDER list. C and H are first in the list. Then the
    order follow ELEMENTS_ORDER. Remaining elements are sorted
    alphabetically.

    Args:
        elements (list): list of string of element.
        return_index (bool): return the index of the element in the
            input list, default False.

    Returns:
        The sorted list of elements and the index of elements in the
        input array if ``return_index`` is True.
    """
    sorted_elements = list()
    tmp_els = elements.copy()

    # first C and H and the elements in ELEMENT_ORDER
    for el in ["C", "H"] + ELEMENTS_ORDER:
        if el in tmp_els:
            sorted_elements.append(tmp_els.pop(tmp_els.index(el)))

    # other elements
    sorted_elements += sorted(tmp_els)

    if return_index:
        indices = [elements.index(el) for el in sorted_elements]
        return sorted_elements, indices
    else:
        return sorted_elements


def compute_dbe(composition):
    """ Compute the Double Bond Equivalent (DBE) from a key value
    pairs list of element compostion.

    Args:
        composition (dict like): dict or pandas series such as
            ``{"C": 6, "H": 6}``

    Returns:
        Double Bond Equivalent

    TODO: the DBE function assume C, H are always present.
    """
    comp_X = 0  # Halogens

    if 'C' not in composition:
        comp_C = 0
    else:
        comp_C = composition['C']

    if 'H' not in composition:
        comp_H = 0
    else:
        comp_H = composition['H']

    if "Na" not in composition:
        comp_Na = 0
    else:
        comp_Na = composition["Na"]

    if "Si" not in composition:
        comp_Si = 0
    else:
        comp_Si = composition["Si"]

    if "P" not in composition:
        comp_P = 0
    else:
        comp_P = composition["P"]

    if "N" not in composition:
        comp_N = 0
    else:
        comp_N = composition["N"]

    if "Cl" in composition:
        comp_X += composition["Cl"]
    elif "Br" in composition:
        comp_X += composition["Br"]
    elif "F" in composition:
        comp_X += composition["F"]
    elif "I" in composition:
        comp_X += composition["I"]

    composition_exist = any([
        c != 0 
        for c in [comp_C, comp_Si, comp_H, comp_Na, comp_X, comp_N, comp_P]]
    )
    if composition_exist:
        dbe = comp_C + comp_Si - (comp_H + comp_Na + comp_X) / 2 + \
            (comp_N + comp_P) / 2 + 1
    else:
        dbe = np.nan

    return dbe


def get_str_formula(composition):
    """
    Function to create a string representing the molecular formula from
    a composition dict. This function aims to provide a way to produce
    efficiently a string representation without instanciating a Formula
    or Composition object and without sorting the species in the composition.
    This may lead to non reproductible results are dict key are not
    sorted.

    Args:
        composition (dict like): dict or pandas series such as
            ``{"C": 6, "H": 6}``

    Returns:
        A string representing the formula
    """

    return " ".join(
        [f"{el}{int(n)}" for el, n in composition.items() if n > 0]
    )


def get_dict_formula(composition: str):
    """
    Returns a dictionary with the set formula. Assume whitespace
    separated.
    
    Args:
        composition (str): A formula as string. The form should be
            as the following example: 'C13 H14 O6 B1 13C1'.
    
    Returns:
        A dictionary with the stoichiometry of the set formula.
    """
    d = dict()
    for chunk in composition.split():
        if m := el_patt.match(chunk):
            key = m.group(1) + m.group(2)
            if m.group(3):
                value = int(m.group(3))
            else:
                value = 1
        else:
            raise ValueError(f"Error in formula '{composition}' in part '{chunk}'.")

        if value > 0:
            d[key] = value
    return d


class IsotopicPattern:
    r"""
    This class defines an Isotopic Pattern from a Formula or Composition.

    When the class is instantiated, and if the isotopic pattern is not
    given through the arguments of the class, the intance method will
    determine all the possible combinations of isotopes for the given
    formula under a certain threshold (which is 0.001% by default). To
    do this, the method calculates the probability of abundance of n
    atoms of each isotope in the formula, and then it calculates all
    the possible combinations and evaluates the probability for each one.

    The probability for each combination (k) is calculated as follows:

    .. math::

        P_{k} = \prod_{j=1}^{N} \prod_{i=1}^{M} r_{j}v_{j}
    
    Where Pk is the probability of the k-th combination, N is the number of
    possible isotopes to combine, and M is the number of times that each
    isotope is present in the combination. The r and v are, the ratio of
    abundances of the i-th isotope, and the stoichiometric coefficient of
    the element of i in the molecular formula, respectively.

    The IsotopicPattern class has some interesting attributes.

    .. attribute: formula

        A String, Composition or a Formula object of the current molecular
        formula.

    .. attribute: abundances

        Pandas Series that ontains the Calculated Abundances for each 
        isotopic pattern, according to the combinations.
    
    .. attribute: combinations

        This is a list or array of tuples that contains the possible 
        combinations of isotopes of the isotopic pattern.
    
    .. attribute: threshold

        The absolute threshold of abundances to filter the isotopic
        patterns.
    
    .. attribute: isotopes_list

        The list of isotopes that where considered to determine the
        isotopic pattern based on the threshold. The order of the 
        symbols in the list corresponds to the order of the columns
        in the combinations attribute.
    
    .. attribute: pattern_exact_masses

        The exact masses of the corresponding isotopic pattern.

    .. attribute: isotope_id

        A string with the number of isotopes of the corresponding pattern.
    
    .. attribute: plot

        Creates a STEM plot of the Isotopic Pattern.

    """

    def __init__(self,
                 formula: Union[Composition, Formula],
                 threshold: float = 0.001,
                 isotopic_patterns: dict = None,
                 isotopes_dict: dict = None,
                 add_exact_mass: bool = False,
                 id_isotopes: bool = True):
        """ 
        Instantiate the IsotopicPattern class.

        Args:
            formula (Composition or Formula): A Composition or a Formula 
                object of the current molecular formula.
            threshold (float): this parameter represent the relative abundance
                threshold to filter the isotopic pattern structure. It is
                0.001 by default.
            isotopic_patterns (dict): list of isotope combinations 
                expressed as dictionary.
                The keys of the dict are the couple (Z, A) and the values are 
                the maximum number of isotopes to be included in the grid. For
                example, for at most one 10B and two 13C: 
                ``{(5, 10): 1, (6, 13): 1}``
                If this argument is not given, then the algorithm search for the
                most likely combination of isotopes.
            isotopes_dict (dict): Is the dictionary of Isotopes objects, in
                which the keys are the couple (Z, A) and the values are the
                corresponding Isotope object.
            add_mass (bool): if True the mass column is added in the output
                data frame. If True and exact_masses is not given when the formula
                is instantiated, the calculation time is doubled.
            id_isotopes (bool): if False the isotope's names are not returned.
        """
        # test that formula is a Formula object
        if not isinstance(formula, Formula):
            try:
                self.formula = get_formula(formula)
            except ValueError:
                raise ValueError(f'bad formula: {formula}.')
        else:
            self.formula = formula

        # get the elemental composition
        elm_list = list(self.formula.elemental_composition.keys())

        # if the isotopic pattern is None, search all possible candidates
        if isotopic_patterns is None:
            if isotopes_dict is None:
                # is more efficient to get the data frame rather than instance
                # each Isotope class
                listof_iso = Isotope.get_isotopes_data(elm_list, dropna=True)
                idx = []
                for elm in elm_list:
                    df_elm = listof_iso[listof_iso['element'].values == elm]
                    idx.append(
                        df_elm[
                            df_elm['isotopic abundance'].values ==
                            df_elm['isotopic abundance'].max()].index.item())
                listof_iso['is_most_abund'] = [
                    True if i in idx else False for i in listof_iso.index.values]
                df_iso = listof_iso.drop(np.array(idx))
                dict_most_abund = listof_iso.iloc[idx].set_index('element').to_dict()
                
                d = dict()
                delta = list()
                isotopes_sym = list()
                ratio_abund = list()
                stoich_num = list()
                ntimes = list()
                super_sym = list()
                for __, row in df_iso.iterrows():
                    delta.append(
                        row['exact mass'] - dict_most_abund['exact mass'][row['element']]
                    )
                    super_sym.append(row['super symbol'])
                    ratio_abund.append(
                        row['isotopic abundance'] /
                        dict_most_abund['isotopic abundance'][row['element']])
                    stoich_num.append(formula[row['element']])
                    number = 0
                    elm_num = self.formula[row['element']]
                    prob = elm_num * row['isotopic abundance']
                    while ((prob > threshold * 0.1) and
                           (self.formula[row['element']] - number > 0)):
                        number += 1
                        prob = prob * row['isotopic abundance']
                    isotopes_sym.append(row['symbol'])
                    ntimes.append(number)
                    d[(row['atomic number'], row['mass number'])] = number
                self._isotopic_patterns = d
            else:
                d = dict()
                delta = list()
                isotopes_sym = list()
                ratio_abund = list()
                stoich_num = list()
                ntimes = list()
                super_sym = list()
                for key, iso in isotopes_dict.items():
                    number = 0
                    prob = formula[iso.element.symbol] * iso.abundance
                    while (prob > threshold*0.1 and 
                           formula[iso.element.symbol] - number > 0):
                        number += 1
                        prob = prob * iso.abundance
                    d[key] = number
                    try:
                        delta.append(iso.exact_mass - iso.exact_mass_most_abundant)
                        isotopes_sym.append(iso.symbol)
                        ratio_abund.append(iso.abundance / iso.element.abundance)
                        stoich_num.append(formula[iso.element.symbol])
                        ntimes.append(number)
                        super_sym.append(iso.super_symbol)
                    except AttributeError:
                        raise TypeError('The values of the isotopes_dict must be '
                                        'objects of the pyc2mc.core.Isotope class.')
                self._isotopic_patterns = d
        else:
            self._isotopic_patterns = isotopic_patterns
            if not isinstance(isotopic_patterns, dict):
                raise TypeError('The isotopic_pattern argument must be '
                                'a dictionary.')

            if isotopes_dict is None:
                try:
                    isotopes_dict = {
                        key: Isotope(key[0], key[1]) 
                        for key, __ in isotopic_patterns.items()}
                except ValueError:
                    raise ValueError(f'Isotope Z = {key[0]}, A = {key[1]} '
                                     'does not exist.')        
            delta = list()
            isotopes_sym = list()
            ratio_abund = list()
            stoich_num = list()
            ntimes = list()
            super_sym = list()
            for key, num in isotopic_patterns.items():
                iso = isotopes_dict[key]
                try:
                    delta.append(iso.exact_mass - iso.exact_mass_most_abundant)
                    isotopes_sym.append(iso.symbol)
                    ratio_abund.append(iso.abundance / iso.element.abundance)
                    stoich_num.append(formula[iso.element.symbol])
                    ntimes.append(num)
                    super_sym.append(iso.super_symbol)
                except AttributeError:
                    raise TypeError('The values of the isotopes_dict must be '
                                    'objects of the pyc2mc.core.Isotope class.')

        # set init attribute
        self._super_sym = super_sym
        self._stoich_num = stoich_num
        self._ratio_abund = ratio_abund
        self._deltas = delta
        self._n_times = ntimes
        self.isotopes_list = isotopes_sym
        self.threshold = threshold
        self.isotope_id = []
        self.pattern_exact_masses = []

        self._setup(add_exact_mass, id_isotopes)
        self.plot = IsoPatternPlotter(self)
    
    @property
    def probabilities(self):
        return np.array([a / sum(self.abundances) for a in self.abundances])

    def _setup(self, add_exact_mass: bool = False,
               id_isotopes: bool = True):
        """
        Setup the class.

        Args:
            add_exact_mass (bool): if True the exact mass column is
                added in the output data frame. If True and exact_masses
                is not given when the formula is instantiated, the
                calculation time is doubled.
            id_isotopes (bool): if False the isotope's names are not
                returned.

        """
        # add masses to calculation
        if add_exact_mass:
            if self.formula.exact_mass is not None:
                exact_mass = self.formula.exact_mass
            else:
                exact_mass = self.formula.get_exact_mass()

        indexes = [range(0, n + 1) for n in self._n_times]
        self.combinations = [comb for comb in itertools.product(*indexes)]

        probability = []
        for row in self.combinations:
            P = 1
            if id_isotopes:
                name = ''
            if add_exact_mass:
                comb_exact_mass = exact_mass

            for i, r in enumerate(row):
                for __ in range(r):
                    P = P * self._ratio_abund[i] * self._stoich_num[i]
                if r > 0:
                    if id_isotopes:
                        # name += self._super_sym[i] + subscripts(str(r))
                        name += f"{self.isotopes_list[i]}{r} "
                    if add_exact_mass:
                        comb_exact_mass = comb_exact_mass + self._deltas[i] * r

            if add_exact_mass:
                self.pattern_exact_masses.append(comb_exact_mass)

            if id_isotopes:
                self.isotope_id.append(name.strip())

            probability.append(round(P, 6))

        abund = pd.Series(probability, name='calc. abund.')

        if id_isotopes:
            self.isotope_id = np.array(self.isotope_id)

        if add_exact_mass:
            self.pattern_exact_masses = np.array(self.pattern_exact_masses)

        self.abundances = abund
  
    def to_dataframe(self):
        """Returns a data frame with the isotopic pattern infortmation. """
        df = pd.DataFrame([self.abundances]).transpose()
        if len(self.isotope_id) > 0:
            df['isotopes'] = self.isotope_id
        if len(self.pattern_exact_masses) > 0:
            df['pattern_exact_mass'] = self.pattern_exact_masses
        df['combinations'] = self.combinations
        df.set_index('combinations', drop=True, inplace=True)
        df.reset_index(inplace=True)
        df['prob.'] = self.probabilities
        df = df[df['calc. abund.'] > self.threshold].copy()
        df.reset_index(inplace=True)
        df.drop(columns='index', inplace=True)
        return df
    
    def _get_masses(self):
        """
        Returns an array with the masses of the isotopic pattern.
        """
        if len(self.pattern_exact_masses) > 0:
            return self.pattern_exact_masses
        else:
            if self._deltas:
                exact_mass = self.formula.exact_mass
                pattern_exact_masses = []
                for row in self.combinations:
                    pattern_exact_mass = exact_mass
                    for i, r in enumerate(row):
                        if r > 0:
                            pattern_exact_mass += self._deltas[i] * r
                    pattern_exact_masses.append(pattern_exact_mass)
                self.pattern_exact_masses = np.array(pattern_exact_masses)
                return self.pattern_exact_masses
            else:
                return []
            
    def __repr__(self):
        return repr(self.to_dataframe())


class Composition(Mapping):
    """
    Class to define a composition from a molecular formula. This class
    is more efficient than the Formula class to manage strings or
    dictionaries containing only the molecular composition. The main
    reason is that the Formula class was designed to manage AttributedPeaks,
    while the Composition class goal is to achieve simple operations on
    the amount of species in a formula lif for example to obtain Fine
    Isotopic Structures.
    """

    def __init__(self, d=None, /, **kwargs):
        """
        Initialize a composition from a dict whose keys are string
        representation of the species and values are the amount of each
        species in the composition.

        Args:
            d (dict): dictionary containing a composition.
        """
        if d is not None:
            # remove elements if amount is zero
            d = {el: d[el] for el in d if d[el] > 0}
            self._composition = d
        else:
            self._composition = dict()

        # check if all keys are valid element/isotope
        for k in kwargs:
            if el_patt.match(k):
                try:
                    self._composition[k] = int(kwargs[k])
                except ValueError:
                    raise ValueError(
                        f"For element '{k}', the value '{kwargs[k]}' is "
                        "not a valid nnumber.")
        
        # isotopes detection
        self._isotopic = False
        self._isotopes = list()
        self._elemental_composition = dict()
        self._isotopes_per_element = dict()
        for k in self._composition:
            m = el_patt.match(k)
            if m.group(1):
                # k is isotopic
                self._isotopic = True
                isotope = m.group(1) + m.group(2)
                self._isotopes.append(isotope)
                element = m.group(2)
                if element in self._elemental_composition:
                    self._elemental_composition[element] += self._composition[k]
                else:
                    self._elemental_composition[element] = self._composition[k]
                
                # keep a list of isotopes of an element
                if element in self._isotopes_per_element:
                    self._isotopes_per_element[element].append(k)
                else:
                    self._isotopes_per_element[element] = [isotope]

            else:
                # k is not isotopic (it is the most abundant isotope)
                if k in self._elemental_composition:
                    self._elemental_composition[k] += self._composition[k]
                else:
                    self._elemental_composition[k] = self._composition[k]

        # determine chem_class and chem_group
        (self._chem_class, self._chem_class_mono, 
         self._chem_group, self._chem_group_mono) = self._get_chem_class()

    @property
    def is_isotopic(self):
        """ Returns a bool that indicates if is or not an isotopic formula. """
        return self._isotopic
    
    @property
    def isotopes(self):
        """ Returns a list of isotopes present in the composition """
        return self._composition.keys() - self.elemental_composition.keys()
    
    @property
    def elemental_composition(self):
        """ Returns the elemental composition. """
        return self._elemental_composition
    
    @property
    def isotopes_per_element(self):
        """ Returns the list of isotopes for each element """
        return self._isotopes_per_element
    
    @property
    def chem_class(self):
        """ Returns the chemical class of the molecular formula."""
        return self._chem_class
    
    @property
    def chem_group(self):
        """ Returns the chemical group of the molecular formula."""
        return self._chem_group

    @property
    def chem_class_mono(self):
        """ Returns the chemical class of the molecular formula."""
        return self._chem_class_mono
    
    @property
    def chem_group_mono(self):
        """ Returns the chemical group of the molecular formula."""
        return self._chem_group_mono

    @property
    def dbe_value(self) -> float:
        """ Returns the DBE value of a molecular formula."""
        return compute_dbe(self._composition)
    
    @property
    def elements(self) -> set:
        """ List of elements object in the composition """
        return set([Element.from_string(k) for k in self.elemental_composition])
    
    @property
    def species_as_isotope(self) -> set:
        """ """
        d = {el.symbol: el.most_abundant_isotope for el in self.elements}
        isotopes = [Isotope.from_string(sp) for sp in self.isotopes]
        d.update({isotope.symbol: isotope for isotope in isotopes})
        return d
    
    def to_formula(self, exact_masses: dict = None) -> Formula:
        """ Return a formula from the composition """
        return Formula(self, exact_masses=exact_masses)

    def is_pattern_possible(self, isotopic_patterns: dict = {(6, 13): 1}):
        """ Determine if the set pattern is a possible isotopic pattern 
        of the current formula.
        
        Args:
            isotopic_patterns (list-like): list of isotope combinations expressed a
                    s a dictionary.
                    The keys of the dict are the couple (Z, A) and the values are 
                    the maximum number of isotopes to be included in the grid. For
                    example, for at most one 10B and two 13C: 
                    ``{(5, 10): 1, (6, 13): 1}``
                    It is 13C1 by default {(6, 13): 1}.
        """
        for k, num in isotopic_patterns.items():
            if not isinstance(num, int):
                raise ValueError(f'The number {num} is not an integer but a {type(num)}')
            
            if num > 0:
                try:
                    (Z, A) = k
                    iso = Isotope(Z, A)
                    elm_sym = iso.element.symbol   
                    if self._composition[elm_sym] < num:
                        raise ValueError(
                            f'The number of {elm_sym} in {self._composition} '
                            f'do not allow to add {num} of {iso.symbol}')                                       
                    if elm_sym in self._composition:
                        return True
                    else:
                        return False
                except ValueError:
                    raise ValueError(
                        f'The introduced pattern {isotopic_patterns} is not a '
                        'correct isotopic_patterns format.')

    def get_pattern_abundance(self, pattern: str = '13C'):
        """
        Calculate the abundance for one isotopic pattern.

        Args:
            pattern (str): The isotope name for which the abundance will
                be estimated.
        
        Returns the estimated abundance ratio of the isotopic pattern.
        """
        try:
            iso = Isotope.from_string(pattern)
        except ValueError:
            raise ValueError(
                f'The isotope {pattern} is not a correct isotope format.')

        iso_abundance = iso.abundance
        # elm_num = iso.element.mass_number_most_abundant
        mono_abundance = iso.element.abundance
        ratio_abund = iso_abundance / mono_abundance
        return ratio_abund

    def get_isotopic_pattern(self, *args, **kwargs):
        """
        Returns an Isotopic Pattern object with the fine isotopic structure of
        the current Formula or Composition. This method is more efficient when
        is executed from a Formula, rather than a Composition.

        Args:
            isotopic_patterns (list-like): list of isotope combinations 
                expressed as dictionary.
                The keys of the dict are the couple (Z, A) and the values are 
                the maximum number of isotopes to be included in the grid. For
                example, for at most one 10B and two 13C: 
                ``{(5, 10): 1, (6, 13): 1}``
                It this is not given, the algorithm determines the most likely
                combination of isotopes.
            isotopes_dict (dict): Is the dictionary of Isotopes objects, in
                which the keys are the couple (Z, A) and the values are the
                corresponding Isotope object.
            threshold (float): this parameter represent the relative abundance
                threshold to filter the isotopic pattern structure. It is
                0.001 by default.
            add_exact_mass (bool): if True the mass column is added in the output
                data frame. If True and exact_masses is not given when the formula
                is instantiated, the calculation time is doubled.
            id_isotopes (bool): if False the isotope's names are not returned.
        
        Returns an Isotopic Pattern.
        """

        return IsotopicPattern(formula=self, *args, **kwargs)
    
    def get_dict_of_isotopes(self):
        """ Returns a dictionary containing the corresponding Isotope
        object for each specie of the composition.
        
        The returned isotope is the most abundant one if the specie
        is just an element. Else, the corresponding isotope is returned.
        """
        dict_iso = dict()
        for sp in self._composition.keys():
            try:
                elm = Element.from_string(sp)
                (Z, A) = (elm.Z, elm.mass_number_most_abundant)
                dict_iso[sp] = Isotope(Z, A)
            except ValueError:
                dict_iso[sp] = Isotope.from_string(sp)
        return dict_iso

    def substitute_elements(self, element_block: dict = {}) -> Composition:
        """
        Returns a new Composition obtained by adding or removing the 
        elements defined in the element_block dictionary.

        Args:
            element_block (dict): A dictionary containing the elements
                and the corresponding values to modify in the current
                molecular formula.
        
        Returns a Composition with new molecular formula.
        """
        compo = self._composition.copy()
        if element_block:
            for specie, num in element_block.items():
                # if specie is not in the composition, add a new specie
                if specie not in compo:
                    if num > 0:
                        compo[specie] = num
                    else:
                        raise ValueError(f'The specie {specie} is not in {self}')
                else:
                    compo[specie] += num
                    if compo[specie] < 0:
                        raise ValueError(
                            f'Cannot substitute {num} species to {compo}.'
                            f'The number of species {specie} in the '
                            f'Composition is {compo[specie]}.')
            return Composition(compo)
        else:
            return Composition(compo)
    
    def insert_isotope(self, Z: int, A: int, number: int = 1):
        """
        Substitute one element of atomic number Z, corresponding to the
        most abundant isotope of this element from the composition by one
        of its possible isotope. The method returns a new Composition.

        Args:
            Z (int): Atomic Number.
            A (int): Mass Number.
            number (int): Substitute number element

        Returns:
            A Composition with subtituted isotopes.
        """
        try:
            specie = Isotope(Z, A)
        except ValueError:
            raise ValueError(
                f'The Atomic Number {Z} or Mass Number {A} does not match'
                ' for a valid isotope.')
        
        if specie.is_most_abundant:
            raise ValueError(f'Isotope {specie} is the most abundant.')

        if specie.element.symbol not in self._composition:
            raise ValueError(
                f"Element {specie.element.symbol }({Z}) does not exist in "
                f"the composition {self}.")

        # insert isotope
        compo = self._composition.copy()
        compo[specie.element.symbol] -= number
        if compo[specie.element.symbol] < 0:
            raise ValueError(
                f'Cannot substitute {number} species to {compo}. The number '
                f'of species {specie.element.symbol} in the composition '
                f'is {compo[specie.element.symbol]}.')
        if specie.symbol in self._composition:
            compo[specie.symbol] += number
        else:
            compo[specie.symbol] = 1
        return Composition(compo)
    
    def _get_chem_class(self) -> tuple:
        """
        Returns the chemical class and group of a molecular formula with
        and without isotopes.

        In both attributes, elements are sorted according to the ELEMENT_ORDER
        constant in order to obtain reproducible attributes. Isotopes are
        sorted using the number of mass (A).

        Returns:
            chem_class (str): The chemical class is HC if only H and C are
                present or provides the heteroatoms composition and includes
                isotopes composition.
            chem_class_mono (str): The chemical class only based on the
                elemental composition (not isotopes).
            chem_group (str): The chemical group is the chemical class
                without the amount of elements. Chemical classes O3 and O4
                both belong to the chemical group O. The chemical group
                differentiates isotopes.
            chem_group_mono (str): The monoisotopic chemical group is based
                only on the elemental composition.
            
        """
        
        elemental_composition = self.elemental_composition
        if elemental_composition.keys() == {'C', 'H'}:
            chem_class = 'HC'
            chem_class_mono = "HC"
            group_mono = "HC"
            if self._composition.keys() == {'C', 'H'}:
                group = 'HC'
            else:
                chem_class += f" 13C{self._composition['13C']}"
                group = '13C'
        else:
            # remove C, H and isotopes
            elms = self._composition.keys() - {'C', 'H'}
            if self._isotopic:
                elms = elms - set(self.isotopes)
            elms = list(elms)

            # sort elements according to the ELEMENTS_ORDER list
            # in order to have a fixed chem_class and chem_group
            sorted_elms = sort_elements(elms)

            if self._isotopic:
                # complete with isotopes
                # sort isotopes according to A
                sorted_elms += sorted(
                    self.isotopes,
                    key=lambda x: int(re.match(r"^(\d+)", x).group(1))
                )
                group = " ".join(sorted_elms)
                chem_class = " ".join([f"{el}{self._composition[el]}" 
                                       for el in sorted_elms])
                
                # monoisotopic class
                elms = elemental_composition.keys() - {'C', 'H'}
                sorted_elms = sort_elements(list(elms))
                group_mono = " ".join(sorted_elms)
                chem_class_mono = " ".join([f"{el}{elemental_composition[el]}" 
                                            for el in sorted_elms])
            else:
                chem_class_mono = " ".join([f"{el}{self._composition[el]}" 
                                            for el in sorted_elms])
                group_mono = " ".join(sorted_elms)
                group = group_mono
                chem_class = chem_class_mono

        return chem_class, chem_class_mono, group, group_mono
        
    @classmethod
    def from_string(cls, s: str):
        """ Formula from string. Assume whitespace separated.
        
        Args:
            s (str): string containing a composition.
        """
        d = get_dict_formula(s)
        return cls(d)

    def __len__(self):
        return len(self._composition)

    def __str__(self) -> str:
        """ For efficiency composition string is not sorted. """
        formula = ""
        for el in self._composition:
            formula += f"{el}{str(self._composition[el])} "
        return formula.strip()

    def __repr__(self) -> str:
        return f"Composition({self})"

    def __iter__(self):
        return iter(self._composition)

    def __getitem__(self, key):
        return self._composition[key]


class Formula(Composition):
    """
    Class that provides all the chemical information from a molecular
    formula. This class has a higher complexity that has a larger set
    of properties related to a MS Peak object. This class is less
    efficient to be instantiated due to the fact that the exact mass of
    each element is determined here to calculate the exact mass of the
    molecular formula. To increase the speed of the 
    Formula class initialization, it is highly recommended to give the
    exact_masses argument. If this one is provided the efficiency is
    similar as the Composition class.
    """

    def __init__(self, composition: Union[dict, Composition] = None,
                 exact_masses: dict = None, **kwargs):
        """
        Initialize a formula from a dict.

        Args:
            composition (dict): dictionary containing a composition.
            exact_masses (dict): dictionary containing the values for
                the exact masses, this is optional, if not, the masses
                will be obtained from the Elements and Isotope classes.
        """
        super().__init__(composition, **kwargs)
        
        if exact_masses:
            if not set(composition).issubset(exact_masses):
                missing = set.symmetric_difference(
                    set(exact_masses), set(composition))
                raise ValueError(
                    f"For element '{missing}', exact mass is not available.\n"
                    f"The following mass were provided {exact_masses}.")
        else:
            species = list(self)
            exact_masses = all_exact_masses(species)

        self._species = list(self)
        self._exact_masses = exact_masses

        # compute exact mass
        self._exact_mass = sum(
            [self[el] * self._exact_masses[el] for el in self]
        )
        
        # nominal masses => integer part of the most abundant isotope
        self._nominal_masses = {k: np.round(m).astype(np.uint16)
                                for k, m in self._exact_masses.items()}
        if self.is_isotopic:
            em = all_exact_masses(self._isotopes)
            self._exact_masses.update(em)
            for isotope in self._isotopes:
                for element in self._isotopes_per_element:
                    if isotope in self._isotopes_per_element[element]:
                        break
                self._nominal_masses[isotope] = self._nominal_masses[element]
        
        self._nominal_mass = np.sum(
            [self[el] * self._nominal_masses[el] for el in self])

    @property
    def list_of_species(self):
        """ Returns a list with the elements """
        message = "Please use the elemental_composition.keys() or self.keys() method instead."
        warnings.warn(message, DeprecationWarning, stacklevel=2)
        return self._species
    
    @property
    def exact_mass(self):
        """ Returns the calculated exact mass, this mass is not the ion
        exact mass as the ionization (polarity) is not included. See
        ``get_ion_exact_mass``."""
        return self._exact_mass
    
    @property
    def exact_masses(self):
        """ Dictionary of species present in the formula and their exact
        mass. """
        return self._exact_masses

    @property
    def nominal_mass(self) -> int:
        """ This is the calculated nominal mass.
        the mass of a formula calculated using the mass of the most
        abundant isotope of each species rounded to the nearest integer.
        """
        return self._nominal_mass

    @property
    def nominal_masses(self) -> dict:
        """ Dictionary of species (isotopic or not) present in the formula and 
        their nominal mass """
        return self._nominal_masses
    
    @property
    def integer_mass(self) -> int:
        """ Compute the integer mass of the formula using the mass number
        of each species in the formula. """
        sp_iso = self.species_as_isotope
        return np.sum([n * sp_iso[el].A for el, n in self._composition.items()])

    def get_ion_exact_mass(self, polarity: int) -> float:
        """
        This method returns the calculated ion exact mass which is computed
        as the mass / charge ratio ``(m - polarity x m_e) / |polarity|``.

        Args:
            polarity (int): ionization charge.
        """
        return (self.exact_mass - polarity * ELECTRON_MASS) / abs(polarity)
    
    def get_calculated_mz(self, polarity: int) -> float:
        """ This is a synonym of the ion exact mass.
        
        Args:
            polarity (int): ionization charge. 
        """
        return self.get_ion_exact_mass(polarity)
    
    def kendrick_mass(
            self,
            polarity: int,
            building_block: Union[KendrickBuildingBlock, str, float, Formula]
    ) -> float:
        """
        Returns the kendrick mass. The building block formula is used first
        and the value of the building block mass is used if not any formula
        is provided.
        
        Args:
            polarity (int): ionization charge.
            building_block (KendrickBuildingBlock): Kendrick Building Block
                as formula, a float or a KendrickBuildingBlock, default CH2.
        """
        building_block = KendrickBuildingBlock(building_block)        
        calc_mass = self.get_ion_exact_mass(polarity)
        factor = np.round(building_block.exact_mass)
        return calc_mass * factor / building_block.exact_mass

    def kendrick_mass_defect(
            self,
            polarity: int,
            building_block: Union[KendrickBuildingBlock, str, float, Formula]
    ) -> float:
        """
        Returns the kendrick mass. The building block formula is used first
        and the value of the building block mass is used if not any formula
        is provided.
        
        Args:
            polarity (int): ionization charge.
            building_block (KendrickBuildingBlock): Kendrick Building Block
                as formula, a float or a KendrickBuildingBlock, default CH2.
        """
        km = self.kendrick_mass(polarity, building_block)
        return np.round(km) - km
    
    def error_ppm(self, polarity: int, mz: float) -> float:
        """
        Calculates the error in ppm with respect to a target mass.
        
        Args:
            polarity (int): ionization charge.
            mass (float): target m/z value.
        """
        calc_mz = self.get_ion_exact_mass(polarity)
        return (calc_mz - mz) / calc_mz * 1e6
    
    def error_mDa(self, polarity: int, mz: float) -> float:
        """
        Calculates the error in mDa with respect to a target mass.
        
        Args:
            polarity (int): ionization charge.
            mass (float): target m/z value.
        """
        calc_mz = self.get_ion_exact_mass(polarity)
        return 1000 * (calc_mz - mz)

    @classmethod
    def from_string(cls, s, exact_masses: dict = None):
        """ 
        Formula from string. Assume whitespace separated.

        Args:
            formula (dict): dictionary containing a composition.
            exact_masses (dict): dictionary containing the values for
                the exact masses, this is optional, if not, the masses
                will be obtained from the Elements and Isotope classes.
        """
        composition = Composition.from_string(s)
        return cls(composition, exact_masses=exact_masses)

    def substitute_elements(self, element_block: dict = {}) -> Formula:
        """
        Returns a new Formula obtained by adding or removing the 
        elements defined in the element_block dictionary.

        Args:
            element_block (dict): A dictionary containing the elements
                and the corresponding values to modify in the current
                molecular formula.
        
        Returns a Formula with new molecular composition.
        """
        compo = super().substitute_elements(element_block)
        return Formula(compo, exact_masses=self.exact_masses)

    def __str__(self):
        """ Obtain the formula as string with sorted elements. Sorting is
        important for reproductibility of the formula string. """

        if self._isotopic:
            elms = list(self._composition.keys() - set(self.isotopes))
        else:
            elms = list(self._composition.keys())

        # sort element symbols according to the ELEMENTS_ORDER
        sorted_elms = sort_elements(elms)

        if self._isotopic:
            # complete with isotopes
            # sort isotopes according to A
            sorted_elms += sorted(
                self.isotopes,
                key=lambda x: int(re.match(r"^(\d+)", x).group(1))
            )

        formula = " ".join([
            f"{el}{str(self._composition[el])}" for el in sorted_elms
        ])
        
        return formula
    
    def __repr__(self):
        return f"Formula({self})"
    

def get_formula(thing, exact_masses: dict = None) -> Formula:
    """ Try to return a Formula object depending on the object.

    Args:
        thing: object from which one is trying to instanciate a Formula
        exact_masses (dict): dict of masses of species in the formula.

    Returns:
        Returns an instance of Formula if it succeed, else return None
    """
    if isinstance(thing, Formula):
        return Formula(thing, exact_masses=thing.exact_masses)
    
    elif isinstance(thing, str):
        # empty string ?
        if len(thing.strip()) == 0:
            return None

        # try to get a valid formula
        try:
            thing = Formula.from_string(thing, exact_masses=exact_masses)
            return thing
        except ValueError:
            # from_string fails to identify formula
            message = f"Bad formula: '{thing}', None is returned"
            warnings.warn(message, RuntimeWarning, stacklevel=2)
            return None

    elif isinstance(thing, (Composition, dict)):
        return Formula(thing, exact_masses=exact_masses)
        
    else:
        return None


def get_species(formulas: list) -> list:
    """ Return the list of species contained in a list of formulas 

    Args:
        formulas (list): list of Composition or Formula or object that
            can be converted to a Composition.
    """

    species = list()
    for f in formulas:
        if isinstance(f, str):
            try:
                composition = Composition.from_string(f)
            except ValueError:
                continue
            species += list(composition.keys())
        elif isinstance(f, (Composition, Formula)):
            species += list(f.keys())

    species = list(set(species))
    for specie in species:
        # check if species exists, is a real element/isotope
        # should be improved maybe changing try/except by if statements
        try:
            Element.from_string(specie)
        except ValueError:
            try:
                Isotope.from_string(specie)
            except ValueError:
                species.pop(species.index(specie))

    return species


class KendrickBuildingBlock:

    def __init__(self, building_block: Any, exact_masses: dict = None):
        """ Initialize the builing block from various possible object """

        self._formula = Formula()

        if isinstance(building_block, KendrickBuildingBlock):
            self._exact_mass = building_block.exact_mass
            if building_block.formula_avail:
                self._formula = building_block.formula
        elif isinstance(building_block, Formula):
            self._exact_mass = building_block.exact_mass
            self._formula = building_block
        else:
            if formula := get_formula(building_block, exact_masses):
                self._formula = formula
                self._exact_mass = formula.exact_mass
            else:
                try:
                    self._exact_mass = float(building_block)
                except ValueError:
                    raise ValueError(
                        f"'{building_block}' is not a valid building block")

        self._factor = np.round(self._exact_mass)
 
    @property
    def formula(self) -> Formula:
        """ Formula object of the building block """
        if len(self._formula) == 0:
            warnings.warn("Formula is not defined", RuntimeWarning, 2)
        return self._formula

    @property
    def exact_mass(self) -> float:
        """ exact mass of the building block """
        return self._exact_mass
    
    @property
    def formula_avail(self) -> bool:
        """ True if the formula is available. """
        return len(self._formula) > 0
    
    @property
    def factor(self) -> float:
        """ Nearest integer of the building block mass """
        return self._factor

    def __str__(self):
        return (f"{self.__class__.__name__}(mass={self._exact_mass:.6f}, "
                f"formula={self._formula})")

    def __repr__(self):
        return self.__str__()
