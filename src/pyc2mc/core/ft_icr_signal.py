# coding: utf-8

from dataclasses import dataclass, asdict
from enum import Enum, unique

import numpy as np
from numpy.typing import ArrayLike


@unique
class SignalFileType(str, Enum):
    """ File type for signal data from FT-ICR-MS """

    DAT = ".dat"
    PFD = ".pfd"
    RAW = ".raw"


@dataclass
class SignalMetadata:
    """ This class provides metadata associated to a signal obtained from
    an FT-ICR-MS experiment. The metadata are later use to swicth between
    time, frequency and m/z spaces. 
    
    Attributes:
        lowfreq (float):
        highfreq (float):
        number_points (int):
        bandwidth (float):
        file_type (SignalFileType):
        voltage_scale (float):
        zoomcount (int):
        zeroorder (float):
        start_index (int):
        a_term (float): 
        b_term (float):
        trap_voltage (float):
        rephase_sections (int):
        phased (bool): Whether the data were rephased or not
        step_size (int): the step size used to coadd

    """

    lowfreq: float
    highfreq: float
    number_points: int
    bandwidth: float
    file_type: SignalFileType
    voltage_scale: float
    zoomcount: int = None, 
    zeroorder: float = None
    start_index: int = None
    a_term: float = None 
    b_term: float = None
    trap_voltage: float = None
    rephase_sections: int = None
    phased: bool = False
    step_size: int = 1

    @property
    def hz_per_point(self):
        """ Hertz per point """
        return self.bandwidth / self.number_points
    
    @property
    def max_mz(self):
        return (
            self.metadata.a_term / self.metadata.lowfreq +
            self.metadata.b_term * np.abs(self.metadata.trap_voltage) / self.metadata.lowfreq ** 2
        )
                        
    @property
    def min_mz(self):
        return (
            self.metadata.a_term / self.metadata.highfreq +
            self.metadata.b_term * np.abs(self.metadata.trap_voltage) / self.metadata.highfreq ** 2
        )

    def to_dict(self):
        return asdict(self)


class FT_ICR_Signal:
    """ This class represents a raw FT-ICR signal. It provides the 
    signal itself and the way to convert get the m/z values. """

    def __init__(self, signal: ArrayLike, file_path: str = None, **kwargs):
        """ Init the class with the signal data and all metadata as kwargs

        TODO: file_path is used to be able to write dat or pfd into a new
        file by copy/paste the header to the new file. This needs to be
        improved from a better knowledge of the dat and pfd header and
        write direclty the header from metadata.
        
        Args:
            signal: The signal
            kwargs are all the metadata associated to the signal and are
                passed to the SignalMetaData class.  
        """
        
        self.signal = signal
        self.metadata = SignalMetadata(**kwargs)
        self.file_path = file_path

    def mz_space(self, range_mz=(150, 2000)):
        """
        Converts the signal to mz space. NOTE: Tested for latest version of phased .pfd files. Check for other versions
        :param range_mz: The range of mz values to convert to.
        :return: The mz signal, the signal in mz space, and the range of mz values.
        """

        if self.metadata.file_type == SignalFileType.DAT or self.signal is None:
            return -1

        # Check if the range is within the real mz range, else correct it
        # TODO: check here what happe ???
        real_mz_range = (np.floor(self.metadata.min_mz),
                         np.ceil(self.metadata.max_mz))

        low_mz = max(range_mz[0], real_mz_range[0])
        high_mz = min(range_mz[1], real_mz_range[1])

        range_mz = (low_mz, high_mz)

        if self.metadata.start_index is None:
            self.metadata.start_index = 0

        extended_signal = np.zeros(self.metadata.number_points)
        extended_signal[self.metadata.start_index:self.metadata.start_index + len(self.signal)] = self.signal
        signal = extended_signal

        abs_trap_voltage = np.abs(self.metadata.trap_voltage)
        hzppt = self.metadata.bandwidth / self.metadata.number_points

        freq_array = np.arange(len(signal)) * hzppt
        freq_array[0] = 1e-10

        mz_signal = (self.metadata.a_term / freq_array) + (self.metadata.b_term * abs_trap_voltage / (freq_array * freq_array))

        valid_indices = np.where((mz_signal > range_mz[0]) & (mz_signal < range_mz[1]))[0]
        signal = signal[valid_indices]
        mz_signal = mz_signal[valid_indices]

        sort_indices = np.argsort(mz_signal)
        mz_signal = mz_signal[sort_indices]
        signal = signal[sort_indices]

        return mz_signal, signal, range_mz
