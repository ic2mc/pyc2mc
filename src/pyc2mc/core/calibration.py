#!/usr/bin/env python
# coding: utf-8

"""
This module provides a class to read and encapsulate the data associated
to a list of internal calibrants (known formulas) used as a calibration list.
"""

from __future__ import annotations
from typing import Union
from collections.abc import Sequence
from collections import defaultdict
from dataclasses import dataclass, asdict
import warnings

import numpy as np
from numpy.typing import ArrayLike
import pandas as pd

from pyc2mc.utils import outliers_detection_TSE
from pyc2mc.core.peak_search import SearchingMode
from pyc2mc.core.isotopes import Isotope
from pyc2mc.core.formula import (Composition, get_formula, get_species,
                                 all_exact_masses)
from pyc2mc.core.peak import PeakList, AttributedPeakList
from pyc2mc.plotinator.calibration_plotter import CalibListPlotter

MAX_PEAKS = 50

__all__ = ["CalibrationPoint", "CalibList"]


@dataclass
class CalibrationPoint:
    """
    This class describes a calibration point associated to a calibrant species. 
    The only mandatory attribute is the calculated ion exact mass corresponding
    to the theoretical mz value of the calibrant which will be used as target
    data for the calibration. Nevertheless, for building the calibration list
    the polarity will be necessary in some cases.

    Attributes:
        mz_theo (float): calculated ion exact mass of the calibrant
        mz_exp (float): experimental ion mass
        formula (str): molecular formula, associated to the ion
        frequency (float): frequency
        intensity (float): intensity
        pid (int): peak identifier
        weight (float): weight of the point for the calibration
        polarity (int): ionization charge
        is_isotopic (bool): is the formula isotopic or not.

    """
    mz_theo: float
    formula: str = ""
    mz_exp: float = np.nan
    frequency: float = np.nan
    intensity: float = np.nan
    pid: int = np.nan
    weight: float = 1.
    polarity: int = None
    is_isotopic: bool = False

    @property
    def err_ppm(self):
        """ Calculate the error in ppm. """
        return (self.mz_theo - self.mz_exp) / self.mz_theo * 1e6
    
    def as_dict(self):
        """
        Returns a dictionary with the calibrant point information.
        """
        d = asdict(self)
        d["formula"] = str(self.formula)
        d["err_ppm"] = self.err_ppm
        return d
    
    def __repr__(self):
        s = f'Calibration Point: {self.mz_theo:.2f}'
        if self.formula:
            s += f" ({self.formula})"
        return s

    def __eq__(self, other):
        if self.pid is not None and other.pid is not None:
            return self.pid == other.pid
        else:
            raise ValueError("Cannot compare calibration points without pid.")
    
    def __gt__(self, other):
        return self.mz_theo > other.mz_theo
    
    def __lt__(self, other):
        return self.mz_theo < other.mz_theo


class CalibList(Sequence):
    """
    This class contains the methods employed for reading and defining data to
    further perform a recalibration.
    """
    
    def __init__(
        self,
        mz_theo: ArrayLike = None,
        formula: ArrayLike = None,
        mz_exp: ArrayLike = None,
        frequency: ArrayLike = None,
        intensity: ArrayLike = None,
        pid: ArrayLike = None,
        weight: ArrayLike = None,
        polarity: int = None,
        is_isotopic: ArrayLike = None,
        name: str = "calibration_list",
    ):
        """
        Instantiate the CalibList class by providing all the corresponding
        data individually. A better option is to get the instance from
        a PeakList or AttributedPeakList. All the data are not mandatory
        but can be necessary depending on the context of the utilization
        of the class.
        
        Args:
            mz_theo (ArrayLike): list of m/z values which will be used 
                to perform a recalibration of a mass spectra.
            formula (ArrayLike): list of strings with the corresponding 
                molecular formula.
            mz_exp (ArrayLike): list of the peaks were the internal calibrants
                were taken. This is optional.
            frequency (ArrayLike): list of frequencies if available.
            intensity (ArrayLike): list of intensities if available.
            pid (ArrayLike): list of peak identifiers.
            weight (ArrayLike): The array of weights, containing the
                values to correct the effect of each calibration point in the
                regression. This is an optional parameter. This array is used
                to create the matrix of weight which is a diagonal matrix,
                with the weights as the diagonal. It must have the same size of
                the calibration list.
            polarity (int): ionization charge
            is_isotopic (ArrayLike): Array of bool to define is the associated
                peak is isotopic or not.
            name (str): A name for the calibration list.
    
        """
        if mz_theo is None:
            mz_theo = list()
        void = np.array([np.nan] * len(mz_theo))

        try:
            mz_theo = np.array(mz_theo, dtype=np.float64)
        except TypeError:
            raise TypeError(
                f"The calibrant mz values must be float numbers: '{mz_theo}'.")

        if mz_exp is None:
            self._is_mz_exp_avail = False
            mz_exp = void
        else:
            self._is_mz_exp_avail = True
            try:
                mz_exp = np.array(mz_exp, dtype=np.float64)
            except TypeError:
                raise TypeError(
                    f"The experimental mz values must be float numbers: '{mz_exp}'.")

        if formula is None:
            formula = np.full(len(mz_theo), "")
            self._is_formula_avail = False
        else:
            self._is_formula_avail = True
            species = get_species(formula)
            exact_masses = all_exact_masses(species)
            for f in formula:
                f = get_formula(f, exact_masses=exact_masses)
                if f is None:
                    self._is_formula_avail = False
                    break
            
            if self._is_formula_avail:
                # this prevent from iterating over formula mapping
                array_formula = np.empty(len(formula), dtype=object)
                array_formula[:] = formula
                formula = array_formula
                del array_formula
            else:
                formula = np.full(len(mz_theo), "")

        if frequency is None:
            self._is_freq_avail = False
            frequency = void
        else:
            self._is_freq_avail = True
            frequency = np.array(frequency, dtype=np.float64)
        
        if intensity is None:
            self._is_intens_avail = False
            intensity = void
        else:
            self._is_intens_avail = True
            intensity = np.array(intensity, dtype=np.float64)

        if pid is None:
            self._is_pid_avail = False
            pid = void
        else:
            self._is_pid_avail = True
            pid = np.array(pid, dtype=np.uint16)

        # define the matrix of weights for the calibration points
        if weight is None:
            weight = np.ones_like(mz_theo)
        else:
            if len(weight) != len(mz_theo):
                raise ValueError('The dimension of the matrix of weights do not '
                                 'match the length of the calibrant_list: '
                                 f'{len(weight)}, {len(mz_theo)}.')
            weight = np.array(weight, dtype=np.float64)

        if is_isotopic is None:
            is_isotopic = np.full(mz_theo.shape, False)
        else:
            is_isotopic = np.array(is_isotopic)

        # check shape consistency
        for array in [formula, mz_exp, frequency, weight, intensity, is_isotopic]:
            if array.shape != mz_theo.shape:
                print("Array with wron shape:", array)
                raise ValueError(
                    f"Shapes of {mz_theo.shape} and {array.shape} are not consistent.")

        # sort values according to the mz order in the calibration list
        argsort = np.argsort(mz_theo)
        mz_theo = mz_theo[argsort]
        if self._is_mz_exp_avail:
            mz_exp = mz_exp[argsort]
        if self._is_freq_avail:
            frequency = frequency[argsort]
        if self._is_intens_avail:
            intensity = intensity[argsort]
        if self._is_pid_avail:
            pid = pid[argsort]
        formula = formula[argsort]
        is_isotopic = is_isotopic[argsort]
        weight = weight[argsort]

        # setup polarity
        if polarity is None:
            polarity = void
            self._polarity = None
        else:
            if np.array(polarity).shape == mz_theo.shape:
                polarity = np.array(polarity)[argsort]
                if len(set(polarity)) != 1:
                    raise ValueError(
                        "The calibration list contains several values"
                        f" of the polarity '{set(polarity)}'."
                    )
            else:
                polarity = np.full(mz_theo.shape, polarity)
            
            # keep the uniq polarity value.
            self._polarity = polarity[0]

        # set up the list of calibration points
        self._calibration_points = [CalibrationPoint(
            mz_theo=mz_theo[i],
            formula=formula[i],
            mz_exp=mz_exp[i],
            frequency=frequency[i],
            intensity=intensity[i],
            pid=pid[i],
            weight=weight[i],
            polarity=polarity[i],
            is_isotopic=is_isotopic[i]
        ) for i in range(len(mz_theo))]

        self.name = name
        self.plot = CalibListPlotter(self)

    @property
    def values(self):
        """Returns the main properties as a numpy array mz, mz_exp, frequency
        and intensity.
        """
        return np.concatenate([
            self.mz_theo[np.newaxis],
            self.mz_exp[np.newaxis],
            self.frequency[np.newaxis],
            self.intensity[np.newaxis]
        ]).transpose()

    @property
    def mz_theo(self):
        """Returns a numpy array with the calibration list values."""
        return np.array([item.mz_theo for item in self._calibration_points])
    
    @property
    def formula(self):
        """Returns the corresponding formulas of the calibration list."""
        # avoid the iteraction over formula composition
        formulas = [item.formula for item in self._calibration_points]
        array = np.empty(len(formulas), dtype=object)
        array[:] = formulas
        return formulas
    
    @property
    def err_ppm(self):
        """Returns the error in ppm if the experimental mz was given."""
        return np.array([item.err_ppm for item in self._calibration_points])
    
    @property
    def mz_exp(self):
        """ Returns the experimental mz values for each corresponding mz
            in the calibraion list. 
        """
        return np.array([item.mz_exp for item in self._calibration_points])
    
    @property 
    def frequency(self):
        """
        Returns the array of frequencies associated to the calibrant's 
        mz list.
        """
        return np.array([item.frequency for item in self._calibration_points])

    @property 
    def intensity(self):
        """
        Returns the array of relative abundances or intensities associated
        to the calibrant's mz list.
        """
        return np.array([item.intensity for item in self._calibration_points])

    @property 
    def pid(self):
        """
        Returns the array of peak id associated to the calibration points.
        """
        return np.array([item.pid for item in self._calibration_points])

    @property 
    def weights(self):
        """
        Returns the array of weights associated to the calibration points.
        """
        return np.array([item.weight for item in self._calibration_points])

    @property
    def polarity(self):
        """ Charge of the ionization. """
        return self._polarity
    
    @property
    def is_isotopic(self):
        """
        Returns the array of bool which define if the calibration point is
        isotopic or not.
        """
        return np.array([item.is_isotopic for item in self._calibration_points])

    @property
    def is_freq_avail(self) -> bool:
        """ True if frequency is available for recalibration """
        return self._is_freq_avail

    @property
    def is_intens_avail(self) -> bool:
        """ True if intensity is available for recalibration """
        return self._is_intens_avail

    @property
    def is_pid_avail(self) -> bool:
        """ True if peak ids (pid) are available. """
        return self._is_pid_avail

    @property
    def is_mz_exp_avail(self) -> bool:
        """ True if experimental m/z values are available """
        return self._is_mz_exp_avail

    @property
    def is_formula_avail(self) -> bool:
        """ True if molecular formula are available. """
        return self._is_formula_avail

    def to_dataframe(self):
        """ Returns a pandas DataFrame with the calibration list values """
        return pd.DataFrame([cp.as_dict() for cp in self._calibration_points])
    
    @classmethod
    def from_peaklist(
        cls,
        peaklist: PeakList,
        mz_theo: ArrayLike,
        lambda_parameter: float = 5.0,
        searching_mode: str = 'most_abundant',
        use_SN: bool = True,
        polarity: int = None,
        name: str = None,
    ):
        """
        Create a calibration list containing all the possible information
        as available in the peak list. For this purpose it is necessary to
        enter the original peak list and the mz values of the internal
        calibrants. The algorithm will look for the experimental m/z values
        in the peak list according to the searching mode and the error in ppm 
        (``lambda_parameter``). The ``mean_error`` option implements a
        correction on the searching_mode to select the peaks leading to
        the most abundant error.

        Args:
            peaklist (PeakList): the original peak list to search the 
                corresponding values of the internal calibrants to fill
                the calibration list experimental values.
            mz_theo (list): list of mz values of the internal calibrants.
            lambda_parameter (float): Range in ppm for searching exp m/z
                values from theoretical m/z values.
            searching_mode (string): Defines the criteria to select the best
                candidates for performing the calibration. Possibile values
                are ``'closest'``, ``'most_abundant'`` (default) or
                ``'most_abundant_corrected'``. Look at ``SearchingMode`` 
                in ``peak_search`` module for more details.
            use_SN (bool): If True, default, use S/N else, use intensity.
            polarity (int): Ionization charge
            name (str): name of the calibration list. Use the peaklist 
                name if it is None.
        
        Returns:
            A CalibList object.
        """

        if name is None:
            name = peaklist.name

        cl = CalibList(mz_theo=mz_theo, polarity=polarity, name=name)

        cl.search_mz_exp(peaklist=peaklist, lambda_parameter=lambda_parameter,
                         searching_mode=searching_mode, use_SN=use_SN,
                         inplace=True, dropna=True)
    
        return cl
 
    @classmethod
    def from_attributed_peaklist(
        cls, 
        peaklist: AttributedPeakList,
        weights: ArrayLike = None,
        name: str = None,
    ):
        """
        Create a calibration list from an attributed peak list. In that case
        the theoretical m/z values (calibrant m/z) are computed as the ion
        exact mass of the formulas.        

        Args:
            peaklist (AttributedPeakList): the attributed peak list defining
                the experimental m/z value and the related data.
            weights (ArrayLike): weights associated to each calibration point.
            name (str): name of the calibration list. Use the peaklist 
                name if it is None.
            
        Returns:
            A CalibList object.
        """
        # set default values of weights
        if weights is None:
            weights = np.ones_like(peaklist.mz, dtype=np.float64)
        else:
            if len(weights) != len(peaklist):
                raise ValueError(
                    "Shape mismatch, cannot combine peaklist and weights."
                    f"peaklist: {len(peaklist)}, weights {len(weights)}."
                )

        if name is None:
            name = peaklist.name

        # set up the calibration points
        calibration_points = list()
        for i in range(len(peaklist)):

            if not peaklist[i].is_attributed:
                continue

            peak = peaklist[i]
            if "frequency" in peak.properties:
                frequency = peak.properties["frequency"]
            else:
                frequency = np.nan

            calibration_points.append(CalibrationPoint(
                mz_theo=peak.formula.get_ion_exact_mass(peak.polarity),
                formula=str(peak.formula),
                mz_exp=peak.mz,
                frequency=frequency,
                intensity=peak.intensity, 
                pid=peak.pid,
                weight=weights[i],
                polarity=peak.polarity,
                is_isotopic=peak.formula.is_isotopic,
            ))

        return cls.from_list(calibration_points, name=name)

    @classmethod
    def from_list(cls, calibration_points: list, name: str = "calibration_list"):
        """
        Set up a calibration list from a list of calibration points.

        Args:
            calibration_points (list): a list of CalibrationPoints.
            name (str): name of the calibration list
        
        Returns:
            A CalibList object.
        """
        data = defaultdict(list)
        for cp in calibration_points:
            for k, v in cp.as_dict().items():
                data[k].append(v)

        if len(data) == 0:
            return cls()
        
        # remove err_ppm and void data
        data.pop("err_ppm")
        pop_keys = [k for k in data if k != "formula" and np.all(np.isnan(data[k]))]
        [data.pop(k) for k in pop_keys]

        if "polarity" in data:
            polarity = set(data.pop("polarity"))
            if len(polarity) > 1:
                raise ValueError("Polarity of calibration point is not unique.")
            polarity = polarity.pop()

            cl = cls(**data, polarity=polarity, name=name)
        else:
            cl = cls(**data, name=name)

        return cl
    
    @classmethod
    def from_dataframe(cls, dataframe: pd.DataFrame, dropna: bool = True,
                       polarity: int = None,
                       name: str = "calibration_list_from_df"):
        """
        Reading function for the calibration list from a dataframe. The
        function will try to use all the columns of the data frame. The
        polarity can be provided to keep a consistant link between formula
        and theoretical m/z values.

        Args:
            dataframe (pandas.DataFrame): a data frame containing at least the 
                theoretical m/z values of the calibrants.
            dropna (bool): if True, delete all columns that contains NaN.
            polarity (int): The ionization charge.
            name (str): name of the calibration list
        
        Returns:
            A CalibList object.

        """
        
        if dropna:
            dataframe = dataframe.dropna(axis='columns')

        # try to setup the calibration list with all the data
        d = dataframe.to_dict("list")
        try:
            cl = cls(**d, polarity=polarity)
            return cl
        except TypeError:
            pass

        # try to find columns
        # find the mass column index m_idx
        mz_theo_names = ['m/z', 'mz', 'mz_theo', 'calibrant', "calibrant mz"]
        m_idx, = np.nonzero(dataframe.columns.str.lower().isin(mz_theo_names))
        if m_idx.size == 0:
            raise ValueError('The argument must be a data frame containing'
                             ' at least one column with theoretical m/z.')
    
        m_idx = m_idx[0]
        d = {"mz_theo": dataframe.iloc[:, m_idx].values}

        # try to add other column
        columns = ["intensity", "frequency", "mz_exp", "weight", "pid", 
                   "is_isotopic"]
        d.update({k: dataframe.loc[:, k].values for k in columns if k in dataframe})

        return cls(**d, polarity=polarity, name=name)

    def search_mz_exp(
        self,
        peaklist: PeakList,
        lambda_parameter: float = 2.0,
        searching_mode: str = 'most_abundant',
        use_SN: bool = True,
        inplace: bool = False,
        dropna: bool = True,
    ) -> Union[CalibList, None]:
        """ Look into a provided peak list for the experimental mz values
        corresponding to the already available theoretical mz values.

        Args:
            peaklist (PeakList): the peak list into which search the 
                corresponding values of the internal calibrants to fill
                the calibration list experimental values.
            lambda_parameter (float): Range in ppm for searching exp m/z
                values from theoretical m/z values.
            searching_mode (string): Defines the criteria to select the best
                candidates for performing the calibration. Possible values
                are ``'closest'``, ``'most_abundant'`` (default) or
                ``'most_abundant_corrected'``. Look at ``SearchingMode`` 
                in ``peak_search`` module for more details.
            use_SN (bool): If True, default, use S/N else, use intensity.
            inplace (bool): If True, the calibration list is modified inplace.
                If False (default), a new calibration list is returned
            dropna (bool): If True and inplace is True, remove from the
                calibration list, points for which neither experimental mz
                was found.

        Returns:
            A new calibration list if inplace is True.
    
        """

        mz_theo = self.mz_theo
        min_mz_theo, max_mz_theo = mz_theo.min(), mz_theo.max()
        
        if not isinstance(peaklist, PeakList):
            peaklist = PeakList(mz=peaklist, 
                                intensity=np.full(peaklist, np.nan))
    
        # check if peaklist is out of mz theo bounds
        pl_mz_min, pl_mz_max = peaklist.get_min_max_mz()
        if (pl_mz_max < min_mz_theo) and (pl_mz_min > max_mz_theo):
            # calibration list is empty
            if not inplace:
                return CalibList()
        
        # search peaks
        searching_mode = SearchingMode(searching_mode)
        idx, peaks, weights = candidates_searching(
            peaklist, mz_theo, lambda_parameter, searching_mode, use_SN)

        # if no candidate was found (which is possible) return an empty list
        if len(peaks) == 0:
            if not inplace:
                return CalibList()

        calibration_points = list()
        for i, peak in enumerate(peaks):
            if "frequency" in peak.properties:
                frequency = peak.properties["frequency"]
            else:
                frequency = np.nan

            if inplace:
                cp = self._calibration_points[idx[i]]
                cp.mz_exp = peak.mz
                cp.pid = peak.pid
                cp.intensity = peak.intensity
                cp.frequency = frequency
                cp.weight = weights[i]

            else:
                d_cp = self._calibration_points[idx[i]].as_dict()
                d_cp.pop("err_ppm")
                d_cp.update({
                    "mz_exp": peak.mz,
                    "intensity": peak.intensity,
                    "pid": peak.pid,
                    "weight": weights[i],
                    "frequency": frequency,
                })
                calibration_points.append(CalibrationPoint(**d_cp))

        if inplace:
            # check which data are available
            self._is_mz_exp_avail = True
            if not np.any(np.isnan(self.intensity)):
                self._is_intens_avail = True
            if not np.any(np.isnan(self.intensity)):
                self._is_freq_avail = True
            if not np.any(np.isnan(self.pid)):
                self._is_pid_avail = True

            if dropna:
                # remove points without mz experimental values
                idx_remove = list()
                for idx, cp in enumerate(self._calibration_points):
                    if np.isnan(cp.mz_exp):
                        idx_remove.append(idx)
                
                # reverse in order to start from the end and pop points
                idx_remove.reverse()
                for idx in idx_remove:
                    self.pop(idx)

        else:
            # return the new calibration list
            return CalibList.from_list(
                calibration_points, name=f"new_{self.name}")

    def update_weights(self, k: int = 2, reset: bool = False):
        """ Update the weights from the value of the current mean error 
        and standard error deviation. If experimental m/z values are not
        available, weights are set to 1.

        Args:
            k (int): multiplication factor of the standard deviation error
            reset (bool): If True, reset all weights to 1 (default False)
        
        """
        if not self._is_mz_exp_avail or reset:
            weights = np.ones_like(self.mz_theo)
        else:
            # error calculation
            err = (self.mz_theo - self.mz_exp) / self.mz_theo * 1e6
            mean_err = np.mean(err)
            err_vs_mean = np.abs(err - mean_err)
            stddev_err = np.std(err)

            # Weights for regression
            weights = np.where(
                err_vs_mean < k * stddev_err,
                err,
                mean_err + err_vs_mean
            )
            # eliminate zeros to prevent division by zero 
            weights[weights == 0] = 1e-7
            weights = 1 / weights ** 2

        for i, cp in enumerate(self._calibration_points):
            cp.weight = weights[i]

    def update_mz_exp(self, mz_exp: ArrayLike, inplace: bool = False) -> CalibList:
        """ Update the experimental m/z values. For example, this method
        can be called after the recalibration to obtain a new calibration
        list with the new calibrated experimental m/z values.

        Args:
            mz_exp (array): The m/z experimental value
            inplace (bool): If False (default) returns a new calibration list

        Returns:
            A new calibration list.
        
        """
        if len(self._calibration_points) != len(mz_exp):
            raise ValueError(
                f"Length mismatch between mz_exp ({len(mz_exp)}) and the "
                f"calibration list ({len(self._calibration_points)})"
            )

        if inplace:
            for i, cp in enumerate(self._calibration_points):
                cp.mz_exp = mz_exp[i]
        else:
            calibration_points = list()
            for i, cp in enumerate(self._calibration_points):
                # make a deepcopy of the calibration list
                d_cp = cp.as_dict()
                d_cp["mz_exp"] = mz_exp[i]
                d_cp.pop("err_ppm")
                calibration_points.append(CalibrationPoint(**d_cp))
                
            return CalibList.from_list(
                calibration_points, name=f"updated_{self.name}")

    def to_csv(self, filename='calib_list.csv', **kwargs):
        """
        Generates a CSV file from the calibration list. 

        Args:
            filename (str): the filename with the CSV extension.
            kwargs of pandas.DataFrame.to_csv()
        """
        self.to_dataframe().dropna(axis='columns').to_csv(filename, **kwargs)
    
    def add_isotopes(
        self,
        peaklist: PeakList = None,
        lambda_parameter: float = None,
        searching_mode: str = 'most_abundant',
        use_SN: bool = True,
        inplace: bool = False,
    ) -> Union[None, CalibList]:
        """
        From all peaks selected in the calibration list, try to find
        isotopic peaks in the provided peak list to extend the calibration
        list. Peaks with 1 or 2 13C isotopes are selected in a given 
        error range.

        Args:
            peaklist (PeakList): the original peak list to search the 
                corresponding values of the iternal calibrants to fill
                the peak list.
            lambda_parameter (float): Range in ppm for searching exp m/z
                values from theoretical m/z values. if None, the range
                will be computed as 3 times the standard deviation error of
                the no isotopic calibration points.
            searching_mode (string): Defines the criteria to select the best
                candidates for performing the calibration. Possibile values
                are ``'closest'``, ``'most_abundant'`` (default) or
                ``'most_abundant_corrected'``. Look at ``SearchingMode`` 
                in ``peak_search`` module for more details.
            use_SN (bool): If True, default, use S/N else, use intensity.
            inplace (bool): If False (default) returns a new calibration list

        Returns:
            The expanded calibration list containing the 13C1 and 13C2
            isotopomers, if inplace is False.
        """

        # isotopic data
        C12 = Isotope(6, 12)
        C13 = Isotope(6, 13)
        deltaC12C13 = C13.exact_mass - C12.exact_mass

        if self._is_mz_exp_avail and (peaklist is not None):
            
            # polarity
            if self._polarity is not None:            
                charge = abs(self._polarity)
            else:
                raise ValueError(
                    "Polarity is not known and is needed to search isotopes.")
            
            # define target isotopic m/z
            mz_theo = self.mz_theo
            mz_theo_iso = np.concatenate([mz_theo + deltaC12C13 / charge,
                                          mz_theo + 2 * deltaC12C13 / charge])            
            target_mz = mz_theo_iso.copy()

            # set up compositions
            if self._is_formula_avail:    
                compositions = [Composition.from_string(str(formula))
                                for formula in self.formula]
                
                iso_compositions = list()
                for n in range(2):
                    iso_compositions.extend([
                        c.insert_isotope(6, 13, n + 1) for c in compositions
                    ])
            else:
                iso_compositions = np.full(mz_theo_iso.shape, "")

            if lambda_parameter is None:
                # compute the current mean error between mz_theo and mz_exp
                # select isotopic peak trying to minimize the error standard
                # deviation
                median_err = np.median(self.err_ppm)
                stddev_err = np.std(self.err_ppm)
                lambda_parameter = 3 * stddev_err  # in ppm
            
                # add a correction to the points
                target_mz += target_mz * median_err * 1e-6

            # search peak
            searching_mode = SearchingMode(searching_mode)
            iso_idx, peaks, weights = candidates_searching(
                peaklist, target_mz, lambda_parameter, searching_mode, use_SN)

            # pid avail
            if self._is_pid_avail:
                pids = set(self.pid)

            new_calibration_points = list()
            for i, peak in enumerate(peaks):
                if "frequency" in peak.properties:
                    frequency = peak.properties["frequency"]
                else:
                    frequency = np.nan

                # check if peak id already exist
                if self._is_pid_avail:
                    if peak.pid in pids:
                        continue
                else:
                    # check if peak is already present
                    rel_err = np.abs(self.mz_theo - mz_theo_iso[iso_idx[i]])
                    rel_err = rel_err / mz_theo_iso[iso_idx[i]] * 1e6
                    idx, = np.nonzero(rel_err < lambda_parameter)
                    if len(idx) != 0:
                        continue

                # add calibration point
                new_calibration_points.append(CalibrationPoint(
                    mz_theo=mz_theo_iso[iso_idx[i]],
                    mz_exp=peak.mz,
                    intensity=peak.intensity,
                    pid=peak.pid,
                    formula=str(iso_compositions[iso_idx[i]]),
                    polarity=self._polarity,
                    weight=weights[i],
                    frequency=frequency,
                    is_isotopic=True,
                ))

            if inplace:
                self.extend(new_calibration_points)
            else:
                return CalibList.from_list(
                    self._calibration_points + new_calibration_points,
                    name=f"{self.name}_w_isotopes")

        else:
            # the caliblist does not contain experimental m/z values or
            # the peaklist was not provided, extend the theoretical m/z
            # values with isotopes
            mz_theo = self.mz_theo

            if self._is_mz_exp_avail:
                warnings.warn(
                    "You ask to add isotopes in the calibration list but "
                    "the peaklist was not provided but experimental m/z values"
                    " are already available. If you want a list of "
                    "theoretical m/z including isotopes, get first the raw "
                    "calibration list using get_raw_caliblist().",
                    UserWarning, stacklevel=2)
                iso_mz_theo = np.array([])
            else:
                iso_mz_theo = np.concatenate([mz_theo + deltaC12C13,
                                              mz_theo + 2 * deltaC12C13])
    
            if inplace:
                iso_cl = CalibList(
                    mz_theo=iso_mz_theo, polarity=self.polarity,
                    is_isotopic=np.full(len(iso_mz_theo), True)
                )
                self.extend(iso_cl)
            else:
                return CalibList(
                    mz_theo=np.concatenate((mz_theo, iso_mz_theo)),
                    is_isotopic=np.concatenate(
                        (np.full(len(mz_theo), False),
                         np.full(len(iso_mz_theo), True))
                    ),
                    polarity=self.polarity,
                    name="{self.name}_w_isotopes"
                )
    
    def get_monoisotopic_caliblist(self) -> CalibList:
        """ Return a new calibration list with only monoistopic peaks.
        There is a caveat here because, by default, all calibration point
        are set as non-isotopic. If the list was extended using isotopic
        expension function, new calibration points are set as isotopic. But,
        if you added points by hand the results may be wrong.
        """
        return CalibList.from_list([
            cp for cp in self._calibration_points if not cp.is_isotopic
        ], name=self.name)

    def get_raw_caliblist(self) -> CalibList:
        """ Return a new calibration list with only based information:
        theoretical m/z, formula and polarity. Isotopic formula are also
        removed.
        """
        mono_caliblist = self.get_monoisotopic_caliblist()
        if self._is_formula_avail:
            formula = mono_caliblist.formula
        else:
            formula = None
        return CalibList(mz_theo=mono_caliblist.mz_theo,
                         formula=formula,
                         polarity=mono_caliblist.polarity,
                         name=self.name)

    def calculate_exp_mz(self, A: float, B: float, inplace=True):
        """ If the calibration list comes from Predator the experimental
        m/z values may be missing.  In that case, the experimental m/z values
        can be computed from the frequency using the A and B coefficient.
        The A and B coefficients are the parameters of the two terms
        walking equation which reads ``A / f + B / f**2``. This function
        compute the experimental m/z values and returns a new calibration
        list or overwrite experimental m/z values.
        
        Args:
            A (float): linear parameter.
            B (float): quadratic parameter.
            inplace (bool): if True, substitue the expermiental mz
                attribute.
        
        Returns:
            None or a new calibration list.

        """
        if not self._is_freq_avail:
            raise ValueError("Frequency are not available.")
        
        mz_exp = A / self.frequency + B / self.frequency**2

        if inplace:
            for i, cp in enumerate(self._calibration_points):
                cp.mz_exp = mz_exp[i]
        else:
            calibration_points = self._calibration_points.copy()
            for i, cp in enumerate(calibration_points):
                cp.mz_exp = mz_exp[i]

            return CalibList.from_list(calibration_points)

    def filter_outliers(self, inplace: bool = True):
        """ Estimate outliers using the Thiel-Sen Estimator and remove
        the corresponding point from the calibration list. 
        
        TODO: move the default to False
        Args:
            inplace (bool): if True modify the calibration list in place

        Returns:
            A new calibration list if inplace is True

        """

        if not self._is_mz_exp_avail:
            raise ValueError("Experimental m/z values are not available.")
        
        # Eliminate Outliers using the 
        outliers_idx = outliers_detection_TSE(self.mz_theo, self.mz_exp)

        # remove outliers from the end of the list
        outliers_idx = np.sort(outliers_idx)[::-1]
        if inplace:
            for idx in outliers_idx:
                self._calibration_points.pop(idx)
        else:
            cl_points = [
                pts for i, pts in enumerate(self._calibration_points)
                if i not in outliers_idx
            ]
            return CalibList.from_list(cl_points, name=f"filtered_{self.name}")

    def pop(self, idx: int = -1) -> CalibrationPoint:
        """ Remove item at index idx from the list and return item.
        TODO: it means CalibList may be MutableSequence. If True, it is 
        necessary to implement moreover __setitem__, __delitem__ and insert.
        In that case, pop will be automatically implemented.
          
        """
        item = self._calibration_points.pop(idx)
        return item
    
    def append(self, cp: CalibrationPoint):
        """ Append item to the list of calibration points and check if all
        the data (mz_exp, frequency ... ) are still available.

        TODO: it means CalibList may be MutableSequence. If True, it is 
        necessary to implement moreover __setitem__, __delitem__ and insert.
        In that case, pop will be automatically implemented.
          
        """
        if not isinstance(cp, CalibrationPoint):
            raise TypeError("You must provide a CalibrationPoint but "
                            f"cp is <{type(cp)}>: {cp}")
        
        if np.isnan(cp.mz_exp):
            self._is_mz_exp_avail = False
        if np.isnan(cp.frequency):
            self._is_freq_avail = False
        if np.isnan(cp.intensity):
            self._is_intens_avail = False
        if np.isnan(cp.pid):
            self._is_pid_avail = False
        if cp.formula == "":
            self._is_formula_avail = False
        
        self._calibration_points.append(cp)

    def extend(
        self,
        other: Union[CalibList, list, pd.DataFrame, PeakList, AttributedPeakList],
        lambda_parameter: float = None,
        **kwargs
    ):
        """ Extend the calibration list with new calibration points provided 
        from: another ``CalibList`` object, a pandas DataFrame, an
        ``AttributedPeakList``, a list of ``CalibrationPoints`` or a
        ``PeakList``. In this later case you must provide also theoretical
        m/z values.

        If peak ids (pid) are available, duplicated peaks are not added to
        the list. If you provide ``lambda_parameter``, it is used to determine
        if a peak is a duplicate.
        
        Args:
            other (CalibList, list, pd.DataFrame, PeakList, AttributedPeakList):
                The object from which you want to extend the current
                calibration list.
            lambda_parameter (float): Relative error in ppm to define is a 
                calibration points is already present in the calibration list.
            kwargs are passed to the corresponding class method.

        """
        if self._is_pid_avail:
            pid = set(self.pid)

        if isinstance(other, CalibList):
            # nothing to do
            pass

        elif isinstance(other, pd.DataFrame):
            if "polarity" in kwargs:
                polarity = kwargs["polarity"]
            elif "polarity" in other:
                polarity = set(other["polarity"].values).pop()
            else:
                polarity = None
            other = CalibList.from_dataframe(other, polarity=polarity)

        elif isinstance(other, AttributedPeakList):
            weights = np.ones_like(other.mz)
            if "weights" in kwargs:
                weights = kwargs["weights"]
            other = CalibList.from_attributed_peaklist(other, weights)

        elif isinstance(other, list):
            other = CalibList.from_list(other)

        elif isinstance(other, PeakList):
            if "mz_theo" in kwargs:
                mz_theo = kwargs.pop("mz_theo")
            else:
                raise TypeError("Theoretical m/z are missing.")
            other = CalibList.from_peaklist(peaklist=other, mz_theo=mz_theo,
                                            **kwargs)
    
        else:
            raise TypeError("Cannot extend the calibration list from "
                            f"type {type(other)}.")

        if self._is_pid_avail and other._is_pid_avail:
            # compare pid and select points if they are not in the list
            calibration_points = list()
            for cp in other._calibration_points:
                if cp.pid not in pid:
                    calibration_points.append(cp)
        
        elif lambda_parameter is not None:
            # check if the mz_theo value already exists or not according
            # to lambda_parameter
            calibration_points = list()
            mz_theo = self.mz_theo
            for cp in other._calibration_points:
                error = np.abs(mz_theo - cp.mz_theo) / cp.mz_theo * 1e6
                idx, = np.nonzero(error < lambda_parameter)
                if len(idx) == 0:
                    calibration_points.append(cp)

        else:
            # include new points without any control.
            calibration_points = other._calibration_points

        # include new calibration points
        self._calibration_points.extend(calibration_points)

        # sort the calibration list
        self.sort()

    def sort(self):
        """ Sort inplace the Calibration list based on the calibrant mz """
        self._calibration_points.sort()

    def to_ref_file(self, path: str = "calib.ref", mono_isotopic: bool = True):
        """ Export the calibration list in ref format. 
        
        Args:
            path (str): paht of output file 
            mono_isotopic (bool): If True (default), only monoisotopic formula
                are exported.
        """
        if mono_isotopic:
            df = self.get_monoisotopic_caliblist().to_dataframe()
        else:
            df = self.to_dataframe()

        df = df[["formula", "mz_theo", "polarity"]]
        df["formula"] = [str(f).replace(" ", "") for f in df["formula"].values]
        df["polarity"] = df["polarity"].map({-1: "1-", 1: "1+"})

        csv = df.to_csv(sep=" ", index=False, float_format="%12.6f", quotechar=" ")
        header = "# Calibration list from PyC2MC\n\n"

        csv = header + csv
        with open(path, "w") as fout:
            fout.write(csv)

    def get_walking_segments(
        self,
        min_npts_per_segment: int = 10,
        min_segment_size: float = 25,
    ) -> list:
        """ Determine the segments' edges to be used for a walking calibration
        using as criteria, a minimum number of calibration points per
        segment and a minimum segment size.
        
        Args:
            min_npts_per_segment (int): Minimum number of point per segment.
            min_segment_size (float): Minimum segment width.
        
        Returns:
            List of edges
        """
        mz_theo = np.sort(self.mz_theo)

        mzmin = (1.0 - 1e-6) * mz_theo.min()  # - 1 ppm to include first point
        mzmax = (1.0 + 1e-6) * mz_theo.max()  # + 1ppm to include last point

        edges = [mzmin]
        while edges[-1] < mzmax:
            new_edge = edges[-1] + min_segment_size

            # check how much points are in the current segment
            idx, = np.where((edges[-1] < mz_theo) & (mz_theo <= new_edge))
            npts = len(idx)
        
            if npts >= min_npts_per_segment:
                edges.append(new_edge)
            else:
                # extend segment to add points
                npts_missing = min_npts_per_segment - npts
                if len(idx) == 0:
                    idx = np.searchsorted(mz_theo, edges[-1])
                    last_idx = np.searchsorted(mz_theo, edges[-1]) + npts_missing
                else:
                    last_idx = idx[-1] + npts_missing

                if last_idx < len(mz_theo) - 1:
                    new_edge = (1.0 + 1e-6) * mz_theo[last_idx]
                    edges.append(new_edge)
                else:
                    # there is not enough point is the last segment.
                    # extend the last segment to mzmax
                    edges[-1] = mzmax
                    
        return edges

    def __repr__(self):
        out = [f"CalibList ({len(self._calibration_points)} peaks) "
               f"{self.mz_theo.min():.2f} -> {self.mz_theo.max():.2f}"]
        if len(self._calibration_points) > MAX_PEAKS:
            for peak in self._calibration_points[:6]:
                out.append(repr(peak))
            out.append("...")
            for peak in self._calibration_points[-5:]:
                out.append(repr(peak))
        else:
            for peak in self._calibration_points:
                out.append(repr(peak))
        return "\n".join(out)

    def __len__(self):
        return len(self._calibration_points)
    
    def __getitem__(self, item: Union[int, slice]):
        """ Return the required calibration point if item is an integer
        or a new CalibList if item is a slice object. """
        # if isinstance(item, slice):
        #     assume the current calibration list is already well sorted

        if isinstance(item, slice):
            start = item.start if item.start is not None else 0
            step = item.step if item.step is not None else 1
            if item.stop is None:
                stop = len(self._calibration_points) if step > 0 else -1
            else:
                stop = item.stop
            if start == stop:
                return CalibList()
            else:
                return CalibList.from_list(
                    [self._calibration_points[i] for i in range(start, stop, step)]
                )
        else:
            return self._calibration_points[item]


def candidates_searching(
    peaklist: PeakList,
    mz_theo: list,
    lambda_parameter: float = 10,
    searching_mode: SearchingMode = SearchingMode.most_abundant,
    use_SN: bool = True,
):
    """
    This function looks for experimental m/z values in the peaklist
    corresponding to the theoretical m/z values (the calibrants). The
    selection of m/z values in the peaklist relies on the searching mode
    and the error defined in ppm from the ``lambda_parameter``.

    Args:
        peaklist (pyc2mc.core.peak.PeakList): the target peak list to
            search the experimental peaks.
        mz_theo (list): the list of theoretical m/z, the calibrant's mz.
        lambda_parameter (float64): Range in ppm for searching exp m/z values
            based on the theoretical m/z values.
        searching_mode (SearchingMode): the chosen searching mode.
        use_SN (bool): If True, default, use S/N else, use intensity.

    Returns:
        A tuple containing a list of arrays and the weights
        matrix. The list contains 4 arrays. The first element is a list
        with the theoretical values, the second element is the list with
        the corresponding experimental values, the third one is the list
        of frequencies, and the fourth are the corresponding intensities.
    """

    # look for experimental mz
    idx, peaks = searching_mode(mz_theo, peaklist, lambda_parameter,
                                use_SN=use_SN)

    if len(peaks) != 0:
        # error calculation
        # computer error averages
        err = [(peak.mz - mz_theo[i]) / mz_theo[i] * 1e6 
               for i, peak in zip(idx, peaks)]

        mean_err = np.mean(err)
        err_vs_mean = np.abs(err - mean_err)
        stddev_err = np.std(err)

        # Weights for regression
        weights = np.where(
            err_vs_mean < 2 * stddev_err,
            err,
            mean_err + err_vs_mean
        )
        # eliminate zeros to prevent division by zero 
        weights[weights == 0] = 1e-7
        weights = 1 / weights ** 2
    else:
        weights = np.array([])
    
    return idx, peaks, weights
