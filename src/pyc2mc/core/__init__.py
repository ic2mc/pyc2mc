# coding: utf-8

""" The core module provides core classes which support the importation
of data, the processing and then analyzes of the data. """

# from typing import Union
from enum import Enum, unique


@unique
class SampleType(str, Enum):
    """ This class list all possible sample types """
    raw_1D = "1D"
    raw_time_dependent = "TD"
    raw_2D = "2D"

    att_1D = "att_1D"
    att_time_dependent = "att_TD"
    att_2D = "att_2D"

    caliblist = "caliblist"

    def is_1D(self):
        """ return True if it is 1D data """
        return self.value in [self.raw_1D, self.att_1D]

    def is_TD(self):
        """ return True if it is Time Dependent """
        return self.value in [self.raw_time_dependent, self.att_time_dependent]

    def is_attributed(self):
        """ Return True if the sample contains attributed data """
        return self.value in [self.att_1D, self.att_time_dependent,
                              self.att_2D]
