# coding: utf-8

from typing import Sequence, Union
from collections import abc

import numpy as np
from numpy.typing import ArrayLike
import pandas as pd
import csv
from pathlib import Path

from pyc2mc.utils import fill_with_na, smoothing_method
from pyc2mc.core.isotopes import all_exact_masses
from pyc2mc.core.peak import PeakList, AttributedPeakList, Peak, AttributedPeak
from pyc2mc.core.formula import Formula, Composition, get_formula, get_species
from pyc2mc.core.formula_grid import FormulaGrid
from pyc2mc.processing.attribution import AttributionAlgorithm
from pyc2mc.time_dependent.td_data import AttributedTimeDependentData
from pyc2mc.plotinator.roi_plotter import ROIPlotter, ROIListPlotter

MAX_ROIS = 50


class ROI(abc.Sequence):
    """
    This class describe a single Region of Interest (ROI) or Feature object.
    It provides the data associated to one ROI including, the mz, intensity,
    S/N ratio (if it exists) and the scan's indexes corresponding to the
    feature.

    Attributes:
        scans (ArrayLike): Array of scans index belonging to the ROI
        mz (ArrayLike): m/z values of the ROI
        intensity (ArrayLike): Intensity of the points in the ROI
        SN (ArrayLike): S/N values of the ROI
        roi_id (int): Uniq identifier of the ROI
        dt (float): Time step between scans
        t0 (float): Time of scan 0.

    """

    def __init__(
        self,
        scans: Sequence[int],
        mz: Sequence[float],
        intensity: Sequence[float],
        SN: float = None,
        roi_id: int = None,
        dt: float = 1.,
        t0: float = 0.,
    ):
        """
        Initialize the ROI.

        Args:
            scans (list): a list that contains the grouped scans of a
                Region of Interest.
            mz (list): m/z values of the corresponding scans.
            intensity (list): intensity for the corresponding scans.
            SN (list): S/N for the corresponding scans
            roi_id (int): ROI ID.
            dt (float): Time step between scans
            t0 (float): Time of the first scan in the original time dependent
                data used to build the ROI.
        """

        self.scans = np.array(scans, dtype=np.int32)
        self.mz = np.array(mz, dtype=np.float64)
        self.intensity = np.array(intensity, dtype=np.float64)
        if SN is None:
            self._SN_avail = False
            self.SN = None
        else:
            self.SN = np.array(SN, dtype=np.float64)

        self.roi_id = roi_id if roi_id is not None else 0
        self.dt = dt
        self.t0 = t0

        # set up properties
        diff = np.diff(self.scans)
        self._is_continuous = np.all(diff == 1)
        self._largest_hole = np.max(diff) - 1
        self._total_intensity = np.nansum(self.intensity)
        self._eic = fill_with_na(intensity, self.scans, self.start, self.end)
        self._mz_mean = np.average(self.mz, weights=self.intensity)
        self._mz_std = np.sqrt(np.average(
            (self.mz - self._mz_mean) ** 2, weights=self.intensity))
        self._is_attributed = False

        self.plot = ROIPlotter(self)

    @property
    def start(self):
        """ First scan value. """
        return self.scans[0]

    @property
    def end(self):
        """ Last scan value. """
        return self.scans[-1]

    @property
    def mz_mean(self):
        """ The average of m/z values of the ROI weighted by intensity """
        return self._mz_mean

    @property
    def mz_std(self):
        """ The standard deviation of m/z values of the ROI weighted by intensity """
        return self._mz_std

    @property
    def SN_avail(self):
        """ True if S/N ratio is available, False if not """
        return self._SN_avail

    @property
    def extractive_ion_chromatogram(self):
        """
        Obtain the extractive ion chromatogram list between the start
        and end values, filling with zero the empty values.
        """
        return self._eic

    @property
    def total_intensity(self):
        """
        Calculate the total intensity as the sum of the intensity for all
        points in the Region of Interest.
        """
        return self._total_intensity
    
    @property
    def is_continuous(self):
        """ True if the ROI is continuous """
        return self._is_continuous

    @property
    def largest_hole(self):
        """ Return the length of the largest interval of missing values
        along the time axis. """
        return self._largest_hole
    
    @property
    def is_attributed(self) -> bool:
        """ Bool to define if ROI is attributed. For compatibility between ROI
        and attributed ROI. """
        return self._is_attributed
    
    def get_peak_index(self, scan: int = 0) -> int:
        """ Return the peak index in the ROI corresponding to a given
        scan index in the scans attribute.

        Args:
            scan (int): scan number

        Returns:
            index (int): the index of the peak in the ROI.

        """
        idx, = np.nonzero(self.scans == scan)
        if len(idx) == 1:
            return idx[0]
        else:
            raise IndexError(f"Scan {scan} not in the ROI.")
    
    def get_peak(self, scan: int = 0) -> int:
        """ Return the peak in the ROI corresponding to a given
        scan index in the scans attribute.

        Args:
            scan (int): scan number

        Returns:
            the peak in the ROI for the given scan

        """
        return self[self.get_peak_index(scan)]

    def isin_scan_interval(self, start: int, end: int, n_scans: int = 1) -> bool:
        """ Return True if at least ``n_scans`` scans of the current ROI
        belongs to the given interval defined by start and end.

        Args:
            start (int): first index of scans interval
            end (int): last index of scans interval 
            n_scans (int): number of scan to be in the interval

        Returns
            True if n_scans scans belong to the interval
        """
        idx, = np.nonzero((start <= self.scans) & (self.scans <= end))
        return len(idx) >= n_scans

    def get_interval_overlap(self, start: int, end: int) -> float:
        """ Compute the ration between the number of scans of the ROI which
        belong to the interval defined by start and end, and the number of
        scans in this interval.

        Args:
            start (int): first index of scans interval
            end (int): last index of scans interval 
        
        Returns:
            The overlap between 0 and 1.

        """
        idx, = np.nonzero((start <= self.scans) & (self.scans <= end))
        percentage = len(idx) / (end - start + 1)
        return percentage
    
    def to_dict(self):
        """ Returns a dictionnary containing the main information. """
        SN = list(self.SN) if self.SN_avail else np.nan
        return {
            'mz_mean': self._mz_mean,
            "mz_std": self._mz_std,
            'total_intensity': self._total_intensity,
            'start': self.start,
            'end': self.end,
            'EIC': list(self._eic),
            'mz': list(fill_with_na(self.mz, self.scans, self.start, self.end)),
            "SN": SN,
            "attributed": self._is_attributed,
        }
    
    def __getitem__(self, item: Union[int, slice]):
        """ return a point """
        SN = self.SN[item] if self._SN_avail else None
        if isinstance(item, slice):
            return ROI(scans=self.scans[item], mz=self.mz[item],
                       intensity=self.intensity[item], SN=SN,
                       roi_id=self.roi_id)
        else:
            return Peak(mz=self.mz[item], intensity=self.intensity[item],
                        SN=SN,
                        properties=dict(roi=self.roi_id, scan=self.scans[item]))

    def __len__(self):
        return len(self.scans)

    def __repr__(self):
        return f"ROI: mz={self._mz_mean:.2f} from scans {self.start} to {self.end}." 


class AttributedROI(ROI):
    """
    This class describe a single Region of Interest (ROI) or Feature object.
    which is attributed with a molecular formula. Additionally to a 
    normal ROI, it provides the molecular formula associated to the ROI.

    Attributes:
        scans (ArrayLike): Array of scans index belonging to the ROI
        mz (ArrayLike): m/z values of the ROI
        intensity (ArrayLike): Intensity of the points in the ROI
        SN (ArrayLike): S/N values of the ROI
        polarity (int): ionization charge
        roi_id (int): Uniq identifier of the ROI
        dt (float): Time step between scans
        t0 (float): Time of scan 0.

    """

    def __init__(
        self,
        scans: Sequence[int],
        mz: Sequence[float],
        intensity: Sequence[float],
        SN: float = None,
        formula: Formula = None,
        polarity: int = None,
        roi_id: int = None,
        dt: float = 1.,
        t0: float = 0.,
        exact_masses: dict = None,
    ):
        """
        Initialize the attributed ROI.

        Args:
            scans (list): a list that contains the grouped scans of a
                Region of Interest.
            mz (list): m/z values of the corresponding scans.
            intensity (list): intensity for the corresponding scans.
            SN (list): S/N for the corresponding scans
            formula (Formula): The molecular formula
            polarity (int): Ionization charge
            roi_id (int): ROI ID.
            dt (float): Time step between scans
            t0 (float): Time of the first scan in the original time dependent
                data used to build the ROI.
            exact_masses (dict): dict of species and exact masses.
        
        """
        super().__init__(scans=scans, mz=mz, intensity=intensity, SN=SN,
                         roi_id=roi_id, dt=dt, t0=t0)
        if not isinstance(formula, Formula):
            formula = get_formula(formula, exact_masses=exact_masses)
        
        if formula:
            self._formula = formula
            self._is_attributed = True
            self._ion_exact_mass = self._formula.get_ion_exact_mass(polarity)
        else:
            self._formula = None
            self._is_attributed = False
            self._ion_exact_mass = np.nan

        self.polarity = polarity

    @property
    def formula(self) -> Formula:
        """ Molecular formula of the ROI """
        return self._formula
    
    @property
    def mean_error_ppm(self):
        """ Return the averaged mean error weighted by intensity """
        return np.average(
            (self.mz - self._ion_exact_mass) / self._ion_exact_mass * 1e6,
            weights=self.intensity)
    
    @property
    def error_ppm(self):
        """ Calculates the error in ppm with respect to mz_mean. """
        return self.formula.error_ppm(self.polarity, self._mz_mean)

    def to_dict(self):
        """ Returns a dictionnary containing the main information."""
        SN = list(self.SN) if self.SN_avail else np.nan
        return {
            'mz_mean': self._mz_mean,
            "mz_std": self._mz_std,
            'total_intensity': self._total_intensity,
            'start': self.start,
            'end': self.end,
            'EIC': list(self._eic),
            'mz': list(fill_with_na(self.mz, self.scans, self.start, self.end)),
            "SN": SN,
            "polarity": self.polarity,
            "formula": str(self._formula) if self._is_attributed else "",
            "attributed": self._is_attributed,
            "error_ppm": self.error_ppm if self._is_attributed else np.nan,
            "mean_error_ppm": self.mean_error_ppm,
        }

    def __getitem__(self, item: Union[int, slice]):
        """ return a point """
        SN = self.SN[item] if self._SN_avail else None
        if isinstance(item, slice):
            return AttributedROI(
                scans=self.scans[item], mz=self.mz[item],
                polarity=self.polarity,
                formula=self.formula, intensity=self.intensity[item], SN=SN,
                roi_id=self.roi_id)
        else:
            properties = dict(roi=self.roi_id, scan=self.scans[item])
            if self._is_attributed:
                return AttributedPeak(
                    mz=self.mz[item], intensity=self.intensity[item],
                    polarity=self.polarity,
                    formula=self.formula, SN=SN,  properties=properties)
            else:
                return Peak(
                    mz=self.mz[item], intensity=self.intensity[item],
                    SN=SN, properties=properties)

    def __repr__(self):
        return (f"ROI: mz={self._mz_mean:.2f} ({str(self._formula)}) " 
                f"from scans {self.start} to {self.end}.")


class ROIList(abc.Sequence):
    """
    This class contains the methods to manage a list of ROI.
    """

    def __init__(
        self,
        scans: Sequence[list],
        mz: Sequence[list],
        intensity: Sequence[list],
        SN: Sequence[float] = None,
        roi_ids: Sequence[int] = None,
        name: str = '',
        dt: float = 1.0,
        t0: float = 0.0,
        sort_roi: bool = False
    ):
        """
        Initialize the ROIs.

        Args:
            scans (list of lists): Is a sequence containing the list
                of grouped scans for the analyzed data.
            mz (list of lists): this list contains the lists of
                mz obtained for the corresponding ROI_scans elements.
            intensity (list of lists): Is a sequence containing the
                list of intensity for the corresponding ROI_scans elements.
            SN (list of lists): this list contains the lists of S/N
                ratio values recovered for each ROI_scans element as
                found in the ROI.
            roi_ids (list of int): An integer identifier of the ROI.
            name (string): name of the ROI list.
            dt (float): Time step between scans
            t0 (float): Time of the first scan
            sort_roi (bool): If True, sort ROI according the the average
                mz.
        """
        if sort_roi:
            raise NotImplementedError("This is not (yet) implemented.")

        self.name = name
        self._scans = scans
        self._intensity = intensity
        self._mz = mz
        self.dt = dt
        self.t0 = t0
        if SN is None:
            self._SN = None
            self._SN_avail = False
        else:
            self._SN = SN

        # check lengths of scans, mz, intensity
        lengths = [len(item) for item in [scans, intensity, mz]]
        if self._SN_avail:
            lengths.append(len(self._SN))
        if len(set(lengths)) > 1:
            print("Inconsistency of the data:")
            for label, length in zip(lengths, ["scans", "Intens", "mz", "SN"]):
                print(f"{label}: {length}")
            raise ValueError("The amount of data is not consistent.")

        # set up roi ids
        if roi_ids is None:
            self.roi_ids = list(range(len(scans)))
        else:
            self.roi_ids = roi_ids

        # min and max scan index over all scans
        self._first_scan = min([min(scan) for scan in self._scans])  
        self._last_scan = max([max(scan) for scan in self._scans])  

        # set up the ROIs
        if self._SN_avail:
            self._roi_list = [
                ROI(scans[i], mz[i], intensity[i], roi_id=i, SN=SN[i],
                    dt=dt, t0=t0)
                for i in range(len(scans))]
        else:
            self._roi_list = [
                ROI(scans[i], mz[i], intensity[i], roi_id=i, dt=dt, t0=t0)
                for i in range(len(scans))]

        # grouping data to speed up plotting
        self._ROI_xdata, self._ROI_ydata, self._ROI_cdata = self._concatenate_data()
        self.min_I = np.nanmin(self._ROI_cdata)
        self.max_I = np.nanmax(self._ROI_cdata)
        self._x, self._tic = self.get_total_ion_chromatogram()
        # self._values = self._get_values()

        # plotting method
        self.plot = ROIListPlotter(self)

    # @property
    # def roi_values(self):
    #     """ Return a matrix containing the main results of the ROIs."""
    #     return self._values

    @property
    def start(self):
        """ Returns the start value of each scan. """
        return np.array([roi.start for roi in self._roi_list])

    @property
    def end(self):
        """ Returns the last value of each scan. """
        return np.array([roi.end for roi in self._roi_list])

    @property
    def mz_means(self):
        """ Return the array of ponderate mz. """
        return np.array([roi._mz_mean for roi in self._roi_list])

    @property
    def mz_std(self):
        """ Return the array of ponderate mz. """
        return np.array([roi._mz_std for roi in self._roi_list])

    @property
    def total_intensity(self):
        """
        Return the array of the sum of the intensity of each ROI.
        """
        return np.array([roi._total_intensity for roi in self._roi_list])

    @property
    def roi_lengths(self):
        """ Return the number of peaks per ROI. """
        return np.array([len(roi) for roi in self._roi_list])

    @property
    def SN_avail(self):
        """ True if S/N ratio is available, False if not """
        return self._SN_avail

    @classmethod
    def from_list_of_ROI(cls, roi_list: Sequence[ROI], name: str = "",
                         dt: float = 1., t0: float = 0.):
        """ Return an insntance of type ROIList from a list of ROI

        Args:
            roi_list (Sequence of ROI): List of ROI

        """
        if all([roi.SN_avail for roi in roi_list]):
            return cls(
                scans=[roi.scans for roi in roi_list],
                mz=[roi.mz for roi in roi_list],
                intensity=[roi.intensity for roi in roi_list],
                SN=[roi.SN for roi in roi_list],
                roi_ids=[roi.roi_id for roi in roi_list],
                name=name, dt=dt, t0=t0,
            )

        else:
            return cls(
                scans=[roi.scans for roi in roi_list],
                mz=[roi.mz for roi in roi_list],
                intensity=[roi.intensity for roi in roi_list],
                roi_ids=[roi.roi_id for roi in roi_list],
                name=name, dt=dt, t0=t0,
            )

    @classmethod
    def from_file(cls, filepath: Union[str, Path], na_rep: float = 0):
        """
        Read a CSV file from a ROI list. The file must contain the EIC
        and the ROIs information. This method assumes that the missing
        data is represented as 0 else, na_rep should be specifyied.

        Args:
            filepath (str, Path): directory of the file.
            na_rep (float): missing data representation in the input file.
                If missing data is represented with 0, it is not necessary
                to declare this argument. However, if any other value is
                used this is an important input to correctly read data. If
                data is represented as nan, then put None, np.nan or 'nan'.
        """
        if na_rep is None:
            na_rep = np.nan
        else:
            try:
                na_rep = float(na_rep)
            except TypeError:
                raise TypeError(
                    f'The type {type(na_rep)} of na_rep is not supported.')

        row_list = []
        with open(filepath, 'r') as file:
            reader = csv.reader(file)
            for i, row in enumerate(reader):
                if i == 0:
                    continue
                else:
                    row_list.append([
                        float(row[0]),
                        float(row[1]),
                        float(row[2]),
                        float(row[3]),
                        [float(item) for item in row[4:]]
                    ])

        # re-build the ROI inputs
        scans, intensity, mz = ([], [], [])
        for row in row_list:
            start = int(row[2])
            end = int(row[3])
            idx = end - start + 1
            eic = np.array(row[4][:idx])
            masses = np.array(row[4][idx:])
            if not np.isnan(na_rep):  # missing data is represented as defined in na_rep
                scans.append(
                    [start + i for i, val in enumerate(eic) if val != na_rep]
                )
                intensity.append(
                    [intens for intens in eic if intens != na_rep]
                )
                mz.append(
                    [mass for mass in masses if mass != na_rep]
                )
            else:  # missing data is represented as np.nan
                scans.append(
                    [start + i for i,
                        val in enumerate(eic) if not np.isnan(val)]
                )
                intensity.append(
                    [intens for intens in eic if not np.isnan(intens)]
                )
                mz.append(
                    [mass for mass in masses if not np.isnan(mass)]
                )
        return cls(
            scans=scans,
            mz=mz,
            intensity=intensity,
            name=str(filepath)
        )
    
    def get_ave_peaklist(self) -> PeakList:
        """ Return a peak list whose mz values are the weighted mz values
        along each ROI and the intensity are the sum of intensity over the
        ROI.

        In this peak list, the peak ids are the index of the ROI in the
        ROIList object. This is mandatory to get back ROIs after attribution.

        Peak properties with roi ids are provided.
        """
        return PeakList(
            mz=self.mz_means,
            intensity=self.total_intensity,
            SN=None,
            pid=list(range(len(self._roi_list))),  # important
            peak_properties=dict(roi_ids=self.roi_ids),
            name=self.name
        )

    def attribute(
        self,
        formula_grid: Union[list, FormulaGrid],
        algo: Union[AttributionAlgorithm, str] = AttributionAlgorithm.LOWEST_ERROR,
        verbose: bool = False,
        **kwargs,
    ):
        """ Attribution the averaged peak list and return the list of 
        attributed ROI.

        Args:
            formula_grid (list or FormulaGrid): List of formula grid to be
                used for the attribution.
            algo (AttributionAlgorithm): Select the attribution algorithm
            kwargs keywords parameters of the selected algorithm.

        Returns:
            An AttributedROIList object containing a list of AttributedROI.

        """
        if isinstance(algo, str):
            algo = AttributionAlgorithm(algo)

        if isinstance(formula_grid, FormulaGrid):
            formula_grid = [formula_grid]

        # get the ave peaklist
        pl = self.get_ave_peaklist()
    
        # first attribution
        att_pl = algo(peaklist=pl, formula_grid=formula_grid[0], **kwargs)
        exact_masses = formula_grid[0].exact_masses.copy()
        if verbose:
            print(formula_grid[0])
            att_pl.summary()
        # iterative attribution
        for fg in formula_grid[1:]:
            if verbose:
                print(fg)
            att_pl = algo(peaklist=att_pl, formula_grid=fg, **kwargs)
            exact_masses.update(fg.exact_masses)
            if verbose:
                att_pl.summary()

        # use pid to get the index of ROI in roi list.
        roi_list = list()
        for roi_id in att_pl.pid:
            if att_pl[roi_id].is_attributed:
                roi = AttributedROI(
                    scans=self._roi_list[roi_id].scans,
                    mz=self._roi_list[roi_id].mz,
                    intensity=self._roi_list[roi_id].intensity,
                    SN=self._roi_list[roi_id].SN,
                    formula=att_pl[roi_id].formula,
                    polarity=att_pl[roi_id].polarity,
                    roi_id=roi_id,
                    dt=self.dt, t0=self.t0,
                    exact_masses=exact_masses,
                )
            else:
                roi = self._roi_list[roi_id]

            roi_list.append(roi)

        return AttributedROIList.from_list_of_ROI(
            roi_list, polarity=formula_grid[0].polarity,
            dt=self.dt, t0=self.t0,
        )

    def filter(
        self,
        mz_bounds: tuple[float, float] = None,
        intensity_bounds: tuple[float, float] = None,
        max_hole: int = None,
        min_length: int = None,
    ):
        """ Filter ROI and return a new ROIList according to the criteria.
        The available criteria are the following:

        * ``mz_bounds``: the average m/z value of the ROI is in the range
        * ``intensity_bounds``: the total intensity of the ROI is in the range
        * ``max_hole``: If the ROI does not exist in more than this number
          consecutive scans, it is discarded. If 0 non continuous ROI are
          discarded.
        * ``min_length``: If the number of scans over which the ROI spans is
           lower than the min length, it is discarded.

        Args:
            mz_bounds (tuple): min and max mz values
            intensity_bounds (tuple): min and max intensity
            max_hole (int): maximum number of consecutive missing scans.
            min_length (int): minimum length of the ROI.

        Returns:
            A new ROILIST
        """
        
        roi_list = list()
        for roi in self._roi_list:
            keep = list()

            # mz bounds filtering
            if mz_bounds:
                if (mz_bounds[0] < roi.mz_mean < mz_bounds[1]):
                    keep.append(True)
                else:
                    keep.append(False)
            
            # intensity filtering
            if intensity_bounds:
                if (intensity_bounds[0] < roi.total_intensity < intensity_bounds[1]):
                    keep.append(True)
                else:
                    keep.append(False)

            # continuity
            if max_hole:
                if roi.largest_hole > max_hole:
                    keep.append(False)
                else:
                    keep.append(True)
            
            # minimum length
            if min_length:
                if len(roi) < min_length:
                    keep.append(False)
                else:
                    keep.append(True)

            if all(keep):
                roi_list.append(roi)

        name = "filtered_" + self.name.removeprefix("filtered_")
        return self.__class__.from_list_of_ROI(
            roi_list, name=name, dt=self.dt, t0=self.t0)

    def find_roi(self, mz: float, lambda_parameter: float = 1,
                 verbose: bool = True) -> list:
        """ Returns the index of the regions of interest whose m/z mean
        lie in a range of width ``lambda_parameter`` in ppm.

        Args:
            mz_bounds (tuple): min and max mz values
            lambda_parameter (float): error in ppm to look for roi
            verbose (bool): If True, print the results

        Returns:
            List of indexes

        """
        idx, = np.nonzero(np.abs(self.mz_means - mz) / mz * 1e6 < lambda_parameter)
        if verbose:
            for i in idx:
                print(f"{i:5d} {self._roi_list[i].mz_mean:.6f}"
                      f" +/- {self._roi_list[i]._mz_std:.2e}"
                      f" ({len(self._roi_list[i])} points)")
        return idx

    def _concatenate_data(self):
        """Group the ROI data into x, y and c parameters to perform the
        scatter plots rapidly.
        """
        xdata = np.concatenate([roi.scans for roi in self._roi_list])
        ydata = np.concatenate([roi.mz for roi in self._roi_list])
        cdata = np.concatenate([roi.intensity for roi in self._roi_list])

        # sort according to mass
        # GSV: I think sorting is useless
        # argsort = np.argsort(group_ydata)

        return xdata, ydata, cdata

    def get_total_ion_chromatogram(
        self,
        normalize: str = "max",
        smoothing: bool = False,
        smooth_kws: dict = None,
    ) -> ArrayLike:
        """ This function compute the total ion chromatogram
        by summing the intensities on each scan. 
        
        Args:
            normalize (str): if ``'max'``, sets the maximum intensity of
                the TIC to 100. If ``'integral'``, divides the TIC by
                its integral.
            smoothing (bool): if True a smoothing TIC is returned
            smooth_kws (dict): smoothing options.

        Returns:
            scans (ArrayLike): Numpy array of scans indexes
            TIC (ArrayLike): The TIC.

        """

        scans = list()
        tic = list()
        for idx_scan in range(self._first_scan, self._last_scan + 1):

            idx, = np.nonzero(self._ROI_xdata == idx_scan)
            scans.append(idx_scan)
            tic.append(np.sum(self._ROI_cdata[idx]))
    
        tic = np.array(tic, dtype=np.float64)
        scans = np.array(scans)

        if smoothing:
            s_params = dict(N=6, Wn=0.2)
            s_params.update(**smooth_kws)
            tic = smoothing_method(signal=tic, **s_params)

        if normalize == "max":
            max_tic = np.nanmax(tic)
            tic = tic / max_tic * 100
        elif normalize == "integral":
            norm = np.trapz(tic)
            tic /= norm

        return scans, tic

    def get_values(self):
        """
        Obtain a matrix with the main data.
        """
        values = np.concatenate([
            self.mz_means[np.newaxis],
            self.mz_std[np.newaxis],
            self.total_intensity[np.newaxis],
            self.start[np.newaxis],
            self.end[np.newaxis],
        ]).transpose()
        return values

    def data_summary(self, dt: float = None, t0: float = None,
                     normalize: str = "max") -> pd.DataFrame:
        """ Produce a data frame with general information on each scan
        as a function of time. 
        
        Args:
            dt (float): time step between MS scan.
            t0 (float): time of the first scan.
            normalize (str): if ``'max'``, sets the maximum intensity of the TIC
                to 100. If ``'integral'``, divides the TIC by its integral.

        Returns:
            A data frame with general information about scans.
        """
        data = self.get_values()

        summary = {
            "roi_ids": self.roi_ids,
            "start": data[:, 3].astype(np.int32),
            "end": data[:, 4].astype(np.int32),
            'mz_mean': data[:, 0],
            "mz_std": data[:, 1],
            "total_intensity": data[:, 2],
        }
        return pd.DataFrame(summary).set_index("roi_ids", drop=True)

    def remove_roi_in_range(
        self,
        threshold: float = 0.8,
        scan_bounds: tuple = (0, -1),
        keep_inferior: bool = True,
    ):
        """
        This function deletes ROI for which the ratio between their length 
        and the number of scans is higher than a give threshold.

        For example, with a threshold of 0.8 if the ROI exists over 80% of
        the total number of scans or more, it is deleted (or kept depending
        on ``keep_inferior``). The scans over which the threshold is computed
        is defined from ``scan_bounds``.
        
        Args:
            percentage (float) : this value is used to delete features with
                length higher than percentage times the range length of the
                time-dependant axis (scans).
                This argument must be a float between 0 an 99.
            scan_bounds (int, int): Indices of scans which define the interval
                over which the criteria is applied.
            keep_inferior (bool) : if True, it keeps features below the
                percentage length. If False, it keeps features above the
                percentage length.

        Returns:
            A ROIList object filtered by percentage.
        """

        if scan_bounds == (0, -1):
            scan_bounds = (self._first_scan, self._last_scan)
    
        roi_list = list()
        for roi in self._roi_list:
            overlap = roi.get_interval_overlap(scan_bounds[0], scan_bounds[1])

            if overlap < threshold and keep_inferior:
                # keep roi with overlap lower than threshold
                roi_list.append(roi)
            elif overlap > threshold and (not keep_inferior):
                # keep roi with overlap higher than threshold
                roi_list.append(roi)

        name = "filtered_" + self.name.removeprefix("filtered_")
        return ROIList.from_list_of_ROI(
            roi_list, dt=self.dt, t0=self.t0, name=name)

    def to_dataframe(self, mz_bounds: tuple = None):
        """
        Return the list of ROI in a pandas Data Frame format. If mz
        boundaries are provided, limit the mz to the corresponding range.

        Args:
            mz_bounds (tuple): min and max mz values as a tuple.

        Returns:
            A data frame with : mz, intensity, S/N ratio, start, end and EIC.
        """

        if mz_bounds is None:
            return pd.DataFrame([r.to_dict() for r in self._roi_list])
        else:
            imin, imax = np.searchsorted(self.values[:, 0], mz_bounds)
            return pd.DataFrame([r.to_dict() for r in self._roi_list])[imin: imax]

    def to_csv(self, filename: str = 'results.csv', include_mz: bool = True):
        """
        Export the ROI list as CSV file.

        Args:
            filename (str): the filepath or filename including the csv
                extension.
            na_rep (float): missing data representation. Missing data is
                represented as NaN by default. However, 0 or any float could
                be defined in this argument to substitute NaN by the defined
                value.
            include_masses (bool): If True, the array containing the masses
                of the ROI is included just after the Extractive Ion Chromatogram
                values. This option is True by default. Otherwise, to avoid adding
                the values of masses set this option as False.
        """
        if not isinstance(filename, str):
            raise TypeError(f'The filename {filename} is not a valid string.')

        if include_mz:
            columns = ['mz_mean', "mz_std", 'total_intensity', 'start', 'end',
                       'EIC', 'mz_values']
        else:
            columns = ['mz_mean', "mz_std", 'total_intensity', 'start', 'end',
                       'EIC']
        header = ["# Region of interest"] + [None] * len(columns)

        values = np.concatenate([
            self.mz_means[np.newaxis],
            self.mz_std[np.newaxis],
            self.total_intensity[np.newaxis],
            self.start[np.newaxis],
            self.end[np.newaxis],
        ]).transpose()

        with open(filename, "w") as file:
            writer = csv.writer(file)
            writer.writerow(header)
            writer.writerow(columns)
            for i, roi in enumerate(self._roi_list):
                data = roi.to_dict()
                values = [data[k] for k in columns[:5]]
                values.extend(data["EIC"])
                if include_mz:
                    values.extend(data["mz"])
                
                writer.writerow(values)

    def __len__(self):
        return len(self._roi_list)

    def __repr__(self):
        out = [f"ROIList ({len(self._roi_list)} ROIs)"]
        if len(self._roi_list) > MAX_ROIS:
            for roi in self._roi_list[:6]:
                out.append(repr(roi))
            out.append("...")
            for roi in self._roi_list[-5:]:
                out.append(repr(roi))
        else:
            for roi in self._roi_list:
                out.append(repr(roi))
        return "\n".join(out)

    def __iter__(self):
        # this is unnecessary if getitem is fast enough
        return iter(self._roi_list)

    def __getitem__(self, item: Union[int, slice]):
        """ Return the required ROI if item is an integer or a new
        class object if item is a slice object corresponding to the
        provided ROI list slice."""
        if isinstance(item, slice):
            return ROIList(
                scans=self._scans[item],
                intensity=self._intensity[item],
                mz=self._mz[item],
                name=self.name)
        else:
            return self._roi_list[item]


class AttributedROIList(ROIList):
    """
    This class represents a list of regions of interest (ROI) which were
    attributed. A formula is attached to each ROI. The class contains
    methods to manage these data and filter ROI according to the formulas.
    """

    def __init__(
        self,
        scans: Sequence[list],
        mz: Sequence[list],
        intensity: Sequence[list],
        polarity: int,
        formulas: Sequence[Union[str, dict, Composition, Formula]],
        SN: Sequence[float] = None,
        roi_ids: Sequence[int] = None,
        name: str = '',
        dt: float = 1.0,
        t0: float = 0.0,
        exact_masses: dict = None,
        sort_roi: bool = False,
    ):
        """
        Initialize the Attributed ROI list object.

        Args:
            scans (list of lists): Is a sequence containing the list
                of grouped scans for the analyzed data.
            mz (list of lists): this list contains the lists of
                mz obtained for the corresponding ROI_scans elements.
            intensity (list of lists): Is a sequence containing the
                list of intensities for the corresponding ROI_scans elements.
            polarity (int): the ionization charge.
            formulas (list-like): a list or array which the Attributed Ion
                Molecular Formulas of the corresponding ROIs.
            SN (list of lists): this list contains the lists of S/N
                ratio values recovered for each ROI_scans element as
                found in the ROI.
            roi_ids (list of int): An integer identifier of the ROI.
            name (string): name of the peak list.
            dt (float): Time step between scans
            t0 (float): Time of the first scan in the original time dependent
                data used to build the ROI.
            exact_masses (dict): dict of species and exact masses.
            sort_roi (bool): If True, sort ROI according the the average
                mz. Default is False.
        """
        super().__init__(scans=scans, mz=mz, intensity=intensity, SN=SN,
                         roi_ids=roi_ids, name=name, dt=dt, t0=t0,
                         sort_roi=False)

        self.polarity = polarity

        species = get_species(formulas)
        if exact_masses is None or not set(species).issubset(exact_masses):
            self.exact_masses = all_exact_masses(species)
        else:
            self.exact_masses = exact_masses
        self.formulas = [get_formula(f, exact_masses=self.exact_masses)
                         for f in formulas]
    
        # set up the ROIs
        self._roi_list = list()
        for i in range(len(scans)):
            SN = SN[i] if self._SN_avail else None
            if self.formulas[i] is not None:
                self._roi_list.append(AttributedROI(
                    scans=scans[i], mz=mz[i], intensity=intensity[i], SN=SN,
                    formula=self.formulas[i], polarity=polarity,
                    roi_id=roi_ids[i], dt=dt, t0=t0,
                    exact_masses=exact_masses)
                )
            else:
                self._roi_list.append(ROI(
                    scans=scans[i], mz=mz[i], intensity=intensity[i], SN=SN,
                    roi_id=roi_ids[i], dt=dt, t0=t0)
                )

        self._td_data = None

    @property
    def str_formulas(self):
        """ Return a numpy array of the formulas as strings """
        return np.array([str(f) for f in self.formulas], dtype=np.str_)
    
    @classmethod
    def from_list_of_ROI(cls, roi_list: Sequence[ROI], polarity: int,
                         name: str = "",
                         dt: float = 1., t0: float = 0.,):
        """ Return an instance of type AttributedROIList from a list of ROI

        Args:
            roi_list (Sequence of ROI): List of ROI
            polarity (int): ionization charge

        """
        if all([roi.SN_avail for roi in roi_list]):
            return cls(
                scans=[roi.scans for roi in roi_list],
                mz=[roi.mz for roi in roi_list],
                intensity=[roi.intensity for roi in roi_list],
                SN=[roi.SN for roi in roi_list],
                roi_ids=[roi.roi_id for roi in roi_list],
                formulas=[roi.formula if roi.is_attributed else None
                          for roi in roi_list],
                polarity=polarity,
                name=name, dt=dt, t0=t0,
            )

        else:
            return cls(
                scans=[roi.scans for roi in roi_list],
                mz=[roi.mz for roi in roi_list],
                intensity=[roi.intensity for roi in roi_list],
                polarity=polarity,
                roi_ids=[roi.roi_id for roi in roi_list],
                formulas=[roi.formula if roi.is_attributed else None
                          for roi in roi_list],
                name=name, dt=dt, t0=t0,
            )

    @classmethod
    def from_dataframe(cls, df: pd.DataFrame, polarity: int):
        """
        Initialize the class AttributedPeakList from a DataFrame. The
        index of the dataframe will be used as peak id.

        Args:
            df (pd.DataFrame): the pandas dataframe of an AttributedROI.
                The corresponding dataframe must contain the following
                information on columns named; mz, inensities, start, end,
                extractive_ion_c, formulas, and SN (optional).
            polarity (int): the ionization charge.
        """
        raise NotImplementedError("Need to implement from_dataframe")

    @classmethod
    def from_file(cls, filepath: Union[str, Path], polarity: int):
        """
        Read an attributed ROI file. This file must contain, at
        least the columns of mz, intensity, formulas, start, end and
        the extractive ion chromatogram (EIC). The S/N ratio, and the
        Peak_ID they are optional. This reader works only for a PyC2MC
        output.

        Args:
            filepath (str or Path): the directory of the file.
            polarity (int): the ionization charge
            name (str, optional): filename.
            kwargs of pandas.read_csv()
        """
        raise NotImplementedError("Need to implement from_file")

        # TODO: take it from ROIList

    def get_attributed_td_data(self):
        """ Return an AttributedTimeDependantData from Attributed ROI. """

        attributed_peaklists = list()
        scan_ids = list()
        for idx_scan in range(self._first_scan, self._last_scan + 1):

            peak_list = list()
            for roi in self._roi_list:
                if idx_scan in roi.scans:
                    peak_list.append(roi.get_peak(idx_scan))

            # no peak for this scan
            if len(peak_list) == 0:
                continue

            attributed_peaklists.append(
                AttributedPeakList.from_list(
                    peak_list, exact_masses=self.exact_masses,
                    polarity=self.polarity,
                ))
            scan_ids.append(idx_scan)
        
        return AttributedTimeDependentData(
            attributed_peaklists, scan_ids=scan_ids,
            name=f"from ROI {self.name}", dt=self.dt, t0=self.t0,
        )

    def get_ave_attributed_peaklist(self):
        """ Returns the corresponding Attributed Peak List. """
        return AttributedPeakList(
            mz=self.mz_means,
            intensity=self.total_intensity,
            formulas=self.formulas,
            polarity=self.polarity,
            pid=self.roi_ids,
            name=self.name,
        )

    def get_attributed_rois(self):
        """ Return a new Attributed List object with only attributed
        ROI. """
        return AttributedROIList.from_list_of_ROI(
            [roi for roi in self._roi_list if roi._is_attributed],
            polarity=self.polarity, dt=self.dt, t0=self.t0,
        )

    def data_summary(self, dt: float = None, t0: float = None,
                     normalize: str = "max") -> pd.DataFrame:
        """ Produce a data frame with general information on each scan
        as a function of time. 
        
        Args:
            dt (float): time step between MS scan.
            t0 (float): time of the first scan.
            normalize (str): if ``'max'``, sets the maximum intensity of the TIC
                to 100. If ``'integral'``, divides the TIC by its integral.

        Returns:
            A data frame with general information about scans.
        """
        df = super().data_summary(dt=dt, t0=t0, normalize=normalize)
        df["formula"] = self.str_formulas
        return df

    def to_dataframe(self, mz_bounds: tuple = None):
        """
        Return the peak list in a pandas Data Frame format. If mz
        boundaries are provided, limit the mz to the corresponding range.

        Args:
            mz_bounds (tuple): min and max mz values as a tuple.

        Returns:
            A data frame with : mz, intensity, S/N ratio, formula, start,
            end and EIC.
        """
        df = super().to_dataframe(mz_bounds)
        df.insert(3, "formula", self.str_formulas)
        return df

    def to_csv(self, filename: str = 'results.csv', include_mz: bool = True):
        """
        Export the ROI list as CSV file.

        Args:
            filename (str): the filepath or filename including the csv
                extension.
            na_rep (float): missing data representation. Missing data is
                represented as NaN by default. However, 0 or any float could
                be defined in this argument to substitute NaN by the defined
                value.
            include_masses (bool): If True, the array containing the masses
                of the ROI is included just after the Extractive Ion Chromatogram
                values. This option is True by default. Otherwise, to avoid adding
                the values of masses set this option as False.
        """
        if not isinstance(filename, str):
            raise TypeError(f'The filename {filename} is not a valid string.')

        columns = ['mz_mean', "mz_std", 'total_intensity', "formula",
                   "polarity", 'start', 'end', 'EIC']
        header = ["# Region of interest"] + [None] * len(columns)

        with open(filename, "w") as file:
            writer = csv.writer(file)

            # write header
            writer.writerow(header)
            if include_mz:
                writer.writerow(columns + ["mz_values"])
            else:
                writer.writerow(columns)

            # write ROIs
            for i, roi in enumerate(self._roi_list):
                data = roi.to_dict()
                values = [data[k] if k in data else "" for k in columns[:-1]]
                values.extend(data["EIC"])
                if include_mz:
                    values.extend(data["mz"])
                
                writer.writerow(values)

    def __repr__(self):
        out = [f"AttributedROIList ({len(self._roi_list)} ROIs)"]
        if len(self._roi_list) > MAX_ROIS:
            for roi in self._roi_list[:6]:
                out.append(repr(roi))
            out.append("...")
            for roi in self._roi_list[-5:]:
                out.append(repr(roi))
        else:
            for roi in self._roi_list:
                out.append(repr(roi))
        return "\n".join(out)
