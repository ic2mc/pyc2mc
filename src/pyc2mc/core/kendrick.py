# coding: utf-8

""" This module provides classes to represent Kendrick series including
or not formula attribution. """

from typing import Sequence, Union, Tuple
from collections import defaultdict
from collections import abc

import numpy as np
from numpy.typing import ArrayLike
import pandas as pd
import warnings

from pyc2mc.core.formula_grid import FormulaGrid
from pyc2mc.processing.assignment import assign_candidates
from pyc2mc.core.formula import Formula, get_formula, KendrickBuildingBlock
from pyc2mc.core.peak import PeakList, AttributedPeakList, AttributedPeak

CH2 = Formula.from_string("C1 H2")


class KendrickSeries(PeakList):
    """ Class that defines a single Kendrick Series which corresponds to
    a list of peaks linked through a build block formula. """

    def __init__(
        self,
        mz: Sequence[float],
        intensity: Sequence[float],
        SN: Sequence[float] = None,
        pid: Sequence[int] = None,
        peak_properties: dict = None,
        name: str = "",
        sort_mz: bool = False,
        building_block: Union[KendrickBuildingBlock, str, float] = CH2,
    ):
        """
        Initialize the KendrickSeries class.

        Args:
            mz (list-like): List or array of mass over z values.
            intensity (list-like): list or array of intensity.
            SN (list-like): list or array of the signal/noise values.
            pid (list-like): list or peak identifiers.
            peak_properties (dict): a dictionary containing additional
                properties of a peak.
            name (str): kendrick series name
            sort_mz (bool): if true, sort data by m/z values.
            building_block (KendrickBuildingBlock): Kendrick Building Block
                as formula, a float or a KendrickBuildingBlock, defaut CH2.

        """
        super().__init__(mz=mz, intensity=intensity, SN=SN, pid=pid,
                         peak_properties=peak_properties, name=name,
                         sort_mz=sort_mz)
        self.columns = ['mz', 'intensity', 'SN', 'KM']

        self.building_block = KendrickBuildingBlock(building_block)
        self.kendrick_mass = self.get_kendrick_masses(building_block)
        # self._is_attributed = False
        values = self.values
        self._values = np.concatenate(
            [values, self.kendrick_mass[:, np.newaxis]],
            axis=1,
        )
        if len(self._peaks) > 1:
            self._rounded_diffs = np.round(
                np.diff(self.mz) / self.building_block.exact_mass
            ).astype(np.uint16)
            self._is_continuous = np.all(self._rounded_diffs <= 1)
            self._largest_hole = np.max(self._rounded_diffs) - 1
        else:
            self._rounded_diffs = np.array([], dtype=np.uint16)
            self._is_continuous = False
            self._largest_hole = -1

    @property
    def is_continuous(self) -> bool:
        """ True if the series does not present any hole. A hole
        corresponds to a missing peak in the series.  """
        return self._is_continuous

    @property
    def largest_hole(self) -> int:
        """ the largest number of missing peaks in the series """
        return self._largest_hole

    @property
    def is_shape_valid(self) -> bool:
        """ EXPERIMENTAL!!
        Detect how much maxima exists in the kendrick series. The shape
        is considered as "valid" if there is only one uniq maxima.

        This could also be done by looking for peaks but there is maybe
        something to learn from the shape of the derivative of the kendrick
        series.
        """
        intens_diff = np.diff(self.intensity)
        diff_sign = np.sign(intens_diff).astype(np.int8)
        return np.count_nonzero(np.diff(diff_sign)) == 1

    @classmethod
    def from_dataframe(
        cls,
        df: pd.DataFrame,
        building_block: Union[KendrickBuildingBlock, str, float] = CH2
    ):
        """ Return a Kendrick Series object from a data frame. Only
        the columns associated to the peak list are used.

        Args:
            df (pd.DataFrame):
            building_block (KendrickBuildingBlock): Kendrick Building Block
                as formula, a float or a KendrickBuildingBlock, default CH2.
        """
        # TODO: this one is new CHECK IT !!
        pl = super().from_dataframe(df)
        return cls(pl.mz, pl.intensity, pl.SN, pl.pid,
                   pl.peak_properties, building_block=building_block)

    @classmethod
    def from_list(
        cls,
        peaks: list,
        building_block: Union[KendrickBuildingBlock, str, float] = CH2,
        **kwargs
    ):
        """ Initialize the class KendrickSeries from a list of Peak object.

        Args:
            peaks (list): list of Peak object
            kwargs (dict): keywords argument passed to PeakList init.
        """
        props = defaultdict(list)
        pids = list()
        values = list()
        SN_avail = list()
        for peak in peaks:
            pids.append(peak.pid)
            SN_avail.append(peak.SN_avail)
            values.append([peak.mz, peak.intensity, peak.SN])
            for k, v in peak.properties.items():
                props[k].append(v)

        values = np.array(values, dtype=np.float64)
        building_block = KendrickBuildingBlock(building_block)
        if all(SN_avail):
            SN = values[:, 2]
        else:
            SN = None

        return cls(
            mz=values[:, 0],
            intensity=values[:, 1],
            SN=SN,
            pid=pids,
            peak_properties=props,
            building_block=building_block,
            **kwargs
        )

    def get_candidates(self, formula_grid: FormulaGrid,
                       lambda_parameter: float = 10):
        """
        Get candidates of the lowest mz from a Kendrick Series or PeakList
        based on the formula grid and the lambda_parameter (max error).

        Args:
            formula_grid (pyc2mc.core.formula_grid.FormulaGrid): a formula grid to
                perform the attribution.
            lambda_parameter (float): the max error in ppm to search the candidates
                for the peak.

        Returns:
            A data frame with the candidates for the first peak of the series.
        """
        # assign candidates to first peak of the series
        candidates = assign_candidates(
            peak_list=self._peaks[0],
            formula_grid=formula_grid,
            lambda_parameter=lambda_parameter,
            returns_candidates_data=True
        )
        if not candidates.iloc[0].candidates_exist:
            raise ValueError(
                "The assignment of candidates fails. "
                "Try to change the lambda parameter or the formula grid.")

        return candidates.iloc[0].cand_list.sort_values(by="err_ppm", key=np.abs)

    def attribute_series(
        self,
        formula_grid: FormulaGrid,
        formula: Union[str, Formula] = None,
        target_error: float = 5,
        lambda_parameter: float = None,
        verbose: bool = True,
    ):
        """
        Attribute to each peak of the series a molecular formula using the
        building block and the attribution of the first peak of the series.
        If the molecular formula is not provided, candidates are identified
        using the ``lambda_parameter`` and the one with the error closest to
        ``target_error`` is selected.

        Args:
            formula_grid (pyc2mc.core.formula_grid.FormulaGrid): a formula grid to
                perform the attribution.
            formula (str, Formula): a molecular formula assigned to the first
                peak of the series (lowest m/z).
            target_error (float): Target error in ppm. If the formula is
                not provided, the formula with the error in ppm closest to that
                value is selected. This value could be negative. Default 5 ppm.
            lambda_parameter (float): the error in ppm to search the candidates
                for the peak in the formula grid. If not provided, two times
                the absolute value of ``target_error`` is used.
            verbose (bool): method verbosity, default True.

        Returns:
            A data frame of the attributed kendrick series based on the given formula.
        """
        # The formula grid is used to obtain some setup information such as
        # the charge and the exact_masses
        try:
            exact_masses = formula_grid.exact_masses # exact masses from FG
            charge = formula_grid.polarity # get charge from FG
        except TypeError as err:
            raise TypeError('formula_grid must be a FormulaGrid object.') from err

        # setup the building block
        if not self.building_block.formula_avail:
            raise ValueError('Building_block must be given as a formula object, '
                             'to attribute all the peaks of the series.')

        # if a formula was not given
        if formula is None:
            # define lambda parameter to be larger than target_error
            if lambda_parameter is None:
                lambda_parameter = min(2 * abs(target_error), 1)
            elif lambda_parameter < abs(target_error):
                lambda_parameter = min(2 * abs(target_error), 1)
                warnings.warn(
                    ("lambda_parameter is lower than |target_error|. "
                     f"lambda_parameter is set to {lambda_parameter} ppm"),
                    UserWarning, stacklevel=2)

            # assign only the first peak of the series and select formula
            # with lowest error
            candidates = self.get_candidates(formula_grid, lambda_parameter)

            if len(candidates) > 1 and verbose:
                # use lowest error
                print(f"Several candidates are available, n={len(candidates)}")
                print("No formula was provided. The candidate is selected"
                      " according to the error the\nclosest to the target error.")
                print(f"first peak m/z: {self._peaks[0].mz:.4f}")
                print(candidates[["ion_exact_mass", "err_ppm", "formula"]])
                print(f"target error: {target_error:.4f} ppm")
                # warnings.warn(
                #     ('No formula was provided in set_formula_to_series. '
                #      'A candidate will be selected by closest error.'),
                #     UserWarning, stacklevel=2)

            err_ppm = candidates.err_ppm.values
            idx = np.argmin(np.abs(err_ppm - target_error))
            formula = candidates.formula.values[idx]
            target_formula = get_formula(formula, exact_masses=exact_masses)

            if verbose:
                print(f"selected candidate for lowest m/z: {target_formula}")
        else:
            target_formula = get_formula(formula, exact_masses=exact_masses)

        peak = self._peaks[0]
        mz_0 = peak.mz
        attributed_peaks = [AttributedPeak(**peak.as_dict(),
                                           formula=target_formula,
                                           attribution_method="kendrick_series",
                                           polarity=charge)]

        bb_exact_mass = self.building_block.exact_mass
        bb_composition = {el: n for el, n in self.building_block.formula.items()}
        for peak in self._peaks[1:]:
            multiplier = np.round((peak.mz - mz_0) / bb_exact_mass)

            shift = {el: int(n * multiplier) for el, n in bb_composition.items()}
            formula = target_formula.substitute_elements(element_block=shift)
            attributed_peaks.append(AttributedPeak(
                **peak.as_dict(), formula=formula, polarity=charge,
                attribution_method="kendrick_series"))

        return AttributedKendrickSeries.from_list(
            attributed_peaks,
            polarity=charge,
            building_block=self.building_block,
            exact_masses=formula_grid.exact_masses,
            name=f"{self.name} attributed"
        )

    def get_highest_peak_info(self) -> Tuple[ArrayLike, int]:
        """
        Returns the mz, intensity, S/N ratio and Kendrick Mass of the
        peak with the highest intensity or S/N ratio signal.
        """

        if self.SN_avail:
            idx = np.argmax(self.SN)
            return self._values[idx, :], idx
        else:
            idx = np.argmax(self.intensity)
            return self._values[idx, :], idx

    def drop_peak(self, index):
        """
        Drop a peak from the Kendrick Series.

        Args:
            index (int): the index of the peak in the series.

        Return:
            A Kendrick Series without the dropped peak.
        """
        peaks = [p for i, p in enumerate(self._peaks) if i != index]
        return KendrickSeries.from_list(peaks, self.building_block)

    def get_lowest_peak_info(self):
        """
        Returns the mz, intensity, S/N ratio and Kendrick Mass of the
        peak with the lowest intensity or S/N ratio signal.
        """
        if self.SN_avail:
            idx = np.armin(self.SN)
            return self._values[idx, :], idx
        else:
            idx = np.argmin(self.intensity)
            return self._values[idx, :], idx

    def to_dataframe(self, **kwargs) -> pd.DataFrame:
        """
        Return the kendrick series in a pandas Data Frame format.

        Returns:
            A data frame with four columns : mz, intensity, S/N ratio and
            KendrickMass.
        """

        return pd.DataFrame(
            self._values, columns=self.columns, index=self.pid)

    def split_series(self, split: bool = False, min_length: int = 4,
                     n_max_hole: int = 1) -> list:
        """
        Splits a discontinuous Kendrick Series into smaller continuous
        Kendrick Series or returns the largest. The series is assume to be
        not continuous if at least ``n_max_hole`` peaks are missing.

        ``n_max_hole`` describes the number of missing **peaks** and not
        the number of intervals. If one peak is missing, the distance between
        the peaks is two times the building block mass.

        Peaks removed from the series are returned as a ``PeakList``.

        Args:
            split (bool): if true, split the kendrick series into smaller series
                and returns all the series. If false, return the largest
                continuous series
            min_length (int): the minimum number of peaks in a series to
                be considered valid
            n_max_hole (int): the maximum number of missing peaks in a series.
                If the number of missing peaks is greater or equal to n_max_hole
                the series is splitted. Default is 1, the series are splitted
                if one peak is missing.

        Returns:
            * A list of single or multiple kendrick series objects derived
              from a discontinuous one. If already continuous,
              returns the original kendrick series.
            * A PeakList of peaks initially in the kendrick series and not
              in the splitted series.

        """
        if self.is_continuous or len(self._peaks) <= 1:
            return [self], PeakList([], [])

        if min_length <= 0:
            raise ValueError(
                "'min_length' must be strictly positive: "
                f"min_length is {min_length}."
            )

        # find holes
        idx, = np.where(self._rounded_diffs > n_max_hole)

        if len(idx) == 0:
            return [self], PeakList([], [])

        ks_list = []
        non_grouped_peaks = list()
        start = 0
        for hole_idx in idx:
            sub_series_lenghts = hole_idx - start + 1
            if sub_series_lenghts >= min_length:
                ks_list.append(KendrickSeries.from_list(
                    self._peaks[start:hole_idx + 1],
                    building_block=self.building_block))
            else:
                non_grouped_peaks.extend(list(self._peaks[start:hole_idx + 1]))

            start = hole_idx + 1

        # last series
        if len(self) - start >= min_length:
            ks_list.append(KendrickSeries.from_list(
                self._peaks[start:], building_block=self.building_block
            ))
        else:
            non_grouped_peaks.extend(list(self._peaks[start:]))

        if split:
            # returns all splitted series if length is > min_length
            non_grouped_pl = PeakList.from_list(non_grouped_peaks)
            return ks_list, non_grouped_pl
        else:
            idx = np.argmax([len(ks) for ks in ks_list])
            for i, ks in enumerate(ks_list):
                if i == idx:
                    continue
                non_grouped_peaks.extend(list(ks._peaks))
            non_grouped_pl = PeakList.from_list(non_grouped_peaks)
            return [ks_list[idx]], non_grouped_pl

    def contains_mz(
        self,
        mz: float,
        lambda_parameter: float = 1
    ) -> bool:
        """
        Check if the Kendrick Series contains a given mz value.

        Args:
            mz (float): the mz value to check.
            lambda_parameter (float): the maximum error in ppm to search the candidates
                for the peak.

        Returns:
            True if the Kendrick Series contains the mz value, False otherwise.
        """
        return np.any(np.abs(self.mz - mz) / mz * 1e6 <= lambda_parameter)

    def __repr__(self):
        out = super().__repr__().split("\n")[1:]
        header = [
            f"KendrickSeries ({len(self._peaks)} peaks)",
            f"Building block {str(self.building_block.formula)}"
        ]
        out = header + out
        return "\n".join(out)

    # def check_gaussianity(self, alpha=0.05):
    #     '''
    #     Check how the intensity profile or SN of the series fit a gaussian or
    #     skewed gaussian distribution.

    #     Returns:
    #         1 if the intensity profile is skewed to the right
    #         2 if the intensity profile is skewed to the left
    #         0 if the intensity profile is gaussian
    #         -1 if the intensity profile is not gaussian
    #     '''
    #     from scipy import stats

    #     if self.SN_avail:
    #         y = self.SN
    #     else:
    #         y = self.intensity

    #     data = []
    #     for i in range(len(self.mz)):
    #         data.extend([self.mz[i]]*int(y[i]))

    #     # check if the data is gaussian
    #     k2, p = stats.normaltest(data)

    #     if p < alpha:  # null hypothesis: data comes from a normal distribution
    #         return -1
    #     else:
    #         # check if the data is skewed
    #         skew = stats.skew(data)
    #         if skew > 0:
    #             return 1
    #         elif skew < 0:
    #             return 2
    #         else:
    #             return 0


class AttributedKendrickSeries(KendrickSeries):
    """
    This class defines an attributed Kendrick Series.
    """

    def __init__(
        self,
        mz: Sequence[float],
        intensity: Sequence[float],
        formulas: Sequence[Union[str, Formula]],
        polarity: int,
        SN: Sequence[float] = None,
        pid: Sequence[int] = None,
        peak_properties: dict = None,
        name: str = "attributed_kendrick_series",
        sort_mz: bool = False,
        building_block: Union[KendrickBuildingBlock, str, float] = CH2,
        exact_masses: dict = None,
    ):
        """
        Initialize the Attributed Kendrick Series class.

        Args:
            mz (list-like): List or array of mass over z values.
            intensity (list-like): list or array of intensity.
            formulas (list-like): list or array containing the molecular
                formula of each peak as string.
            polarity (int): the ionization charge
            SN (list-like): list or array of the signal/noise values.
            pid (list-like): list or peak identifiers.
            peak_properties (dict): A dict of properties of the peaks
            name (str): kendrick series name
            sort_mz (bool): if true, sort data by m/z values.
            building_block (KendrickBuildingBlock): Kendrick Building Block
                as formula, a float or a KendrickBuildingBlock, defaut CH2.
            exact_masses (dict): dictionnary containing the values for
                the exact masses, this is optional, if not, the masses
                will be obtained from the Elements and Isotope classes.
        """
        self.columns = ['mz', 'intensity', 'SN', 'KM', "formula"]
        self.building_block = KendrickBuildingBlock(building_block)

        # from init of PeakList
        self._SN_avail = False if SN is None else True
        self.name = name

        # composiion with an attributed peaklist
        self._attributed_peaklist = AttributedPeakList(
            mz=mz, intensity=intensity, formulas=formulas, polarity=polarity,
            SN=SN, pid=pid, peak_properties=peak_properties,
            exact_masses=exact_masses, sort_mz=sort_mz
        )
        self._is_attributed = True
        self.kendrick_mass = self._attributed_peaklist.get_kendrick_masses(
            building_block)
        # map peaks with the attributed peaklist
        self._peaks = self._attributed_peaklist._peaks
        self.plot = self._attributed_peaklist.plot

        # data
        self._values = np.concatenate(
            [self._attributed_peaklist.values, 
             self.kendrick_mass[:, np.newaxis]],
            axis=1,
        )

        if len(self._peaks) > 1:
            self._rounded_diffs = np.round(
                np.diff(self.mz) / self.building_block.exact_mass
            ).astype(np.uint16)
            self._is_continuous = np.all(self._rounded_diffs <= 1)
            self._largest_hole = np.max(self._rounded_diffs) - 1
        else:
            self._rounded_diffs = np.array([], dtype=np.uint16)
            self._is_continuous = False
            self._largest_hole = -1

    @property
    def attributed_peaklist(self):
        """ The corresponding attributed peaklist """
        return self._attributed_peaklist

    @classmethod
    def from_list(
        cls,
        peaks: list,
        polarity: int,
        building_block: Union[KendrickBuildingBlock, str, float] = CH2,
        **kwargs
    ):
        """ Initialize the AttributedKendrickSeries class from a list of
        Peak object.

        Args:
            peaks (list): list of Peak object
            kwargs (dict): keywords argument passed to class init.
        """
        if "exact_masses" in kwargs:
            exact_masses = kwargs.pop("exact_masses")
        else:
            exact_masses = dict()
        att_pl = AttributedPeakList.from_list(
            peaks, polarity=polarity, exact_masses=exact_masses, sort_mz=False)
        building_block = KendrickBuildingBlock(building_block)

        return cls(
            mz=att_pl.mz,
            intensity=att_pl.intensity,
            SN=att_pl.SN,
            pid=att_pl.pid,
            polarity=polarity,
            formulas=att_pl.formulas,
            peak_properties=att_pl.peak_properties,
            building_block=building_block,
            exact_masses=att_pl.exact_masses,
            **kwargs
        )

    @classmethod
    def from_dataframe(
        cls,
        df: pd.DataFrame,
        polarity: int,
        building_block: Union[KendrickBuildingBlock, str, float] = CH2,
        **kwargs
    ):
        """ Set up a AttributedKendrickSeries object from the input
        data frame. The index of the data frame will be used as peak id.

        Args:
            df (pd.DataFrame): the pandas dataframe of an attributed
                peaklist. The corresponding dataframe must contain the
                following information on columns named; mz, inensities,
                formulas, and SN.
            polarity (int): the ionization charge of the AttributedPeakList.
            building_block (KendrickBuildingBlock): Kendrick Building Block
                as formula, a float or a KendrickBuildingBlock, defaut CH2.
        """
        apl = AttributedPeakList.from_dataframe(df, polarity=polarity)

        return cls(
            mz=apl.mz,
            intensity=apl.intensity,
            formulas=apl.str_formulas,
            polarity=polarity,
            SN=apl.SN,
            pid=apl.pid,
            sort_mz=False,
            building_block=building_block,
            **kwargs)

    def drop_peak(self, index):
        """
        Drop a peak from the Attributed Kendrick Series.

        Args:
            index (int): the index of the peak in the series.

        Return:
            A Kendrick Series without the dropped peak.
        """
        peaks = [p for i, p in enumerate(self._peaks) if i != index]
        return AttributedKendrickSeries.from_list(
            peaks,
            building_block=self.building_block,
            polarity=self._attributed_peaklist.polarity,
            exact_masses=self._attributed_peaklist.exact_masses,
        )

    def to_dataframe(self, full_data: bool = False, **kwargs) -> pd.DataFrame:
        """
        Return the attributed kendrick series in a pandas Data Frame format.

        Args:
            full_data (bool): If True, returns a dataframe with more data.
            kwargs: dict of arguments of AttributedPeakList.to_dataframe

        Returns:
            A data frame with four columns : mz, intensity, S/N ratio and
            KendrickMass.
        """
        df = self._attributed_peaklist.to_dataframe(
            full_data=full_data, building_block=self.building_block, **kwargs)

        if full_data:
            return df
        else:
            return df.loc[:, ["mz", "intensity", "SN", "error_ppm", "formula"]]

    def __repr__(self):
        out = self._attributed_peaklist.__repr__().split("\n")[1:]
        header = [
            f"AttributedKendrickSeries ({len(self._peaks)} peaks)",
            f"Building block {str(self.building_block.formula)}"
        ]
        out = header + out
        return "\n".join(out)


class KendrickSeriesList(abc.Sequence):
    """ This class describe a list of kendrick series. """

    def __init__(
        self,
        kendrick_series_list: list,
        non_grouped_peaklist: PeakList = None,
        building_block: Union[KendrickBuildingBlock, str, float] = CH2
    ):
        """
        Create the Kendricks series list.

        Args:
            kendrick_series_list (list-like): List or array of kendrick
                series objects.
            non_grouped_peaklist(pyc2mc.core.peak.PeakList): the remaining
                peaklist of non-grouped peaks into Kendrick Series.
            building_block (KendrickBuildingBlock): Kendrick Building Block
                as formula, a float or a KendrickBuildingBlock, defaut CH2.
        """

        self._building_block = KendrickBuildingBlock(building_block)

        if len(kendrick_series_list) > 0:
            if all([ks.SN_avail for ks in kendrick_series_list]):
                self._SN_avail = True
            else:
                self._SN_avail = False
            self._non_grouped_peaklist = non_grouped_peaklist
            self._kendrick_series_list = kendrick_series_list
            self._grouped_peaklist = self._get_grouped_peaklist()
        else:  # empty kendrick series object
            self._SN_avail = False
            self._kendrick_series_list = []
            self._grouped_peaklist = []
            self._non_grouped_peaklist = non_grouped_peaklist

    @property
    def number_of_peaks(self):
        """ Return the amount of peaks in the Kendrick Series List. """
        return sum([len(ks) for ks in self._kendrick_series_list])

    @property
    def non_grouped_peaklist(self):
        """ Returns the list of peaks that were not grouped into a
        Kendrick Series.
        """
        return self._non_grouped_peaklist

    @property
    def grouped_peaklist(self):
        """ Returns the list of peaks that were  grouped into Kendrick
        Series.
        """
        return self._grouped_peaklist

    @property
    def building_block(self):
        """ Return the building block used to build the Kendrick Series """
        return self._building_block

    @property
    def lengths(self) -> list:
        """ List of the lengths of the Kendrick series """
        return [len(ks) for ks in self._kendrick_series_list]

    @property
    def is_continuous(self) -> list:
        """ List continuity conditions of the Kendrick Series """
        return [ks.is_continuous for ks in self._kendrick_series_list]

    def display_series(self, start: int = 0, stop: int = None):
        """
        Display the Kendrick Series sorted by intensity or S/N. The
        list of kendrick series can be delimited by the start and stop
        arguments.

        Args:
            start (int): index of the first kendrick series or the last
                one if stopt is None.
            stop (int): index of the last kendrick series
        """
        if start > 0 and stop is None:
            stop = start
            start = 0
        elif stop is None:
            stop = len(self._kendrick_series_list)

        for i, s in enumerate(self._kendrick_series_list[start: stop]):
            print('========================================================')
            print('SERIES: ', i + start)
            print(f'Lenght: {len(s)}  Continuous: {s.is_continuous}')
            print(s)

    def _get_grouped_peaklist(self):
        """
        This method returns the list of peaks grouped in Kendrick Series.
        """
        peaks = [peak for ks in self._kendrick_series_list for peak in ks]

        return PeakList.from_list(peaks, sort_mz=True)

    def get_series_with_pid(self, pid: int, index_only=False):
        """
        Returns the KendrickSeries objects of the corresponding peak ID.

        Args:
            pid (int): the peak ID according with the original peak list
                used to produce the Kendrick Series List.
            index_only (bool): If True return only the index of the series.

        Returns:
            if index_only is False (default) returns a list of tuple
            ``(idx, kendrick_series)`` else returns only the list of
            indexes.

        """
        ks_found = [(i, series) for i, series in enumerate(
                    self._kendrick_series_list) if pid in series.pid]

        if index_only:
            return [idx for idx, _ in ks_found]
        else:
            return ks_found

    def drop(self, idx: int):
        """
        Drops a Series from the List.

        Args:
            i (int): index

        Returns the droped kendrick series.
        """
        ks_list = [ks for i, ks in enumerate(self._kendrick_series_list) if idx != i]
        non_grouped_peaklist = self._non_grouped_peaklist._peaks +\
            [p for p in self._kendrick_series_list[idx]]
        return KendrickSeriesList(
            ks_list,
            non_grouped_peaklist=non_grouped_peaklist,
            building_block=self.building_block
        )

    def __getitem__(self, item: Union[int, slice]):
        """ Return the required PeakList if item is an integer or a new
        KendrickSeriesList if item is a slice object """
        if isinstance(item, slice):
            start = item.start if item.start is not None else 0
            step = item.step if item.step is not None else 1
            if item.stop is None:
                stop = len(self._kendrick_series_list) if step > 0 else -1
            else:
                stop = item.stop
            if start == stop:
                return KendrickSeriesList([])
            else:
                selected_items = list(range(start, stop, step))
                # TODO: is this relevant? to move all peak in other series
                # to non grouped peaks
                idx = [i for i in range(len(self._kendrick_series_list))
                       if i not in selected_items]
                non_grouped_peaklist = list(self._non_grouped_peaklist._peaks) +\
                    [p for i in idx for p in self._kendrick_series_list[i]]
                return KendrickSeriesList(
                    kendrick_series_list=[self._kendrick_series_list[i]
                                          for i in selected_items],
                    non_grouped_peaklist=non_grouped_peaklist,
                    building_block=self._building_block,
                )
        else:
            return self._kendrick_series_list[item]

    def __len__(self):
        return len(self._kendrick_series_list)

    def __repr__(self):
        return repr(self._kendrick_series_list)

    def split_all(self, split: bool = True, min_length: int = 4,
                  n_max_hole: int = 1, sort_series: bool = False):
        """
        Splits any discontinuous Kendrick Series into smaller continuous
        Kendrick Series. Adds to itself the valid ones or keeps the largest
        one if split is false. The series is assume to be
        not continuous if at least ``n_max_hole`` peaks are missing.

        ``n_max_hole`` describes the number of missing **peaks** and not
        the number of interval. If one peak is missing, the distance between
        the peaks is two times the building block mass.

        Peaks removed from the series are returned as a ``PeakList``.

        Args:
            split (bool): if true, split the kendrick series into smaller series
                and return all series. If false, keeps the largest continuous
                series.
            min_length (int): the minimum number of peaks in a series to be
                considered valid
            n_max_hole (int): the maximum number of missing peaks in a series.
                If the number of missing peaks is greater or equal to n_max_hole
                the series is splitted. Default is 1, the series are splitted
                if one peak is missing.
            sort_series (bool): If True, sort series according to S/N if
                available or intensity if not. Default False.

        """
        # peaks non grouped in kendrick series
        if self._non_grouped_peaklist is not None:
            peaks = list(self._non_grouped_peaklist._peaks)
        else:
            peaks = list()

        # loop over series
        ks_list = []
        for ks in self._kendrick_series_list:
            # split_series returns always a list
            splitted_ks, ng_peaks = ks.split_series(
                split=split,
                min_length=min_length,
                n_max_hole=n_max_hole)

            peaks.extend(list(ng_peaks._peaks))
            # if split:
            #     # keep all longer than min_length
            #     idx_keep = list()
            #     for i, a_ks in enumerate(splitted_ks):
            #         if len(a_ks) >= min_length:
            #             # save it
            #             idx_keep.append(i)
            #         else:
            #             # update non grouped
            #             peaks.extend(list(a_ks._peaks))
            #     splitted_ks = [splitted_ks[i] for i in idx_keep]

            # else:
            #     # look for the longest series
            #     idx = np.argmax([len(a_ks) for a_ks in splitted_ks])
            #     # move other kendrick series to non grouped
            #     for i, a_ks in enumerate(splitted_ks):
            #         if i == idx:
            #             continue
            #         peaks.extend(list(a_ks._peaks))

            #     # save the longest if longer than min_length
            #     if len(splitted_ks[idx]) >= min_length:
            #         splitted_ks = [splitted_ks[idx]]
            #     else:
            #         peaks.extend(list(splitted_ks[idx]._peaks))
            #         splitted_ks = []

            if len(splitted_ks) >= 1:
                ks_list.extend(splitted_ks)

        new_ks_list = KendrickSeriesList(
            ks_list, building_block=self.building_block,
            non_grouped_peaklist=PeakList.from_list(peaks))
        if sort_series:
            new_ks_list.sort_series(inplace=True)
        return new_ks_list

    def get_series_in_range(
        self,
        mz_bounds: tuple,
        verbose: bool = True,
    ) -> list:
        """
        Return indices of series where their largest peak is within a specific range

        Args:
            mz_bounds (tuple): the min and max mz values to search for series
            verbose (bool): if true, print the index, min and max mz values and the length of the series

        Returns:
            A list of indices of series that are within the mz bounds

        """

        ks_idx = list()
        for idx, ks in enumerate(self._kendrick_series_list):
            most_intense_peak = ks.get_highest_peak_info()[0]
            if most_intense_peak[0] > mz_bounds[0] and most_intense_peak[0] < mz_bounds[1]:
                if verbose:
                    print(f"{idx:4d} {most_intense_peak[0]:10.4f} {len(ks):4d}")
                ks_idx.append(idx)

        return ks_idx

    def get_series_with_mz(
            self,
            mz: float,
            lambda_parameter: float = 10
    ) -> list:
        '''
        Returns list of indices of series that contain a peak with the given mz value

        Args:
            mz (float): the mz value to search for
            lambda_parameter (float): the max error in ppm to search the candidates
                for the peak.

        Returns:
            A list of indices of series that contain the given mz value
        '''

        ks_idx = list()
        for idx, ks in enumerate(self._kendrick_series_list):
            if ks.contains_mz(mz, lambda_parameter):
                ks_idx.append(idx)

        return ks_idx

    def get_continuous_series(self, n_max_hole: int = 0):
        """ Return a new KendrickSeriesList object with only continuous series.

        Args:
            n_max_hole (int): maximum number of missing peaks. If the largest
                number of missing peaks is greater than 'n_max_hole' the
                series is supposed to be not continuous. Default is 0, meaning
                that the series is not continuous if one peak is missing.

        The peaks that belong to non continuous series are added to the
        ``non_grouped_peaklist`` PeakList.
        """

        ks = [ks for ks in self._kendrick_series_list
              if ks.largest_hole <= n_max_hole]
        ks_no_cont = [ks for ks in self._kendrick_series_list
                      if ks.largest_hole > n_max_hole]

        # update peaks in non_grouped_peaklist
        if self._non_grouped_peaklist is None:
            peaks = list()
        else:
            peaks = list(self.non_grouped_peaklist._peaks)
        for a_ks in ks_no_cont:
            peaks.extend(list(a_ks._peaks))
        non_grouped_peaklist = PeakList.from_list(peaks)

        return KendrickSeriesList(
            ks,
            non_grouped_peaklist=non_grouped_peaklist,
            building_block=self.building_block
        )

    def sort_series(self, ascending: bool = False, inplace: bool = False):
        """ Sort kendrick series according to SN or intensity if SN is
        not available.

        Args:
            ascending (bool): If True, sort kendrick series in ascending order.
                Default False.
            inplace (bool): If True, sort the kendrick series in place. If
                False (default) return a new KendrickSeriesList

        Retuns:
            A new ``KendrickSeriesList`` if inplace is False.
        """
        max_intensity = [ks.get_min_max_intensity()[1]
                         for ks in self._kendrick_series_list]
        argsort = np.argsort(max_intensity)

        if not ascending:
            argsort = argsort[::-1]

        ks_list = [self._kendrick_series_list[idx] for idx in argsort]
        if inplace:
            self._kendrick_series_list = ks_list
        else:
            return KendrickSeriesList(
                kendrick_series_list=ks_list,
                non_grouped_peaklist=self.non_grouped_peaklist,
                building_block=self.building_block
            )

    def get_valid_shape_series(self):
        """ Return a new KendrickSeriesList object with only series for
        which the shape is defined as valid.

        The peaks that belong to non continuous series are added to the
        ``non_grouped_peaklist`` PeakList.
        """

        ks = [ks for ks in self._kendrick_series_list if ks.is_shape_valid]
        ks_no_valid = [ks for ks in self._kendrick_series_list
                       if not ks.is_shape_valid]

        # update peaks in non_grouped_peaklist
        if self._non_grouped_peaklist is None:
            peaks = list()
        else:
            peaks = list(self.non_grouped_peaklist._peaks)
        for a_ks in ks_no_valid:
            peaks.extend(list(a_ks._peaks))
        non_grouped_peaklist = PeakList.from_list(peaks)

        return KendrickSeriesList(
            ks,
            non_grouped_peaklist=non_grouped_peaklist,
            building_block=self.building_block
        )


class AttributedKendrickSeriesList(KendrickSeriesList):
    """
    This class defines an Attributed List of Kendrick Series.
    """

    def __init__(
        self,
        kendrick_series_list: list,
        polarity: int,
        peaklist: PeakList = None,
        non_grouped_peaklist: PeakList = None,
        building_block: Union[KendrickBuildingBlock, str, float] = CH2
    ):
        """
        Create the Attributed Kendricks Series list.

        Args:
            kendrick_series_list (list-like): List or array of attributed
                kendrick series objects.
            polarity (int): The ionization charge
            peaklist (pyc2mc.core.peak.PeakList): the peaklist used to
                generate the KendrickSeriesList, is an optional parameter.
            non_grouped_peaklist(pyc2mc.core.peak.PeakList): the remaining
                peaklist of non-grouped peaks into Kendrick Series.
            building_block (KendrickBuildingBlock): Kendrick Building Block
                as formula, a float or a KendrickBuildingBlock, defaut CH2.
        """
        # TODO: This init MUST be read again !!
        warnings.warn(
            f"{self.__class__.__name__} is Not Yet Implemented",
            UserWarning, stacklevel=2)
        self.polarity = polarity
        building_block = KendrickBuildingBlock(building_block)
        super().__init__(
            kendrick_series_list=kendrick_series_list,
            non_grouped_peaklist=non_grouped_peaklist,
            building_block=building_block)

    @property
    def attributed_peak_list(self):
        """ Returns the Attributed Peak List. """
        return self._grouped_peaklist

