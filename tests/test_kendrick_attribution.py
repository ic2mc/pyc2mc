#!/usr/bin/env python
# coding: utf-8

from pytest import approx
import numpy as np

# import sys
# sys.path.insert(0, os.path.join(os.path.dirname(__file__), "../src/"))

from pyc2mc.core.formula_grid import FormulaGrid
from pyc2mc.processing.kendrick_attribution import kendrick_attribution
from pyc2mc.core.peak import PeakList

mz = [255.06628, 269.08193, 279.06816, 293.08381, 294.08716,
      307.09946, 308.10281, 321.11511, 322.11846, 336.13412,
      544.133, 558.14865, 572.1643]
intensities = [1.11682, 3.32477, 0.12531, 2.422, 0.34659, 1.25295,
               0.17497, 0.50866, 0.07845, 0.10309, 0.06389, 0.09763,
               0.13802]
SN = [116.9361, 349.7757, 12.3757, 254.5733, 35.7116, 148.7941,
      19.9551, 59.8374, 8.4184, 11.3637, 9.0113, 15.622, 22.5911]
pl = PeakList(mz=mz, intensity=intensities, SN=SN)

fg1 = FormulaGrid(
    elements=["C", "H", "N", "O", "B"],
    limits=[[1, 70], [4, 80], [0, 0], [2, 14], [0, 1]],
    H_C_bounds=[0.2, 3],
    dbe_bounds=[0, 50],
    mass_bounds=[100, 700],
    polarity=-1,
    element_ratios={('O', 'C'): (0, 1.4)},
    isotopes={(5, 10): 1, (6, 13): 2},
    combine_isotopes=[{(5, 10): 1, (6, 13): 1}]
)


class TestKendrickAttribution:

    def setup_method(self):

        self.attr1, _ = kendrick_attribution(
            peaklist=pl,
            formula_grid=fg1,
            lambda_parameter=1,  # ppm
            use_SN=True,
            use_isotopes=True,
            isotopic_pattern=(6, 13),
            building_block='C1 H2',
            min_length_threshold=3,
            use_lowest_error=True,
            ks_kwargs=dict(min_length=3,)
        )

        self.attr2, _ = kendrick_attribution(
            peaklist=pl,
            formula_grid=fg1,
            lambda_parameter=1,  # ppm
            use_SN=False,  # use intensities
            use_isotopes=True,
            isotopic_pattern=(6, 13),
            building_block='C1 H2',
            min_length_threshold=3,
            use_lowest_error=True,
            ks_kwargs=dict(min_length=3,)
        )

    def test_case_SN(self):

        assert len(self.attr1) == len(pl)
        assert len(self.attr1.get_nohits()) == 2
        assert all([('O' or 'B' or '10B') in group
                    for group in set(self.attr1.chem_groups) if group == group])

        attr_formulas = [str(p.formula) for p in self.attr1 if p.is_attributed]
        assert np.all(attr_formulas == [
            'C12 H12 O7 B1', 'C13 H14 O7 B1', 'C12 H14 O7 B1 13C1',
            'C14 H16 O7 B1', 'C13 H16 O7 B1 13C1', 'C15 H18 O7 B1',
            'C14 H18 O7 B1 13C1', 'C15 H20 O7 B1 13C1', 'C32 H21 O8 B1',
            'C33 H23 O8 B1', 'C34 H25 O8 B1'])

        assert np.all(self.attr1.formulas == self.attr2.formulas)
        assert self.attr1.error_in_ppm[6] == approx(0.005297901)

    def test_case_min_length_tresh(self):
        attr, _ = kendrick_attribution(
            peaklist=pl,
            formula_grid=fg1,
            lambda_parameter=1,  # ppm
            use_SN=False,  # use intensities
            use_isotopes=True,
            isotopic_pattern=(6, 13),
            building_block='C1 H2',
            min_length_threshold=4,
            use_lowest_error=True,
            ks_kwargs=dict(min_length=3,)
        )
        assert len(attr.get_nohits()) == 5

    def test_case_attribution_with_overlap(self):
        mz = [563.2241265961733, 595.2615678896756, 405.1397321826445,
              580.2506732273478, 485.2333304163954, 419.1427958605441, 
              407.15537415148134, 638.278619907832, 576.2193692896544, 
              421.158437829381, 422.1662688138092, 565.2397685650101, 
              592.2380949364106, 467.19896280080246, 451.18023715404644,
              623.2677252455042, 452.1880581384649, 560.2006736429277,
              548.2132319338455, 608.2568205831665, 640.2942618766689,
              593.2459159208289, 482.20986746314003, 497.22078212548746,
              578.2350212585011, 453.1958891228931, 624.2755462299226, 
              435.1615115072904, 469.21461476964913, 483.2176984475683, 
              437.177163476137, 625.283367214341, 403.12409021380756, 
              544.1819379961619, 610.2724625520035, 439.1645254173506, 
              546.1975899650085, 556.1648697008388, 427.1883437192669, 
              545.1771789682924, 423.16962979387125, 579.2383722385533, 
              581.2540242074001, 436.1648724873524, 438.1805144561893, 
              577.2227202697068]
        intensity = [1.0430467, 26.333374, 0.2686419, 5.5404735, 0.435822, 
                     0.7258461, 0.2364887, 2.2402458, 5.8399706, 0.5751863, 
                     2.1380296, 0.5235326, 11.3739338, 68.7288284, 15.7251225, 
                     22.3500576, 48.2033157, 0.3757688, 0.2206565, 42.3044281, 
                     1.1791044, 25.9256077, 5.9992766, 0.248057, 6.5065327, 
                     55.1494408, 6.2468152, 11.1819029, 52.554493, 1.5332063, 
                     18.3935661, 15.3562746, 0.2924143, 0.5462928, 40.0340042, 
                     0.1764358, 0.429839, 0.3218998, 0.7110226, 1.6617543, 
                     0.6743967, 2.8775992, 2.2782354, 3.4532962, 5.7286372, 
                     2.5444663]
        pid = [4097, 4866, 1539, 4484, 2696, 1675, 1556, 5914, 4383, 1696, 
               1707, 4141, 4784, 2353, 2097, 5563, 2111, 4032, 3777, 5185, 
               5959, 4812, 2637, 2897, 4433, 2130, 5589, 1878, 2400, 2657, 
               1903, 5615, 1524, 3700, 5240, 1929, 3739, 3935, 1778, 3715, 
               1716, 4456, 4512, 1890, 1918, 4406]
        SN = [31.49, 819.11, 7.54, 171.56, 12.15, 22.14, 6.52, 81.48, 180.88, 
              17.33, 67.22, 15.31, 353.23, 2066.86, 472.16, 823.42, 1449.31, 
              10.71, 8.14, 1559.63, 42.33, 806.41, 179.54, 6.51, 201.64, 
              1658.3, 229.3, 355.93, 1580.23, 45.17, 586.14, 565.39, 8.3, 
              21.91, 1475.86, 4.6, 16.99, 9.03, 21.67, 69.08, 20.5, 88.63, 
              69.96, 109.21, 181.84, 78.25]
    
        fg2 = FormulaGrid(
            elements=['C', 'H', 'N', 'O'],
            limits=[[0, 100], [0, 200], [0, 25], [0, 2]],
            polarity=-1,
            mass_bounds=(200, 1100),
            dbe_bounds=(0, 60),
            isotopes={(6, 13): 2, (7, 15): 1}
        )

        pl = PeakList(mz=mz, intensity=intensity, pid=pid, SN=SN)
        attr1, _ = kendrick_attribution(
            peaklist=pl,
            formula_grid=fg2,
            lambda_parameter=0.5,  # ppm
            use_SN=True,  
            use_isotopes=True,
            isotopic_pattern=(6, 13),
            building_block='N H',
            use_lowest_error=True,
            min_length_threshold=4,
            ks_kwargs=dict(min_length=6, n_max_hole=1)
        )
        assert len(attr1) == len(pl)
        assert len(attr1.get_nohits()) == 26
        assert len(attr1.get_attributed()) == 20

        attr2, _ = kendrick_attribution(
            peaklist=attr1,
            formula_grid=fg2,
            lambda_parameter=0.5,  # ppm
            use_SN=True,  
            use_isotopes=True,
            isotopic_pattern=(6, 13),
            building_block='N H2',
            min_length_threshold=4,
            use_lowest_error=True,
            ks_kwargs=dict(min_length=6, n_max_hole=1)
        )
        assert len(attr2) == len(pl)
        assert len(attr2.get_nohits()) == 11
        assert len(attr2.get_attributed()) == 35
        
        # peak belonging to the two kendrick series
        inter_pid = [1903, 2353, 5185, 5959]
        idx = [pl.get_peak_index(pid=i) for i in inter_pid]
        formulas1 = [attr1.str_formulas[i] for i in idx]
        formulas2 = [attr2.str_formulas[i] for i in idx]
        assert formulas1 == formulas2

        # this peak belongs to N H series
        assert attr1.str_formulas[2] == "C30 H19 N2"
        assert attr2.str_formulas[2] == "C30 H19 N2"

        # this peak belongs to N H2 series and is not attributed in N H
        assert attr1.str_formulas[8] == ""
        assert attr2.str_formulas[8] == "C30 H19 N4"
