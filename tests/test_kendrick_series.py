#!/usr/bin/env python
# coding: utf-8

from pathlib import Path
from pytest import raises

# import sys
# sys.path.insert(0, os.path.join(os.path.dirname(__file__), "../src"))
from pyc2mc.processing.kendrick_series import get_kendrick_series
from pyc2mc.io.peaklist import read_peaklist
from pyc2mc.core.peak import PeakList
from pyc2mc.core.kendrick import KendrickSeriesList


# Incomplete test

# directory with test files
test_dir = Path(__file__).parent / Path("../test_files")


class TestKendrickSeries:

    def setup_method(self):
        pl = read_peaklist(test_dir / "Biofuel_withB-containingSpecies_non_calibrated.csv")
        self.pl = pl.delimit(mz_bounds=(310, 800))

    def test_kendrick_series(self):
        ks_list_1 = get_kendrick_series(
            self.pl,
            building_block="C O", 
            min_length=10,
        )

        # Test initial series
        assert len(ks_list_1) == 24
        assert ks_list_1[0].is_continuous
        assert len(ks_list_1[0]) == 10
        assert not ks_list_1[2].is_continuous
        assert ks_list_1[2].largest_hole == 3

        ks_list_2 = get_kendrick_series(
            self.pl,
            building_block="C O", 
            min_length=9,
            limit_number=5
        )
        assert ks_list_2[0].is_shape_valid
        assert len(ks_list_2) == 5
        assert not ks_list_2[1].is_shape_valid
        assert len(ks_list_2.get_valid_shape_series()) == 4
        assert ks_list_2.get_valid_shape_series()[1][0].pid == 8071

    def test_split_series(self):

        ks_list_1 = get_kendrick_series(
            self.pl,
            building_block="C O", 
            min_length=10,
        )

        # test split
        ks_list_2, pl = ks_list_1[2].split_series(split=True, min_length=1)
        assert len(ks_list_2) == 2
        assert len(ks_list_2[0]) == 1
        assert len(ks_list_2[1]) == 9
        assert ks_list_2[1].is_continuous
        assert len(pl) == 0

        assert len(ks_list_1[2].split_series(split=True, min_length=2)[0]) == 1
        assert len(ks_list_1[2].split_series(split=True, min_length=2)[1]) == 1
        assert len(ks_list_1[2].split_series(split=False, min_length=1)[0]) == 1
        longest_ks, pl = ks_list_1[2].split_series(split=False, min_length=1)
        assert len(longest_ks[0]) == 9
        assert len(pl) == 1

        ks_list_2 = get_kendrick_series(
            self.pl,
            building_block="C O", 
            min_length=9,
            limit_number=25
        )
        
        assert ks_list_2[21].largest_hole == 1

        # test split in several parts
        ks22 = ks_list_2[22]
        ks22 = ks22.drop_peak(4)
        splitted_series, pl = ks22.split_series(split=True, min_length=1)
        assert len(splitted_series) == 3
        assert len(splitted_series[0]) == 1
        assert len(splitted_series[1]) == 3
        assert len(splitted_series[2]) == 5
        assert len(pl) == 0
        pid = list(splitted_series[0].pid)
        splitted_series, pl = ks22.split_series(split=True, min_length=2)
        assert len(pl) == 1
        assert list(pl.pid) == pid
        splitted_series, pl = ks22.split_series(split=False)
        assert len(pl) == 4

    def test_n_hole(self):

        ks_list_3 = get_kendrick_series(
            self.pl,
            building_block="C O", 
            min_length=9,
            limit_number=25
        )
        assert ks_list_3[21].largest_hole == 1
        assert not ks_list_3[21].is_continuous
        assert ks_list_3[22].largest_hole == 3
        assert not ks_list_3[22].is_continuous
        
        filt_ks = ks_list_3.get_continuous_series()
        assert len(filt_ks) == 23
        filt_ks = ks_list_3.get_continuous_series(n_max_hole=1)
        assert len(filt_ks) == 24
        assert filt_ks[21].largest_hole == 1
        assert filt_ks[22].largest_hole == 0

        ks_list_4 = get_kendrick_series(
            self.pl,
            building_block="C O", 
            min_length=9,
            limit_number=25,
            n_max_hole=0,
        )
        assert all(ks_list_4.is_continuous)

    def test_split_all(self):

        # Test filtering all
        ks_list_2 = get_kendrick_series(
            self.pl,
            building_block="C O", 
            min_length=9,
            limit_number=35
        )

        splitted_ks = ks_list_2.split_all(split=False)
        assert len(splitted_ks) == len(ks_list_2)
        assert len(splitted_ks) == 35
        for pid in [13918, 16940, 13856]: 
            # pid of peaks in series before splitting
            assert pid in splitted_ks.non_grouped_peaklist.pid
            assert pid not in ks_list_2.non_grouped_peaklist.pid

        assert len(splitted_ks[21]) == 8
        assert splitted_ks[21].is_continuous
        assert all(splitted_ks.is_continuous)

        assert len(ks_list_2.split_all(split=True, min_length=4)) == 35
        splitted_ks = ks_list_2.split_all(split=True, min_length=1)
        assert len(splitted_ks) == 38
        assert len(splitted_ks[21]) == 8
        assert len(splitted_ks[22]) == 1

        assert splitted_ks[22][0].pid == 13918
        assert splitted_ks[21][0].pid == 8697
        assert splitted_ks[21][-1].pid == 12760

        cont_ks = ks_list_2.get_continuous_series()
        assert len(cont_ks) == 32

    def test_sort_kendrick_series(self):
        mz = [255.06628, 269.08193, 279.06816, 293.08381, 294.08716,
              307.09946, 308.10281, 321.11511, 322.11846, 336.13412,
              544.133, 558.14865, 572.1643]
        intensities = [1.11682, 3.32477, 0.12531, 2.422, 0.34659, 1.25295,
                       0.17497, 0.50866, 0.07845, 0.10309, 0.06389, 0.09763,
                       0.13802]
        SN = [116.9361, 349.7757, 12.3757, 254.5733, 35.7116, 148.7941,
              19.9551, 59.8374, 8.4184, 11.3637, 9.0113, 15.622, 22.5911]
        pl = PeakList(mz=mz, intensity=intensities, SN=SN)
        ks = get_kendrick_series(pl)

        assert ks.lengths == [2, 4, 4, 3]

        ks = get_kendrick_series(pl, min_length=3)
        assert ks.is_continuous == [True, True, True]
        unsks = KendrickSeriesList([ks[2], ks[0], ks[1]])
        assert list(unsks[0].pid) == [10, 11, 12]
        assert list(unsks[1].pid) == [2, 3, 5, 7]
        assert list(unsks[2].pid) == [4, 6, 8, 9]

        sks = unsks.sort_series()
        assert list(sks[0].pid) == [2, 3, 5, 7]
        assert list(sks[1].pid) == [4, 6, 8, 9]
        assert list(sks[2].pid) == [10, 11, 12]
        
        sks = unsks.sort_series(ascending=True)
        assert list(sks[0].pid) == [10, 11, 12]
        assert list(sks[1].pid) == [4, 6, 8, 9]
        assert list(sks[2].pid) == [2, 3, 5, 7]

        unsks.sort_series(inplace=True)
        assert list(unsks[0].pid) == [2, 3, 5, 7]
        assert list(unsks[1].pid) == [4, 6, 8, 9]
        assert list(unsks[2].pid) == [10, 11, 12]

        with raises(AttributeError):
            unsks.sort_series(inplace=True).display_series()

    def test_get_series_in_kendrick_list(self):
        mz = [255.06628, 269.08193, 279.06816, 293.08381, 294.08716,
              307.09946, 308.10281, 321.11511, 322.11846, 336.13412,
              544.133, 558.14865, 572.1643]
        intensities = [1.11682, 3.32477, 0.12531, 2.422, 0.34659, 1.25295,
                       0.17497, 0.50866, 0.07845, 0.10309, 0.06389, 0.09763,
                       0.13802]
        SN = [116.9361, 349.7757, 12.3757, 254.5733, 35.7116, 148.7941,
              19.9551, 59.8374, 8.4184, 11.3637, 9.0113, 15.622, 22.5911]
        pl = PeakList(mz=mz, intensity=intensities, SN=SN)
        ks = get_kendrick_series(pl, min_length=3)

        assert ks.get_series_in_range((500, 600), verbose=False) == [2]
        assert ks.get_series_in_range((290, 300), verbose=False) == [0, 1]

        assert ks.get_series_with_mz(294.08716, lambda_parameter=1) == [1]
        assert ks.get_series_with_pid(12)[0][0] == 2
