#!/usr/bin/env python
# coding: utf-8

import pytest
from pytest import approx, raises
import numpy as np

# import sys
# sys.path.insert(0, os.path.join(os.path.dirname(__file__), "../src/pyc2mc"))

from pyc2mc.core.isotopes import Isotope, Element, NIST_DATA
# from mass_data import Isotope, IsotopicData


class TestElement:

    def test_most_abundant_element(self):
        O = Element(8)
        _16O = O.most_abundant_isotope
        assert _16O.Z == 8
        assert _16O.A == 16


class TestIsotope:

    def setup_method(self):
        self.C = Isotope(6, 12)
        self.W = Isotope(74, 184)

    def test_values(self):
        assert self.C.Z == 6
        assert self.C.A == 12

        assert self.W.Z == 74
        assert self.W.A == 184

        assert self.C.mass_number == self.C.A
        assert self.C.atomic_number == self.C.Z

        assert self.C.element == "C"
        assert self.W.element == "W"

        assert self.C.abundance == approx(np.float64(0.98938))
        assert self.C.exact_mass == approx(np.float64(12.000000000))

        assert self.W.abundance == approx(np.float64(0.30642))
        assert self.W.exact_mass == approx(np.float64(183.9509309294))

    def test_most_abundant_isotope(self):
        _18O = Isotope(8, 18)
        _16O = Isotope(8, 16)

        assert _18O.exact_mass_most_abundant == approx(15.9949146)
        assert _16O.exact_mass_most_abundant == approx(15.9949146)
        assert _16O.mass_number_most_abundant == 16
        assert _18O.mass_number_most_abundant == 16
        assert _18O.exact_mass == approx(17.99915961)

    def test_isotope_filtering(self):
        data = Isotope.get_isotopes_data(elements=["C", "N"])
        idata = Isotope.get_isotopes_data(atomic_numbers=[6, 7])
        assert all(data == idata)
        assert len(data) == 32

        clean_data = Isotope.get_isotopes_data(elements=["C", "N"], dropna=True)
        assert len(clean_data) == 4

        abund = Isotope.get_most_abundant(atomic_numbers=[6, 74])
        assert len(abund) == 2
        abund.set_index("element", inplace=True)
        assert abund.loc["C", "isotopic abundance"] == np.float64(0.98938)
        assert abund.loc["W", "isotopic abundance"] == np.float64(0.30642)

        with pytest.raises(ValueError):
            Isotope.get_most_abundant()
        with pytest.raises(ValueError):
            Isotope.get_most_abundant(elements=["WC"])

    def test_get_isotope_mass_shifts(self):

        df = Isotope.get_mass_shifts("S")
        assert all(df.isotopes.values == ["34S-32S", "33S-32S"])
        assert df.at[1, "mass_shift"] == pytest.approx(0.999388)
        assert df.at[0, "probability"] == pytest.approx(0.0403946532)
        
        df = Isotope.get_mass_shifts("S", threshold=1e-4)
        assert len(df) == 4
        assert df.isotopes.values[3] == "36S-32S"

        assert len(Isotope.get_mass_shifts(2)) == 0

        with pytest.raises(ValueError):
            Isotope.get_mass_shifts(-1)

        with pytest.raises(ValueError):
            Isotope.get_mass_shifts("toto")
        
        with pytest.raises(ValueError):
            Isotope.get_mass_shifts("12C")


class TestNISTData:

    def test_get_element_data(self):

        d = NIST_DATA.get_element_data(34)
        assert d["atomic number"] == 34
        assert d["mass number"] == 80

        with raises(ValueError):
            NIST_DATA.get_element_data(1983)

        with raises(ValueError):
            NIST_DATA.get_element_data(-2)

    def test_get_Z_from_string(self):

        Z = NIST_DATA.get_Z_from_string("Se")
        assert Z == 34

        Z = NIST_DATA.get_Z_from_string(" Pt ")
        assert Z == 78

        with raises(ValueError):
            NIST_DATA.get_Z_from_string("se")

        with raises(ValueError):
            NIST_DATA.get_Z_from_string("toto")

    def test_get_isotope_data(self):
        
        data = NIST_DATA.get_isotope_data(34, 78)
        assert data["mass number"] == 78
        assert data["atomic number"] == 34
        assert data["exact mass"] == approx(77.917309282)

        with raises(ValueError):
            NIST_DATA.get_isotope_data(6, 36)
