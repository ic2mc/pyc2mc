#!/usr/bin/env python
# coding: utf-8

from pathlib import Path
from numpy.testing import assert_allclose
from pytest import approx, raises
import pandas as pd

from pyc2mc.io.peaklist import read_predator
from pyc2mc.core.calibration import CalibList
from pyc2mc.processing.caliblist_builder import CalibListBuilder

# directory with test files
test_dir = Path(__file__).parent / Path("../test_files")

# Non Calibrated Peak List
pl = read_predator(
    test_dir / 'Biofuel_withB-containingSpecies_non_calibrated.csv')
pl = pl.delimit(mz_bounds=(300, 360))


# Calibration List for Standard Calibration
mz_theo = [
    301.108147, 315.123797, 315.160183, 329.139447, 329.175833,
    343.155097, 343.191483, 357.207133
]
mz_exp = [
    301.10918, 315.12490, 315.16128, 329.14061, 329.17699, 343.15631, 
    343.19270, 357.20841
]


class TestCalibList:

    def setup_method(self):

        self.cl = CalibList.from_peaklist(
            pl, mz_theo, 
            searching_mode='most_abundant',
            lambda_parameter=4,
        )
        
        self.cl2 = CalibList.from_peaklist(
            pl, mz_theo, 
            searching_mode='most_abundant',
            lambda_parameter=4,
        )

    def test_caliblist_from_peaklist(self):

        assert len(self.cl) == 8
        assert self.cl.is_freq_avail
        assert self.cl.is_intens_avail
        assert self.cl.name == pl.name
        assert self.cl.polarity is None
        # assert self.cl.is_pid_avail

        assert_allclose(self.cl.mz_exp, mz_exp)

        cl1 = CalibList.from_peaklist(
            pl, mz_theo, 
            searching_mode='closest',
            lambda_parameter=4,
            name="my_calib_list",
            polarity=-1,
        )

        assert cl1.name == "my_calib_list"
        assert cl1.polarity == -1
        assert len(cl1) == 8
        assert_allclose(cl1.mz_exp, [301.10839, 315.12407, 315.16128, 
                                     329.13969, 329.17699, 343.1552, 
                                     343.1927, 357.20841])
        
        cl2 = CalibList.from_peaklist(
            pl, mz_theo, 
            searching_mode='most_abundant_corrected',
            lambda_parameter=4,
        )
        assert len(cl2) == 7
        assert_allclose(cl2.mz_exp, mz_exp[1:])

    def test_calib_list_from_data_frame(self):
        
        for mz_col in ['m/z', 'mz', 'mz_theo', 'calibrant', "calibrant mz",
                       'm/z', 'mZ', 'MZ_Theo', 'Calibrant', "CALIBRANT mZ"]:
            df = pd.DataFrame({mz_col: mz_theo, 'mz_exp': mz_exp})
            cl = CalibList.from_dataframe(df, polarity=-1)
            assert_allclose(cl.mz_theo, mz_theo)

        df = pd.DataFrame({"toto": mz_theo, 'mz_exp': mz_exp})
        with raises(ValueError, match=r'The argument must be a data'):
            cl = CalibList.from_dataframe(df)

        df = pd.DataFrame({"mz_theo": mz_theo, 'mz_exp': mz_exp})
        df["test"] = "random"
        cl = CalibList.from_dataframe(df, polarity=-1)

        assert cl.name == "calibration_list_from_df"
        assert cl.polarity == -1
        assert_allclose(cl.mz_exp, mz_exp)

    def test_caliblist_mean_error(self):
        pass


# class TestCalibListBuilding:

#     def setup_method(self):
#         self.cl_build = CalibListBulding.from_dataframe(df)
#         self.add_df = pd.DataFrame({
#             'mz': [371.222783, 371.186398],
#             'mz_exp': [371.22411836, 371.18773205]})

#     def test_calibration_list_building(self):

#         assert len(self.cl_build) == 8
#         assert all(self.cl_build.calibration_list.columns ==
#                    ['mz', 'formula', 'mz_exp', 'frequency',
#                     'intensity'])
#         self.cl_build.add_dataframe(self.add_df)
#         assert len(self.cl_build) == 10
#         self.cl_build.remove_point(9)
#         assert len(self.cl_build) == 9
#         cl = self.cl_build.get_calibration_list()
#         assert cl.err_ppm[2] == approx(3.48076965)
#         assert cl.err_ppm[-1] == approx(3.5971930095)
