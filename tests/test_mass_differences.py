#!/usr/bin/env python
# coding: utf-8

from pytest import approx
import numpy as np
# from numpy.testing import assert_allclose
# import sys
# sys.path.insert(0, os.path.join(os.path.dirname(__file__), "../src/pyc2mc"))

from pyc2mc.core.peak import PeakList
from pyc2mc.processing.mass_differences import MassDifferences

masses = [
    253.18092, 269.24861, 270.25196, 273.11323, 274.11658, 279.23295,
    280.2363, 281.2486, 282.25196, 283.26426, 284.2676, 287.12888, 288.13224,
    292.08744, 293.08381, 298.11326, 298.11658, 299.10963, 299.20166, 300.2050,
    301.21731, 302.22066, 303.10454, 312.12891, 313.12528, 316.12382, 317.1201,
    340.12383, 341.12019, 343.1551, 344.15845, 354.13948, 355.13585, 368.15512,
    369.15149, 380.15512, 381.15149, 382.13438, 382.17077, 383.16714]

intensities = [
    12.83175, 14.80845, 2.54562, 13.23343, 2.20589, 22.94802,
    4.22542, 36.04525, 6.974, 14.62329, 2.87114, 13.79734, 2.60829, 0.57452,
    2.422, 0.53666, 2.26976, 2.31356, 25.06023, 5.34132, 14.13623, 2.91456,
    1.67492, 0.48992, 1.96472, 0.41788, 1.81642, 0.6449, 2.66164, 12.36872,
    2.5173, 1.10068, 4.38844, 1.15203, 4.77519, 0.46112, 1.87099, 0.40988,
    0.83053, 3.42186]

pl = PeakList(mz=masses, intensity=intensities)


class TestMassDifferences:

    def setup_method(self):
        self.md = MassDifferences(pl, delta_mz_bounds=(10, 20))
        self.sorted_md = MassDifferences(pl, delta_mz_bounds=(10, 20), sort=True)

    def test_values(self):
        data = self.md.get_target_stats(14.01565)
        assert data["count"] == 13
        assert data["m/z mean"] == approx(14.01564230)

        data, (idx, jdx) = self.md.get_target_stats(14.015650, atol=1e-4,
                                                    return_indices=True)
        
        assert np.all(idx == np.array([1,  2,  3,  4, 15, 17, 22, 27,
                                       28, 31, 32, 33, 34]))
        assert np.all(jdx == np.array([9, 10, 11, 12, 23, 24, 26, 31,
                                       32, 33, 34, 38, 39]))
    
    def test_mz_diff_sorted(self):
        sorted_mz_diff = self.sorted_md.mz_diff
        assert np.all(sorted_mz_diff[:-1] <= sorted_mz_diff[1:]), "mz_diff array is not sorted"

    def test_indices_sorted(self):
        mz_diff = self.md.mz_diff
        indices = self.md._indices

        sorted_indices = np.argsort(mz_diff)
        sorted_mz_diff = mz_diff[sorted_indices]

        idx_start = indices[0]
        idx_end = indices[1]
        idx_start_sorted = idx_start[sorted_indices]
        idx_end_sorted = idx_end[sorted_indices]
        sorted_indices_reconstructed = np.vstack((idx_start_sorted, idx_end_sorted))

        assert np.all(self.sorted_md.mz_diff == sorted_mz_diff), "mz_diff array is not sorted as expected"
        assert len(idx_start_sorted) == len(idx_end_sorted) == len(sorted_indices_reconstructed[0]) == len(sorted_indices_reconstructed[1])
        assert len(idx_start_sorted) == len(idx_end_sorted) == len(self.sorted_md._indices[0]) == len(self.sorted_md._indices[1])
        assert np.array_equal(self.sorted_md._indices, sorted_indices_reconstructed), "_indices array is not sorted correctly"

    def test_mz_diff_range(self):
        mz_diff_range = self.md.get_mz_diff_in_range(delta_mz_bounds=(11, 20))
        sorted_mz_diff_range = self.sorted_md.get_mz_diff_in_range(delta_mz_bounds=(11, 20))
        mz_diff_range = np.sort(mz_diff_range)
        assert np.all(mz_diff_range == sorted_mz_diff_range), "mz_diff_in_range is not the same for sorted and unsorted"
