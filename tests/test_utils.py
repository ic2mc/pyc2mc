#!/usr/bin/env python
# coding: utf-8

from pytest import raises
from pathlib import Path

# import sys
# sys.path.insert(0, os.path.join(os.path.dirname(__file__), "../src/"))

from pyc2mc.utils import get_file_list

# directory with test files
test_dir = Path(__file__).parent / Path("../test_files")


class TestGetFileList:

    def test_get_file_list(self):
        file_list = get_file_list(test_dir / "Coaddition" / "raw", extension=".dat")

        assert len(file_list) == 9
        p = Path(file_list[2])
        assert str(p.relative_to(test_dir)) == "Coaddition/raw/2023June16_NegESI_SRFAtypeII_Poros_scan000003.dat"
    
        with raises(ValueError, match="^Directory 'toto' does not"):
            get_file_list("toto")

        with raises(FileNotFoundError,
                    match=r"^No files found with extension '.xxx'"):
            get_file_list(test_dir / "Coaddition" / "raw", extension=".xxx")    

        file_list = get_file_list(test_dir / "EVA_Cross", extension=".ascii")
        assert len(file_list) == 111
        p = Path(file_list[88])
        assert str(p.relative_to(test_dir)) == str("EVA_Cross/Analysis_0089.ascii")

        file_list = get_file_list(test_dir / "EVA_Cross", extension=".xml")
        assert len(file_list) == 1
        p = Path(file_list[0])
        assert str(p.relative_to(test_dir)) == str("EVA_Cross/scan.xml")
