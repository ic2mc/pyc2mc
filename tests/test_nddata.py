#!/usr/bin/env python
# coding: utf-8

import pytest
import numpy as np
from pathlib import Path

# import sys
# sys.path.insert(0, os.path.join(os.path.dirname(__file__), "../src/"))

from pyc2mc.core.peak import PeakList
from pyc2mc.core.nddata import PeakListCollection

# directory with test files
test_dir = Path(__file__).parent / Path("../test_files")


class TestPeakListCollection:

    def test_base(self):
        h = PeakListCollection(peaklists=[
            PeakList([70., 196.33, 71.12, 185.59], [6.21, 0.74, 21.01, 1.56]),
            PeakList([105.15, 45.16, 36.26], [15.86, 18.26, 13.62]),
            PeakList([91.81, 49.22, 91.46, 22.83, 58.2],
                     [4.68, 14.04, 2.86, 10.65, 10.81]),
        ])

        assert len(h) == 3
        assert h.mz.shape == (3, 5)
        assert h.intensity.shape == (3, 5)
        assert np.isnan(h.mz[1, 3])
        assert np.isnan(h.intensity[1, 3])

        assert h.max_length == 5
        assert h.min_length == 3
        assert list(h.scan_ids) == [0, 1, 2]
        assert h.scan_lengths == [4, 3, 5]

        filter_intens = h.filter_intensity(threshold=10)
        assert len(filter_intens) == 3
        assert filter_intens.scan_lengths == [1, 3, 3]
        assert filter_intens.mz[2, 0] == pytest.approx(22.83)
        assert filter_intens.intensity[2, 2] == pytest.approx(10.81)

        min_I, max_I = filter_intens.get_min_max_intensity()
        assert max_I == pytest.approx(21.01)
        assert min_I == pytest.approx(10.65)

        min_m, max_m = filter_intens.get_min_max_mz()
        assert min_m == pytest.approx(22.83)
        assert max_m == pytest.approx(105.15)

        with pytest.raises(ValueError,
                           match=r"^Length of scan ids is not consistent"):
            h = PeakListCollection(peaklists=[
                    PeakList([70., 196.33, 71.12, 185.59], [6.21, 0.74, 21.01, 1.56]),
                    PeakList([105.15, 45.16, 36.26], [15.86, 18.26, 13.62]),
                    PeakList([91.81, 49.22, 91.46, 22.83, 58.2],
                             [4.68, 14.04, 2.86, 10.65, 10.81]),
                ],
                scan_ids=[3, 4])
            
        with pytest.raises(ValueError,
                           match=r"^mz_bounds must be a tuple of 2 floats"):
            h.delimit(3.4)

    def test_EVA_cross(self):
        self.EVA_cross = PeakListCollection.from_directory(
            directory_path=test_dir / 'EVA_Cross/',
            verbose=False
        )

        assert len(self.EVA_cross) == 111

        few_data = self.EVA_cross[20: 60: 5]
        assert len(few_data) == 8
        assert list(few_data.scan_ids) == [20, 25, 30, 35, 40, 45, 50, 55]
        assert self.EVA_cross.intensity[2][1] == pytest.approx(4315908.257733068)

        min_m, max_m = few_data.get_min_max_mz()
        assert max_m == pytest.approx(1981.17498)

        min_I, max_I = few_data.get_min_max_intensity()
        assert min_I == pytest.approx(149516.0)

        delim_data = self.EVA_cross.delimit(mz_bounds=(250, 450))
        assert len(delim_data) == 111
        min_I, max_I = delim_data.get_min_max_intensity()
        assert max_I == pytest.approx(42498060.0)
        assert min_I == pytest.approx(102718.0)

        assert len(np.where(delim_data.mz < 250)[0]) == 0
        assert len(np.where(delim_data.mz > 450)[0]) == 0
