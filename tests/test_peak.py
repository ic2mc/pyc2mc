#!/usr/bin/env python
# coding: utf-8

import pytest
from pytest import approx, raises
import numpy as np
from numpy.testing import assert_almost_equal

# import sys
# sys.path.insert(0, os.path.join(os.path.dirname(__file__), "../src/pyc2mc"))

from pyc2mc.core.peak import Peak, AttributedPeak, PeakList, AttributedPeakList
from pyc2mc.core.formula import Formula


mz = [
    253.18092, 269.24861, 270.25196, 273.11323, 274.11658, 279.23295,
    280.2363, 281.2486, 282.25196, 283.26426, 284.2676, 287.12888, 288.13224,
    292.08744, 293.08381, 298.11326, 298.11658, 299.10963, 299.20166, 300.20501,
    301.21731, 302.22066, 303.10454, 312.12891, 313.12528, 316.12382, 317.12019,
    340.12383, 341.12019, 343.1551, 344.15845, 354.13948, 355.13585, 368.15512,
    369.15149, 380.15512, 381.15149, 382.13438, 382.17077, 383.16714]

intensities = [
    12.83175, 14.80845, 2.54562, 13.23343, 2.20589, 22.94802,
    4.22542, 36.04525, 6.974, 14.62329, 2.87114, 13.79734, 2.60829, 0.57452,
    2.422, 0.53666, 2.26976, 2.31356, 25.06023, 5.34132, 14.13623, 2.91456,
    1.67492, 0.48992, 1.96472, 0.41788, 1.81642, 0.6449, 2.66164, 12.36872,
    2.5173, 1.10068, 4.38844, 1.15203, 4.77519, 0.46112, 1.87099, 0.40988,
    0.83053, 3.42186]

SN = [intens / max(intensities) * 100 for intens in intensities]

formulas = [
    'C15 H25 O3', 'C17 H33 O2', "", 'C16 H17 O4', "", 'C18 H31 O2',
    "", 'C18 H33 O2', "", 'C18 H35 O2 18O1', "", 'C17 H19 O4', "", "",
    "", "", "", "", 'C20 H27 O2', "", 'C20 H29 O2', "",
    "", "", "", "", "", "", "", 'C20 H23 O5 13C1', "",
    "", "", "", "", "", "", "", "", ""]


class TestPeak:

    def setup_method(self):
        self.p0 = Peak(269.24861, 14.80845)
        self.p = Peak(269.24861, 14.80845, 20.2983298, 4)

    def test_peak_min(self):
        assert self.p0.pid == 0
        assert not self.p0.SN_avail
        assert self.p0.SN is None

        assert repr(self.p0) == ("Peak(mz=  269.248610,"
                                 " intensity=1.480845e+01, pid=0)")

    def test_attributes(self):

        assert self.p.pid == 4
        assert self.p.mz == approx(269.24861)
        assert self.p.intensity == approx(14.80845)
        assert self.p.SN == 20.2983298
        assert self.p.SN_avail

        assert self.p.integer_part == approx(269)

        assert repr(self.p) == ("Peak(mz=  269.248610, intensity=1.480845e+01"
                                ", SN=  20.298, pid=4)")


class TestAttributedPeak:

    def setup_method(self):
        self.p1 = AttributedPeak(253.18092, 12.83175, "C15 H25 O3", -1,
                                 SN=None, pid=32)
        self.formula = Formula.from_string("C15 H25 O3")

    def test_attributed_peak(self):

        assert self.p1.formula == self.formula
        assert not self.p1.is_isotopic
        assert not self.p1.SN_avail
        assert self.p1.chem_class == "O3"
        assert self.p1.chem_group == "O"
        assert self.p1.formula == self.formula

        assert self.p1.nominal_mass == approx(253.)
        assert self.p1.calculated_mz == approx(253.1809182445901)
        assert self.p1.str_formula == str(self.formula)

        assert self.p1.error_in_mDa == approx(-0.0017554098974414956)
        assert self.p1.error_in_ppm == approx(-0.006933421008235895)

        for formula in [0, np.nan, 3.4]:
            ap = AttributedPeak(mz=2.3, intensity=3.0, formula=formula, polarity=-1)
            assert not ap.is_attributed
            assert np.isnan(ap.formula)
        
        with pytest.warns(RuntimeWarning):
            AttributedPeak(mz=2.3, intensity=3.0, formula="formula", polarity=-1)


class TestPeakList:

    def setup_method(self):
        self.pl1 = PeakList(mz=mz, intensity=intensities)
        self.pl2 = PeakList(mz=mz, intensity=intensities, SN=SN)

    def test_peaklist(self):

        assert not self.pl1.SN_avail
        assert self.pl2.SN_avail

        with pytest.raises(ValueError):
            PeakList([1, 2, 3], [1, 2])

        with pytest.raises(ValueError):
            PeakList([1, 2, 3], [1, 2, 3], SN=[1.])

        with pytest.raises(ValueError):
            PeakList([1, 2, 3], [1, 2, 3], pid=[1, 4])

        assert len(self.pl1) == len(mz)
        assert len(self.pl2) == len(mz)
        # assert self.pl1.size == len(masses)

        assert self.pl1[6].mz == approx(280.2363)
        assert self.pl1[6].intensity == approx(4.22542)
        assert self.pl2[6].SN == approx(self.pl1[6].intensity / max(intensities) * 100)

        assert self.pl1.mz[6] == approx(280.2363)
        assert self.pl1.intensity[6] == approx(4.22542)

        min_I, max_I = self.pl1.get_min_max_intensity()
        assert min_I == approx(min(intensities))
        assert max_I == approx(max(intensities))

        min_mz, max_mz = self.pl1.get_min_max_mz()
        assert min_mz == approx(min(mz))
        assert max_mz == approx(max(mz))

        pl = PeakList([1, 2, 3], [1, 2, 3], 
                      peak_properties={"freq": [1, 2, 3]})
        assert "freq" in pl.peak_properties
        assert "freq" in pl[0].properties
        assert pl[1].properties["freq"] == 2

        assert self.pl1.get_peak_index(mz_target=288.2) == 12
        assert self.pl1.get_peak_index(pid=12) == 12
        with raises(IndexError):
            self.pl1.get_peak_index(pid=12000)
        with raises(ValueError):
            self.pl1.get_peak_index()

    def test_delimit_peaklist(self):

        pl = self.pl1.delimit()
        assert len(pl) == 0

        with raises(ValueError, match=r"^mz_bounds must be a tuple of 2 floats"):
            self.pl1.delimit(18.0)

        pl = self.pl1.delimit(mz_bounds=(300, 360))
        assert len(pl) == 14
        assert pl[0].mz == approx(300.20501)
        assert pl[-1].mz == approx(355.13585)

        pl = self.pl1.delimit(intensity_bounds=12)
        assert len(pl) == 10
        intensity = np.array([12.83175, 14.80845, 13.23343, 22.94802, 36.04525,
                              14.62329, 13.79734, 25.06023, 14.13623, 12.36872])
        assert_almost_equal(pl.intensity, intensity)
        assert_almost_equal(pl.mz, [253.18092, 269.24861, 273.11323,
                                    279.23295, 281.2486, 283.26426,
                                    287.12888, 299.20166, 301.21731,
                                    343.1551])
        assert np.all(pl.pid == [0, 1, 3, 5, 7, 9, 11, 18, 20, 29])

        pl = self.pl1.delimit(intensity_bounds=(4, 6))
        assert len(pl) == 4
        assert np.all(pl.pid == [6, 19, 32, 34])

        assert len(self.pl1.delimit(SN_bounds=4)) == 0

        pl = self.pl2.delimit(SN_bounds=50)
        assert len(pl) == 3
        assert_almost_equal(pl.mz, [279.23295, 281.2486, 299.20166])
        pl = self.pl2.delimit(SN_bounds=(20, 50))
        assert len(pl) == 7
        assert all(pl.pid == [0, 1, 3, 9, 11, 20, 29])

        pl = self.pl2.delimit(mz_bounds=(260, 280), SN_bounds=40)
        assert len(pl) == 2
        assert np.all(pl.pid == [1, 5])

        assert len(self.pl1.delimit(mz_bounds=(50, 100))) == 0
        assert len(self.pl1.delimit(mz_bounds=(300, 360), intensity_bounds=400)) == 0


class TestAttributedPeakList:

    def setup_method(self):
        self.pl1 = AttributedPeakList(
            mz=mz, intensity=intensities, formulas=formulas,
            polarity=-1)

    def test_attributed_peaklist(self):

        with raises(TypeError):
            AttributedPeakList(mz=mz, intensity=intensities, formulas=formulas)

        assert not self.pl1.SN_avail
        assert self.pl1[6].mz == approx(280.2363)
        assert self.pl1[6].intensity == approx(4.22542)

        assert sorted(self.pl1.species) == ['13C', '18O', 'C', 'H', 'O']
        assert sorted(self.pl1.isotopes) == ["13C", "18O"]
        assert sorted(self.pl1.elemental_composition) == ["C", "H", "O"]

        assert len(self.pl1.get_attributed()) == 10
        assert len(self.pl1.get_nohits()) == 30

        assert self.pl1.all_classes == {'O2', "O2 18O1", 'O3', 'O4', 'O5 13C1'}
        assert self.pl1.monoisotopic_classes == {'O2', 'O3', 'O4'}

        df = self.pl1.get_classes()
        assert list(df["Chem. Group"]) == ["O", "O", "O 18O", "O", "O 13C"]
        assert list(df["Chem. Class"]) == ['O2', 'O4', 'O2 18O1', 'O3', 'O5 13C1']
        assert df.iat[0, 3] == approx(62.828178)
        df = self.pl1.get_classes(threshold=10)
        assert len(df) == 2
        df = self.pl1.get_classes(sort_chem_class=True)
        assert list(df["Chem. Group"]) == ['O', 'O', 'O', 'O 13C', 'O 18O']
        assert list(df["Chem. Class"]) == ['O2', 'O3', 'O4', 'O5 13C1', 'O2 18O1']
        df = self.pl1.get_classes(sort_chem_class=True, monoisotopic=True)
        assert list(df["Chem. Class"]) == ['O2', 'O3', 'O4', 'O5']

    def test_update_attributed_peaklist(self):

        peaks = [
            AttributedPeak(**self.pl1[12].as_dict(), formula="C17 H19 O4",
                           polarity=-1),
            AttributedPeak(**self.pl1[22].as_dict(), formula="C19 H27 O3",
                           polarity=-1),
            AttributedPeak(**self.pl1[30].as_dict(), formula="C20 H24 O5",
                           polarity=-1)
        ]

        pl2 = self.pl1.update_peaks(peaks)

        for ip in [12, 22, 30]:
            assert not self.pl1[ip].is_attributed
            assert pl2[ip].is_attributed

            assert self.pl1[ip].mz == approx(pl2[ip].mz)
            assert self.pl1[ip].intensity == approx(pl2[ip].intensity)

        assert str(pl2[12].formula) == "C17 H19 O4"
        assert str(pl2[22].formula) == "C19 H27 O3"
        assert str(pl2[30].formula) == "C20 H24 O5"
