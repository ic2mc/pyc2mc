#!/usr/bin/env python
# coding: utf-8

from pytest import approx

# import sys
# sys.path.insert(0, os.path.join(os.path.dirname(__file__), "../src/"))

from pyc2mc.core.formula_grid import FormulaGrid
from pyc2mc.processing.standard_attribution import (lowest_error_attribution,
                                                    isotopic_pattern_attribution)
# from pyc2mc.processing.attribution import PeakListAttribution
from pyc2mc.core.peak import Peak, PeakList

masses = [
    253.18092, 269.24861, 270.25196, 273.11323, 274.11658, 279.23295,
    280.2363, 281.2486, 282.25196, 283.26426, 284.2676, 287.12888, 288.13224,
    292.08744, 293.08381, 298.11326, 298.11658, 299.10963, 299.20166, 300.2050,
    301.21731, 302.22066, 303.10454, 312.12891, 313.12528, 316.12382, 317.1201,
    340.12383, 341.12019, 343.1551, 344.15845, 354.13948, 355.13585, 368.15512,
    369.15149, 380.15512, 381.15149, 382.13438, 382.17077, 383.16714]

intensities = [
    12.83175, 14.80845, 2.54562, 13.23343, 2.20589, 22.94802,
    4.22542, 36.04525, 6.974, 14.62329, 2.87114, 13.79734, 2.60829, 0.57452,
    2.422, 0.53666, 2.26976, 2.31356, 25.06023, 5.34132, 14.13623, 2.91456,
    1.67492, 0.48992, 1.96472, 0.41788, 1.81642, 0.6449, 2.66164, 12.36872,
    2.5173, 1.10068, 4.38844, 1.15203, 4.77519, 0.46112, 1.87099, 0.40988,
    0.83053, 3.42186]

pl = PeakList(mz=masses, intensity=intensities)

# Adding noise to the peaklist

masses = [
    253.18092, 269.24861, 270.25196, 273.11323, 274.11658, 279.23295,
    280.2363, 281.2486, 282.25196, 283.26426, 284.2676, 287.12888, 288.13224,
    292.08744, 293.08381, 298.11326, 298.11658, 299.10963, 299.20166, 300.2050,
    301.21731, 302.22066, 303.10454, 312.12891, 313.12528, 316.12382, 317.1201,
    340.12383, 341.12019, 343.1551, 344.15845, 354.13948, 355.13585, 368.15512,
    369.15149, 380.15512, 381.15149, 382.13438, 382.17077, 383.16714,
    415.38368393, 415.50610146, 415.63590702, 415.22591204, 415.69240247,
    415.24413871, 415.87066474, 416.04669897, 415.75609969, 415.90952883,
    415.868888, 415.19804603, 415.69526796, 415.70751277, 415.89802095,
    415.1714698, 415.72957749, 415.48772875, 415.89938987, 415.1346747]

intensities = [
    12.83175, 14.80845, 2.54562, 13.23343, 2.20589, 22.94802,
    4.22542, 36.04525, 6.974, 14.62329, 2.87114, 13.79734, 2.60829, 0.57452,
    2.422, 0.53666, 2.26976, 2.31356, 25.06023, 5.34132, 14.13623, 2.91456,
    1.67492, 0.48992, 1.96472, 0.41788, 1.81642, 0.6449, 2.66164, 12.36872,
    2.5173, 1.10068, 4.38844, 1.15203, 4.77519, 0.46112, 1.87099, 0.40988,
    0.83053, 3.42186, 0.11009898, 0.40759727, 0.30641058, 0.02245286,
    0.32109347, 0.11543149, 0.13432237, 0.12827542, 0.04136736, 0.48812367,
    0.0413115, 0.46116911, 0.38208597, 0.12650053, 0.43630513,
    0.03594391, 0.46312538, 0.31103192, 0.28377343, 0.0386996]

pl2 = PeakList(mz=masses, intensity=intensities)

fg1 = FormulaGrid(
    elements=["C", "H", "N", "O", "B"],
    limits=[[10, 30], [10, 40], [0, 0], [4, 10], [1, 1]],
    H_C_bounds=[0.2, 3],
    dbe_bounds=[0, 50],
    mass_bounds=[100, 800],
    polarity=-1,
    element_ratios={("O", "C"): (0, 0.8)},
    isotopes={(5, 10): 1, (6, 13): 2},
    combine_isotopes=[{(5, 10): 1, (6, 13): 1}]
)

fg2 = FormulaGrid(
    elements=["C", "H", "N", "O", "B"],
    limits=[[10, 25], [10, 40], [0, 0], [0, 10], [0, 0]],
    H_C_bounds=[0.2, 3],
    dbe_bounds=[0, 50],
    mass_bounds=[100, 800],
    polarity=-1,
    element_ratios={("O", "C"): (0, 0.8)},
    isotopes={(6, 13): 2, (8, 18): 1},
    combine_isotopes=None
)


class TestLowestErrorAttribution:

    def test_one_peak(self):
        peak = Peak(mz=355.135850, intensity=1)
        results = lowest_error_attribution(peak, fg1)
        assert len(results) == 1
        attr_peak = results[0]
        assert attr_peak.polarity == -1
        assert str(attr_peak.formula) == "C19 H20 O6 B1"
        assert str(attr_peak.str_formula) == "C19 H20 O6 B1"
        assert attr_peak.attribution_method == 'lowest_error'

    def test_attribution_lowesterror_no_isotopes(self, benchmark):

        attrpl1 = benchmark(
            lowest_error_attribution,
            peaklist=pl,
            formula_grid=fg1,
            lambda_parameter=0.3,
            use_isotopes=False
        )

        assert attrpl1.size == 40
        assert len(attrpl1.to_dataframe(attributed_only=True)) == 10
        chem_groups = {"O B"}
        assert all([peak.formula.chem_group in chem_groups
                    for peak in attrpl1 if peak.is_attributed])

        formulas = [
            'C13 H14 O7 B1', 'C16 H16 O5 B1', 'C15 H16 O6 B1', 'C17 H18 O5 B1',
            'C16 H18 O6 B1', 'C18 H18 O6 B1', 'C19 H20 O6 B1', 'C20 H22 O6 B1',
            'C21 H22 O6 B1', 'C21 H24 O6 B1']
        attr_formulas = [str(p.formula) for p in attrpl1 if p.is_attributed]
        assert attr_formulas == formulas

        assert attrpl1.error_in_ppm[17] == approx(-0.008163954808084819)

    def test_attribution_lowesterror_isotopes(self, benchmark):

        attrpl2 = benchmark(
            lowest_error_attribution,
            peaklist=pl,
            formula_grid=fg1,
            lambda_parameter=0.3,
            use_isotopes=True
        )

        assert attrpl2.size == 40
        assert len(attrpl2.to_dataframe(attributed_only=True)) == 19
        chem_groups = {"O B", "O 10B"}
        assert all([peak.formula.chem_group in chem_groups
                    for peak in attrpl2 if peak.is_attributed])

        formulas = [
            'C13 H14 O7 10B1', 'C13 H14 O7 B1', 'C16 H16 O5 10B1',
            'C16 H16 O5 B1', 'C15 H16 O6 B1', 'C17 H18 O5 10B1',
            'C17 H18 O5 B1', 'C16 H18 O6 10B1', 'C16 H18 O6 B1',
            'C18 H18 O6 10B1', 'C18 H18 O6 B1', 'C19 H20 O6 10B1',
            'C19 H20 O6 B1', 'C20 H22 O6 10B1', 'C20 H22 O6 B1',
            'C21 H22 O6 10B1', 'C21 H22 O6 B1', 'C21 H24 O6 10B1',
            'C21 H24 O6 B1']
        attr_formulas = [str(p.formula) for p in attrpl2 if p.is_attributed]
        assert attr_formulas == formulas

        assert attrpl2.error_in_ppm[24] == approx(-0.007592614369489384)

# TODO: to be use for testing of attribution step
# class TestPeakAttribution:

#     def setup_method(self):
#         self.run1 = PeakListAttribution(
#             polarity=-1,
#             formula_grid=fg1,
#             lambda_parameter=0.3,
#             use_isotopes=False
#         )
#         self.run2 = PeakListAttribution(
#             polarity=-1,
#             formula_grid=fg1,
#             lambda_parameter=0.3,
#             use_isotopes=True
#         )

#     def test_peakattribution_no_isotopes(self, benchmark):

#         attr = benchmark(self.run1.run_attribution, pl)
#         assert attr.size == 40
#         assert len(attr.to_dataframe(attributed_only=True)) == 10
#         chem_groups = {"O B"}
#         assert all([peak.formula.chem_group in chem_groups
#                     for peak in attr if peak.is_attributed])

#         formulas = [
#             'C13 H14 O7 B1', 'C16 H16 O5 B1', 'C15 H16 O6 B1', 'C17 H18 O5 B1',
#             'C16 H18 O6 B1', 'C18 H18 O6 B1', 'C19 H20 O6 B1', 'C20 H22 O6 B1',
#             'C21 H22 O6 B1', 'C21 H24 O6 B1']
#         attr_formulas = [str(p.formula) for p in attr if p.is_attributed]
#         assert attr_formulas == formulas

#         assert attr.error_in_ppm[17] == approx(-0.008163954808084819)

#     def test_peakattribution_with_isotopes(self, benchmark):

#         attr = benchmark(self.run2.run_attribution, pl)
#         assert attr.size == 40
#         assert len(attr.to_dataframe(attributed_only=True)) == 19
#         chem_groups = {"O B", "O 10B"}
#         assert all([peak.formula.chem_group in chem_groups
#                     for peak in attr if peak.is_attributed])

#         formulas = [
#             'C13 H14 O7 10B1', 'C13 H14 O7 B1', 'C16 H16 O5 10B1',
#             'C16 H16 O5 B1', 'C15 H16 O6 B1', 'C17 H18 O5 10B1',
#             'C17 H18 O5 B1', 'C16 H18 O6 10B1', 'C16 H18 O6 B1',
#             'C18 H18 O6 10B1', 'C18 H18 O6 B1', 'C19 H20 O6 10B1',
#             'C19 H20 O6 B1', 'C20 H22 O6 10B1', 'C20 H22 O6 B1',
#             'C21 H22 O6 10B1', 'C21 H22 O6 B1', 'C21 H24 O6 10B1',
#             'C21 H24 O6 B1']
#         attr_formulas = [str(p.formula) for p in attr if p.is_attributed]
#         assert attr_formulas == formulas
#         assert attr.error_in_ppm[24] == approx(-0.007592614369489384)


class TestIsotopicAttribution:

    def test_attribution_isotopicpattern(self, benchmark):

        apl = benchmark(
            isotopic_pattern_attribution,
            peaklist=pl2,
            formula_grid=fg1,
            lambda_parameter=0.3,
            use_SN=False,  # no SN in data
            use_isotopes=True,
            isotopic_pattern=(5, 10)
        )

        assert len(apl) == len(pl2)
        assert len(apl.get_attributed()) == 18
        df_attr = apl.get_attributed().to_dataframe(full_data=True)
        counting = df_attr.groupby(by='attribution_method').count().mz.values
        assert all(counting == [9, 9])
        formulas = [
            'C13 H14 O7 10B1', 'C13 H14 O7 B1', 'C16 H16 O5 10B1',
            'C16 H16 O5 B1', 'C17 H18 O5 10B1',
            'C17 H18 O5 B1', 'C16 H18 O6 10B1', 'C16 H18 O6 B1',
            'C18 H18 O6 10B1', 'C18 H18 O6 B1', 'C19 H20 O6 10B1',
            'C19 H20 O6 B1', 'C20 H22 O6 10B1', 'C20 H22 O6 B1',
            'C21 H22 O6 10B1', 'C21 H22 O6 B1', 'C21 H24 O6 10B1',
            'C21 H24 O6 B1']
        attr_formulas = [str(p.formula) for p in apl if p.is_attributed]
        assert attr_formulas == formulas
        assert set(df_attr.chem_group) == {'O 10B', 'O B'}
        assert apl.error_in_ppm[24] == approx(-0.007592614369489384)
