#!/usr/bin/env python
# coding: utf-8

from pytest import approx, raises
from pathlib import Path
import pandas as pd

from pyc2mc.io.peaklist import read_predator
from pyc2mc.core.calibration import CalibList
from pyc2mc.time_dependent.td_data import TimeDependentData
from pyc2mc.time_dependent.recalibration import TimeDependentCalibration

# directory with test files
test_dir = Path(__file__).parent / Path("../test_files")

# Calibration List for Standard Calibration
mz_list = [
    301.108147, 315.123797, 315.160183, 329.139447, 329.175833,
    343.155097, 343.191483, 357.207133
]
mz_exp = [
    301.10918397, 315.12489548, 315.16128265, 329.1406057,
    329.17699192, 343.15631469, 343.19270318, 357.20840525
]
df = pd.DataFrame({'mz': mz_list, 'mz_exp': mz_exp})

# Non Calibrated Peak List
pl = read_predator(
    test_dir / 'Biofuel_withB-containingSpecies_non_calibrated.csv')
pl = pl.delimit(mz_bounds=(300, 360))


class TestTDRecalibration:

    def setup_method(self):

        # Calibration List for the 2D Test
        mz_list = [
            761.789780, 787.805430, 415.429828, 539.555029, 471.492428,
            457.476778, 705.727179, 693.727179, 511.523729, 581.601979,
            593.601979, 609.633279, 679.711529, 623.648929, 649.664579
        ]
        mz_exp = [
            761.79063, 787.80627, 415.42953, 539.55508, 471.49207, 457.47629,
            705.72793, 693.72774, 511.52375, 581.60177, 593.60196, 609.63354,
            679.71209, 623.64992, 649.66569
        ]

        self.cl_EVA = CalibList(mz_theo=mz_list, mz_exp=mz_exp, polarity=1)

        self.td_data = TimeDependentData.from_directory(
            directory_path=test_dir / 'EVA_Cross', verbose=False)
        self.td_data = self.td_data[60:65]
        self.td_data = self.td_data.delimit(
            mz_bounds=(400, 800))

    def test_td_recalibration_segments(self):

        with raises(ValueError, 
                    match=r'^The index of the first scan cannot be negative'):
            # value negative
            TimeDependentCalibration(
                td_data=self.td_data,
                calibration_lists=[self.cl_EVA] * 2,
                verbose=False,
                segments=[-3, 2, 4],
            )

        with raises(ValueError,
                    match=r"^Segments definition out of scan bounds"):
            # scan limits out of bounds
            TimeDependentCalibration(
                td_data=self.td_data,
                calibration_lists=[self.cl_EVA] * 3,
                verbose=False,
                segments=[0, 2, 5, 8],
            )

        with raises(ValueError,
                    match=r"^Scan boundaries are not in ascending order"):
            # non ordered
            TimeDependentCalibration(
                td_data=self.td_data,
                calibration_lists=[self.cl_EVA] * 2,
                verbose=False,
                segments=[2, 0, 3],
            )

        with raises(ValueError,
                    match=r"^Segments must be provided but segments"):
            # segments are not provided
            TimeDependentCalibration(
                td_data=self.td_data,
                calibration_lists=[self.cl_EVA] * 2,
                verbose=False,
            )

        with raises(ValueError,
                    match=r"^The length of segments is not consistent with"):
            # length mismatch
            TimeDependentCalibration(
                td_data=self.td_data,
                calibration_lists=[self.cl_EVA] * 2,
                verbose=False,
                segments=[0, 2, 3, 5],
            )

        with raises(ValueError,
                    match=r"^The number of segments and calibration lists are"):
            # length mismatch
            TimeDependentCalibration(
                td_data=self.td_data,
                calibration_lists=[self.cl_EVA] * 3,
                verbose=False,
                segments=[1, 3, 4],
            )

        td_recal = TimeDependentCalibration(
            td_data=self.td_data,
            calibration_lists=[self.cl_EVA] * 5,
            verbose=False,
        )
        assert td_recal._segments == [0, 1, 2, 3, 4]
        assert td_recal._calib_idx == [0, 1, 2, 3, 4]

        td_recal = TimeDependentCalibration(
            td_data=self.td_data,
            calibration_lists=self.cl_EVA,
            verbose=False,
        )
        assert td_recal._segments == [0, 4]
        assert td_recal._calib_idx == [0, 0, 0, 0, 0]

        td_recal = TimeDependentCalibration(
            td_data=self.td_data,
            calibration_lists=[self.cl_EVA],
            verbose=False,
        )
        assert td_recal._segments == [0, 4]
        assert td_recal._calib_idx == [0, 0, 0, 0, 0]

        td_recal = TimeDependentCalibration(
            td_data=self.td_data,
            calibration_lists=[self.cl_EVA] * 2,
            verbose=False,
            segments=[0, 3, 4]
        )
        assert td_recal._segments == [0, 3, 4]
        assert td_recal._calib_idx == [0, 0, 0, 1, 1]

        td_recal = TimeDependentCalibration(
            td_data=self.td_data,
            calibration_lists=[self.cl_EVA] * 3,
            verbose=False,
            segments=[2, 3, 4]
        )
        assert td_recal._calib_idx == [0, 1, 2]
        td_data = td_recal.get_recalibrated_td_data()
        assert len(td_data) == 3
        assert td_data.t0 == approx(62.0)
        assert list(td_data.scan_ids) == [62, 63, 64]

    def test_td_recalibration(self):

        td_recal = TimeDependentCalibration(
            td_data=self.td_data,
            calibration_lists=self.cl_EVA,
            method="standard",
            lambda_parameter=3.0,
            searching_mode="most_abundant_corrected",
            add_isotopes=False,
            filter_outliers=True,
            recal_kws=dict(
                model="poly", deg=2,
                fit_intercept=True,
            ),
            verbose=False,
        )

        assert td_recal.recalibrations[1].n_data_points == 15
        assert td_recal.recalibrations[1].RMSE_ppm == approx(0.56810206488)
        assert td_recal.recalibrations[1].mean_error_ppm == \
            approx(0.00115167, abs=1e-7)
        assert td_recal.recalibrations[1].optimized_parameters['A'] == \
            approx(4.6678417e-09, abs=1e-9)
        assert td_recal.recalibrations[1].optimized_parameters['B'] == \
            approx(0.999989856)
        assert td_recal.recalibrations[1].optimized_parameters['C'] == \
            approx(0.004172829)

    def test_bench_standard_td_recalibration(self, benchmark):
        benchmark(
            TimeDependentCalibration,      
            td_data=self.td_data,
            calibration_lists=self.cl_EVA,
            method="standard",
            lambda_parameter=3.0,
            searching_mode="most_abundant_corrected",
            add_isotopes=False,
            filter_outliers=True,
            recal_kws=dict(
                model="poly", deg=2,
                fit_intercept=True,
            ),
            verbose=False,
        )
