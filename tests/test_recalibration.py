#!/usr/bin/env python
# coding: utf-8

from pytest import approx, raises
from pathlib import Path
import pandas as pd
import numpy as np

from pyc2mc.processing.recalibration import Recalibration, WalkingCalibration
from pyc2mc.io.peaklist import read_predator
from pyc2mc.core.calibration import CalibList

# directory with test files
test_dir = Path(__file__).parent / Path("../test_files")

# Calibration List for Standard Calibration
mz_list = [
    301.108147, 315.123797, 315.160183, 329.139447, 329.175833,
    343.155097, 343.191483, 357.207133
]
mz_exp = [
    301.10918397, 315.12489548, 315.16128265, 329.1406057,
    329.17699192, 343.15631469, 343.19270318, 357.20840525
]
df = pd.DataFrame({'mz': mz_list, 'mz_exp': mz_exp})

# Non Calibrated Peak List
pl = read_predator(
    test_dir / 'Biofuel_withB-containingSpecies_non_calibrated.csv')
pl = pl.delimit(mz_bounds=(300, 360))


class TestStandardCalibration:

    def setup_method(self):
        self.cl0 = CalibList(mz_theo=mz_list)

        self.cl = CalibList(
            mz_theo=mz_list,
            mz_exp=mz_exp,
            polarity=-1,
        )

    def test_caliblist_checklist(self):
        with raises(TypeError, match=r"the calibration_list argument must be"):
            Recalibration(calibration_list=mz_exp)

        with raises(ValueError, match=r"^The experimental mz is not available"):
            # exp mz is not available
            Recalibration(calibration_list=self.cl0)

        with raises(ValueError, match=r"^Frequency is not available"):
            Recalibration(calibration_list=self.cl, model="ledford")
        
        with raises(ValueError, match=r"^Frequency is not available"):
            Recalibration(calibration_list=self.cl, model="masselon")

        with raises(RuntimeError, match=r"^A regression with"):
            Recalibration(calibration_list=self.cl[:2])

    def test_quadratic(self):
        recal = Recalibration(
            calibration_list=self.cl,
            model="PoLy", deg=2,  # quadratic model
            fit_intercept=True,  # A*x**2 + B*x + C
            verbose=False
        )
        print(recal.optimized_parameters)

        assert recal.mean_error_ppm == approx(0.0, abs=1e-5)
        assert recal.RMSE_ppm == approx(0.00371462, abs=1e-5)
                                                     
        assert recal.n_data_points == 8

        assert recal.optimized_parameters['A'] == \
            approx(5.6146562e-09, abs=1e-9)
        assert recal.optimized_parameters['B'] == \
            approx(0.9999920403)
        assert recal.optimized_parameters['C'] == \
            approx(0.000884, abs=1e-6)

    def test_bench_recal_quadratic(self, benchmark):
        benchmark(
            Recalibration, calibration_list=self.cl, model="poly", deg=2, 
            fit_intercept=True, verbose=False)


class TestWalkingCalibration:

    def setup_method(self):
        # let's expand this
        self.cl = CalibList(mz_list, mz_exp=mz_exp, polarity=-1)

    def test_walking_recalibration(self, benchmark):

        # extend calibration list with isotopes
        cl = self.cl.add_isotopes(
            peaklist=pl,
            lambda_parameter=10.0,
            searching_mode="most_abundant_corrected",
        )
        assert len(cl) == 22
        assert len(np.nonzero(cl.is_isotopic)[0]) == 14

        calibration = WalkingCalibration(
            calibration_list=cl,
            model="polynomial", deg=2,  # quadratic
            fit_intercept=False,
            segment_size=20.0,  # in Da
            verbose=True
        )

        assert calibration._n_segments == len(calibration)

        # look into one calibration segment
        assert calibration[0].mean_error_ppm == approx(-4.27165953e-07, abs=1e-8)
        assert calibration[0].RMSE_ppm == approx(0.01797155848)
        # assert calibration[0].RMSE_ppm == approx(0.01694372781)
        assert calibration[0].n_data_points == 10
        assert calibration[0].optimized_parameters['A'] == \
            approx(-3.2767926e-09, abs=1e-9)
        assert calibration[0].optimized_parameters['B'] == \
            approx(0.9999975580684)

        # benchmark walking
        benchmark(
            WalkingCalibration,
            calibration_list=cl,
            model="poly", deg=2,
            fit_intercept=False,
            segment_size=20.0,
            verbose=False
        )
