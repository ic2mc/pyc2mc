#!/usr/bin/env python
# coding: utf-8

from pytest import approx
from pathlib import Path

# import sys
# sys.path.insert(0, os.path.join(os.path.dirname(__file__), "../src/"))

from pyc2mc.processing.roi_detection import FeatureDetection, KmeansDetection
from pyc2mc.time_dependent.td_data import TimeDependentData

# directory with test files
test_dir = Path(__file__).parent / Path("../test_files")

data = TimeDependentData.from_directory(
    directory_path=test_dir / 'EVA_Cross/')
data = data.delimit(mz_bounds=(250, 450))


class TestFeatureDetection:

    def setup_method(self):

        self.fd = FeatureDetection(
            raw_data=data
        )
        self.fd2 = FeatureDetection(
            raw_data=data
        )

    def test_scan_missing(self):

        data_min = data.delimit(mz_bounds=(342, 343))
        assert FeatureDetection(raw_data=data_min)

    def test_featuredetection(self):

        results = self.fd.run(
            lambda_parameter=3,
            min_centroids=2, 
            continuity_min=1
        )

        assert len(results) == 1630
        assert len(results[1229]) == 23
        assert results._scans[1229] == list(range(56, 79))
        assert results.get_ave_peaklist().mz[1229] == approx(400.40231)
    
    def test_featuredetection_groupbynominal_false(self):

        results = self.fd2.run(
            lambda_parameter=3,
            min_centroids=2,
            continuity_min=1,
            groupby_integer_part=False
        )
        assert len(results) == 1630
        assert len(results[1229]) == 23
        assert results._scans[1229] == list(range(56, 79))
        assert results.get_ave_peaklist().mz[1229] == approx(400.40231)

    # TODO: move to test_roi    
    # def test_noise_cleaning_by_range(self):

    #     self.fd.noise_cleaning_range(
    #         limits=(100,111),
    #         lambda_parameter=3,
    #         min_centroids=10,
    #         continuity_min=2
    #         )
        
    #     assert len(self.fd) == 1442
    #     assert self.fd.regions_of_interest[1000] == [388.4022, 388.40218, 388.40226]


class TestKmeansDetection:

    def setup_method(self):

        self.km = KmeansDetection(
            raw_data=data,
            lambda_parameter=3
        )
    
    def test_scan_missing_kmeans(self):
        data_min = data.delimit(mz_bounds=(342, 343))
        assert KmeansDetection(raw_data=data_min)
    
    def test_kmeansdetection(self):
        
        results = self.km.run(
            min_centroids=2,
            continuity_min=1,
            clustering_model='ckmeans'
        )

        assert len(results) == 1626
        assert len(results[1225]) == 23
        assert results._scans[1225] == list(range(56, 79))
        assert results.get_ave_peaklist().mz[1226] == approx(400.40231)
