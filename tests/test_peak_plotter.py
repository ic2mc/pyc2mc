#!/usr/bin/env python
# coding: utf-8

from pathlib import Path
from pytest import raises, warns
import matplotlib.pyplot as plt

# import sys
# sys.path.insert(0, os.path.join(os.path.dirname(__file__), "../src/pyc2mc"))

from pyc2mc.io.peaklist import read_peaklist
from pyc2mc.core.peak import AttributedPeakList

# directory with test files
test_dir = Path(__file__).parent / Path("../test_files")


class TestRawDataPlots:

    def setup_method(self):
        self.pl = read_peaklist(test_dir / "PyrPlas_datas.ascii")

    def test_plot_spectrum(self):

        fig = plt.figure()
        self.pl.plot(fig=fig)

        assert len(fig.axes) == 1

        ax = fig.axes[0]
        assert ax.get_ylabel() == "Intensity"
        assert ax.get_xlabel() == "m/z"
        assert ax.get_title() == "PyrPlas_datas.ascii"
        assert len(ax.lines) == 2  # stem plot

        fig = plt.figure()
        self.pl.plot.plot_spectrum(fig=fig, xlabel="toto", ylabel="tata",
                                   title="titi", peak_density=True)
        assert len(fig.axes) == 2
        assert fig.axes[0].get_ylabel() == "tata"
        assert fig.axes[0].get_xlabel() == "toto"
        assert fig.axes[0].get_title() == "titi"
        assert fig.axes[1].get_ylabel() == "Density of Peaks"
        assert len(fig.axes[1].lines) == 2

        # just check it works without error
        self.pl.plot.plot_spectrum(peak_density=True, linefmt="C2:",
                                   density_kws=dict(npts=500, sigma=.02))
        
        # just check it works without error
        self.pl.plot.plot_spectrum(rug=True)

        plt.close("all")

    def test_plot_kmd(self):

        with raises(TypeError):
            # missing building block
            self.pl.plot.plot_kendrick_mass_defect()

        with warns(RuntimeWarning):
            with raises(ValueError):
                # wrong formula
                self.pl.plot.plot_kendrick_mass_defect("CH2") 

        fig = plt.figure()
        self.pl.plot.plot_kendrick_mass_defect("C H2", fig=fig)
        assert len(fig.axes) == 2
        assert fig.axes[0].get_ylabel() == "Kendrick mass defect"
        assert fig.axes[0].get_xlabel() == "Kendrick integer mass"
        assert fig.axes[1].get_ylabel() == "Intensity"
        assert fig.axes[1].get_ylim()[1] == 100.0

        self.pl.plot.plot_kendrick_mass_defect("C H2", fig=fig, normalize="")
        assert fig.axes[1].get_ylim()[1] == 2884921600.0

        # check it works
        self.pl.plot.plot_kendrick_mass_defect("C H2", scale_intensity=True,
                                               marker="s", markersize=90,
                                               cmap="Reds", max_intensity=3e8)

        plt.close("all")

    def test_plot_SN_stats(self):

        with raises(ValueError, match="S/N is not available in the data."):
            pl = read_peaklist(test_dir / "asphaltenes.pks")
            # missing S/N
            pl.plot.plot_SN_statistics()

        fig = plt.figure()
        self.pl.plot.plot_SN_statistics(fig=fig)
        assert len(fig.axes) == 2
        assert fig.axes[0].get_ylabel() == "# peaks weighted by intensity"
        assert fig.axes[1].get_ylabel() == "Cumulative percentage of peaks"
        assert fig.axes[0].get_xscale() == "log"

        fig = plt.figure()
        self.pl.plot.plot_SN_statistics(fig=fig, logscale=False,
                                        weight_by_intensity=False)
        assert len(fig.axes) == 2
        assert fig.axes[0].get_ylabel() == "# peaks"
        assert fig.axes[1].get_ylabel() == "Cumulative percentage of peaks"
        assert fig.axes[0].get_xscale() == "linear"

        plt.close("all")


class TestAttributedPlots:

    def setup_method(self):
        self.apl = AttributedPeakList.from_file(
            test_dir / "attributed_scan_0060.csv",
            polarity=-1
        )

    def test_plot_spectrum(self):
        fig = plt.figure()
        self.apl.plot(fig=fig)

        assert len(fig.axes) == 1
        assert len(fig.axes[0].lines) == 4  # 2 hits + 2 nohits as stem

        ax = fig.axes[0]
        assert ax.get_ylabel() == "Intensity"
        assert ax.get_xlabel() == "m/z"
        assert ax.get_title() == "attributed_scan_0060.csv"

        fig = plt.figure()
        self.apl.plot.plot_spectrum(fig=fig, xlabel="toto", ylabel="tata",
                                    title="titi", peak_density=True)
        assert len(fig.axes) == 2
        assert fig.axes[0].get_ylabel() == "tata"
        assert fig.axes[0].get_xlabel() == "toto"
        assert fig.axes[0].get_title() == "titi"
        assert fig.axes[1].get_ylabel() == "Density of Peaks"
        assert len(fig.axes[0].lines) == 4  # stem 2 + 2
        assert len(fig.axes[1].lines) == 4  # 2 + shade (2)

        # just check it works without error
        self.apl.plot.plot_spectrum(rug=True, nohits_fmt="C7:", 
                                    hits_fmt="C1-")
        
        # just check it works without error
        self.apl.plot.plot_spectrum(rug=True, hide_nohit=True)

        plt.close("all")

    def test_plot_classes(self):

        with raises(ValueError):
            # data empty
            self.apl.plot.plot_classes(cum_threshold=.9)

        # check it works
        self.apl.plot.plot_classes(monoisotopic=False)
        self.apl.plot.plot_classes(monoisotopic=True, orientation="H")
        self.apl.plot.plot_classes(monoisotopic=True, orientation="h")

        plt.close("all")

    def test_plot_error(self):

        with raises(ValueError):
            # chem group does not exist
            self.apl.plot.plot_error(chem_group="Z")

        with raises(ValueError):
            # chem class does not exist
            self.apl.plot.plot_error(chem_class=["O6", "Z2"])

        fig = plt.figure()
        self.apl.plot.plot_error(fig=fig)
        assert len(fig.axes) == 2
        assert fig.axes[0].get_ylabel() == "error (ppm)"
        assert fig.axes[0].get_xlabel() == "m/z (Da)"
        assert fig.axes[1].get_xlabel() == "distribution"

        fig = plt.figure()
        self.apl.plot.plot_error(fig=fig, distribution=False)
        assert len(fig.axes) == 1

        fig = plt.figure()
        self.apl.plot.plot_error(fig=fig, distribution=True, 
                                 chem_class=["O6", "all"],
                                 chem_group="O S")
        assert len(fig.axes[1].lines) == 3 + 1  # with horizontal line
        assert len(fig.axes[0].collections) == 3

        fig = plt.figure()
        self.apl.plot.plot_error(fig=fig, chem_class=["O6", "all"],
                                 chem_group="O B S")
        assert len(fig.axes[1].lines) == 3 + 1 - 1  # +1 horizontal
                                                    # -1 O B S not enough data
        plt.close("all")

    def test_plot_dbe(self):
        with raises(ValueError):
            # chem group does not exist
            self.apl.plot.plot_dbe(chem_group="Z")

        with raises(ValueError):
            # chem class does not exist
            self.apl.plot.plot_dbe(chem_class="Z2")

        with raises(ValueError):
            self.apl.plot.plot_dbe(chem_class="O12 13C1", monoisotopic=True)

        with raises(ValueError):
            self.apl.plot.plot_dbe(plot_type="toto")

        fig = plt.figure()
        self.apl.plot.plot_dbe(fig=fig, chem_class="O6")
        assert len(fig.axes) == 2
        assert len(fig.axes[0].collections) == 1
        assert fig.axes[0].get_xlabel() == "C#"
        assert fig.axes[0].get_ylabel() == "DBE"

        # just check it works
        self.apl.plot.plot_dbe(chem_class="O6", plot_type="KDE", sigma=0.1,
                               resolution=.9)
        self.apl.plot.plot_dbe(chem_group="O", plot_type="points", marker="o",
                               markersize=80, scale_intensity=True)
        self.apl.plot.plot_dbe(chem_group="O", non_integer_dbe=True)
        self.apl.plot.plot_dbe(chem_group="O", integer_dbe=True)

        plt.close("all")
    
    def test_plot_attribution_stats(self):

        with raises(ValueError):
            self.apl.plot.plot_attribution_stats(xaxis="toto")

        # just check it works
        self.apl.plot.plot_attribution_stats()
        self.apl.plot.plot_attribution_stats(xaxis="intensity")
        self.apl.plot.plot_attribution_stats(xaxis="peaks")

        plt.close("all")

    def test_plot_van_krevelen(self):

        with raises(ValueError):
            self.apl.plot.plot_van_krevelen("Z / C")
        with raises(ValueError):
            self.apl.plot.plot_van_krevelen(x_ratio="Z / O")
        with raises(ValueError):
            self.apl.plot.plot_van_krevelen(y_ratio="C / Z")

        # check it works
        fig = plt.figure()
        self.apl.plot.plot_van_krevelen(
            "H / O", "O / C",
            markersize=80, max_intensity=100,
            vmin=0, vmax=60, fig=fig)

        assert len(fig.axes) == 2
        assert len(fig.axes[0].collections) == 1
        assert fig.axes[0].get_xlabel() == "H / O"
        assert fig.axes[0].get_ylabel() == "O / C"
        assert fig.axes[1].get_ylabel() == "Normalized Intensity"

        plt.close("all")

    def test_plot_classes_circular(self):

        with raises(ValueError):
            # missing groups
            self.apl.plot.plot_classes_circular(
                group_order=["O", "O S"], threshold=0.1)

        with raises(KeyError):
            # wrong group
            self.apl.plot.plot_classes_circular(
                threshold=0.1, group_order=["O", "O S", "O N SS"])
            
        fig = plt.figure()
        self.apl.plot.plot_classes_circular(threshold=.1, fig=fig)
        assert len(fig.axes) == 1

        # check it works
        self.apl.plot.plot_classes_circular(
            cum_threshold=97.5, group_order=["O", "O 13C", "O N S"],
            fontsize=8, group_colors=["C3", "#123DEF", "orange"], padding=2,
            monoisotopic=False,
        )
