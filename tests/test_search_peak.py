#!/usr/bin/env python
# coding: utf-8

from pathlib import Path
import numpy as np
from numpy.testing import assert_allclose
# from pytest import approx
import pandas as pd

from pyc2mc.io.peaklist import read_predator
from pyc2mc.core.peak_search import SearchingMode

# directory with test files
test_dir = Path(__file__).parent / Path("../test_files")

# Non Calibrated Peak List
pl = read_predator(
    test_dir / 'Biofuel_withB-containingSpecies_non_calibrated.csv')
pl = pl.delimit(mz_bounds=(300, 360))


# Calibration List for Standard Calibration
mz_theo = [
    301.108147, 315.123797, 315.160183, 329.139447, 329.175833,
    343.155097, 343.191483, 357.207133
]
mz_exp = [
    301.10918, 315.12490, 315.16128, 329.14061, 329.17699, 343.15631, 
    343.19270, 357.20841
]
df = pd.DataFrame({'mz': mz_theo, 'mz_exp': mz_exp})


class TestPeakSearch:

    def test_peak_search_closest(self):
        search_peak = SearchingMode("closest")
        idx, peaks = search_peak(mz_theo, pl, lambda_parameter=3)
        
        assert idx == [0, 1, 3, 5]
        assert [p.pid for p in peaks] == [16062, 16258, 16063, 16064]

        idx, peaks = search_peak(mz_theo, pl, lambda_parameter=5)
        mz_exp_closest = np.array([p.mz for p in peaks])
        assert_allclose(mz_exp_closest, [301.10839, 315.12407, 315.16128, 
                                         329.13969, 329.17699, 343.1552, 
                                         343.1927, 357.20841])

    def test_peak_search_most_abundant(self):
        search_peak = SearchingMode("most_abundant")
        idx, peaks = search_peak(mz_theo, pl, lambda_parameter=5)

        assert len(idx) == 8
        
        mz_exp_most = np.array([p.mz for p in peaks])
        assert_allclose(mz_exp_most, mz_exp)
   
    def test_peak_search_most_abundant_corrected(self):
        search_peak = SearchingMode("most_abundant_corrected")
        idx, peaks = search_peak(mz_theo, pl, lambda_parameter=5)

        assert len(idx) == 7
        
        mz_exp_most = np.array([p.mz for p in peaks])
        assert_allclose(mz_exp_most, mz_exp[1:])
