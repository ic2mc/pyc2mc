#!/usr/bin/env python
# coding: utf-8

from pathlib import Path
from pytest import approx, raises, warns
import numpy as np

# import sys
# sys.path.insert(0, os.path.join(os.path.dirname(__file__), "../src"))
from pyc2mc.core.formula import Formula
from pyc2mc.io.peaklist import (read_ascii, read_pks, read_predator,
                                read_peaklist)
from pyc2mc.io.nddata import read_MZxml
from pyc2mc.core.calibration import CalibList
from pyc2mc.io.caliblist import read_csv, read_ref

# directory with test files
test_dir = Path(__file__).parent / Path("../test_files")


class TestReadPeaklist:

    def test_errors_read_peaklist(self):

        with raises(FileNotFoundError):
            read_peaklist("toto.asc")

        with raises(ValueError):
            read_peaklist(test_dir / "recalibr" / "MALDI Tholin pos.ref")
        
        with raises(ValueError):
            read_peaklist(
                test_dir / "Biofuel_withB-containingSpecies_non_calibrated.csv",
                fmt="ascii")

        with raises(ValueError):
            read_peaklist(
                test_dir / "Biofuel_withB-containingSpecies_non_calibrated.csv",
                fmt="csv")

        with raises(ValueError):
            read_peaklist(
                test_dir / "Biofuel_withB-containingSpecies_non_calibrated.csv",
                fmt="txt")
            
    def test_read_peaklist(self):
        pl1 = read_peaklist(test_dir / "PyrPlas_datas.ascii")
        pl2 = read_peaklist(test_dir / "PyrPlas_datas.ascii", fmt="ascii")
        assert np.all(pl1.values == pl2.values)


class TestAsc:

    def setup_method(self):
        self.pl = read_ascii(test_dir / "HULIS/ESI_pos_pH3_HULIS_darkAged.asc")

    def test_read_asc(self):
        assert len(self.pl) == 4864
        assert self.pl.mz.dtype == np.float64

        assert self.pl.values[2, 1] == approx(3241234.0)
        assert self.pl.values[2] == approx(
            [1.1503631e+02, 3.2412340e+06, 1.0000000e-04])


class TestAscii:

    def setup_method(self):
        self.pl = read_ascii(test_dir / "PyrPlas_datas.ascii")

    def test_read_ascii(self):
        assert len(self.pl) == 1175
        assert self.pl.mz.dtype == np.float64

        assert self.pl.values[2, 1] == approx(3405183.0)
        assert self.pl.values[2] == approx(
            [3.0017466e+02, 3.4051830e+06, 1.9300000e+01])


class TestPKS:

    def test_read_pks(self):
        pl = read_pks(test_dir / "asphaltenes.pks")
        assert len(pl) == 27388
        assert pl.mz.dtype == np.float64
        assert not pl.SN_avail

        assert pl.values[2, 1] == approx(0.0743096)
        assert pl.values[2, :2] == approx([150.0036, 0.0743096])

        assert pl.peak_properties["resolving power"][3] == 2473821
        assert pl.peak_properties["frequency"][4] == approx(2153501.56215)

    def test_data_SN(self):
        pl = read_pks(test_dir / "biooil.pks")
        assert len(pl) == 14782
        assert pl.SN_avail
        assert pl.SN[2] == approx(57.548)

        assert pl.metadata.npeaks == 14782
        assert pl.metadata.peak_threshold == approx(0.073971)
        assert pl.metadata.noise_threshold == approx(0.057128)
        assert pl.metadata.baseline_SN == approx(0.017829)
        assert pl.metadata.sigma_SN == approx(0.009357)
        assert pl.metadata.min_mz == approx(180.0)
        assert pl.metadata.max_mz == approx(1200.0)

    def test_data_TD(self):
        """ Spectific case of a pks file from an LC run that includes time """
        pl = read_pks(test_dir / "LC_SRFA_scan66_dat.pks")
        assert len(pl) == 2128
        assert not pl.SN_avail
        assert pl.metadata.time == approx(310.949448)

        assert pl.metadata.npeaks == 2128
        assert pl.metadata.peak_threshold == approx(0.779432)
        assert pl.metadata.noise_threshold == approx(0.545602)
        assert pl.metadata.baseline_SN is None
        assert pl.metadata.sigma_SN is None
        assert pl.metadata.min_mz == approx(175.0)
        assert pl.metadata.max_mz == approx(1400.0)


class TestPredator_csv:

    def setup_method(self):
        self.pl = read_predator(test_dir / "low_complexity.csv")

    def test_data(self):
        assert len(self.pl) == 20236
        assert self.pl.mz.dtype == np.float64

        assert self.pl.values[2, 1] == approx(0.4055)
        assert self.pl.values[2, :2] == approx([180.00181, 0.4055])


class TestMZxml:

    def setup_method(self):
        self.peaklists = read_MZxml(test_dir / "EVA_Cross/EVA_cross.mzXML")

    def test_data(self):
        assert len(self.peaklists) == 111
        assert self.peaklists[2].intensity[1] == approx(4315908.257733068)


class TestCalibList:

    def setup_method(self):

        self.cl_data = read_ref(test_dir / 'recalibr/MALDI Tholin pos.ref')

        with warns(UserWarning):
            read_csv(test_dir / 'recalibr/2021July21NegESI_Sample1_TMHA_MeOH-Tol_1-1_Sum75_CalibrationList.csv')
        self.cl_predator = read_csv(
            test_dir / 'recalibr/2021July21NegESI_Sample1_TMHA_MeOH-Tol_1-1_Sum75_CalibrationList.csv',
            polarity=-1,
        )

        self.list = self.cl_predator.mz_theo

        self.cl_only_calibrant_mz = CalibList(self.list)

    def test_caliblist_read_ref(self):

        assert not self.cl_data._is_mz_exp_avail
        assert not self.cl_data._is_freq_avail
        assert not self.cl_data._is_intens_avail

        assert len(self.cl_data) == 127
        assert self.cl_data.polarity == 1

        assert self.cl_data[0].formula == Formula.from_string("C6 H11 N2")
        assert self.cl_data[8].formula == Formula.from_string("C5 H10 N5")
        assert self.cl_data[109].formula == Formula.from_string("C23 H26 N O2")
        assert self.cl_data[109].mz_theo == approx(348.195805)

    def test_caliblist_read_predator(self):

        assert self.cl_predator._is_freq_avail
        assert self.cl_predator._is_intens_avail
        assert not self.cl_predator._is_mz_exp_avail
        assert self.cl_predator.polarity == -1

    def test_caliblist_read_only_calibrant(self):

        assert not self.cl_only_calibrant_mz._is_mz_exp_avail
        assert not self.cl_only_calibrant_mz._is_intens_avail
        assert not self.cl_only_calibrant_mz._is_freq_avail

        self.cl_predator.calculate_exp_mz(A=323038948.68, B=-2441986309.4)

        cp = self.cl_only_calibrant_mz.pop(0)

        assert np.isnan(cp.frequency)
        assert cp.mz_theo == approx(231.029897)
        assert self.cl_predator[1].mz_theo == approx(self.cl_only_calibrant_mz[0].mz_theo)

        cp = self.cl_predator.pop(0)
        self.cl_predator.append(cp)
        self.cl_predator.sort()

        assert all(self.cl_predator.mz_theo == [
            231.029897, 231.066282, 245.045547, 245.081932, 259.061197,
            259.097583, 273.076847, 273.113233, 287.092497, 287.128883,
            301.108147, 315.123797, 315.160183, 329.139447, 329.175833,
            343.155097, 343.191483, 357.207133, 371.186398, 371.222783,
            385.202048, 385.238433, 399.217698, 399.254083, 409.108147,
            413.233348, 413.269733, 421.056506, 423.123797, 427.248998,
            427.285383, 435.072156, 437.139447, 441.264648, 441.301033,
            449.087806, 451.155097, 455.280298, 455.316683, 463.103456,
            465.170747, 469.295948, 469.332334, 477.119106, 479.186398,
            483.311598, 483.347984, 491.134756, 497.327248, 505.150406,
            507.217698, 511.342898, 519.166056, 521.233348, 533.181706,
            535.248998, 549.264648, 561.213006, 563.280298, 575.228656,
            589.244306, 603.259956, 617.130064, 617.275606, 631.145715,
            631.145715, 631.291257, 645.161365, 645.161365, 645.306907,
            649.156279, 659.177015, 659.177015, 663.171929, 673.192665,
            673.192665, 677.187579, 687.208315, 687.208315, 691.203229,
            701.223965, 701.223965, 705.218879, 719.23453, 729.255265,
            729.255265, 743.270915, 743.270915, 747.26583, 757.286565,
            757.286565, 761.28148, 771.302215, 771.302215, 775.29713,
            785.317865, 785.317865, 789.31278, 799.333515, 803.32843,
            813.349165])
