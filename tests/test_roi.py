#!/usr/bin/env python
# coding: utf-8

from pathlib import Path
from pytest import approx, raises
import numpy as np
import matplotlib.pyplot as plt

# import sys
# sys.path.insert(0, os.path.join(os.path.dirname(__file__), "../src/pyc2mc"))

from pyc2mc.core.roi import ROI
from pyc2mc.time_dependent.td_data import TimeDependentData
from pyc2mc.processing.roi_detection import FeatureDetection

test_dir = Path(__file__).parent / Path("../test_files")


class TestROI:

    def setup_method(self):
        self.scans = np.array([ 
            0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16,
            17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33,
            34, 35, 37, 39, 42, 43])
        self.mz = np.array([
            501.1246, 501.12462, 501.12462, 501.12465, 501.12468, 501.12471,
            501.12471, 501.12469, 501.12475, 501.12475, 501.12476, 501.12473,
            501.12478, 501.12478, 501.12478, 501.12479, 501.12478, 501.12479,
            501.12479, 501.12477, 501.12478, 501.12476, 501.12478, 501.12477,
            501.12476, 501.12474, 501.12477, 501.12475, 501.12476, 501.12476,
            501.12475, 501.12476, 501.1247, 501.1247, 501.12473, 501.12473,
            501.12471, 501.12469, 501.12468, 501.12463])
        self.intensity = np.array([
            0.8550635, 0.8462293, 1.54184, 1.5628752, 2.2354732, 2.2033894,
            2.6995118, 3.2934375, 2.5939088, 3.443361, 3.9520974, 3.402684,
            3.8820651, 3.5656426, 3.5292141, 3.1545067, 3.312861, 3.4573307,
            3.4042976, 2.6471071, 2.8416235, 2.3764641, 2.7572005, 2.3711882,
            2.0790422, 2.2097352, 1.9864066, 2.5229814, 1.9697535, 1.9694961,
            1.1811726, 1.7282962, 1.0657517, 1.0498886, 1.3613778, 1.1954117,
            1.336962, 0.7520771, 0.7471647, 0.772703])
        self.roi = ROI(scans=self.scans, mz=self.mz, intensity=self.intensity)

    def test_roi(self):

        assert not self.roi.is_continuous
        assert self.roi.total_intensity == approx(89.8575927)
        assert self.roi.largest_hole == 2
        assert self.roi.isin_scan_interval(10, 20, 5)
        assert not self.roi.isin_scan_interval(10, 20, 15)
        assert self.roi.get_interval_overlap(0, 30) == approx(1)
        assert self.roi.get_interval_overlap(30, 50) == approx(0.4761904)

        assert self.roi.get_peak_index(11) == 11

        with raises(IndexError, match="Scan 48 not in the ROI."):
            self.roi.get_peak_index(48)


class TestROIList:

    def setup_method(self):
        raw_data = TimeDependentData.from_MZxml(
            test_dir / "EVA_Cross" / "EVA_cross.mzXML")
        raw_data = raw_data[2:30]
        raw_data = raw_data.delimit(mz_bounds=(600, 1100))
        fd = FeatureDetection(
            raw_data=raw_data,
            min_centroids=6,
            lambda_parameter=1,
            continuity_min=6,
            groupby_integer_part=True
        )
        self.roi_list = fd.run()
        
    def test_plot_roi(self):
        self.roi_list.plot(mz_bounds=(700, 705))
        plt.close("all")
