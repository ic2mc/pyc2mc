#!/usr/bin/env python
# coding: utf-8

from pytest import approx, warns, raises

# import sys
# sys.path.insert(0, os.path.join(os.path.dirname(__file__), "../src/pyc2mc"))

from pyc2mc.core.formula import (Formula, Composition, get_formula,
                                 sort_elements, KendrickBuildingBlock)
# from mass_data import Isotope, IsotopicData

formula = {'C': 12, 'H': 12, 'O': 1, 'Na': 1, '13C': 1}
elm_formula = {'C': 13, 'H': 12, 'O': 1, 'Na': 1}
formula_str = 'C12 H12 O1 Na1 13C1'
polarity = -1
mz = 208.08248770343098


class TestModuleFormula:

    def test_sort_elements_formula(self, benchmark):

        results = ["C", "H", "O", "N", "B", "P", "S", "F", "Cl", "Br",
                   "I", "Be", "Ca", "Cs", "Na", "Ru", "Se", "V", "W"]

        species = ['Br', 'W', 'Na', 'I', 'P', 'Cl', 'S', 'Ru', 'Cs',
                   'Be', 'O', 'Se', 'H', 'B', 'V', 'Ca', 'C', 'F', 'N']

        sorted_els = benchmark(sort_elements, species)
        assert sorted_els == results


class TestComposition:

    def setup_method(self):

        self.composition = Composition(formula)
        self.composition_str = Composition.from_string(formula_str)

    def test_values(self):

        assert self.composition_str == self.composition
        assert self.composition._composition == formula
        assert isinstance(self.composition._composition, dict)
        assert self.composition.elemental_composition == elm_formula
        assert self.composition.chem_class == 'O1 Na1 13C1'
        assert self.composition.chem_class_mono == 'O1 Na1'
        assert self.composition.chem_group == 'O Na 13C'
        assert self.composition.chem_group_mono == 'O Na'
        assert self.composition.dbe_value == 6.5
        assert self.composition.is_isotopic
        assert str(self.composition) == formula_str

    def test_isotopic_composition(self):
        # this composition is already isotopic
        c = Composition.from_string("C5 H11 O2 N1 78Se")
        assert c.elemental_composition == {'C': 5, 'H': 11, 'O': 2, 'N': 1, 
                                           'Se': 1}
        
        assert c.isotopes_per_element == {'Se': ['78Se']}
        assert self.composition.isotopes_per_element == {'C': ['13C']}
        c = Composition({'C': 12, 'H': 12, '18O': 1, '13C': 1})
        assert c.isotopes_per_element == {"C": ['13C'], "O": ["18O"]}

        c = Composition({'C': 12, 'H': 12, '13C': 2, "78Se": 1, "80Se": 1,
                         "Se": 1})
        assert c.elemental_composition == {'C': 14, 'H': 12, 'Se': 3}
        assert c.isotopes_per_element == {'C': ['13C'], 'Se': ['78Se', '80Se']}

    def test_insert_isotopes(self):

        c_iso = self.composition.insert_isotope(8, 18, 1)
        assert c_iso.elemental_composition == self.composition.elemental_composition
        assert c_iso.isotopes == {'13C', '18O'}
        assert c_iso.is_isotopic

        with raises(ValueError):
            # not enough oxygen in the initial composition
            self.composition.insert_isotope(8, 18, 2)

        with raises(ValueError):
            # this is "not" an isotope (it is the most abundant one)
            self.composition.insert_isotope(8, 16, 1)

        c = Composition.from_string("C5 H11 O2 N1 78Se")
        with raises(ValueError):
            # the composition does not contains the most abundant isotope
            # of Se to be substituted by another one.
            c.insert_isotope(34, 77, 1)

    def test_substitute_element(self):

        c = Composition({'C': 12, 'H': 12, '13C': 2, "78Se": 1, "80Se": 1,
                         "Se": 1})
        csub = c.substitute_elements({"C": -1, "13C": -1})
        assert csub["13C"] == 1
        assert csub["C"] == 11
        assert csub.elemental_composition["C"] == 12
        
        csub = c.substitute_elements({"80Se": -1, "78Se": -1, "Se": -1})
        with raises(KeyError):
            csub["Se"]

        c = Composition.from_string("C5 H11 O2")
        csub = c.substitute_elements({"C": 1, "H": 2})
        assert csub["C"] == 6
        assert csub["H"] == 13

    def test_make_composition_instance(self, benchmark):
        benchmark(lambda d: Composition(d), formula)

    def test_composition_from_string(self, benchmark):
        benchmark(Composition.from_string, formula_str)


class TestFormula:

    def setup_method(self):

        self.formula = Formula(formula)
        self.formula_str = Formula.from_string(formula_str)
        self.long_formula = Formula.from_string(
            "C16 H18 18O1 Mn2 Ni3 14C1 S6 13C2 N2 Na1 O6 B3 10B1 V1 ")
        
    def test_exact_masses(self):

        with raises(ValueError):
            Formula.from_string("C4 H5 O1", exact_masses={"C": 12, "H": 1})

    def test_chem_class_group(self):
        assert self.formula.chem_class == 'O1 Na1 13C1'
        assert self.formula.chem_class_mono == 'O1 Na1'
        assert self.formula.chem_group == 'O Na 13C'
        assert self.formula.chem_group_mono == 'O Na'

        assert self.long_formula.chem_group == \
            'O N B S Mn Na Ni V 10B 13C 14C 18O'
        assert self.long_formula.chem_group_mono == \
            'O N B S Mn Na Ni V'
        assert self.long_formula.chem_class == \
            'O6 N2 B3 S6 Mn2 Na1 Ni3 V1 10B1 13C2 14C1 18O1'
        assert self.long_formula.chem_class_mono == \
            'O7 N2 B4 S6 Mn2 Na1 Ni3 V1'

    def test_values(self):

        assert self.formula_str == self.formula
        assert self.formula._composition == formula
        assert isinstance(self.formula._composition, dict)
        assert self.formula.elemental_composition == elm_formula
        assert self.formula.dbe_value == 6.5
        assert self.formula.is_isotopic
        assert str(self.formula) == formula_str
        assert self.formula.isotopes == {'13C'}
        assert self.long_formula.isotopes == {'10B', '13C', '14C', '18O'}

        assert self.formula.error_mDa(polarity=polarity, mz=mz + 1e-4) == \
            approx(-1e-1)
        assert self.formula.error_ppm(polarity=polarity, mz=mz + 1e-4) == \
            approx(-1e-4 / mz * 1e6)
        CH2 = Formula.from_string("C1 H2")
        km = self.formula.kendrick_mass(polarity=polarity, building_block=CH2)
        assert km == approx(207.85014093873872)
        kmd = self.formula.kendrick_mass_defect(polarity=polarity, 
                                                building_block=14.01565)
        assert kmd == approx(0.14985906126128157)
        assert self.formula.get_ion_exact_mass(polarity=polarity) == \
            approx(mz)
        assert self.formula.nominal_mass == 207
        assert self.formula.integer_mass == 208

    def test_make_formula_instance(self, benchmark):
        benchmark(lambda d: Formula(d), formula)

    def test_formula_from_string(self, benchmark):
        benchmark(Formula.from_string, formula_str)


class TestGetFormula:

    def setup_method(self):
        self.bad_formulas = ["nan", "#nan", "#NaN", "non", "na",
                             "NaN", "false", "#Na", "#na", "plop", "faux"]

    def test_get_formula_function(self, benchmark):
        
        f = benchmark(get_formula, formula)
        assert str(f) == "C12 H12 O1 Na1 13C1"

        for s in self.bad_formulas:
            with warns(RuntimeWarning):
                get_formula(s)

        assert get_formula("") is None
        assert get_formula(None) is None


class TestKendrickBuildingBlock:

    def test_kendrick_building_block(self):

        formula = Formula.from_string("C1 H2")

        kbb = KendrickBuildingBlock(formula)

        assert formula == kbb.formula
        assert formula.exact_mass == approx(kbb.exact_mass)
        assert kbb.formula_avail

        with raises(ValueError):
            with warns(RuntimeWarning):
                KendrickBuildingBlock("toto")

        kbb = KendrickBuildingBlock(14.002)
        assert kbb.formula_avail is False

        with warns(RuntimeWarning):
            _ = kbb.formula

        kbb = KendrickBuildingBlock(formula)
        assert kbb.formula == formula


class Test_IsotopicPattern:

    def setup_method(self):

        self.formula = get_formula('C12 H12 O6 B1')
        self.isopatt = self.formula.get_isotopic_pattern(add_exact_mass=True)

    def test_isotopic_pattern(self):

        df = self.isopatt.to_dataframe()
        assert len(df) == 11
        assert df['prob.'][0] == approx(0.68643413939)
        assert df['prob.'][1] == approx(0.00846922441)
        assert all(df.isotopes.values == [
            '', '18O1', '17O1', '13C1', '13C1 18O1', '13C2', '10B1',
            '10B1 18O1', '10B1 13C1', '10B1 13C2', '2H1'])
        # assert all(df.isotopes.values == [
        #     '', '¹⁸O₁', '¹⁷O₁', '¹³C₁', '¹³C₁¹⁸O₁', '¹³C₂', '¹⁰B₁',
        #     '¹⁰B₁¹⁸O₁', '¹⁰B₁¹³C₁', '¹⁰B₁¹³C₂', '²H₁'])
        
        mass_values = [
            263.0726934687982,
            265.0769384620941,
            264.0769106057334,
            264.0760483038705,
            266.0802932971664,
            265.0794031389428,
            262.0763250583982,
            264.0805700516941,
            263.0796798934705,
            264.0830347285427,
            264.0789702146804
            ]
        for i, row in df.iterrows():
            assert row.pattern_exact_mass == approx(mass_values[i])

    def test_get_isotopic_pattern(self, benchmark):
        benchmark(self.formula.get_isotopic_pattern)
