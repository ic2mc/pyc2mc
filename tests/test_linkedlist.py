import numpy as np
from pyc2mc.data_structures.linked_list import DoublyLinkedList

class TestDoublyLinkedList:

    def setup_method(self):
        self.dll = DoublyLinkedList()
        self.dll.append(10)
        self.dll.append(20)
        self.dll.append(30)
        self.dll.append(40)

    def test_append(self):
        self.dll.append(50)
        assert self.dll.to_list() == [10, 20, 30, 40, 50]
        assert self.dll.get_length() == 5

    def test_insert_at_beginning(self):
        self.dll.insert_at_beginning(5)
        assert self.dll.to_list() == [5, 10, 20, 30, 40]
        assert self.dll.get_length() == 5

    def test_insert_after(self):
        self.dll.insert_after(self.dll.head.next, 25)
        assert self.dll.to_list() == [10, 20, 25, 30, 40]
        assert self.dll.get_length() == 5

    def test_insert_before(self):
        self.dll.insert_before(self.dll.head.next, 15)
        assert self.dll.to_list() == [10, 15, 20, 30, 40]
        assert self.dll.get_length() == 5

    def test_remove(self):
        self.dll.remove(self.dll.head.next)
        assert self.dll.to_list() == [10, 30, 40]
        assert self.dll.get_length() == 3

    def test_to_numpy_array(self):
        np.testing.assert_array_equal(self.dll.to_numpy_array(), np.array([10, 20, 30, 40]))

    def test_generate_bin_centers_array(self):
        np.testing.assert_array_equal(self.dll.generate_bin_centers_array(), np.array([15.0, 35.0]))

    def test_extend(self):
        self.dll.extend([50, 60])
        assert self.dll.to_list() == [10, 20, 30, 40, 50, 60]
        assert self.dll.get_length() == 6

    def test_non_zero_intervals(self):
        dll = DoublyLinkedList([0, 1, 2, 0, 3, 4, 0, 5, 0])
        assert dll.non_zero_intervals() == [(1, 2), (4, 5), (7, 7)]
        assert dll.non_zero_intervals(min_size=2) == [(1, 2), (4, 5)]
        assert dll.non_zero_intervals(min_size=3) == []

    def test_to_list(self):
        assert self.dll.to_list() == [10, 20, 30, 40]
