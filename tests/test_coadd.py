#!/usr/bin/env python
# coding: utf-8

# import pytest
import numpy as np
from pathlib import Path

# import sys
# sys.path.insert(0, os.path.join(os.path.dirname(__file__), "../src/"))

from pyc2mc.io.ft_icr_signal import read_signal
from pyc2mc.signal_processing.coadd import FT_ICR_SignalCollection

# directory with test files
test_dir = Path(__file__).parent / Path("../test_files")


class TestCoAddition:

    def setup_method(self):
        self.signals = FT_ICR_SignalCollection.from_files(
            test_dir / "Coaddition" / "raw", extension=".dat")

    def test_coadd_signals(self):
        # coadded_signals = coadd(scan_range=[0, 9], step_size=10, directory=folder / "raw")
        coadded_signals = self.signals.coadd(step_size=1)
        print(type(coadded_signals))
        assert len(coadded_signals) == 9

        coadded_signals = self.signals.coadd()
        ref_dat = read_signal(test_dir / "Coaddition" / "predator 1-9.dat")
        assert np.all(np.isclose(ref_dat.signal, coadded_signals[0].signal))
        # warning: comparing with some signals data you have to multiply
        # by the voltage_scale
        # np.isclose(signal, coad.signal / coad.metadata.voltage_scale)
