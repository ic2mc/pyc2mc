#!/usr/bin/env python
# coding: utf-8

import pytest
from pytest import approx, raises
import pandas as pd
from pydantic import ValidationError
# import sys
# sys.path.insert(0, os.path.join(os.path.dirname(__file__), "../src/"))
from pyc2mc.core.formula_grid import FormulaGrid, FormulaGridList
from pyc2mc.controls.formula_grid import FormulaGridControls

FG_TOML = """# FormulaGrids from PyC2MC

[[formula_grid]]
elements = ["C", "H", "O"]
limits = [[0, 100], [0, 200], [0, 6]]
polarity = 1
H_C_bounds = [0.3, 2.4]
mass_bounds = [0.0, 1500.0]
name = "FormulaGrid_0"
dbe_bounds = [0, 40]

[formula_grid.element_ratios]
O_C = [0.0, 0.8]

[formula_grid.isotopes]
13C = 1

[[formula_grid]]
elements = ["C", "H", "O", "N"]
limits = [[0, 100], [0, 200], [0, 6], [0, 6]]
polarity = 1
H_C_bounds = [0.2, 3.0]
mass_bounds = [0.0, 1500.0]
name = "FormulaGrid_CHON"

[[formula_grid]]
elements = ["C", "H", "O", "N", "S"]
limits = [[0, 100], [0, 200], [0, 6], [0, 6], [0, 1]]
polarity = 1
H_C_bounds = [0.2, 3.0]
mass_bounds = [0.0, 1500.0]
name = "FormulaGrid_0"

[[formula_grid.combine_isotopes]]
13C = 1
18O = 1

[formula_grid.isotopes]
13C = 1
18O = 1
34S = 1
"""


class TestFormulaGrid:

    def setup_method(self):
        self.fg = FormulaGrid(
            elements=["C", "H", "N", "O"],
            limits=[[0, 20], [1, 40], [0, 4], [0, 4]],
            H_C_bounds=[0.2, 3],
            dbe_bounds=[0, 30],
            mass_bounds=[100, 400],
            polarity=1,
            isotopes={(6, 13): 2, (7, 15): 1, (8, 18): 1}
        )

    def test_error_isotopes(self):
        # value error, isotope is not in the element list
        with pytest.raises(ValueError):
            self.fg = FormulaGrid(
                elements=["C", "H"],  limits=[[0, 5], [1, 4]],
                isotopes={(8, 18): 1}, polarity=1)

        # value error, combination include non available isotope
        with pytest.raises(ValueError):
            self.fg = FormulaGrid(
                elements=["C", "H"],  limits=[[0, 5], [1, 4]],
                isotopes={(6, 13): 1}, polarity=1,
                combine_isotopes=[{(6, 13): 1, (8, 18): 1}])

    def test_formulagrid_simple(self):

        assert 'C' in self.fg.element_symbols
        assert 'H' in self.fg.element_symbols
        assert 'N' in self.fg.element_symbols
        assert 'O' in self.fg.element_symbols
        assert len(self.fg.element_symbols) == 4
        assert len(self.fg.integer_parts) == 303

        assert self.fg.element_ratios == {}
        # assert self.fg.chunk_size == 5_000_000

        assert len(self.fg) == 5
        assert len(self.fg[6, 13, 2].values) == 10936
        assert len(self.fg.get_group(300)) == 272

        assert self.fg.exact_masses.keys() == {"C", "O", "N", "H", "13C",
                                               "15N", "18O"}
        assert self.fg.exact_masses["C"] == approx(12.0)
        assert self.fg.exact_masses["O"] == approx(15.9949146195717)
        assert self.fg.exact_masses["N"] == approx(14.003074004432)
        assert self.fg.exact_masses["H"] == approx(1.007825032239)
        assert self.fg.exact_masses["13C"] == approx(13.0033548350723)
        assert self.fg.exact_masses["18O"] == approx(17.9991596128676)
        assert [iso.symbol for iso in self.fg.isotopes] == ["13C", "15N", "18O"]

        with raises(ValueError):
            FormulaGrid(polarity=1, element_ratios={"O_C": (0, 1)})

    def test_pattern_grids(self):
        assert len(self.fg) == 5
        assert self.fg.no_isotope_grid == self.fg[0, 0, 0]
        assert self.fg[0, 0, 0].values[3].tolist() == [1, 1, 2, 4, 0, 0, 0]
        assert self.fg[6, 13, 1].values[0].tolist() == [0, 1, 2, 4, 1, 0, 0]
        assert self.fg[0, 0, 0].nominal_mass[3] == self.fg[6, 13, 1].nominal_mass[0]

        assert len(self.fg[6, 13, 1].isotopes) == 1
        assert self.fg[6, 13, 1].isotopes[0].element.symbol == "C"
        assert self.fg[6, 13, 1].mz_shift == approx(1.00335483)
        assert [iso.symbol for iso in self.fg[6, 13, 1].isotopes] == ["13C"]

        fg = FormulaGrid(
            elements=["C", "H", "O", "B"],
            limits=[[10, 20], [10, 30], [1, 3], [1, 3]],
            polarity=-1,
            isotopes={(6, 13): 1, (5, 10): 1, (8, 18): 1},
            combine_isotopes=[{(5, 10): 1, (6, 13): 1}, 
                              {(8, 18): 1, (6, 13): 1}]
        )

        assert fg.exact_masses.keys() == {"C", "H", "O", "B", "13C", "10B",
                                          "18O"}
        assert len(fg) == 6

        assert list(fg.keys()) == [(0, 0, 0), (5, 10, 1), (6, 13, 1), (8, 18, 1),
                                   (5, 10, 1, 6, 13, 1), (6, 13, 1, 8, 18, 1)]

        assert [iso.symbol for iso in fg[6, 13, 1, 8, 18, 1].isotopes] == ["13C", "18O"]
        assert fg[6, 13, 1, 8, 18, 1].isotope_numbers == [1, 1]
        assert fg[6, 13, 1, 8, 18, 1].mz_shift == approx(3.007599828368)

    def test_no_carbon_grid(self):

        fg = FormulaGrid(
            elements=["Li", "O"],
            limits=[[0, 2], [0, 6]],
            polarity=-1,
        )

        assert fg[(0, 0, 0)].values.shape == (21, 2)
        df = pd.DataFrame(fg[(0, 0, 0)].values, columns=['O', 'Li'])
        assert list(df.O.unique()) == list(range(7))
        assert list(df.Li.unique()) == [0, 1, 2]
        assert fg[(0, 0, 0)].nominal_mass[13] == 103

        fg2 = FormulaGrid.from_formula("Li2 O6", polarity=-1)
        assert all(pd.DataFrame(fg2[(0, 0, 0)].values, columns=['O', 'Li']) == df)

    def test_from_formula(self):

        fg = FormulaGrid.from_formula(
            "C20 H40 O2 N2 13C2",
            polarity=1,
            H_C_bounds=[1., 2.])

        # assert fg.keys() == [(0, 0, 0), (6, 13, 1), (6, 13, 2)]
        assert fg.element_symbols == ["C", "H", "O", "N"]
        assert fg.H_C_bounds == [1., 2.]
        assert fg.limits == [[1, 22], [0, 40], [0, 2], [0, 2]]
        assert fg.keys() == {(0, 0, 0), (6, 13, 1), (6, 13, 2)}

    def test_formula_grid_dict(self):

        d = FormulaGrid.from_formula("C10 H20", polarity=-1)
        assert "isotopes" not in d
        assert "combine_isotopes" not in d
        assert "element_ratios" not in d

        d = self.fg.as_dict(symbol=False)
        # here set is just to avoid to check order
        assert set(d["elements"]) == set(["C", "H", "N", "O"])
        assert set(d["isotopes"].keys()) == set([(6, 13), (7, 15), (8, 18)])
        assert d["polarity"] == 1
        assert "combine_isotopes" not in d

        fg = FormulaGrid(
            elements=["C", "H", "O", "B"],
            limits=[[10, 20], [10, 30], [1, 3], [1, 3]],
            polarity=-1,
            isotopes={(6, 13): 1, (5, 10): 1, (8, 18): 1},
            combine_isotopes=[{(5, 10): 1, (6, 13): 1}, 
                              {(8, 18): 1, (6, 13): 1}]
        )
        d = fg.as_dict(symbol=True)
        assert set(d["isotopes"].keys()) == set(["13C", "10B", "18O"])
        assert set(d["combine_isotopes"][0].keys()) == set(["13C", "10B"])

    def test_formula_grid_lewis(self):

        # test charge = +1, no filter
        fg1 = FormulaGrid(
            elements=["C", "H", "N", "O"],
            limits=[[10, 12], [16, 16], [1, 2], [4, 4]],
            polarity=1,
            lewis_filter=False,
        )
        assert fg1[(0, 0, 0)].values.shape == (6, 4)

        # test filter with charge = +1 no Na => only N with odd number
        fg2 = FormulaGrid(
            elements=["C", "H", "N", "O"],
            limits=[[10, 12], [16, 16], [1, 2], [4, 4]],
            polarity=1,
            lewis_filter=True
        )
        assert fg2[(0, 0, 0)].values.shape == (3, 4)
        formulas = list(fg2[(0, 0, 0)].to_dataframe()["formula"].values)
        assert formulas == ["C10 H16 O4 N1", "C11 H16 O4 N1", "C12 H16 O4 N1"]

        # test filter with charge = -1 => only N with odd number
        fg3 = FormulaGrid(
            elements=["C", "H", "N", "O"],
            limits=[[10, 12], [16, 16], [1, 2], [4, 4]],
            polarity=-1,
            lewis_filter=True
        )
        assert fg3[(0, 0, 0)].values.shape == (3, 4)
        formulas = list(fg3[(0, 0, 0)].to_dataframe()["formula"].values)
        assert formulas == ["C10 H16 O4 N1", "C11 H16 O4 N1", "C12 H16 O4 N1"]

        # test with charge -2 => even number of N
        fg4 = FormulaGrid(
            elements=["C", "H", "N", "O"],
            limits=[[10, 12], [16, 16], [1, 2], [4, 4]],
            polarity=-2,
            lewis_filter=True
        )
        assert fg4[(0, 0, 0)].values.shape == (3, 4)
        formulas = list(fg4[(0, 0, 0)].to_dataframe()["formula"].values)
        assert formulas == ["C10 H16 O4 N2", "C11 H16 O4 N2", "C12 H16 O4 N2"]

        # test with Na and charge +1 => N even if Na, N odd if no Na
        fg = FormulaGrid(
            elements=["C", "H", "N", "O", "Na"],
            limits=[[10, 12], [16, 16], [1, 2], [4, 4], [0, 1]],
            # mass_bounds=[100, 400],
            polarity=1,
            lewis_filter=True
        )
        assert fg[(0, 0, 0)].values.shape == (6, 5)
        formulas = list(fg[(0, 0, 0)].to_dataframe()["formula"].values)
        assert formulas == ["C10 H16 O4 N1", "C10 H16 O4 N2 Na1", 
                            "C11 H16 O4 N1", "C11 H16 O4 N2 Na1", 
                            "C12 H16 O4 N1", "C12 H16 O4 N2 Na1"]

        # test with Na and charge +2 => N odd if Na, N even if no Na
        fg5 = FormulaGrid(
            elements=["C", "H", "N", "O", "Na"],
            limits=[[10, 12], [16, 16], [1, 2], [4, 4], [0, 1]],
            # mass_bounds=[100, 400],
            polarity=2,
            lewis_filter=True
        )
        assert fg5[(0, 0, 0)].values.shape == (6, 5)
        formulas = list(fg5[(0, 0, 0)].to_dataframe()["formula"].values)
        assert formulas == ["C10 H16 O4 N1 Na1", "C10 H16 O4 N2", 
                            "C11 H16 O4 N1 Na1", "C11 H16 O4 N2", 
                            "C12 H16 O4 N1 Na1", "C12 H16 O4 N2"]


class TestIterationFormulaGridList:

    def setup_method(self):
        pass

    def test_load(self, tmp_path):

        fg_toml = tmp_path / "test.toml"
        with fg_toml.open("w") as f:
            f.write(FG_TOML)

        fg_list = FormulaGridList()
        fg_list.load(tmp_path / "test.toml")
        assert len(fg_list) == 3

        fg_list = FormulaGridList(tmp_path / "test.toml")
        assert len(fg_list) == 3
        fg_list.load(tmp_path / "test.toml")
        assert len(fg_list) == 6


class TestFormulaGridControls:

    def setup_method(self):
        self.base_params = dict(
            polarity=1, elements=["O", "Li"], limits=[[0, 10], [0, 20]])

    def test_formula_grid_controls_validation(self):

        ctrl = FormulaGridControls(**self.base_params)
        
        assert ctrl.elements == ["O", "Li"]
        assert ctrl.mass_bounds == (0, 2000)

        d = ctrl.model_dump()
        assert len(d) == 11
        assert d["element_ratios"] is None

        fg = FormulaGrid.from_dict(d)
        assert len(fg) == 1
        assert fg.element_symbols == ctrl.elements

        with raises(ValidationError):
            ctrl = FormulaGridControls(mass_bounds=(10, 2), **self.base_params)

        with raises(ValidationError):
            ctrl = FormulaGridControls(H_C_bounds=(2, .2), **self.base_params)

        with raises(ValidationError):
            ctrl = FormulaGridControls(dbe_bounds=(30, 20), **self.base_params)

        with raises(ValidationError):
            ctrl = FormulaGridControls(
                polarity=1, elements=["O", "Li"], limits=[[100, 10], [0, 20]])

        with raises(ValidationError):
            ctrl = FormulaGridControls(
                polarity=1, elements=["O", "Li", "C"], limits=[[0, 10], [0, 20]])

        with raises(ValidationError):
            ctrl = FormulaGridControls(
                polarity=0, elements=["O", "Li"], limits=[[0, 10], [0, 20]])
            
        with raises(ValidationError):
            ctrl = FormulaGridControls(
                polarity=3.14, elements=["O", "Li"], limits=[[0, 10], [0, 20]])

        with raises(ValidationError):
            ctrl = FormulaGridControls(
                polarity="pos", elements=["O", "Li"], limits=[[0, 10], [0, 20]])

        with raises(ValidationError):
            ctrl = FormulaGridControls(
                polarity=1, elements=["O", "Li"], limits=[[0, 10], [0, 20]],
                element_ratios={"O_X": (0, 1)})
        
        with raises(ValidationError):
            ctrl = FormulaGridControls(
                polarity=1, elements=["O", "Li"], limits=[[0, 10], [0, 20]],
                element_ratios={"OX": (0, 1)})
