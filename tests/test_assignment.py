#!/usr/bin/env python
# coding: utf-8

from pytest import approx

# import sys
# sys.path.insert(0, os.path.join(os.path.dirname(__file__), "../src/"))

import numpy as np
from pyc2mc.core.formula_grid import FormulaGrid
from pyc2mc.processing.assignment import assign_candidates
from pyc2mc.core.peak import Peak

masses = [500.19262, 501.30691, 502.21234, 503.14995, 503.18636, 504.27412,
          505.21094, 505.25051, 506.15963, 506.20532, 507.16794, 508.20252,
          508.40078, 509.16245, 509.31208]


class TestAssignCandidates:

    def setup_method(self):
        self.fg = FormulaGrid(
            elements=["C", "H", "N", "O", "Na"],
            limits=[[10, 30], [30, 50], [0, 3], [0, 10], [0, 2]],
            H_C_bounds=[0.2, 3],
            mass_bounds=[350, 450],
            polarity=1,
            # isotopes={(5, 10): 1, (6, 13): 2},
            # combine_isotopes=[{(5, 10): 1, (6, 13): 1}]
        )
        self.mass = 380.2772
        self.candidates = ['C21 H38 O1 N2 Na2',
                           'C20 H39 O4 N1 Na1',
                           'C19 H40 O7']

        self.fg1 = FormulaGrid(
            elements=["C", "H", "N", "O", "B"],
            limits=[[15, 40], [15, 60], [0, 2], [0, 20], [0, 1]],
            H_C_bounds=[0.3, 2.4],
            dbe_bounds=[0, 50],
            mass_bounds=[500, 510],
            polarity=-1,
            element_ratios={("O", "C"): (0, 1.4)},
            isotopes={(5, 10): 1, (6, 13): 2, (8, 18): 1},
        )

    def test_one_peak(self):
        peak = Peak(mz=self.mass, intensity=1, pid=1504)
        df = assign_candidates(peak, self.fg)

        assert "cand_list" not in df

        df = assign_candidates(peak, self.fg, returns_candidates_data=True)

        assert "cand_list" in df
        assert len(df) == 1
        candidate = df.iloc[0]
        assert candidate.integer_part == 380
        assert candidate.upper_limit == approx(380.2775802772)
        assert candidate.lower_limit == approx(380.2768197228)
        assert candidate.pid == 1504

        candidates = [cand.strip() for cand in candidate.candidates.split(",")]
        assert candidates == self.candidates

    def test_one_mass(self):
        df = assign_candidates(self.mass, self.fg,
                               returns_candidates_data=True)

        candidate = df.iloc[0]
        assert candidate.integer_part == 380
        assert candidate.upper_limit == approx(380.2775802772)
        assert candidate.lower_limit == approx(380.2768197228)
        assert candidate.pid == 0

        candidates = [cand.strip() for cand in candidate.candidates.split(",")]
        assert candidates == self.candidates

    def test_assign_candidates(self, benchmark):
        candidates = benchmark(
            assign_candidates,
            peak_list=masses,
            formula_grid=self.fg1,
            lambda_parameter=0.3,
            returns_candidates_data=True
        )

        assert len(candidates) == len(masses)

        candidates.dropna(axis="rows", subset="candidates", inplace=True)
        assert len(candidates) == 14

        cand_list = candidates.loc[3, "cand_list"]
        assert len(cand_list) == 4
        assert len(cand_list[cand_list.isotopic]) == 3
        formula = ['C32 H23 O6', 'C29 H21 O6 N1 B1 13C1', 'C38 H18 B1 18O1',
                   'C31 H21 O4 N2 18O1']
        assert cand_list.formula.values.tolist() == formula
        assert cand_list.nominal_mass.values[0] == 503
        error_ppm = np.array([-0.12330087, -0.29052642,
                              0.17064978, 0.21791329])
        assert np.all(np.isclose(cand_list.err_ppm, error_ppm, rtol=1e-5))

        n_cand = [len(cl) for cl in candidates.cand_list]
        n_cand_no_iso = [len(cl[~cl.isotopic]) for cl in candidates.cand_list]
        assert n_cand == [1, 2, 2, 4, 3, 2, 1, 3, 1, 2, 2, 2, 2, 2]
        assert n_cand_no_iso == [1, 1, 0, 1, 1, 1, 0, 1, 0, 2, 0, 1, 2, 1]
