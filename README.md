# PyC2MC

PyC2MC provides a set of classes and processing tools to implement
data analyses in order to ease the molecular charaterization of complex matrices.

## General

The file `developers.rst` provides few hints about the contents of the 
code and the naming of several quantities.

Several tasks can be achieve using [`invoke`](https://www.pyinvoke.org).
Run `invoke -l` in the terminal or look at `tasks.py` file.

## Installation

In order to install and use the library, follow these steps. The 
creation of a dedicated environment is optional.

Right now, the installation is more a developper mode.

If you download the source code by clonning the git repository, it is
strongly recommanded to not work in the pyc2mc folder unless you are 
editing the code itself (of course).

### Create a new conda environment

Optional

```shell
$ > conda create --name c2mc python=3.9
$ > conda activate c2mc
```

### Download source or install using pip

Using pip

```shell
$ > pip install git+https://github.com/iC2MC/pyc2mc.git
```

Download sources and installation

```shell
$ > git clone https://github.com/iC2MC/pyc2mc.git
$ > cd pyc2mc
$ > pip install -e .
```

The `-e` option means `--editable`. This option is mainly for developpers.

### Update the code

In the `pyc2mc` folder, use git to update the source code.

```shell
$ > git pull
```

If you did not use the `-e` option, you need to reinstall the code.

```shell
$ > cd pyc2mc
$ > pip install .
```

If you installed pyc2mc directly using pip, run again: 

```shell
$ > pip install --update git+https://github.com/iC2MC/pyc2mc.git
```

### Full installation of dependencies

If you intend to develop new features in pyc2mc, you will need more tools
to run tests, build the documentation and other stuffs. This is 
possible from the installation options:

* `docs`
* `tests`
* `full_dev`

```shell
$ > pip install -e .[full_dev]
```

If your shell is zsh (MacOS) or the previous does not work, these lines may help
and produce the same result

```shell
$ > pip install -e .[full_dev]
$ > pip install -e .\[full_dev\]
$ > pip install -e ".[full_dev]"
```
