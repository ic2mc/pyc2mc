===========================
Standard Attribution module
===========================

.. automodule:: pyc2mc.processing.standard_attribution

The following functions aim to implement the attribution of molecular
formula following different strategies by considering a formula grid
and a previous assignment.

.. autofunction:: pyc2mc.processing.standard_attribution.lowest_error_attribution

.. autofunction:: pyc2mc.processing.standard_attribution.attribute_pattern

.. autofunction:: pyc2mc.processing.standard_attribution.attribute_isotopes

.. autofunction:: pyc2mc.processing.standard_attribution.isotopic_pattern_attribution



