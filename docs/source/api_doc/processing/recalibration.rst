=============
Recalibration
=============

.. automodule:: pyc2mc.processing.recalibration

Recalibration from calibration lists
====================================

The ``Recalibration`` class performs the recalibration
of 1D data using a given model. Look at the 
`recalibration_model module <recalibration_model>`_  for details on the
available models. The classes below rely on a predefined calibration list
which contains essentially a list of theoretical m/z values and measured
m/z values.

Recalibration needs the following arguments:

* the calibration list, used to determine the parameters
* the calibration model

The ``WalkingCalibration`` class performs a walking calibration of a 1D
mass spectrum by dividing the m/z range into a set of segments.

The walking calibration needs the same parameters as the standard
recalibration plus:

* the width or the edges of the segments.
* a calibration model for each segments

The widths of the segments can be constant over the m/z domain or variable.
An automatic calculation can be done from the calibration list object
to determine segment edges using a minimal number of point per segment and
a minimal segment width, see 
`CalibList.get_walking_segments <CalibList.get_walking_segments>`_.


Class BaseRecalibration
-----------------------

This class must not be used directly.

.. autoclass:: pyc2mc.processing.recalibration.BaseRecalibration
    :members:


Class Recalibration
-------------------

.. autoclass:: pyc2mc.processing.recalibration.Recalibration
    :members:


Class WalkingRecalibration
--------------------------

.. autoclass:: pyc2mc.processing.recalibration.WalkingCalibration
    :members:

Recalibration from mass differences
===================================

This class implements the calibration of the data using a mass differences
analyses. Look at the `MassDifferences class <MassDifferences>`_ for
more details.

Class MassDifferencesCalibration
--------------------------------

.. autoclass:: pyc2mc.processing.recalibration.MassDifferencesCalibration
    :members:
