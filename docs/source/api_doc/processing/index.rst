==================
Processing modules
==================

.. automodule:: pyc2mc.processing

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   recalibration
   caliblist_builder
   recalibration_models
   mass_differences
   assignment
   attribution
   standard_attribution
   kendrick_series
   kendrick_attribution
   roi_detection
