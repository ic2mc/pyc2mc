==================
ROI detection
==================

.. automodule:: pyc2mc.processing.roi_detection

.. .. autofunction:: pyc2mc.processing.roi_detection.unifying_rois

Class FeatureDetection
======================

.. autoclass:: pyc2mc.processing.roi_detection.FeatureDetection
    :members:

Class KmeansDetection
=====================

.. autoclass:: pyc2mc.processing.roi_detection.KmeansDetection
    :members:
