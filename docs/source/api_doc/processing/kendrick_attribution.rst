====================
Kendrick Attribution
====================

.. automodule:: pyc2mc.processing.kendrick_attribution

.. autofunction:: pyc2mc.processing.kendrick_attribution.kendrick_attribution

.. autofunction:: pyc2mc.processing.kendrick_attribution.attribute_isotopic_series

.. autofunction:: pyc2mc.processing.kendrick_attribution.lowest_error_series_attribution
