==================
Attribution module
==================

.. automodule:: pyc2mc.processing.attribution


.. autoclass:: pyc2mc.processing.attribution.AttributionAlgorithm
    :members:
    

Peak list attribution
=====================

.. autoclass:: pyc2mc.processing.attribution.PeakListAttribution
    :members:

Iteration tables
================

.. autoclass:: pyc2mc.processing.attribution.IterationTable
    :members:
