==================
N dimensional data
==================

.. automodule:: pyc2mc.io.nddata

Reading of xml files
====================

.. autofunction:: read_MZxml

.. autofunction:: read_mzml

