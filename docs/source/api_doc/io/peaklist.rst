===========================
Reading peaklist from files
===========================

.. automodule:: pyc2mc.io.peaklist

Reading Functions
=================

.. autofunction:: read_peaklist

.. autofunction:: read_ascii

.. autofunction:: read_pks

.. autofunction:: read_predator

.. autofunction:: read_csv

.. autofunction:: read_txt



