==========
io modules
==========

.. automodule:: pyc2mc.io

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   peaklist
   nddata
   caliblist