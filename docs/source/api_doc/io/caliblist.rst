=========
caliblist
=========

.. automodule:: pyc2mc.io.caliblist


Reading functions
=================

.. autofunction:: pyc2mc.io.caliblist.read_caliblist

.. autofunction:: pyc2mc.io.caliblist.read_txt

.. autofunction:: pyc2mc.io.caliblist.read_ref

.. autofunction:: pyc2mc.io.caliblist.read_csv


Utility functions to manage input path
======================================

The functions listed bellow are low level functions that are
used to manage the information given through the input path
argument of the main functions of the class.

.. autofunction:: pyc2mc.io.caliblist.get_file_format