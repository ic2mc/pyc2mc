======================
Time dependent modules
======================

.. automodule:: pyc2mc.signal_processing

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   coadd
