==================================
Co-Addition of Profile Signal Data
==================================

.. automodule:: pyc2mc.signal_processing.coadd


Class FT_ICR_SignalCollection
=============================

.. autoclass:: pyc2mc.signal_processing.coadd.FT_ICR_SignalCollection
    :members:

    
