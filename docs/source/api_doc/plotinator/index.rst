==========
Plotinator
==========

.. automodule:: pyc2mc.plotinator

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   peak_plotter
   roi_plotter
   td_data_plotter
   calibration_plotter
   isotopic_plotter
   widgenator


Configuration
=============

Matplotib style is configured in the file ``pyc2mc.mplstyle``.

Utility functions
=================

The functions below are low level tools to perform simple plots which 
return a matplotlib.pyplot.subplot figure and ax.

.. autofunction:: pyc2mc.plotinator.default_plots.scatter_plot

.. autofunction:: pyc2mc.plotinator.default_plots.stem_plot

.. autofunction:: pyc2mc.plotinator.default_plots.bar_plot
