=================
Widgenator module
=================

.. automodule:: pyc2mc.plotinator.widgenator

.. autofunction:: pyc2mc.plotinator.widgenator.display_chem_classes

.. image:: widget_class_1.png
    :width: 75%
    :align: center

.. image:: widget_class_2.png
    :width: 75%
    :align: center



.. autofunction:: pyc2mc.plotinator.widgenator.display_error

.. image:: widget_error.png
    :width: 75%
    :align: center

.. autofunction:: pyc2mc.plotinator.widgenator.display_kendrick_series

.. image:: widget_kendrick.png
    :width: 75%
    :align: center
