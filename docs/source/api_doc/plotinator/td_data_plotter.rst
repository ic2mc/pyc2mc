==================================
Time dependent data plotter module
==================================

.. automodule:: pyc2mc.plotinator.td_data_plotter

Class TDDataPlotter
====================

.. autoclass:: pyc2mc.plotinator.td_data_plotter.TDDataPlotter
    :members:


Class AttributedTDDataPlotter
=============================

.. autoclass:: pyc2mc.plotinator.td_data_plotter.AttributedTDDataPlotter
    :members:
