===================
peak plotter module
===================

.. automodule:: pyc2mc.plotinator.peak_plotter

Class PeakListPlotter
=====================

.. autoclass:: pyc2mc.plotinator.peak_plotter.PeakListPlotter
    :members:
