==========================
Calibration plotter module
==========================

.. automodule:: pyc2mc.plotinator.calibration_plotter

Class CalibListPlotter
=========================

.. autoclass:: pyc2mc.plotinator.calibration_plotter.CalibListPlotter
    :members:
