==================
roi plotter module
==================

.. automodule:: pyc2mc.plotinator.roi_plotter

Class ROIPlotter
================

.. autoclass:: pyc2mc.plotinator.roi_plotter.ROIPlotter
    :members:

Class ROIListPlotter
=====================

.. autoclass:: pyc2mc.plotinator.roi_plotter.ROIListPlotter
    :members: