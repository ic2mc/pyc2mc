================
API Documenation
================

This part of the documentation presents all the modules and classes included
in the PyC2MC API. It provides for the developer a full documentation of 
all the available functions, methods, objects and attributes available.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   core/index
   io/index
   signal_processing/index
   processing/index
   time_dependent/index

.. toctree::
   :maxdepth: 1

   plotinator/index

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`