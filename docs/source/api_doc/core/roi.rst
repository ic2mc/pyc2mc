========================
Region Of Interest (ROI)
========================

.. automodule:: pyc2mc.core.roi

Class ROI
=========

.. autoclass:: pyc2mc.core.roi.ROI
    :members:

Class AttributedROI
===================

.. autoclass:: pyc2mc.core.roi.AttributedROI
    :members:

Class ROIList
=============

.. autoclass:: pyc2mc.core.roi.ROIList
    :members:
    :private-members:

Class AttributedROIList
=======================

.. autoclass:: pyc2mc.core.roi.AttributedROIList
    :members:
    :private-members:
