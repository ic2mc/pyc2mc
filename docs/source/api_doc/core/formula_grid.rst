============
Formula Grid
============

.. automodule:: pyc2mc.core.formula_grid


Class PatternGrid
=================

.. autoclass:: pyc2mc.core.formula_grid.PatternGrid
    :members:

Class Formual Grid
==================

.. autoclass:: pyc2mc.core.formula_grid.FormulaGrid
    :members:

Class FormulaGridList
=====================

.. autoclass:: pyc2mc.core.formula_grid.FormulaGridList
    :members:

