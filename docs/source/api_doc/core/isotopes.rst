========
Isotopes
========

.. automodule:: pyc2mc.core.isotopes

Class Element
=============

.. autoclass:: pyc2mc.core.isotopes.Element
    :members:

Class Isotope
=============

.. autoclass:: pyc2mc.core.isotopes.Isotope
    :members:
