============
Core modules
============

.. automodule:: pyc2mc.core

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   isotopes
   formula
   peak
   ft_icr_signal
   formula_grid
   calibration
   peak_search
   kendrick
   nddata
   roi
   