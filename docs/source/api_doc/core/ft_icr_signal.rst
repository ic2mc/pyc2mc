=============
FT ICR Signal
=============

.. automodule:: pyc2mc.core.ft_icr_signal
    
Class AttributedPeak
====================

.. autoclass:: pyc2mc.core.ft_icr_signal.SignalMetadata
    :members:

Class FT_ICR_Signal
===================

.. autoclass:: pyc2mc.core.ft_icr_signal.FT_ICR_Signal
    :members:
