==================
N-dimensional data
==================

.. automodule:: pyc2mc.core.nddata

Class PeakListCollection
========================

.. autoclass:: pyc2mc.core.nddata.PeakListCollection
    :members:
