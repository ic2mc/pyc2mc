===============
Kendrick Series
===============

.. automodule:: pyc2mc.core.kendrick

Class KendrickSeries
====================

.. autoclass:: pyc2mc.core.kendrick.KendrickSeries
    :members:

Class KendrickSeriesList
========================

.. autoclass:: pyc2mc.core.kendrick.KendrickSeriesList
    :members:

Class AttributedKendrickSeries
==============================

.. autoclass:: pyc2mc.core.kendrick.AttributedKendrickSeries
    :members:

Class AttributedKendrickSeriesList
==================================

.. autoclass:: pyc2mc.core.kendrick.AttributedKendrickSeriesList
    :members: