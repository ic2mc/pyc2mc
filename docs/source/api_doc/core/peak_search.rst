===========
Peak search
===========

.. automodule:: pyc2mc.core.peak_search

Peak search selection mode
==========================

The following classes is an enumeration of the available algorithms and
provides access to the related functions.

.. autoclass:: pyc2mc.core.peak_search.SearchingMode

Peak search functions
=====================

This first function is used to apply a correction and try to minimize
the standard error deviation.

.. autofunction:: pyc2mc.core.peak_search.apply_mean_error_correction

The following functions are used to search peaks.

.. autofunction:: pyc2mc.core.peak_search.select_closest_mz

.. autofunction:: pyc2mc.core.peak_search.select_closest_mz_corrected

.. autofunction:: pyc2mc.core.peak_search.select_most_abundant

.. autofunction:: pyc2mc.core.peak_search.select_most_abundant_corrected
