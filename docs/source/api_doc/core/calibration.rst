===========
Calibration
===========

.. automodule:: pyc2mc.core.calibration

Class CalibList
===============

.. autoclass:: pyc2mc.core.calibration.CalibList
    :members:

Utility functions
=================

The following function is a low level function used by the main
classes of this module to look for peaks from a peak list and return
candidates.

.. autofunction:: pyc2mc.core.calibration.candidates_searching
