========
Formula
========

.. automodule:: pyc2mc.core.formula


Class Composition
=================

.. autoclass:: pyc2mc.core.formula.Composition
    :members:

Class Formula
=============

.. autoclass:: pyc2mc.core.formula.Formula
    :members:

Class IsotopicPattern
=====================

.. autoclass:: pyc2mc.core.formula.IsotopicPattern
    :members:


Utility functions
=================

The functions below are low level tools to work with formula or make
operations on formulas.

.. autofunction:: pyc2mc.core.formula.all_exact_masses

.. autofunction:: pyc2mc.core.formula.compute_dbe

.. autofunction:: pyc2mc.core.formula.get_str_formula
