===================
Time dependent data
===================

.. automodule:: pyc2mc.time_dependent.td_data


Class TimeDependentData
-----------------------

.. autoclass:: pyc2mc.time_dependent.td_data.TimeDependentData
    :members:


Class AttributedTimeDependentData
---------------------------------

.. autoclass:: pyc2mc.time_dependent.td_data.AttributedTimeDependentData
    :members:

