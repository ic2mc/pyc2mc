==================
User documentation
==================

This part of the documentation provides case studies examples to show
how to use the library from jupyter notebooks.

Quick start
===========

The `quickstart notebook <./quickstart.ipynb>`_ is a good starting
point to explore the main classes of PyC2MC.

.. nbgallery::
    quickstart


Tutorials
=========

The notebooks below provide tutorials and detailed explanations for
using the processing tools available in ``PyC2MC``.

1D data
-------

.. nbgallery::
    recalibrate_from_ref
    walking_calibration_1D
    calibration_from_kendricks
    isotopic_pattern_attribution
    kendrick_attribution


Time Dependent data
-------------------

.. nbgallery::
    time_dependent_recalibration
    feature_detection
    kmeans_detection


Plotting facilities
-------------------

.. nbgallery::
    plotting
    wplotting


Template notebooks
==================

The notebooks in this gallery provide templates to run your own 
processing on your data.

.. nbgallery::
    :caption: Template notebooks

    isotopic_pattern_attribution
