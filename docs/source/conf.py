# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
try:
    import pyc2mc
except ImportError:
    sys.path.insert(0, os.path.abspath('../../src/'))


# -- Project information -----------------------------------------------------

project = 'PyC2MC'
copyright = '2021, Carlos Celis Cornejo, Germain Salvato Vallverdu'
author = 'Carlos, Germain'

# The full version, including alpha/beta/rc tags
release = pyc2mc.__version__


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "sphinx.ext.autodoc",  # automatic generation of doc
    "sphinx.ext.viewcode",  # link to code source in the doc
    "sphinx.ext.napoleon",  # doc format
    "nbsphinx",  # jupyter notebook support
    "sphinx_gallery.load_style",  # notebook gallery
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']
html_logo = '_static/img/logo_transp.png'
html_theme_options = {
    'logo_only': True,
    'display_version': True,
    'style_external_links': True,
}

# These paths are either relative to html_static_path
# or fully qualified paths (eg. https://...)
html_css_files = [
    'css/custom.css',
]

# -- Custum conf -------------------------------------------------------------

autodoc_member_order = "bysource"
autoclass_content = 'both'
master_doc = 'index'
# add_module_names = False
modindex_common_prefix = ["pyc2mc."]

# shorten type hints in signature
autodoc_typehints = 'none'
# napoleon_custom_sections = [('Returns', 'params_style')]

# nbsphinx
nbsphinx_prolog = """.. rst-class:: right

`Download notebook <{{ env.doc2path(env.docname) }}>`_

"""
# with base=None, this add only the path from the root source directory of the doc
# https://example.org/notebooks/{{ env.doc2path(env.docname, base=None) }}
