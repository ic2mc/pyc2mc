===========================================
PyC2MC: Python analysis of complex matrices
===========================================

.. contents:: Table of contents

--------------------

pyc2mc is a python library that aims to perform in depth data analysis
of 1D mass spectrometry data or 2D coupled LC-MS or GC-MS data. The
library provides an high level interface to efficient methodologies to
perform molecular assignment, calibration and 1D data treatment.


Main features
=============

Main features of the pyc2mc library included:

* Isotopic attribution and isotopic patterns recognition
* Kendrick Series attribution
* 1D and 2D calibration of MS data coupled to chromatography
* Region of interest detection in two dimensional data.

Citing pyc2mc
=============

Please, consider to cite the following papers when using pyc2mc ...

Installation
============

Right now PyC2CM is only available as a python library associated to a 
set of case study examples under jupyter notebooks. In consequence,
you need first to have a python distribution installed on your computer.
Python version 3.9 or higher is required. It is recommended to install the
`anaconda distribution <https://www.anaconda.com/products/distribution>`_, 
in particular on windows, which will provides both python and a set
of packages or tools useful. The following assume that anaconda is installed
on your computer.

In order to install and use the library it is recommended to create a 
dedicated environment. follow these steps from the anaconda prompt available
from the start menu of you computer:

Short installation
------------------

The following command will install pyc2mc as any other python package
in the current python environment.

.. code-block:: console

    $ > python -m pip install git+https://git.univ-pau.fr/ic2mc/pyc2mc.git


You can now have access to the ``pyc2mc`` library. 

Developer installation
----------------------

The following will download the source code of pyc2mc and install it. If
you intend to develop new features in pyc2mc, it is strongly recommended 
to use a python environment.

.. code-block:: console

    $ > conda create --name c2mc python=3.9
    $ > conda activate c2mc
    $ > git clone https://git.univ-pau.fr/ic2mc/pyc2mc.git
    $ > cd pyc2mc
    $ > python -m pip install -e .


The `-e` option means `--editable` and avoid to have to reinstall the
package each time you make a modification or update it to a new version.

If you want to install the `options` as specified in the `setup.cfg`,
change the last line to this one

.. code-block:: console

    $ > pip install -e .[DEV]


If your shell is zsh or the previous does not work, these lines may help
and produce the same result

.. code-block:: console

    $ > pip install -e .[DEV]
    $ > pip install -e .\[DEV\]
    $ > pip install -e ".[DEV]"


Licence and contact
===================

This software was developed in the context of the international joint
laboratory `iC2MC <https://ic2mc.cnrs.fr>`_.

**Authors and contacts**

* Carlos Celis Cornejo `Carlos Celis Cornejo <mailto:carlos.celis-cornejo@univ-pau.fr>`_
* Germain Salvato Vallverdu: `Germain Salvato Vallverdu <mailto:germain.vallverdu@univ-pau.fr>`_
* Julien Maillard `Julien Maillard <mailto:julien.maillard@totalenergies.com>`_
* Christopher Rüger `Christopher Rüger <mailto:christopher.rueger@uni-rostock.de>`_
* Pierre Giusti `Pierre Giusti <mailto:pierre.giusti@total.com>`_
* Carlos Afonso `Carlos Afonso <mailto:carlos.afonso@univ-rouen.fr>`_
* Brice Bouyssiere `Brice Bouyssiere <mailto:brice.bouyssiere@univ-pau.fr>`_
* Ryan Rodgers `Ryan Rodgers <mailto:rodgers@magnet.fsu.edu>`_
