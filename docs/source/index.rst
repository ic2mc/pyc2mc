.. PyC2MC documentation master file, created by
   sphinx-quickstart on Wed Jul 21 10:25:51 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
    :caption: Introduction
    :hidden:
    
    introduction

.. include:: introduction.rst


Documentation of PyC2MC
=======================

.. toctree::
   :maxdepth: 1

   user_doc/index
   gui_doc/index
   api_doc/index



|iC2MC|

.. |iC2MC| image:: https://ic2mc.cnrs.fr/wp-content/uploads/2020/10/logos-3.png
  :target: https://ic2mc.cnrs.fr
