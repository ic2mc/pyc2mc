==================================
Contributing and coding guidelines
==================================

Key variables
=============

MS variables
------------

``exact_mass``:
    stands for the mass in gram per mol extracted from 
    `NIST data <https://www.nist.gov/pml/atomic-weights-and-isotopic-compositions-relative-atomic-masses>`_
    and computed for a formula or an isotope.
    In the exact mass, the polarity or the charge is not included.

``mz``:
    Stands for the ionic mass associated to a peak. ``mz`` values are 
    experimental (measured) values. The unit is in gram per mol per 
    elemental charge.

``calculated_mz``:
    Stands for the ionic mass associated to a formula or a chemical 
    species computed theoretically from the composition. 
    The unit is in gram per mol per elemental charge.

``intensity``:
    stands for the intensity of the signal and is associated to a peak. The
    unit of intensity is the same as the one from the imported data.

``nominal_mass``:
    stands for the mass of a formula calculated using the mass of the most
    abundant isotope of each species rounded to the nearest integer.

``integer_mass``:
    Sum of the mass numbers of the constituent atoms of an molecule or ion.
    Note that contrary to the nominal mass, this includes the real mass
    number of the isotopes.

Most of the definition above come from `IUPAC recommendations <https://doi.org/10.1351/PAC-REC-06-04-06>`_.
A wiki is provided at `this url <http://mass-spec.lsu.edu/msterms/index.php/Category:Final>`_.

More technical variables
------------------------

``lambda_parameter``:
    stands for the expected relative error and is in ``ppm``. 
    The ``lambda_parameter`` is used to define accuracy of assignment and
    attribution methods.

``exact_masses``:
    The `exact_masses` variable is a dictionary which provide as keys a
    species (isotope or not) and as values the exact mass of the species.
    This dictionary is used to speed up the instantiation of Formula object
    avoiding to look for species data in the NIST data base.

``integer_part``:
    This is the integer part of the calculated mz values using the floor
    function. The main aim of this quantity is for grouping masses and 
    speed up searching candidates.

Vocabulary
==========

This help to precise some definitions because of slang (argot) words used
in practice by the community.

Element, species and isotopes
-----------------------------

Element are defined from the atomic number Z. For one given elements,
several isotopes may exist, depending on the mass number, A. Among the
possible isotopes, one of them is the most abundant (or most probable).
Abundance of each isotope comes from the
`NIST data base <https://www.nist.gov/pml/atomic-weights-and-isotopic-compositions-relative-atomic-masses>`_. 

Species correspond to all isotopes included in a composition or formula.
Among the species of the same element, a species is called an isotopes if
it corresponds to an isotope which is not the most abundant.

For example in the composition C1 H6 13C1, the species are C, H and 13C.
13C is considered as an isotope because C (12C) is more abundant.

isotopic
--------

``Formula`` and ``Composition`` can be defined as ``isotopic`` or not. A composition
or formula is defined as isotopic if at least one of its species is
not the most abundant isotope of the corresponding element.

* C2 H6 is not isotopic
* C1 H6 13C1 is isotopic

Composition vs Formula
----------------------

``Composition`` and ``Formula`` are two key objects in the ``fomula`` module.
``Formula`` inherit from ``Composition``. ``Composition`` is just the composition
of the formula. Basically it is a mapping between species and their amount
using a python dictionary. The ``Formula`` represents to the chemical
formula and provide, in addition to the composition, attributes related
to the mass of the formula.

assignment
----------

Assignment is related to the determination of a list of potential candidates
to a given peak (or mz). The assignment rely on a defined accuracy, usually defined
from ``lambda_parameter``. After the assignment, one or several candidate
formulas can be obtained.

attribution
-----------

Attribution comes after assignment. From the list of potential candidates
the attribution will select one final formula based on the selected
algorithm.

Contributing
============

pyc2mc is developed under python 3 and only support python 3.9+

Basic workflow is the following:

1. Create a new branch for your job
2. Implement functionalities and commit changes. 
3. Implement the new tests of the functionalities in ``tests/`` folder
4. Implement the documentation inside the code and update documentation
   files in ``docs\`` if necessary
5. Run full tests and fix possible errors. Edit the tests only if you are
   completely sure about the expected results.
6. Push you job on your branch and ask for a pull request in the main branch

Commit early and commit often and keep your code up to date.
Merge new commits from the main branch
in your dev branch periodically. It will be easier to manage possible conflict
in your branch before adding it in the master branch.

Steps 3, 4 and 5 are the only way to minimize code regression and ensure that
new changes do not affect the whole functionalities. It is a way to do adopt
quality control of the code.

Coding style is supposed to follow python PEP8 recommendations. You can
configure your IDE to help you on that point.
Documentation is required for all modules, classes and methods. In particular,
the method docstrings should make clear the arguments expected and the return
values. 
